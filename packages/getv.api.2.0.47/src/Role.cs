﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Getv.Security;

namespace Getv.Security
{
    public class Role : IRole
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Rank { get; set; }
    }
}
