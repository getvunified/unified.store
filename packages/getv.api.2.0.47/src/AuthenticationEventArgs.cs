using System;

namespace Getv.Security
{
    public class AuthenticationEventArgs : EventArgs
    {
        public IUser User { get; set; }
        public string Token { get; set; }
    }
}