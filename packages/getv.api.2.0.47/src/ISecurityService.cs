using System;
using System.Collections.Generic;
using Foundation;

namespace Getv.Security
{
    public interface ISecurityService
    {
        ApiMessage<IUser> AuthenticateUser(string username, string password, string provider=null);
        ApiMessage<IUser> LoadUserByUsername(string username);
        ApiMessage<IUser> LoadUserById(Guid id);
        ApiMessage<IEnumerable<IUser>> LoadUsersBy(string firstName, string lastName, string username, string emailAddress, long donorAccountNumber);
        ApiMessage InitiatePasswordReset(string emailAddress, string confirmationUrl);
        ApiMessage InitiateEmailAddressChange(string oldEmailAddress, string newEmailAddress, string confirmationUrl);
        ApiMessage ConfirmEmailAddressChange(string token, string confirmationUrl);
        ApiMessage ConfirmPasswordReset(string newPassword, string token);
        ApiMessage RequestForgottenUsername(string emailAddress);
        ApiMessage CreateUser(string username, string password, string emailAddress, string firstName, string lastName, bool emailSignUp);
        ApiMessage SaveUser(Guid id, string username, string emailAddress, string firstName, string lastName, bool emailSignUp, long donorAccountNumber, string donorEnvironment);
        ApiMessage SaveUser(IUser user);
        ApiMessage UpdatePassword(string id, string currentPassword, string newPassword);
        ApiMessage UpdatePasswordByAdmin(string adminId, string userId, string newPassword);
        ApiMessage DeleteUser(Guid id); 
        event EventHandler<AuthenticationEventArgs> SuccessfulAuthenticationEvent;

        ApiMessage<IEnumerable<IRole>> GetAllRoles();
        ApiMessage SetUserRoles(string userId, Guid[] roleIds);
        ApiMessage CreateRole(string name, int rank);
        ApiMessage<IEnumerable<IUser>> GetUsersInRole(Guid roleId);
        ApiMessage<IRole> GetRole(Guid roleId);
    }
}