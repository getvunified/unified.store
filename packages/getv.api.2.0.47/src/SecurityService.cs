using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Foundation;

namespace Getv.Security
{
    public class SecurityService : ISecurityService
    {
        private readonly HttpClient client;
        public SecurityService()
        {
            client = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["GetvWebApiBaseUrl"])
                };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public ApiMessage<IUser> AuthenticateUser(string username, string password, string provider=null)
        {
            var myUser = new User
                {
                    Username = username, 
                    Password = password, 
                    ProviderName = provider
                };

            var response = client.PutAsJsonAsync("api/Auth", myUser).Result;

            if(!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IUser>();
            
            var user = response.Content.ReadAsAsync<User>().Result;
            var args = new AuthenticationEventArgs { User = user };
            OnSuccessfulAuthentication(args);
            
            return HttpStatusCode.OK.ToApiMessage<IUser>(user);
        }

        public ApiMessage<IUser> LoadUserByUsername(string username)
        {
            var request = new { Username = username};
            HttpResponseMessage response = client.PostAsJsonAsync("api/Users/GetByUsername", request).Result;


             if(!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IUser>();
            
            IUser user = response.Content.ReadAsAsync<User>().Result;
            
            return HttpStatusCode.OK.ToApiMessage(user);

        }

        public ApiMessage<IUser> LoadUserById(Guid id)
        {
            HttpResponseMessage response = client.GetAsync("api/Users/{0}".FormatWith(id)).Result;

            if (!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IUser>();

            IUser user = response.Content.ReadAsAsync<User>().Result;
            return HttpStatusCode.OK.ToApiMessage(user);

        }

        public ApiMessage<IEnumerable<IUser>> LoadUsersBy(string firstName, string lastName, string username, string emailAddress, long donorAccountNumber)
        {
            var myUser = new User { Username = username, EmailAddress = emailAddress, FirstName = firstName, LastName = lastName, DonorAccountNumber = donorAccountNumber };
            var response = client.PostAsJsonAsync("api/Users/GetUsers", myUser).Result;
            if (!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IEnumerable<IUser>>();
            var usersList = response.Content.ReadAsAsync<IEnumerable<User>>().Result;
            return HttpStatusCode.OK.ToApiMessage<IEnumerable<IUser>>(usersList);
        }

        public ApiMessage InitiatePasswordReset(string emailAddress, string confirmationUrl)
        {
            var request = new {Email = emailAddress, ConfirmationUrl=confirmationUrl};
            var response = client.PostAsJsonAsync("api/users/RequestResetPassword", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : HttpStatusCode.OK.ToApiMessage();  //Hide the error for security
        }

        public ApiMessage ConfirmPasswordReset(string newPassword, string token)
        {
            var request = new { NewPassword = newPassword, Token = token };
            var response = client.PostAsJsonAsync("api/users/ConfirmResetPassword", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage InitiateEmailAddressChange(string oldEmailAddress, string newEmailAddress, string confirmationUrl)
        {
            var request = new { OldEmail = oldEmailAddress, NewEmail = newEmailAddress, ConfirmationUrl = confirmationUrl };
            var response = client.PostAsJsonAsync("api/users/RequestEmailChange", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage ConfirmEmailAddressChange(string token, string confirmationUrl)
        {
            var request = new { Token = token, ConfirmationUrl = confirmationUrl };
            var response = client.PostAsJsonAsync("api/users/ConfirmEmailChange", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage RequestForgottenUsername(string emailAddress)
        {
            var request = new { Email = emailAddress };
            var response = client.PostAsJsonAsync("api/users/RequestUsername", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : HttpStatusCode.OK.ToApiMessage(); //Hide the error for security
        }
        
        
        public ApiMessage CreateUser(string username, string password, string emailAddress, string firstName, string lastName, bool emailSignUp)
        {
            IUser myUser = new User() { Username = username, Password = password, EmailAddress = emailAddress, FirstName = firstName, LastName = lastName, OptInNotificationEmails = emailSignUp};
            var response = client.PostAsJsonAsync("api/Users/CreateUser", myUser).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage SaveUser(Guid id, string username, string emailAddress, string firstName, string lastName, bool emailSignUp, long donorAccountNumber, string donorEnvironment)
        {
            IUser myUser = new User() { Id = id.ToString(), Username = username,EmailAddress = emailAddress, FirstName = firstName, LastName = lastName, OptInNotificationEmails = emailSignUp, DonorAccountNumber  = donorAccountNumber, DonorEnvironment = donorEnvironment};
            var response = client.PutAsJsonAsync("api/Users/{0}".FormatWith(id.ToString()), myUser).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage SaveUser(IUser user)
        {
            IUser myUser = new User() { Id = user.Id.ToString(CultureInfo.InvariantCulture), Username = user.Username, EmailAddress = user.EmailAddress, FirstName = user.FirstName, LastName = user.LastName, OptInNotificationEmails = user.EmailSignUp, DonorAccountNumber = user.DonorAccountNumber, DonorEnvironment = user.DonorEnvironment };
            var response = client.PutAsJsonAsync("api/Users/{0}".FormatWith(user.Id.ToString(CultureInfo.InvariantCulture)), myUser).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage UpdatePassword(string id, string currentPassword, string newPassword)
        {
            var request = new { Id = id, CurrentPassword = currentPassword, NewPassword = newPassword};
            var response = client.PostAsJsonAsync("api/Users/UpdatePassword", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage UpdatePasswordByAdmin(string adminId, string userId, string newPassword)
        {
            var request = new { AdminId = adminId, UserId = userId, NewPassword = newPassword };
            var response = client.PostAsJsonAsync("api/Users/UpdatePasswordByAdmin", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage DeleteUser(Guid id)
        {
            var response = client.DeleteAsync("api/Users/{0}".FormatWith(id.ToString())).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public event EventHandler<AuthenticationEventArgs> SuccessfulAuthenticationEvent;

        private void OnSuccessfulAuthentication(AuthenticationEventArgs args)
        {
            var handler = SuccessfulAuthenticationEvent;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public ApiMessage<IEnumerable<IRole>> GetAllRoles()
        {
            var response = client.PostAsJsonAsync("api/Roles/GetAll",new{}).Result;
            if (!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IEnumerable<IRole>>();
            var roles = response.Content.ReadAsAsync<IEnumerable<Role>>().Result;
            return HttpStatusCode.OK.ToApiMessage<IEnumerable<IRole>>(roles);
        }

        public ApiMessage SetUserRoles(string userId, Guid[] roleIds)
        {
            var request = new { UserId = userId, Roles = roleIds };
            var response = client.PostAsJsonAsync("api/Roles/SetUserRoles", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage CreateRole(string name, int rank)
        {
            var request = new { Name = name, Rank = rank };
            var response = client.PostAsJsonAsync("api/Roles/Create", request).Result;
            return !response.IsSuccessStatusCode ? response.StatusCode.ToApiMessage() : response.Content.ReadAsAsync<ApiMessage>().Result;
        }

        public ApiMessage<IEnumerable<IUser>> GetUsersInRole(Guid roleId)
        {
            var response = client.PostAsJsonAsync("api/Roles/GetUsersInRole", new {RoleId = roleId}).Result;
            if (!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IEnumerable<IUser>>();
            var users = response.Content.ReadAsAsync<IEnumerable<IUser>>().Result;
            return HttpStatusCode.OK.ToApiMessage(users);
        }

        public ApiMessage<IRole> GetRole(Guid roleId)
        {
            HttpResponseMessage response = client.GetAsync("api/Roles/{0}".FormatWith(roleId)).Result;

            if (!response.IsSuccessStatusCode)
                return response.StatusCode.ToApiMessage<IRole>();

            IRole role = response.Content.ReadAsAsync<Role>().Result;
            return HttpStatusCode.OK.ToApiMessage(role);

        }
    }


   

}