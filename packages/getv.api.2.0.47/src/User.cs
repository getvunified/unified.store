using System.Collections.Generic;
using System.Security.Claims;

namespace Getv.Security
{
    public class User : IUser
    {
        public string Id { get; set; }
        public string Username { get; set; }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public bool EmailSignUp { get; set; }
        public List<Claim> Claims { get; set; }
        public string[] SecurityRoles { get; set; }


        public string ProviderName { get; set; }
        public bool OptInNotificationEmails { get; set; }
        public long DonorAccountNumber { get; set; }
        public string DonorEnvironment { get; set; }

    }
}