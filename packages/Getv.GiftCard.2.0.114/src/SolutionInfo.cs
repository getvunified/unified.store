using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


[assembly: AssemblyCompany("Global Evangelism, Inc.")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDescription("Getv - Gift Cards")]



#if(DEBUG)
[assembly: AssemblyConfiguration("DEBUG")]
#endif
#if(RELEASE)
[assembly: AssemblyConfiguration( "RELEASE" )]
#endif


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyVersion("1.0.5.0")]
//[assembly: AssemblyInformationalVersion("1.0.5.0")]
//[assembly: AssemblyFileVersion( "1.0.5.1" )]