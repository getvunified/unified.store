﻿using System.Linq;
using System.Security.Cryptography;

namespace Getv.GiftCard.Infrastructure
{
    public interface IRandomStringGenerator
    {
        string GetString(string alphabet, int stringLength);
    }

    public class RandomStringGenerator : IRandomStringGenerator
    {
        readonly RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

        public string GetString(string alphabet, int stringLength)
        {
            var bytes = new byte[stringLength];
            rng.GetBytes(bytes);
            var alphabetLength = alphabet.Length;
            return new string(bytes.Select(x => alphabet[x % alphabetLength]).ToArray());
        }
    }
}