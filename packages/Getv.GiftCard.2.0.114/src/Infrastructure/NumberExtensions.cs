﻿using System;
using System.Collections.Generic;

namespace Getv.GiftCard.Infrastructure
{
    public static class NumberExtensions
    {
        public static void Times(this int times, Action<int> action)
        {
            for (var i = 0; i < times; i++) action(i);
        }

        public static IEnumerable<T> Select<T>(this int times, Func<int, T> func)
        {
            for (var i = 0; i < times; i++) yield return func(i);
        }

        public static IEnumerable<T> Get<T>(this int times, Func<int, T> func, Func<T, bool> valid)
        {
            var i = 0;
            do
            {
                var obj = func(i);
                if (!valid(obj)) continue;
                i++;
                yield return obj;
            } while (i != times);
        }
    }
}