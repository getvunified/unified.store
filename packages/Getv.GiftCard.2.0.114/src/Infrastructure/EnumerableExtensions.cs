using System.Collections.Generic;

namespace Getv.GiftCard.Infrastructure
{
    public static class EnumerableExtensions
    {
        public static SortedSet<T> ToSortedSet<T>(this IEnumerable<T> list)
        {
            return new SortedSet<T>(list);
        }
    }
}