﻿using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Reporting
{
    public class GetLast10Transactions : IQuery<GiftCardTransaction[]>
    {
        public string Number { get; set; }
        public string Pin { get; set; }
    }

    public class GetLast10TransactionsHandler : IQueryHandler<GetLast10Transactions, GiftCardTransaction[]>
    {
        readonly IDataSession session;

        public GetLast10TransactionsHandler(IDataSession session)
        {
            this.session = session;
        }

        public GiftCardTransaction[] Execute(GetLast10Transactions query)
        {
            var info = session.AsQueryable<GiftCardInfo>()
                .SingleOrDefault(x => x.Number == query.Number && x.Pin == query.Pin);

            if (info == null) return new GiftCardTransaction[]{};

            return session.AsQueryable<GiftCardTransaction>()
                .Where(x => x.GiftCardId == info.GiftCardId)
                .OrderByDescending(x => x.DateTime)
                .Take(10)
                .ToArray();
        }
    }
}