﻿using System;
using System.Linq;
using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Reporting.Projections
{
    public class GiftCardInfoProjection : 
        IEventProcessor<NewGiftCardsAddedToBatch>,
        IEventProcessor<GiftCardLoaded>,
        IEventProcessor<GiftCardRedeemed>,
        IEventProcessor<GiftCardRefunded>,
        IEventProcessor<GiftCardDeactivated>,
        IEventProcessor<GiftCardTransactionVoided>
    {
        readonly IDataSession session;

        public GiftCardInfoProjection(IDataSession session)
        {
            this.session = session;
        }

        public void Process(NewGiftCardsAddedToBatch e)
        {
            e.Added.Select(x => new GiftCardInfo
                {
                    BatchId = e.BatchId,
                    GiftCardId = x.GiftCardId,
                    Number = x.Number,
                    Pin = x.Pin,
                    Balance = x.Balance
                }).ForEach(x => session.Store(x));

            session.SaveChanges();
        }

        public void Process(GiftCardLoaded e)
        {
            UpdateBalance(e.GiftCardId, e.Balance);
        }

        void UpdateBalance(Guid id, decimal balance)
        {
            var giftCardInfo = session.AsQueryable<GiftCardInfo>().SingleOrDefault(x => x.GiftCardId == id);

            if (giftCardInfo == null) return;

            giftCardInfo.Balance = balance;

            session.StoreAndSaveChanges(giftCardInfo);
        }
        
        public void Process(GiftCardRedeemed e)
        {
            UpdateBalance(e.GiftCardId, e.Balance);
        }

        public void Process(GiftCardRefunded e)
        {
            UpdateBalance(e.GiftCardId, e.Balance);
        }


        public void Process(GiftCardDeactivated e)
        {
            UpdateBalance(e.GiftCardId, e.Balance);
        }

        public void Process(GiftCardTransactionVoided e)
        {
            UpdateBalance(e.GiftCardId, e.Balance);
        }
    }
}