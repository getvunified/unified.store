﻿using System.Linq;
using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Reporting.Projections
{
    public class GiftCardTransactionProjection : 
        IEventProcessor<GiftCardLoaded>,
        IEventProcessor<GiftCardRedeemed>,
        IEventProcessor<GiftCardRefunded>,
        IEventProcessor<GiftCardDeactivated>,
        IEventProcessor<GiftCardTransactionVoided>
    {
        readonly IDataSession session;
       
        public GiftCardTransactionProjection(IDataSession session)
        {
            this.session = session;
        }

        void StoreTransaction(GiftCardTransactionBase e, string type)
        {
            var transaction = session.AsQueryable<GiftCardTransaction>()
                .SingleOrDefault(x => x.TransactionId == e.TransactionId) ?? new GiftCardTransaction();

            transaction.GiftCardId = e.GiftCardId;
            transaction.Type = type;
            transaction.Amount = e.Amount;
            transaction.DateTime = e.On;
            transaction.TransactionId = e.TransactionId;
            transaction.Balance = e.Balance;
            //transaction.UserId = e.UserId; //Lookup user name

            session.StoreAndSaveChanges(transaction);
        }

        public void Process(GiftCardLoaded e)
        {
            StoreTransaction(e, "Loaded");
        }

        public void Process(GiftCardRedeemed e)
        {
            StoreTransaction(e, "Redeemed");
        }

        public void Process(GiftCardRefunded e)
        {
            StoreTransaction(e, "Refunded");
        }

        public void Process(GiftCardDeactivated e)
        {
            StoreTransaction(e, "Deactivated");
        }

        public void Process(GiftCardTransactionVoided e)
        {
            StoreTransaction(e, "Voided");
        }
    }

}