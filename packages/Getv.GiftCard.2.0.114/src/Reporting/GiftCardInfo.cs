﻿using System;

namespace Getv.GiftCard.Reporting
{
    public class GiftCardInfo
    {
        public Guid GiftCardId { get; set; }
        public Guid BatchId { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public decimal Balance { get; set; }
    }
}