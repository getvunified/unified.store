﻿using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Reporting
{
    public class GetBalance : IQuery<GiftCardBalance>
    {
        public string Number { get; set; }
    }

    public class GetBalanceHandler : IQueryHandler<GetBalance, GiftCardBalance>
    {
        readonly IDataSession session;

        public GetBalanceHandler(IDataSession session)
        {
            this.session = session;
        }

        public GiftCardBalance Execute(GetBalance query)
        {
            var info = session.AsQueryable<GiftCardInfo>().SingleOrDefault(x => x.Number == query.Number);
            var giftCardBalance = new GiftCardBalance
            {
                Balance = info == null ? 0 : info.Balance
            };

            return giftCardBalance;
        }
    }


}
