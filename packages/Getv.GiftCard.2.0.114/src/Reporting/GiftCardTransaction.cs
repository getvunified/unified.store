﻿using System;

namespace Getv.GiftCard.Reporting
{
    public class GiftCardTransaction
    {
        public Guid TransactionId { get; set; }
        public Guid GiftCardId { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public decimal Balance { get; set; }
    }
}