﻿using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Reporting
{
    public class GetLast10TransactionsById : GetLast10Transactions{}
    public class GetLast10TransactionsByIdHandler : IQueryHandler<GetLast10TransactionsById, GiftCardTransaction[]>
    {
        readonly IDataSession session;

        public GetLast10TransactionsByIdHandler(IDataSession session)
        {
            this.session = session;
        }

        public GiftCardTransaction[] Execute(GetLast10TransactionsById query)
        {
            var info = session.AsQueryable<GiftCardInfo>()
            .SingleOrDefault(x => x.Number == query.Number && x.Pin == query.Pin);

            if (info == null) return new GiftCardTransaction[] { };

            return session.AsQueryable<GiftCardTransaction>()
                .Where(x => x.GiftCardId == info.GiftCardId)
                .OrderByDescending(x => x.TransactionId)
                .Take(10)
                .ToArray();
        }
    }
}