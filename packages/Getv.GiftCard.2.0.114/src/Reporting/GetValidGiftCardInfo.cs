﻿using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Reporting
{
    public class GetValidGiftCardInfo : IQuery<GiftCardInfo>
    {
        public string Number { get; set; }
        public string Pin { get; set; }
    }

    public class GetValidGiftCardInfoHandler : IQueryHandler<GetValidGiftCardInfo, GiftCardInfo>
    {
        readonly IDataSession session;

        public GetValidGiftCardInfoHandler(IDataSession session)
        {
            this.session = session;
        }

        public GiftCardInfo Execute(GetValidGiftCardInfo query)
        {
            return session.AsQueryable<GiftCardInfo>().SingleOrDefault(x => x.Number == query.Number && x.Pin == query.Pin);
        }
    }

}