﻿using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Reporting
{
    public class GetAll : IQuery<GiftCardInfo[]>
    {
         
    }

    public class GetAllHandler : IQueryHandler<GetAll, GiftCardInfo[]>
    {
        readonly IDataSession session;

        public GetAllHandler(IDataSession session)
        {
            this.session = session;
        }

        public GiftCardInfo[] Execute(GetAll query)
        {
            return session.AsQueryable<GiftCardInfo>().ToArray();
        }
    }
}