﻿using System;
using System.Collections.Generic;
using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Core
{
    public class Batch : AggregateRoot
    {
        Batch(){ }

        public static Batch Create(string userId, string description, string[] tags)
        {

            if(description.IsNullOrWhiteSpace())
                throw new DomainException(Resources.Batch_Create_Gift_card_batch_must_include_a_description);

            if (tags.Any(string.IsNullOrWhiteSpace))
                throw new DomainException(Resources.GiftCard_Create_Gift_card_tag_not_provided);

            var batch = new Batch();
            batch.Raise(new BatchCreated
            {
                BatchId = SequentialUuid.NewUuid(),
                Tags = tags,
                On = SystemTime.Now(),
                Description = description,
                UserId = userId
            });

            return batch;
        }

        void Apply(BatchCreated evt)
        {
            id = evt.BatchId;
        }

        public IEnumerable<GiftCard> AddNewGiftCards(GiftCardNumber[] giftCardNumbers, decimal startingAmount, string userId)
        {
            var giftCards = giftCardNumbers.Select(x => GiftCard.Create(x.Number, x.Pin, startingAmount, userId)).ToArray();

            Raise(new NewGiftCardsAddedToBatch
                {
                    BatchId = id,
                    Added = giftCards.SelectMany(x => x.GetUncommittedEvents())
                                    .OfType<GiftCardCreated>()
                                    .Select(x => new GiftCardAdded
                                            {
                                                GiftCardId = x.GiftCardId, 
                                                Number = x.Number, 
                                                Pin = x.Pin,
                                                Balance = startingAmount,
                                                UserId = userId
                                            }).ToArray()
                });

            return giftCards;
        }
    }

    public class BatchCreated
    {
        public Guid BatchId { get; set; }
        public string[] Tags { get; set; }
        public DateTimeOffset On { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }

    public class GiftCardNumber
    {
        public string Number { get; set; }
        public string Pin { get; set; }
    }

    public class NewGiftCardsAddedToBatch
    {
        public Guid BatchId { get; set; }
        public GiftCardAdded[] Added { get; set; }
    }

    public class GiftCardAdded
    {
        public Guid GiftCardId { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public decimal Balance { get; set; }
        public string UserId { get; set; }
    }
}