﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Getv.EventSourcing;
using Getv.GiftCard.Reporting;

namespace Getv.GiftCard.Core
{
    public interface IGiftCardExecutor
    {
        void Execute(string cardNumber, Action<GiftCard> action);
        void Execute(string cardNumber, string cardPin, Action<GiftCard> action);
    }

    public class GiftCardExecutor : IGiftCardExecutor
    {
        readonly IUnitOfWork unitOfWork;
        readonly IDataSession session; //Should be the reporting data store

        public GiftCardExecutor(
            IUnitOfWork unitOfWork,
            IDataSession session)
        {
            this.unitOfWork = unitOfWork;
            this.session = session;
        }

        public void Execute(string cardNumber, Action<GiftCard> action)
        {
            Execute(x => x.Number == cardNumber, action);
        }

        public void Execute(string cardNumber, string cardPin, Action<GiftCard> action)
        {
            Execute(x => x.Number == cardNumber && x.Pin == cardPin, action);
        }
        
        void Execute(Expression<Func<GiftCardInfo, bool>> whereFunc, Action<GiftCard> action)
        {
            var info = session.AsQueryable<GiftCardInfo>().SingleOrDefault(whereFunc);

            if (info == null) throw new DomainException(Resources.LoadGiftCardExecutor_Execute_Invalid_gift_card);

            unitOfWork.ExecuteById(info.GiftCardId, action);
        }
    }
}