﻿using System;
using System.Collections.Generic;
using System.Linq;
using Getv.EventSourcing;

namespace Getv.GiftCard.Core
{
    //http://www.ded.co.uk/magnetic-stripe-card-standards/
    public class GiftCard : AggregateRoot
    {
        decimal balance;
        readonly List<GiftCardTransactionBase> transactions = new List<GiftCardTransactionBase>();

        GiftCard()
        {

        }

        /// <summary>
        /// Convenience property ONLY. DO NOT add setter
        /// </summary>
        public decimal Balance
        {
            get { return balance; }
        }

        public static GiftCard Create(string number, string pin, string userId)
        {
            const long numberLength = 19;
            const int pinLength = 3;

            ValidateUser(userId);

            if (number.Length != numberLength)
                throw new DomainException(Resources.GiftCard_Create_Gift_card_number_out_of_range);

            if (pin.Length != pinLength)
                throw new DomainException(Resources.GiftCard_Create_Gift_card_pin_out_of_range);

            var card = new GiftCard();
            card.Raise(new GiftCardCreated
            {
                GiftCardId = SequentialUuid.NewUuid(),
                Number = number,
                Pin = pin,
                On = SystemTime.Now(),
                UserId = userId,
            });

            return card;
        }

        public static GiftCard Create(string number, string pin, decimal startingBalance, string userId)
        {
            var card = Create(number, pin, userId);
            if(startingBalance > 0)
                card.Load(startingBalance, userId, "Initial");
            return card;
        }

        void Apply(GiftCardCreated evt)
        {
            id = evt.GiftCardId;
        }

        public void Load(decimal amount, string userId, string description)
        {
            ValidateAmount(amount);
            ValidateUser(userId);

            if(string.IsNullOrWhiteSpace(description))
                throw new DomainException(Resources.GiftCard_Load_Load_description_not_provided);

            var evt = Build<GiftCardLoaded>(amount, userId);
            evt.Description = description;
            Raise(evt);
        }

        void Apply(GiftCardLoaded evt)
        {
            balance = evt.Balance;
            transactions.Add(evt);
        }

        static void ValidateAmount(decimal amount)
        {
            if (amount < 0)
                throw new DomainException(Resources.GiftCard_negative_amount_not_allowed);
        }

        static void ValidateUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new DomainException(Resources.GiftCard_ValidateUser_User_must_be_provided);
        }

        public void Redeem(decimal amount, string userId, Guid transactionId)
        {
            ValidateAmount(amount);
            ValidateUser(userId);
            ValidateTransactionIsUnique(transactionId);

            if (amount > balance)
                throw new DomainException(Resources.GiftCard_Redeem_Redemption_amount_exceeded_the_card_balance);

            Raise(Build<GiftCardRedeemed>(-amount, userId, transactionId));
        }

        void ValidateTransactionIsUnique(Guid transactionId)
        {
            if(transactions.Any(x => x.TransactionId == transactionId))
                throw new DomainException(Resources.Transaction_id_already_exists);
        }

        void Apply(GiftCardRedeemed evt)
        {
            balance = evt.Balance;
            transactions.Add(evt);
        }

        public void Refund(decimal amount, string reason, string userId)
        {
            ValidateAmount(amount);
            ValidateUser(userId);

            if (reason.IsNullOrWhiteSpace())
                throw new DomainException(Resources.GiftCard_Refund_A_reason_must_be_provided_when_refunding_an_amount_to_a_card);

            var evt = Build<GiftCardRefunded>(amount, userId);
            evt.Reason = reason;
            Raise(evt);
        }

        void Apply(GiftCardRefunded evt)
        {
            balance = evt.Balance;
            transactions.Add(evt);
        }

        public void Deactivate(string reason, string userId)
        {
            ValidateUser(userId);

            if(reason.IsNullOrWhiteSpace())
                throw new DomainException(Resources.GiftCard_Deactivate_A_reason_must_be_provided_when_deactivating_a_card);

            var evt = Build<GiftCardDeactivated>(-balance, userId);
            evt.Reason = reason;

            Raise(evt);
        }

        void Apply(GiftCardDeactivated evt)
        {
            balance = evt.Balance;
            transactions.Add(evt);
        }

        public void Void(Guid transactionId, string reason, string userId)
        {
            ValidateUser(userId);

            if (reason.IsNullOrWhiteSpace())
                throw new DomainException(Resources.GiftCard_Void_A_reason_must_be_provided_when_voiding_a_transaction);

            var transaction = transactions.SingleOrDefault(x => x.TransactionId == transactionId);
            if(transaction == null)
                throw new DomainException(Resources.GiftCard_Void_Invalid_Gift_Card_transaction_provided);

            if(transactions.OfType<GiftCardTransactionVoided>().Any(x => x.VoidedTransactionId == transactionId))
                throw new DomainException(Resources.GiftCard_Void_Gift_card_transaction_has_already_been_voided);

            var evt = Build<GiftCardTransactionVoided>(transaction.Amount * -1, userId);
            evt.Reason = reason;
            evt.VoidedTransactionId = transactionId;
            Raise(evt);
        }

        void Apply(GiftCardTransactionVoided evt)
        {
            balance = evt.Balance;
            transactions.Add(evt);
        }

        T Build<T>(decimal amount, string userId) where T : GiftCardTransactionBase, new()
        {
            return Build<T>(amount, userId, SequentialUuid.NewUuid());
        }

        T Build<T>(decimal amount, string userId, Guid transactionId) where T : GiftCardTransactionBase, new()
        {
            return new T
            {
                GiftCardId = id,
                Amount = amount,
                Balance = balance + amount,
                On = SystemTime.Now(),
                TransactionId = transactionId,
                UserId = userId
            };
        }

    }

}