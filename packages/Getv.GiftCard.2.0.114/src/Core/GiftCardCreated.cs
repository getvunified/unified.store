using System;

namespace Getv.GiftCard.Core
{
    public class GiftCardCreated
    {
        public Guid GiftCardId { get; set; }
        public DateTimeOffset On { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public string UserId { get; set; }
    }

    public abstract class GiftCardTransactionBase
    {
        public Guid TransactionId { get; set; }
        public Guid GiftCardId { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
        public DateTimeOffset On { get; set; }
        public string UserId { get; set; }
    }

    public class GiftCardLoaded : GiftCardTransactionBase{
        public string Description { get; set; }
    }

    public class GiftCardRedeemed : GiftCardTransactionBase{}

    public class GiftCardRefunded : GiftCardTransactionBase
    {
        public string Reason { get; set; }
    }

    public class GiftCardDeactivated : GiftCardTransactionBase
    {
        public string Reason { get; set; }
    }

    public class  GiftCardTransactionVoided : GiftCardTransactionBase
    {
        public Guid VoidedTransactionId { get; set; }
        public string Reason { get; set; }
    }
}