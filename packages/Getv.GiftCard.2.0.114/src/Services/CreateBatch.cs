using System.Collections.Generic;
using System.Linq;
using Getv.EventSourcing;
using Getv.GiftCard.Core;
using Getv.GiftCard.Infrastructure;
using Getv.GiftCard.Reporting;
using IRandomStringGenerator = Getv.GiftCard.Infrastructure.IRandomStringGenerator;

namespace Getv.GiftCard.Services
{
    public class CreateBatch
    {
        public int TotalCards { get; set; }
        public string[] Tags { get; set; }
        public decimal StartingBalance { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }

        public GiftCardNumber[] GiftCardNumbers { get; set; }
    }

    public class CreateBatchExecutor : ICommandExecutor<CreateBatch>
    {
        readonly IUnitOfWork unitOfWork;
        readonly IRandomStringGenerator randomStringGenerator;
        readonly IDataSession session;

        public CreateBatchExecutor(
            IUnitOfWork unitOfWork,
            IRandomStringGenerator randomStringGenerator,
            IDataSession session)
        {
            this.unitOfWork = unitOfWork;
            this.randomStringGenerator = randomStringGenerator;
            this.session = session;
        }

        public void Execute(CreateBatch c)
        {
            var batch = Batch.Create(c.UserId, c.Description, c.Tags);

            var newUniqueNumbers = (c.GiftCardNumbers == null || c.GiftCardNumbers.Length == 0) ?
                                                    CreateUniqueGiftCardNumbers(c.TotalCards) :
                                                    AddGiftCardNumbers(c.GiftCardNumbers);

            var giftCards = batch.AddNewGiftCards(newUniqueNumbers, c.StartingBalance, c.UserId);
            unitOfWork.Register(batch);
            giftCards.ForEach(x => unitOfWork.Register(x));
        }

        GiftCardNumber[] AddGiftCardNumbers(IEnumerable<GiftCardNumber> giftCardNumbers)
        {
            var existingNumbers = session.AsQueryable<GiftCardInfo>()
                .Select(x => x.Number)
                .ToSortedSet();
            
            var numbersAdded = giftCardNumbers.Where(x => existingNumbers.Add(x.Number)).ToArray();

            return numbersAdded;
        }

        GiftCardNumber[] CreateUniqueGiftCardNumbers(int totalCards)
        {
            //NOTE GiftCardNumbers is NOT event sourced
            var existingNumbers = session.AsQueryable<GiftCardInfo>()
                .Select(x => x.Number)
                .ToSortedSet();

            var newNumbers = totalCards.Get(x => CreateGiftCardNumber(), existingNumbers.Add)
                .Select(x => new GiftCardNumber{Number = x, Pin = CreateSecurityPin()})
                .ToArray();

            return newNumbers;
        }

        string CreateGiftCardNumber()
        {
            return randomStringGenerator.GetString("123456789", 1) + randomStringGenerator.GetString("0123456789", 18);
        }

        string CreateSecurityPin()
        {
            return randomStringGenerator.GetString("0123456789", 3);
        }
    }
}