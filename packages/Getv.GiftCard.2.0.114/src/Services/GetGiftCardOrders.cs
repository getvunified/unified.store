﻿using Getv.EventSourcing;

namespace Getv.GiftCard.Services
{
    public class GetGiftCardOrders : IQuery<GiftCardOrder[]>
    {
        public string PackingSlipId { get; set; }
    }

    public class GetGiftCardOrdersHandler : IQueryHandler<GetGiftCardOrders, GiftCardOrder[]>
    {
        readonly ICrmService crmService;

        public GetGiftCardOrdersHandler(ICrmService crmService)
        {
            this.crmService = crmService;
        }

        public GiftCardOrder[] Execute(GetGiftCardOrders query)
        {
            return crmService.GetUncompletedGiftCardOrders(query.PackingSlipId);
        }
    }
}