﻿using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class Deactivate
    {
        public string Number { get; set; }
        public string Reason { get; set; }
        public string UserId { get; set; }
    }

    public class DeactivateExecutor : ICommandExecutor<Deactivate>
    {
        readonly IGiftCardExecutor giftCardExecutor;

        public DeactivateExecutor(IGiftCardExecutor giftCardExecutor)
        {
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(Deactivate c)
        {
            giftCardExecutor.Execute(c.Number, x => x.Deactivate(c.Reason, c.UserId));
        }
    }
}