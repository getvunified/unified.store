﻿namespace Getv.GiftCard.Services
{
    public interface ICrmService
    {
        void CompleteGiftCardLoad(string crmId, string giftCardNumber);
        GiftCardOrder[] GetUncompletedGiftCardOrders(string packingSlipId);
    }

    public class GiftCardOrder
    {
        public string CrmId { get; set; }
        public decimal Amount { get; set; }
        public string PackingSlipId { get; set; }
    }

}