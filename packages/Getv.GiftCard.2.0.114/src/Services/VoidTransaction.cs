using System;
using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class VoidTransaction
    {
        public string Number { get; set; }
        public Guid TransactionId { get; set; }
        public string Reason { get; set; }
        public string UserId { get; set; }
    }

    public class VoidTransactionExecutor : ICommandExecutor<VoidTransaction>
    {
        readonly IGiftCardExecutor giftCardExecutor;

        public VoidTransactionExecutor(IGiftCardExecutor giftCardExecutor)
        {
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(VoidTransaction c)
        {
            giftCardExecutor.Execute(c.Number, x => x.Void(c.TransactionId, c.Reason, c.UserId));
        }
    }
}