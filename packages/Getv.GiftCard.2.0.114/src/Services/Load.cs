using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class Load
    {
        public string Number { get; set; }
        public decimal Amount { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
    }

    public class LoadExecutor : ICommandExecutor<Load>
    {
        readonly IGiftCardExecutor giftCardExecutor;

        public LoadExecutor(IGiftCardExecutor giftCardExecutor)
        {
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(Load c)
        {
            giftCardExecutor.Execute(c.Number, x => x.Load(c.Amount, c.UserId, c.Description));
        }
    }
}