using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class Refund
    {
        public string Number { get; set; }
        public string Pin { get; set; }
        public decimal Amount { get; set; }
        public string Reason { get; set; }
        public string UserId { get; set; }
    }

    public class RefundExecutor : ICommandExecutor<Refund>
    {
        readonly IGiftCardExecutor giftCardExecutor;

        public RefundExecutor(IGiftCardExecutor giftCardExecutor)
        {
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(Refund c)
        {
            giftCardExecutor.Execute(c.Number, c.Pin, x => x.Refund(c.Amount, c.Reason, c.UserId));
        }
    }
}