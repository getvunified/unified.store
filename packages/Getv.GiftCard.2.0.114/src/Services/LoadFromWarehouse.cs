﻿using System.Linq;
using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class LoadFromWarehouse
    {
        public string Number { get; set; }
        public string PackingSlipId { get; set; }
        public string CrmId { get; set; }
        public string UserId { get; set; }
    }

    public class LoadFromWarehouseExecutor : ICommandExecutor<LoadFromWarehouse>
    {
        readonly ICrmService crmService;
        readonly IGiftCardExecutor giftCardExecutor;

        public LoadFromWarehouseExecutor(ICrmService crmService, 
            IGiftCardExecutor giftCardExecutor)
        {
            this.crmService = crmService;
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(LoadFromWarehouse c)
        {
            var order = crmService.GetUncompletedGiftCardOrders(c.PackingSlipId).FirstOrDefault(x => x.CrmId == c.CrmId);
            if (order == null)
                throw new DomainException("Invalid gift card order");

            giftCardExecutor.Execute(c.Number, x => x.Load(order.Amount, c.UserId, "Warehouse"));
            crmService.CompleteGiftCardLoad(c.CrmId, c.Number);
        }
    }
    
}