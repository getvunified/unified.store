using System;
using Getv.EventSourcing;
using Getv.GiftCard.Core;

namespace Getv.GiftCard.Services
{
    public class Redeem
    {
        public string Number { get; set; }
        public string Pin { get; set; }
        public decimal Amount { get; set; }
        public string UserId { get; set; }
        public Guid TransactionId { get; set; }
    }

    public class RedeemExecutor : ICommandExecutor<Redeem>
    {
        readonly IGiftCardExecutor giftCardExecutor;

        public RedeemExecutor(IGiftCardExecutor giftCardExecutor)
        {
            this.giftCardExecutor = giftCardExecutor;
        }

        public void Execute(Redeem c)
        {
            giftCardExecutor.Execute(c.Number, c.Pin, x => x.Redeem(c.Amount, c.UserId, c.TransactionId));
        }
    }
}