﻿/*
 * Author: Alex Payares
 * Date Created: June 26th / 2013
 */
 
DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME
DECLARE @dfd DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)
DECLARE @allowOnSaleItems BIT


DECLARE @groupId UNIQUEIDENTIFIER
DECLARE @groupAnd SMALLINT
DECLARE @productAnd SMALLINT
DECLARE @isFreeItem SMALLINT


SET @startDate = '2013-10-25 00:00:00:00'
SET @endDate = '2099-10-25 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @isFreeShipping = 1
SET @isFixedAmount = 0
SET @isAutoApply = 1
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 1
SET @isActive = 1

SET @applicableCompanies = 1


DECLARE @quantity SMALLINT
DECLARE @productWarehouse SMALLINT
DECLARE @appliesOnlyToItemsMatchingCriteria BIT



SET @quantity = 1
SET @productWarehouse = 10


DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

SET @appliesOnlyToItemsMatchingCriteria = 0

SET @allowOnSaleItems = 1;

SET @idInit = 'ce0999b3-48d9-4e5f-b620-784d5380826e'
SET @batchId = NEWID()


SET @amountOff = 0.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 0

-- SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @codeName = '83baeaac-9cab-4380-85a9-fb861d78d81c'
SET @shortDescription = 'Purchase 36 paperback copies and receive over 20% off the regular price and free shipping.'
SET @longDescription = @shortDescription


SET @groupId = NEWID()

SET @groupAnd = 0
SET @productAnd = 1
SET @isFreeItem = 0

SET @quantity = 1
SET @productWarehouse = 10


IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[LongDescription]
			   ,[StartDate]
			   ,[EndDate]
			   ,[IsFixedAmount]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   @shortDescription,
			   @longDescription,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)


	INSERT INTO [dbo].[DiscountCodeRequirements]

				([Id]
				,[DiscountCodeId]
				,[StartDate]
				,[EndDate]
			   ,[IsFixedAmount]
			   ,[FreeShippingType]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[Quantity]
			   ,AppliesOnlyToItemsMatchingCriteria)
			   VALUES
			   (NEWID(),
			   @idInit,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @freeShippingType,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @quantity,
			   @appliesOnlyToItemsMatchingCriteria)

END

INSERT INTO [dbo].[DiscountCodeItems]
(
	   [Id]
      ,[CodeId]
      ,[GroupId]
      ,[GroupAnd]
      ,[ProductAnd]
      ,[ProductCode]
      ,[DiscountCode]
      ,[IsFreeItem]
      ,[StartDate]
      ,[EndDate]
      ,[IsFixedAmount]
      ,[FreeShipping]
      ,[AmountOff]
      ,[MinPurchaseAmount]
      ,[MaxAmountOff]
      ,[ProductWarehouse]
      ,[Quantity])
	  VALUES
	  (NEWID(),
	  @idInit,
	  @groupId,
	  @groupAnd,
	  @productAnd,
	  'K574',
	  @codeName,
	  @isFreeItem,
	  @startDate,
	  @endDate,
	  @isFixedAmount,
	  @isFreeShipping,
	  @amountOff,
	  @minPurchaseAmount,
	  @maxAmountOff,
	  @productWarehouse,
	  @quantity)
	  
GO


