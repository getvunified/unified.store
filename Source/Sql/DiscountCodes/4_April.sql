﻿DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME
DECLARE @dfd DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)


SET @idInit = NEWID()
SET @batchId = NEWID()

set @dfd = '2013-02-28 00:00:00'
SET @startDate = '2013-04-01 00:00:00:00'
SET @endDate = '2013-04-30 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @amountOff = 5.0
SET @maxAmountOff = 5.0
SET @minPurchaseAmount = 25.0

SET @isFreeShipping = 0
SET @isFixedAmount = 1
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 0
SET @isActive = 1

SET @applicableCompanies = 7

SET @codeName = 'APRILMP3'
SET @shortDescription = 'Free MP3 Download with the purchase of $25.00 or more'
SET @longDescription = 'Free MP3 Download with the purchase of $25.00 or more'




INSERT INTO [dbo].[DiscountCode]
           ([Id]
           ,[CodeName]
           ,[ShortDescription]
           ,[LongDescription]
           ,[StartDate]
           ,[EndDate]
           ,[IsFixedAmount]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive]
		   ,[CreationDate]
		   ,[SendOutDate]
		   ,[UsedDate]
		   ,[BatchId])
		VALUES
           (@idInit,
		   @codeName,
           @shortDescription,
           @longDescription,
           @startDate,
           @endDate,
           @isFixedAmount,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive,
		   @createdDate,
		   @sendOutDate,
		   @usedDate,
		   @batchId)





DECLARE @groupId UNIQUEIDENTIFIER
DECLARE @groupAnd SMALLINT
DECLARE @productAnd SMALLINT
DECLARE @isFreeItem SMALLINT

DECLARE @quantity SMALLINT
DECLARE @productWarehouse SMALLINT

SET @groupId = NEWID()

SET @groupAnd = 0
SET @productAnd = 1
SET @isFreeItem = 0

SET @quantity = 1
SET @productWarehouse = 10


DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

INSERT INTO [dbo].[DiscountCodeRequirements]

			([Id]
			,[DiscountCodeId]
			,[StartDate]
			,[EndDate]
           ,[IsFixedAmount]
		   ,[FreeShippingType]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive]
		   ,[Quantity])
		   VALUES
		   (NEWID(),
		   @idInit,
		   @startDate,
           @endDate,
           @isFixedAmount,
		   @freeShippingType,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive,
		   @quantity)




DECLARE @criteriaId UNIQUEIDENTIFIER

DECLARE @valueDataType NVARCHAR(10)
DECLARE @criteriaClass SMALLINT
DECLARE @mandatory BIT
DECLARE @optional BIT


SET @criteriaId = NEWID()


SET @valueDataType = 'string'
SET @criteriaClass = 1 -- StockItem

SET @mandatory = 1
SET @optional = 0



INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
	([Id]
	,[DiscountCodeId]
	,[Criteria]
	,[Value]
	,[ValueDataType]
	,[AssociatedDiscountCode]
	,[ApplicableClass]
	,[IsMandatory])
	VALUES
	(@criteriaId,
	@idInit,
	'MediaType',
	'MP3 Download',
	@valueDataType,
	@codeName,
	@criteriaClass,
	@mandatory)
	  
GO