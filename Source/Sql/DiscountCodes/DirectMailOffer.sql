

DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER
DECLARE @requirementsId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)

DECLARE @quantity SMALLINT

SET @idInit = NEWID()
SET @batchId = NEWID()
SET @requirementsId = NEWID()

SET @startDate = '2014-08-01 00:00:00:00'
SET @endDate = '2014-09-14 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @amountOff = 0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 0.0

SET @isFreeShipping = 0
SET @isFixedAmount = 1
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 1
SET @isActive = 1

SET @applicableCompanies = 3

SET @quantity = 1

SET @codeName = 'HEALING14'
SET @shortDescription = 'free �Power to Heal� book With Purchase of �The Power to Heal� DVD series'
SET @longDescription = 'free �Power to Heal� book With Purchase �The Power to Heal� DVD series'


INSERT INTO [dbo].[DiscountCode]
           ([Id]
           ,[CodeName]
           ,[ShortDescription]
           ,[LongDescription]
		   ,[CreationDate]
		   ,[SendOutDate]
		   ,[UsedDate]
		   ,[BatchId])
		VALUES
           (@idInit,
		   @codeName,
           @shortDescription,
           @longDescription,
		   @createdDate,
		   @sendOutDate,
		   @usedDate,
		   @batchId)

DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

INSERT INTO [dbo].[DiscountCodeRequirements]

			([Id]
			,[DiscountCodeId]
			,[StartDate]
			,[EndDate]
           ,[IsFixedAmount]
		   ,[FreeShippingType]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive])
		   VALUES
		   (@requirementsId,
		   @idInit,
		   @startDate,
           @endDate,
           @isFixedAmount,
		   @freeShippingType,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive)




DECLARE @groupId UNIQUEIDENTIFIER
DECLARE @groupAnd SMALLINT
DECLARE @productAnd SMALLINT
DECLARE @isFreeItem SMALLINT

DECLARE @productWarehouse SMALLINT

SET @groupId = NEWID()

SET @groupAnd = 0
SET @productAnd = 0
SET @isFreeItem = 1

SET @quantity = 1
SET @productWarehouse = 10

INSERT INTO [dbo].[DiscountCodeItems]
(
	   [Id]
      ,[CodeId]
      ,[GroupId]
      ,[GroupAnd]
      ,[ProductAnd]
      ,[ProductCode]
      ,[DiscountCode]
      ,[IsFreeItem]
      ,[StartDate]
      ,[EndDate]
      ,[IsFixedAmount]
      ,[FreeShipping]
      ,[AmountOff]
      ,[MinPurchaseAmount]
      ,[MaxAmountOff]
      ,[ProductWarehouse]
      ,[Quantity])
	  VALUES
	  (NEWID(),
	  @idInit,
	  @groupId,
	  @groupAnd,
	  @productAnd,
	  'B19',
	  @codeName,
	  @isFreeItem,
	  @startDate,
	  @endDate,
	  @isFixedAmount,
	  @isFreeShipping,
	  @amountOff,
	  @minPurchaseAmount,
	  @maxAmountOff,
	  @productWarehouse,
	  @quantity),
	  (NEWID(),
	  @idInit,
	  @groupId,
	  @groupAnd,
	  @productAnd,
	  'S1426D',
	  @codeName,
	  0, --@isFreeItem,
	  @startDate,
	  @endDate,
	  @isFixedAmount,
	  @isFreeShipping,
	  @amountOff,
	  @minPurchaseAmount,
	  @maxAmountOff,
	  @productWarehouse,
	  @quantity)
	  



DECLARE @criteriaId UNIQUEIDENTIFIER

DECLARE @valueDataType NVARCHAR(10)
DECLARE @StockItem_CriteriaClass SMALLINT
DECLARE @Product_criteriaClass SMALLINT
DECLARE @mandatory BIT
DECLARE @optional BIT

SET @StockItem_CriteriaClass = 1 -- StockItem
SET @Product_criteriaClass = 2 -- Product
SET @mandatory = 1
SET @optional = 0


SET @valueDataType = 'String'


INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
	([Id]
	,[DiscountCodeId]
	,[Criteria]
	,[Value]
	,[ValueDataType]
	,[AssociatedDiscountCode]
	,[ApplicableClass]
	,[IsMandatory])
	VALUES
	(NEWID(),
	@idInit,
	'ProductCode',
	'S1426',
	@valueDataType,
	@codeName,
	@StockItem_CriteriaClass,
	@mandatory)

INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
	([Id]
	,[DiscountCodeId]
	,[Criteria]
	,[Value]
	,[ValueDataType]
	,[AssociatedDiscountCode]
	,[ApplicableClass]
	,[IsMandatory])
VALUES
	(NEWID(),
	@idInit,
	'MediaType',
	'DVD',
	@valueDataType,
	@codeName,
	@StockItem_CriteriaClass,
	@mandatory)
	  

GO