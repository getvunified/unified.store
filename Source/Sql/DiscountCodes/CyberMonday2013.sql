/*
 * Author: Alex Payares
 * Date Created: June 26th / 2013
 */
 
DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME
DECLARE @dfd DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)
DECLARE @allowOnSaleItems BIT



SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @isFreeShipping = 0
SET @isFixedAmount = 0
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 0
SET @isActive = 1

SET @applicableCompanies = 1


DECLARE @quantity SMALLINT
DECLARE @productWarehouse SMALLINT
DECLARE @appliesOnlyToItemsMatchingCriteria BIT



SET @quantity = 1
SET @productWarehouse = 10


DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

SET @appliesOnlyToItemsMatchingCriteria = 0

SET @allowOnSaleItems = 1;

SET @idInit = 'abbd3266-1bcf-4877-99a5-4582c19b9ca1'
SET @batchId = NEWID()



SET @maxAmountOff = 0
SET @minPurchaseAmount = 0

-- SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @codeName = 'CYBER13'


IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   NULL,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)
	
	-- DISCOUNT CODE REQUIREMENTS FOR CYBERMONDAY AM
	SET @amountOff = 30.0
	SET @shortDescription = 'Cyber Monday Sale - 30% Off'
	SET @startDate = '2013-11-02 00:00:00:00'
	SET @endDate = '2013-12-02 11:59:59:00'

	INSERT INTO [dbo].[DiscountCodeRequirements]
	([Id]
	,[DiscountCodeId]
	,[StartDate]
	,[EndDate]
	,[IsFixedAmount]
	,[FreeShippingType]
	,[AmountOff]
	,[FreeShipping]
	,[MinPurchaseAmount]
	,[MaxAmountOff]
	,[ApplicableCompanies]
	,[IsSingleUsePrefix]
	,[IsTiedToProducts]
	,[IsAutoApply]
	,[IsActive]
	,[Quantity]
	,AppliesOnlyToItemsMatchingCriteria
	,[Description])
	VALUES
	(NEWID(),
	@idInit,
	@startDate,
	@endDate,
	@isFixedAmount,
	@freeShippingType,
	@amountOff,
	@isFreeShipping,
	@minPurchaseAmount,
	@maxAmountOff,
	@applicableCompanies,
	@isSingleUsePrefix,
	@isTiedToProducts,
	@isAutoApply,
	@isActive,
	@quantity,
	@appliesOnlyToItemsMatchingCriteria,
	@shortDescription)


	-- DISCOUNT CODE REQUIREMENTS FOR CYBERMONDAY PM
	SET @startDate = '2013-12-02 12:00:00:00'
	SET @endDate = '2013-12-02 23:59:59:00'
	SET @amountOff = 20.0
	SET @shortDescription = 'Cyber Monday Sale - 20% Off'
	
	INSERT INTO [dbo].[DiscountCodeRequirements]
	([Id]
	,[DiscountCodeId]
	,[StartDate]
	,[EndDate]
	,[IsFixedAmount]
	,[FreeShippingType]
	,[AmountOff]
	,[FreeShipping]
	,[MinPurchaseAmount]
	,[MaxAmountOff]
	,[ApplicableCompanies]
	,[IsSingleUsePrefix]
	,[IsTiedToProducts]
	,[IsAutoApply]
	,[IsActive]
	,[Quantity]
	,AppliesOnlyToItemsMatchingCriteria
	,[Description])
	VALUES
	(NEWID(),
	@idInit,
	@startDate,
	@endDate,
	@isFixedAmount,
	@freeShippingType,
	@amountOff,
	@isFreeShipping,
	@minPurchaseAmount,
	@maxAmountOff,
	@applicableCompanies,
	@isSingleUsePrefix,
	@isTiedToProducts,
	@isAutoApply,
	@isActive,
	@quantity,
	@appliesOnlyToItemsMatchingCriteria,
	@shortDescription)

END




	  
GO