﻿/*
 * Author: Alex Payares
 * Date Created: June 26th / 2013
 */
 
DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME
DECLARE @dfd DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)
DECLARE @allowOnSaleItems BIT


SET @startDate = '2013-12-25 18:00:00:00'
SET @endDate = '2013-12-26 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @isFreeShipping = 0
SET @isFixedAmount = 0
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 0
SET @isActive = 1

SET @applicableCompanies = 6


DECLARE @quantity SMALLINT
DECLARE @productWarehouse SMALLINT
DECLARE @appliesOnlyToItemsMatchingCriteria BIT



SET @quantity = 1
SET @productWarehouse = 50


DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

SET @appliesOnlyToItemsMatchingCriteria = 0

SET @allowOnSaleItems = 1;

SET @idInit = 'eb0cc567-fbfe-4c1d-aa17-a08c7a30d357'
SET @batchId = NEWID()


SET @amountOff = 30.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 50

-- SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @codeName = 'BOXING30 '
SET @shortDescription = 'Boxing Day 2013 - 30% Off'
SET @longDescription = @shortDescription


IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[LongDescription]
			   ,[StartDate]
			   ,[EndDate]
			   ,[IsFixedAmount]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   @shortDescription,
			   @longDescription,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)


	INSERT INTO [dbo].[DiscountCodeRequirements]

				([Id]
				,[DiscountCodeId]
				,[StartDate]
				,[EndDate]
			   ,[IsFixedAmount]
			   ,[FreeShippingType]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[Quantity]
			   ,AppliesOnlyToItemsMatchingCriteria)
			   VALUES
			   (NEWID(),
			   @idInit,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @freeShippingType,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @quantity,
			   @appliesOnlyToItemsMatchingCriteria)

END




	  
GO