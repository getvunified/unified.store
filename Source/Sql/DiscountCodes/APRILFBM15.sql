﻿DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)

DECLARE @quantity SMALLINT

SET @idInit = NEWID()
SET @batchId = NEWID()

SET @startDate = '2014-03-15 00:00:00:00'
SET @endDate = '2014-04-30 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @amountOff = 15.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 0.0

SET @isFreeShipping = 0
SET @isFixedAmount = 0
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 0
SET @isActive = 1

SET @applicableCompanies = 7

SET @quantity = 1

SET @codeName = 'APRILFBM15'
SET @shortDescription = '15% coupon code for B194P Four Blood Moons Book'
SET @longDescription = '15% coupon code for B194P Four Blood Moons Book'


INSERT INTO [dbo].[DiscountCode]
           ([Id]
           ,[CodeName]
           ,[ShortDescription]
           ,[LongDescription]
           ,[StartDate]
           ,[EndDate]
           ,[IsFixedAmount]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive]
		   ,[CreationDate]
		   ,[SendOutDate]
		   ,[UsedDate]
		   ,[BatchId])
		VALUES
           (@idInit,
		   @codeName,
           @shortDescription,
           @longDescription,
           @startDate,
           @endDate,
           @isFixedAmount,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive,
		   @createdDate,
		   @sendOutDate,
		   @usedDate,
		   @batchId)


DECLARE @groupId UNIQUEIDENTIFIER
DECLARE @groupAnd SMALLINT
DECLARE @productAnd SMALLINT
DECLARE @isFreeItem SMALLINT

DECLARE @productWarehouse SMALLINT

SET @groupId = NEWID()

SET @groupAnd = 0
SET @productAnd = 0
SET @isFreeItem = 0

SET @quantity = 1
SET @productWarehouse = 10

DECLARE @freeShippingType NVARCHAR(20)
DECLARE @appliesOnlyToItemsMatchingCriteria BIT

SET @freeShippingType = 'standard'
SET @appliesOnlyToItemsMatchingCriteria = 1

INSERT INTO [dbo].[DiscountCodeRequirements]

			([Id]
			,[DiscountCodeId]
			,[StartDate]
			,[EndDate]
           ,[IsFixedAmount]
		   ,[FreeShippingType]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive]
		   ,[Quantity]
		   ,AppliesOnlyToItemsMatchingCriteria)
		   VALUES
		   (NEWID(),
		   @idInit,
		   @startDate,
           @endDate,
           @isFixedAmount,
		   @freeShippingType,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive,
		   @quantity,
		   @appliesOnlyToItemsMatchingCriteria)

DECLARE @criteriaId UNIQUEIDENTIFIER

DECLARE @valueDataType NVARCHAR(10)
DECLARE @StockItem_CriteriaClass SMALLINT
DECLARE @Product_criteriaClass SMALLINT
DECLARE @mandatory BIT
DECLARE @optional BIT

SET @StockItem_CriteriaClass = 1 -- StockItem
SET @Product_criteriaClass = 2 -- Product
SET @mandatory = 1
SET @optional = 0


SET @valueDataType = 'String'


INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
	([Id]
	,[DiscountCodeId]
	,[Criteria]
	,[Value]
	,[ValueDataType]
	,[AssociatedDiscountCode]
	,[ApplicableClass]
	,[IsMandatory])
	VALUES
	(NEWID(),
	@idInit,
	'ProductCode',
	'B194',
	@valueDataType,
	@codeName,
	@StockItem_CriteriaClass,
	@mandatory)

INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
	([Id]
	,[DiscountCodeId]
	,[Criteria]
	,[Value]
	,[ValueDataType]
	,[AssociatedDiscountCode]
	,[ApplicableClass]
	,[IsMandatory])
VALUES
	(NEWID(),
	@idInit,
	'MediaType',
	'Paperback',
	@valueDataType,
	@codeName,
	@StockItem_CriteriaClass,
	@mandatory)
	  
GO