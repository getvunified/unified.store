﻿/*
 * Author: Alex Payares
 * Date Created: June 26th / 2013
 */
 
DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME
DECLARE @dfd DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)
DECLARE @allowOnSaleItems BIT


SET @idInit = '6DB96A7F-7616-400F-8E13-DD63461093B8'
SET @batchId = NEWID()



SET @startDate = '2013-07-04 00:00:00:00'
SET @endDate = '2013-07-04 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @amountOff = 10.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 15

SET @isFreeShipping = 0
SET @isFixedAmount = 0
SET @isAutoApply = 1
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 0
SET @isActive = 1

SET @applicableCompanies = 1

SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @shortDescription = '4th of July Sale: 10% off when you spend $15.00 - $39.99'
SET @longDescription = @shortDescription

DECLARE @quantity SMALLINT
DECLARE @productWarehouse SMALLINT
DECLARE @appliesOnlyToItemsMatchingCriteria BIT



SET @quantity = 1
SET @productWarehouse = 10


DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

SET @appliesOnlyToItemsMatchingCriteria = 0

SET @allowOnSaleItems = 0;


IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[LongDescription]
			   ,[StartDate]
			   ,[EndDate]
			   ,[IsFixedAmount]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   @shortDescription,
			   @longDescription,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)


	INSERT INTO [dbo].[DiscountCodeRequirements]

				([Id]
				,[DiscountCodeId]
				,[StartDate]
				,[EndDate]
			   ,[IsFixedAmount]
			   ,[FreeShippingType]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[Quantity]
			   ,AppliesOnlyToItemsMatchingCriteria)
			   VALUES
			   (NEWID(),
			   @idInit,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @freeShippingType,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @quantity,
			   @appliesOnlyToItemsMatchingCriteria)
END



SET @idInit = '0A49C5DA-06CD-469C-B13A-445625FD5647'
SET @batchId = NEWID()


SET @amountOff = 20.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 40

SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @shortDescription = '4th of July Sale: 20% off when you spend $40.00 - $74.99'
SET @longDescription = @shortDescription


IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[LongDescription]
			   ,[StartDate]
			   ,[EndDate]
			   ,[IsFixedAmount]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   @shortDescription,
			   @longDescription,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)


	INSERT INTO [dbo].[DiscountCodeRequirements]

				([Id]
				,[DiscountCodeId]
				,[StartDate]
				,[EndDate]
			   ,[IsFixedAmount]
			   ,[FreeShippingType]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[Quantity]
			   ,AppliesOnlyToItemsMatchingCriteria)
			   VALUES
			   (NEWID(),
			   @idInit,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @freeShippingType,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @quantity,
			   @appliesOnlyToItemsMatchingCriteria)

END




SET @idInit = 'C624BD09-1237-48D2-81FE-708361374C0E'
SET @batchId = NEWID()


SET @amountOff = 30.0
SET @maxAmountOff = 0
SET @minPurchaseAmount = 75

SET @codeName = CONVERT(NVARCHAR(50), NEWID())
SET @shortDescription = '4th of July Sale: 30% off when you spend $75.00 and up'
SET @longDescription = @shortDescription



IF ( NOT EXISTS (SELECT * FROM [dbo].[DiscountCode] WHERE Id = @idInit) )
BEGIN
	INSERT INTO [dbo].[DiscountCode]
			   ([Id]
			   ,[CodeName]
			   ,[ShortDescription]
			   ,[LongDescription]
			   ,[StartDate]
			   ,[EndDate]
			   ,[IsFixedAmount]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[CreationDate]
			   ,[SendOutDate]
			   ,[UsedDate]
			   ,[BatchId]
			   ,[AllowOnSaleItems])
			VALUES
			   (@idInit,
			   @codeName,
			   @shortDescription,
			   @longDescription,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @createdDate,
			   @sendOutDate,
			   @usedDate,
			   @batchId,
			   @allowOnSaleItems)


	INSERT INTO [dbo].[DiscountCodeRequirements]

				([Id]
				,[DiscountCodeId]
				,[StartDate]
				,[EndDate]
			   ,[IsFixedAmount]
			   ,[FreeShippingType]
			   ,[AmountOff]
			   ,[FreeShipping]
			   ,[MinPurchaseAmount]
			   ,[MaxAmountOff]
			   ,[ApplicableCompanies]
			   ,[IsSingleUsePrefix]
			   ,[IsTiedToProducts]
			   ,[IsAutoApply]
			   ,[IsActive]
			   ,[Quantity]
			   ,AppliesOnlyToItemsMatchingCriteria)
			   VALUES
			   (NEWID(),
			   @idInit,
			   @startDate,
			   @endDate,
			   @isFixedAmount,
			   @freeShippingType,
			   @amountOff,
			   @isFreeShipping,
			   @minPurchaseAmount,
			   @maxAmountOff,
			   @applicableCompanies,
			   @isSingleUsePrefix,
			   @isTiedToProducts,
			   @isAutoApply,
			   @isActive,
			   @quantity,
			   @appliesOnlyToItemsMatchingCriteria)
END

	  
GO