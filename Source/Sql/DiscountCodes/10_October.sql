﻿/*
 * Author: Alex Payares
 */
 
DECLARE @idInit UNIQUEIDENTIFIER
DECLARE @batchId UNIQUEIDENTIFIER
DECLARE @requirementsId UNIQUEIDENTIFIER

DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @createdDate DATETIME
DECLARE @sendOutDate DATETIME
DECLARE @usedDate DATETIME

DECLARE @amountOff DECIMAL
DECLARE @maxAmountOff DECIMAL
DECLARE @minPurchaseAmount DECIMAL

DECLARE @isFreeShipping SMALLINT
DECLARE @isFixedAmount SMALLINT
DECLARE @isAutoApply SMALLINT
DECLARE @applicableCompanies SMALLINT
DECLARE @isSingleUsePrefix SMALLINT
DECLARE @isTiedToProducts SMALLINT
DECLARE @isActive SMALLINT

DECLARE @codeName NVARCHAR(MAX)
DECLARE @shortDescription NVARCHAR(MAX)
DECLARE @longDescription NVARCHAR(MAX)

DECLARE @quantity SMALLINT

SET @idInit = NEWID()
SET @batchId = NEWID()
SET @requirementsId = NEWID()

SET @startDate = '2013-10-01 00:00:00:00'
SET @endDate = '2013-10-31 23:59:59:00'
SET @createdDate = GETDATE()
SET @sendOutDate = NULL
SET @usedDate = NULL

SET @amountOff = 10
SET @maxAmountOff = 00
SET @minPurchaseAmount = 0

SET @isFreeShipping = 0
SET @isFixedAmount = 0
SET @isAutoApply = 0
SET @isSingleUsePrefix = 0
SET @isTiedToProducts = 1
SET @isActive = 1

SET @applicableCompanies = 7

SET @quantity = 1

SET @codeName = 'SHAWL10'
SET @shortDescription = '10% Off when you purchase prayer shawl and prayer shawl pouch'
SET @longDescription = '10% Off when you purchase prayer shawl and prayer shawl pouch'


INSERT INTO [dbo].[DiscountCode]
           ([Id]
           ,[CodeName]
           ,[ShortDescription]
           ,[LongDescription]
		   ,[CreationDate]
		   ,[SendOutDate]
		   ,[UsedDate]
		   ,[BatchId])
		VALUES
           (@idInit,
		   @codeName,
           @shortDescription,
           @longDescription,
		   @createdDate,
		   @sendOutDate,
		   @usedDate,
		   @batchId)

DECLARE @freeShippingType NVARCHAR(20)

SET @freeShippingType = 'standard'

INSERT INTO [dbo].[DiscountCodeRequirements]

			([Id]
			,[DiscountCodeId]
			,[StartDate]
			,[EndDate]
           ,[IsFixedAmount]
		   ,[FreeShippingType]
           ,[AmountOff]
           ,[FreeShipping]
           ,[MinPurchaseAmount]
           ,[MaxAmountOff]
		   ,[ApplicableCompanies]
		   ,[IsSingleUsePrefix]
		   ,[IsTiedToProducts]
		   ,[IsAutoApply]
		   ,[IsActive])
		   VALUES
		   (@requirementsId,
		   @idInit,
		   @startDate,
           @endDate,
           @isFixedAmount,
		   @freeShippingType,
           @amountOff,
           @isFreeShipping,
           @minPurchaseAmount,
           @maxAmountOff,
		   @applicableCompanies,
		   @isSingleUsePrefix,
		   @isTiedToProducts,
		   @isAutoApply,
		   @isActive)


--Classes for Criteria are either StockItem(1) or Product(2)

--DECLARE @criteriaId UNIQUEIDENTIFIER

--DECLARE @criteria1 NVARCHAR(10)
--DECLARE @value1 NVARCHAR(5)
--DECLARE @value1DataType NVARCHAR(10)
--DECLARE @criteria1Class SMALLINT

--SET @criteriaId = NEWID()

--SET @criteria1 = 'MediaType'
--SET @value1 = 'B'
--SET @value1DataType = 'string'
--SET @criteria1Class = 1

--INSERT INTO [dbo].[DiscountCodeOptionalCriteria]
--	([Id]
--	,[DiscountCodeId]
--	,[Criteria]
--	,[Value]
--	,[ValueDataType]
--	,[AssociatedDiscountCode]
--	,[ApplicableClass])
--	VALUES
--	(@criteriaId,
--	@idInit,
--	@criteria1,
--	@value1,
--	@value1DataType,
--	@codeName,
--	@criteria1Class)


DECLARE @groupId UNIQUEIDENTIFIER
DECLARE @groupAnd SMALLINT
DECLARE @productAnd SMALLINT
DECLARE @isFreeItem SMALLINT

DECLARE @productWarehouse SMALLINT

SET @groupId = NEWID()

SET @groupAnd = 0
SET @productAnd = 0
SET @isFreeItem = 0

SET @quantity = 1
SET @productWarehouse = 10

INSERT INTO [dbo].[DiscountCodeItems]
(
	   [Id]
      ,[CodeId]
      ,[GroupId]
      ,[GroupAnd]
      ,[ProductAnd]
      ,[ProductCode]
      ,[DiscountCode]
      ,[IsFreeItem]
      ,[StartDate]
      ,[EndDate]
      ,[IsFixedAmount]
      ,[FreeShipping]
      ,[AmountOff]
      ,[MinPurchaseAmount]
      ,[MaxAmountOff]
      ,[ProductWarehouse]
      ,[Quantity])
	  VALUES
	  (NEWID(),
	  @idInit,
	  @groupId,
	  @groupAnd,
	  @productAnd,
	  'K542',
	  @codeName,
	  @isFreeItem,
	  @startDate,
	  @endDate,
	  @isFixedAmount,
	  @isFreeShipping,
	  @amountOff,
	  @minPurchaseAmount,
	  @maxAmountOff,
	  @productWarehouse,
	  @quantity),

	  (NEWID(),
	  @idInit,
	  @groupId,
	  @groupAnd,
	  @productAnd,
	  'K543',
	  @codeName,
	  @isFreeItem,
	  @startDate,
	  @endDate,
	  @isFixedAmount,
	  @isFreeShipping,
	  @amountOff,
	  @minPurchaseAmount,
	  @maxAmountOff,
	  @productWarehouse,
	  @quantity)
	  
GO