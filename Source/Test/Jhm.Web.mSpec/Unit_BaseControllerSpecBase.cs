using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;
using Machine.Specifications.DevelopWithPassion.Rhino;
using Rhino.Mocks;

namespace Jhm.Web.mSpec
{
    public class Unit_BaseControllerSpecBase<T, S> : Observes<T>
        where T : BaseController<S> where S: class, IServiceCollection
        
    {
        Establish c = () =>
        {
            serviceUnderTest = the_dependency<S>();
            var unitOfWorkContainer = an<IUnitOfWorkContainer>();
            var unitOfWork = an<IUnitOfWork>();
            
            unitOfWorkContainer.Stub(x => x.CurrentUOW).Return(unitOfWork);
            unitOfWork.Stub(x => x.IsInTransaction).Return(true);


            provide_a_basic_sut_constructor_argument<IService>(serviceUnderTest);
            provide_a_basic_sut_constructor_argument(unitOfWorkContainer);
        };

        public static S serviceUnderTest;
    }
}