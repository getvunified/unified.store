using Jhm.Common;
using Jhm.Common.UOW;
using Machine.Specifications;
using Machine.Specifications.DevelopWithPassion.Rhino;

namespace Jhm.Web.mSpec
{
    public class Unit_PresenterBaseSpecBase<T,V, S> : Observes<T>
        where T : PresenterBase<V,S> 
        where V: class, IView
        where S: class, IService
        
    {
        Establish c = () =>
        {
            viewUnderTest = the_dependency<V>();
            serviceUnderTest = the_dependency<S>();
            var unitOfWorkFactory = an<IUnitOfWorkFactory>();
            var unitOfWork = new UnitOfWork(unitOfWorkFactory);

            provide_a_basic_sut_constructor_argument<IView>(viewUnderTest);
            provide_a_basic_sut_constructor_argument<IService>(serviceUnderTest);
            provide_a_basic_sut_constructor_argument<UnitOfWork>(unitOfWork);
            
        };


        public static V viewUnderTest;
        public static S serviceUnderTest;
        
    }
}