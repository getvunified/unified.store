﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core.Models;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio.core.Inventory;
using getv.donorstudio.services.IoC;
using getv.donorstudio.services.Services;


namespace Jhm.Web.ReloadAllProducts
{
    [TestClass]
    public class ReloadAllProducts_MSTest
    {
        private static string environment = "JHMPROD";
        static string filepath = "C:\\products\\" + environment + ".bin";
        static string jsonFilepath = "C:\\products\\" + environment + ".json.txt";
        private static List<Product> products;
        protected static DsProductService productService;

        [TestInitialize]
        public void Init()
        {
            ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
            productService = ObjectFactory.GetInstance<DsProductService>();
        }

        [TestMethod]
        public void Getting_all_products_from_US_environment_should_write_file()
        {
            // Arrange
            products = productService.GetAllProducts(environment).Select(DsProductMapper.Map).OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).ToList();

            // Act
            if (products != null)
                products.Count.ShouldBeGreaterThan(0);

            // Assert
            Assert.IsNotNull(products);
            Assert.IsTrue(products.Count > 0, "Product count should be greater than zero");
            Assert.IsTrue(File.Exists(filepath), "File: {0} does not have a file length > 0");
            Assert.IsTrue(File.Exists(jsonFilepath), "File: {0} does not have a file length > 0");
            filepath.Is( fp =>
            {
                var f = new FileInfo(fp);
                return f.Length > 0;
            }, "File: {0} does not have a file length > 0");
            jsonFilepath.Is(fp =>
            {
                var f = new FileInfo(fp);
                return f.Length > 0;
            }, "File: {0} does not have a file length > 0");

        }

        public static void WriteBinaryToFile<T>(string filename, T entity) where T : class
        {
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(stream, entity);
            }
        }

        public static void WriteProductsToJSONFile(string filename, List<Product> products)
        {
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = 2147483644;
            var json = serializer.Serialize(products);
            WriteStringToFile(filename, json, true);
        }

        public static void WriteStringToFile(string filename, string contents, bool overwrite)
        {
            if (!overwrite)
            {
                if (File.Exists(filename))
                {
                    // We don't want to overwrite the file, so just return
                    return;
                }
            }
            using (var filestream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                using (var streamwriter = new StreamWriter(filestream))
                {
                    streamwriter.Write(contents);
                }
            }
        }
    }
}
