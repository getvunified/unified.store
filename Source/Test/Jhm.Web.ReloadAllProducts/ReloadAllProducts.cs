﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core.Models;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio.core.Inventory;
using getv.donorstudio.services.IoC;
using getv.donorstudio.services.Services;

namespace Jhm.Web.ReloadAllProducts
{
    
        public class Concern
        {
            Establish context = () =>
            {
                ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                productService = ObjectFactory.GetInstance<DsProductService>();

                //DependencyResolver.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                //productService = DependencyResolver.GetInstance<DsProductService>();

            };

            public static void WriteBinaryToFile<T>(string filename, T entity) where T : class
            {
                var formatter = new BinaryFormatter();
                using (var stream = File.Open(filename, FileMode.Create, FileAccess.Write))
                {
                    formatter.Serialize(stream, entity);
                }
            }

            public static void WriteProductsToJSONFile(string filename, List<Product> products)
            {
                var serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = 2147483644;
                var json = serializer.Serialize(products);
                WriteStringToFile(filename, json, true);
            }

            public static void WriteStringToFile(string filename, string contents, bool overwrite)
            {
                if (!overwrite)
                {
                    if (File.Exists(filename))
                    {
                        // We don't want to overwrite the file, so just return
                        return;
                    }
                }
                using (var filestream = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    using (var streamwriter = new StreamWriter(filestream))
                    {
                        streamwriter.Write(contents);
                    }
                }
            }

            protected static DsProductService productService;
        }

        [Subject(typeof(DsProductService))]
        public class When_getting_all_products_from_US_environment : Concern
        {
            Because of = () =>
            {
                products = productService.GetAllProducts(environment).Select(DsProductMapper.Map).OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).ToList();
            };

            private It should_return_a_result = () =>
            {
                if (products != null)
                    products.Count.ShouldBeGreaterThan(0);
            };


            private It should_write_products_to_JSON_file = () =>
                {
                    WriteBinaryToFile(filepath, products);
                    WriteProductsToJSONFile(jsonFilepath, products);
                };

            private static string environment = "JHMPROD";
            static string filepath = "C:\\products\\" + environment + ".bin";
            static string jsonFilepath = "C:\\products\\" + environment + ".json.txt";
            private static List<Product> products;
        }

        [Subject(typeof(DsProductService))]
        public class When_getting_all_products_from_Canada_environment : Concern
        {
            Because of = () =>
            {
                products = productService.GetAllProducts(environment).Select(DsProductMapper.Map).OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).ToList();
            };

            private It should_return_a_result = () =>
            {
                if (products != null)
                    products.Count.ShouldBeGreaterThan(0);
            };

            private It should_write_products_to_JSON_file = () =>
            {
                WriteBinaryToFile(filepath, products);
                WriteProductsToJSONFile(jsonFilepath, products);
            };

            private static string environment = "CANPROD";
            static string filepath = "C:\\products\\" + environment + ".bin";
            static string jsonFilepath = "C:\\products\\" + environment + ".json.txt";
            private static List<Product> products;
        }

        [Subject(typeof(DsProductService))]
        public class When_getting_all_products_from_UK_environment : Concern
        {
            Because of = () =>
            {
                products = productService.GetAllProducts(environment).Select(DsProductMapper.Map).OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).ToList();
            };

            private It should_return_a_result = () =>
            {
                if (products != null)
                    products.Count.ShouldBeGreaterThan(0);
            };

            private It should_write_products_to_JSON_file = () =>
            {
                WriteBinaryToFile(filepath, products);
                WriteProductsToJSONFile(jsonFilepath, products);
            };

            private static string environment = "UKPROD";
            static string filepath = "C:\\products\\"+ environment + ".bin";
            static string jsonFilepath = "C:\\products\\" + environment + ".json.txt";
            private static List<Product> products;
        }


        
    
}
