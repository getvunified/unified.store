﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public static class MSTestExtensions
    {
        /// <summary>IsNull</summary>
        public static void Is<T>(this T value)
        {
            Assert.IsNull(value);
        }

        public static void Is<T>(this T actual, T expected, string message = "")
        {
            Assert.AreEqual(expected, actual, message);
        }

        public static void Is<T>(this T actual, Func<T, bool> expected, string message = "")
        {
            Assert.IsTrue(expected(actual), message);
        }

        public static void Is<T>(this IEnumerable<T> actual, IEnumerable<T> expected, string message = "")
        {
            CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), message);
        }

        public static void Is<T>(this IEnumerable<T> actual, params T[] expected)
        {
            Is(actual, expected.AsEnumerable());
        }

        public static void Is<T>(this IEnumerable<T> actual, IEnumerable<Func<T, bool>> expected)
        {
            var count = 0;
            foreach (var cond in actual.Zip(expected, (v, pred) => pred(v)))
            {
                Assert.IsTrue(cond, "Index = " + count++);
            }
        }

        public static void Is<T>(this IEnumerable<T> actual, params Func<T, bool>[] expected)
        {
            Is(actual, expected.AsEnumerable());
        }
    }
}