﻿using System;
using Jhm.Web.UI.IBootStrapperTasks;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio;

namespace Jhm.Web.Specs.Unit.Modules.DonorStudio
{
    
    public class WithDonorStudioReportService
    {
        private Establish context = () =>
                                        {
                                            ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                                            donorReportService = ObjectFactory.GetInstance<DsReportService>();

                                            //donorReportService = new DsReportService();
                                        };

        protected static DsReportService donorReportService;
    }



     [Subject("Donor Studio Reports")]
    public class When_you_run_annual_tax_receipt_report : WithDonorStudioReportService
     {
         private Establish context = () =>
                                         {
                                             begDate = new DateTime(2012, 1, 1);
                                             endDate = new DateTime(2012, 12, 31);
                                             accountNumber = 1420998459;
                                         };

        Because of = () =>
                         {
                             pdfFile = donorReportService.GetTaxReceiptReport(begDate, endDate, accountNumber, out extension,
                                                                    out mimeType, out encoding);
                         };

        It should_be_able_to_get_a_pdf_back = () => pdfFile.ShouldBeOfType<byte[]>();


         private static DateTime begDate;
         private static DateTime endDate;
         private static int accountNumber;
         private static byte[] pdfFile;
         private static string extension;
         private static string mimeType;
         private static string encoding;
     }

    
}
