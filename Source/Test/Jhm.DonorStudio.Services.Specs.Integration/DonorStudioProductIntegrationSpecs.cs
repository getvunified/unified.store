using System;
using System.Collections.Generic;
using System.Linq;

using Jhm.Web.UI.IBootStrapperTasks;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Inventory;

namespace Jhm.DonorStudio.Services.Specs.Integration.ProductSpecs
{
    public class Concern
    {
        Establish context = () =>
                                {
                                    ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                                    productService = ObjectFactory.GetInstance<DsInventoryService>();

                                };

        protected static DsInventoryService productService;
    }

    [Subject(typeof(DsInventoryService))]
    public class When_searching_for_a_list_of_products_by_environment : Concern
    {
        Because of = () =>
                         {
                             products = productService.GetProducts(environment, null, null, null, null, 0, 0, new DateTime(), null, null, null, true, false, 1, int.MaxValue, ref totalRows);
                         };

        private It should_find_a_list_of_products = () =>
                                                        {
                                                            products.Count.ShouldBeGreaterThan(0);
                                                        };

        private static string environment = "JHMPROD";
        private static List<DsProduct> products;
        private static int totalRows;
    }

    [Subject(typeof(DsInventoryService))]
    public class When_searching_for_a_product_by_search_criteria_and_viewing_the_first_page_of_the_results : Concern
    {
        Because of = () =>
                         {
                             products = productService.GetProducts(environment, author, description, productCode, title, priceMin, priceMax, releaseDate, category, subject, mediaType, isAvailable, onSale, pageNumber, maxRowsOnPage, ref numberOfPages);
                         };

        It should_return_a_list_of_products_that_satisfy_that_criteria = () =>
            {
                foreach (var DsProduct in products)
                {
                    if (!string.IsNullOrEmpty(author))
                        DsProduct.Author.ToLower().ShouldContain(author.ToLower());
                    if (!string.IsNullOrEmpty(description))
                        DsProduct.Description.ToLower().ShouldContain(description.ToLower());
                    if (!string.IsNullOrEmpty(productCode))
                        DsProduct.RootProductCode.ToLower().ShouldContain(productCode.ToLower());
                    if (!string.IsNullOrEmpty(title))
                        DsProduct.Title.ToLower().ShouldContain(title.ToLower());
                    if (priceMin > 0)
                        DsProduct.Items.Select(x => x.SalePrice < priceMin).Count().ShouldEqual(0);
                    if (priceMax > 0)
                        DsProduct.Items.Where(x => x.SalePrice > priceMax).Count().ShouldEqual(0);
                    if (releaseDate.CompareTo(new DateTime()) > 0)
                        DsProduct.Items.Where(x => x.ReleaseDate.GetValueOrDefault().CompareTo(releaseDate) >= 0).Count().ShouldBeGreaterThan(0);
                    if (!string.IsNullOrEmpty(category))
                        DsProduct.Categories.ShouldContain(category);
                    if (!string.IsNullOrEmpty(subject))
                        DsProduct.Categories.ShouldContain(subject);
                    if (!string.IsNullOrEmpty(mediaType))
                        DsProduct.Items.Where(x => x.MediaType == mediaType).Count().ShouldBeGreaterThan(0);
                    if (isAvailable)
                        DsProduct.Items.Where(x => x.QuantityInStock <= 0 && !x.IsInHouse).Count().ShouldEqual(0);
                    if (onSale)
                        DsProduct.Items.Where(x => x.SalePrice < x.BasePrice).Count().ShouldEqual(DsProduct.Items.Count);
                }
            };

        It should_contain_information_about_the_number_of_pages_in_the_whole_results_set = () =>
            {
                numberOfPages.ShouldNotBeNull();
                numberOfPages.ShouldBeGreaterThan(-1);
            };

        It should_only_show_results_for_that_page = () =>
            {
                products.Count.ShouldBeLessThan(maxRowsOnPage + 1);
            };

        It should_contain_one_or_more_items_for_each_product_in_the_list = () =>
        {
            if (products.Count > 0)
            {
                foreach (var p in products)
                {
                    p.Items.Count.ShouldBeGreaterThan(0);
                }
            }
        };


        private static string environment = "JHMPROD";
        private static List<DsProduct> products;

        private static string author = string.Empty;
        private static string description = string.Empty;
        private static string productCode = string.Empty;
        private static string title = string.Empty;
        private static decimal priceMin;
        private static decimal priceMax;
        private static DateTime releaseDate = new DateTime(2011, 03, 07);
        private static string category = "CAT1";
        private static string subject = string.Empty;
        private static string mediaType = string.Empty;
        private static bool isAvailable = true;
        private static bool onSale = false;
        private static int numberOfPages = -1;
        private static int pageNumber = 1;
        private static int maxRowsOnPage = 3;
    }

    [Subject(typeof(DsInventoryService))]
    public class When_searching_for_a_product_by_search_criteria_and_viewing_the_2nd_page_of_the_results : Concern
    {
        Because of = () =>
        {
            products = productService.GetProducts(environment, author, description, productCode, title, priceMin, priceMax, releaseDate, category, subject, mediaType, isAvailable, onSale, pageNumber, maxRowsOnPage , ref numberOfPages);
        };


        It should_return_a_list_of_products_that_satisfy_that_criteria = () =>
            {
                foreach (var DsProduct in products)
                {
                    if (!string.IsNullOrEmpty(author))
                        DsProduct.Author.ToLower().ShouldContain(author.ToLower());
                    if (!string.IsNullOrEmpty(description))
                        DsProduct.Description.ToLower().ShouldContain(description.ToLower());
                    if (!string.IsNullOrEmpty(productCode))
                        DsProduct.RootProductCode.ToLower().ShouldContain(productCode.ToLower());
                    if (!string.IsNullOrEmpty(title))
                        DsProduct.Title.ToLower().ShouldContain(title.ToLower());
                    if (priceMin > 0)
                        DsProduct.Items.Select(x => x.SalePrice < priceMin).Count().ShouldEqual(0);
                    if (priceMax > 0)
                        DsProduct.Items.Where(x => x.SalePrice > priceMax).Count().ShouldEqual(0);
                    if (releaseDate.CompareTo(new DateTime()) > 0)
                        DsProduct.Items.Where(x => x.ReleaseDate.GetValueOrDefault().CompareTo(releaseDate) >= 0).Count().ShouldBeGreaterThan(0);
                    if (!string.IsNullOrEmpty(category))
                        DsProduct.Categories.ShouldContain(category);
                    if (!string.IsNullOrEmpty(subject))
                        DsProduct.Categories.ShouldContain(subject);
                    if (!string.IsNullOrEmpty(mediaType))
                        DsProduct.Items.Where(x => x.MediaType == mediaType).Count().ShouldBeGreaterThan(0);
                    if (isAvailable)
                        DsProduct.Items.Where(x => x.QuantityInStock <= 0 && !x.IsInHouse).Count().ShouldEqual(0);
                    if (onSale)
                        DsProduct.Items.Where(x => x.SalePrice < x.BasePrice).Count().ShouldEqual(DsProduct.Items.Count);
                }
            };

        It should_contain_information_about_the_number_of_pages_in_the_whole_results_set = () =>
            {
                numberOfPages.ShouldNotBeNull();
                numberOfPages.ShouldBeGreaterThan(-1);
            };

        It should_only_show_results_for_that_page = () =>
            {
                products.Count.ShouldBeLessThan(maxRowsOnPage + 1);
            };

        private static string author = "";
        private static string description = string.Empty;
        private static string productCode = string.Empty;
        private static string title = string.Empty;
        private static decimal priceMin;
        private static decimal priceMax;
        private static DateTime releaseDate;
        private static string category = string.Empty;
        private static string subject = string.Empty;
        private static string mediaType = string.Empty;
        private static bool isAvailable = true;
        private static bool onSale = false;
        private static string environment = "JHMPROD";
        private static List<DsProduct> products;
        private static int numberOfPages = -1;
        private static int pageNumber = 2;
        private static int maxRowsOnPage = 3;
    }

    [Subject(typeof(DsInventoryService))]
    public class When_getting_a_product_by_product_code : Concern
    {
        Because of = () =>
        {
            product = productService.GetProduct(environment, productCode);
        };

        private It should_contain_at_least_one_item_if_it_is_not_null = () =>
                                                          {
                                                              if (product != null)
                                                                  product.Items.Count.ShouldBeGreaterThan(0);
                                                          };

        private static string environment = "JHMPROD";
        private static DsProduct product;
        private static string productCode = "S1244";
    }

    [Subject(typeof(DsInventoryService))]
    public class When_getting_all_products : Concern
    {
        Because of = () =>
        {
            products = productService.GetAllProducts(environment);
        };

        private It should_return_a_result = () =>
        {
            if (products != null)
                products.Count.ShouldBeGreaterThan(0);
        };

        private static string environment = "JHMPROD";
        private static List<DsProduct> products;
    }
}