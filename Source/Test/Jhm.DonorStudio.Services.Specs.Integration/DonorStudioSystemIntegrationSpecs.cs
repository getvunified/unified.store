using System.Collections.Generic;
using Jhm.Web.UI.IBootStrapperTasks;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Global;
using getv.donorstudio.core.eCommerce;

namespace Jhm.DonorStudio.Services.Specs.Integration.SystemSpecs
{
    public class Concern
    {
        Establish context = () =>
                                {
                                    ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                                    saService = ObjectFactory.GetInstance<DsSystemAdministrationService>();

                                };

        protected static DsSystemAdministrationService saService;
    }

    [Subject(typeof(DsSystemAdministrationService))]
    public class When_searching_for_a_control_record : Concern
    {
        Because of = () =>
                         {
                             controlRecord = saService.GetControlRecord(environment, controlRecord: "INVSYS001");
                         };

        private It should_find_the_control_record = () =>
                                                        {
                                                            controlRecord.ShouldNotBeNull();
                                                            controlRecord.Value.ShouldBeEqualIgnoringCase("DDC");
                                                        };

        private static string environment = "JHMPROD";
        private static DsControlRecord controlRecord;
    }

    [Subject(typeof(DsSystemAdministrationService))]
    public class When_searching_for_a_list_of_code_values : Concern
    {
        Because of = () =>
        {
            codeValues = saService.GetCodeValues(environment, codeType: "DDCGENDER");
        };

        private It should_find_the_control_record = () =>
        {
            codeValues.ShouldNotBeNull();
            codeValues.Count.ShouldBeGreaterThan(0);
        };

        private static string environment = "JHMPROD";
        private static List<DsCodeValue> codeValues;
    }

    [Subject(typeof(DsSystemAdministrationService))]
    public class When_retrieving_shipping_methods : Concern
    {
        Because of = () =>
        {
            shippingMethods = saService.GetShippingMethods(environment);
        };

        private It should_find_the_shipping_methods = () =>
        {
            shippingMethods.ShouldNotBeNull();
            shippingMethods.Count.ShouldBeGreaterThan(0);
        };

        private static string environment = "JHMPROD";
        private static List<DsShippingMethod> shippingMethods;
    }

}