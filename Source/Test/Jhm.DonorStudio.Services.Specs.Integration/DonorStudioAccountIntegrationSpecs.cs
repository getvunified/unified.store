using System;
using System.Collections.Generic;
using Jhm.Web.UI.IBootStrapperTasks;
using Machine.Specifications;
using StructureMap;
using getv.donorstudio;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Utilities;
using getv.donorstudio.core.eCommerce;

namespace Jhm.DonorStudio.Services.Specs.Integration
{
    public class Concern
    {
        Establish context = () =>
                                {
                                    ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
                                    accountService = ObjectFactory.GetInstance<DsAccountTransactionService>();

                                    /*
                                    new ConfigureDependencies().Execute();
                                    var internalService = DependencyResolver.GetInstance<IDSInternalService>();
                                    accountService = new DsAccountTransactionService(internalService);
                                    */
                                };

        protected static DsAccountTransactionService accountService;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_creating_a_new_account_in_DS : Concern
    {
        Because of = () =>
                                 {
                                     accountNumber = accountService.CreateAccount(environment, DsAccount);
                                 };

        It should_return_an_account_number = () =>
                                                         {
                                                             accountNumber.ShouldBeGreaterThan(0);
                                                         };

        private static string environment = "JHMPROD";

        private static DsAccount DsAccount = new DsAccount
                                                   {
                                                       FirstName = "Web",
                                                       MiddleName = "Dash",
                                                       LastName = "Test5",
                                                       Gender = "M",
                                                       Birthday = new DateTime(1972, 12, 12),
                                                       Email = "webtest@test.com",
                                                       Salutation = "Mr. Test",
                                                       Source = "WEB",
                                                       Suffix = "Jr.",
                                                       Title = "Mr.",
                                                       Type = DsConstants.AccountType.Individual,
                                                       DsAddress =
                                                           new DsAddress
                                                               {
                                                                   Type = "HOME",
                                                                   Line1 = "1233 Drive",
                                                                   Line2 = "Apt 223",
                                                                   City = "Frankfort",
                                                                   State = "KY",
                                                                   Country = "USA",
                                                                   ZipCode = "40601"
                                                               },
                                                       Phone = new DsPhone
                                                                   {
                                                                       CountryCode = "1",
                                                                       AreaCode = "342",
                                                                       Number = "2314451",
                                                                       Extension = string.Empty,
                                                                       Type = "HOME"
                                                                   }
                                                   };

        private static long accountNumber = -1;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_updating_an_account_in_DS : Concern
    {
        Because of = () =>
                         {
                             var DsAccount = accountService.GetAccount(environment, accountNumber: 1420952135);
                             DsAccount.ShouldNotBeNull();
                             DsAccount.FirstName = newFirstName;
                             DsAccount.DsAddress.Line2 = newLine2;
                             DsAccount.Email = newEmail;
                             DsAccount.Phone.Number = newNumber;

                             accountService.UpdateAccount(environment, DsAccount);
                         };

        It should_return_perform_the_update = () =>
                                                 {
                                                     var test = accountService.GetAccount(environment, accountNumber: 1420952135);
                                                     test.ShouldNotBeNull();
                                                     test.DsAddress.Line2.ShouldEqual(newLine2);
                                                     test.FirstName.ShouldEqual(newFirstName);
                                                     test.Email.ShouldEqual(newEmail);
                                                     test.Phone.Number.ShouldEqual(newNumber);
                                                 };

        private static string environment = "JHMPROD";
        private static string newFirstName = "New-Web-Name";
        private static string newLine2 = "New Line2";
        private static string newEmail = "emailnew@new.com";
        private static string newNumber = "123412341";
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_searching_for_an_account_in_donor_studio : Concern
    {
        Because of = () =>
                         {
                             DsAccount = accountService.GetAccount(environment, accountNumber: 1420952135);
                         };

        It should_return_a_result_if_account_is_found_by_account_number = () =>
                                                                              {
                                                                                  DsAccount.ShouldNotBeNull();
                                                                              };

        private static string environment = "JHMPROD";
        private static DsAccount DsAccount;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_searching_for_all_addresses_of_an_account_in_donor_studio : Concern
    {
        Because of = () =>
        {
            addresses = accountService.GetAccountAddresses(environment, accountNumber: 1420952135);
        };

        It should_return_a_result_if_account_is_found = () =>
        {
            addresses.ShouldNotBeNull();
            addresses.Count.ShouldBeGreaterThan(0);
        };

        private static string environment = "JHMPROD";
        private static List<DsAddress> addresses;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_adding_a_new_account_address : Concern
    {
        Because of = () =>
        {
            addressId = accountService.CreateAccountAddress(environment, accountNumber, dsAddress);
        };

        It should_return_an_addressId_if_address_is_successfully_added = () =>
        {
            addressId.ShouldNotBeNull();
            addressId.ShouldBeGreaterThan(0);
        };

        private static string environment = "JHMPROD";
        private static List<DsAddress> addresses;
        private static DsAddress dsAddress = new DsAddress
                                             {
                                                 City = "Dallas",
                                                 Country = "USA",
                                                 IsPrimary = false,
                                                 Line1 = "123 Elsewhere Ln",
                                                 Line2 = "Apt 1234",
                                                 State = "TX",
                                                 Type = "WEB",
                                                 ZipCode = "75033"
                                             };

        private static long accountNumber = 1420952135;
        private static long addressId;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_managing_the_profile_and_viewing_the_transaction_history : Concern
    {
        Because of = () =>
                         {
                             DsTransactions = accountService.GetTransactionHistory(environment, howManyMonthsBack: 24, accountNumber: 1420998460);
                         };
        It should_return_a_list_of_transactions_based_on_the_account_number_provided = () =>
                                                                                           {
                                                                                               DsTransactions.ShouldNotBeNull();
                                                                                               DsTransactions.ShouldNotBeEmpty();
                                                                                               foreach (var t in DsTransactions)
                                                                                               {
                                                                                                   var hasEither = t.Donations.Count > 0 || t.Orders.Count > 0;
                                                                                                   hasEither.ShouldNotEqual(false);
                                                                                               }
                                                                                           };
        private static string environment = "JHMPROD";
        private static List<DsTransaction> DsTransactions;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_searching_for_a_merged_account_number : Concern
    {
        private Because of = () =>
        {
            accountNumber = accountService.GetCurrentAccountNumber(environment, mergedAccountNumber: 1421000495);
        };

        private It should_return_the_current_account_number = () =>
                                                                  {
                                                                      accountNumber.ShouldEqual(1420998460);
                                                                  };

        private static long accountNumber = -1;
        private static string environment = "JHMPROD";
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_processing_a_transaction : Concern
    {
        private Because of = () =>
                                 {
                                     transactionId = accountService.CreateTransaction(environment, transaction, true);
                                 };

        private It should_create_a_new_transaction_associated_to_the_given_account = () =>
                        {
                            transactionId.ShouldBeGreaterThan(0);
                        };

        private static OrderDetail orderDetail = new OrderDetail
                                                     {
                                                         Price = 15,
                                                         Quantity = 1,
                                                         Comment = "A Comment",
                                                         ProductCode = "0107D",
                                                         PriceCode = "ST",
                                                         SourceCode = "INET_PROD"
                                                     };
        private static Order order = new Order
                                         {
                                             OrderDetails = new List<OrderDetail> { orderDetail },
                                             //AddressId = 18426926,
                                             ShippingMethod = "STANDARD",
                                             ShippingAmount = 4,
                                             Warehouse = "10"
                                         };

        private static Donation donation = new Donation
                                               {
                                                   Amount = 15,
                                                   SourceCode = "INET_GIFT",
                                                   Anonymous = false,
                                                   IsDeductible = true,
                                                   ProjectCode = "JHM"
                                               };

        private static DsTransaction transaction = new DsTransaction
                                                        {
                                                            AccountNumber = 1420952135,
                                                            CurrencyCode = "USD",
                                                            Date = DateTime.Today,
                                                            MediaId = string.Empty,
                                                            Status = "A",
                                                            Orders = new List<Order> { order },
                                                            Donations = new List<Donation> { donation },
                                                            Payments = new List<Payment> { new Payment { PaymentType = DsConstants.PaymentType.NoFunds } } //Can be changed with CreditCard and CC info needs to be uncommented below
                                                            /*CreditCard = new CreditCard
                                                                             {
                                                                                 Number = "4444555566667777",
                                                                                 MerchantId = "BLUE",
                                                                                 ExpirationMonth = "01",
                                                                                 ExpirationYear = "01",
                                                                                 AlreadyProcessed = true,
                                                                                 Token = "111111"
                                                                             }*/
                                                        };
        private static long transactionId;
        private static string environment = "JHMPROD";
    }


    [Subject(typeof(DsAccountTransactionService))]
    public class When_getting_a_batch_number_based_on_userId_and_companyCode : Concern
    {
        private Because of = () =>
        {
            batchNumber = accountService.GetAutoBatchNumber(environment, userid, companyCode);
        };

        private It should_return_a_batch_number = () =>
                                                      {
                                                          batchNumber.ShouldBeGreaterThan(0);
                                                      };

        private static string environment = "JHMPROD";
        private static string userid = "Mihai";
        private static string companyCode = "00001";
        private static long batchNumber;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_searching_for_an_account_code : Concern
    {
        private Because of = () =>
        {
            codeValues = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);
        };

        private It should_return_the_current_account_number_code_values = () =>
        {
            foreach (var cv in codeValues)
            {
                cv.ShouldNotBeNull();
                cv.Value.ShouldNotBeNull();
                cv.Value.ShouldNotBeEmpty();
            }
        };

        private static string environment = "JHMPROD";
        private static List<DsAccountCode> codeValues;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_adding_account_codes : Concern
    {
        private Because of = () =>
        {
            accountCodes.Add(new DsAccountCode { Active = true, DataSource = DsConstants.DataSource, Type = sampleType1, Value = "SALT" });
            accountCodes.Add(new DsAccountCode { Active = true, DataSource = DsConstants.DataSource, Type = sampleType2, Value = "1105" });

            accountService.AddAccountCodes(environment, 1420952184, accountCodes);

            codes = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);
        };

        private It should_persist_changes_to_DB = () =>
                                                      {
                                                          var check = 0;
                                                          foreach (var c in codes)
                                                          {
                                                              c.Active.ShouldEqual(true);
                                                              if (c.Type == sampleType1)
                                                                  check++;
                                                              if (c.Type == sampleType2)
                                                                  check++;
                                                          }
                                                          check.ShouldEqual(2);
                                                      };

        private static string environment = "JHMPROD";
        private static List<DsAccountCode> accountCodes = new List<DsAccountCode>();
        private static List<DsAccountCode> codes;
        private static string sampleType1 = "007";
        private static string sampleType2 = "DRAW";
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_updating_account_codes : Concern
    {
        private Because of = () =>
        {
            codes = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);

            foreach (var c in codes)
            {
                c.DataSource = "DDD";
            }

            accountService.UpdateAccountCodes(environment, accountNumber: 1420952184, accountCodes: codes);

            codes = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);
        };

        private It should_persist_changes_to_DB = () =>
        {
            foreach (var c in codes)
            {
                c.Active.ShouldEqual(true);
                c.DataSource.ShouldEqual("DDD");
            }
        };

        private static string environment = "JHMPROD";
        private static List<DsAccountCode> accountCodes = new List<DsAccountCode>();
        private static List<DsAccountCode> codes;
    }

    [Subject(typeof(DsAccountTransactionService))]
    public class When_deleting_account_codes : Concern
    {
        private Because of = () =>
        {
            codes = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);

            foreach (var c in codes)
            {
                if (c.RecordId % 2 == 0)
                    c.RecordId = 0;
            }

            accountService.DeleteAccountCodes(environment, accountNumber: 1420952184, accountCodes: codes);

            codes = accountService.GetAccountCodes(environment, accountNumber: 1420952184, activeOnly: true);
        };

        private It should_persist_changes_to_DB = () =>
        {
            foreach (var c in codes)
            {
                if (c.RecordId % 2 == 0)
                    c.RecordId.ShouldBeGreaterThan(0);
            }
        };

        private static string environment = "JHMPROD";
        private static List<DsAccountCode> accountCodes = new List<DsAccountCode>();
        private static List<DsAccountCode> codes;
    }

}