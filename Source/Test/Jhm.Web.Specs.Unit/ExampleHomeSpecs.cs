using System;
using System.Web.Mvc;
using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.ExampleHomeSpecs
{
    public class concern : Unit_BaseControllerSpecBase<HomeController, IServiceCollection>
    {

    }

    [Subject(typeof(HomeController))]
    public class when_Home_page_is_loaded : concern
    {
        Because of = () => result = sut.Index(null) as ViewResult;

        It should_have_a_message_that_reads_Welcome_To_Asp_Net_MVC;

        static ViewResult result;

    }

    [Subject(typeof(HomeController))]
    public class when_About_Us_page_is_loaded : concern
    {
        Because of = () => result = sut.About(null,String.Empty) as ViewResult;

        It should_have_have_a_the_title_of_the_page_should_say_About_Us;

        static ViewResult result;

    }
}