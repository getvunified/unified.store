﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Web.Core.Models;
using Jhm.Web.mSpec;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Service.Modules.Account;
using Machine.Specifications;
using Machine.Specifications.DevelopWithPassion.Rhino;
using Rhino.Mocks;

namespace Jhm.Web.Specs.Unit.Modules.Account.AccountServiceSpecs
{

    #region Concerns

    public class Concern : Unit_IServiceSpecBase<AccountService>
    {
        Establish context = () =>
                                {
                                    userRepository = the_dependency<IUserRepository>();
                                    roleRepository = the_dependency<IRoleRepository>();
                                };

        protected static IUserRepository userRepository;
        protected static IRoleRepository roleRepository;
    }
    public class With_User : Concern
    {
        Establish context = () =>
                                {
                                    user = an<User>();
                                    userRepository.Stub(x => x.GetUser(user.Username,applicationName)).IgnoreArguments().Return(user);
                                    userRepository.Stub(x => x.GetUser(user.Id, applicationName)).IgnoreArguments().Return(user);
                                    user.Stub(x => x.IsTransient()).Return(false);
                                    username = "username";
                                    applicationName = "applicationName";
                                };


        protected static User user;
        protected static string username;
        protected static string applicationName;
    }
    public class With_Role : Concern
    {
        Establish context = () =>
                                {
                                    role = an<Role>();
                                    roleRepository.Stub(x => x.GetRole(null, null)).IgnoreArguments().Return(role);

                                    roleName = "roleName";
                                    applicationName = "applicationName";
                                };

        protected static Role role;
        protected static string roleName;
        protected static string applicationName;
    }
    public class With_Profile : Concern
    {
        Establish context = () =>
                                {
                                    userName = "userName";
                                    isAnonymous = false;
                                    applicationName = "applicationName";

                                    profile = an<Profile>();
                                };

        protected static string userName;
        protected static bool isAnonymous;
        protected static string applicationName;
        protected static Profile profile;
    }
    public class With_User_And_Profile : Concern
    {
        Establish context = () =>
                                {
                                    userName = "userName";
                                    isAnonymous = false;
                                    applicationName = "applicationName";

                                    profile = an<Profile>();
                                    user = new User(Guid.NewGuid()) { ApplicationName = applicationName, Username = userName };
                                    user.AddProfile(profile);
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(user);
                                    userRepository.Stub(x => x.GetUser(Guid.NewGuid(), null)).IgnoreArguments().Return(user);
                                    
                                    
                                };

        protected static string userName;
        protected static bool isAnonymous;
        protected static string applicationName;
        protected static User user;
        protected static Profile profile;
        protected static Profile p;

    }
    public class With_User_And_Role : Concern
    {
        Establish context = () =>
                                {
                                    user = an<User>();
                                    role = an<Role>();
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(user);
                                    roleRepository.Stub(x => x.GetRole(null, null)).IgnoreArguments().Return(role);

                                    userName = "userName";
                                    roleName = "roleName";
                                    applicationName = "applicationName";
                                };


        protected static User user;
        protected static Role role;

        protected static string userName;
        protected static string roleName;
        protected static string applicationName;
    }

    #endregion


    #region Role Specific Specs

    [Subject(typeof(AccountService))]
    public class When_searching_for_role_with_role_name_and_application_and_finds_a_role : With_Role
    {
        Establish context = () => role.Stub(x => x.IsTransient()).Return(false);

        Because of = () => r = sut.GetRole(roleName, applicationName);

        It Should_call_the_Role_Repository_with_Role_Name_and_Application_Name = () => roleRepository.received(x => x.GetRole(roleName, applicationName));
        It should_return_role = () =>
                                    {
                                        r.ShouldNotBeNull();
                                        r.IsTransient().ShouldBeFalse();
                                    };
        private static Role r;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_role_with_role_name_and_application_and_does_NOT_find_a_role : Concern
    {

        Establish context = () =>
                                {
                                    roleName = "roleName";
                                    applicationName = "applicationName";
                                    roleRepository.Stub(x => x.GetRole(null, null)).IgnoreArguments().Return(null);

                                };

        Because of = () => { r = sut.GetRole(roleName, applicationName); };

        It Should_call_the_Role_Repository_with_Role_Name_and_Application_Name = () => roleRepository.received(x => x.GetRole(roleName, applicationName));
        It should_return_a_transient_role = () =>
                                                {
                                                    r.ShouldNotBeNull();
                                                    r.IsTransient().ShouldBeTrue();
                                                };


        private static string roleName;
        private static string applicationName;
        private static Role r;
    }

    [Subject(typeof(AccountService))]
    public class When_adding_users_to_roles : With_User_And_Role
    {
        Establish context = () =>
                                {
                                    users = new string[] { userName };
                                    roles = new string[] { roleName };
                                };

        Because of = () => sut.AddUsersToRoles(users, roles, applicationName);

        It should_search_for_the_user_with_username_and_application_name = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_return_a_transient_user = () =>
                                                {
                                                    user.ShouldNotBeNull();
                                                    user.IsTransient().ShouldBeFalse();
                                                };
        It should_search_for_a_role_with_roleName_and_applicationName = () => roleRepository.received(x => x.GetRole(roleName, applicationName));
        It should_add_the_role_to_the_user = () => user.received(x => x.AddRole(role));
        It should_save_the_user = () => userRepository.received(x => x.Save(user));

        private static string[] users;
        private static string[] roles;
    }

    [Subject(typeof(AccountService))]
    public class When_Creating_A_Role : With_Role
    {
        Establish context = () =>
                                {
                                    r = new Role();
                                    r.ApplicationName = applicationName;
                                    r.RoleName = roleName;
                                };
        Because of = () => sut.CreateRole(r);

        It should_save_the_role_with_the_roleName_and_ApplicationName = () => roleRepository.received(x => x.Save(r));
        private static Role r;
    }

    [Subject(typeof(AccountService))]
    public class When_Deleting_A_Role : With_Role
    {
        Establish context = () =>
                                {
                                    r = new Role();
                                    r.ApplicationName = applicationName;
                                    r.RoleName = roleName;
                                };
        Because of = () => sut.DeleteRole(r);

        It should_delete_the_role_with_the_roleName_and_ApplicationName = () => roleRepository.received(x => x.Delete(r));
        private static Role r;
    }


    [Subject(typeof(AccountService))]
    public class When_getting_all_roles_with_applicationName : With_Role
    {
        Because of = () => sut.GetAllRoles(applicationName);

        It should_get_a_roles_from_repository_for_that_applicationName = () => roleRepository.received(x => x.GetAllRoles(applicationName));
    }

    [Subject(typeof(AccountService))]
    public class when_removing_user_from_roles : Concern
    {
        Establish context = () =>
                                {
                                    applicationName = "applicationName";
                                    userName = "userName";
                                    roleName = "roleName";

                                    myUser = new User(Guid.NewGuid()) { ApplicationName = applicationName, Username = userName, CreationDate = DateTime.Now};
                                    myRole = new Role() {ApplicationName = applicationName, RoleName = roleName};
                                    myUser.AddRole(myRole);
                                    users = new string[] { userName };
                                    roles = new string[] { roleName };
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(myUser); //Overwrite return above
                                    roleRepository.Stub(x => x.GetRole(null, null)).IgnoreArguments().Return(myRole); //Overwrite return above
                                    shouldNotContainRoles = myUser.Roles.ToArray();
                                };


        Because of = () => sut.RemoveUsersFromRoles(users, roles, applicationName);

        It should_get_a_roles_from_repository_for_that_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));

        It should_remove_role_from_user = () =>
                                              {
                                                  myUser.Roles.ShouldNotContain(shouldNotContainRoles);
                                              };
        It should_save_the_user_after_removing_the_roles = () => userRepository.Expect(x => x.Save(myUser));

        private static string[] users;
        private static string[] roles;
        private static User myUser;
        private static Role myRole;
        private static Role[] shouldNotContainRoles;
        private static string applicationName;
        private static string userName;
        private static string roleName;
    }

    [Subject(typeof(AccountService))]
    public class when_removing_user_from_roles_and_user_is_not_found : Concern
    {
        Establish context = () =>
        {
            applicationName = "applicationName";
            userName = "userName";
            roleName = "roleName";

            myUser = new User() { ApplicationName = applicationName, Username = userName, CreationDate = DateTime.Now }; // No ID to make sure User Is Transient
            myRole = new Role() { ApplicationName = applicationName, RoleName = roleName };
            myUser.AddRole(myRole);
            users = new string[] { userName };
            roles = new string[] { roleName };
            userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(myUser); //Overwrite return above
            roleRepository.Stub(x => x.GetRole(null, null)).IgnoreArguments().Return(myRole); //Overwrite return above
            shouldNotContainRoles = myUser.Roles.ToArray();
        };


        Because of = () => sut.RemoveUsersFromRoles(users, roles, applicationName);

        It should_get_a_roles_from_repository_for_that_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_check_and_continue_if_user_is_not_found = () => myUser.IsTransient();

        private static string[] users;
        private static string[] roles;
        private static User myUser;
        private static Role myRole;
        private static Role[] shouldNotContainRoles;
        private static string applicationName;
        private static string userName;
        private static string roleName;
    }

    #endregion


    #region Profile Specific Specs

    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_username_isAnonymous_and_applicationName_And_Finds_a_profile : With_User_And_Profile
    {

        Because of = () => { p = sut.GetProfile(user.Username, isAnonymous, applicationName); };

        It should_get_user_with_username_and_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_get_an_user_that_is__NOT_transient = () => user.IsTransient().ShouldBeFalse();
        It should_return_a_valid_profile = () => p.ShouldBeAn<Profile>();
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_username_isAnonymous_and_applicationName_and_does_NOT_find_a_profile : Concern
    {

        Establish context = () =>
                                {
                                    userName = "userName";
                                    isAnonymous = false;
                                    applicationName = "applicationName";

                                    myUser = new User(Guid.NewGuid()) { ApplicationName = applicationName, Username = userName };
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(myUser);


                                };
        Because of = () => { p = sut.GetProfile(myUser.Username, isAnonymous, applicationName); };

        It should_get_user_with_username_and_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_get_an_user_that_is__NOT_transient = () => myUser.IsTransient().ShouldBeFalse();
        It should_return_a_new_transient_profile = () => p.IsTransient().ShouldBeTrue();

        private static string userName;
        private static bool isAnonymous;
        private static string applicationName;
        private static User myUser;
        private static Profile myProfile;
        private static Profile p;
    }


    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_username_isAnonymous_and_applicationName_and_does_NOT_find_the_user : Concern
    {

        Establish context = () =>
                                {
                                    userName = "userName";
                                    isAnonymous = false;
                                    applicationName = "applicationName";

                                    myUser = new User(Guid.NewGuid()) { ApplicationName = applicationName, Username = userName };
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(null);


                                };
        Because of = () => { p = sut.GetProfile(myUser.Username, isAnonymous, applicationName); };

        It should_get_user_with_username_and_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_return_a_new_transient_profile = () => p.IsTransient().ShouldBeTrue();

        private static string userName;
        private static bool isAnonymous;
        private static string applicationName;
        private static User myUser;
        private static Profile myProfile;
        private static Profile p;
    }


    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_username_and_applicationName_And_Finds_a_profile : With_User_And_Profile
    {

        Because of = () => { p = sut.GetProfile(user.Username, applicationName); };

        It should_get_user_with_username_and_applicationName = () => userRepository.received(x => x.GetUser(userName, applicationName));
        It should_get_an_user_that_is__NOT_transient = () => user.IsTransient().ShouldBeFalse();

        It should_return_a_valid_profile = () => p.ShouldBeAn<Profile>();
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_user_ID_and_applicationName_And_Finds_a_profile : With_User_And_Profile
    {

        Because of = () => { p = sut.GetProfile(user.Id, applicationName); };


        It should_return_a_valid_profile = () => p.ShouldBeAn<Profile>();
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_profile_with_user_ID_and_applicationName_does_NOT_find_a_profile : Concern
    {
        Establish context = () =>
                                {
                                    applicationName = "applicationName";
                                    userName = "userName";
                                    isAnonymous = false;
                                    myUser = new User(Guid.NewGuid()) { ApplicationName = applicationName, Username = userName };
                                    userRepository.Stub(x => x.GetUser(Guid.NewGuid(), applicationName)).IgnoreArguments().Return(null);
                                    
                                };
        Because of = () => { p = sut.GetProfile(myUser.Id, applicationName); };

        It should_return_a_valid_profile = () =>
                                               {
                                                   p.ShouldBeAn<Profile>();
                                                   p.IsTransient().ShouldBeTrue();
                                               };
        
        private static string applicationName;
        private static string userName;
        private static User myUser;
        private static Profile p;
        private static bool isAnonymous;
    }

    




    [Subject(typeof(AccountService))]
    public class When_Getting_Profiles_with_user_Inactive_Since_Date_and_isAnonymous_and_applicationName_and_Finds_Profile_or_profiles : With_User_And_Profile
    {
        Establish context = () =>
        {
            userInactiveSinceDate = DateTime.Now;
            profiles = new List<Profile> { profile };
            userRepository.Stub(x => x.GetProfiles(userInactiveSinceDate, isAnonymous, applicationName)).IgnoreArguments().Return(profiles);
        };

        Because of = () => myProfiles = sut.GetProfiles(userInactiveSinceDate, isAnonymous, applicationName);

        It should_get_a_list_of_profiles = () =>
        {
            myProfiles.ShouldNotBeEmpty();
            myProfiles.ShouldContain(profile);
        };
        private static IList<Profile> profiles;
        private static DateTime userInactiveSinceDate;
        private static IList<Profile> myProfiles;
    }


    [Subject(typeof(AccountService))]
    public class When_Getting_Profiles_with_user_Inactive_Since_Date_and_isAnonymous_and_applicationName_and_does_NOT_Find_Profile_or_profiles : With_User_And_Profile
    {
        Establish context = () =>
        {
            userInactiveSinceDate = DateTime.Now;
        };

        Because of = () => myProfiles = sut.GetProfiles(userInactiveSinceDate, isAnonymous, applicationName);

        It should_get_an_empty_list_of_profiles = () => myProfiles.ShouldBeEmpty();

        private static IList<Profile> profiles;
        private static DateTime userInactiveSinceDate;
        private static IList<Profile> myProfiles;
    }


    [Subject(typeof(AccountService))]
    public class When_Getting_Profiles_with_applicationName_and_Finds_Profile_or_profiles : With_User_And_Profile
    {
        Establish context = () =>
        {
            userInactiveSinceDate = DateTime.Now;
            profiles = new List<Profile> { profile };
            userRepository.Stub(x => x.GetProfiles(applicationName)).IgnoreArguments().Return(profiles);
        };

        Because of = () => myProfiles = sut.GetProfiles(applicationName);

        It should_get_a_list_of_profiles = () =>
        {
            myProfiles.ShouldNotBeEmpty();
            myProfiles.ShouldContain(profile);
        };
        private static IList<Profile> profiles;
        private static DateTime userInactiveSinceDate;
        private static IList<Profile> myProfiles;
    }


    [Subject(typeof(AccountService))]
    public class When_Getting_Profiles_with_applicationName_and_does_NOT_Find_Profile_or_profiles : With_User_And_Profile
    {
        Establish context = () =>
        {
            userInactiveSinceDate = DateTime.Now;
        };

        Because of = () => myProfiles = sut.GetProfiles(applicationName);

        It should_get_an_empty_list_of_profiles = () => myProfiles.ShouldBeEmpty();

        private static IList<Profile> profiles;
        private static DateTime userInactiveSinceDate;
        private static IList<Profile> myProfiles;
    }


    #endregion


    #region User Specific Specs

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_username_and_application_name_and_finds_a_user : With_User
    {
        Establish context = () => user.Stub(x => x.IsTransient()).Return(false);

        Because of = () => u = sut.GetUser(username, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUser(username, applicationName));
        It should_return_user = () =>
                                    {
                                        u.ShouldNotBeNull();
                                        u.IsTransient().ShouldBeFalse();
                                    };

        private static User u;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_username_and_application_name_and_does_NOT_find_a_user : Concern
    {
        Establish context = () =>
                                {
                                    username = "username";
                                    applicationName = "applicationName";
                                    userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(null);
                                };


        Because of = () => u = sut.GetUser(username, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUser(username, applicationName));
        It should_return_a_transient_user = () =>
                                               {
                                                   u.ShouldNotBeNull();
                                                   u.IsTransient().ShouldBeTrue();
                                               };

        private static string username;
        private static string applicationName;
        private static User u;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_userId_and_application_name_and_finds_a_user : With_User
    {
        Establish context = () =>
                                {
                                    
                                };
        Because of = () => u = sut.GetUser(user.Id, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUser(user.Id, applicationName));
        It should_return_user = () =>
        {
            u.ShouldNotBeNull();
            u.IsTransient().ShouldBeFalse();
        };

        private static User u;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_userId_and_application_name_and_does_NOT_find_a_user : Concern
    {
        Establish context = () =>
        {
            username = "username";
            applicationName = "applicationName";
            userId = Guid.NewGuid();
            
            userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(null);
        };


        Because of = () => u = sut.GetUser(userId, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUser(userId, applicationName));
        It should_return_a_transient_user = () =>
        {
            u.ShouldNotBeNull();
            u.IsTransient().ShouldBeTrue();
        };

        private static string username;
        private static string applicationName;
        private static User u;
        private static Guid userId;
    }

    [Subject(typeof(AccountService))]
    public class When_Getting_List_of_Users_with_ApplicationName : With_User
    {
        Establish context = () =>
                                {
                                    myUsers = new List<User>() {user};
                                    userRepository.Stub(x => x.GetUsers(applicationName)).IgnoreArguments().Return(myUsers);
                                };

        Because of = () => uList = sut.GetUsers(applicationName);

        It should_call_Get_Users_On_Repository = () => userRepository.received(x => x.GetUsers(applicationName));
        It should_return_list_of_users = () => 
        { 
            uList.ShouldNotBeEmpty();
            uList.ShouldContain(user);
            
        };
        
        private static IList<User> myUsers;
        private static IList<User> uList;
    }

    [Subject(typeof(AccountService))]
    public class When_Getting_List_of_Users_with_ApplicationName_and_No_User_Found : With_User
    {
        Establish context = () => userRepository.Stub(x => x.GetUsers(applicationName)).IgnoreArguments().Return(null);

        Because of = () => uList = sut.GetUsers(applicationName);

        It should_call_Get_Users_On_Repository = () => userRepository.received(x => x.GetUsers(applicationName));
        It should_return_list_of_users = () => uList.ShouldBeEmpty();

        private static IList<User> uList;
    }

    [Subject(typeof(AccountService))]
    public class When_Getting_List_of_Users_with_emailToMatch_userNameToMatch_and_ApplicationName : With_User
    {
        Establish context = () =>
                                {
                                    emailToMatch = "emailToMatch";
                                    userNameToMatch = "usernameToMatch";
                                    myUsers = new List<User>() { user };
                                    userRepository.Stub(x => x.GetUsers(emailToMatch, userNameToMatch, applicationName)).IgnoreArguments().Return(myUsers);
        };

        Because of = () => uList = sut.GetUsers(emailToMatch, userNameToMatch, applicationName);

        It should_call_Get_Users_On_Repository = () => userRepository.received(x => x.GetUsers(emailToMatch, userNameToMatch, applicationName));
        It should_return_list_of_users = () =>
        {
            uList.ShouldNotBeEmpty();
            uList.ShouldContain(user);

        };

        private static IList<User> myUsers;
        private static IList<User> uList;
        private static string userNameToMatch;
        private static string emailToMatch;
    }

    [Subject(typeof(AccountService))]
    public class When_Getting_List_of_Users_with_emailToMatch_userNameToMatch_and_ApplicationName_and_No_User_Found : With_User
    {
        Establish context = () =>
        {
            emailToMatch = "emailToMatch";
            userNameToMatch = "usernameToMatch";
            userRepository.Stub(x => x.GetUsers(emailToMatch, userNameToMatch, applicationName)).IgnoreArguments().Return(null);
        };

        Because of = () => uList = sut.GetUsers(emailToMatch, userNameToMatch, applicationName);

        It should_call_Get_Users_On_Repository = () => userRepository.received(x => x.GetUsers(emailToMatch, userNameToMatch, applicationName));
        It should_return_list_of_users = () => uList.ShouldBeEmpty();

        private static IList<User> uList;
        private static string userNameToMatch;
        private static string emailToMatch;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_email_and_application_name_and_finds_a_user : With_User
    {
        Establish context = () =>
                                {
                                    email = "email@email.com";
                                    user.Stub(x => x.IsTransient()).Return(false);
                                    userRepository.Stub(x => x.GetUserByEmail(email, applicationName)).IgnoreArguments().Return(user);
                                };

        Because of = () => u = sut.GetUserByEmail(email, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUserByEmail(email, applicationName));
        It should_return_user = () =>
        {
            u.ShouldNotBeNull();
            u.IsTransient().ShouldBeFalse();
        };

        private static User u;
        private static string email;
    }

    [Subject(typeof(AccountService))]
    public class When_searching_for_user_with_email_and_application_name_and_does_NOT_find_a_user : Concern
    {
        Establish context = () =>
        {
            applicationName = "applicationName";
            email = "email@email.com";
            userRepository.Stub(x => x.GetUserByEmail(email, applicationName)).IgnoreArguments().Return(null);
            userRepository.Stub(x => x.GetUser(null, null)).IgnoreArguments().Return(null);
        };


        Because of = () => u = sut.GetUserByEmail(email, applicationName);

        It should_call_the_repository_to_with_username_and_application_name = () => userRepository.received(x => x.GetUserByEmail(email, applicationName));
        It should_return_a_transient_user = () =>
        {
            u.ShouldNotBeNull();
            u.IsTransient().ShouldBeTrue();
        };

        private static string applicationName;
        private static User u;
        private static string email;
    }

    [Subject(typeof(AccountService))]
    public class When_Getting_Count_of_Users_with_ApplicationName : With_User
    {
        Establish context = () =>
                                {
                                    userCount = 10;
                                    userRepository.Stub(x=>x.GetAllUserCount(applicationName)).IgnoreArguments().Return(userCount);
                                };
        
        Because of = () => numOfUsers = sut.GetAllUserCount(applicationName);

        It should_call_GetAllUserCount_from_the_repository = () => userRepository.received(x => x.GetAllUserCount(applicationName));
        It should_return_the_correct_number_of_users = () => numOfUsers.ShouldEqual(userCount);
        
        private static int userCount;
        private static int numOfUsers;
    }

    [Subject(typeof(AccountService))]
    public class When_getting_the_number_of_users_online_with_comparetime_and_applicationName : With_User
    {
        Establish context = () =>
                                {
                                    compareTime = DateTime.Now;
                                    onlineUsers = 10;
                                    userRepository.Stub(x => x.GetNumberOfUsersOnline(compareTime, applicationName)).IgnoreArguments().Return(onlineUsers);
                                };

        Because of = () => { onlineUserCount = sut.GetNumberOfUsersOnline(compareTime, applicationName); };

        It should_call_the_repository_for_the_number_of_users_online = () => userRepository.received(x => x.GetNumberOfUsersOnline(compareTime, applicationName));
        It should_return_the_correct_number_of_users_online = () => onlineUserCount.ShouldEqual(onlineUsers);
        
        private static DateTime compareTime;
        private static int onlineUsers;
        private static int onlineUserCount;
    }

    [Subject(typeof(AccountService))]
    public class When_saving_a_user : With_User
    {
        Because of = () => sut.SaveUser(user, false);

        It should_call_save_on_userRepository = () => userRepository.received(x => x.Save(user));
    }

    [Subject(typeof(AccountService))]
    public class When_deleting_a_user : With_User
    {
        Because of = () => sut.DeleteUser(user);

        It should_call_save_on_userRepository = () => userRepository.received(x => x.Delete(user));
    }

    #endregion

}
