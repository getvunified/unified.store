using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.WishListSpecs
{
    public class when_viewing_a_Product_page_and_the_Product_HAS_STOCKITEMS_in_stock
    {
        It should_allow_you_to_add_to_Wish_List;
    }

    public class when_viewing_a_Product_page_and_the_Product_DOES_NOT_HAVE_STOCKITEMS_in_stock
    {
        It should_allow_you_to_add_to_Wish_List;
        It should_allow_you_to_signup_for_notification_of_when_the_product_is_in_stock;
    }
}