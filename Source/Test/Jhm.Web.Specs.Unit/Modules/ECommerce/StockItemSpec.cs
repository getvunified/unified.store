using System;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.StockItemSpec
{
    [Subject(typeof(StockItem))]
    public class When_creating_a_stockitem
    {
        Establish c = () => { releaseDate = DateTime.Now; };

        Because of = () =>
                         {
                             stockItem = new StockItem(mediaType, quantityInStock, allowBackorder, basePrice, salePrice, inHouse, warehouseCode);
                             stockItem.SetReleaseDate(releaseDate);
                             stockItem.EnableAllowPreorder();
                         };

        It should_have_a_mediatype = () => stockItem.MediaType.ShouldEqual(mediaType);
        It should_have_a_quantity_in_stock = () => stockItem.QuantityInStock.ShouldEqual(quantityInStock);
        It should_indicate_if_backorder_is_allowed = () => stockItem.AllowBackorder.ShouldEqual(allowBackorder);
        It should_have_a_baseprice = () => stockItem.BasePrice.ShouldEqual(basePrice);
        It should_have_a_saleprice = () => stockItem.SalePrice.ShouldEqual(salePrice);
        It should_indicate_if_it_is_inhouse = () => stockItem.IsInHouse.ShouldEqual(inHouse);
        It should_have_a_release_date = () => stockItem.ReleaseDate.ShouldEqual(releaseDate);
        It should_indicate_if_preorder_is_allowed = () => stockItem.AllowPreorder.ShouldBeTrue();

        private static StockItem stockItem;
        private static MediaType mediaType = MediaType.DVD;
        private static int quantityInStock = 10;
        private static bool allowBackorder = true;
        private static decimal basePrice = 10;
        private static decimal salePrice = 10;
        private static bool inHouse = false;
        private static DateTime releaseDate;
        private static string warehouseCode = "5";
    }

    public class when_creating_a_Stockitem_with_a_product_and_asking_the_stockitem_for_the_product_sku
    {
        Establish c = () =>
                          {
                              productCodeWithoutSuffix = "TESTCode";
                              var product = new Product(productCodeWithoutSuffix, new Category[] { });
                              stockItem =
                                  new StockItem(MediaType.OnDemand, 0, true, 10, 10, true, "10").AddProduct(product);

                          };

        Because of = () => { productCode = stockItem.ProductCode; };

        It should_remove_the_trailing_MediaType_suffix = () => productCode.ShouldEqual(productCodeWithoutSuffix);
        private static string productCodeWithoutSuffix;
        private static string productCode;
        private static StockItem stockItem;
    }
}