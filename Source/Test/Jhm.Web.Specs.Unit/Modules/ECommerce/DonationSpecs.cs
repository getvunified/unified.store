using System;
using System.Collections.Generic;
using System.Globalization;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Machine.Specifications;
using getv.donorstudio.core.eCommerce;
using Donation = Jhm.Web.Core.Models.Donation;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.DonationSpecs
{
    public class with_new_donation
    {
        Establish c = () =>
        {
            culturalInfo = new CultureInfo("en-US");
            donationCode = "Donation Code";
            title = "title";
            description = "description";
            body = "body";
            listViewImageUrl = "listViewImage";
            detailViewImageUrl = "detailViewImage";
            formViewImageUrl = "formViewImageUrl";
            pdfVersion = "pdfLinkUrl";
            donation = new Donation(donationCode: donationCode, title: title, description: description, body: body, 
                                    listViewImageUrl: listViewImageUrl, detailViewImageUrl: detailViewImageUrl, formViewImageUrl: formViewImageUrl, 
                                    pdfVersion: pdfVersion);
        };

        protected static string donationCode;
        protected static string title;
        protected static string body;
        protected static string pdfVersion;
        protected static Donation donation;
        protected static string description;
        protected static string listViewImageUrl;
        protected static string detailViewImageUrl;
        protected static string formViewImageUrl;
        private static CultureInfo culturalInfo;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_donation
    {
        Establish c = () =>
                          {
                              donationCode = "Donation Code";
                              title = "title";
                              description = "description";
                              body = "body";
                              listViewImageUrl = "listViewImage";
                              detailViewImageUrl = "detailViewImage";
                              formViewImageUrl = "formViewImageUrl";
                              pdfVersion = "pdfLinkUrl";
                          };
        Because of = () => donation = new Donation(donationCode: donationCode, title: title, description: description, body: body,
                                                   listViewImageUrl: listViewImageUrl, detailViewImageUrl: detailViewImageUrl, formViewImageUrl: formViewImageUrl,  
                                                   pdfVersion: pdfVersion);

        It should_contain_a_donation_code = () => donation.DonationCode.ShouldEqual(donationCode);
        It should_contain_a_title = () => donation.Title.ShouldEqual(title);
        It should_contain_a_description = () => donation.Description.ShouldEqual(description);
        It should_contain_a_body = () => donation.Body.ShouldEqual(body);
        It should_contain_a_listViewImageUrl = () => donation.ListViewImageUrl.ShouldEqual(listViewImageUrl);
        It should_contain_a_detailViewImageUrl = () => donation.DetailViewImageUrl.ShouldEqual(detailViewImageUrl);
        It should_contain_a_formViewImageUrl = () => donation.FormViewImageUrl.ShouldEqual(formViewImageUrl);
        It should_contain_a_pdf_version_link_url = () => donation.PdfVersion.ShouldEqual(pdfVersion);
        

        private static string donationCode;
        private static string title;
        private static string body;
        private static string pdfVersion;
        private static Donation donation;
        private static string description;
        protected static string listViewImageUrl;
        protected static string detailViewImageUrl;
        protected static string formViewImageUrl;
    }

    [Subject(typeof(DonationOption))]
    public class When_creating_a_DonationOption
    {
        Establish context = () =>
                                {
                                    var product = new Product("TEST123", new[] { Category.Music });
                                    stockItems = new List<StockItem>()
                                               {
                                                   new StockItem(MediaType.OnDemand, 10, true, 10.00m, 10.00m, true, "10").AddProduct(product),
                                                   new StockItem(MediaType.Book, 10, true, 10.00m, 10.00m, true, "10").AddProduct(product),
                                                   new StockItem(MediaType.DVD, 10, true, 10.00m, 10.00m, true, "5").AddProduct(product),
                                               };

                                    id = Guid.NewGuid();
                                    sortOrder = 10;
                                    title = "title";
                                    description = "description";
                                    imageUrl = "imageUrl";
                                };

        Because of = () =>
                            {
                                option = new DonationOption(id);
                                option.SetTitle(title);
                                option.SetDescription(description);
                                option.SetImageUrl(imageUrl);
                                option.SetSortOrder(sortOrder);
                                option.AddStockItem(null, stockItems.ToArray());

                            };

        It should_have_an_id = () => option.Id.ShouldEqual(id);
        It should_contain_StockItems = () => option.StockItems.ShouldContainOnly(stockItems.ToArray());
        It should_have_a_SortOrder = () => option.SortOrder.ShouldEqual(sortOrder);
        It should_have_a_title = () => option.Title.ShouldEqual(title);
        It should_have_a_description = () => option.Description.ShouldEqual(description);
        It should_have_an_imageurl = () => option.ImageUrl.ShouldEqual(imageUrl);

        private static List<StockItem> stockItems;
        private static Guid id;
        private static DonationOption option;
        private static int sortOrder;
        private static string title;
        private static string description;
        private static string imageUrl;
    }

    [Subject(typeof(DonationOption))]
    public class When_creating_a_DonationOption_with_a_MINIMUM_shipping_amount_AND_minimum_shippingmethod_is_USPSGround
    {
        Establish context = () =>
        {
            viableShippingMethods = new List<ShippingMethod> { ShippingMethod.USPSGround, ShippingMethod.UPSRush, ShippingMethod.USPSInternational };
        };

        Because of = () =>
        {
            option = new DonationOption(Guid.NewGuid());
            option.SetMinimumShippingMethod(ShippingMethod.USPSGround);
        };

        It should_not_allow_the_STANDARD_shipping_method = () => option.GetAllowedShippingMethods().ShouldNotContain(ShippingMethod.USPSStandard);
        It should_allow_only_allow_uspsground_and_uspsinternational = () => option.GetAllowedShippingMethods().ShouldContainOnly(viableShippingMethods.ToArray());

        private static DonationOption option;
        private static List<ShippingMethod> viableShippingMethods;
    }


    [Subject(typeof(DonationOption))]
    public class when_creating_a_DonationOption_with_a_list_of_StockItems : with_new_donation
    {
        Establish c = () =>
        {
            var product = new Product("TEST123", new[] { Category.Music });
            stockItems = new List<StockItem>()
                                               {
                                                   new StockItem(MediaType.OnDemand, 10, true, 10.00m, 10.00m, true, "10").AddProduct(product),
                                                   new StockItem(MediaType.Book, 10, true, 10.00m, 10.00m, true, "10").AddProduct(product),
                                                   new StockItem(MediaType.DVD, 10, true, 10.00m, 10.00m, true, "5").AddProduct(product),
                                               };

            id = Guid.NewGuid();
            var donationOption = new DonationOption(id);
            donationOption.AddStockItem(null, stockItems.ToArray());
            donation.AddDonationOption(donationOption);
        };

        Because of = () => option = donation.DonationOptions.ToLIST().Find(x => x.Id == id);

        It should_contain_only_the_StockItems_added = () => option.StockItems.ShouldContainOnly(stockItems.ToArray());

        private static List<StockItem> stockItems;
        private static Guid id;
        private static DonationOption option;
    }

    [Subject(typeof(DonationOption))]
    public class When_creating_a_AnyAmountDonationOption_with_a_list_of_DonationFrequencies_and_you_choose_an_available_donation_option : with_new_donation
    {
        Establish context = () =>
                                {
                                    id = Guid.NewGuid();
                                    donationOption = new AnyAmountDonationOption(id);
                                    donationFrequency = DonationFrequency.YearlyResidual;
                                    donationFrequencyOptions = new List<DonationFrequency> { DonationFrequency.OneTime, DonationFrequency.MonthlyResidual, donationFrequency };
                                    donationOption.AddDonationFrequency(null, donationFrequencyOptions.ToArray());
                                    donation.AddDonationOption(donationOption);

                                    donationPrice = 20.00m;
                                };

        Because of = () => { donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(id, donationPrice, donationFrequency: donationFrequency)); };

        It should_contain_a_list_of_expected_DonationFrequencies = () => donationOption.DonationFrequencyOptions.ShouldContainOnly(donationFrequencyOptions.ToArray());
        It should_allow_you_to_set_the_amount_of_the_donation = () => donationCartItem.Price.ShouldEqual(donationPrice);
        It should_allow_you_to_set_the_frequency_of_the_donation = () => donationCartItem.DonationFrequency.ShouldEqual(donationFrequency);
        private static Guid id;
        private static DonationFrequency donationFrequency;
        private static DonationCartItem donationCartItem;
        private static decimal donationPrice;
        private static AnyAmountDonationOption donationOption;
        private static List<DonationFrequency> donationFrequencyOptions;
    }

    [Subject(typeof(DonationOption))]
    public class When_creating_a_AnyAmountDonationOption_with_a_list_of_StockitemChoices_and_you_choose_an_StockItem_choice : with_new_donation
    {
        Establish context = () =>
        {
            id = Guid.NewGuid();
            donationOption = new AnyAmountDonationOption(id);
            var product = new Product("code");
            stockItemChoiceId = Guid.NewGuid();
            stockItemChosen = new StockItem(stockItemChoiceId, MediaType.DVD, 10, true, 10m, 10m, true, "5").AddProduct(product);
            stockItemChoices = new List<StockItem> { stockItemChosen,
                                                     new StockItem(Guid.NewGuid(), MediaType.Book, 10, true, 10m, 10m, true, "10").AddProduct(product),
                                                     new StockItem(Guid.NewGuid(), MediaType.AudioFileDownload, 10, true, 10m, 10m, true, "10").AddProduct(product),
                                                     new StockItem(Guid.NewGuid(), MediaType.OnDemand, 10, true, 10m, 10m, true, "10").AddProduct(product),
            };
            donationOption.AddStockItemChoice(null, stockItemChoices.ToArray());
            donation.AddDonationOption(donationOption);

            donationPrice = 20.00m;
        };

        Because of = () => { donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(id, donationPrice, stockItemChoiceId: stockItemChoiceId)); };

        It should_contain_a_list_of_expected_StockItemChoices = () => donationOption.StockItemChoices.ShouldContainOnly(stockItemChoices.ToArray());
        It should_allow_you_to_set_the_amount_of_the_donation = () => donationCartItem.Price.ShouldEqual(donationPrice);
        It should_allow_you_to_set_the_chosen_stockitem = () => donationCartItem.StockItemChosen.ShouldEqual(stockItemChosen);
        private static Guid id;
        private static decimal donationPrice;
        private static AnyAmountDonationOption donationOption;
        private static List<StockItem> stockItemChoices;
        private static Guid stockItemChoiceId;
        private static DonationCartItem donationCartItem;
        private static StockItem stockItemChosen;
    }

    [Subject(typeof(DonationOption))]
    public class When_creating_a_AnyAmountDonationOption_with_a_list_of_DonationFrequencies_and_you_choose_an_UNavailable_DonationFrequency_option : with_new_donation
    {
        Establish context = () =>
        {
            id = Guid.NewGuid();
            donationOption = new AnyAmountDonationOption(id);
            donationFrequencyOptions = new List<DonationFrequency> { DonationFrequency.OneTime, DonationFrequency.MonthlyResidual, DonationFrequency.YearlyResidual };
            donationOption.AddDonationFrequency(null, donationFrequencyOptions.ToArray());
            donation.AddDonationOption(donationOption);

            donationPrice = 20.00m;
            donationFrequency = DonationFrequency.QuarterlyResidual; //set to force exception
        };

        Because of = () =>
        {
            try
            {
                DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(id, donationPrice, donationFrequency: donationFrequency));
            }
            catch (Exception e)
            {
                exception = e;
            }
        };

        It should_FAIL_when_you_try_to_set_the_frequency_of_the_donation = () =>
                                                                               {
                                                                                   exception.ShouldNotBeNull();
                                                                                   exception.ShouldBe(typeof(ArgumentException));
                                                                                   exception.Message.ShouldEqual("Donation Frequency [{0}] is unavailable.".FormatWith(donationFrequency.DisplayName));
                                                                               };
        private static Guid id;
        private static DonationFrequency donationFrequency;
        private static decimal donationPrice;
        private static AnyAmountDonationOption donationOption;
        private static List<DonationFrequency> donationFrequencyOptions;
        private static Exception exception;
    }

    [Subject(typeof(DonationOption))]
    public class when_creating_a_DonationCartItem_from_a_DonationOption_with_Certificate_ENABLED : with_new_donation
    {
        Establish c = () =>
        {
            certificateName = "Certificate Name";

            id = Guid.NewGuid();
            var donationOption = new DonationOption(id);
            donationOption.EnableRequireCertificateField();
            donation.AddDonationOption(donationOption);
        };

        Because of = () =>
                         {
                             donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.DonationCriteria(id, certificateName));
                         };


        It should_allow_you_to_enter_a_name_for_the_certificate = () => donationCartItem.CertificateName.ShouldEqual(certificateName);

        private static string certificateName;
        private static Guid id;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(DonationOption))]
    public class when_creating_a_DonationCartItem_from_a_DonationOption_with_Certificate_NOT_ENABLED : with_new_donation
    {
        Establish c = () =>
        {
            certificateName = "Certificate Name";

            id = Guid.NewGuid();
            var donationOption = new DonationOption(id);
            donationOption.EnableRequireCertificateField();
            donationOption.DisableOptionField();
            donation.AddDonationOption(donationOption);
        };

        Because of = () =>
        {
            try
            {
                donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.DonationCriteria(id, certificateName));
            }
            catch (Exception e)
            {
                exception = e;
            }
        };

        It should_ERROR_when_you_try_to_enter_a_name_for_the_certificate = () =>
                                                                                    {
                                                                                        exception.ShouldBe(typeof(ArgumentException));
                                                                                        exception.Message.ShouldEqual("Certificate IS NOT enabled.");
                                                                                    };

        private static Exception exception;
        private static string certificateName;
        private static Guid id;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_an_AnyAmountOption : with_new_donation
    {
        Establish c = () =>
                            {
                                choiceId = Guid.NewGuid();
                                anyAmountOption = new AnyAmountDonationOption(choiceId);
                                donation.AddDonationOption(anyAmountOption);

                                donationAmount = 100.00m;
                            };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, donationAmount));

        It should_contain_ONLY_an_AnyAmountOption = () => donation.DonationOptions.ShouldContainOnly(anyAmountOption);
        It should_allow_you_to_enter_in_ANY_AMOUNT_as_your_donation_amount = () => donationCartItem.Price.ShouldEqual(donationAmount);


        private static AnyAmountDonationOption anyAmountOption;
        private static decimal donationAmount;
        private static Guid choiceId;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_two_FixedAmountOptions : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            Guid Id1 = Guid.NewGuid();
            Guid Id2 = Guid.NewGuid();
            chosenDonationAmount = 100.00m;


            donation.AddDonationOption(new FixedAmountDonationOption(Id1, 25.00m));
            donation.AddDonationOption(new FixedAmountDonationOption(Id2, 25.00m));
            donation.AddDonationOption(new FixedAmountDonationOption(choiceId, chosenDonationAmount));

        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.DonationCriteria(choiceId));

        It should_ALLOW_you_to_choose_one_of_the_provided_donation_amounts = () => donationCartItem.Price.ShouldEqual(chosenDonationAmount);


        private static decimal chosenDonationAmount;
        private static Guid choiceId;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_two_FixedAmountOptions_and_one_AnyAmountOption_AND_you_choose_a_FixedAmountOption : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            Guid Id1 = Guid.NewGuid();
            Guid Id2 = Guid.NewGuid();
            chosenDonationAmount = 100.00m;


            donation.AddDonationOption(new FixedAmountDonationOption(Id1, 25.00m));
            donation.AddDonationOption(new FixedAmountDonationOption(choiceId, chosenDonationAmount));
            donation.AddDonationOption(new AnyAmountDonationOption(Id2));

        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.DonationCriteria(choiceId));

        It should_allow_you_to_choose_one_of_the_FixedDonationOptions = () => donationCartItem.Price.ShouldEqual(chosenDonationAmount);


        private static decimal chosenDonationAmount;
        private static Guid choiceId;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_two_FixedAmountOptions_and_one_AnyAmountOption_AND_you_choose_the_AnyAmountOption : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            Guid Id1 = Guid.NewGuid();
            Guid Id2 = Guid.NewGuid();
            chosenDonationAmount = 100.00m;


            donation.AddDonationOption(new FixedAmountDonationOption(Id1, 25.00m));
            donation.AddDonationOption(new FixedAmountDonationOption(Id2, 50.00m));
            donation.AddDonationOption(new AnyAmountDonationOption(choiceId));
        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, chosenDonationAmount));

        It should_allow_you_to_enter_in_ANY_AMOUNT_as_your_donation_amount = () => donationCartItem.Price.ShouldEqual(chosenDonationAmount);


        private static decimal chosenDonationAmount;
        private static Guid choiceId;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_FixedMultipleOfDonationOption : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            pricePerMultiple = 10.00m;
            multipleOf = 10;

            donation.AddDonationOption(new MultipleOfdonationOption(choiceId, pricePerMultiple));

        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.MultipleOfDonationCriteria(choiceId, multipleOf));

        It should_total_the_PricePerMultiple_times_the_MultipleOf = () => donationCartItem.Price.ShouldEqual(pricePerMultiple * multipleOf);

        private static decimal pricePerMultiple;
        private static Guid choiceId;
        private static int multipleOf;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_FixedMultipleOfDonationOption_and_Multiple_is_related_to_certificate : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            pricePerMultiple = 10.00m;
            multipleOf = 10;

            multipleOfdonationOption = new MultipleOfdonationOption(choiceId, pricePerMultiple);
            multipleOfdonationOption.EnableRequireCertificateField();
            multipleOfdonationOption.EnableCertificateRelatedMultiple();
            donation.AddDonationOption(multipleOfdonationOption);

        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.MultipleOfDonationCriteria(choiceId, multipleOf, certificateInfo));

        It should_total_the_PricePerMultiple_times_the_MultipleOf = () => donationCartItem.Price.ShouldEqual(pricePerMultiple * multipleOf);
        It should_require_the_certificate_to_be_filled_out = () => multipleOfdonationOption.OptionFieldIsRequired().ShouldBeTrue();
        It should_indicate_the_MULTIPULE_is_tied_to_the_certificate = () => multipleOfdonationOption.CertificateIsRelatedToMultiple.ShouldBeTrue();

        private static decimal pricePerMultiple;
        private static Guid choiceId;
        private static int multipleOf;
        private static DonationCartItem donationCartItem;
        private static string certificateInfo = "Name1, Name2, Name3";
        private static MultipleOfdonationOption multipleOfdonationOption ;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_AmountRangeDonationOption_with_VALID_amount : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            donationAmount = 30.00m;

            donation.AddDonationOption(new AmountRangeDonationOption(choiceId, 25.00m, 50.00m));
        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, donationAmount));

        It should_ALLOW_you_to_enter_in_any_Amount_within_the_defined_range = () => donationCartItem.Price.ShouldEqual(donationAmount);

        private static decimal donationAmount;
        private static Guid choiceId;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_AmountRangeDonationOption_with_INVALID_amount : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            donationAmount = 10.00m;
            minAmount = 25.00m;
            maxAmount = 50.00m;
            

            donation.AddDonationOption(new AmountRangeDonationOption(choiceId, minAmount, maxAmount));
        };

        Because of = () =>
                         {
                             try
                             {
                                 donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, donationAmount));
                             }
                             catch (Exception e)
                             {
                                 exception = e;
                             }
                         };

        It should_ERROR_when_amount_is_not_within_the_defined_range = () =>
                                                                          {
                                                                              exception.ShouldBe(typeof(DonationOptionException));
                                                                              exception.Message.ShouldEqual("Value must be between [C:{0}] and [C:{1}]".FormatWith(minAmount, maxAmount));
                                                                          };

        private static decimal donationAmount;
        private static Guid choiceId;
        private static Exception exception;
        private static decimal maxAmount;
        private static decimal minAmount;
        private static DonationCartItem donationCartItem;
        private static CultureInfo culturalInfo;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_MinimumAmountDonationOption_with_VALID_amount : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            donationAmount = 30.00m;
            minAmount = 25.00m;

            donation.AddDonationOption(new MinimumAmountDonationOption(choiceId, minAmount));
        };

        Because of = () => donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, donationAmount));

        It should_ALLOW_you_to_enter_in_any_Amount_above_the_minimum_amount = () => donationCartItem.Price.ShouldEqual(donationAmount);

        private static decimal donationAmount;
        private static Guid choiceId;
        private static Exception exception;
        private static decimal minAmount;
        private static DonationCartItem donationCartItem;
    }

    [Subject(typeof(Donation))]
    public class when_creating_a_DonationCartItem_from_a_Donation_with_a_MinimumAmountDonationOption_with_INVALID_amount : with_new_donation
    {
        Establish c = () =>
        {
            choiceId = Guid.NewGuid();
            donationAmount = 10.00m;
            minAmount = 25.00m;

            donation.AddDonationOption(new MinimumAmountDonationOption(choiceId, minAmount));
        };

        Because of = () =>
        {
            try
            {
                donationCartItem = DonationCartItemFactory.CreateFrom(donation, DonationCriteriaFactory.AmountDonationCriteria(choiceId, donationAmount));
            }
            catch (Exception e)
            {
                exception = e;
            }
        };

        It should_ERROR_when_amount_is_not_within_the_defined_range = () =>
        {
            exception.ShouldBe(typeof(ArgumentException));
            exception.Message.ShouldEqual("Value must be greater than or equal to [C:{0}].".FormatWith(minAmount));
        };

        private static decimal donationAmount;
        private static Guid choiceId;
        private static Exception exception;
        private static decimal minAmount;
        private static DonationCartItem donationCartItem;
    }

    

}