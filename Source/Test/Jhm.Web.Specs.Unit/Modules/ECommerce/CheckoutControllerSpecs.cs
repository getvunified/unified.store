using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.em3.Core;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.Service.Modules.ECommerce;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;
using Rhino.Mocks;
using getv.donorstudio.core.Global;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.CheckoutControllerSpecs
{
    public class concern<VR, RVD> : Unit_BaseControllerSpecBase<CheckoutController, IServiceCollection>
        where VR : ActionResult
        where RVD : CartViewModel
    {
        Establish c = () =>
        {
            catalogService = an<ICatalogService>();
            serviceUnderTest.Stub(x => x.CatalogService).Return(catalogService);

            cartService = an<ICartService>();
            serviceUnderTest.Stub(x => x.CartService).Return(cartService);

        };
        protected static ICatalogService catalogService;
        protected static VR result;
        protected static RVD resultViewDataModel;
        protected static ICartService cartService;

        #region Utility Methods

        protected static List<CartItem> GetStubICartItems(int returnNumber)
        {
            var returnList = new List<CartItem>();

            for (int i = 0; i < returnNumber; i++)
            {
                var stockItem = new StockItem(MediaType.Book, i, true, 99, 99, true, "10");
                stockItem.AddProduct(new Product("Code-{0}".FormatWith(i), new Category[] { Category.ChildrenAndTeens }));
                returnList.Add(stockItem);
            }

            return returnList;
        }
        protected static List<IProduct> GetStubIProducts(int returnNumber, string sku, MediaType mediaType)
        {
            var returnList = new List<IProduct>();
            for (int i = 0; i < returnNumber; i++)
            {
                var product = new Product(sku, new[] { Category.Music });
                product.AddStockItems(new StockItem(mediaType, 1, true, 99, 99, true, "10"));
                returnList.Add(product);
            }

            return returnList;
        }
        protected static Cart GetStubCart(IEnumerable<CartItem> cartItems)
        {
            var returnCart = Cart.NewEmpty();

            foreach (var cartItem in cartItems)
            {
                returnCart.AddItem(cartItem, 1);
            }

            return returnCart;
        }
        protected static List<MediaType> GetStubCategories(int numberOfCategories)
        {
            var returnList = new List<MediaType>();

            for (int i = 0; i < numberOfCategories; i++)
            {
                returnList.Add(MediaType.Book);
            }

            return returnList;
        }

        #endregion
    }

    [Subject(typeof(CatalogController))]
    public class when_viewing_the_index_page : concern<ViewResult, CheckoutOrderReviewViewModel>
    {
        Establish c = () =>
        {
            var product = new Product("B222", new[] { Category.BooksBiblesDevotionals });
            price = 99;
            var stockItem1 = new StockItem(MediaType.Book, 1, true, price, price, true, "10").AddProduct(product);
            var stockItem2 = new StockItem(MediaType.DVD, 1, true, price, price, true, "5").AddProduct(product);

            client = new Client {Cart = Cart.NewEmpty()};
            var quantity = 1;
            client.Cart.AddItem(stockItem1, quantity);
            client.Cart.AddItem(stockItem2, quantity);

            expectedCartItems = client.Cart.Items;


            company = Company.JHM_UnitedStates;
            expectedCountries = company.Countries;
            expectedStatesProvinces = company.StateProvinces;

            //stubs
            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);
            catalogService.Stub(x => x.GetCompany()).Return(company);

        };

        Because of = () =>
        {
            result = sut.Payment(client);
            resultViewDataModel = result.ViewData.ModelAs<CheckoutOrderReviewViewModel>();

        };

        It should_show_the_Index_view = () => result.ViewName.ShouldEqual("Payment");
        It should_display_a_READONLY_list_of_ICartItems_in_the_Shopping_Cart = () =>
        {
            resultViewDataModel.CartItems.ShouldBe(typeof(IEnumerable<CartLineItem>));
            resultViewDataModel.CartItems.ShouldContainOnly(expectedCartItems);
        };
        It should_display_a_READONLY_total_quantity_for_each_item_in_the_Shopping_Cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Quantity == 1).Count.ShouldEqual(2);
        It should_display_a_READONLY_line_item_price_for_each_item_in_the_cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Item.Price == price).Count.ShouldEqual(2);
        It should_display_a_READONLY_subtotal_for_the_cart = () => resultViewDataModel.CartSubTotal.ShouldEqual(price * 2);
        //It should_display_the_correct_Countries_based_on_the_company_where_the_order_is_being_placed = () => resultViewDataModel.Countries.ShouldContainOnly(expectedCountries);
        //It should_display_the_correct_States_or_Provinces_based_on_the_company_where_the_order_is_being_placed = () => resultViewDataModel.StatesProvinces.ShouldContainOnly(expectedStatesProvinces);

        private static IEnumerable<CartLineItem> expectedCartItems;
        private static Company company;
        private static IEnumerable<Country> expectedCountries;
        private static IEnumerable<State> expectedStatesProvinces;
        private static decimal price;
        private static Client client;
    }

    [Subject(typeof(CheckoutController))]
    public class when_viewing_the_My_Shopping_Cart_page : concern<ViewResult, CheckoutShoppingCartViewModel>
    {
        Establish c = () =>
        {
            var product = new Product("B222", new[] { Category.BooksBiblesDevotionals });
            price = 99;
            var stockItem1 = new StockItem(MediaType.Book, 1, true, price, price, true, "10").AddProduct(product);
            var stockItem2 = new StockItem(MediaType.DVD, 1, true, price, price, true, "5").AddProduct(product);


            client = new Client { Cart = Cart.NewEmpty() };
            var quantity = 1;
            client.Cart.AddItem(stockItem1, quantity);
            client.Cart.AddItem(stockItem2, quantity);

            expectedCartItems = client.Cart.Items.ToLIST();


            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);

        };

        Because of = () =>
        {
            result = sut.Cart(client);
            resultViewDataModel = result.ViewData.ModelAs<CheckoutShoppingCartViewModel>();
        };
        It should_show_the_ShoppingCart_view = () => result.ViewName.ShouldEqual("Cart");
        It should_display_a_list_of_ICartItems_in_the_Shopping_Cart = () => resultViewDataModel.CartItems.ShouldContainOnly(expectedCartItems.ToArray());
        It should_display_a_total_quantity_for_each_item_in_the_Shopping_Cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Quantity == 1).Count.ShouldEqual(2);
        It should_display_a_line_item_price_for_each_item_in_the_cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Item.Price == price).Count.ShouldEqual(2);
        It should_display_a_subtotal_for_the_cart = () => resultViewDataModel.CartSubTotal.ShouldEqual(price * 2);

        private static List<CartLineItem> expectedCartItems;
        private static decimal price;
        private static Client client;
    }

    [Subject(typeof(CheckoutController))]
    public class HTTPPost_When_UPDATING_the_cart_and_REMOVING_an_item : concern<ViewResult, CheckoutShoppingCartViewModel>
    {
        Establish c = () =>
        {
            donationCartItemId = Guid.NewGuid();
            var donationCartItem = new DonationCartItem(donationCartItemId);
            donationCartItem.SetCode("DONATIONCODE1");
            idOfDonationCartItemToKeep = Guid.NewGuid();
            donationCartItemToKeep = new DonationCartItem(idOfDonationCartItemToKeep);
            donationCartItemToKeep.SetCode("DONATIONCODE2");

            client = new Client { Cart = Cart.NewEmpty() };
            client.Cart.AddItem(donationCartItem);
            client.Cart.AddItem(donationCartItemToKeep);

            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {CheckoutShoppingCartViewModel.SelectedCheckBoxName(donationCartItemId), "true,false"},
                                                                            });
        };

        Because of = () => sut.ShoppingCart_UpdateCart(client, formCollection);

        private It should_remove_the_selectedItem_from_the_cart = () =>
                                                                      {
                                                                          client.Cart.NumberOfItemsInCart().ShouldEqual(1);
                                                                          client.Cart.Items.Where(x => x.Item.Id.Equals(idOfDonationCartItemToKeep)).Count().ShouldEqual(1);
                                                                          client.Cart.Items.Where(x => x.Item.Id.Equals(donationCartItemId)).Count().ShouldEqual(0);
                                                                      };

        private static FormCollection formCollection;
        private static Guid donationCartItemId;
        private static DonationCartItem donationCartItemToKeep;
        private static Guid idOfDonationCartItemToKeep;
        private static Client client;
    }

    [Subject(typeof(CheckoutController))]
    public class HTTPPost_When_UPDATING_the_cart_and_CHANGING_QUANTITY_of_an_item : concern<ViewResult, CheckoutShoppingCartViewModel>
    {
        Establish c = () =>
                          {
                              stockItemId = Guid.NewGuid();
                              var stockItem1 = new StockItem(stockItemId, MediaType.Book, 1, true, 99, 99, true, "10").AddProduct(new Product("B222", new[] { Category.BooksBiblesDevotionals }));

                              client = new Client { Cart = Cart.NewEmpty() };
                              client.Cart.AddItem(stockItem1, 1);

                              cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);

                              formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {CheckoutShoppingCartViewModel.QuantityTextBoxName(stockItemId), "10"},
                                                                            });
                          };

        Because of = () => sut.ShoppingCart_UpdateCart(client, formCollection);

        private It should_set_quantity_of_item_to_new_quantity = () => client.Cart.Items.First(x => x.Item.Id == stockItemId).Quantity.ShouldEqual(10);

        private static FormCollection formCollection;
        private static Guid stockItemId;
        private static Client client;
    }

    [Subject(typeof(CheckoutController))]
    public class HTTPPost_When_UPDATING_the_cart_and_CHANGING_QUANTITY_of_an_item_to_ZERO : concern<ViewResult, CheckoutShoppingCartViewModel>
    {
        Establish c = () =>
        {
            stockItemId = Guid.NewGuid();
            var stockItem1 = new StockItem(stockItemId, MediaType.Book, 1, true, 99, 99, true, "10").AddProduct(new Product("B222", new[] { Category.BooksBiblesDevotionals }));

            client = new Client { Cart = Cart.NewEmpty() };
            client.Cart.AddItem(stockItem1, 1);

            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {CheckoutShoppingCartViewModel.QuantityTextBoxName(stockItemId), "0"},
                                                                            });
        };

        Because of = () => sut.ShoppingCart_UpdateCart(client, formCollection);

        private It should_remove_the_item_from_the_cart = () => client.Cart.NumberOfItemsInCart().ShouldEqual(0);

        private static FormCollection formCollection;
        private static Guid stockItemId;
        private static Client client;
    }

    [Subject(typeof(CheckoutController))]
    public class HTTPPost_When_CLICKING_on_the_Checkout_button : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Because of = () => { result = sut.ShoppingCart_Checkout(formCollection) as RedirectToRouteResult; };

        It should_redirect_to_Checkout = () =>
                                                {
                                                    result.ShouldNotBeNull();
                                                    result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Payment);
                                                };
        private static FormCollection formCollection;
    }

    [Subject(typeof(CheckoutController))]
    public class when_viewing_the_Order_Review_page : concern<ViewResult, CheckoutOrderReviewViewModel>
    {
        Establish c = () =>
        {
            var product = new Product("B222", new[] { Category.BooksBiblesDevotionals });
            price = 99;
            var stockItem1 = new StockItem(MediaType.Book, 1, true, price, price, true, "10").AddProduct(product);
            var stockItem2 = new StockItem(MediaType.DVD, 1, true, price, price, true, "5").AddProduct(product);


            client = new Client { Cart = Cart.NewEmpty() };
            var quantity = 1;
            client.Cart.AddItem(stockItem1, quantity);
            client.Cart.AddItem(stockItem2, quantity);
            billingAddress = new Address("Billing-address1", "Billing-address2", "Billing-city", "Billing-TX", "Billing-78249", "Billing-USA");
            shippingAddress = new Address("Shipping-address1", "Shipping-address2", "Shipping-city", "Shipping-TX", "Shipping-78249", "Shipping-USA");

            //cart.AddBillingAddress(billingAddress);
            //cart.AddShippingAddress(shippingAddress);
            shippingMethod = ShippingMethod.USPSGround;
            client.Cart.SetShippingType(shippingMethod);


            expectedCartItems = client.Cart.Items;


            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);

        };
        Because of = () =>
        {
            result = sut.Payment(client);
            resultViewDataModel = result.ViewData.ModelAs<CheckoutOrderReviewViewModel>();
        };

        It should_show_the_OrderReview_view = () => result.ViewName.ShouldEqual("Payment");
        It should_display_a_READONLY_list_of_ICartItems_in_the_Shopping_Cart = () =>
        {
            resultViewDataModel.CartItems.ShouldBe(typeof(IEnumerable<CartLineItem>));
            resultViewDataModel.CartItems.ShouldContainOnly(expectedCartItems);
        };
        It should_display_a_READONLY_total_quantity_for_each_item_in_the_Shopping_Cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Quantity == 1).Count.ShouldEqual(2);
        It should_display_a_READONLY_line_item_price_for_each_item_in_the_cart = () => resultViewDataModel.CartItems.ToLIST().FindAll(x => x.Item.Price == price).Count.ShouldEqual(2);
        It should_display_a_READONLY_subtotal_for_the_cart = () => resultViewDataModel.CartSubTotal.ShouldEqual(price * 2);
        It should_display_a_READONLY_Billing_address = () =>
        {
            // resultViewDataModel.BillingAddress.ShouldBe(typeof(IAddress));
            // VerifyAddress(resultViewDataModel.BillingAddress, billingAddress);
        };
        It should_display_a_READONLY_Shipping_address = () =>
        {
            //resultViewDataModel.ShippingAddress.ShouldBe(typeof(IAddress));
            // VerifyAddress(resultViewDataModel.ShippingAddress, shippingAddress);
        };

        private static IEnumerable<CartLineItem> expectedCartItems;
        private static Address billingAddress;
        private static Address shippingAddress;
        private static ShippingMethod shippingMethod;
        private static decimal price;
        private static Client client;


        private static void VerifyAddress(IAddressReadonly addressReadonly, Address address)
        {
            addressReadonly.Address1.ShouldEqual(address.Address1);
            addressReadonly.Address2.ShouldEqual(address.Address2);
            addressReadonly.City.ShouldEqual(address.City);
            addressReadonly.StateCode.ShouldEqual(address.StateCode);
            addressReadonly.PostalCode.ShouldEqual(address.PostalCode);
            addressReadonly.Country.ShouldEqual(address.Country);
        }
    }
}