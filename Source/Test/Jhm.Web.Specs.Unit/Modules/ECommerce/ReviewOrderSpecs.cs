using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.ReviewOrder
{
    public class UserConcern
    {
        Establish c = () =>
                          {
                              sut = new User(){
                                   Username = "userName",
                                   ApplicationName = "applicationName",
                                   Comment = "comment",
                                   CreationDate = DateTime.Now,
                                   Email = "email@email.com",
                                   FailedPasswordAnswerAttemptCount = 0,
                                   FailedPasswordAnswerAttemptWindowStart = DateTime.Now,
                                   FailedPasswordAttemptCount = 0,
                                   FailedPasswordAttemptWindowStart = DateTime.Now,
                                   IsApproved = true,
                                   Password = "password",
                                   PasswordQuestion = "PasswordQuestion",
                                   PasswordAnswer = "PasswordAnswer",
                                   LastActivityDate = DateTime.Now,
                                   LastLoginDate = DateTime.Now,
                                   LastPasswordChangedDate = DateTime.Now,
                                   IsOnLine = false,
                                   IsLockedOut = false,
                                   LastLockedOutDate = DateTime.Now,
                               };
                          };

        protected static User sut;
    }

    [Subject(typeof(User))]
    public class when_retrieving_user_information : UserConcern
    {
        private Establish c = () =>
                                  {
                                      expectedType = typeof (IEnumerable<CreditCard>);
                                  };

        It should_contain_a_list_of_credit_cards_property = () =>
                                                                        {
                                                                            var properties = new List<PropertyInfo>(sut.GetType().GetProperties());
                                                                            var hasCreditCardProperty = properties.Select(propertyInfo => propertyInfo.PropertyType).Any(propertyType => expectedType.IsAssignableFrom(propertyType));
                                                                            hasCreditCardProperty.ShouldBeTrue();
                                                                            //var creditCardsProperty = properties.Find(x =>x.MemberType.GetType().Equals(expectedType));
                                                                            //creditCardsProperty.ShouldNotBeNull();
                                                                        };
        protected static Type expectedType;
    }


    public class when_retrieving_list_of_credit_cards_from_user : UserConcern
    {
        Because of = () =>
                         {
                             sut.AddCreditCard(new CreditCard("1234", "{05E440D8-B209-4509-A206-5435B1F6E61C}", CreditCardType.Visa, 1, DateTime.Today.Year - 1));// Expired
                             sut.AddCreditCard(new CreditCard("1111", "{83F8D407-831A-4CCD-B6AA-F8F4D2C12AEE}", CreditCardType.MasterCard, DateTime.Today.Month, DateTime.Today.Year));// Not Expired
                             sut.AddCreditCard(new CreditCard("4444", "{8FFAB599-7491-4071-906E-2F1164669701}", CreditCardType.Discover, 1, DateTime.Today.Year + 1));// Not Expired
                             sut.AddCreditCard(new CreditCard("2222", "{253BE90A-9BD8-47C0-BB1C-4C530B5E6114}", CreditCardType.Amex, 1, DateTime.Today.Year + 2));// Not Expired
                         };

        private It should_NOT_return_expired_credit_cards = () =>
                                                                {
                                                                    var now = DateTime.Now;
                                                                    var unexpiredCreditCards = sut.GetUnexpiredCreditCards(now);
                                                                    unexpiredCreditCards.Count.ShouldEqual(3);
                                                                };

    }
    public class when_user_wants_an_existing_credit_card_to_be_added__but_with_different_token : UserConcern
    {
        Because of = () =>
        {
            sut.AddCreditCard(new CreditCard("1111", "{83F8D407-831A-4CCD-B6AA-F8F4D2C12AEE}", CreditCardType.MasterCard, DateTime.Today.Month, DateTime.Today.Year));// Not Expired
            sut.AddCreditCard(new CreditCard("4444", "{8FFAB599-7491-4071-906E-2F1164669701}", CreditCardType.Discover, 1, DateTime.Today.Year + 1));// Not Expired
            
            existingCreditCardToken = "{253BE90A-9BD8-47C0-BB1C-4C530B5E6114}";
            existingCreditCard = new CreditCard("2222", existingCreditCardToken, CreditCardType.Amex, 1, DateTime.Today.Year + 2);
            sut.AddCreditCard(existingCreditCard);

            differentToken = "{89909B16-EDBF-4EFB-9830-308C1CD620A7}";
            existingCreditCardWithDifferentToken = new CreditCard("2222", differentToken, CreditCardType.Amex, 1, DateTime.Today.Year + 2);
        };

        private It should_be_able_to_find_existing_credit_card = () =>
                                                                     {
                                                                         existingCreditCard = sut.GetCreditCard(existingCreditCardToken);
                                                                         existingCreditCard.ShouldNotBeNull();
                                                                     };

        private It should_NOT_add_a_new_entry_int_the_list = () =>
                                                                 {
                                                                    var countBeforeAdding = sut.GetUnexpiredCreditCards().Count;
                                                                    sut.AddCreditCard(existingCreditCardWithDifferentToken);
                                                                    sut.GetUnexpiredCreditCards().Count.ShouldEqual(countBeforeAdding);
                                                                 };

        private It should_update_token_of_existing_credit_card = () => existingCreditCard.Token.ShouldEqual(differentToken);

        private It should_NOT_be_able_to_find_credit_card_using_old_token = () =>
                                                                                    {
                                                                                        var oldToken = existingCreditCardToken;
                                                                                        sut.GetCreditCard(oldToken).ShouldBeNull();
                                                                                    };
        private It should_be_able_to_find_credit_card_using_new_token = () =>
        {
            var newToken = differentToken;
            sut.GetCreditCard(newToken).ShouldNotBeNull();
        };

        protected static CreditCard existingCreditCardWithDifferentToken;
        protected static CreditCard existingCreditCard;
        protected static string existingCreditCardToken;
        protected static string differentToken;
    }
}