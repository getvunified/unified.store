using System;
using Jhm.CommerceIntegration;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce
{
    #region Concerns

    public class WithPaymentRequest
    {
        Establish context = () =>
        {
            loginId = "120442522524";   //  Account # (Gateway):  120442522524 
            password = "BfxH8amYJmBCoX7k";  // Dynamic IP Security Code:  BfxH8amYJmBCoX7k
            isTest = true;
            firstName = "firstName";
            lastName = "lastName";
            address = "address1";
            city = "city";
            state = "TX";
            zipcode = "77777";
            country = "US";
            descriptionOfCharge = "Donation Description";
            cardNumber = "4444333322221111";
            cardCcv = "111";
            amount = (decimal)25.45;
            currency = "USD";
            address1 = "address1";
            address2 = "address2";

            pinCodeForStoredCardHolder = "1234";
            cardTrack1 = "cardTrack1";
            cardTrack2 = "cardTrack2";

            cispStorageEnabled = true;

            disableNegativeDb = false;
            disableEmailReceipts = true;
            disableFraudChecks = true;
            disableCvv2 = false;
            disableAvs = true;
            miscInfo = "Misc\nInfo";
            extraUserData = "More extra info";
            customerBrowser = "Browser";
            customerHost = "Host";
            customerIp = "123.123.123.123";
            siteTag = "MAIN";

            expDateTime = DateTime.Now.AddYears(1);
            cardExpDate = string.Format("{0}{1}", expDateTime.ToString("MM"), expDateTime.ToString("yy"));
            
            email = "email@email.com";
            phone = "210-210-2100";






            paymentRequest = new PaymentRequest(loginId, password, firstName, lastName,
                                                address, city, state, zipcode, country,
                                                descriptionOfCharge, cardNumber, cardExpDate,
                                                cardCcv, amount, isTest, address1, address2, pinCodeForStoredCardHolder,
                                                cardTrack2, cardTrack1,
                                                cispStorageEnabled, disableNegativeDb, disableEmailReceipts,
                                                disableFraudChecks, disableCvv2,
                                                disableAvs, miscInfo, extraUserData, customerBrowser, customerHost,
                                                customerIp, phone, email,
                                                siteTag, currency);
        };

        protected static PaymentRequest paymentRequest;
        protected static string cardExpDate;
        protected static string loginId;
        protected static string password;
        protected static bool isTest;
        protected static string firstName;
        protected static string lastName;
        protected static string address;
        protected static string city;
        protected static string state;
        protected static string zipcode;
        protected static string country;
        protected static string descriptionOfCharge;
        protected static string cardNumber;
        protected static string cardCcv;
        protected static decimal amount;
        protected static string currency;
        protected static string email;
        protected static string phone;
        private static DateTime expDateTime;
        private static string address1;
        private static string address2;
        private static string pinCodeForStoredCardHolder;
        private static string cardTrack2;
        private static string cardTrack1;
        private static bool cispStorageEnabled;
        private static bool disableNegativeDb;
        private static bool disableEmailReceipts;
        private static bool disableFraudChecks;
        private static bool disableCvv2;
        private static bool disableAvs;
        private static string miscInfo;
        private static string extraUserData;
        private static string customerBrowser;
        private static string customerHost;
        private static string customerIp;
        private static string siteTag;
    }

    [Subject(typeof(BlueFinPaymentService))]
    public class When_you_send_in_a_valid_paymentRequest : WithPaymentRequest
    {
        Because of = () => paymentResponse = new BlueFinPaymentService().Process(paymentRequest);

        It should_be_able_to_successfully_call_a_process_to_the_gateway = () => paymentResponse.IsAuthorized.ShouldBeTrue();
        It should_have_authorization_number = () => paymentResponse.ApprovalCode.ShouldNotBeEmpty();
        It should_have_transaction_id = () => paymentResponse.TransactionID.ShouldNotBeEmpty();

        private static IPaymentResponse paymentResponse;
    }

    [Subject(typeof(BlueFinPaymentService))]
    public class When_you_send_in_a_NON_valid_paymentRequest : WithPaymentRequest
    {
        Because of = () =>
                         {
                             paymentRequest.Amount = 3390.00m;
                             paymentResponse = new BlueFinPaymentService().Process(paymentRequest);
                         };

        It should_be_able_to_successfully_call_a_process_to_the_gateway_and_Auth_Should_Fail = () => paymentResponse.IsAuthorized.ShouldBeFalse();
        It should_NOT_have_authorization_number = () => paymentResponse.ApprovalCode.ShouldBeEmpty();
        It should_have_transaction_id = () => paymentResponse.TransactionID.ShouldNotBeEmpty();
        It should_have_AVS_response_of_Z = () => paymentResponse.AvsResponse.ShouldEqual("Z");
        It should_have_CVV2_response_of_S = () => paymentResponse.Cvv2Response.ShouldEqual("S");

        private static IPaymentResponse paymentResponse;
    }
    #endregion

    
}