using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.DigitalDownloadSpecs
{
    [Subject(typeof(DigitalDownload))]
    public class When_user_purchases_a_digital_asset
    {
        Because of = () =>
                         {
       
                         };

        It should_create_a_link_for_digital_asset_to_be_downloaded;
        It should_have_a_download_link_that_expires;
        It should_should_send_an_email_to_customer_when_download_link_is_generated;
        It should_limit_number_of_times_the_asset_can_be_downloaded;
        It should_allow_admin_to_reset_download_link_for_customer;
        It should_expire_old_link_when_regenerating_a_new_link;
        It should_provide_a_place_for_users_to_see_download_links_for_purchased_digital_assets;
        It should_provide_a_restartable_download_connection;


    }

}