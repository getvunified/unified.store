using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.Service.Modules.ECommerce;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;
using Rhino.Mocks;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.DonationControllerSpecs
{
    public class concern<VR, RVD> : Unit_BaseControllerSpecBase<DonationController, IServiceCollection>
        where VR : ActionResult
        where RVD : CartViewModel
    {
        Establish c = () =>
        {
            donationService = an<IDonationService>();
            serviceUnderTest.Stub(x => x.DonationService).Return(donationService);

            cartService = an<ICartService>();
            serviceUnderTest.Stub(x => x.CartService).Return(cartService);
            var cart = an<Cart>();
            client = new Client { Cart = cart, Company = Company.JHM_UnitedStates };
            cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(client.Cart);
        };
        protected static IDonationService donationService;
        protected static VR result;
        protected static RVD resultViewDataModel;
        protected static ICartService cartService;
        protected static Client client;
    }

    [Subject(typeof(DonationController))]
    public class when_viewing_the_Donation_INDEX_page : concern<ViewResult, DonationIndexViewModel>
    {
        Establish c = () =>
                          {
                              donations = new List<Donation>()
                                              {
                                                  new Donation("code", "title", "description", "body"),
                                                  new Donation("code", "title", "description", "body"),
                                                  new Donation("code", "title", "description", "body"),
                                              };
                              donationService.Stub(x => x.GetActiveDonationOpportunities(client.Company)).Return(donations);
                          };

        Because of = () =>
                         {
                             result = sut.Index(client);
                             resultViewDataModel = result.ViewData.ModelAs<DonationIndexViewModel>();
                         };

        It should_show_the_Index_view = () => result.ViewName.ShouldEqual("Opportunities");
        It should_show_a_List_of_Donation_opportunities = () => resultViewDataModel.DonationOpportunities.ShouldContainOnly(donations.ToArray());
        private static List<Donation> donations;
    }

    [Subject(typeof(DonationController))]
    public class When_viewing_a_donation_DETAIL_page_for_a_specific_donation_object : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
                                {
                                    var donation = new Donation(code, title, description, body, imageUrls[0], imageUrls[1], imageUrls[2], pdfVersion);

                                    donationOptions = new List<DonationOption>()
                                                          {
                                                              new DonationOption(Guid.NewGuid()),
                                                              new DonationOption(Guid.NewGuid()),
                                                              new DonationOption(Guid.NewGuid()),
                                                          };
                                    foreach (var donationOption in donationOptions)
                                    {
                                        donation.AddDonationOption(donationOption);
                                    }

                                    donationService.Stub(x => x.GetDonation(client.Company,code)).Return(donation);
                                };

        Because of = () =>
        {
            result = sut.DonationDetail(client, code);
            resultViewDataModel = result.ViewData.ModelAs<DonationFormViewModel>();
        };

        It should_show_the_DonationResult_view = () => result.ViewName.ShouldEqual("DonationDetail");
        It should_show_the_donation_Code = () => resultViewDataModel.DonationCode.ShouldEqual(code);
        It should_show_the_donation_title = () => resultViewDataModel.Title.ShouldEqual(title);
        It should_show_the_donation_Description = () => resultViewDataModel.Description.ShouldEqual(description);
        It should_show_the_donation_Body = () => resultViewDataModel.Body.ShouldEqual(body);
        It should_show_the_donation_detail_view_imageurl = () => resultViewDataModel.DetailViewImageUrl.ShouldEqual(imageUrls[1]);
        It should_show_the_donation_PdfVersion = () => resultViewDataModel.PdfVersion.ShouldEqual(pdfVersion);
        It should_show_the_donation_options = () => resultViewDataModel.DonationOptions.ShouldContainOnly(donationOptions.ToArray());
        private static string code = "DonationCode";
        private static string title = "DonationTitle";
        private static string description = "DonationDescription";
        private static string body = "DonationBody";
        private static List<string> imageUrls = new List<string>() { "ImageUrl1", "ImageUrl2", "ImageUrl3" };
        private static string pdfVersion = "pdfVersion";
        private static List<DonationOption> donationOptions;
    }

    [Subject(typeof(DonationController))]
    public class When_viewing_a_donation_FORM_for_a_specific_donation_object : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {
            var donation = new Donation(code, title, description, body, imageUrls[0], imageUrls[1], imageUrls[2], pdfVersion);

            donationOptions = new List<DonationOption>()
                                                          {
                                                              new DonationOption(Guid.NewGuid()),
                                                              new DonationOption(Guid.NewGuid()),
                                                              new DonationOption(Guid.NewGuid()),
                                                          };
            foreach (var donationOption in donationOptions)
            {
                donation.AddDonationOption(donationOption);
            }

            donationService.Stub(x => x.GetDonation(client.Company, code)).Return(donation);
        };

        Because of = () =>
        {
            result = sut.DonationForm(client,code);
            resultViewDataModel = result.ViewData.ModelAs<DonationFormViewModel>();
        };

        It should_show_the_DonationResult_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_show_the_donation_Code = () => resultViewDataModel.DonationCode.ShouldEqual(code);
        It should_show_the_donation_title = () => resultViewDataModel.Title.ShouldEqual(title);
        It should_show_the_donation_Description = () => resultViewDataModel.Description.ShouldEqual(description);
        It should_show_the_donation_Body = () => resultViewDataModel.Body.ShouldEqual(body);
        It should_show_the_donation_form_image_url = () => resultViewDataModel.FormViewImageUrl.ShouldEqual(imageUrls[2]);
        It should_show_the_donation_PdfVersion = () => resultViewDataModel.PdfVersion.ShouldEqual(pdfVersion);
        It should_show_the_donation_options = () => resultViewDataModel.DonationOptions.ShouldContainOnly(donationOptions.ToArray());
        private static string code = "DonationCode";
        private static string title = "DonationTitle";
        private static string description = "DonationDescription";
        private static string body = "DonationBody";
        private static List<string> imageUrls = new List<string>() { "ImageUrl1", "ImageUrl2", "ImageUrl3" };
        private static string pdfVersion = "pdfVersion";
        private static List<DonationOption> donationOptions;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_DonationOption_with_REQUIRED_CERTIFICATE_NAME_and_name_IS_entered : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {
            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            var donationoption = new AnyAmountDonationOption(optionId);
            donationoption.EnableRequireCertificateField();
            donation.AddDonationOption(donationoption);

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.CertificateTextBoxName(optionId), "CertificateName"},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 200.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client, donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
                                                    {
                                                        result.ShouldNotBeNull();
                                                        result.RouteValues["controller"].ShouldEqual("Checkout");
                                                        result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Cart);
                                                    };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_DonationOption_with_REQUIRED_CERTIFICATE_NAME_and_name_IS_NOT_entered : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            var donationoption = new AnyAmountDonationOption(optionId);
            donationoption.EnableRequireCertificateField();
            donation.AddDonationOption(donationoption);

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 200.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_the_Certificate_field_is_invalid = () => result.ViewData.ModelState[DonationFormViewModel.CertificateTextBoxName(optionId)].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Certificate NAME is required.");

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_AnyAmountDonationOption_and_amount_entered_is_VALID : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new AnyAmountDonationOption(optionId));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 200.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client, donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["controller"].ShouldEqual("Checkout");
            result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Cart);
        };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_AnyAmountDonationOption_and_amount_entered_is_INVALID : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
                                {

                                    donationCode = "donationCode";
                                    optionId = Guid.NewGuid();
                                    donation = new Donation(donationCode, "donationTitle", "description", "body");
                                    donation.AddDonationOption(new AnyAmountDonationOption(optionId));

                                    formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), (-200).ToString()}
                                                                            });

                                    //stubs
                                    donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

                                };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_the_amount_field_is_invalid = () => result.ViewData.ModelState[DonationFormViewModel.AmountTextBoxName(optionId)].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Value must be greater than ZERO.\r\nParameter name: Amount");

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_displaying_a_FixedAmountDonationOption_and_selection_IS_made : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new FixedAmountDonationOption(optionId, 20));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["controller"].ShouldEqual("Checkout");
            result.RouteValues["action"].ShouldEqual("Cart");
        };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_displaying_a_FixedAmountDonationOption_and_selection_IS_NOT_made : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new FixedAmountDonationOption(optionId, 25));

            formCollection = new FormCollection(new NameValueCollection());

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_a_selection_is_required = () => result.ViewData.ModelState[DonationFormViewModel.SelectedChoiceName].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Must make a selection.");

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_MultipleOfdonationOption_and_quantity_is_VALID : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new MultipleOfdonationOption(optionId, 25));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.QuantityTextBoxName(optionId), 5.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client, donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["controller"].ShouldEqual("Checkout");
            result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Cart);
        };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_MultipleOfdonationOption_and_quantity_is_INVALID : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new MultipleOfdonationOption(optionId, 25));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.QuantityTextBoxName(optionId), (-5).ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_the_amount_field_is_invalid = () => result.ViewData.ModelState[DonationFormViewModel.QuantityTextBoxName(optionId)].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Quantity must be greater than ZERO.");

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_AmountRangeDonationOption_and_amount_entered_is_VALID : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new AmountRangeDonationOption(optionId, 10, 100));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 50.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client, donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["controller"].ShouldEqual("Checkout");
            result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Cart);
        };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_AmountRangeDonationOption_and_amount_entered_is_INVALID : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            minAmount = 10;
            maxAmount = 100;
            donation.AddDonationOption(new AmountRangeDonationOption(optionId, minAmount, maxAmount));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 5.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_the_amount_field_is_invalid = () => result.ViewData.ModelState[DonationFormViewModel.AmountTextBoxName(optionId)].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Value must be between {0:C} and {1:C}".FormatWith(minAmount, maxAmount));

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
        private static decimal minAmount;
        private static decimal maxAmount;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_MinimumAmountDonationOption_and_amount_entered_is_VALID : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            donation.AddDonationOption(new MinimumAmountDonationOption(optionId, 10));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 50.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client, donationCode, formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["controller"].ShouldEqual("Checkout");
            result.RouteValues["action"].ShouldEqual(MagicStringEliminator.CheckoutActions.Cart);
        };
        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
    }

    [Subject(typeof(DonationController))]
    public class HTTPPost_When_selecting_an_MinimumAmountDonationOption_and_amount_entered_is_INVALID : concern<ViewResult, DonationFormViewModel>
    {
        Establish context = () =>
        {

            donationCode = "donationCode";
            optionId = Guid.NewGuid();
            donation = new Donation(donationCode, "donationTitle", "description", "body");
            minAmount = 10;
            donation.AddDonationOption(new MinimumAmountDonationOption(optionId, minAmount));

            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {DonationFormViewModel.SelectedChoiceName, optionId.ToString()},
                                                                                {DonationFormViewModel.AmountTextBoxName(optionId), 5.ToString()}
                                                                            });

            //stubs
            donationService.Stub(x => x.GetDonation(null,null)).IgnoreArguments().Return(donation);

        };

        Because of = () => { result = sut.DonationForm(client,donationCode, formCollection) as ViewResult; };

        It should_remain_on_DonationForm_view = () => result.ViewName.ShouldEqual("DonationForm");
        It should_indicate_the_ModelState_is_invalid = () => result.ViewData.ModelState.IsValid.ShouldBeFalse();
        It should_indicate_the_amount_field_is_invalid = () => result.ViewData.ModelState[DonationFormViewModel.AmountTextBoxName(optionId)].Errors[0].ErrorMessage.ShouldEqual(
                                                                       "Value must be greater than or equal to {0:C}.".FormatWith(minAmount));

        private static FormCollection formCollection;
        private static string donationCode;
        private static Guid optionId;
        private static IDonation donation;
        private static decimal minAmount;
    }
}