using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.ProductSpecs
{
    [Subject(typeof(Review))]
    public class When_creating_a_Review
    {
        Because of = () =>
                         {
                             productCode = "PRODUCT_CODE";
                             review = new Review(productCode, reviewdate, rating, title, body, reviewedBy, status);
                         };

        It should_have_a_review_date = () => review.Date.ShouldEqual(reviewdate);
        It should_have_a_rating = () => review.Rating.ShouldEqual(rating);
        It should_have_a_title = () => review.Title.ShouldEqual(title);
        It should_have_a_body = () => review.Body.ShouldEqual(body);
        It should_have_a_ReviewedBy_user = () => review.ReviewedBy.ShouldEqual(reviewedBy);
        It should_have_a_status = () => review.ReviewStatus.ShouldEqual(status);

        private static Review review;
        private static DateTime reviewdate = DateTime.Now;
        private static decimal rating = 50;
        private static string title = "TITLE";
        private static string body = "BODY";
        private static User reviewedBy = new User(new Guid());
        private static ReviewStatus status = ReviewStatus.Active;
        protected static string productCode;
    }

    public class when_adding_a_REVIEW_to_a_product
    {
        Establish c = () =>
        {
            productCode = "PRODUCT_CODE";
            product = new Product(productCode, new Category[] { });
            rating = 99;
            userId = Guid.NewGuid();
            user = new User(userId);
            review = new Review(productCode, DateTime.Now, rating, "TITLE", "BODY", user, ReviewStatus.Active);
        };

        Because of = () => product.AddReview(review);

        It should_be_able_to_calculate_the_rating = () => product.Rating().ShouldEqual(rating);
        It should_have_a_list_of_reviews = () => product.Reviews.ShouldContainOnly(review);
        It should_have_a_user_associated_to_the_review = () => product.Reviews.First(x => x.ReviewedBy.Id == userId).ReviewedBy.ShouldEqual(user);
        private static Product product;
        private static Review review;
        private static int rating;
        private static User user;
        private static Guid userId;
        protected static string productCode;
    }

    [Subject(typeof(Product))]
    public class when_creating_a_product
    {
        Establish c = () =>
        {
            productCode = "SKU1234";
            producttitle = "ProductTitle";
            productDescription = "Product Description";
            productCategories = new List<Category>() { Category.BooksBiblesDevotionals };
            trailerUrl = "/content/trailers/trailer1.avi";
            stockItemNotToInclude = new StockItem(MediaType.Book, quantityInStock: -10, allowBackorder: false, basePrice: 100.00m, salePrice: 90.00m, isInHouse: false, warehouseCode: "10");
            alternativeImages = new List<string>() {
                                                          "/content/images/image1.jpg",
                                                          "/content/images/image2.jpg",
                                                          "/content/images/image3.jpg",
                                                          "/content/images/image4.jpg",
                                                          "/content/images/image5.jpg",
                                                      };
            expectedStockItems = new List<StockItem>()
                                                          {
                                                             new StockItem(MediaType.Book, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true, warehouseCode: "10"),
                                                             new StockItem(MediaType.DVD, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true, warehouseCode: "5"),
                                                             new StockItem(MediaType.AudioFileDownload, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true, warehouseCode: "10"),
                                                             new StockItem(MediaType.OnDemand, quantityInStock: -100, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true, warehouseCode: "10"),
                                                          };
           
            user = new User();
            productRating = 100;
            productReviews = new List<Review>()
                                                   {
                                                      new Review(productCode,DateTime.Now, productRating,"Title",  "Product review1", user, ReviewStatus.Active),
                                                       new Review(productCode,DateTime.Now, productRating,"Title",  "Product review2", user, ReviewStatus.Active),
                                                       new Review(productCode,DateTime.Now, productRating,"Title", "Product review3", user, ReviewStatus.Active),
                                                       new Review(productCode,DateTime.Now, productRating,"Title", "Product review4", user, ReviewStatus.Active),
                                                       new Review(productCode,DateTime.Now, productRating,"Title", "Product review5", user, ReviewStatus.Active),
                                                   };
            productAuthor = "Product Author";
            publisher = "publisher";
            publishDateRange = new DateRange(7.Days().Ago(), 7.Days().InTheFuture());
            
        };

        Because of = () =>
        {
            product = new Product(productCode, productCategories.ToArray()); ;
            product.SetTitle(producttitle);
            product.AddImages("", alternativeImages.ToArray());
            product.SetTrailer(trailerUrl);
            product.SetDescription(productDescription);
            product.AddStockItems(stockItemNotToInclude, expectedStockItems.ToArray());
            product.AddReview(null, productReviews.ToArray());
            product.SetAuthor(productAuthor);
            productRelatedItems = new List<RelatedItem>()
                                                        {
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"},
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"},
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"}
                                                        };
            product.AddRelatedItems(null, productRelatedItems.ToArray());
            product.SetPublisher(publisher);
            product.SetPublishDateRange(publishDateRange);
        };

        It should_have_a_product_code = () => product.Code.ShouldEqual(productCode);
        It should_have_a_product_title = () => product.Title.ShouldEqual(producttitle);
        It should_have_a_list_of_alternative_images = () => product.AlternativeImages.ShouldContainOnly(alternativeImages.ToArray());
        It should_have_a_trailer_if_available = () => product.HasTrailer();
        It should_have_a_link_to_the_trailer = () => product.Trailer.ShouldEqual(trailerUrl);
        It should_have_a_product_description = () => product.Description.ShouldEqual(productDescription);
        It should_have_a_list_of_StockItems = () =>
        {
            product.StockItems.ShouldContain(expectedStockItems.ToArray());
            product.StockItems.ShouldContain(stockItemNotToInclude);
        };
        It should_have_a_list_of_only_the_AVAILABLE_stockitems = () =>
        {
            product.GetAvailableStockItems().ShouldContainOnly(expectedStockItems.ToArray());
            product.GetAvailableStockItems().ShouldNotContain(stockItemNotToInclude);
        };
        It should_indicate_if_the_product_is_in_stock = ()=> product.InStock().ShouldBeTrue();
        It should_have_reviews_of_the_product = () => product.Reviews.ShouldContainOnly(productReviews.ToArray());
        It should_have_the_product_rating = () => product.Rating().ShouldEqual(productRating);
        It should_have_the_author_for_the_product = () => product.Author.ShouldEqual(productAuthor);
        It should_have_the_category_of_the_product = () => product.Categories.ShouldContainOnly(productCategories);
        It should_have_a_list_of_related_items = () => product.RelatedItems.ShouldContainOnly(productRelatedItems);
        It should_have_a_publisher = () => product.Publisher.ShouldEqual(publisher);
        It should_have_a_publishDateRange = () => product.PublishDateRange.ShouldEqual(publishDateRange);

        private static string productCode;
        private static Product product;
        private static List<string> alternativeImages;
        private static string trailerUrl;
        private static string productDescription;
        private static List<StockItem> expectedStockItems;
        private static string productAuthor;
        private static List<Category> productCategories;
        private static List<RelatedItem> productRelatedItems;
        private static StockItem stockItemNotToInclude;
        private static List<Review> productReviews;
        private static decimal productRating;
        private static User user;
        private static string producttitle;
        private static string publisher;
        private static DateRange publishDateRange;
    }
}