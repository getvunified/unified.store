using System.Collections.Generic;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.ShoppingCartSpecs
{
    public class concern
    {
        Establish c = () =>
                          {
                              sut = Cart.NewEmpty();
                          };

        protected static void AddCartItemsToCart(IEnumerable<CartItem> cartItems)
        {
            foreach (var cartItem in cartItems)
                sut.AddItem(cartItem, 1);
        }
        protected static List<CartItem> GetStubICartItems(int returnNumber)
        {
            var returnList = new List<CartItem>();

            for (int i = 0; i < returnNumber; i++)
            {
                var stockItem = new StockItem(MediaType.Book, i, true, 99, 99, true, "10");
                stockItem.AddProduct(new Product( "Code-".FormatWith(i), new Category[] { Category.ChildrenAndTeens }));
                returnList.Add(stockItem);

            }

            return returnList;
        }

        protected static Cart sut;
    }

    [Subject(typeof(Cart))]
    public class when_emptying_the_Shopping_Cart : concern
    {
        Establish c = () =>
                          {
                              const int numberOfICartItemsInCart = 5;
                              itemsInCart = GetStubICartItems(numberOfICartItemsInCart);
                              AddCartItemsToCart(itemsInCart);
                          };


        Because of = () => sut.Empty();
        It should_remove_all_items_in_the_cart = () => sut.Items.ShouldBeEmpty();

        private static List<CartItem> itemsInCart;
    }

    [Subject(typeof(Cart))]
    public class when_quantities_are_updated_in_the_shopping_cart : concern
    {
        Establish c = () =>
                          {
                              var product = new Product("B111", new[] { Category.BooksBiblesDevotionals });
                              stockItem = new StockItem(MediaType.DVD, 1, true, 99, 99, true, "5").AddProduct(product);
                              sut.AddItem(stockItem, 1);
                          };

        Because of = () => sut.ChangeQuantity(stockItem, 2);

        It should_be_able_to_alter_the_quantity_of_a_single_line_item = () => sut.GetLineItem(stockItem).Quantity.ShouldEqual(2);

        private static CartItem stockItem;
    }

    [Subject(typeof(Cart))]
    public class when_an_item_is_ADDED_to_the_Shopping_Cart : concern
    {
        Establish c = () =>
                          {
                              var product = new Product("B111", new[] { Category.BooksBiblesDevotionals });
                              stockItem = new StockItem(MediaType.DVD, 5, true, 99, 99, true, "5").AddProduct(product);
                              sut.AddItem(stockItem, 1);
                          };

        private Because of = () => sut.AddItem(stockItem, 3);

        It should_increment_the_quantity_by_the_number_of_items_added = () =>
                                                                            {
                                                                                sut.NumberOfItemsInCart().ShouldEqual(1);
                                                                                sut.GetLineItem(stockItem).Quantity.ShouldEqual(4);
                                                                            };

        private static CartItem stockItem;
    }

    [Subject(typeof(Cart))]
    public class when_an_item_is_REMOVED_from_the_Shopping_Cart : concern
    {

        Establish c = () =>
        {
            var product = new Product("B111", new[] { Category.BooksBiblesDevotionals });
            stockItem1 = new StockItem(MediaType.DVD, 1, true, 99, 99, true, "5").AddProduct(product);
            stockItem2 = new StockItem(MediaType.OnDemand, 1, true, 99, 99, true, "10").AddProduct(product);
            sut.AddItem(stockItem1, 1);
            expectedCartItems = sut.Items.ToLIST();
            sut.AddItem(stockItem2, 1);
        };

        Because of = () => sut.RemoveItem(stockItem2);

        It should_remove_all_items_of_the_same_SKU = () => sut.Items.ShouldContainOnly(expectedCartItems.ToArray());

        private static CartItem stockItem1;
        private static CartItem stockItem2;

        private static List<CartLineItem> expectedCartItems;
    }

    [Subject(typeof(Cart))]
    public class when_an_item_is_adjusted_leaving_a_quantity_of_0_items : concern
    {
        Establish c = () =>
                          {
                              var product = new Product( "B111", new[] { Category.BooksBiblesDevotionals });
                              stockItem1 = new StockItem(MediaType.DVD, 1, true, 99, 99, true, "5").AddProduct(product);
                              stockItem2 = new StockItem(MediaType.OnDemand, 1, true, 99, 99, true, "10").AddProduct(product);
                              sut.AddItem(stockItem1, 1);
                              expectedCartItems = sut.Items.ToLIST();
                              sut.AddItem(stockItem2, 1);
                          };

        private Because of = () => sut.ChangeQuantity(stockItem2, 0);

        private It should_remove_the_item_completely = () => sut.Items.ShouldContainOnly(expectedCartItems.ToArray());



        private static CartItem stockItem1;
        private static CartItem stockItem2;
        private static List<CartLineItem> expectedCartItems;
    }

    [Subject(typeof(Cart))]
    public class when_adding_a_product_and_the_product_has_MORE_STOCKITEMS_in_stock_then_you_are_adding : concern
    {
        Establish c = () =>
        {
            var product = new Product( "B111", new[] { Category.BooksBiblesDevotionals });
            stockItem = new StockItem(MediaType.DVD, 100, true, 99, 99, true, "5").AddProduct(product);
            
        };

        Because of = () =>
                         {
                             quantityExpected = 100;
                             quantityAllowed = sut.AddItem(stockItem, quantityExpected);
                         };


        It should_allow_you_to_add_you_expected_quantity_to_cart = () => quantityAllowed.ShouldEqual(quantityExpected);

        private static CartItem stockItem;
        private static int quantityExpected;
        private static int quantityAllowed;
    }

    [Subject(typeof(Cart))]
    [Ignore]
    // Ignored for now due to revisiting logic
    public class when_adding_a_product_and_the_product_has_LESS_STOCKITEMS_in_stock_then_you_are_adding : concern
    {
        Establish c = () =>
        {
            var product = new Product( "B111", new[] { Category.BooksBiblesDevotionals });
            quantityInStock = 50;
            stockItem = new StockItem(MediaType.DVD, quantityInStock, true, 99, 99, true, "5").AddProduct(product);

        };

        Because of = () =>
        {
            quantityExpected = 100;
            quantityAllowed = sut.AddItem(stockItem, quantityExpected);
        };

        It should_ONLY_allow_you_to_add_the_TOTAL_ITEMS_IN_STOCK_as_the_quantity_to_cart = () =>
                                                                                               {
                                                                                                   quantityAllowed.ShouldNotEqual(quantityExpected);
                                                                                                   quantityAllowed.ShouldEqual(quantityInStock);
                                                                                               };

        private static CartItem stockItem;
        private static int quantityExpected;
        private static int quantityAllowed;
        private static int quantityInStock;
    }

}