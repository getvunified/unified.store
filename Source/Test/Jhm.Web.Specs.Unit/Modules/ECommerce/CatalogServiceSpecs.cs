using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.CatalogServiceSpecs
{
    public class concern //: Unit_IServiceSpecBase<CatalogService>
    {
        //Establish c = () =>
        //{
        //    cartItemRepository = the_dependency<IRepository<CartItem>>();
        //    cartRepository = the_dependency<IRepository<Cart>>();

        //    provide_a_basic_sut_constructor_argument(cartItemRepository);
        //    provide_a_basic_sut_constructor_argument(cartRepository);

        //    //Stubs
        //    //cartRepository.Stub(x => x.FindAll(null)).IgnoreArguments().Return(inmemoryRepo);
        //};

        //protected static IRepository<CartItem> cartItemRepository;
        //protected static IRepository<Cart> cartRepository;
    }

    [Subject(typeof(CatalogController))]
    public class when_processing_a_credit_card : concern
    {
        It should_only_allow_valid_credit_card_types_based_on_the_company_the_order_is_going_to_be_placed_with;
        It should_require_experation_date;
        It should_require_cvv;
    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_signs_in_with_an_UNKNOWN_email_address : concern
    {
        It should_ask_the_user_to_create_a_password_or_choose_to_checkout_as_a_guest;
    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_chooses_to_checkout_as_a_guest : concern
    {
        It should_not_save_any_of_their_information_into_the_database;
    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_signs_in_with_VALID_email_address_and_password_are_provided_at_login : concern
    {
        It should_show_saved_billing_address;
        It should_show_saved_shipping_address;
        It should_show_any_discounts_the_user_is_entitled_to; //IE: Salt Discount
        It should_show_only_viable_shipping_options_based;
    }

    [Subject(typeof(CatalogController))]
    public class when_entering_Credit_Card_information : concern
    {
        It should_only_allow_valid_credit_card_types_based_on_the_company_the_order_is_going_to_be_placed_with;
        It should_require_experation_date;
        It should_require_cvv;
    }

    [Subject(typeof(CatalogController))]
    public class when_calulating_shipping : concern
    {
        It should_get_the_shipping_rate_based_on_the_entered_Shipping_address;
    }
}