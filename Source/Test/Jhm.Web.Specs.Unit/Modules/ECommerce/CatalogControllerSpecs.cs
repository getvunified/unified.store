using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.Service.Modules.ECommerce;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;
using Rhino.Mocks;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.CatalogControllerSpecs
{
    public class concern<VR, RVD> : Unit_BaseControllerSpecBase<CatalogController, IServiceCollection>
        where VR : ActionResult
        where RVD : CartViewModel
    {
        Establish c = () =>
        {
            catalogService = an<ICatalogService>();
            serviceUnderTest.Stub(x => x.CatalogService).Return(catalogService);

            cartService = an<ICartService>();
            serviceUnderTest.Stub(x => x.CartService).Return(cartService);

        };
        protected static ICatalogService catalogService;
        protected static VR result;
        protected static RVD resultViewDataModel;
        protected static ICartService cartService;

        #region Utility Methods

        protected static List<CartItem> GetStubICartItems(int returnNumber)
        {
            var returnList = new List<CartItem>();

            for (int i = 0; i < returnNumber; i++)
            {
                var stockItem = new StockItem(MediaType.Book, i, true, 99, 99, true, "10");
                stockItem.AddProduct(new Product("Code-{0}".FormatWith(i), new Category[] { Category.Interviews }));
                returnList.Add(stockItem);
            }

            return returnList;
        }
        protected static List<IProduct> GetStubIProducts(int returnNumber, string sku, MediaType mediaType)
        {
            var returnList = new List<IProduct>();
            for (int i = 0; i < returnNumber; i++)
            {
                var product = new Product(sku, new[] { Category.Interviews });
                product.AddStockItems(new StockItem(mediaType, 1, true, 99, 99, true, "10"));
                returnList.Add(product);
            }

            return returnList;
        }
        protected static Cart GetStubCart(IEnumerable<CartItem> cartItems)
        {
            var returnCart = Cart.NewEmpty();

            foreach (var cartItem in cartItems)
            {
                returnCart.AddItem(cartItem, 1);
            }

            return returnCart;
        }
        protected static List<MediaType> GetStubCategories(int numberOfCategories)
        {
            var returnList = new List<MediaType>();

            for (int i = 0; i < numberOfCategories; i++)
            {
                returnList.Add(MediaType.Book);
            }

            return returnList;
        }

        #endregion
    }

    [Subject(typeof(CatalogController))]
    [Ignore]
    public class when_viewing_the_Catalog_INDEX_page : concern<ViewResult, CatalogIndexViewModel>
    {
        Establish c = () =>
                          {
                              const int numberOfFeatured = 5;
                              numberOfICartItemsInCart = 4;

                              const int numberOfRecentBookItems = 10;
                              const int numberOfRecentDVDItems = 10;

                              featuredItems = GetStubIProducts(numberOfFeatured, "F111", MediaType.Book);

                              itemsInCart = GetStubICartItems(numberOfICartItemsInCart);
                              client.Cart = GetStubCart(itemsInCart);

                              latestBookReleaseItems = GetStubIProducts(numberOfRecentBookItems, "H111", MediaType.Book);
                              latestDVDReleaseItems = GetStubIProducts(numberOfRecentDVDItems, "H112", MediaType.DVD);
                              latestMP3ReleaseItems = GetStubIProducts(numberOfRecentDVDItems, "H113", MediaType.AudioFileDownload);
                              latestOnDemandReleaseItems = GetStubIProducts(numberOfRecentDVDItems, "H114", MediaType.OnDemand);

                              latestReleaseItems = new List<IProduct>();
                              latestReleaseItems.AddRange(latestBookReleaseItems);
                              latestReleaseItems.AddRange(latestDVDReleaseItems);
                              latestReleaseItems.AddRange(latestMP3ReleaseItems);
                              latestReleaseItems.AddRange(latestOnDemandReleaseItems);

                              //Stubs
                              cartService.Stub(x => x.GetCart(null)).IgnoreArguments().Return(cart);
                              catalogService.Stub(x => x.GetFeaturedItems(null, null, ref totalRows)).Return(featuredItems);
                              catalogService.Stub(x => x.GetLatestReleasedProducts(null, null, ref totalRows)).Return(latestReleaseItems);
                          };

        Because of = () =>
        {
            result = sut.Index(client);

            resultViewDataModel = result.ViewData.ModelAs<CatalogIndexViewModel>();
        };

        It should_show_the_Index_view = () => result.ViewName.ShouldEqual("Index");
        It should_show_a_List_of_Featured_Products = () => resultViewDataModel.FeaturedItems.ShouldContainOnly(featuredItems.ToArray());
        It should_show_the_number_of_products_in_the_cart = () => resultViewDataModel.NumberOfItemsInCart.ShouldEqual(numberOfICartItemsInCart);
        It should_show_a_list_of_latest_release_items = () => resultViewDataModel.LatestReleaseItems().ShouldContain(latestReleaseItems.ToArray());

        It should_show_a_list_of_latest_release_books = () => resultViewDataModel.LatestReleaseItems(MediaType.Book).ShouldContainOnly(latestBookReleaseItems.ToArray());

        It should_show_a_list_of_latest_release_dvds = () => resultViewDataModel.LatestReleaseItems(MediaType.DVD).ShouldContainOnly(latestDVDReleaseItems.ToArray());
        It should_show_a_list_of_latest_release_mp3s = () => resultViewDataModel.LatestReleaseItems(MediaType.AudioFileDownload).ShouldContainOnly(latestMP3ReleaseItems.ToArray());
        It should_show_a_list_of_most_recent_OnDemand = () => resultViewDataModel.LatestReleaseItems(MediaType.OnDemand).ShouldContainOnly(latestOnDemandReleaseItems.ToArray());



        private static List<IProduct> featuredItems;
        private static Cart cart;
        private static Client client;
        private static List<CartItem> itemsInCart;
        private static int numberOfICartItemsInCart;

        private static List<IProduct> latestReleaseItems;
        private static List<IProduct> latestBookReleaseItems;
        private static List<IProduct> latestDVDReleaseItems;
        private static List<IProduct> latestMP3ReleaseItems;
        private static List<IProduct> latestOnDemandReleaseItems;
        private static int totalRows;
    }

    [Subject(typeof(CatalogController))]
    [Ignore]
    public class when_viewing_a_Category_Page_With_Only_A_MediaType_And_WITHOUT_any_categories : concern<ViewResult, ProductListingViewModel>
    {
        Establish c = () =>
                                  {
                                      mediaType = MediaType.Book;
                                      expectedCartItems = new List<IProduct>
                                                  {
                                                     new Product("H111", new[]{Category.Interviews}),
                                                     new Product("H111", new[]{Category.Interviews}),
                                                     new Product("H111", new[]{Category.Interviews}),
                                                     new Product("H111", new[]{Category.Interviews}),
                                                     new Product("H111", new[]{Category.Interviews}),
                                                  };


                                      //Stubs
                                      catalogService.Stub(x => x.GetProducts(null,null, mediaType.ToString(), null, null, ref totalRows)).Return(expectedCartItems);
                                  };

        Because of = () =>
        {
            result = sut.Category(null, string.Empty);
            resultViewDataModel = result.ViewData.ModelAs<ProductListingViewModel>();
        };

        It should_show_the_Category_view = () => result.ViewName.ShouldEqual("Category");
        private It should_able_to_list_products_that_are_available_in_that_MediaType; 
        //= () => resultViewDataModel.Products.ShouldContainOnly(expectedCartItems);


        private static MediaType mediaType;
        private static List<IProduct> expectedCartItems;
        private static int totalRows;
    }

    [Subject(typeof(CatalogController))]
    [Ignore]
    public class when_viewing_a_Category_Page_With_mediatype_and_categories : concern<ViewResult, ProductListingViewModel>
    {
        Establish c = () =>
        {
            mediaType = MediaType.Book;
            catalogFilters = "{0}, {1}, {2}, {3}".FormatWith(Category.Music, Category.ChildrenAndTeens,
                                                             Category.HealthAndHealing, Category.BooksBiblesDevotionals);
            expectedCartItems = new List<IProduct>
                                                  {
                                                     new Product("H111", new[]{Category.Music}),
                                                     new Product("H111", new[]{Category.ChildrenAndTeens}),
                                                     new Product("H111", new[]{Category.HealthAndHealing}),
                                                     new Product("H111", new[]{Category.BooksBiblesDevotionals}),
                                                     new Product("H111", new[]{Category.BooksBiblesDevotionals}),
                                                  };


            //Stubs
            catalogService.Stub(x => x.GetProducts(catalogFilters, null, mediaType.ToString(), null, null, ref totalRows)).Return(expectedCartItems);
        };

        Because of = () =>
        {
            result = sut.Category(null, catalogFilters);
            resultViewDataModel = result.ViewData.ModelAs<ProductListingViewModel>();
        };

        It should_show_the_Category_view = () => result.ViewName.ShouldEqual("Category");
        It should_able_to_list_products_of_that_MediaType_and_category = () => resultViewDataModel.Products.ShouldContainOnly(expectedCartItems);


        private static MediaType mediaType;
        private static List<IProduct> expectedCartItems;
        private static string catalogFilters;
        private static int totalRows;
    }

    [Subject(typeof(CatalogController))]
    [Ignore]
    public class when_viewing_a_Product_page : concern<ActionResult, CatalogProductViewModel>
    {
        Establish c = () =>
                          {
                              sku = "SKU1234";
                              productCategories = new List<Category>() { Category.BooksBiblesDevotionals };
                              product = new Product(sku, productCategories.ToArray()); ;
                              product.SetTitle("ProductTitle");

                              alternativeImages = new List<string>()
                                                      {
                                                          "/content/images/image1.jpg",
                                                          "/content/images/image2.jpg",
                                                          "/content/images/image3.jpg",
                                                          "/content/images/image4.jpg",
                                                          "/content/images/image5.jpg",
                                                      };
                              product.AddImages("", alternativeImages.ToArray());

                              trailerUrl = "/content/trailers/trailer1.avi";
                              product.SetTrailer(trailerUrl);

                              productDescription = "Product Description";
                              product.SetDescription(productDescription);

                              expectedStockItems = new List<StockItem>()
                                                          {
                                                             new StockItem(MediaType.Book, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true,warehouseCode: "10"),
                                                             new StockItem(MediaType.DVD, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true,warehouseCode: "5"),
                                                             new StockItem(MediaType.AudioFileDownload, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true,warehouseCode: "10"),
                                                             new StockItem(MediaType.OnDemand, quantityInStock: 1, allowBackorder: true, basePrice: 100.00m, salePrice: 90.00m, isInHouse:true,warehouseCode: "10"),
                                                          };
                              stockItemNotToInclude = new StockItem(MediaType.Book, quantityInStock: -10, allowBackorder: false, basePrice: 100.00m, salePrice: 90.00m, isInHouse: false, warehouseCode: "10");
                              product.AddStockItems(stockItemNotToInclude, expectedStockItems.ToArray());

                              productRating = 100;
                              User user = new User();
                              productReviews = new List<Review>()
                                                   {
                                                        new Review(product.Code, DateTime.Now, productRating,"Title",  "Product review1", user, ReviewStatus.Active),
                                                        new Review(product.Code, DateTime.Now, productRating,"Title",  "Product review2", user, ReviewStatus.Active),
                                                        new Review(product.Code, DateTime.Now, productRating,"Title", "Product review3", user, ReviewStatus.Active),
                                                        new Review(product.Code, DateTime.Now, productRating,"Title", "Product review4", user, ReviewStatus.Active),
                                                        new Review(product.Code, DateTime.Now, productRating,"Title", "Product review5", user, ReviewStatus.Active),
                                                   };
                              product.AddReview(null, productReviews.ToArray());



                              productAuthor = "Product Author";
                              product.SetAuthor(productAuthor);


                              productRelatedItems = new List<RelatedItem>()
                                                        {
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"},
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"},
                                                            new RelatedItem{Code = "Code", Description = "Description", Image = "Image", Title = "Title"}
                                                        };
                              product.AddRelatedItems(null, productRelatedItems.ToArray());

                              //Stubs
                              catalogService.Stub(x => x.GetProduct(null, sku)).Return(product);
                          };

        Because of = () =>
                                 {
                                     result = sut.Product(null,sku);
                                     resultViewDataModel = ((ViewResult)result).ViewData.ModelAs<CatalogProductViewModel>();
                                 };

        It should_show_the_Product_view = () => ((ViewResult)result).ViewName.ShouldEqual("Product");
        It should_show_be_the_a_Product_with_searched_SKU = () => resultViewDataModel.Code.ShouldEqual(product.Code);
        It should_have_a_section_title_of_the_product_title = () => resultViewDataModel.Title.ShouldEqual(product.Title);
        It should_show_all_alternative_images_available = () => resultViewDataModel.AlternativeImages.ShouldContainOnly(alternativeImages.ToArray());
        It should_show_a_trailer_if_available = () =>
                                                    {
                                                        resultViewDataModel.HasTrailer.ShouldBeTrue();
                                                        resultViewDataModel.Trailer.ShouldEqual(trailerUrl);
                                                    };
        It should_show_if_the_product_is_in_stock = () => resultViewDataModel.InStock.ShouldBeTrue();
        It should_show_the_product_description = () => resultViewDataModel.Description.ShouldEqual(productDescription);
        It should_show_All_StockItems_available = () =>
                                                       {
                                                           resultViewDataModel.StockItems.ShouldContainOnly(expectedStockItems.ToArray());
                                                           resultViewDataModel.StockItems.ShouldNotContain(stockItemNotToInclude);
                                                       };
        It should_show_reviews_of_the_product = () => resultViewDataModel.Reviews.ShouldContainOnly(productReviews.ToArray());
        It should_show_the_product_rating = () => resultViewDataModel.Rating.ShouldEqual(productRating);
        It should_show_the_author_for_the_product = () => resultViewDataModel.Author.ShouldEqual(productAuthor);
        It should_show_the_category_of_the_product = () => resultViewDataModel.Categories.ShouldContainOnly(productCategories);
        It should_show_a_list_of_related_items = () => resultViewDataModel.RelatedItems.ShouldContainOnly(productRelatedItems);

        private static string sku;
        private static Product product;
        private static List<string> alternativeImages;
        private static string trailerUrl;
        private static string productDescription;
        private static List<StockItem> expectedStockItems;

        private static string productAuthor;
        private static List<Category> productCategories;
        private static List<RelatedItem> productRelatedItems;
        private static StockItem stockItemNotToInclude;
        private static List<Review> productReviews;
        private static decimal productRating;
    }

    [Ignore]
    [Subject(typeof(CatalogController))]
    public class when_viewing_a_Products_trailer_video
    {
        It should_stream_from_our_CDN;
    }



    [Subject(typeof(CatalogController))]
    public class HTTPPost_When_searching_for_a_product : concern<RedirectToRouteResult, DonationFormViewModel>
    {
        Establish context = () =>
        {
            queryString = "TEST";
            formCollection = new FormCollection(new NameValueCollection()
                                                                            {
                                                                                {"queryString", queryString},
                                                                            });

            //stubs
            catalogService.Stub(x => x.FindProducts(null, null, null, ref totalRows)).IgnoreArguments().Return(new List<Product>());

        };

        Because of = () => { result = sut.Index_ProductSearch(formCollection) as RedirectToRouteResult; };

        It should_redirect_to_shopping_cart = () =>
        {
            result.ShouldNotBeNull();
            result.RouteValues["action"].ShouldEqual("Search");
            result.RouteValues["queryString"].ShouldEqual(queryString);
        };
        
        private static FormCollection formCollection;
        private static string queryString;
        private static int totalRows;
    }


}