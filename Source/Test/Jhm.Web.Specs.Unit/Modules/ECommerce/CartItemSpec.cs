using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Unit.Modules.ECommerce.CartItemSpec
{
    [Subject(typeof(StockItem))]
    public class When_creating_a_cartitem
    {
        Establish c = () => { cartItem = new CartItem(); };

        Because of = () => cartItem.SetImageLocation(imageLocation);

        It should_have_an_image_location = ()=> cartItem.ImageLocation.ShouldEqual(imageLocation);


        private static CartItem cartItem;
        private static string imageLocation = "ImageLocation";
    }
}