﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Repository
{
    [Subject(typeof(UserRepository))]
    public class when_UserRepository_is_configured : JhmWebRepositoryIntegrationSpecBase<User>
    {
        It should_be_able_to_SAVE_a_new_entity = () => when_repository_is_configured.should_be_able_to_SAVE_a_new_entity();
        It should_be_able_to_Get_Compare_Modify_And_Save_Entity1 = () => when_repository_is_configured.should_be_able_to_Get_Compare_Modify_And_Save_Entity1();
        It should_be_able_to_Get_Compare_And_Delete_Entity2 = () => when_repository_is_configured.should_be_able_to_Get_Compare_And_Delete_Entity2();
        It should_no_long_exist_in_datastore = () => when_repository_is_configured.should_have_NO_Entities_in_datastore();

        private static Role role;


        protected override void ModifyEntity(User entity)
        {
            entity.Password = "NewPassword";
            entity.PasswordQuestion = "NewPasswordQuestion";
            entity.PasswordAnswer = "NewPasswordAnswer";
            entity.RemoveRole(role);
        }

        protected override void CompareEntities(User actual, User expected)
        {
            actual.Username.ShouldEqual(expected.Username);
            actual.ApplicationName.ShouldEqual(expected.ApplicationName);
            actual.Comment.ShouldEqual(expected.Comment);
            actual.CreationDate.ShouldEqual(expected.CreationDate);
            actual.Email.ShouldEqual(expected.Email);
            actual.FailedPasswordAnswerAttemptCount.ShouldEqual(expected.FailedPasswordAnswerAttemptCount);
            actual.FailedPasswordAnswerAttemptWindowStart.ShouldEqual(expected.FailedPasswordAnswerAttemptWindowStart);
            actual.FailedPasswordAttemptCount.ShouldEqual(expected.FailedPasswordAttemptCount);
            actual.FailedPasswordAttemptWindowStart.ShouldEqual(expected.FailedPasswordAttemptWindowStart);
            actual.IsApproved.ShouldEqual(expected.IsApproved);
            actual.Password.ShouldEqual(expected.Password);
            actual.PasswordQuestion.ShouldEqual(expected.PasswordQuestion);
            actual.PasswordAnswer.ShouldEqual(expected.PasswordAnswer);
            actual.LastActivityDate.ShouldEqual(expected.LastActivityDate);
            actual.LastLoginDate.ShouldEqual(expected.LastLoginDate);
            actual.LastPasswordChangedDate.ShouldEqual(expected.LastPasswordChangedDate);
            actual.IsOnLine.ShouldEqual(expected.IsOnLine);
            actual.IsLockedOut.ShouldEqual(expected.IsLockedOut);
            actual.LastLockedOutDate.ShouldEqual(expected.LastLockedOutDate);

            expected.Roles.ShouldContain(actual.Roles.ToLIST().ToArray());
        }

        protected override void DeleteEntityDependencies()
        {

        }

        protected override IRepository<User> GetRepository()
        {
            return new UserRepository(null);
        }

        protected override User CreateEntity()
        {
            return CreateUser();
        }

        protected static User CreateUser()
        {
            var user = new User()
                       {
                           Username = "userName",
                           ApplicationName = "applicationName",
                           Comment = "comment",
                           CreationDate = DateTime.Now,
                           Email = "email@email.com",
                           FailedPasswordAnswerAttemptCount = 0,
                           FailedPasswordAnswerAttemptWindowStart = DateTime.Now,
                           FailedPasswordAttemptCount = 0,
                           FailedPasswordAttemptWindowStart = DateTime.Now,
                           IsApproved = true,
                           Password = "password",
                           PasswordQuestion = "PasswordQuestion",
                           PasswordAnswer = "PasswordAnswer",
                           LastActivityDate = DateTime.Now,
                           LastLoginDate = DateTime.Now,
                           LastPasswordChangedDate = DateTime.Now,
                           IsOnLine = false,
                           IsLockedOut = false,
                           LastLockedOutDate = DateTime.Now,
                       };
            role = CreateRole();
            user.AddRole(role);
            return user;
        }

        private static Role CreateRole()
        {
            return new Role() { ApplicationName = "applicationName", RoleName = "roleName" };
        }


    }

    [Subject(typeof(UserRepository))]
    public class With_Seeded_Users_and_Profiles : JhmWebIntegrationSpecBase<Profile>
    {
        Establish context = () =>
        {
            repository = new UserRepository(null);
            applicationName = new AppSettingsReader().GetValue("applicationName", typeof(string)).TryConvertTo<string>();
            userList = CreateUserList();
            WithUOWAndTransaction(() => { foreach (var user in userList) repository.Save(user); });
        };

        Cleanup clean = () => WithUOWAndTransaction(() => { foreach (var user in userList) repository.Delete(user); });

        private static IList<User> CreateUserList()
        {
            IList<User> myList = new List<User>();
            userName1 = "UserName1";
            userName2 = "UserName2";
            userName3 = "UserName1_2";

            emailMatchString = "email.com";
            emailMatchCount = 2;
            userNameMatchString = "UserName1";
            userNameMatchCount = 2;

            countOfAllSeededUsers = 3;


            profile1 = CreateRandomProfile(false, 3.Hours().Ago());
            profile2 = CreateRandomProfile(true, 5.Days().Ago());
            profile3 = CreateRandomProfile(false, 1.Minutes().Ago());

            randomUser1 = CreateRandomUser(userName1, applicationName, "email1@email.com", true, 3.Hours().Ago(), profile1);
            randomUser2 = CreateRandomUser(userName2, applicationName, "email2@myDomain.com", false, 3.Days().Ago(), profile2);
            randomUser3 = CreateRandomUser(userName3, applicationName, "email3@email.com", true, 1.Hours().Ago(), profile3);

            myList.Add(randomUser1);
            myList.Add(randomUser2);
            myList.Add(randomUser3);
            return myList;
        }
        private static Profile CreateRandomProfile(bool isAnonymous, DateTime lastActivityDate)
        {
            return new Profile()
            {
                ApplicationName = applicationName,
                BirthDate = DateTime.Now,
                FirstName = "FirstName",
                LastName = "LastName",
                Gender = "Male",
                IsAnonymous = isAnonymous,
                Language = "Language",
                LastActivityDate = lastActivityDate,
                LastUpdatedDate = DateTime.Now,
                Subscription = "Subscription",
                //Occupation = "Occupation",
                Website = "Website",
                Company = "Company"
            };
        }
        private static User CreateRandomUser(string userName, string applicationname, string emailAddress, bool isOnline, DateTime lastActivityDate, Profile profile)
        {
            var user = new User()
            {
                Username = userName,
                ApplicationName = applicationname,
                Comment = "comment",
                CreationDate = DateTime.Now,
                Email = emailAddress,
                FailedPasswordAnswerAttemptCount = 0,
                FailedPasswordAnswerAttemptWindowStart = DateTime.Now,
                FailedPasswordAttemptCount = 0,
                FailedPasswordAttemptWindowStart = DateTime.Now,
                IsApproved = true,
                Password = "password",
                PasswordQuestion = "PasswordQuestion",
                PasswordAnswer = "PasswordAnswer",
                LastActivityDate = lastActivityDate,
                LastLoginDate = DateTime.Now,
                LastPasswordChangedDate = DateTime.Now,
                IsOnLine = isOnline,
                IsLockedOut = false,
                LastLockedOutDate = DateTime.Now,

            };
            user.AddProfile(profile);
            return user;
        }

        protected static string applicationName;
        protected static string userName1;
        protected static string userName2;
        protected static string userName3;
        protected static User randomUser1;
        protected static User randomUser2;
        protected static User randomUser3;
        protected static Profile profile1;
        protected static Profile profile2;
        protected static Profile profile3;
        protected static IList<User> userList;
        protected static UserRepository repository;
        protected static string emailMatchString;
        protected static string userNameMatchString;
        protected static int userNameMatchCount;
        protected static int emailMatchCount;
        protected static int countOfAllSeededUsers;
    }

   


    [Subject(typeof(UserRepository))]
    public class When_Seeding_Users_ : With_Seeded_Users_and_Profiles
    {
        It should_be_able_to_save_seeded_users_and_verify_they_exist = () => WithUOW(() => repository.GetAllUserCount(applicationName).ShouldEqual(userList.Count));
    }

    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_user_by_username_and_applicationName : With_Seeded_Users_and_Profiles
    {
        Because of = () => WithUOW(() => { user = repository.GetUser(userName1, applicationName); });

        It should_be_able_to_get_user = () => user.ShouldEqual(randomUser1);
        private static User user;
    }




    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_user_by_Id_and_applicaitonName : With_Seeded_Users_and_Profiles
    {
        Because of = () => WithUOW(() => { user = repository.GetUser(randomUser1.Id, applicationName); });

        It should_be_able_to_get_user = () => user.ShouldEqual(randomUser1);
        
        private static User user;
    }


    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_user_by_email_and_applicationName : With_Seeded_Users_and_Profiles
    {                                                   
        Because of = () => WithUOW(() => { user = repository.GetUserByEmail(randomUser1.Email, applicationName); });

        It should_be_able_to_get_user = () => user.ShouldEqual(randomUser1);
        
        private static User user;
    }


    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_users_by_application : With_Seeded_Users_and_Profiles
    {
        Because of = () => WithUOW(() => { users = repository.GetUsers(applicationName); });

        It should_be_able_to_get_a_list_of_users = () => users.ShouldContain(userList.ToLIST().ToArray());
        
        
        private static IList<User> users;
    }


    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_seaching_for_users_by_emailMatch_and_applicationName : With_Seeded_Users_and_Profiles
    {

        Because of = () => WithUOW(() => { users = repository.GetUsers(emailMatchString, null, applicationName); });

        It should_be_able_to_get_a_list_of_users = () => users.Count.ShouldEqual(emailMatchCount);
        
        private static IList<User> users;
    }

    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_seaching_for_users_by_userNameMatch_and_applicationName : With_Seeded_Users_and_Profiles
    {

        Because of = () => WithUOW(() => { users = repository.GetUsers(null, userNameMatchString, applicationName); });

        It should_be_able_to_get_a_list_of_users = () => users.Count.ShouldEqual(userNameMatchCount);

        private static IList<User> users;
    }



    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_count_of_users_by_applicationName : With_Seeded_Users_and_Profiles
    {

        Because of = () => WithUOW(() => { userCount = repository.GetAllUserCount(applicationName); });

        It should_be_able_to_get_a_list_of_users = () => userCount.ShouldEqual(countOfAllSeededUsers);

        private static int userCount;
    }


    [Subject(typeof(UserRepository))]
    public class When_users_exists_and_searching_for_count_of_users_online_within_the_last_4_hours_and_by_applicationName : With_Seeded_Users_and_Profiles
    {

        Because of = () => WithUOW(() => { userCount = repository.GetNumberOfUsersOnline(4.Hours().Ago(), applicationName); });

        It should_be_able_to_get_a_list_of_users = () => userCount.ShouldEqual(2);

        private static int userCount;
    }
    
    
    [Subject(typeof(UserRepository))]
    public class When_removing_all_seeded_users : With_Seeded_Users_and_Profiles
    {

        Because of = () =>
        {
            WithUOWAndTransaction(() => { foreach (var user in userList) repository.Delete(user); });
            WithUOW(() => dbUserCount = repository.GetAllUserCount(applicationName));
        };

        It should_be_able_to_delete_all_seeded_users = () => dbUserCount.ShouldEqual(0);


        private static int dbUserCount;
    }

}
