﻿using System.Collections.Generic;
using System.Configuration;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.Repositories;
using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Repository
{
    [Subject(typeof(RoleRepository))]
    public class when_RoleRepository_is_configured : JhmWebRepositoryIntegrationSpecBase<Role>
    {
        It should_be_able_to_SAVE_a_new_entity = () => when_repository_is_configured.should_be_able_to_SAVE_a_new_entity();
        It should_be_able_to_Get_Compare_Modify_And_Save_Entity1 = () => when_repository_is_configured.should_be_able_to_Get_Compare_Modify_And_Save_Entity1();
        It should_be_able_to_Get_Compare_And_Delete_Entity2 = () => when_repository_is_configured.should_be_able_to_Get_Compare_And_Delete_Entity2();
        It should_no_long_exist_in_datastore = () => when_repository_is_configured.should_have_NO_Entities_in_datastore();


        protected override void ModifyEntity(Role entity)
        {
            entity.ApplicationName = "newApplicationName";

        }

        protected override void CompareEntities(Role actual, Role expected)
        {
            actual.RoleName.ShouldEqual(expected.RoleName);
            actual.ApplicationName.ShouldEqual(expected.ApplicationName);
            actual.UsersInRole.ShouldBeEmpty();
        }

        protected override void DeleteEntityDependencies()
        {

        }

        protected override IRepository<Role> GetRepository()
        {
            return new RoleRepository(null);
        }
        protected override Role CreateEntity()
        {
            return CreateRole();
        }

        private static Role CreateRole()
        {
            var r = new Role() { ApplicationName = "applicationName", RoleName = "roleName" };
            return r;
        }
    }




    [Subject(typeof(RoleRepository))]
    public class With_Seeded_Roles : JhmWebIntegrationSpecBase<Role>
    {
        Establish context = () =>
        {
            repository = new RoleRepository(null);
            applicationName = new AppSettingsReader().GetValue("applicationName", typeof(string)).TryConvertTo<string>();
            roleList = CreateRandomRoles();
            WithUOWAndTransaction(() => { foreach (var role in roleList) repository.Save(role); });
        };

        private static IList<Role> CreateRandomRoles()
        {
            var myRoleList = new List<Role>();
            roleName1 = "RoleName1";
            roleName2 = "RoleName2";
            roleName3 = "RoleName3";

            role1 = CreateRandomRole(roleName1);
            role2 = CreateRandomRole(roleName2);
            role3 = CreateRandomRole(roleName3);

            myRoleList.Add(role1);
            myRoleList.Add(role2);
            myRoleList.Add(role3);
            return myRoleList;
        }
        private static Role CreateRandomRole(string roleName)
        {
            return new Role() { ApplicationName = applicationName, RoleName = roleName };
        }

        Cleanup clean = () => WithUOWAndTransaction(() => { foreach (var role in roleList) repository.Delete(role); });



        protected static RoleRepository repository;
        protected static string applicationName;
        protected static string roleName3;
        protected static string roleName1;
        protected static string roleName2;
        protected static Role role1;
        protected static Role role2;
        protected static Role role3;
        protected static IList<Role> roleList;
    }


    [Subject(typeof(RoleRepository))]
    public class When_roles_exists_and_searching_for_a_role_by_roleName_and_applicationName : With_Seeded_Roles
    {
        Because of = () => WithUOW(() => role = repository.GetRole(role2.RoleName, applicationName));

        It should_return_a_role = () => role.ShouldEqual(role2);

        private static Role role;
    }


    [Subject(typeof(RoleRepository))]
    public class When_roles_exists_and_searching_for_all_roles_by_applicationName : With_Seeded_Roles
    {
        Because of = () => WithUOW(() => roles = repository.GetAllRoles(applicationName));

        It should_return_a_list_of_roles = () => roles.ShouldContain(roleList.ToLIST().ToArray());

        private static IList<Role> roles;
    }


    [Subject(typeof(RoleRepository))]
    public class When_removing_all_seeded_roles : With_Seeded_Roles
    {

        Because of = () =>
                         {
                             WithUOWAndTransaction(() => { foreach (var role in roleList) repository.Delete(role); });
                             WithUOW(() => roles = repository.GetAllRoles(applicationName));

                         };

        It should_be_able_to_delete_all_seeded_roles = () => roles.Count.ShouldEqual(0);


        private static IList<Role> roles;
    }

}
