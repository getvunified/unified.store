﻿using System.Web.Mvc;
using Jhm.Common;
using Jhm.Common.UOW;

using Jhm.Web.mSpec;
using Jhm.Web.UI.IBootStrapperTasks;

namespace Jhm.Web.Specs.Integration.Repository
{

    public abstract class JhmWebRepositoryIntegrationSpecBase<T> : RepositoryIntegrationSpecBase<T> where T : class, IEntity
    {
        protected internal static UnitOfWork CreateUnitOfWork()
        {
            new ConfigureDependencies().Execute();
            return new UnitOfWork(DependencyResolver.Current.GetService<IUnitOfWorkFactory>());
        }

        protected override UnitOfWork GetUnitOfWork()
        {
            return CreateUnitOfWork();
        }
    }

    public abstract class JhmWebIntegrationSpecBase<T> : IntegrationSpecBase<T> where T : class, IEntity
    {
        protected override UnitOfWork GetUnitOfWork()
        {
            return JhmWebRepositoryIntegrationSpecBase<T>.CreateUnitOfWork();
        }
    }
}
