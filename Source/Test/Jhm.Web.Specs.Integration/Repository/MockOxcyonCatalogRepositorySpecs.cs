using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Repository
{
    [Ignore]
    [Subject(typeof(Product))]
    public class When_creating_a_list_of_products_from_the_oxcyon_xml_feed_of_GENCOM_products
    {
        Because of = () =>
        {
            string sUrl = "http://www.jhm.org/ME2/Console/XmlSyndication/Display/XML.asp?xsid=447F66CC4C124593A088C447AD0A151F";
            XmlTextReader xmlReader = new XmlTextReader(sUrl);

            XDocument xmlDoc = XDocument.Load(xmlReader);

            var xmlProducts = from record in xmlDoc.Descendants("Record")
                              select new
                                        {
                                            Tier1Type = GetValueFor(record, "1stTierType"),
                                            Tier2Type = GetValueFor(record, "2ndTierType"),
                                            Tier3Type = GetValueFor(record, "3rdTierType"),
                                            IdentificationNumber1 = GetValueFor(record, "IdentificationNumber1"),
                                            IdentificationNumber2 = GetValueFor(record, "IdentificationNumber2"),
                                            ItemImage1 = GetValueFor(record, "ItemImage1"),
                                            ItemName = GetValueFor(record, "ItemName"),
                                            Price1 = GetValueFor(record, "Price1"),
                                            Price2 = GetValueFor(record, "Price2"),
                                        };

            products = new List<Product>();
            foreach (var xmlProduct in xmlProducts)
            {
                var product = new Product(xmlProduct.IdentificationNumber1, null);
                product.SetTitle(xmlProduct.IdentificationNumber2);
                product.AddImages("http://www.jhm.org{0}".FormatWith(xmlProduct.ItemImage1));
                var mediaType = xmlProduct.Tier2Type.IsNot().NullOrEmpty() && xmlProduct.Tier2Type.Length.Is().GreaterThan(3) ? xmlProduct.Tier2Type.ToTitleCase() : xmlProduct.Tier2Type;
                mediaType = mediaType == "Mp3 - Downloadable Content" ? "MP3" : mediaType;
                product.AddStockItems(new StockItem(MediaType.FromDisplayName<MediaType>(mediaType), 100, true, xmlProduct.Price1.TryConvertTo<decimal>(), xmlProduct.Price2.TryConvertTo<decimal>(), true, "10"));
                products.Add(product);
            }
        };

        private static string GetValueFor(XElement record, string labelId)
        {
            return record.Elements("Field").ToLIST().Find(x => x.Attribute("Label").Value == labelId).Value;
        }

        It should_be_able_to_get_products_from_oxcyon_xml_feed = () => products.ShouldNotBeNull();
        private static List<Product> products;
    }
}