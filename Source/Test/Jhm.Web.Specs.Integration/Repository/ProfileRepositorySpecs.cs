﻿
namespace Jhm.Web.Specs.Integration.Repository
{
    //[Subject(typeof(ProfileRepository))]
    //public class when_ProfileRepository_is_configured : JhmWebRepositoryIntegrationSpecBase<Profile>
    //{
    //    It should_be_able_to_SAVE_a_new_entity = () => when_repository_is_configured.should_be_able_to_SAVE_a_new_entity();
    //    It should_be_able_to_Get_Compare_Modify_And_Save_Entity1 = () => when_repository_is_configured.should_be_able_to_Get_Compare_Modify_And_Save_Entity1();
    //    It should_be_able_to_Get_Compare_And_Delete_Entity2 = () => when_repository_is_configured.should_be_able_to_Get_Compare_And_Delete_Entity2();
    //    It should_no_long_exist_in_datastore = () => when_repository_is_configured.should_have_NO_Entities_in_datastore();




    //    protected override void ModifyEntity(Profile entity)
    //    {
    //        entity.ApplicationName = "newApplicationName";
    //        entity.BirthDate = 1.Days().Ago();
    //        entity.Street = "newStreet";
    //        entity.City = "newCity";
    //        entity.State = "newState";
    //        entity.Zip = "newZip";
    //        entity.Country = "newCountry";
    //        entity.FirstName = "newFirstName";
    //        entity.LastName = "newLastName";
    //        entity.Gender = "Female";
    //        entity.IsAnonymous = true;
    //        entity.Language = "newLanguage";
    //        entity.LastActivityDate = 1.Days().Ago();
    //        entity.LastUpdatedDate = 1.Days().Ago();
    //        entity.Subscription = "newSubscription";
    //        entity.Occupation = "newOccupation";
    //        entity.Website = "newWebsite";
    //    }

    //    protected override void CompareEntities(Profile actual, Profile expected)
    //    {
    //        actual.ApplicationName.ShouldEqual(expected.ApplicationName);
    //        actual.BirthDate.ShouldEqual(expected.BirthDate);
    //        actual.Street.ShouldEqual(expected.Street);
    //        actual.City.ShouldEqual(expected.City);
    //        actual.State.ShouldEqual(expected.State);
    //        actual.Zip.ShouldEqual(expected.Zip);
    //        actual.Country.ShouldEqual(expected.Country);
    //        actual.FirstName.ShouldEqual(expected.FirstName);
    //        actual.LastName.ShouldEqual(expected.LastName);
    //        actual.Gender.ShouldEqual(expected.Gender);
    //        actual.IsAnonymous.ShouldEqual(expected.IsAnonymous);
    //        actual.Language.ShouldEqual(expected.Language);
    //        actual.LastActivityDate.ShouldEqual(expected.LastActivityDate);
    //        actual.LastUpdatedDate.ShouldEqual(expected.LastUpdatedDate);
    //        actual.Subscription.ShouldEqual(expected.Subscription);
    //        actual.Occupation.ShouldEqual(expected.Occupation);
    //        actual.Website.ShouldEqual(expected.Website);
    //    }

    //    protected override void DeleteEntityDependencies()
    //    {

    //    }

    //    protected override IRepository<Profile> GetRepository()
    //    {
    //        return new ProfileRepository();
    //    }
    //    protected override Profile CreateEntity()
    //    {
    //        return CreateProfile();
    //    }

    //    private Profile CreateProfile()
    //    {
    //        return new Profile()
    //                   {
    //                       ApplicationName = "applicationName",
    //                       BirthDate = DateTime.Now,
    //                       Street = "Street",
    //                       City = "City",
    //                       State = "State",
    //                       Zip = "Zip",
    //                       Country = "Country",
    //                       FirstName = "FirstName",
    //                       LastName = "LastName",
    //                       Gender = "Male",
    //                       IsAnonymous = false,
    //                       Language = "Language",
    //                       LastActivityDate = DateTime.Now,
    //                       LastUpdatedDate = DateTime.Now,
    //                       Subscription = "Subscription",
    //                       Occupation = "Occupation",
    //                       Website = "Website"
    //                   };
    //    }
    //}





    //[Subject(typeof(ProfileRepository))]
    //public class With_Seeded_Users_and_Profiles : JhmWebIntegrationSpecBase<Profile>
    //{
    //    Establish context = () =>
    //    {
    //        repository = new ProfileRepository();
    //        applicationName = new AppSettingsReader().GetValue("applicationName", typeof(string)).TryConvertTo<string>();
    //        userList = CreateUserList();
    //        WithUOWAndTransaction(() => { foreach (var user in userList) new UserRepository().Save(user); });
    //    };

    //    Cleanup clean = () => WithUOWAndTransaction(() => { foreach (var user in userList) new UserRepository().Delete(user); });

    //    private static IList<User> CreateUserList()
    //    {
    //        IList<User> myList = new List<User>();
    //        userName1 = "UserName1";
    //        userName2 = "UserName2";
    //        userName3 = "UserName1_2";

    //        profile1 = CreateRandomProfile(false, 3.Hours().Ago());
    //        profile2 = CreateRandomProfile(true, 5.Days().Ago());
    //        profile3 = CreateRandomProfile(false, 1.Minutes().Ago());

    //        randomUser1 = CreateRandomUser(userName1, applicationName, "email1@email.com", true, 3.Hours().Ago(), profile1);
    //        randomUser2 = CreateRandomUser(userName2, applicationName, "email2@myDomain.com", false, 3.Days().Ago(), profile2);
    //        randomUser3 = CreateRandomUser(userName3, applicationName, "email3@email.com", true, 1.Hours().Ago(), profile3);

    //        myList.Add(randomUser1);
    //        myList.Add(randomUser2);
    //        myList.Add(randomUser3);
    //        return myList;
    //    }
    //    private static Profile CreateRandomProfile(bool isAnonymous, DateTime lastActivityDate)
    //    {
    //        return new Profile()
    //                   {
    //                       ApplicationName = applicationName,
    //                       BirthDate = DateTime.Now,
    //                       Street = "Street",
    //                       City = "City",
    //                       State = "State",
    //                       Zip = "Zip",
    //                       Country = "Country",
    //                       FirstName = "FirstName",
    //                       LastName = "LastName",
    //                       Gender = "Male",
    //                       IsAnonymous = isAnonymous,
    //                       Language = "Language",
    //                       LastActivityDate = lastActivityDate,
    //                       LastUpdatedDate = DateTime.Now,
    //                       Subscription = "Subscription",
    //                       Occupation = "Occupation",
    //                       Website = "Website"
    //                   };
    //    }
    //    private static User CreateRandomUser(string userName, string applicationname, string emailAddress, bool isOnline, DateTime lastActivityDate, Profile profile)
    //    {
    //        var user = new User()
    //                       {
    //                           Username = userName,
    //                           ApplicationName = applicationname,
    //                           Comment = "comment",
    //                           CreationDate = DateTime.Now,
    //                           Email = emailAddress,
    //                           FailedPasswordAnswerAttemptCount = 0,
    //                           FailedPasswordAnswerAttemptWindowStart = DateTime.Now,
    //                           FailedPasswordAttemptCount = 0,
    //                           FailedPasswordAttemptWindowStart = DateTime.Now,
    //                           IsApproved = true,
    //                           Password = "password",
    //                           PasswordQuestion = "PasswordQuestion",
    //                           PasswordAnswer = "PasswordAnswer",
    //                           LastActivityDate = lastActivityDate,
    //                           LastLoginDate = DateTime.Now,
    //                           LastPasswordChangedDate = DateTime.Now,
    //                           IsOnLine = isOnline,
    //                           IsLockedOut = false,
    //                           LastLockedOutDate = DateTime.Now,

    //                       };
    //        user.AddProfile(profile);
    //        return user;
    //    }

    //    protected static ProfileRepository repository;
    //    protected static string applicationName;
    //    protected static string userName1;
    //    protected static string userName2;
    //    protected static string userName3;
    //    protected static User randomUser1;
    //    protected static User randomUser2;
    //    protected static User randomUser3;
    //    protected static Profile profile1;
    //    protected static Profile profile2;
    //    protected static Profile profile3;
    //    protected static IList<User> userList;
    //}

    //[Subject(typeof(ProfileRepository))]
    //public class When_users_and_profiles_exists_and_searching_for_a_profile_by_userId_isAnonymous_and_applicationName : With_Seeded_Users_and_Profiles
    //{
    //    Because of = () => WithUOW(() => { profile = repository.GetProfile(randomUser1.Id, profile1.IsAnonymous, applicationName); });

    //    It should_return_the_profile = () => profile.ShouldEqual(randomUser1.Profile);
        
    //    private static Profile profile;
    //}


    //[Subject(typeof(ProfileRepository))]
    //public class When_users_and_profiles_exists_and_searching_for_profiles_by_userInactiveSinceDate_isAnonymous_and_applicationName : With_Seeded_Users_and_Profiles
    //{
    //    Because of = () => WithUOW(() => { profiles = repository.GetProfiles(7.Hours().Ago(), false, applicationName); });

    //    It should_return_a_list_of_profiles = () => profiles.Count.ShouldEqual(2);

    //    private static IList<Profile> profiles;
    //}



    //[Subject(typeof(ProfileRepository))]
    //public class When_users_and_profiles_exists_and_searching_for_profiles_by_applicationName : With_Seeded_Users_and_Profiles
    //{
    //    Because of = () => WithUOW(() => { profiles = repository.GetProfiles(applicationName); });

    //    It should_return_a_list_of_profiles = () => profiles.Count.ShouldEqual(3);

    //    private static IList<Profile> profiles;
    //}




    //[Subject(typeof(ProfileRepository))]
    //public class When_removing_all_seeded_user_and_profiles : With_Seeded_Users_and_Profiles
    //{

    //    Because of = () =>
    //    {
    //        WithUOWAndTransaction(() => { foreach (var user in userList) new UserRepository().Delete(user); });
    //        WithUOW(() => profiles = repository.GetProfiles(applicationName));
    //    };

    //    It should_be_able_to_delete_all_seeded_user_and_profiles = () => profiles.Count.ShouldEqual(0);


    //    private static IList<Profile> profiles;
    //}
}
