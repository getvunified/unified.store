using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

using Jhm.Web.UI.IBootStrapperTasks;
using Machine.Specifications;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;
using StructureMap;
using Environment = System.Environment;

namespace Jhm.Web.Specs.Integration.Modules.MiscSpecs
{
    public class when_running_schema_update
    {
        Establish context = () =>
                                {
                                    new ConfigureDependencies().Execute();
                                    config = Fluently.Configure().Database(ObjectFactory.GetInstance<IPersistenceConfigurer>()).Mappings(
                                            ObjectFactory.GetInstance<Action<MappingConfiguration>>()).
                                            BuildConfiguration();
                                };

        Because of = () =>
                                 {
                                     ddl = config.GenerateSchemaCreationScript(new MsSql2008Dialect());

                                     string ddlFilePathName = Environment.CurrentDirectory + @"\ddl.sql";
                                     Console.WriteLine("\n" + ddlFilePathName);
                                     new SchemaExport(config).SetOutputFile(ddlFilePathName).Create(true, false);
                                 };

        It should_be_able_to_get_an_updated_schema = () => ddl.ShouldNotBeEmpty();

        private static Configuration config;
        private static string[] ddl;
    }


    public class When_you_navigate_the_website_into_inner_pages
    {
        It should_show_breadcrumbs_that_allow_you_to_easily_navigate_back_to_the_path_you_came_from;
    }
}