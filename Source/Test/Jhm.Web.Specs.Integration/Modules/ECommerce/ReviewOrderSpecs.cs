using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Modules.ECommerce.ReviewOrder
{
    public class concern : Unit_BaseControllerSpecBase<CatalogController, IServiceCollection>
    {

    }

    [Subject(typeof(CatalogController))]
    public class when_placing_the_order: concern
    {
        It should_authorize_the_credit_card_on_file_for_the_total_amount_in_the_shopping_cart;
    }

}