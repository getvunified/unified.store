using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Modules.ECommerce.CheckOutSpecs
{
    public class concern : Unit_BaseControllerSpecBase<CatalogController, IServiceCollection>
    {

    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_signs_in_with_an_unknown_email_address_and_chooses_to_create_a_password : concern
    {
        It should_create_an_account_for_the_user;
    }

    [Subject(typeof(CatalogController))]
    public class when_calulating_shipping : concern
    {
        It should_get_the_shipping_rate_based_on_the_entered_Shipping_address;
    }

    [Subject(typeof(CatalogController))]
    public class when_placing_the_order
    {
        It should_authorize_the_credit_card_on_file_for_the_total_amount_in_the_shopping_cart;
    }

}