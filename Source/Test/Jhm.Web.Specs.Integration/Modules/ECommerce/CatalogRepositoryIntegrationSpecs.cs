using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Modules.ECommerce.CatalogRepositoryIntegrationSpecs
{
    public class concern
    {
        

    }

    [Subject(typeof(CatalogController))]
    public class when_viewing_a_list_of_products : concern
    {
        It should_be_sortable_by_Availablity;
        It should_be_sortable_by_Alpha_Product_Title;
        It should_be_sortable_by_Alpha_Author_Name;
        It should_be_sortable_by_Alpha_Product_Code;
        It should_be_sortable_by_Price;
        It should_be_sortable_by_Has_Images;
        It should_be_sortable_by_Rating;
        It should_be_sortable_by_Popular;
        It should_be_sortable_by_Downloadable;
        It should_be_sortable_by_Has_Trailer;
    }

    [Subject(typeof(CatalogController))]
    public class when_searching_for_a_product : concern
    {
        It should_be_able_to_search_by_Product_Title;
        It should_be_able_to_search_by_Availablity;
        It should_be_able_to_search_by_Author_Name;
        It should_be_able_to_search_by_Product_Code;
        It should_be_able_to_search_by_Price;
        It should_be_able_to_search_by_Has_Images;
        It should_be_able_to_search_by_Rating;
        It should_be_able_to_search_by_if_it_is_Downloadable;
        It should_be_able_to_search_by_if_it_has_a_Trailer;
    }

}