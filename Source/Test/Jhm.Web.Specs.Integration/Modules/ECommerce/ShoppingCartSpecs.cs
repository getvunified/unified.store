using Jhm.Web.mSpec;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Machine.Specifications;

namespace Jhm.Web.Specs.Integration.Modules.ECommerce.ShoppingCartSpecs
{
    public class concern : Unit_BaseControllerSpecBase<CatalogController, IServiceCollection>
    {

    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_ends_their_session_and_doesnt_place_order : concern
    {
        It should_save_the_Shopping_Cart_to_the_database;
    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_signs_in_and_has_a_saved_shopping_cart_and_is_INSIDE_the_Save_Shopping_Cart_timeline : concern
    {
        It should_update_the_Cart_link_with_quantity_of_item_in_saved_shopping_cart;
        It should_show_the_details_of_the_saved_shopping_cart;
    }

    [Subject(typeof(CatalogController))]
    public class when_a_user_signs_in_and_has_a_saved_shopping_cart_and_is_OUTSIDE_the_Save_Shopping_Cart_timeline : concern
    {
        It should_present_the_user_with_an_empty_shopping_cart;
        It should_delete_the_expired_shopping_cart_from_the_database;
    }
}