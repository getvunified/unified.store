﻿using System;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Service;

namespace Jhm.Web
{
    public class SessionData
    {
        private readonly IServiceCollection service;
        private readonly IUnitOfWorkContainer uow;

        public SessionData()
        {
            service = DependencyResolver.Current.GetService<IServiceCollection>();
            uow = DependencyResolver.Current.GetService<IUnitOfWorkContainer>();
            LoadCurrentUser();
        }

        private void LoadCurrentUser()
        {
            if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                using (uow.Start())
                {
                    if (uow.CurrentUOW.IsInTransaction)
                    {
                        CurrentUser = service.AccountService.GetUser(HttpContext.Current.User.Identity.Name);
                    }
                    else
                    {
                        using (uow.CurrentUOW.BeginTransaction())
                        {
                            CurrentUser = service.AccountService.GetUser(HttpContext.Current.User.Identity.Name);
                        }
                    }
                }
            }
        }

        private Core.Models.User _currentUser;
        public Core.Models.User CurrentUser
        {
            get
            {
                if (_currentUser == null && RequestIsAuthenticated)
                {
                    LoadCurrentUser();
                }
                return _currentUser;
            }
            set { _currentUser = value; }
        }

        public bool RequestIsAuthenticated { get { return HttpContext.Current.Request.IsAuthenticated; } }

        public Getv.Security.IUser SecurityUser { get; set; }

        public void ClearCurrentUser()
        {
            _currentUser = null;
            SecurityUser = null;
        }
    }
}
