﻿using System.Web;

namespace Jhm.Web
{
    public static class ApplicationManager
    {
        // User Values
        private static string _appData = "MyAppData";

        public static ApplicationData ApplicationData
        {
            get
            {
                if (HttpContext.Current.Application[_appData] == null)
                {
                    HttpContext.Current.Application.Lock();
                    HttpContext.Current.Application[_appData] = new ApplicationData();
                    HttpContext.Current.Application.UnLock();
                }
                return HttpContext.Current.Application[_appData] as ApplicationData;
            }
        }

    }
}