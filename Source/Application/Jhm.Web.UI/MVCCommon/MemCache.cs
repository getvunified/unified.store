﻿using System;
using System.Runtime.Caching;

namespace Jhm.Web.UI.MVCCommon {

    public static class MemCache {

        public static void SaveToCache(this Object cacheItem, String key)
        {
            var cache = MemoryCache.Default;

            //
            // This call will overwrite a pre-existing key/value if it is found.
            //
            cache.Set(key, cacheItem, new CacheItemPolicy {Priority = CacheItemPriority.NotRemovable});
        }

        public static Object GetFromCache(this String key)
        {
            var cache = MemoryCache.Default;

            return cache[key];
        }

        public static void RemoveFromCache(this String key)
        {
            try
            {
                var cache = MemoryCache.Default;
                cache.Remove(key);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { } // don't care if this fails
        }
    }
}