﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.Web.Core.Models;

namespace Jhm.Web
{
    public interface IWebUserHelper
    {
        User GetLoggedInUser();
    }
}
