﻿#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using Foundation;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

#endregion

namespace Jhm.Web
{
    public class ApplicationData
    {
        public ThemeCollection ThemeCollection = new ThemeCollection();

        public ApplicationData()
        {
            ReloadAllData();
        }

        public void ReloadAllData()
        {
            SetBanner();
            SetArtist();
            SetNews();
            SetPressKit();
            SetFeaturedPhotos();
            SetFeaturedVideos();
            SetSocialMedia();
            SetTheme(true);
            SetJobPostings();
        }

        private readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };


        private void SetArtist()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/artists.json");
                var root = JsonConvert.DeserializeObject<ArtistModel>(json, JsonSettings);
                Artists.Clear();
                foreach (var item in root.Data.Items)
                {
                    Artists.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetNews()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/stories.json");
                var root = JsonConvert.DeserializeObject<StoryModel>(json, JsonSettings);
                Stories.Clear();
                foreach (var item in root.Data.Items)
                {
                    Stories.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetPressKit()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/presskits.json");
                var root = JsonConvert.DeserializeObject<PressKitModel>(json, JsonSettings);
                PressKits.Clear();
                foreach (var item in root.Data.Items)
                {
                    PressKits.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetBanner()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/banners.json");
                var root = JsonConvert.DeserializeObject<BannerModel>(json, JsonSettings);
                Banners.Clear();
                foreach (var item in root.Data.Items)
                {
                    Banners.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetFeaturedVideos()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/featuredVideos.json");
                var root = JsonConvert.DeserializeObject<FeaturedVideoModel>(json, JsonSettings);
                FeaturedVideos.Clear();
                foreach (var item in root.Data.Items)
                {
                    item.Id = item.Id ?? Guid.NewGuid().ToString();
                    FeaturedVideos.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetFeaturedPhotos()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/featuredPictures.json");
                var root = JsonConvert.DeserializeObject<FeaturedPhotoModel>(json, JsonSettings);
                FeaturedPhotos.Clear();
                foreach (var item in root.Data.Items)
                {
                    item.Id = item.Id ?? Guid.NewGuid().ToString();
                    FeaturedPhotos.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        private void SetSocialMedia()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/SocialMedia.json");
                var root = JsonConvert.DeserializeObject<SocialMediaItemModel>(json, JsonSettings);
                SocialMediaItems.Clear();
                foreach (var item in root.Data.Items)
                {
                    item.Id = item.Id ?? Guid.NewGuid().ToString();
                    SocialMediaItems.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }


        public void SetJobPostings()
        {
            try
            {
                var json = WebUtilities.GetStringFromUrl(@"http://content.jhm.org/dm/jobpostings.json");
                var root = JsonConvert.DeserializeObject<JobPostingJsonModel>(json, JsonSettings);
                JobPostings.Clear();
                foreach (var item in root.Data.Items)
                {
                    JobPostings.Add(item.Id, item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

        }

        public void SetTheme(bool force = false)
        {
            CurrentTheme = ThemeCollection.GetActiveTheme(forceRefresh: force);
        }

        public void SetTheme(string themeName)
        {
            CurrentTheme = themeName;
        }

        public string CurrentTheme
        {
            get
            {
                if (_currentTheme.IsNullOrEmpty())
                {
                    _currentTheme = "Classic";
                }

                return _currentTheme;
            }

            set
            {
                _currentTheme = value;
            }
        }

        private Dictionary<string, PressKitItem> _pressKits;
        private Dictionary<string, Story> _stories;
        private Dictionary<string, Artist> _artists;
        private Dictionary<string, BannerItem> _banners;
        private Dictionary<string, FeaturedVideo> _featuredVideos;
        private Dictionary<string, FeaturedPhoto> _featuredPhotos;
        private Dictionary<string, SocialMediaItem> _socialMedia;
        private Dictionary<string, JobPosting> _jobPostings;
        private string _firstDayOfMonthOfNovember;
        private string _currentTheme;

        public Dictionary<string, BannerItem> Banners
        {
            get { return _banners ?? (_banners = new Dictionary<string, BannerItem>()); }
        }

        public Dictionary<string, PressKitItem> PressKits
        {
            get { return _pressKits ?? (_pressKits = new Dictionary<string, PressKitItem>()); }
        }

        public Dictionary<string, Story> Stories
        {
            get { return _stories ?? (_stories = new Dictionary<string, Story>()); }
        }

        public Dictionary<string, JobPosting> JobPostings
        {
            get { return _jobPostings ?? (_jobPostings = new Dictionary<string, JobPosting>()); }
        }

        public Dictionary<string, Artist> Artists
        {
            get { return _artists ?? (_artists = new Dictionary<string, Artist>()); }
        }

        public Dictionary<string, FeaturedVideo> FeaturedVideos
        {
            get { return _featuredVideos ?? (_featuredVideos = new Dictionary<string, FeaturedVideo>()); }
        }

        public Dictionary<string, FeaturedPhoto> FeaturedPhotos
        {
            get { return _featuredPhotos ?? (_featuredPhotos = new Dictionary<string, FeaturedPhoto>()); }
        }

        public Dictionary<string, SocialMediaItem> SocialMediaItems
        {
            get { return _socialMedia ?? (_socialMedia = new Dictionary<string, SocialMediaItem>()); }
        }

        private ReadOnlyCollection<HomePagePopupAnnouncement> HomePagePopups
        {

            get
            {
                ObjectCache cache = MemoryCache.Default;
                var homePagePopups = cache["HomePagePopups"] as ReadOnlyCollection<HomePagePopupAnnouncement>;
                return homePagePopups ?? GetHomePagePopups();
            }
        }

        public HomePagePopupAnnouncement ActiveHomePagePopup
        {
            get
            {
                var activePopup = HomePagePopups.FirstOrDefault(x => (x.StartDate < DateTime.Now) && (x.EndDate > DateTime.Now));
				//var activePopup = HomePagePopups.FirstOrDefault();
                return activePopup;
            }
        }

        public ReadOnlyCollection<HomePagePopupAnnouncement> GetHomePagePopups()
        {
            var result = new List<HomePagePopupAnnouncement>();

            ObjectCache cache = MemoryCache.Default;
            //var homePagePopupsJsonUrl = cache["HomePagePopups"] as string;
            var homePagePopupsJsonUrl = ConfigurationManager.AppSettings["Global.HomePagePopups"];

            try
            {
                var json = WebUtilities.GetStringFromUrl(homePagePopupsJsonUrl);
                var root = JsonConvert.DeserializeObject<ApiDataWrapper<HomePagePopupAnnouncement>>(json, JsonSettings);
                foreach (var item in root.Data.Items)
                {
                    //item.Id = item.Id ?? Guid.NewGuid().ToString();
                    result.Add(item);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                //throw;
            }

            var readOnlyResult = result.AsReadOnly();
            UpdateCache(readOnlyResult);

            return readOnlyResult;
        }


        private void UpdateCache(ReadOnlyCollection<HomePagePopupAnnouncement> homePagePopups)
        {
            ObjectCache cache = MemoryCache.Default;

            var homePagePopupsFile = ConfigurationManager.AppSettings["Global.HomePagePopupsFile"];
            var policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(10.0);

            var filePaths = new List<string> { homePagePopupsFile };
          //  policy.ChangeMonitors.Add(new HostFileChangeMonitor(filePaths));

            cache.Set("HomePagePopups", homePagePopups, policy);

        }
    }
}