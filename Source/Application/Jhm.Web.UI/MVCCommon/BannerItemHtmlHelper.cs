﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Jhm.Web.Core.Models;

namespace Jhm.Common.Utilities.HTMLHelpers
{
    public static class BannerItemHtmlHelper
    {
        public static string BannerItemImage(this HtmlHelper helper, HomeBannerItem bannerItem)
        {
            if ( bannerItem == null )
            {
                return string.Empty;
            }
            var tag = new TagBuilder("img");
            tag.MergeAttribute("src", bannerItem.ImageUrl);
            tag.MergeAttribute("name", bannerItem.Title);
            tag.MergeAttribute("alt", bannerItem.SubTitle);
            tag.MergeAttribute("width", "900");
            tag.MergeAttribute("height", "400");
            if ( !String.IsNullOrEmpty(bannerItem.ImageLinkUrl) )
            {
                tag.MergeAttribute("class", "link");
                tag.MergeAttribute("rel", bannerItem.ImageLinkUrl);
                if ( bannerItem.OpenTargetInNewWindow )
                {
                    tag.MergeAttribute("target", "_blank");
                }
            }

            if ( !String.IsNullOrEmpty(bannerItem.CustomAttributes) )
            {
                var attributesRegex = new Regex(@"(\w+)=(?:[""']?([^""'>=]*)[""']?)");
                var matches = attributesRegex.Matches(bannerItem.CustomAttributes);
                foreach (var myMatch in matches.Cast<Match>().Where(myMatch => myMatch.Groups.Count > 2))
                {
                    tag.MergeAttribute(myMatch.Groups[1].Value, myMatch.Groups[2].Value);
                }
            }
            return tag.ToString(TagRenderMode.SelfClosing);
        }

        public static string ExternalHtmlFor(this HtmlHelper helper, IEnumerable<HomeBannerItem> bannerItems)
        {
            var sb = new StringBuilder();
            foreach (var bannerItem in bannerItems)
            {
                sb.Append(bannerItem.ExternalHtml);
            }
            return new HtmlString(sb.ToString()).ToHtmlString();
        }
    }
}
