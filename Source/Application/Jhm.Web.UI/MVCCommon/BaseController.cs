#region

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using Jhm.em3.Core;
using getv.donorstudio.core.Global;

#endregion

namespace Jhm.Web.UI.Controllers
{
    public delegate ActionResult ViewEvent();

    public enum PageTypes
    {
        Default,
        Facebook
    }

    public abstract class BaseController<S> : Controller, IWebUserHelper where S : IServiceCollection
    {
        protected readonly S service;
        protected readonly IUnitOfWorkContainer unitOfWork;
        
        public delegate ActionResult DerivedTryCatch(Exception ex);

        private DerivedTryCatch derivedCatch;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ApplicationManager.ApplicationData.SetTheme();

            object client = TempData[ClientModelBinder.clientSessionKey];
            if (client == null)
            {
                client = new ClientModelBinder().LoadClientInContext(filterContext);
            }
            filterContext.Controller.ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;

            string hostname = Request.Url.Host;

            //DM-SWITCH
            //IsDM = true;
            if ( hostname.Contains("differencemedia.org"))
            {
                IsDM = true;
            }
        
            base.OnActionExecuting(filterContext);
        }


        /**
         * Test for Differnece Media Redirect
         * 
         * 
         */

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var action = filterContext.Result as ViewResult;

            if (action != null)
            {
                if (IsDM) action.MasterName = "DifferenceMedia";
                else if (action.MasterName == PageTypes.Default.ToString()) action.MasterName = string.Empty;
            }

            

            base.OnActionExecuted(filterContext);
        }

        //is difference media
        protected bool IsDM
        {
            get; 
            private set;
        }

        protected void UpdateClientCompany(Company newCompany)
        {
            if (ViewData[MagicStringEliminator.ViewDataKeys.Client] != null)
            {
                ((Client) ViewData[MagicStringEliminator.ViewDataKeys.Client]).Company = newCompany;
            }
        }

        protected BaseController(S service, IUnitOfWorkContainer unitOfWork)
        {
            this.service = service;
            this.unitOfWork = unitOfWork;
        }

        public void TryWithTransaction(Action method)
        {
            try
            {
                using (unitOfWork.Start())
                {
                    if (unitOfWork.CurrentUOW.IsInTransaction)
                    {
                        method();
                    }
                    else
                    {
                        using (ITransaction tx = unitOfWork.CurrentUOW.BeginTransaction())
                        {
                            try
                            {
                                method();
                                tx.Commit();
                            }
                            catch
                            {
                                tx.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.For(this).Erroneous(
                    "[TryWithTransaction]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}", e.Message,
                    e.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url);
                throw;
            }
        }

        public ActionResult TryOnEventWithoutTransaction(ViewEvent method)
        {
            try
            {
                ActionResult result = method();
                return result;
            }
            catch (Exception ex)
            {
                try
                {
                    if (derivedCatch.IsNot().Null())
                    {
                        Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                        return derivedCatch(ex);
                    }
                    throw;
                }
                catch (Exception e)
                {
                    Log.For(this).Erroneous(
                        "[TryOnEventWithoutTransaction]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}",
                        e.Message, e.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url);
                    return View("Error", new HandleErrorInfo(e, GetControllerName(), GetActionName(method)));
                }
            }
        }

        public ActionResult TryOnEvent(ViewEvent method)
        {
            try
            {
                using (unitOfWork.Start())
                {
                    if (unitOfWork.CurrentUOW.IsInTransaction)
                    {
                        return method();
                    }
                    
                    using (ITransaction tx = unitOfWork.CurrentUOW.BeginTransaction())
                    {
                        try
                        {
                            ActionResult result = method();
                            tx.Commit();
                            return result;
                        }
                        catch
                        {
                            tx.Rollback();
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string cartItems = String.Empty;
                if (method.Method.DeclaringType.FullName != null &&
                    method.Method.DeclaringType.FullName.Contains("CheckoutController"))
                {
                    var client = (Client) ControllerContext.HttpContext.Session[ClientModelBinder.clientSessionKey];
                    if (client != null)
                    {
                        cartItems = client.Cart.Items.Aggregate(cartItems,
                                                                (current, item) =>
                                                                current +
                                                                ("{Code: " + item.Item.Code + ", Title: " +
                                                                 item.Item.Title + "}\n"));
                    }
                }
                try
                {
                    if (derivedCatch.IsNot().Null())
                    {
                        Log.For(this).Erroneous(
                            "[TryOnEvent1]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}\nCart Items: \n{4}",
                            ex.Message, ex.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url, cartItems);
                        return derivedCatch(ex);
                    }
                    throw;
                }
                catch (Exception e)
                {
                    Log.For(this).Erroneous(
                        "[TryOnEvent2]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}\nCart Items: {4}",
                        e.Message, e.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url, cartItems);
                    return View("Error", new HandleErrorInfo(e, GetControllerName(), GetActionName(method)));
                }
            }
        }

        public void SetDerivedCatch(DerivedTryCatch derivedTryCatch)
        {
            derivedCatch = derivedTryCatch;
        }

        private string GetActionName(ViewEvent method)
        {
            return method.Method.ToString().Replace(method.Method.ReturnParameter.ToString(), "");
        }

        private string GetControllerName()
        {
            return GetType().Name.Replace(GetType().Assembly.FullName, "");
        }

        public CultureInfo GetCultureInfo()
        {
            return Thread.CurrentThread.CurrentCulture;
        }

        public string GetApplicationName()
        {
            var config = new NameValueCollection(WebConfigurationManager.AppSettings);
            if (config == null)
                throw new ArgumentNullException("config");

            return GetConfigValue(config["applicationName"],
                                  System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            string loggerName = string.Format("{0}Controller.{1}", controller, action);

            string stackTrace = "StackTrace Not Available";
            Exception lastError = filterContext.HttpContext.Server.GetLastError();
            if (lastError.IsNot().Null())
                stackTrace = lastError.StackTrace;

            Log.For(this).Erroneous(string.Format("Location:\n{0}\nException:\n{1}\nStack:\n{2}", loggerName,
                                                  filterContext.Exception, stackTrace));
            filterContext.ExceptionHandled = true;
            View("Error", new HandleErrorInfo(filterContext.Exception, controller, action)).ExecuteResult(ControllerContext);
        }

        protected override void HandleUnknownAction(string actionName)
        {
            if (actionName.ToLower().EndsWith(".asp"))
            {
                Response.StatusCode = 301;
                Response.Redirect("/");
                return;
            }


            base.HandleUnknownAction(actionName);
        }

        protected bool EMailSignUp(ENewsLetterSignUpViewModel data)
        {
            bool ReturnValue = true;

            if (ModelState.IsValid)
            {
                const string connectionString =
                    "Data Source=JS-SQL1;Initial Catalog=JhmDonorStudioBusiness;User Id=EmailCampaign;Password=MG2877;";
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    const string sql = @"INSERT INTO dbo.[JHM_AccountEmailLists] (List,EmailAddress,FirstName,LastName,
                                    AccountNumber, 
                                    OptOut, 
                                    HardBounce, 
                                    SoftBounceGeneral, 
                                    SoftBounceDNSFailure, 
                                    SoftBounceMailboxFull, 
                                    SoftBounceMessageSizeTooLarge, 
                                    GeneralBounce, 
                                    MailBlockGeneral, 
                                    MailBlockKnownSpammer, 
                                    MailBlockSpamDetected, 
                                    MailBlockAttachmentDetected, 
                                    MailBlockRelayDenied, 
                                    AutoReply, 
                                    TransientBounce, 
                                    SubscribeRequest, 
                                    UnsubscribeRequest, 
                                    ChallengeResponse, 
                                    BounceButNoEmailAddressReturned, 
                                    NonBounce 
                                    ) 
                                VALUES (@listCode,@email,@firstName,@lastName, @accountNumber,
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0,   
                                    0)";
                    using (var comm = new SqlCommand(sql, conn))
                    {
                        comm.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = data.Email;
                        comm.Parameters.Add("@firstName", SqlDbType.NVarChar, 300).Value =
                            String.IsNullOrEmpty(data.FirstName) ? (object) DBNull.Value : data.FirstName;
                        comm.Parameters.Add("@lastName", SqlDbType.NVarChar, 300).Value =
                            String.IsNullOrEmpty(data.LastName) ? (object) DBNull.Value : data.LastName;
						comm.Parameters.Add("@listCode", SqlDbType.VarChar, 10);
                        
                        comm.Parameters.Add("@accountNumber", SqlDbType.Int).Value = data.AccountNumber;

                        // Future use - original code below was looking for a collection of email lists. Not implimented. JFE
                        if (data.SelectedEmailLists!=null)
                        {

                            foreach (var emailList in data.SelectedEmailLists)
                            {
                                comm.Parameters["@listCode"].Value = emailList;
                                try
                                {
                                    comm.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("IX_JHM_AccountEmailLists_Uniqueness"))
                                    {
                                        // Primary Key constraint error ignored
                                    }
                                    else
                                    {
                                        // other exceptions are not ignored
                                        ReturnValue = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // add a single email list subscription if provided. Updated 8/12/2014 JFE
                            if (data.EmailListCode!=null)
                            {
                                comm.Parameters["@listCode"].Value = data.EmailListCode;
                                try
                                {
                                    comm.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("IX_JHM_AccountEmailLists_Uniqueness"))
                                    {
                                        // Primary Key constraint error ignored
                                    }
                                    else
                                    {
                                        // other exceptions are not ignored
                                        ReturnValue = false;
                                    }
                                }
                            }
                        }
                    }
                }
                //return Redirect("/");


            }
            ViewData["StateList"] = new SelectList(State.UnitedStates, "ID", "Name");
            return ReturnValue;
            //return View("ENewsLetterSignUpForm", data);
        }


        public ActionResult TryCatch(Exception ex)
        {
            try
            {
                throw (ex);
            }
            catch (DonationCartItemExistsException e)
            {
                return RedirectToCartPage(DonationCartItemExistsException.DonationAlreadyExists);
            }
        }

        protected ViewResult MapCartRenderView(Client client, string viewName, CartViewModel model, bool doLightMapping = true)
        {
            model.RegisterClient(client);
            model.MapCart(client.Cart, doLightMapping);
            if (!String.IsNullOrEmpty(client.Cart.DiscountErrorMessage))
            {
                var popup = new PopupMessageViewModel
                    {
                        Message = FormatHelper.ReplaceCurrencyFormat(client.Cart.DiscountErrorMessage, client),
                        Title = "Shopping Cart Message"
                    };
                client.Cart.ClearDiscountErrorMessage();
                AddPopupMessage(popup);
            }
            model.IsDifferenceMediaPage = IsDM;
            return View(viewName, model);
        }

        protected Cart GetCart()
        {
            return service.CartService.GetCart(User);
        }

        protected ActionResult RedirectToCartPage(string eMessage = null, string catalogRtrToUrl = null,
                                                  string donationsRtrToUrl = null)
        {
            catalogRtrToUrl = catalogRtrToUrl ?? "/" + MagicStringEliminator.Routes.Catalog.Route;
            donationsRtrToUrl = donationsRtrToUrl ?? "/" + MagicStringEliminator.Routes.Donation_Opportunities.Route;
            return
                RedirectToRoute(
                    new
                        {
                            controller = "Checkout",
                            action = MagicStringEliminator.CheckoutActions.Cart,
                            errorMessage = eMessage,
                            catalogReturnToUrl = catalogRtrToUrl,
                            donationsReturnToUrl = donationsRtrToUrl
                        });
        }

        protected string DetermineDSEnvironment(User user, Client client)
        {
            return String.IsNullOrEmpty(user.DsEnvironment) ? client.Company.DonorStudioEnvironment : user.DsEnvironment;
        }

        public User GetLoggedInUser()
        {
            if (Request.IsAuthenticated)
            {
                User user = null;
                TryWithTransaction(
                    () => { user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name); });
                return user;
            }
            return null;
        }

        protected void ClearRegisteredClient()
        {
            Session.Remove(ClientModelBinder.clientSessionKey);
        }

        protected void AddPopupMessage(PopupMessageViewModel popupMessage)
        {
            TempData[MagicStringEliminator.ViewDataKeys.PopupMessage] = popupMessage;
        }

        protected void AddPopupMessage(string message, string title)
        {
            var popup = new PopupMessageViewModel {Message = message, Title = title};
            TempData[MagicStringEliminator.ViewDataKeys.PopupMessage] = popup;
        }


        protected void AddHeaderMessage(string message, string type)
        {
            TempData[type] = message;
        }

        protected PopupMessageViewModel GetCurrentPopupMessage()
        {
            return (PopupMessageViewModel) TempData[MagicStringEliminator.ViewDataKeys.PopupMessage];
        }

        protected ActionResult DoLogon(LogOnModel model, Client client)
        {
            service.FormsService.SignIn(model.UserName, model.RememberMe);
            User user = service.AccountService.GetUser(model.UserName);

            CheckAndSetDSEnvironment(user,client);

            Session[MagicStringEliminator.SessionKeys.LoggedUserFullName] = "{0} {1} {2}".FormatWith(user.Profile.Title, user.Profile.FirstName, user.Profile.LastName);
            if (user.MustChangePasswordOnLogin)
            {
                return RedirectToAction("ChangePassword", "Account");
            }

            var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];
            if (redirectRoute != null)
            {
                Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                return RedirectToRoute(redirectRoute);
            }

            var test = new ReadOnlyCollection<Int32>(new List<int>());

            // Returns null if no redirect was necessary
            return null;
        }

        protected ActionResult DoSecurityUserLogon(ILogOnModel model, Client client)
        {
            service.FormsService.SignIn(model.UserName, model.RememberMe);
            var user = service.AccountService.GetUserByGlobalId(model.GlobalUserId);

            if (user.IsTransient())
            {
                return null;
            }
            CheckAndSetDSEnvironment(user, client);
        

            if (user.MustChangePasswordOnLogin)
            {
                return RedirectToAction("ChangePassword", "Account");
            }

            var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];
            if (redirectRoute != null)
            {
                Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                return RedirectToRoute(redirectRoute);
            }

            // Returns null if no redirect was necessary
            return null;
        }

        protected void CheckAndSetDSEnvironment(User user, Client client)
        {
            if (user.DsEnvironment != null && user.DsEnvironment != client.Company.DonorStudioEnvironment)
            {
                ClearRegisteredClient();
            }
            if (user.AccountNumber == 0 && user.Profile.PrimaryAddress == null)
            {
                return;
            }
            if (user.AccountNumber == 0 &&
                Country.FindByISOCode(user.Profile.PrimaryAddress.Country).Company == Company.JHM_Canada)
            {
                var popupMessage = new PopupMessageViewModel();
                popupMessage.Title = "Important Message";
                popupMessage.Message =
                    "We noticed that you have a Canadian address and so we have updated you to our JHM Canada system.  Future transactions will now be processed by JHM Canada and will be in Canadian Dollars.  Thank you for your continued support!";
                popupMessage.Buttons.Add(new HtmlAnchor
                    {
                        InnerText = "Verify my account information",
                        HRef = String.Format("location.href = '/{0}';", MagicStringEliminator.Routes.MyAccount.Route)
                    });
                popupMessage.Buttons.Add(new HtmlAnchor {InnerText = "Ok", HRef = "$(this).dialog('close');"});
                AddPopupMessage(popupMessage);

                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_Canada.DonorStudioEnvironment;
                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
            else if (user.AccountNumber == 0 &&
                     Country.FindByISOCode(user.Profile.PrimaryAddress.Country).Company == Company.JHM_UnitedKingdom)
            {
                var popupMessage = new PopupMessageViewModel();
                popupMessage.Title = "Important Message";
                popupMessage.Message =
                    "We noticed that you have an address outside the United States and so we have updated you to our JHM British system to offer you a better service. Future transactions will now be processed by JHM United Kingdom and will be in the British Pound Sterling currency. Thank you for your continued support!";
                popupMessage.Buttons.Add(new HtmlAnchor
                    {
                        InnerText = "Verify my account information",
                        HRef =
                            String.Format("location.href = '/{0}';", MagicStringEliminator.Routes.MyAccount.Route)
                    });
                popupMessage.Buttons.Add(new HtmlAnchor {InnerText = "Ok", HRef = "$(this).dialog('close');"});
                AddPopupMessage(popupMessage);

                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_UnitedKingdom.DonorStudioEnvironment;
                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).
                        AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
            else if ( user.AccountNumber == 0 )
            {
                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_UnitedStates.DonorStudioEnvironment;
                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).
                        AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
        }

        protected void ChangeDsEnvirontmentPrimaryAddress(User user, Client client, string newEnvironment, Address newPrimaryAddress)
        {
            if (user.DsEnvironment != newEnvironment)
            {
                ClearRegisteredClient();
            }

            if (Country.FindByISOCode(newPrimaryAddress.Country).Company == Company.JHM_Canada)
            {
                var popupMessage = new PopupMessageViewModel();
                popupMessage.Title = "Important Message";
                popupMessage.Message =
                    "We noticed that you have a Canadian address and so we have updated you to our JHM Canada system.  Future transactions will now be processed by JHM Canada and will be in Canadian Dollars.  Thank you for your continued support!";
                popupMessage.Buttons.Add(new HtmlAnchor
                {
                    InnerText = "Verify my account information",
                    HRef = String.Format("location.href = '/{0}';", MagicStringEliminator.Routes.MyAccount.Route)
                });
                popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Ok", HRef = "$(this).dialog('close');" });
                AddPopupMessage(popupMessage);

                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_Canada.DonorStudioEnvironment;
                user.Profile.PrimaryAddress = newPrimaryAddress;

                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
            else if (Country.FindByISOCode(newPrimaryAddress.Country).Company == Company.JHM_UnitedKingdom)
            {
                var popupMessage = new PopupMessageViewModel();
                popupMessage.Title = "Important Message";
                popupMessage.Message =
                    "We noticed that you have an address outside the United States and so we have updated you to our JHM British system to offer you a better service. Future transactions will now be processed by JHM United Kingdom and will be in the British Pound Sterling currency. Thank you for your continued support!";
                popupMessage.Buttons.Add(new HtmlAnchor
                {
                    InnerText = "Verify my account information",
                    HRef =
                        String.Format("location.href = '/{0}';", MagicStringEliminator.Routes.MyAccount.Route)
                });
                popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Ok", HRef = "$(this).dialog('close');" });
                AddPopupMessage(popupMessage);

                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_UnitedKingdom.DonorStudioEnvironment;
                user.Profile.PrimaryAddress = newPrimaryAddress;

                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).
                        AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
            else
            {
                //Creating DS account out of the information in our database
                string dsEnvironment = Company.JHM_UnitedStates.DonorStudioEnvironment;
                user.Profile.PrimaryAddress = newPrimaryAddress;

                long dsAccountNumber =
                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment,
                                                                                          DsAccountMapper.Map(user));
                long dsAddressId =
                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,
                                                                                              dsAccountNumber).
                        AddressId;
                user.AccountNumber = dsAccountNumber;
                user.DsEnvironment = dsEnvironment;
                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                user.Profile.ApplicationName = GetApplicationName();
                service.AccountService.SaveUser(user);
            }
        }
    }
}