﻿using System;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Service;

namespace Jhm.Web
{
    public static class SessionManager
    {
        // User Values
        private static string _sessionData = "SessionData";


        
        public static SessionData SessionData
        { get 
            {
                if (HttpContext.Current.Session[_sessionData] == null)
                {
                    HttpContext.Current.Session[_sessionData] = new SessionData();
                }

                return HttpContext.Current.Session[_sessionData] as SessionData;
            }
        }

        public static Getv.Security.IUser SecurityUser
        {
            get
            {
                if (SessionData.SecurityUser == null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var user = CurrentUser;
                    Guid globalId;
                    if (!Guid.TryParse(user.GlobalId, out globalId))
                    {
                        return new Getv.Security.User();
                    }
                    var service = DependencyResolver.Current.GetService<IServiceCollection>();
                    var result = service.SecurityService.LoadUserById(globalId);
                    if (result.IsSuccessful)
                    {
                        SessionData.SecurityUser = result.Data;
                    }
                    else
                    {
                        return new Getv.Security.User();
                    }
                }
                return SessionData.SecurityUser;
            }
            set { SessionData.SecurityUser = value; }
        }


        public static Jhm.Web.Core.Models.User CurrentUser
        {
            get { return SessionData.CurrentUser; }
            set { SessionData.CurrentUser = value; }
        }

        public static void ClearCurrentUser()
        {
            SessionData.ClearCurrentUser();
        }
    }
}