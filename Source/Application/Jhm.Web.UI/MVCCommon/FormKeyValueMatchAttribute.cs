using System;
using System.Reflection;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.UI.Controllers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FormKeyValueMatchAttribute : ActionNameSelectorAttribute
    {
        public string MatchFormKey { get; set; }
        public string MatchFormValue { get; set; }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            return controllerContext.HttpContext.Request[MatchFormKey] != null &&
                controllerContext.HttpContext.Request[MatchFormKey] == MatchFormValue;
        }
    }

    public static class FormKeyValueMatchAttributes
    {
        public const string ActionBottonClicked = "ActionBottonClicked";

        public static MvcHtmlString FormKeyValueMatchAttributeButton(this HtmlHelper helper, string buttonText, string value = null, string imageUrlPath = null, string hiddenFieldName = ActionBottonClicked)
        {
            value = value ?? buttonText;
            var buttonTag = new TagBuilder("input");

            if (imageUrlPath.IsNot().Null())
            {
                buttonTag.MergeAttribute("type", "image");
                buttonTag.MergeAttribute("src", imageUrlPath);
                buttonTag.MergeAttribute("alt", buttonText);
            }
            else
            {
                buttonTag.MergeAttribute("type", "submit");
                buttonTag.MergeAttribute("value", buttonText);
            }
            buttonTag.MergeAttribute("title", buttonText);
            buttonTag.MergeAttribute("onclick", "$('#{0}').val('{1}');".FormatWith(hiddenFieldName, value));

            return MvcHtmlString.Create(buttonTag.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FormKeyValueMatchAttributeHiddenField(this HtmlHelper helper, string hiddenFieldName = ActionBottonClicked)
        {
            var hiddenFieldTag = new TagBuilder("input");
            hiddenFieldTag.MergeAttribute("type", "hidden");
            hiddenFieldTag.MergeAttribute("name", hiddenFieldName);
            hiddenFieldTag.MergeAttribute("id", hiddenFieldName);

            return MvcHtmlString.Create(hiddenFieldTag.ToString(TagRenderMode.SelfClosing));
        }
    }
}