﻿using System;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using FluentNHibernate.Mapping;
using Recaptcha;

namespace Jhm.Web.UI.Recaptcha
{
    public enum CaptchaThemes
    {
        Default,
        White,
        Blackglass,
        Clean
    }
    public static class RecaptchaControlMvc
    {
        private static string publicKey;
        private static string privateKey;
        private static bool skipRecaptcha;

        public static string PublicKey
        {
            get { return publicKey; }
            set { publicKey = value; }
        }

        public static string PrivateKey
        {
            get { return privateKey; }
            set { privateKey = value; }
        }

        public static bool SkipRecaptcha
        {
            get { return skipRecaptcha; }
            set { skipRecaptcha = value; }
        }

        static RecaptchaControlMvc()
        {
            publicKey = ConfigurationManager.AppSettings["RecaptchaPublicKey"];
            privateKey = ConfigurationManager.AppSettings["RecaptchaPrivateKey"];
            if (!bool.TryParse(ConfigurationManager.AppSettings["RecaptchaSkipValidation"], out skipRecaptcha))
            {
                skipRecaptcha = false;
            }
        }

        public class CaptchaValidatorAttribute : ActionFilterAttribute
        {
            private const string CHALLENGE_FIELD_KEY = "recaptcha_challenge_field";
            private const string RESPONSE_FIELD_KEY = "recaptcha_response_field";

            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                // Remove next 3 lines to enable recaptcha validation
                filterContext.ActionParameters["captchaIsValid"] = true;
                base.OnActionExecuting(filterContext);
                return;

                var captchaValidator = new RecaptchaValidator
                {
                    PrivateKey = PrivateKey,
                    RemoteIP = filterContext.HttpContext.Request.UserHostAddress,
                    Challenge = filterContext.HttpContext.Request.Form[CHALLENGE_FIELD_KEY],
                    Response = filterContext.HttpContext.Request.Form[RESPONSE_FIELD_KEY]
                };
                try
                {
                    var recaptchaResponse = captchaValidator.Validate();
                    // this will push the result value into a parameter in our Action  
                    filterContext.ActionParameters["captchaIsValid"] = recaptchaResponse.IsValid;
                }
                catch (Exception ex)
                {
                    filterContext.ActionParameters["captchaIsValid"] = false;
                }
                

                base.OnActionExecuting(filterContext);
            }
        }

        public static string GenerateCaptcha(this HtmlHelper helper, CaptchaThemes theme = CaptchaThemes.White, string id = "recaptcha")
        {
            if (string.IsNullOrEmpty(publicKey) || string.IsNullOrEmpty(privateKey))
            {
                throw new ApplicationException("reCAPTCHA needs to be configured with a public & private key.");
            }

            var captchaControl = new RecaptchaControl
            {
                ID = id,
                Theme = theme.ToString().ToLower(),
                PublicKey = publicKey,
                PrivateKey = privateKey,
                CustomTranslations = new Attributes(){{"instructions_visual","Please enter the words:"}}
            };

            
            var htmlWriter = new HtmlTextWriter(new StringWriter());

            captchaControl.RenderControl(htmlWriter);

            return htmlWriter.InnerWriter.ToString();
        }
    }
}