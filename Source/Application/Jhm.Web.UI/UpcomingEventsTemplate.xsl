<?xml version="1.0" encoding="utf-8" ?>
<!--<!DOCTYPE stylesheet [ <!ENTITY nbsp "&#160;">]>-->
<xsl:stylesheet version="1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:e="urn:extension:xslt" exclude-result-prefixes="xhtml xsl xs e">
  <xsl:output method="html" indent="yes" encoding="utf-8" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:for-each select="//CPXmlSyndication/Record">
      <xsl:variable name="EventMonthYear">
        <xsl:call-template name="ShortDateToMonthAndYear">
          <xsl:with-param name="DateTime" select="Field[@Label='Event Date']"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="FullDate">
        <xsl:call-template name="FormatDateLong">
          <xsl:with-param name="DateTime" select="Field[@Label='Event Date']"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="EventDateAbove">
        <xsl:call-template name="ShortDateToMonthAndYear">
          <xsl:with-param name="DateTime" select="preceding-sibling::*[1]/Field[@Label='Event Date']"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="EventTime" select="normalize-space(Field[@Label='Event Time'])" />
      <xsl:variable name="ContactInformation" select="normalize-space(Field[@Label='Contact Information'])" />
      <xsl:variable name="CostInformation" select="normalize-space(Field[@Label='Cost Information'])" />
      <xsl:variable name="RegistrationInformation" select="normalize-space(Field[@Label='Registration Information'])" />
      



      <xsl:if test="$EventMonthYear != $EventDateAbove">
        <h1>
          <xsl:value-of select="$EventMonthYear"/>          
        </h1>
      </xsl:if>
      
      
      <h2>
        <xsl:value-of select="Field[@Label='Title']" disable-output-escaping="yes"/>
      </h2>
      <h4>
        <xsl:choose>
          <xsl:when test="string-length($EventTime) = 0">
            <xsl:value-of select="$FullDate"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$FullDate"/> - <xsl:value-of select="$EventTime"/>
          </xsl:otherwise>
        </xsl:choose>
      </h4>
      
          <table style="font-weight:normal;">
            <tr>
              <td valign="top" style="width:100px;">
                <b>Location</b>
              </td>
              <td>
                <xsl:value-of select="Field[@Label='Event Location']" disable-output-escaping="yes" />
              </td>
            </tr>
            <tr>
              <td valign="top">
                <b>Description</b>
              </td>
              <td>
                <xsl:value-of select="Field[@Label='Event Description']" disable-output-escaping="yes" />
              </td>
            </tr>
            <xsl:if test="string-length($ContactInformation) &gt; 0">
              <tr>
                <td valign="top">
                  <b>Contact Information:</b>
                </td>
                <td>
                  <xsl:value-of select="$ContactInformation" disable-output-escaping="yes" />
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="string-length($CostInformation) &gt; 0">
              <tr>
                <td valign="top">
                  <b>Cost Information:</b>
                </td>
                <td>
                  <xsl:value-of select="$CostInformation" disable-output-escaping="yes" />
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="string-length($RegistrationInformation) &gt; 0">
              <tr>
                <td valign="top">
                  <b>Registration Information:</b>
                </td>
                <td>
                  <xsl:value-of select="$RegistrationInformation" disable-output-escaping="yes" />
                </td>
              </tr>
            </xsl:if>
          </table>
      <h5 />



    </xsl:for-each>

  </xsl:template>

  <xsl:template name="FormatDateLong">
    <!-- expected date format 1/20/2007 10:22:28 PM [OR] 01/20/2007 10:22:28 PM -->
    <xsl:param name="DateTime" />
    <!-- new date format January 20, 2007 -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day-temp">
      <xsl:value-of select="substring-after($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring-before($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year-temp">
      <xsl:value-of select="substring-after($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($year-temp,1,4)" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:if test="(string-length($day) &lt; 2)">
      <xsl:value-of select="0"/>
    </xsl:if>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="', '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
  <xsl:template name="ShortDateToMonthAndYear">
    <!-- expected date format 1/20/2007 [OR] 01/20/2007 -->
    <xsl:param name="DateTime" />
    <!-- new date format January 2007 -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day-temp">
      <xsl:value-of select="substring-after($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring-before($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year-temp">
      <xsl:value-of select="substring-after($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($year-temp,1,4)" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
  <xsl:template name="FormatLink">
    <!-- expected date format 1/20/2007 10:22:28 PM [OR] 01/20/2007 10:22:28 PM -->
    <xsl:param name="DateTime" />
    <xsl:param name="ModuleSID"/>
    <!-- new date format January 20, 2007 -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day-temp">
      <xsl:value-of select="substring-after($DateTime,'/')" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring-before($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year-temp">
      <xsl:value-of select="substring-after($day-temp,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($year-temp,1,4)" />
    </xsl:variable>

    <xsl:value-of select="'http://www.jhm.org/ME2/Sites/dirmod.asp?sid=B13CB8A01FFB4581BAF22705360BDFE5&amp;nm=&amp;type=recurring&amp;mod=Calendar+of+Events&amp;mid=7A41221F63DA4B72998F3F5810279001&amp;SiteID=4AC79C9B25B24DF3AF21C42311BE3921&amp;tier=2&amp;day='"/>
    <xsl:if test="(string-length($day) &lt; 2)">
      <xsl:value-of select="0"/>
    </xsl:if>
    <xsl:value-of select="$day"/>
    <xsl:value-of select="'&amp;month='"/>
    <xsl:value-of select="$mo"/>
    <xsl:value-of select="'&amp;year='"/>
    <xsl:value-of select="$year"/>
    <xsl:value-of select="'&amp;id='"/>
    <xsl:value-of select="$ModuleSID"/>
  </xsl:template>
  <xsl:template name="ShortMonthToFullMonthAndYear">
    <!-- expected date format Mon, 12 Mar 2012 00:00:00 EST -->
    <xsl:param name="DateTime" />
    <!-- return format March 2009 -->
    <xsl:variable name="right-cut">
      <!-- right-cut = '12 Mar 2012 00:00:00 EST' -->
      <xsl:value-of select="substring-after($DateTime,', ')" />
    </xsl:variable>
    <xsl:variable name="left-cut">
      <!-- right-cut = '12 Mar 2012' -->
      <xsl:value-of select="substring-before($right-cut,' 00:00:00')" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring-before($left-cut,' ')" />
    </xsl:variable>
    <xsl:variable name="month-year">
      <!-- month-year = 'Mar 2012' -->
      <xsl:value-of select="substring-after($left-cut,' ')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after($month-year,' ')" />
    </xsl:variable>
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before($month-year,' ')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = 'Jan'">January</xsl:when>
      <xsl:when test="$mo = 'Feb'">February</xsl:when>
      <xsl:when test="$mo = 'Mar'">March</xsl:when>
      <xsl:when test="$mo = 'Apr'">April</xsl:when>
      <xsl:when test="$mo = 'May'">May</xsl:when>
      <xsl:when test="$mo = 'Jun'">June</xsl:when>
      <xsl:when test="$mo = 'Jul'">July</xsl:when>
      <xsl:when test="$mo = 'Aug'">August</xsl:when>
      <xsl:when test="$mo = 'Sep'">September</xsl:when>
      <xsl:when test="$mo = 'Oct'">October</xsl:when>
      <xsl:when test="$mo = 'Nov'">November</xsl:when>
      <xsl:when test="$mo = 'Dec'">December</xsl:when>
    </xsl:choose>
    <xsl:value-of select="' '"/>
    <xsl:value-of select="$year"/>
  </xsl:template>
</xsl:stylesheet>


