﻿<%@ Page Title="Careers at JHM" Language="C#" Inherits="System.Web.Mvc.ViewPage<System.Collections.Generic.List<Jhm.Web.JobPosting>>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Careers & Employment Opportunities
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content-header">
        <h1>Careers & Employment Opportunities</h1>
    </div>
    <p>Are you talented, faithful, and want to become part of our dynamic staff?  The following positions are currently available.</p>
    <hr/>
    <% foreach (var item in Model)
       { %>
            
                <h2><%: item.Title %></h2>
                    <dl>
                        <dt style="font-weight:bold;">Date Posted:</dt>
                        <dd><%: item.StartDate.ToShortDateString() %></dd>
                    </dl>
                    <dl>
                        <dt style="font-weight:bold;">Description:</dt>
                        <dd><%: item.ShortDescription %> 
                                <a href="<%: Url.Action("CareerDetail", new{id=item.Id}) %>">Read More...</a>
                            </dd>
                    </dl>
                <hr/>
    <% } %>
</asp:Content>