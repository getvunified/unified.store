﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.DailyDevotionalViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Daily Devotionals
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>
       Daily Devotionals</h2>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title">
                Select from the options below to restrict your search.</h2>
            <div class="content">
                <% Html.RenderPartial("~/Views/Resources/SearchNewsForm.ascx"); %>
                <ul>
                    <% Html.Repeater(Model.GetAll(), "row", "row-alt", (news, css) =>
                   { %>
                        <li>
                            <h4><%: Html.ActionLink(news.Title, "ViewDevotional", "Resources",new{Id = news.Id},null)%></h4>
                            <%: news.PublicationDate %>
                            <br /> <br />
                        </li>
                    <% 
                   }); %>
                </ul>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>

