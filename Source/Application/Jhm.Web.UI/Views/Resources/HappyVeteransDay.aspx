﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CartViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>
<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
Happy Veteran's Day
</asp:Content>
<asp:Content runat="server" ID="Main" ContentPlaceHolderID="MainContent">
    <div style="text-align:center">
    <h2>Pastor John Hagee on Veteran's Day</h2>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/ir06YvoqnNc" frameborder="0" allowfullscreen showinfo="0"></iframe>    
    <h6>In this video blog, Pastor John Hagee gives thanks to the veterans of the United States Military for their sacrifice.</h6>
    </div>
</asp:Content>

