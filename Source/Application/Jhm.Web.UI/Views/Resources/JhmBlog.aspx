﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.BlogViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    All the Gospel to all the Blogosphere
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<img src="<%: Url.Content("~/Content/images/WeeklyUpdate.jpg") %>" />--%>

    <% foreach (var item in Model.Posts)
       {%>
    <%
           var categoryContainer = item.PostCategories.Aggregate(item.PostCategories.Count > 0 ? String.Empty : ", ",
                                                                 (text, t) => text + t + ", ",
                                                                 res => res.Remove(res.Length - 2));
    %>
    <div class="post" category="<%= categoryContainer %>">
        <table>
            <a name="<% = item.blogID %>"></a>
            <h2  style="font-size: 13px; font-weight: bolder; margin-bottom: -2px" class="title">
                <%= item.Title %>  - <i> <%= item.DateCreated.ToShortDateString() %>  </i>
            </>
            </h2>
            <br/>
           <%-- <h6>
                <i>
                <%= item.DateCreated.ToShortDateString() %>
                </i>
            </h6>--%>
            <div>
                <%= item.Description %>
            </div>
            <br />
            <br/>
            <a href="#">> Click to go back to the top</a>
        </table>
    </div>
    <% } %>
    <% using (Html.BeginForm("JhmBlog", "Resources", FormMethod.Post, new {id="myForm"}))
       { %>
    <div>
        <input type="hidden" name="LoadMorePosts" value="<%=Model.LoadMorePosts%>" />
        <% if (Model.LoadMorePosts <= Model.Posts.Count)
           {%>
           <%--<button class="easybutton" type="submit">Load More Posts</button>--%>
           <a style="cursor: pointer" class="easybutton" onclick="document.getElementById('myForm').submit()">Load More Posts</a>
        <%}%>
    </div>
    <%}%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        div.section p span
        {
            text-align: left !important;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('#content').css('background-color', '#FFFFF9');

            /*jQuery('.post').find("span").each(function (i) {
                jQuery(this).css('text-indent', '20px');
                /*jQuery(this).css('padding-right', '3px').css('padding-left', '3px').css('text-align', 'left');#1#
            });*/

            jQuery('.aligncenter').css('width', '570px');
        });

        function hideShowPost(categoryLi, categoryName) {
            jQuery('.post').each(function () {
                var categories = jQuery(this).attr('category');
                if (categories.indexOf(categoryName) != -1) {
                    jQuery(this).show();
                }
                else {
                    jQuery(this).hide();
                }
            });

            jQuery('#categoriesSideMenu ul').children('li').removeClass('current');
            jQuery(categoryLi).addClass('current');
        }

        function showAll(categoryLi) {
            jQuery('.post').each(function () {
                jQuery(this).show();

            });
            jQuery('#categoriesSideMenu ul').children('li').removeClass('current');
            jQuery(categoryLi).addClass('current');
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <form id="formArchive" runat="server">
    <div id="categoriesSideMenu" class="block">
        <h2 class="title">
            A Moment With Matt</h2>
        <ul>
            <% foreach (var item in Model.Posts)
               {%>
                    <li>
                        <a href="#<%= item.blogID%>" style="">
                            <b><%= item.Title %></b> <br/> <p style="padding: 0 0 0 10px; margin: 0 0 10px 0;">[<%= item.DateCreated.ToShortDateString() %>]</p>
                        </a>
                    </li>
            <% } %>
        </ul>    
                
      <%--   <asp:TreeView id="BlogTreeView" runat="server">

            <Nodes>

              <asp:TreeNode Value="Archives" 
                NavigateUrl="http://www.jhm.org/Resources/jhmBlog/#" 
                Text="Home"
                Target="Content" 
                Expanded="True">
              
                     <% foreach (var item in Model.Posts)
                       {%>
                            <asp:TreeNode Value="<%= item.blogID %>" 
                      NavigateUrl="http://www.jhm.org/Resources/jhmBlog/#<%= item.blogID %>" 
                      Text="<%= item.Title %>"
                      Target="Content">
                    
                    <% } %>
                </asp:TreeNode>

              </asp:TreeNode> 
            </Nodes>

      </asp:TreeView>--%>
    </ul>
    </div>
    </form>
<%--  </body>
</html>
        </ul>
    </div>--%>
</asp:Content>
