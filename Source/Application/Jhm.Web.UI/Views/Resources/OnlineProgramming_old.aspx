﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Online Programming
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table.custom
        {
            border: 1px dashed #CCCCCC;
            border-collapse: collapse;
            width: 350px;
        }
        table.custom td
        {
            border: 1px dashed #CCCCCC;
            padding: 1px;
        }
        .customList {
            list-style-type:none;
            line-height: 20px;
        }
        .customList li {
            height: 32px;
        }
        .customList .easybutton.mini {
            width:240px;
            text-align:left;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
		    $('#dialogModal').dialog({
				    autoOpen: false,
				    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 500
            });
            $('#pdfViewer').dialog({
				    autoOpen: false,
				    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 600
            });
		    <%= ViewData["ShowLoginScript"] %>
	    });
        function validateInputs(){
            if ( $('#UserName').val() == '' ){
                $('#rfvUsername').css("display","");
                return false;
            }
            $('#rfvUsername').css("display","none");
            if ( $('#Password').val() == '' ){
                $('#rfvPassword').css("display","");
                return false;
            }
            $('#rfvPassword').css("display","none");
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="dialogModal" title="Login or Register">
	    <p>Please login with your user name and password or click “register” for a free account.<br /><br />
         <strong>Note: If you have not created a user account on our new website, please create a free account now to listen or view programming.</strong>
        </p>
        <%
            using (Html.BeginForm("OnlineProgramming"))
            {%>
                <%= Html.ValidationSummary("Logon was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                <%: Html.Hidden("PageType", ViewData["PageType"])%>
                <dl>
                    <dt>Username:</dt>
                    <dd>
                        <%: Html.TextBox("UserName", ViewData["UserName"])%> <%: Html.ActionLink("Forgot Username? Click here", "ForgotUsername", "Account", null, new { @title = "Click here if you forgot your username", @style = "color:#165C86; font-weight:bold; font-size:14px;" })%>
                        <span id="rfvUsername" style="color:red; display:none;"><br />* Username is required</span>
                    </dd>
                    <dt>Password:</dt>
                    <dd>
                        <%: Html.Password("Password")%> <%: Html.ActionLink("Forgot Password? Click here", "ForgotPassword", "Account", null, new { @title = "Click here if you forgot your password", @style = "color:#165C86; font-weight:bold; font-size:14px;" })%>
                        <span id="rfvPassword" style="color:red; display:none;">* Password is required</span>
                    </dd>
                    <dt></dt>
                </dl>
                <%:
                    Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Login", true, "Signing you in...", "Logon"), "PopupActionBottonClicked", "validateInputs()")%>  OR  <%:
                    Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Register", "Register"), "PopupActionBottonClicked")%>

        <%        
            }
        %>
    </div>
    
    <div class="block">
        <h2 class="title">
            Today's TV Broadcast</h2>
        <div class="content">
            <table>
                <tr>
                    <td style="width: 220px" align="center">
                        <img src="<%: Url.Content("/Content/images/onetime-sm.jpg") %>" width="160" />
                    </td>
                    <td valign="top">
                        <h2>
                            What is on Television Today?</h2>
                            Watch online now.. <br /><br />
                        <b>Note: To view our daily program please log into your account using your user name and password.  If you do not have one you can register for a free account here:</b><br />
                        <table>
                            <tr>
                                <td>
                                    <ul class="customList">
                                        <li><a class="easybutton opener" href="<%:Url.Action("TvBroadcast")%>" pageType="TvBroadcast">
                                            <img src="<%: Url.Content("/Content/images/icon-watch.png") %>" />
                                            Watch Now</a>
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <ul class="customList">
                                        <li><a class="easybutton" href="<%: Url.Content("/Content/media/TvSchedule.pdf") %>" target="_blank">
                                            <img src="<%: Url.Content("/Content/images/calendar.png") %>" />
                                            View Schedule</a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            Cornerstone Church Live</h2>
        <div class="content">
            <h2>
                Watch Cornerstone Church Live... Online Every Sunday!</h2>
            <table>
                <tr>
                    <td style="width: 220px" align="center" valign="top">
                        <img src="<%: Url.Content("/Content/images/satellite_128.png") %>" />
                    </td>
                    <td valign="top">
                        <table class="custom" rules="all" border="1">
                            <tr>
                                <td colspan="2" align="center">
                                    <h4>
                                        Cornerstone Church Service Times</h4>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <h6>
                                        Each Sunday</h6>
                                </td>
                                <td>
                                    <ul>
                                        <li>Early Service: 8:30a - 10:00a CST</li>
                                        <li>Sunday School: 10:15a - 10:45a CST</li>
                                        <li>Late Service: 11:00a - 12:30p CST</li>
                                        <li>Evening Service: 06:30p - 08:00p CST</li>
                                    </ul>
                                    <h6>
                                        * All Times Are Subject To Change
                                    </h6>
                                </td>
                            </tr>
                        </table>
                        <b>Note: To view our services LIVE please log into your account using your user name and password.  If you do not have one you can register for a free account here:</b><br />
                        <ul class="customList">
                            <li><a class="easybutton opener" href="<%:Url.Action("ChurchBroadcast")%>" pageType="ChurchBroadcast">
                                <img src="<%: Url.Content("/Content/images/icon-watch.png") %>" />
                                Watch Now</a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            JHM Radio</h2>
        <div class="content">
            <table>
                <tr>
                    <td style="width: 220px" align="center">
                        <img src="<%: Url.Content("/Content/images/microphone.gif") %>" width="200" />
                    </td>
                    <td valign="top">
                        <h4>
                            Listen 24/7
                        </h4>
                        <b>Note: To listen to our daily program please log into your account using your user name and password.  If you do not have one you can register for a free account here:</b><br />
                        <ul class="customList">
                            <li><a class="easybutton opener" href="<%:Url.Action("JHMRadio")%>" pageType="JHMRadio">
                                <img src="<%: Url.Content("/Content/images/wmpIcon_16.png") %>" />
                                Listen Now </a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>