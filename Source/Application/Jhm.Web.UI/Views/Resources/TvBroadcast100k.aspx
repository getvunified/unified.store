﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Today's Tv Broadcast 100k
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Today's Tv Broadcast - Low Bandwidth Video</h2>
    <center>
        <iframe 
            height="360" 
            src="http://media.jhm.org/silverlight_livestreaming/DynPlayer.htm?height=170&amp;width=240&amp;playBtn=false&amp;fileName=http://stream1.jhm.org/JHMTV2" 
            frameborder="0" 
            width="660" 
            scrolling="no">
        </iframe>
    </center>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
