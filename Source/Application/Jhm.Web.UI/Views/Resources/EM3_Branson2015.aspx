﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Branson 2015
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
     
<div id="container">
    	<div class="header">
        	<div style="display: inline-block;"> <img src="<%: Url.Content("~/Content/images/Branson2015/JHM_logo.png") %>" alt="John Hagee Ministries Logo"></div>
        	<div style="display:inline-block; vertical-align: top; margin-top: 10px;">
        	    <div>
        	        <p style="color: white; letter-spacing: 5px; font-size: 43px;">THE HAGEES RETURN TO<%--<br>--%>
			        <%--<span class="branson">BRANSON, MO</span>--%>
                    </p>
                </div>
                <div class="branson" style="line-height: normal;">BRANSON, MO</div>
            </div>
            <div style="position: absolute; right: 0; line-height: normal; margin-right: 34px; margin-top: -5px;">
                <a href="https://events.jhm.org/Login/Login.aspx?ReturnUrl=%2fRegistration.aspx%3fEventCode%3dEVENT1501&EventCode=EVENT1501" style="float:right; margin-top: -60px;"><img src="<%: Url.Content("~/Content/images/Branson2015/register_button.png")%>" alt="Register Button"></a>
                <span class="date" style="margin-right: -20px; height: 46px; padding-top: 5px;">APRIL 30-MAY 1, 2015</span>
            </div>
		</div>
        
        <div class="speakers">
        	<img src="<%: Url.Content("~/Content/images/Branson2015/speakers.jpg") %>" alt="Speakers">
        </div>
        
        <div class="table">
        	<table>
            	<tr>
                	<td width="302px" align="center"><span class="speaker">PASTOR MATTHEW HAGEE</span></td>
                    <td width="302px" align="center"><span class="speaker">DIANA HAGEE</span></td>
                    <td width="302px" align="center"><span class="speaker">PASTOR JOHN HAGEE</span></td>
                </tr>
                <tr>
                	<td width="302px" align="center">April 30, 2015 &#8226; 7:30pm</td>
                    <td width="302px" align="center">May 1, 2015 &#8226; 10:00am</td>
                    <td width="302px" align="center">May 1, 2015 &#8226; 7:30pm</td>
                </tr>
            </table>
        </div>
            
            <div class="music">
            	<img src="<%: Url.Content("~/Content/images/Branson2015/concert.png")%>" alt="Guest Speaker Pastor John Hagee with Special Music from The Hagees and Canton Junction">
            </div>
            
            <div class="info">
            	<p><span class="header">COME JOIN US</span><br>
for a life changing message from Pastor John<br>
Hagee, Pastor Matt Hagee and Diana Hagee<br>
in Branson, Missouri. Canton Junction, one of<br>
America's finest quartets, and The Hagees will<br>
be in concert!<br><br>
To register or for more information call<br>
<span class="bold">1-800-854-9899</span> or click button below.<br>
Please make your reservation today!<br><br>

               </p>
            </div>
            
            <div class="programme">
            	&#42;dates and program indicated may be subject to change
            </div>
            
              <div class="location">
            	<span style="color: #fdbd40;">Branson Convention Center</span><br>
				200 Sycamore St.<br>
				Branson, MO 65616<br><br>
                <span style="margin-left: 70px;"><a href="https://events.jhm.org/Login/Login.aspx?ReturnUrl=%2fRegistration.aspx%3fEventCode%3dEVENT1501&EventCode=EVENT1501"><img src="<%: Url.Content("~/Content/images/Branson2015/register_button.png")%>" alt="Register Button"></a></span>
            </div>
            
        </div>
    
    
    
    
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style>

body {
	margin: 0px;
}

p {
	margin: 0px;
}

#container {
	background: #03192d;
	background-image: url(<%: Url.Content("~/Content/images/Branson2015/background.jpg")%>);
	background-repeat:no-repeat;
	width: 900px;
	height: 1230px;
	margin: auto;
}

.header {
	color: #FFF;
	font-family: 'Raleway', sans-serif;
	font-weight: 300;
	letter-spacing: 3px;
	font-size: 47px;
	margin: 0;
}

.branson {
	font-size: 79px;
	font-weight: 400;
	color: #f1ad1c;
	letter-spacing: 8px;
	line-height: 60px;

}

.header img {
	float: left;
	margin-left: 20px;
	margin-right: 15px;
	margin-top: 30px;
}

.header p {
	padding-top: 50px;
}

.date {
	font-family:Arial, Helvetica, sans-serif;
	font-size: 40px;
    float: right;
	background-color: #fdbd40;
	color:#000;
	padding-right: 50px;
	padding-left: 40px;
	/*padding-top: 5px;
	padding-bottom: 5px;*/
	margin-top: -30px;
}

.speakers {
	margin-bottom: -4px;
}

.table {
	font-family: 'Raleway', sans-serif;
	color: #FFF;
	width: 900px;
	font-size: 18px;
	background-color: #000;
	margin-top: 0px;
	line-height: 18px;
	padding-top: 8px;
	padding-bottom: 8px;
}

.speaker {
	letter-spacing: 1px;
	color: #ecab34;
}

.info {
	font-family: Arial, Helvetica, sans-serif;
	font-weight:300;
	letter-spacing: 0px;
	color: #FFF;
	font-size: 18px;
	width: 420px;
	margin-left: 50px;
	margin-top: -135px;
    line-height: normal;
}

.header {
	color: #fdbd40;
	font-size: 30px;
	letter-spacing: 0px;
}

.bold {
	color: #fdbd40;
	font-size: 25px;
}

a {
	color: #fdbd40;
}

a:hover {
	color: #ff4800;
}

.programme {
	font-family: Arial, Helvetica, sans-serif;
	font-variant: small-caps;
	color: #fdbd40;
	letter-spacing: 0px;
	font-size: 18px;
	margin-left: 50px;
}

.location {
	color: #FFF;
	font-family:Arial, Helvetica, sans-serif;
	font-size: 24px;
	float: right;
	margin-right: 90px;
	margin-top: -150px;
    line-height: 1.05em;
}


</style>
    
    
    


</asp:Content>
