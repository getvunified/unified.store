﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    London Landing Page 2014
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <div class="container">
            <img src="/Content/images/LondonConference2014/landing_event.jpg" alt="London Conference 2014">
            
        <p class="register"><a href="https://events.jhm.org/Registration.aspx?EventCode=EV1404"><img src="/Content/images/LondonConference2014/Register_button.png" alt="Register Now"></a><br><br><br></p>
        
        
	<div class="footer">
    
        <br>
            <a href="https://www.facebook.com/JohnHageeMinistries"><img src="/Content/images/LondonConference2014/facebook.jpg" alt="facebook" width="30" height="30"></a>
            
            <a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=PastorJohnHagee"><img src=			
            "/Content/images/LondonConference2014/twitter.jpg" alt="twitter" width="30" height="30"></a>
            
            <a href="https://plus.google.com/+JohnHageeMinistries/posts"><img src="/Content/images/LondonConference2014/google_plus.jpg" alt="Google Plus" width="30" height="30"></a>
            
            <a href="http://www.youtube.com/user/HageeMinistries?sub_confirmation=1"><img src="/Content/images/LondonConference2014/youtube.jpg" width="30" height="30" alt="Youtube"></a>
            
            <a href="http://www.pinterest.com/HageeMinistries/"><img src="/Content/images/LondonConference2014/pintrest.jpg" width="30" height="30" alt="pintrest"></a>
                        
            <a href="http://instagram.com/HageeMinistries"><img src="/Content/images/LondonConference2014/instagram.jpg" width="30" height="30" alt="istagram"></a><br><br>
    
    </div>










	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">

    <style>
/* Reset */
html, body, div, span, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
abbr, address, cite, code,
del, dfn, em, img, ins, kbd, q, samp,
small, strong, sub, sup, var,
b, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section, summary,
time, mark, audio, video {
	margin:0;
	padding:0;
	border:0;
	outline:0;
	font-size:100%;
	vertical-align:text-top;
	background:transparent;
}


.container {
	width: 900px;
	margin: auto;	
}

.register {
	width: 900px;
	background-color: #f2f2f0;
	text-align:center;
}

.footer {
	width: 900px;
	height: 100px;
	background-color: #242424;
	text-align:center;
	font-family:Verdana, Geneva, sans-serif;
	color:#FFF;
	font-size: .65em;
}

.footer img {
	padding-right: 3px;
}




</style>
</asp:Content>
