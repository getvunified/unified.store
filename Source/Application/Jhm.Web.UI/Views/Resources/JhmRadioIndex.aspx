﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Jhm Radio
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table.custom
        {
            border: 1px dashed #CCCCCC;
            border-collapse: collapse;
            width: 350px;
        }
        table.custom td
        {
            border: 1px dashed #CCCCCC;
            padding: 1px;
        }
        .customList {
            list-style-type:none;
            line-height: 20px;
        }
        .customList li {
            height: 32px;
        }

        

        .customList .easybutton.mini {
            width:240px;
            text-align:left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
            JHM Radio</h2>
        <div class="content">
            <table>
                <tr>
                    <td style="width: 220px" align="center">
                        <img src="<%: Url.Content("/Content/images/microphone.gif") %>" width="200" />
                    </td>
                    <td valign="top">
                        <h4>
                            Listen 24/7
                        </h4>
                        <ul class="customList">
                            <%--<li><a target="_blank" href="http://feeds.jhm.org/ME2/Sites/dirmod.asp?SiteID=8112722C039B4E508F0AB8552B898895&sid=44B1F4F45A21487384B5908703716A8B&nm=Program+Guide&type=ESpotlight&mod=Directories%3A%3ASpotlight&mid=0C0A17CACB074D638E44FBEB1F6A3CD2&tier=1">
            Find your station</a> </li>--%>
                            <%--<li><a target="_blank" href="http://itunes.apple.com/us/podcast/john-hagee-ministries-podcast/id293154402">
            Podcast</a> </li>--%>
                            <li><a class="easybutton mini" target="_blank" href="http://feeds.jhm.org/Media/ASX/JHMRadio.asx">
                                <img src="<%: Url.Content("/Content/images/wmpIcon_16.png") %>" />
                                Listen Now </a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
