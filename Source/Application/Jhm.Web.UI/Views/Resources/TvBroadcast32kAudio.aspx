﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Today's Tv Broadcast 32k Audio
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Today's Tv Broadcast (32k Audio Stream)</h2>
    <p><img src="<%: Url.Content("~/Content/images/icon-listen.png") %>" /> Note: Please make sure your speakers are turned on</p>
    <iframe 
        width="1" 
        scrolling="no" 
        height="1" 
        frameborder="0" 
        src="http://media.jhm.org/silverlight_livestreaming/DynPlayer.htm?height=1&amp;width=1&amp;playBtn=false&amp;fileName=http://stream1.jhm.org/JHMTV3">
    </iframe>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
