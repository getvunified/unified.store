﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
    Pastor John Hagee Statement on His Opposition to San Antonio Non-Discrimination Ordinance
</asp:Content>
<asp:Content runat="server" ID="aboutContent" ContentPlaceHolderID="MainContent">

    <h2>Pastor John Hagee Statement on His Opposition to San Antonio Non-Discrimination Ordinance</h2>
    <p>
        SAN ANTONIO – Pastor John Hagee, founder and Senior Pastor of Cornerstone Church, 
        has released the following statement discussing his opposition to the proposed City of San Antonio non-discrimination ordinance:</p>
    <p>An August 12th headline in the San Antonio Express News misstated my position on San Antonio’s non-discrimination ordinance
         and their subsequent coverage ignored a statement I made clarifying my position that same day. While the most egregious violation of 
        the First Amendment has been removed, I oppose the ordinance.</p>


    <p>Yesterday I hosted a meeting of over 200 civic and religious leaders during which we united in organized opposition to the ordinance. 
        The group agreed that the ordinance violates both the First Amendment of the Constitution and the Texas Religious Freedom Act. 
        We will work together to ensure the voice of San Antonio’s religious community is heard, and our ability to abide by God’s commandments is not abridged.</p>

</asp:Content>
