﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    London Landing Page 2015
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="main">

        <img class="header_img" src="<%: Url.Content("~/Content/images/LondonConference2015/london_flag.png" )%>"/>
        <img class="banner" src="<%: Url.Content("~/Content/images/LondonConference2015/banner.png")%>"/>

        <div class="content" >
          <a href="https://events.jhm.org/Login/Login.aspx?ReturnUrl=%2fRegistration.aspx%3fEventCode%3dEVENT1502&EventCode=EVENT1502"><img src="<%: Url.Content("~/Content/images/LondonConference2015/register_btn_London.png" )%>" class="register_btn" alt="Register Button"></a>
          <div class="text-content">
            <div class="p1" >
              <div class="empty-div inline-block left"></div>JOHN HAGEE MINISTRIES<div class="empty-div inline-block right"></div>
                <br/>
                <br/>
            </div>
              
            <div class="h1 fulljustify">
                <br/>
                <br/>
              PROPHECY
            </div>
            <br/>
            <div class="h2 fulljustify">
              CONFERENCE
            </div>
            <div class="p3">
              PRESENTED BY PASTOR JOHN HAGEE
            </div>
              <br/>
            <div class="h3">
              JUNE 19-20, 2015 @ 7:30PM
            </div>
              <br/>
            <div class="p2">
              WESTMINSTER CHAPEL
            </div>
              <br/>
            <div class="p4">
              BUCKINGHAM GATE, LONDON SW1E 6BS
            </div>
            <div class="empty-div"></div>
            <div class="h3 fulljustify">
                <br/>
              PROPHECY FOR TODAY
            </div>
            <div class="p4">
              Pastor John Hagee speaks on how today's news is lining<br/>
              up with Biblical prophecy. Pastor Hagee and Diana<br/>
              will both be speaking. "<b>Prophecy Update</b>"<br/>
              &amp; "<b>Power to Heal</b>" as well as hold<br/>
              an anointing service.
            </div>
          </div>
        </div>
        <img src="<%: Url.Content("~/Content/images/LondonConference2015/bottom_left_line.png")%>" class="bottom-left"/>
        <img src="<%: Url.Content("~/Content/images/LondonConference2015/bottom_right_line.png")%>" class="bottom-right"/>
    
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
.main {
  width: 900px;
  height: 1253px;
  background-image: url(<%: Url.Content("~/Content/images/LondonConference2015/London_bg.png")%>);
  background-repeat:no-repeat;
  background-position: top left;
  background-size: cover;
}

.header_img {
  position: absolute;
  top: 0;
  left:0;
  z-index: 3;
  width: 186px;
  height: 202px;
}

.bottom-left {
  width: 249px;
  position: absolute;
  top: 871px;
  left: 0;
}

.bottom-right {
  width: 249px;
  position: absolute;
  top: 872px;
  left: 651px;
}

.banner {
  position: relative;
  z-index: 2;
  width: 900px;

}

.register_btn {
  box-shadow: 7px 7px 7px rgba(0,0,0,0.4);
  width: 202px;
}

.content {
  position: relative;
  z-index: 3;
  width: 82%;
  margin-top: -150px;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}
.fulljustify {
  text-align: justify;
}
.fulljustify:after {
  content: "";
  display: inline-block;
  width: 100%;
}
.h1 {
  color: #d9c2a4;
  text-align: justify;
  height: 110px;
  font-size: 8.8em;
  margin-top: -1px;
}
.h2 {
  color: #d9c2a4;
  text-align: justify;
  height: 110px;
  font-size: 7em;
}
.h3 {
  color: #d9c2a4;
  text-align: center;
  height: 65px;
  font-size: 3.75em;
}
.p1 {
  color: #fff;
  text-align: center;
  font-size: 2.5em;
  word-spacing: 15px;
  margin-top: 60px;
}
.p2 {
  color: #fff;
  text-align: center;
  font-size: 2em;
  font-weight: 900;
}
.p3 {
  color: #fff;
  text-align: center;
  font-size: 1.6em;
  border-top: #d9c2a4 solid 2px;
  border-bottom: #d9c2a4 solid 2px;
}
.p4 {
  color: #fff;
  text-align: center;
  font-size: 1.5em;
  margin-top: -5px;
  margin-bottom: 10px;
}

.empty-div {
  border: #d9c2a4 solid 1px;
}

.inline-block {
  display: inline-block;
  width: 80px;
  margin-top: 13px;
  margin-bottom: 13px;
}

.left {
  margin-right: 20px;
}

.right {
  margin-left: 20px;
}



</style>
</asp:Content>
