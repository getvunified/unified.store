﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Cornerstone Church Live Broadcast Flash Player
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Cornerstone Church Live Broadcast (Flash Player)</h2>
    <center>
        <iframe 
            width="700" 
            scrolling="no" 
            height="480" 
            frameborder="0" 
            src="http://media.jhm.org/flash/FlashDynPlayer.html?width=470&amp;height=320&amp;fileName=rtmp://jhmapl.total-stream.net/jhm/jhmapl1"></iframe>
    </center>
    <a href="<%: Url.Action("OnlineProgramming") %>"  class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
