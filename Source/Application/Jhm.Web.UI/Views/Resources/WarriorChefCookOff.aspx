<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CartViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content runat="server" ID="Main" ContentPlaceHolderID="MainContent">
    <div style="background-image: url(/Content/images/WarriorChef-bg.jpg); height: 900px;
        padding: 10px; color: White;">
        <h2>
            Warrior Chef Challenge
        </h2>
        <p>
            A few Sundays ago an idea was born when Pastor Matthew Hagee stood on the stage
            of Cornerstone Church and issued a challenge from the pulpit to his mother. He challenged
            Mrs. Diana Hagee to a cook off.
        </p>
        <p>
            Pastor Matthew Hagee will be paired with Chef Eric Rocha. Diana Hagee will be paired
            with her sister Sandy Farhart. Each team will be required to cook and present a
            four course meal and each course will have the added pressure of a <strong><i>secret</i></strong>
            ingredient. The winner will be determined by a panel of restaurant owners.
        </p>
        <p>
            Proceeds from this event will benefit the children of single parents who would like
            to attend Cornerstone Christian Schools in San Antonio, Texas. You can watch this
            competition LIVE by registering on jhm.org and clicking the �media services� button.
            Help support the free airing of this event by clicking �DONATE�. Team Pastor Matthew
            Hagee or Team Diana Hagee � Who will be the WARRIOR CHEF?
        </p>
        <p>
            <a class="easybutton" href="<%: Url.Action(Jhm.Web.Core.MagicStringEliminator.Routes.Donation_Form.Action, Jhm.Web.Core.MagicStringEliminator.Routes.Donation_Form.Controller, new { donationCode = "Warrior_Chef_CookOff" }, null)%>"
                style="padding-top: 4px;">
                <img src="<%: Url.Content("/Content/images/donateIcon_20.png") %>" height="18" />Donate
                Now </a>
        </p>
        <center>
            <div>
                <iframe width="470" scrolling="no" height="320" frameborder="0" src="http://media.jhm.org/flash/SimpleFlashPlayer.html?width=470&amp;height=320&amp;fileName=rtmp://jhmapl.total-stream.net/jhm/jhmapl1">
                </iframe>

                <%--object height="320" width="470">
                    <param name="movie" value="/Content/media/FlashMediaPlayback.swf">
                    <param name="flashvars" value="src=">
                    <param name="allowFullScreen" value="true">
                    <param name="allowscriptaccess" value="always">
                    <embed src="/Content/media/FlashMediaPlayback.swf" type="application/x-shockwave-flash"
                        allowscriptaccess="always" flashvars="src=rtmp://istream.jhm.org/jhm/jhmapl1"
                        height="320" width="470"></object>--%>
            </div>
        </center>
    </div>
</asp:Content>
<asp:Content runat="server" ID="SidebarFirst" ContentPlaceHolderID="SidebarFirstContent">
</asp:Content>
<asp:Content runat="server" ID="SidebarSecond" ContentPlaceHolderID="SidebarSecondContent">
</asp:Content>
