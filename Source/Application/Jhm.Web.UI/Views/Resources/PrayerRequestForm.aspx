﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.PrayerRequestViewModel>" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Recaptcha" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Prayer Request
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        // window.onload = setTimeout(NFFix, 10000);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="region-content-top">
        <div id="section-checkout">
            <div class="block">
                <h2 class="title">
                    Prayer Request Form</h2>
                <div class="content">
                    <p>
                        Please fill out the form below:
                    </p>
                    <h5>
                        * Denotes Required Fields</h5>
                    <% using (Html.BeginForm("PrayerRequest", "Resources",new{pageType = Request.QueryString["pageType"]}, FormMethod.Post, new { Class = "niceform" }))
                       { %>
                    <%:Html.ValidationSummary(false, "Your request was not successful. Please correct the errors and try again.", new {@class = "messages error"})%>
                    <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                    <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                    <table>
                        <tr>
                            <td style="width: 100px;">
                                <%: Html.LabelFor(m => m.FirstName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.FirstName, new{ size = 30 }) %>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessageFor(m => m.FirstName) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.LastName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.LastName, new { size = 30 })%>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessageFor(m => m.LastName) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Address1) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Address1, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Address2) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Address2, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.City) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.City, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.State) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.State, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Zip) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Zip, new { size = 30 })%>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessageFor(m => m.Zip) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Country) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Country, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Phone) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Phone, new { size = 30 })%>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessageFor(m => m.Phone) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Fax) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Fax, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Email) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Email, new { size = 30 })%>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessageFor(m => m.Email) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(m => m.Title) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(m => m.Title, new { size = 30 })%>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <%: Html.LabelFor(m => m.PrayerRequest) %>
                            </td>
                            <td>
                                <%: Html.TextAreaFor(m => m.PrayerRequest, 5, 50, null) %>
                            </td>
                        </tr>
                        <tr>
                            <td />
                            <td>
                                <%: Html.CheckBoxFor(m => m.IncludeInNewsletter)%>
                                <%: Html.LabelFor(m => m.IncludeInNewsletter)%>
                                <br style="clear: both;" />
                                or call in your prayer request at 210-491-5100
                            </td>
                        </tr>
                        <%--<tr>
                            <td />
                            <td>
                                <br/>
                                <label>Please type the following verification words inside the box below:</label>
                                <%= Html.GenerateCaptcha() %>
                                <span style="color: #900;">*
                                    <%: Html.ValidationMessage(MagicStringEliminator.Captcha.ValidationErrorFieldKey)%>
                                </span>
                            </td>
                        </tr>--%>
                    </table>
                    <p>
                        <hr />
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Send"))%>
                    </p>
                    <%  } %>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
