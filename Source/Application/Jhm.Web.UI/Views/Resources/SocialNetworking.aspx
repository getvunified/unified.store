﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Social Networking
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .socialUl
        {
            border: #9DBED2 dashed 1px;
            padding-left: 0 !important;
            margin: 4px !important;
        }
        .socialUl li
        {
            list-style: none;
            padding: 6px;
            border-top: #9DBED2 dashed 1px;
        }
        .socialUl li:first-child
        {
            border-top: none;
        }
        
        .socialImage
        {
            border: 2px solid #9DBED2;
            background-color: #0C496F;
        }
        
        .socialImageCell
        {
            vertical-align: top;
            padding-top: 4px;
        }
        .linkTitle
        {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="color: #BB9841 /*Christmas Theme*/">
        Social Networking</h2>
    <div class="block">
        <h2 class="title">
            John Hagee Ministries
        </h2>
        <div class="content">
            <table>
                <col width="200" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="John Hagee Ministries" width="400" class="socialImage" src="<%: Url.Content("~/Content/Images/JHM_banner_FacebookTimeline.png")%>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("http://www.facebook.com/#!/johnhageeministries") %>">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook </span>
                            </a></li>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("http://twitter.com/#!/PastorJohnHagee") %>">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter </span></a>
                            </li>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("http://www.youtube.com/user/HAGEEMINISTRIES?feature=mhum") %>">
                                <img alt="Youtube Icon" src="<%: Url.Content("~/Content/Images/icon-youtube.png")%>" />
                                <span class="linkTitle">Youtube </span>
                            </a></li>
                            <%--<li><a target="_blank" href="<%= Url.ConvertToSafeUrl("http://itunes.apple.com/us/podcast/john-hagee-ministries-video/id461752101") %>">
                                <img alt="Podcast Icon" src="<%: Url.Content("~/Content/Images/podcast.jpg")%>" />
                                <span class="linkTitle">Video Podcast: </span>http://itunes.apple.com/us/podcast/john-hagee-ministries-video/id461752101
                            </a></li>--%>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("http://itunes.apple.com/us/podcast/john-hagee-ministries-audio/id461747136") %>">
                                <img alt="Podcast Icon" src="<%: Url.Content("~/Content/Images/podcast.jpg")%>" />
                                <span class="linkTitle">Audio Podcast </span>
                            </a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            GETV
        </h2>
        <div class="content">
            <table>
                <col width="200" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="GeTv" width="400" class="socialImage" src="<%: Url.Content("~/Content/Images/GETVMain.jpg") %>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="https://www.facebook.com/GETVchannel">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook </span></a>
                            </li>
                            <li><a target="_blank" href="https://twitter.com/#!/GE_TV">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter </span></a></li>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("https://www.youtube.com/user/HAGEEMINISTRIES?feature=mhee") %>">
                                <img alt="Youtube Icon" src="<%: Url.Content("~/Content/Images/icon-youtube.png")%>" />
                                <span class="linkTitle">Youtube </span>
                            </a></li>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("https://plus.google.com/113039896961749439146/posts") %>">
                                <img alt="Youtube Icon" src="<%: Url.Content("~/Content/Images/google-plus-16px.png")%>" />
                                <span class="linkTitle">Google+ </span>
                            </a></li>
                            <li><a target="_blank" href="<%= Url.ConvertToSafeUrl("https://secure.flickr.com/photos/hageeministries/") %>">
                                <img alt="Youtube Icon" src="<%: Url.Content("~/Content/Images/flickr.png")%>" />
                                <span class="linkTitle">flickr </span>
                            </a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            Difference Media
        </h2>
        <div class="content">
            <table>
                <col width="200" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="Difference Media" width="400" class="socialImage" src="<%: Url.Content("~/Content/dm/images/DM_banner.jpg") %>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="https://www.facebook.com/DifferenceMediaGroup">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook </span></a>
                            </li>
                            <li><a target="_blank" href="http://twitter.com/differencemedia">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter </span></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            Canton Junction
        </h2>
        <div class="content">
            <table>
                <col width="200" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="Junction" width="400" class="socialImage" src="<%: Url.Content("~/Content/dm/images/CJ_banner.png") %>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="http://www.facebook.com/CantonJunctionMusic">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook </span></a>
                            </li>
                            <li><a target="_blank" href="https://twitter.com/CantonJunction">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter </span></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--<div class="block">
        <h2 class="title">
            Pastor John Hagee
        </h2>
        <div class="content">
            <table>
                <col width="160" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="John Hagee Ministries" width="400" class="socialImage" src="<%: Url.Content("~/Content/Images/Pastor_John_FacebookTimeline.png")%>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="http://www.facebook.com/#!/PastorJohnHagee">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook: </span>http://www.facebook.com/#!/PastorJohnHagee
                            </a></li>
                            <li><a target="_blank" href="http://twitter.com/#!/PastorJohnHagee">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter: </span>http://twitter.com/#!/PastorJohnHagee </a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
    <%--<div class="block">
        <h2 class="title">
            Pastor Matthew Hagee
        </h2>
        <div class="content">
            <table>
                <col width="160" />
                <col width="800" />
                <tr>
                    <td class="socialImageCell">
                        <img alt="John Hagee Ministries" width="400" class="socialImage" src="<%: Url.Content("~/Content/Images/Pastor_Matt_FacebookTimeline.png")%>" />
                    </td>
                    <td valign="top">
                        <ul class="socialUl">
                            <li><a target="_blank" href="http://www.facebook.com/#!/pastormatthewhagee">
                                <img alt="Facebook Icon" src="<%: Url.Content("~/Content/Images/icon-facebook.png")%>" />
                                <span class="linkTitle">Facebook: </span>http://www.facebook.com/#!/pastormatthewhagee
                            </a></li>
                            <li><a target="_blank" href="http://twitter.com/#!/PastorMattHagee">
                                <img alt="Twitter Icon" src="<%: Url.Content("~/Content/Images/icon-twitter.png")%>" />
                                <span class="linkTitle">Twitter: </span>http://twitter.com/#!/PastorMattHagee </a>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
