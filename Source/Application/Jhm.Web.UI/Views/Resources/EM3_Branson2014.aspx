﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Branson 2013
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   
    
    
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" style="width:800px">

  <tr><td colspan="2" align="left" valign="top">
      <a href="http://www.jhm.org/"><img src="<%: Url.Content("~/Content/images/Branson2013/BransonRally2013_Landing.jpg") %>"  height="351" width="800" alt="John Hagee Rally & Concert London" /></a></td></tr>
  
  <tr><td colspan="1" align="left" valign="top"><img src="<%: Url.Content("~/Content/images/Branson2013/info.jpg") %>"  height="523" width="395
  " alt="John Hagee Rally & Concert Branson" /></td>
    
<td colspan="1" align="right" valign="top"><a href="https://events.jhm.org/Registration.aspx?EventCode=EV1403"><img src="<%: Url.Content("~/Content/images/Branson2013/Register.jpg") %>"  height="523" width="405" alt="John Hagee Rally & Concert Branson" /></a></td></tr>

</table>


    
    
    
    
    
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 10pt;
	line-height:1.3em;
	color: #0A1B39;
}
body {
	background-color: #CCC;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.media {
	margin:auto;
	width: 800px;
	height: 100px;
	background-color: #292929;
	padding-top: 0;
}

p {
	color:#FFF;
	font-size: 10px;
	line-height:12px;
	
}



</style>
</asp:Content>
