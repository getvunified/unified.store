﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Today's TV Broadcast (High Bandwidth)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Today's Tv Broadcast - High Bandwidth Video</h2>
    <center>
        <iframe height="600" width="660" src="http://media.jhm.org/silverlight_livestreaming/DynPlayer.htm?height=434&amp;width=640&amp;playBtn=false&amp;fileName=http://stream1.jhm.org/JHMTV1"
            frameborder="0" scrolling="no"></iframe>
    </center>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
