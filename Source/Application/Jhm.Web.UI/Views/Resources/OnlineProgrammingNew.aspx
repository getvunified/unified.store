﻿<%@ Page Title="OnlineProgramming" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CartViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
            TV and LIVE Broadcast Announcement</h2>
        <div class="content">
            <table>
                <tr>
                    <td align="center">
                        <img src="http://test.getv.org/Content/images/GETV-Generic.jpg" class="bordered alignleft"
                            alt="" width="545" height="200">
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <h5>
                            Watch Us Now on GETV!</h5>
                        <p>
                            Thank you for your interest in watching programming from Cornerstone Church & John
                            Hagee Ministries (JHM). JHM is excited to announce our latest venture in technology.
                            We are launching an internet television channel called Global Evangelism Television
                            (GETV). GETV is your newest way to keep up with all things Pastor John Hagee, Pastor
                            Matthew Hagee, John Hagee Ministries and even Cornerstone Church. If you are a first
                            time user, please register for an account.</p>
                        <br />
                        <div style="text-align: center">
                            <a class="easybutton" href="http://www.getv.org/">
                                <img src="<%: Url.Content("/Content/images/icon-watch.png") %>" />
                                Go to GETV.org</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
