﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    San Antonio Tour 2015
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
    <div id="container">

         <div style="width:900px; align-content:center">
                <div>
                    <img src="/Content/images/CJEH_Tour2015/CJ_Tour_Landing_Page.png" width="900px" usemap="#map"/>
					
					<map name="map">
						<area shape="rect" coords="39,750,277,932" href="http://www.friendlybaptist.org" />
						<area shape="rect" coords="323,754,558,929" href="mailto:info@victory.com" />
						<area shape="rect" coords="599,752,860,927" href="http://www.mcarthurchurch.com" />
						<area shape="rect" coords="23,1154,507,1287" href="http://www.peacehaven.com" />
					</map> 
                </div>
            </div>
   
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style>

        body {
	        margin: 0px;
        }

        p {
	        margin: 0px;
        }

        #container {
	        background: #03192d;	
	        background-repeat:no-repeat;
	        width: 900px;
	        margin: auto;
        }

        .date {
	        font-family:Arial, Helvetica, sans-serif;
	        font-size: 40px;
            float: right;
	        background-color: #fdbd40;
	        color:#000;
	        padding-right: 50px;
	        padding-left: 40px;
	        /*padding-top: 5px;
	        padding-bottom: 5px;*/
	        margin-top: -30px;
        }

        .bold {
	        color: #fdbd40;
	        font-size: 25px;
        }

        a {
	        color: #fdbd40;
        }

        a:hover {
	        color: #ff4800;
        }
     </style>    
</asp:Content>
