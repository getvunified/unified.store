﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Branson 2013
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="Branson" style="text-align: center; font-size: 0px; width: 900px;">
<table width="800" border="0" align="center" valign="top" cellpadding="0" cellspacing="0" bgcolor="#270d0f">
    <tbody>
        <tr>
            <td colspan="2" align="center" valign="top">
                <a href="http://www.jhm.org/">
                    <img src="<%: Url.Content("~/Content/images/Branson2013/Branson_header.jpg") %>" width="800" height="257" alt="John Hagee Rally & Concert Branson" />
                </a>
            </td>
        </tr>
  
        <tr>
            <td colspan="2" align="center" valign="top">
                <img src="<%: Url.Content("~/Content/images/Branson2013/Rally_Concert.jpg") %>" width="800" height="407" alt="John Hagee Rally & Concert Branson" />
            </td>
        </tr>
    
        <tr>
            <td colspan="2" style="background-repeat: no-repeat; background-position: center" align="center" valign="top"   background="<%: Url.Content("~/Content/images/Branson2013/background.jpg") %>" width="800" height="84" >
                 <a href="https://events.jhm.org/Registration.aspx?EventCode=BRAN13">
                     <img src="<%: Url.Content("~/Content/images/Branson2013/button.png") %>" alt="General Admission" width="350" height="82" />
                 </a>
            </td>
        </tr>

        <tr>      
              <td colspan="2" valign="top" align="center">
                  <img src="<%: Url.Content("~/Content/images/Branson2013/Event-info.png") %>" width="800" height="416" alt="Event information"/>
              </td>
        </tr>
      
        <tr >
            <td align="center">
                <br />
                <div id="social-icons">
                    <a href="https://www.facebook.com/JohnHageeMinistries"><img src="<%: Url.Content("~/Content/images/Branson2013/facebook.png") %>" width="35" height="35" alt="Twitter" /></a>
                    <a href="http://www.twitter.com/PastorJohnHagee"><img src="<%: Url.Content("~/Content/images/Branson2013/twitter.png") %>" width="35" height="35" alt="Facebook" /></a>
                    <a href="https://plus.google.com/b/100704479814999051273/100704479814999051273"><img src="<%: Url.Content("~/Content/images/Branson2013/google.png") %>" width="35" height="35" alt="Google Plus" /></a>
                    <a href="http://instagram.com/hageeministries"><img src="<%: Url.Content("~/Content/images/Branson2013/instagram.png") %>" width="35" height="35" alt="Instagram" /></a>
                    <a href="http://www.youtube.com/HAGEEMINISTRIES"><img src="<%: Url.Content("~/Content/images/Branson2013/youtube.png") %>" width="35" height="35" alt="YouTube" /></a>
                    <a href="http://pinterest.com/hageeministries"><img src="<%: Url.Content("~/Content/images/Branson2013/pintrest.png") %>" width="35" height="35" alt="YouTube" /></a>
                </div>
            </td>
        </tr>

    </tbody>
</table>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #Branson body, td, th {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 10pt;
            line-height: 1.3em;
            color: #0A1B39;
        }

        #Branson body {
            background-color: #CCC;
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }
        #Branson img { display:block; }
        #social-icons img { display: inline-block;}
    </style>
</asp:Content>
