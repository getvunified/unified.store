﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/PopupPreview.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.PressKitItem>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PreviewPressKit
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        body {
            background: #FFF;
            color: #000;
            height: 430px;
            width: 720px;
        }

        #leftImg {
            background: url("") no-repeat;
            float: left;
            height: 430px;
            width: 420px;
        }

        #rightContent {
            float: right;
            height: 390px;
            overflow-y: scroll;
            padding: 20px;
            width: 250px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="leftImg"><%: Model.PressKitPicture %></div>
    <div id="rightContent">
        <h1><%: Model.Tile %></h1>
        <p>
            <%: Model.Body %>
        </p>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
