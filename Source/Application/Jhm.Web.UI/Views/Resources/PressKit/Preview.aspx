﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.PressKitItem>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        body {
            background: #FFF;
            color: #000;
            height: 430px;
            width: 720px;
        }

        #leftImg {
            background: url("") no-repeat;
            float: left;
            height: 430px;
            width: 420px;
        }

        #rightContent {
            float: right;
            height: 390px;
            overflow-y: scroll;
            padding: 20px;
            width: 250px;
        }
    </style>
</head>
<body>
    <div id="leftImg"><%: Model.PressKitPicture %></div>
    <div id="rightContent">
        <h1><%: Model.Tile %></h1>
        <p>
            <%: Model.Body %>
        </p>
    </div>
</body>
</html>
