﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sacramento 2014 Landing Page
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    
<style>
/* Reset */
html, body, div, span, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
abbr, address, cite, code,
del, dfn, em, img, ins, kbd, q, samp,
small, strong, sub, sup, var,
b, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section, summary,
time, mark, audio, video {
	margin:0;
	padding:0;
	border:0;
	outline:0;
	font-size:100%;
	vertical-align:text-top;
	background:transparent;
}


.wrapper {
	width: 900px;
	margin: auto;
	background-color: #f4f4f2;
	font-family: 'Montserrat', sans-serif;
}

img {
	padding: 0px;
	margin: 0px;
}

.top-banner {
	background-color: #242424;
	height: 50px;
	text-align:center;
	color:#FFF;
	line-height: 36px;
	font-family: 'Montserrat', sans-serif;
	font-size: 20px;
}


.title h3 {
	text-align: center;
	font-size: 26px;
	letter-spacing: 7.5px;
	font-family: Arial, Helvetica, sans-serif;
	text-transform: uppercase;
	font-weight: lighter;
	color: #636466;
}

.title h1 {
	text-align: center;
	letter-spacing: -3px;
	font-size: 96px;
	color: #00396a;
	font-family: 'Montserrat', sans-serif;
	line-height: 100px;
	font-weight: 600;
}

.title h2 {
	text-align: center;
	font-size: 37px;
	font-family: 'Montserrat', sans-serif;
	font-weight: 200;
	line-height: 50px;
	color: #830f14;
}

.speakers {
	font-family: Verdana, Geneva, sans-serif;
	text-transform: uppercase;
	font-size: 15px;
	font-stretch:condensed;
	font-weight: 600;
	color: #535353;
}

.blue {
	color: #00396a;
}

.red {
	color: #830f14;
}

.matt {
	text-align: center;
	display: inline-block;
}

.diana {
	text-align: center;
	display: inline-block;
}

.speakers {
	padding-top: -10px;
	padding-bottom: 0px;
	margin: 0px;
	text-align: center;
	font-family:Arial, Helvetica, sans-serif;
}

.box1 {
  display: inline-block;
  width: 200px;
  height: 230px;
  margin: .6em;
}

.info {
	font-size: 17px;
	text-transform: uppercase;
	color: #3a3a3a;
	padding-top: -50px;
	padding-bottom: 0px;
	width: 900px;
	height: 186px;
	background-image: url(images/info_background.jpg);
	text-align:center;
}

.box2 {
  padding-top: 45px;
  display: inline-block;
  width: 275px;
  height: 100px;
}

.sm-text {
	text-transform: none;
	font-size: 14px;
}

.register {
	width: 900px;
	background-color: #f2f2f0;
	text-align:center;
}

.footer {
	width: 900px;
	height: 100px;
	background-color: #242424;
	text-align:center;
	font-family:Verdana, Geneva, sans-serif;
	color:#FFF;
	font-size: .65em;
}

.footer img {
	padding-right: 3px;
}

p {
	padding-top: 0px;
	margin: 0px;
}


</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
   <div class="wrapper">
    
    	<div class="top-banner">
        	<em>Taking all the Gospel to all the World and to all Generations.</em>
        </div>
        
        <div class="title">
        	<img src="/Content/images/Sacramento2014/jhm_sacramento.png" width="900" height="363" alt="JHM Sacramento">
            <h3>John Hagee Ministries Presents!</h3>
            <h1>Rally & Concert</h1>
            <h2>October 9-10, 2014 in Sacramento, CA</h2>
        </div>
        
        <div class="speakers">
        
            <div class="box1">
            	<img src="/Content/images/Sacramento2014/matt.jpg" width="198" height="170" alt="Pastor Matthew Hagee">
                <span class="blue">Pastor<br />
                Matthew Hagee</span><br />
                Thurday - 7:30pm
            </div>
        
            <div class="box1">
            	<img src="/Content/images/Sacramento2014/diana.jpg" width="198" height="170" alt="Diana Hagee">
                <span class="blue">Diana Hagee</span><br />
                Friday Morning<br />
                10:00am
            </div>
            
            <div class="box1">
            	<img src="/Content/images/Sacramento2014/john.jpg" width="198" height="170" alt="Pastor John Hagee"><br />
                <span class="blue">Pastor<br />
                John Hagee</span><br />
                Friday - 7:30pm
            </div>
            
            <div class="box1">
            <img src="/Content/images/Sacramento2014/canton_junction.png" width="198" height="170" alt="Canton Junction">
                <span class="blue">Canton Junction</span><br />
                In Concert!
            </div>
            
        </div>
        
        <div class="info">
        
        	<div class="box2" style="padding-left: 20px;">
            	<span class="red">Where</span><br />
				Memorial Auditorium<br />
				1515 J Street<br />
				Sacramento, CA 95814
            </div>
            
            <div class="box2">
            	<span class="red">Hotel information</span><br />
				Sheraton Grand<br />
				Sacramento<br />
				1-800-325-3535
            </div>
            
             <div class="box2" style="padding-right: 10px;">
            	<span class="red">Registration fee</span><br />
				$10.00<br />
				<span class="sm-text">(does not include hotel,<br />
				food or transportation)</span>
            </div>
  		</div>
        
        	<p style="padding-top: 20px; font-size: 23px; text-align:center; text-transform: none; color: #535353;">For more information call <span class="blue">		
        	    1-800-854-9899</span><br /><br />
				You may register onsite in Sacramento.</p><br />
        
        
         <%--       <p class="register"><a href="https://events.jhm.org/Registration.aspx?EventCode=EV1407"><img src="/Content/images/Sacramento2014/Register_button.png" alt="Register Now"></a><br /><br /></p>
      --%>  
        
	<div class="footer">
    
        <br />
            <a href="https://www.facebook.com/JohnHageeMinistries"><img src="/Content/images/Sacramento2014/facebook.jpg" alt="facebook" width="30" height="30"></a>
            
            <a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=PastorJohnHagee"><img src=			
            "/Content/images/Sacramento2014/twitter.jpg" alt="twitter" width="30" height="30"></a>
            
            <a href="https://plus.google.com/+JohnHageeMinistries/posts"><img src="/Content/images/Sacramento2014/google_plus.jpg" alt="Google Plus" width="30" height="30"></a>
            
            <a href="http://www.youtube.com/user/HageeMinistries?sub_confirmation=1"><img src="/Content/images/Sacramento2014/youtube.jpg" width="30" height="30" alt="Youtube"></a>
            
            <a href="http://www.pinterest.com/HageeMinistries/"><img src="/Content/images/Sacramento2014/pintrest.jpg" width="30" height="30" alt="pintrest"></a>
                        
            <a href="http://instagram.com/HageeMinistries"><img src="/Content/images/Sacramento2014/instagram.jpg" width="30" height="30" alt="istagram"></a><br /><br />
        
    	<p>P.O. Box 1400, San Antonio, Texas 78295-1400  |  1-800-854-9899<br />
			Copyright © 2000-2013 John Hagee Ministries. All rights reserved.</p>
    
    
    
    
    
    </div>
</asp:Content>
