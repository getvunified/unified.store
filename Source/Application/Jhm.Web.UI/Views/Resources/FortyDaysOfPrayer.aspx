﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
         Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.FortyDaysOfPrayerModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    40 Days of Prayer
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="FortyDays" style="width: 900px; height: 1306px; text-align: center; background-image: url('/EmailTemplates/FortyDaysOfPrayer/40Days_Prayer_Landing_02.jpg'); background-position: 0px 315px;">
        <table align="center" cellpadding="0" cellspacing="0" style="border-top: 15px solid white; width: 900px; border-collapse: collapse; border-spacing: 0px;">
            
            <tr >
                <td style="height: 405px;">
                    <img src="/EmailTemplates/FortyDaysOfPrayer/40Days_Prayer_Landing_01.jpg" width="900" height="405" alt="40 Days of Prayer" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="background-image: url('/EmailTemplates/FortyDaysOfPrayer/40Days_Prayer_Landing_02.jpg');">
                    <br/><br/>
                    <h1>September 28th - November 6th, 2012</h1>
                    <p>We're calling for 40 Days of Prayer beginning Sept. 28 – Nov. 6 ending just before the election. America is in a tremendous<br />
                        geo-political and economic crisis. We are headed in the wrong direction. We must return to the God of our fathers.</p>
                    <span style="display: block;">
                        <a href="#FortyDaysInfoLink" id="CallPopupForEmail" style="text-decoration: none;">
                            <img src="/EmailTemplates/FortyDaysOfPrayer/sign_up.png" width="450" height="75" alt="Sign Up" />
                        </a>
                        <br />
                        
                        <span style="font: bolder">
                            <a href="/Content/media/40Prayer_proclamation-color.pdf" target="_blank">Download</a> The 40 Days of Prayer Proclamation<br />
                            Certificate America and receive daily emails<br />
                            with prayer targets for each 40 days.
                        </span>
                        
                        <br />
                        
                    </span>
                    
                    <div id="FortyDaysInfo">
                        <a name="FortyDaysInfoLink"></a>
                        <% using (Html.BeginForm())
                           {%>
                            <%: Html.FormKeyValueMatchAttributeHiddenField() %>
                            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
                            
                            <table style="margin-left: 20px; width: 410px;">
                                <tr>
                                    <td><strong><%: Html.LabelFor(model => model.Email) %>:</strong></td>
                                    <td><%: Html.TextBoxFor(model => model.Email, htmlAttributes: new {size = 40, maxlength = 100}) %><br/><span style="color: #600;"><i>* Required</i></span><br/>
                                        <span style="color: #600;"><%: Html.ValidationMessageFor(model => model.Email) %></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong><%: Html.LabelFor(model => model.FirstName) %>:</strong></td>
                                    <td><%: Html.TextBoxFor(model => model.FirstName, htmlAttributes: new {size = 40, maxlength = 100}) %></td>
                                </tr>
                                <tr>
                                    <td><strong><%: Html.LabelFor(model => model.LastName) %>:</strong></td>
                                    <td><%: Html.TextBoxFor(model => model.LastName, htmlAttributes: new { size = 40, maxlength = 100 })%></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 25px; text-align: center; width: 200px;" colspan="2">
                                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Sign Up"))%>
                                        <!--<a style="text-decoration: none; padding-top: 10px; padding-bottom: 8px; padding-right: 2px; background-image: url('/Content/images/button.png'); ">Sign Up</a>-->
                                    </td>
                                </tr>
                            </table>
                            
                            <br />
                            <br />
                            
                        <% } %>
                    </div>

                    <img src="/EmailTemplates/FortyDaysOfPrayer/40Days_Prayer_Landing_03.jpg" width="900" height="491" alt="landing"/>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(function () {

            $("#FortyDaysInfo").hide();
            $("#CallPopupForEmail").click(function () {
                
                $("#FortyDaysInfo").show();
            });
        });

    </script>

    <style type="text/css">
        
        #FortyDaysInfo {
            border: thick;
            border-color: #BF2025;
            border-style: ridge;

            height: 200px;
            margin-bottom: 25px;
            margin-top: 25px;
            width: 450px;
            text-align: left;
            
            background-image: url('/EmailTemplates/FortyDaysOfPrayer/FortyDaysOfPrayerEmailFill.png');
        }

        #FortyDays body, td, th {
            color: #0A1B39;
            font-family: Verdana, Geneva, sans-serif;
            font-size: 10pt;            
        }

        #FortyDays body {
            background-color: #a49a81;
            margin-bottom: 0px;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 0px;
        }

        #FortyDays h1 {
            color: #003067;
            font-family: "Times New Roman", Times, serif;
            font-size: 33pt;
            line-height: normal;
            text-align: center;
        }

        #FortyDays h2 {
            color: #003067;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 25pt;
            line-height: normal;
            text-align: center;
        }

        #FortyDays a {
            color: #ad1d22;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 25pt;
            line-height: normal;
            text-align: center;
            text-decoration: none;
        }

        #FortyDays a:hover {
            color: #ad1d22;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 25pt;
            line-height: normal;
            text-align: center;
            text-decoration: underline;
        }

        #FortyDays p {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 25px;
            line-height: 28px;
            text-align: center;
            width: 750px;
        }
    </style>
   
</asp:Content>