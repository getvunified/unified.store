﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Admin.Models.JhmMagazineListingViewModel>" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Digital Magazines
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
	<% if (Model.Issues.Any(y => y.IsMagazine == false)) {  %>
		<div id="content-header">
			<h2 class="section-title">
				JHM Digital Catalog</h2>
		</div>
		<div class="region-content-bottom">
			<!-- <a style="float:right;" class="easybutton buy" href="<%: Url.Action("JhmMagazineSubscription") %>"
										target="_blank"><span class="">Subscribe</span></a> -->
			<% foreach (var issue in Model.Issues.Where(y=> y.IsMagazine == false).OrderByDescending(x => x.Rank))
			   { %>
				<div class="block">
					<h2 class="title">
						<%: issue.Title %></h2>
					<div class="content">
						<div class="node node-type-product node-teaser clearfix">
							<table>
								<tr>
									<td rowspan="3" style="width: 160px">
										<span class="product-photo">
											<img src="<%: issue.ImagePath %>" width="99" height="135" />
										</span>
									</td>
									<td>
										<h2>
											<%: issue.SubTitle %></h2>
									</td>
								</tr>
								<tr>
									<td>
										<h6><%: issue.Description %></h6>
									</td>
								</tr>
								<tr>
									<td>
										<a style="margin-right: 246px;" class="easybutton buy right" href="http://media.jhm.org/Jhm.Magazine/?id=<%: issue.MagazineContentId %>"
											target="_blank"><span class="">View Catalog</span></a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			<% }
		   } %>
           <h2 class="section-title"> JHM Digital Magazines </h2>
        <% foreach (var issue in Model.Issues.Where(y => y.IsMagazine).OrderByDescending(x => x.Rank))
           { %>
            <div class="block">
                <h2 class="title">
                    <%: issue.Title %></h2>
                <div class="content">
                    <div class="node node-type-product node-teaser clearfix">
                        <table>
                            <tr>
                                <td rowspan="3" style="width: 160px">
                                    <span class="product-photo">
                                        <img src="<%: issue.ImagePath %>" width="99" height="135" />
                                    </span>
                                </td>
                                <td>
                                    <h2>
                                        <%: issue.SubTitle %></h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6><%: issue.Description %></h6>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a style="margin-right: 246px;" class="easybutton buy right" href="http://media.jhm.org/Jhm.Magazine/?id=<%: issue.MagazineContentId %>"
                                        target="_blank"><span class="">View Magazine</span></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        <% } %>
        <% if ( !Model.Issues.Any() )
           { %>
                <div class="block">
                    <div class="content">
                        <h6>Sorry, at this moment there are no Digital magazines available. Please check back later.</h6>
                    </div>
                </div>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/WholeRightMenu.ascx"); %>
</asp:Content>
