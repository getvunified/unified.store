﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.LatestNewsViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Latest News
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>
        Latest News</h2>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title">
                Select from the options below to restrict your search.</h2>
            <div class="content">
                <% Html.RenderPartial("~/Views/Resources/SearchNewsForm.ascx"); %>
                <ul>
                    <% Html.Repeater(Model.GetAll(), "row", "row-alt", (news, css) =>
                   { %>
                        <li>
                            <h4><%: Html.ActionLink(news.Title, "ViewNews", "Resources",new{Id = news.Id},null)%></h4>
                            <%: news.PublicationDate %>
                            <br /> <br />
                        </li>
                    <% 
                   }); %>
                </ul>
            </div>
        </div>
    </div>

</asp:Content>




