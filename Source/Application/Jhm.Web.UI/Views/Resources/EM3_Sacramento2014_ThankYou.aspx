﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sacramento 2014 Thank You For Registering
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        /* Reset */
        div, span, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        abbr, address, cite, code,
        del, dfn, em, img, ins, kbd, q, samp,
        small, strong, sub, sup, var,
        b, i,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, figcaption, figure,
        footer, header, hgroup, menu, nav, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: text-top;
            background: transparent;
        }


        .container {
            width: 900px;
            margin: auto;
        }

        .register {
            width: 900px;
            background-color: #f2f2f0;
            text-align: center;
        }

        .footer {
            width: 900px;
            height: 100px;
            background-color: #242424;
            text-align: center;
            font-family: Verdana, Geneva, sans-serif;
            color: #FFF;
            font-size: .65em;
        }

            .footer img {
                padding-right: 3px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" style="background: white;">
            <img src="/Content/images/Sacramento2014/thank_you.jpg" alt="JHM Sacramento Rally and Concert">
        
                

                    
        <br><br>
            <p style="font-size: 25px;text-align: center">Your registration was successful.  We look forward to seeing you there!</p>
        <br><br><br><br>
        
        
	<div class="footer">
    
        <br>
            <a href="https://www.facebook.com/JohnHageeMinistries"><img src="/Content/images/Sacramento2014/facebook.jpg" alt="facebook" width="30" height="30"></a>
            
            <a href="https://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=PastorJohnHagee"><img src=			
            "/Content/images/Sacramento2014/twitter.jpg" alt="twitter" width="30" height="30"></a>
            
            <a href="https://plus.google.com/+JohnHageeMinistries/posts"><img src="/Content/images/Sacramento2014/google_plus.jpg" alt="Google Plus" width="30" height="30"></a>
            
            <a href="http://www.youtube.com/user/HageeMinistries?sub_confirmation=1"><img src="/Content/images/Sacramento2014/youtube.jpg" width="30" height="30" alt="Youtube"></a>
            
            <a href="http://www.pinterest.com/HageeMinistries/"><img src="/Content/images/Sacramento2014/pintrest.jpg" width="30" height="30" alt="pintrest"></a>
            
            <a href="http://instagram.com/HageeMinistries"><img src="/Content/images/Sacramento2014/instagram.jpg" width="30" height="30" alt="istagram"></a><br><br>
        
    	<p>P.O. Box 1400, San Antonio, Texas 78295-1400  |  1-800-854-9899<br>
			Copyright © 2000-2013 John Hagee Ministries. All rights reserved.</p>
    
    </div>
                </div>
    
    <!-- Google Code for Branson 2014 Registration Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1000675274;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "PFevCPaa5gYQyq-U3QM";
    var google_conversion_value = 0;
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1000675274/?value=0&amp;label=PFevCPaa5gYQyq-U3QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</asp:Content>
