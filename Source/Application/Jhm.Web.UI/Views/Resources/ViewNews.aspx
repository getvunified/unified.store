﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.XmlSyndicationRecordViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%:  Model.Title %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        pre 
        {
            font-family: Helvetica,Arial,"Nimbus Sans L",sans-serif;
            font-size: 0.875em;
            line-height: 1.286em;
        }
    </style>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title">
                <%:  Model.Title %></h2>
            <div class="content">
                <%= Server.HtmlDecode(Model.Content) %>
            </div>
        </div>
   </div>
    <p>
        <%: Html.ActionLink("Back to List", "LatestNews") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>

