﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.JobPosting>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Model.Title %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1><%: Model.Title %></h1>
    <dl>
        <dt style="font-weight:bold;">Date Posted:</dt>
        <dd><%: Model.StartDate.ToShortDateString() %></dd>
    </dl>
    <dl>
        <dt style="font-weight:bold;">Description:</dt>
        <dd><%: Html.Raw(Model.Description) %> </dd>
    </dl>
    <dl>
        <dd>
            <a style="padding-top:6px" href="<%:Url.Action("DownloadPdfApplicationForm", new{id = Model.Id}) %>" class="easybutton"><span style="background: url('<%: Url.Content("~/Content/images/Icons/PDF-Document-icon.png") %>') left top no-repeat; height:24px; padding-top:4px; display:block; padding-left:28px;">Click here to download PDF form</span></a>
        </dd>
    </dl>
    <br/>
    <%: Html.ActionLink("<< Return to Careers List","Careers") %>
</asp:Content>
