﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Prophetic Blessing
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link type="text/css" rel="stylesheet" href="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/css/main.css") %>" />
    <link type="text/css" rel="stylesheet" href="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/css/Gibson-webfont.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%----%>
    <%
        var startDate = new DateTime(2012, 08, 27);
        var endDate = new DateTime(2012, 08, 31);
        var today = DateTime.Now;    
        
        var amazonLink =
            "http://www.amazon.com/dp/1617950777/ref=as_li_tf_til?tag=johnhagemini-20&camp=14573&creative=327641&linkCode=as1&creativeASIN=1617950777&adid=0F531WSHY05WDQBNWEEF&&ref-refURL=http%3A%2F%2Frcm.amazon.com%2Fe%2Fcm%3Flt1%3D_blank%26bc1%3D000000%26IS2%3D1%26bg1%3DFFFFFF%26fc1%3D000000%26lc1%3D0000FF%26t%3Djohnhagemini-20%26o%3D1%26p%3D8%26l%3Das1%26m%3Damazon%26f%3Difr%26ref%3Dtf_til%26asins%3D1617950777";
        if (today >= startDate && today <= endDate)
    
       {%>
            <div id="mainMain">
        <div id="wrapper">
            <div id="header_p">
                <img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/Hagee-Header.png") %>" style="width: 900px !important; height: 161px !important;" alt="Header"/>
                <div style="z-index: 100; position: absolute; left: 789px; top: 75px;">
                    <a href="https://www.facebook.com/PastorJohnHagee?ref=ts">
                        <img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/fb-icon.png") %>" style="display: inline" alt="facebook" />
                    </a>
                    <a href="https://twitter.com/PastorJohnHagee">
                        <img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/twitter-icon.png") %>" style="display: inline; margin-left: 10px" alt="twitter" />
                    </a>
                </div>
            </div>
            <div id="content_p">
                <center>
                    <h1 class="orange" style="margin-bottom: 0">Discover the Power to Sculpt Your Future,</h1>
                    <h1 class="darkblue">Your Career, Your Life, and Your Family's Life!</h1>
					
                    <div id="video" style="float:left;">
                        <iframe width="512" height="288" src="http://www.youtube.com/embed/H8BsFTjcUWM?feature=player_embedded" frameborder="0" allowfullscreen></iframe>
                    </div>
					
                    <br/><br/>
                    <h3>6 FREE BONUS ITEMS<br/>-a $79 value-<br/>to the first <span class="blue">750 people</span><br/>who respond,<br/>including an exclusive<br/>audio download of<br/>a blessing from<br/>Pastor John Hagee!</h3>
                </center>
                <br style="clear: both" />
                <div id="book">
                    <div id="cover">
                        <center>
                            <img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/Hagee-prophet-book-cover-3d.png") %>" style="margin-left: -50px; width: 340px !important; height: 309px !important;" alt="Prophetic Blessing Cover" />
                            <h2>ORDER TODAY AT:</h2>
                            <a href="<%: amazonLink %>" target="_blank"><img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/amazon-button-upper.png") %>" alt="Go to Amazon"/></a>
                        </center>
                    </div>
                    <p><strong>HOW WOULD YOUR LIFE CHANGE</strong> if you knew that every blessing recorded in Scripture - the power to receive spiritual and material prosperity, to gain health and peace of mind - is available to you for the asking? And what would you do if you knew that the only reason you had not was because you asked not? A message of hope and deliverance, <i>The Power of the Prophetic Blessing</i> emphatically proclaims that you have the potential to lead a blessed life...</p>
                    <h1 style="font-size: 30px">BECAUSE YOU WERE BORN TO BE BLESSED!</h1>
                    <h2>Pastor Hagee unwraps the gift that can produce supernatural results beyond your dreams:</h2>
                    <ul style="margin-left: 313px">
                        <li>Discover the power to impact your future, your career and your life</li>
                        <li>Find out how to revolutionize the lives of your children and grandchildren for today, tomorrow, and forever</li>
                        <li>Enjoy spiritual, physical, emotional and relational prosperity</li>
                        <li>Learn to release and receive the Prophetic Blessing</li>
                        <li>And much, much more!</li>
                    </ul>
                </div>
                <h2 class="orange">HOW TO ENTER:</h2>
                <p>Simply order Pastor John Hagee's 
                    <strong>
                        <i>The Power of the Prophetic Blessing</i>
                    </strong> on 
                    <a href="">Amazon.com</a> between August 26 - August 30, 2012. Then, email your Amazon receipt number to 
                    <a href="mailto:offer@jhm.org">offer@jhm.org</a> and we will send you a link to access the SIX FREE BONUSES below ($79.00 value) for FREE!</p>
            </div>
            <div id="footer_p" style="padding: 0 25px 0 25px; width: 700px !important;">
                <img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/hagee-bonus-items.png") %>" style="width: 856px !important; height: 618px !important;" alt="Bonus Items" />
                <div style="z-index: 100; position: absolute; left: 478px; bottom: 70px;">
                    <a href="<%: amazonLink %>" target="_blank"><img src="<%: Url.Content("~/EmailTemplates/PropheticBlessingAmazon/img/amazon-button-lower.png") %>" style="display: inline; width: 366px !important; height: 112px !important;" alt="Go to Amazon"/></a>
                </div>
            </div>
        </div>
    </div>
    <% }
       else
       {%>
            <iframe style="border: none; width: 900px; height: 1140px" src="http://media.jhm.org/PropheticBlessing/PropheticBlessing.html"></iframe>
    <% } %>
</asp:Content>
