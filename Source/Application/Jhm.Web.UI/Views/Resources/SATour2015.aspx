﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    San Antonio Tour 2015
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      
    <div id="container">

         <div style="width:900px; align-content:center">

             <div style="background-image:url(/Content/images/SATour2015/SanAntonioTour2015-body-textbackground.jpg);">
                    <img src="/Content/images/SATour2015/SanAntonioTour2015-header.jpg" />
                    <br />
                    <p style="padding:10px;">Have you ever watched Pastor Hagee on television as he preaches from Cornerstone Church and thought, “I wonder what it would be like to be there?” Well, now you can experience this extraordinary church service in person!</p>
                    <p style="padding:10px;">This year, John Hagee Ministries is hosting a tour to San Antonio, Texas where you will experience everything that John Hagee Ministries and Cornerstone Church has to offer. This is an exclusive tour for UK supporters and it falls on the Feast of Tabernacles celebration weekend, one of the most exciting dates on the Cornerstone calendar. We think it is the perfect way to immerse you in this dynamic ministry.</p>
                    <p style="padding:10px;">This is not just your average holiday. You will tour San Antonio and the local area, learning the rich history and experiencing Texas in all its glory—including Texas sized fireworks. You will learn new phrases like “Tex-Mex” (apparently it’s edible) and be referred to as “Ya’ll”. And let’s not forget the food—after all everything is bigger in Texas!</p>
                    <p style="padding:10px;">As well as a sightseer, you will also partake of a unique spiritual experience. The Feast of Tabernacles weekend is complete with Spirit-filled preaching, inspirational music, and a Night to Honor Israel. These exciting events will demonstrate first-hand how God has grown this ministry from a tiny office with one secretary into a global outreach that now proclaims the Gospel message to the nations of the world, reaching millions on a daily basis.</p>
                    <p style="padding:10px;">This will be a life-changing adventure you will not soon forget! Join us in San Antonio this fall as we tour historic San Antonio and celebrate the Feast of Tabernacle’s with Pastor Hagee, Pastor Matthew and John Hagee Ministries!</p>
                    <a href="/Content/images/SATour2015/2015-SanAntonioTourBrochure-WebView.pdf" border="0"><img src="/Content/images/SATour2015/SanAntonioTour2015-filled-button.png" /></a>
                    <br />
                    <br />
                    <img src="/Content/images/SATour2015/SanAntonioTour2015-footer.jpg" />
                </div>
            </div>
   
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style>

        body {
	        margin: 0px;
        }

        p {
	        margin: 0px;
        }

        #container {
	        background: #03192d;	
	        background-repeat:no-repeat;
	        width: 900px;
	        margin: auto;
        }

        .date {
	        font-family:Arial, Helvetica, sans-serif;
	        font-size: 40px;
            float: right;
	        background-color: #fdbd40;
	        color:#000;
	        padding-right: 50px;
	        padding-left: 40px;
	        /*padding-top: 5px;
	        padding-bottom: 5px;*/
	        margin-top: -30px;
        }

        .bold {
	        color: #fdbd40;
	        font-size: 25px;
        }

        a {
	        color: #fdbd40;
        }

        a:hover {
	        color: #ff4800;
        }
     </style>    
</asp:Content>
