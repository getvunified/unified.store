﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.FortyDaysOfPrayerModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Toronto Rally &amp; Concert 2013
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="background-color: #CCC;">
            <table style="width:800px; margin:auto;" border="0" align="center" cellpadding="0" cellspacing="0">

                <tr><td colspan="2" align="left" valign="top"><a href="http://www.ticketmaster.com/john-hagee-ministries-2day-pass-toronto-ontario/event/10004AB77B9630C1?artistid=12962&majorcatid=10001&minorcatid=50"><img src="/Content/images/Toronto2013/Toronto2013_header.jpg" height="412" width="800" alt="John Hagee Rally & Concert Toronto" /></a></td></tr>
  
                <tr><td colspan="1" align="left" valign="top"><img src="/Content/images/Toronto2013/info.jpg" height="610" width="400" alt="John Hagee Rally & Concert Toronto" /></td>
    
                <td colspan="1" align="right" valign="top"><a href="http://www.ticketmaster.com/john-hagee-ministries-2day-pass-toronto-ontario/event/10004AB77B9630C1?artistid=12962&majorcatid=10001&minorcatid=50"><img src="/Content/images/Toronto2013/registration.jpg" height="610" width="402" alt="John Hagee Rally & Concert Toronto" /></a></td></tr>

            </table>


            <div class="media">

                <br />
                <p align="center"><a href="https://www.facebook.com/JohnHageeMinistries"><img src="/Content/images/Toronto2013/facebook.png" width="35" height="35" alt="Twitter" /></a>

                <a href="http://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=PastorJohnHagee"><img src="/Content/images/Toronto2013/twitter.png" width="35" height="35" alt="Facebook" /></a>

                <a href="https://plus.google.com/100704479814999051273/posts"><img src="/Content/images/Toronto2013/google.png" width="35" height="35" alt="Google Plus" /></a>

                <a href="http://instagram.com/HageeMinistries"><img src="/Content/images/Toronto2013/instagram.png" width="35" height="35" alt="Instagram" /></a>

                <a href="http://www.youtube.com/user/HageeMinistries"><img src="/Content/images/Toronto2013/youtube.png" width="35" height="35" alt="YouTube" /></a>

                <a href="http://pinterest.com/HageeMinistries/"><img src="/Content/images/Toronto2013/pintrest.png" width="35" height="35" alt="Pintrest" /></a></p>

                <p align="center">
                P.O. Box 1400, San Antonio, Texas 78295-1400  |  1-800-854-9899  |  Copyright © 2000-2013 John Hagee Ministries. All rights reserved.</p>

            </div>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table {
            border-spacing: 0px;
        }

        body, td, th {
            line-height: 1.3em;
            color: #0A1B39;
            padding: 0px;
            margin: 0px;
        }

        body {
            background-color: #D9D8DE;
        }

        body, td, th {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 10pt;
            line-height: 1.3em;
            color: #0A1B39;
        }

        body {
            background-color: #CCC;
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .media {
            margin: auto;
            width: 800px;
            height: 100px;
            background-color: #292929;
            padding-top: 0;
        }

        p {
            color: #FFF;
            font-size: 10px;
            line-height: 12px;
        }
    </style>

</asp:Content>
