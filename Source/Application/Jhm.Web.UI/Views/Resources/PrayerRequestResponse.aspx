﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Prayer Request
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Prayer Request</h2>
    <p>
        Diana and I thank you for trusting John Hagee Ministries with your prayer request.&nbsp;
        Your prayer request has been forwarded to our intercessory prayer team.&nbsp; We
        will stand with you in agreement according to the Word of God, believing Him for
        your supernatural answer.</p>
    <p>
        <b>Matt 18:19-20:&nbsp; “Again I say to you that if two of you agree on earth concerning
            anything that they ask, it will be done for them by my Father in heaven.&nbsp; For
            where two or three are gathered together in My name, I am there in the midst of
            them.”</b></p>
    <p>
        There is hope for a brighter tomorrow.&nbsp; Dreams still come true. There is a
        hope that is guaranteed by the promises of God.
    </p>
    <p>
        We join you in your faith in God and your supplications to Him for the perfect answer
        to your prayers.&nbsp;
    </p>
    <p>
        God Bless You and Those You Love,
        <br />
        Pastor John Hagee
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
