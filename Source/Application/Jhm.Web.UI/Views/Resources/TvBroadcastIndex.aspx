﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Tv Broadcast
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table.custom
        {
            border: 1px dashed #CCCCCC;
            border-collapse: collapse;
            width: 350px;
        }
        table.custom td
        {
            border: 1px dashed #CCCCCC;
            padding: 1px;
        }
        .customList
        {
            list-style-type: none;
            line-height: 20px;
        }
        .customList li
        {
            height: 32px;
        }
        
        .customList .easybutton.mini
        {
            width: 240px;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
            Today's TV Broadcast</h2>
        <div class="content">
            <table>
                <tr>
                    <td style="width: 220px" align="center">
                        <img src="<%: Url.Content("/Content/images/onetime-sm.jpg") %>" width="160" />
                    </td>
                    <td valign="top">
                        <h2>
                            What is on Television Today?</h2>
                        <b>Watch online now.. Please Choose a connection speed</b><br />
                        <ul class="customList">
                            <li><a class="easybutton mini" href="<%:Url.Action("TvBroadcast300k")%>">
                                <img src="<%: Url.Content("/Content/images/icon-watch.png") %>" />
                                High Bandwidth Video</a> </li>
                            <li><a class="easybutton mini" href="<%:Url.Action("TvBroadcast100k")%>">
                                <img src="<%: Url.Content("/Content/images/icon-watch.png") %>" />
                                Low Bandwidth Video</a> </li>
                            <%--<li><a class="easybutton mini" href="<%:Url.Action("TvBroadcast32kAudio")%>">
                                <img src="<%: Url.Content("/Content/images/icon-listen.png") %>" />
                                Audio Only (32k)</a> </li>--%>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page
    </a>
</asp:Content>
