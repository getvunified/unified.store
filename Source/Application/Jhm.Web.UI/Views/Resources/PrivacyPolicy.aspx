<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CartViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
    Privacy Policy
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content runat="server" ID="Main" ContentPlaceHolderID="MainContent">
    <h2>
        JOHN HAGEE MINISTRIES
    </h2>
    <h3>
        Privacy, Shipping, Billing, Return Policy, Terms of Use, and Contact
    </h3>
    <p>
        JOHN HAGEE MINISTRIES is committed to maintaining your trust and confidence. The
        following online privacy policy is intended to protect and secure the personally
        identifiable information (any information by which you can be identified) you provide
        to us online.
    </p>
    <p>
        In addition, JOHN HAGEE MINISTRIES will not send you e-mail that you have not agreed
        to receive. We may periodically send you e-mail announcing news and event information.
        If you choose to supply your postal address in an online form, you may receive mailings
        from us as well.
    </p>
    <p>
        <b>No Sharing of Personal Information</b>
        <br />
        JOHN HAGEE MINISTRIES never sells, rents, leases or exchanges your personal information
        with other organizations. JOHN HAGEE MINISTRIES assures you that the identity of
        all who contact us through this Web site will be kept confidential. JOHN HAGEE MINISTRIES
        may occasionally send mailings on behalf of other organizations or ministries when
        their interests align with JOHN HAGEE MINISTRIES.
    </p>
    <p>
        <b>Security</b><br />
        JOHN HAGEE MINISTRIES is committed to ensuring the security of your personal information.
        To prevent unauthorized access, maintain data accuracy, and ensure the proper use
        of information, we have established and implemented appropriate physical, electronic
        and managerial procedures to safeguard and secure the information we collect online.
        JOHN HAGEE MINISTRIES uses Internet Encryption Software, Secure Socket Layer (SSL)
        Protocol when collecting or transferring sensitive data such as credit card information.
        Any information you enter is encrypted at your browser, sent over the public Internet
        in encrypted form, and then de-encrypted at our server. Once we receive your credit
        card information, it is accessible only to a small number of trusted JOHN HAGEE
        MINISTRIES employees who have been specially trained in processing this information.
    </p>
    <p>
        <b>Cookies</b><br />
        From time to time, we may send a "cookie" to your computer. A cookie is a small
        piece of data that is sent to your browser from a Web server and stored on your
        computer's hard drive. A cookie can't read data off your hard drive or read cookie
        files created by other sites. Cookies do not damage your system. We use cookies
        to recognize you when you return to our sites, or to identify which areas of our
        network of Web sites you have visited (i.e. e-commerce sites, etc.). We may use
        this information to better personalize the content you see on our sites.
    </p>
    <p>
        Many Web sites place cookies on your hard drive. You can choose whether to accept
        cookies by changing the settings of your browser. Your browser can refuse all cookies,
        or show you when a cookie is being sent. If you choose not to accept these cookies,
        your experience at our site and other Web sites may be diminished and some features
        may not work as intended.
    </p>
    <p>
        <b>Children's Online Privacy</b>
        <br />
        We are concerned about the privacy of young children and do not knowingly collect
        any personal information from a child under 13. We encourage you to become involved
        with your child's access to the Internet and to our site in order to ensure that
        his or her privacy is well protected.
    </p>
    <p>
        To read more about online safety and how to become more involved with your child's
        online experience, visit the Federal Trade Commission Web site. You may also benefit
        from the information and resources available on ProtectKids.com.
    </p>
    <p>
        <a name="shipping"></a><b>Shipping Policy</b>
        <br />
        Most orders are shipped within 3 business days. All orders are shipped via USPS,
        DHL, FedEx, UPS or comparable carrier. Processing time of your order can vary from
        3-14 days depending on product availability, or in some cases up to 4-6 weeks. Orders
        shipped by ground service average 4-10 business days from date of order to reach
        its destination. Orders shipped by priority service (Priority Shipping) average
        3-7 business days from date of order to reach its destination. Additional charges
        may apply depending on weight. We will notify you of any additional charges before
        shipping. When calculating actual ship date, please do not count Saturdays, Sundays
        or Federal Holidays. DHL will not ship to P.O. Boxes. Please include a complete
        street address for all orders. If ordering internationally, any taxes, customs,
        or related fees will be your responsibility at the time of receipt.</p>
    <p>
        <b>Billing Policy</b>
        <br />
        We accept Visa, MasterCard, Discover, and American Express. All online orders are
        processed through 128 bit SSL encryption to protect your credit card information
        during your online transaction.&nbsp;All prices and figures are listed in US Dollars.
    </p>
    <p>
        <b>JHM Return & Exchange Policy:</b>
        <br />
        We want you to be completely satisfied with your purchase.</p>
    <ul>
        <li>
            You may exchange any unused products (if products are shrink-wrapped, such products must remain unopened) 
            for other products of equal or greater value within 15 days of receipt at customerís expense.  
            JHM does not offer store credit.
        </li>
        <li>
            Any damaged, defective, or incorrect shipment of JHM products may be exchanged for the same product within 30 days of ship date.
            <ul>
                <li>Damaged and/or defective items must be sent back to the Ministry in order to process an exchange.</li>
                <li>The Ministry is not responsible for products returned for exchange that become lost in the mail.</li>
            </ul>
        </li>
        <li>
            Prior to submitting any products for exchange, a Return Merchandise Authorization (RMA) number is required. 
            To obtain your RMA number, please call Customer Service at (800) 600-9743, Monday- Friday, 8:30-5:00 CST.  
            Products received without an RMA number will be considered a donation to the Ministry Missions Outreach.
        </li>
        <li>
            Downloads are non-refundable. 
        </li>
        <li>
            Other than the exceptions set forth above, there are no returns and all sales are final.
            If you have questions regarding our return policy - please call Customer Service at (800) 600-9743.
        </li>
    </ul>
    <p>
        <b>Account policy</b><br />
        JOHN HAGEE MINISTRIES reserves&nbsp;the right to de-activate or remove any user
        accounts provided at <a href="http://www.jhm.org">www.jhm.org</a> without&nbsp;a&nbsp;notice
        if user has provided wrong information, has violated common Internet policies or
        if the account has been inactive for more than 60 days.</p>
    <p>
        <b>Download License</b><br />
        Downloads are&nbsp;solely for personal, noncommercial use. Copies of download can
        only be stored on computers or devices owned by the person or entity who purchased
        the download.&nbsp;Additional licenses of downloads can be purchases from www.jhm.org.
    </p>
    <p>
        <b>Wholesale</b><br />
        Thank you for your interest in our products! Please review our Wholesale Policy for more information.<br/>
        <a href="/Content/media/Wholesale Policy.pdf" target="_self">Wholesale Policy</a>
    </p>
    <p>
        <b>Contacting Us</b>
        <br />
        If you have comments or questions about any of our policies or our website, please
        contact us. For information about your payment or order, please contact us at 1-800-854-9899<span
            style="font-family: 'Helvetica','sans-serif';">&nbsp;during office hours (8:30am
        - 5pm CST).</span>
    </p>
    <p>
        Our mailing addresses are:
    </p>
    <table width="100%">
        <tr>
            <td valign="top">
                <p>
                    <img alt="USA flag" src="<%: Url.Content("~/Content/images/usa.png") %>" /> United States
                    <br />
                    PO Box 1400<br />
                    San Antonio, TX<br />
                    78295-1400
                </p>
            </td>
            <td  valign="top">
                <p>
                    <img alt="CA flag" src="<%: Url.Content("~/Content/images/canada.png") %>" /> Canada
                    <br />
                    P.O. Box 9900<br />
                    Toronto, Ontario<br />
                    M3C 2T9
                </p>
            </td>
            <td  valign="top">
                <p>
                    <img alt="UK flag" src="<%: Url.Content("~/Content/images/uk.png") %>" /> United Kingdom
                    <br />
                     
                     Mailing Address: <br/>
                     P.O. Box 2959<br />
                     Swindon, UK<br />
                     SN6 7WS<br/> <br/>   

                    Legal Address:<br />
                    2 Blackworth court<br />
                    Blackworth Industrial Estate<br />
                    Highworth, Wiltshire, UK<br />
                    SN6 7NS<br />
                    

                </p>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content runat="server" ID="SidebarFirst" ContentPlaceHolderID="SidebarFirstContent">
</asp:Content>
<asp:Content runat="server" ID="SidebarSecond" ContentPlaceHolderID="SidebarSecondContent">
</asp:Content>
