﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.UI.Models.JhmMagSubscriptionViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Models" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>


<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Magazine Subscription
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <link href="<%: Url.Content("~/Content/css/forms.css") %>" rel="stylesheet" />
    <%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] { "PhoneCode" })%>
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("PartialViews/_ShadowBoxLogon",new LogOnModel()) %>
    <script type="text/javascript" >

        function clearPersonalOptions() {
            $("#MainContent_oneYearSubscription").prop("checked", false);
        }

        function ClearGiftOptions() {
            $("#MainContent_oneYearGiftSubscription").prop("checked", false);
        }
        
        function ShowGiftSubscriptionDialog(subscriptionCode) {
            if (!validateOptionSelected(subscriptionCode)) {
                jQuery("#" + subscriptionCode + "SelectionMessage").show();
            }
            else {
                jQuery("#" + subscriptionCode + "SelectionMessage").hide();
                var priceCode = jQuery('input[subcode="' + subscriptionCode + '"]:checked').val();
                var label = jQuery('#label' + subscriptionCode + priceCode);
                jQuery('#giftSubsciptionCode').val(subscriptionCode);
                jQuery('#SelectedPriceCode').val(priceCode);
                jQuery("#popupSelectedPriceText").html(label.html().replace("Receive","Give"));
                jQuery('#giftSubPopup').dialog('open');
            }
        }
        
        function validateOptionSelected(subscriptionCode) {
            var selectedOptions = jQuery('input[subcode="' + subscriptionCode + '"]:checked');
            return selectedOptions.length == 1;
        }
        
        $(function () {
            jQuery('#giftSubPopup').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                buttons: {
                    'Add to Cart': function () {
                        validateGiftForm();
                        //
                    },
                    Cancel: function () {
                        jQuery(this).dialog('close');
                    }
                }
            });
        });

        function validateGiftForm() {
            var formIsValid = true;

            var address1 = jQuery("#GiftSubscriber_DeliveryAddress_Address1");
            var city = jQuery("#GiftSubscriber_DeliveryAddress_City");
            var country = jQuery("#GiftSubscriber_DeliveryAddress_Country");
            var state = jQuery("#GiftSubscriber_DeliveryAddress_State");
            var firstName = jQuery("#GiftSubscriber_FirstName");
            var lastName = jQuery("#GiftSubscriber_LastName");
            var areaCode = jQuery("#GiftSubscriber_PrimaryPhone_AreaCode");
            var phoneNumber = jQuery("#GiftSubscriber_PrimaryPhone_Number");


            if (!validateJQueryField(firstName)) {
                formIsValid = false;
            }

            if (!validateJQueryField(lastName)) {
                formIsValid = false;
            }

            if (!validateJQueryField(address1)) {
                formIsValid = false;
                
            }

            if (!validateJQueryField(city)) {
                formIsValid = false;
            }

            if (!validateJQueryField(country)) {
                formIsValid = false;
            }

            if ( !validateJQueryField(state) || state.val() == "Select State/Provice/Division") {
                state.addClass("input-validation-error");
                formIsValid = false;
            }
            if (!validateJQueryField(areaCode)) {
                formIsValid = false;
            }
            
            if (!validateJQueryField(phoneNumber)) {
                formIsValid = false;
            }

            if (formIsValid) {
                jQuery('#GiftSubscriberForm').trigger('submit');
            } else {
                jQuery('#popupValidateMessage').show();
                setTimeout(function() { jQuery('#popupValidateMessage').fadeOut(1000, null); }, 4000);
                return;
            }
        }
        
        function validateJQueryField(jQueryField) {
            if (jQuery.trim(jQueryField.val()).length == 0) {
                jQueryField.addClass("input-validation-error");
                return false;
            }
            jQueryField.removeClass("input-validation-error");
            return true;
        }
        
        function submitSubscriptionForm(subscriptionCode) {
            var isValid = validateOptionSelected(subscriptionCode);
            if (isValid) {
                jQuery("#" + subscriptionCode + "SelectionMessage").hide();
                <% if (Request.IsAuthenticated)
                   {%>
                if (subscriptionCode == '<%= Model.ActiveSubscription == null? String.Empty : Model.ActiveSubscription.SubscriptionCode %>') {
                    if (!confirm('Our records show that you have an active subscription valid through <%= Model.ActiveSubscription == null? String.Empty : Model.ActiveSubscription.ExpirationDateString %>. Are you sure you want to renew your subscription?')) {
                        return false;
                    }
                }
                jQuery('#SubsciptionCode').val(subscriptionCode);
                jQuery('#SubscriptionForm').trigger('submit');
                return false;
                <% } %>
            } else {
                jQuery("#" + subscriptionCode + "SelectionMessage").show();
            }
            return isValid;
        }

        function populateReturnUrl(rbMagazine) {
            <% if ( !Request.IsAuthenticated ) { %>
            $("#addMagazineTocart").attr("href", "javascript:showLogonShadowBox('<%: Url.Action("JhmMagazineSubscription") %>?selectedMagazine=" + rbMagazine.value + "');");
            <% } %>
        }
    </script>
    <div id="content-header">
        <h2 class="section-title">JHM Magazine Subscription</h2>
    </div>
    <% using (Html.BeginForm("JhmMagazineSubscription", "Resources", FormMethod.Post, new { id = "SubscriptionForm" }))
       { %>
           <%: Html.HiddenFor(m => m.SubsciptionCode) %>
           <%: Html.HiddenFor(m => m.ActiveSubscription.Id) %>
         <%foreach (var sub in Model.AvailableSubscriptions)
           { %>
            <div class="block">
                <h2 class="title">
                    <%: sub.Description %></h2>
                <div class="content">
                    <div class="region-content-bottom">
                        <h3 class="section-title">Subscription Options</h3>
                    </div>
                    <% foreach (var price in sub.Prices)
                       { %>
                        <div class="region-content-bottom">
                            <input type="radio" onclick="populateReturnUrl(this);" subCode="<%: sub.SubscriptionCode %>" id="<%: sub.SubscriptionCode %><%: price.PriceCode %>" name="SelectedPriceCode" value="<%: price.PriceCode %>" <%= Model.SelectedPriceCode == price.PriceCode ? "checked" : String.Empty %> /><label for="<%: sub.SubscriptionCode %><%: price.PriceCode %>"><%: price.Description %></label><br />
                            <div>
                                <em>
                                    <label id="label<%: sub.SubscriptionCode %><%: price.PriceCode %>" for="<%: sub.SubscriptionCode %><%: price.PriceCode %>" style="margin-left: 30px;">Receive by <%: sub.Fullfilment %> <%: price.NumberOfIssues %> issues bi-monthly for just <%: price.Price.FormatWithClientCulture(Client,"C") %>.</label> 
                                </em>
                            </div>
                            <br />
                            <br />
                        </div>
                    <% } %>
                        <span id="<%: sub.SubscriptionCode %>SelectionMessage" style="color:red; display:none">* Please select a subscription option from above.</span>
                        <br/>
                        <br/>
                        <%:Html.AuthenticateWithShadowBoxActionLink<ResourcesController>(c => c.JhmMagazineSubscription(null,(string) null), "Add to Cart", new{@class="easybutton buy",onclick="return submitSubscriptionForm('"+ sub.SubscriptionCode +"')", id="addMagazineTocart"}) %>
                        <%--<a href="#" class="easybutton buy right" onclick="submitSubscriptionForm('<%: sub.SubscriptionCode %>');" title="Subscribe to selected magazine">Add to Cart</a>--%>
                        <a href="#" class="easybutton buy right" id="clearSubscriptionOptions" onclick="ShowGiftSubscriptionDialog('<%: sub.SubscriptionCode %>');" title="Give a subscription gift to a friend or loved one.">Gift Subscription</a>
                    <br/>
                </div>
            </div>
<%      }
    } %>
    <div id="giftSubPopup" title="Gift Subscription">
                <% using (Html.BeginForm("AddGiftSubscriptionToCart", "Resources", FormMethod.Post, new { id = "GiftSubscriberForm" }))
               {%>
                    <%: Html.HiddenFor(m => m.SubsciptionCode,new{id="giftSubsciptionCode"}) %>
                    <%: Html.HiddenFor(m => m.SelectedPriceCode) %>
                    <p>
                    Give a subscription gift to a friend or loved one.
                    </p>
                    <p id="popupSelectedPriceText" style="font-style:italic;font-weight:bold;font-size:smaller;">
                        <br/>
                    </p>
                <div >
                    <strong><%: Html.LabelFor(model => model.GiftSubscriber.FirstName) %><span class="form-required">*</span></strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.FirstName) %>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.FirstName) %>
                </div>
                <br/>
                <div >
                    <strong><%: Html.LabelFor(model => model.GiftSubscriber.LastName) %><span class="form-required">*</span></strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.LastName) %>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.LastName) %>
                </div>
                <br/>
                <div >
                    <strong>Recipient's <%: Html.LabelFor(model => model.GiftSubscriber.Email) %></strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.Email) %>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.Email) %>
                </div>
                <br/>
                    <h4>Recipient's Shipping Address</h4>
                <div >
                    <strong><%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.Address1) %><span class="form-required">*</span></strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.DeliveryAddress.Address1) %>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.DeliveryAddress.Address1) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.Address2) %></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.DeliveryAddress.Address2) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.City) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.DeliveryAddress.City) %>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.DeliveryAddress.City) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.Country) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor
                                (
                                    m => m.GiftSubscriber.DeliveryAddress.Country,
                                                                Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name", "840"),
                                    String.Empty,
                                    new
                                        {
                                            @style = "width:292px",
                                            @onchange = "set_city_state__and_phoneCode(this,'GiftSubscriber_DeliveryAddress_State',countryCodes,'GiftSubscriber_PrimaryPhone_CountryCode')"
                                        }//this,'State',countryCodes,'PhoneCountryCode'
                                )%>
                        <%: Html.ValidationMessageFor(model => model.GiftSubscriber.DeliveryAddress.Country) %>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.State) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor(model => model.GiftSubscriber.DeliveryAddress.State, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @style = "width:292px" })%>
                    <%: Html.ValidationMessageFor(model => model.GiftSubscriber.DeliveryAddress.State) %>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.DeliveryAddress.Zip) %>
                    </strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.GiftSubscriber.DeliveryAddress.Zip) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.GiftSubscriber.PrimaryPhone) %><span class="form-required">*</span>:
                    </strong>
                </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor(model => model.GiftSubscriber.PrimaryPhone.CountryCode, new SelectList(Country.GetAll<Country>().Select(x => x.PhoneCode).Distinct().OrderBy(x => x),"+1"), new { @style = "width:60px" })%>
                    <%: Html.TextBoxFor(m => m.GiftSubscriber.PrimaryPhone.AreaCode, new { @style = "width:60px", @size = "4", @maxlength = "4", @class="label-textbox", @label="Area code" })%>
                    <%: Html.TextBoxFor(m => m.GiftSubscriber.PrimaryPhone.Number, new { @style = "width:156px", @size = "10", @maxlength = "10", @class = "label-textbox", @label = "Phone number" })%>
                </div>

                <br/>
                  <br/>
                    <span id="popupValidateMessage" style="color:red;display:none;">* Please fill out required fields and try again</span>
                <%} %>
            </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/WholeRightMenu.ascx"); %>
</asp:Content>


