﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Church Broadcast
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table.custom
        {
            border: 1px dashed #CCCCCC;
            border-collapse: collapse;
            width: 350px;
        }
        table.custom td
        {
            border: 1px dashed #CCCCCC;
            padding: 1px;
        }
        .customList
        {
            list-style-type: none;
            line-height: 20px;
        }
        .customList li
        {
            height: 32px;
        }
        
        
        
        .customList .easybutton.mini
        {
            width: 240px;
            text-align: left;
        }
    </style>
    <script type="text/javascript" src="/Content/js/flowplayer-3.2.6.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
            Cornerstone Church Live</h2>
        <div class="content">
            <h2>
                Watch Cornerstone Church Live... Online Every Sunday!</h2>
            <table>
                <tr>
                    <td style="width: 180px" align="center" valign="top">
                        <img src="<%: Url.Content("/Content/images/satellite_128.png") %>" />
                    </td>
                    <td valign="top">
                        <div style="width: 600px; height: 360px;" id="container">
                        </div>
                        <% var player = "hd"; // sd | multibitrate | hd
                       if ( player == "sd" )
                       { %>
                        <script type="text/javascript">
                            $f('container', '/Content/media/flash/flowplayer.commercial-3.2.7.swf', {
                                key: '#@f6e30063d14332a76f4',
                                clip: {
                                    url: 'FOT2011@17189',
                                    live: true,
                                    provider: 'rtmp'
                                },
                                plugins: {
                                    rtmp: {
                                        netConnectionUrl: 'rtmp://cp141681.live.edgefcs.net/live',
                                        url: '/Content/media/flash/flowplayer.rtmp-3.2.3.swf',
                                        subscribe: true
                                    }
                                }
                            }).ipad();
		               </script>
                       <% } 
                       else if ( player == "multibitrate")
                        { %>
                           <script type="text/javascript">
                               $f('container', '/Content/media/flash/flowplayer.commercial-3.2.7.swf', {
                                   key: '#@f6e30063d14332a76f4',
                                   clip: {
                                       url: '/Content/streaming/bitrates.smil.xml',
                                       live: true,
                                       provider: 'rtmp',
                                       // use smil and bwcheck when resolving the clip URL
                                       urlResolvers: ['smil', 'bwcheck']
                                   },
                                   plugins: {
                                       // the SMIL plugin reads in and parses the SMIL, and provides
                                       // the bitrates info to the bw detection plugin
                                       smil: {
                                           url: '/Content/media/flash/flowplayer.smil-3.2.2.swf'
                                       },

                                       // bandwidth check plugin
                                       bwcheck: {
                                           url: '/Content/media/flash/flowplayer.bwcheck-3.2.5.swf',

                                           // HDDN uses Wowza servers
                                           serverType: 'wowza',

                                           // we use dynamic switching, the appropriate bitrate is switched on the fly
                                           dynamic: true,

                                           netConnectionUrl: 'rtmp://cp141681.live.edgefcs.net/live',

                                           // show the selected file in the content box. This is not used in real installations.
                                           onStreamSwitchBegin: function (newItem, currentItem) {
                                               $f().getPlugin('content').setHtml("Will switch to: " + newItem.streamName +
                " from " + currentItem.streamName);
                                           },
                                           onStreamSwitch: function (newItem) {
                                               $f().getPlugin('content').setHtml("Switched to: " + newItem.streamName);
                                           }
                                       },

                                       // RTMP streaming plugin
                                       rtmp: {
                                           url: '/Content/media/flash/flowplayer.rtmp-3.2.3.swf',
                                           netConnectionUrl: 'rtmp://cp141681.live.edgefcs.net/live'
                                       },

                                       // a content box so that we can see the selected bitrate. This is not normally
                                       // used in real installations.
                                       content: {
                                           url: '/Content/media/flash/flowplayer.content-3.2.0.swf',
                                           top: 0, left: 0, width: 400, height: 150,
                                           backgroundColor: 'transparent', backgroundGradient: 'none', border: 0,
                                           textDecoration: 'outline',
                                           style: {
                                               body: {
                                                   fontSize: 14,
                                                   fontFamily: 'Arial',
                                                   textAlign: 'center',
                                                   color: '#ffffff'
                                               }
                                           }
                                       }
                                   }
                               }).ipad();
		                </script>
                       <% }
                       else if ( player == "hd"){  %>
                        <script type="text/javascript" language="JavaScript">
                            var player = $f
		                        (
			                        "container",
			                        {
			                            src: "/Content/media/flash/flowplayer.commercial-3.2.7.swf"
			                        },

			                        {
			                            key: '#@f6e30063d14332a76f4',
			                            // Apparently debug mode does not work on IE
			                            debug: false,
			                            log:
		     	                        	{
		     	                        	    level: 'warn'
		     	                        	},
			                            clip:
		     	                        	{
		     	                        	    live: true,
		     	                        	    autoPlay: true,
		     	                        	    provider: 'akamai',
		     	                        	    type: 'video',
		     	                        	    baseUrl: 'http://getvhdflash-f.akamaihd.net/'
		     	                        	},


			                            //   scaling:
			                            //       {
			                            //           scale: 'half'
			                            //       },
			                            //   playlist:
			                            //       [
			                            //               'http://getvhdflash-f.akamaihd.net/GETV-HDFLASH-1_PRIMARY_400@36799',
			                            //               'http://getvhdflash-f.akamaihd.net/GETV-HDFLASH-1_PRIMARY_800@36799',
			                            //               'http://getvhdflash-f.akamaihd.net/GETV-HDFLASH-1_PRIMARY_1200@36799',
			                            //               'http://getvhdflash-f.akamaihd.net/GETV-HDFLASH-1_PRIMARY_2400@36799'
			                            //       ],

			                            plugins:
	 			                        {
	 			                            controls:
	        		                        	{
	        		                        	    url: '/Content/media/flash/flowplayer.controls-3.2.5.swf'
	        		                        	},
	 			                            akamai:
	           		                        	{
	           		                        	    url: '/Content/media/flash/AkamaiFlowPlugin.swf'
								    , akamaiMediaType: 'akamai-hdn-multi-bitrate'

	           		                        	    //, tokenService:{url:escape(url here)}

	           		                        	    //, subClip:{clipBegin:400}
	           		                        	    //, useNetSession:true
	           		                        	    //, netsessionMode:'never'
	           		        	                    , mbrObject:
	           		                	            [
	           		                        	    	{ src: "GETV_HDFLASH_1_PRIMARY_400@36799", width: 626, height: 352, bitrate: "400" },
	           		                            		{ src: "GETV_HDFLASH_1_PRIMARY_800@36799", width: 626, height: 352, bitrate: "800" },
		           		                            	{ src: "GETV_HDFLASH_1_PRIMARY_1200@36799", width: 852, height: 480, bitrate: "1200" },
		           		                            	{ src: "GETV_HDFLASH_1_PRIMARY_2400@36799", width: 852, height: 480, bitrate: "2400" }



	           		                        	    //{ src: "GETV_HDFLASH_1_PRIMARY_400@36799", width: 626, height: 352, bitrate: "400" },
	           		                        	    //{ src: "GETV_HDFLASH_1_PRIMARY_800@36799", width: 626, height: 352, bitrate: "800" },
	           		                        	    //{ src: "GETV_HDFLASH_1_PRIMARY_1200@36799", width: 960, height: 542, bitrate: "1200" },
	           		                        	    //{ src: "GETV_HDFLASH_1_PRIMARY_2400@36799", width: 1280, height: 720, bitrate: "2400" }
	        	   		                            ]

		           		                            , mbrStartingBitrate: 2400000
	           		                        	    //, mbrStartingIndex:1

								    , retryLive: true
								    , retryInterval: 5
								    , liveTimeout: 5
								    , connectionAttemptInterval: 5

	           		                        	    //, connectAuthParams:'auth=connectionAuthToken'
	           		                        	    //, streamAuthParams:'auth=livestreamAuthToken'
	           		                        	    //, primaryToken:'1336218178_ee70588889d6e859ffcd58c49c3872be'
	           		                        	    //, akamaiMediaData:{}


	           		                        	    //, useNetSession:false
	           		                        	    //, enableNetSessionDiscovery:false
	           		                        	    //, enableAlternateServerMapping:false
	           		                        	    //, enableEndUserMapping:false
	           		                        	    //, playerVerificationChallenge:'somesalt'
	           		                        	    //, genericNetStreamProperty:{propertyName:'enableAlternateServerMapping', value:true}
	           		                        	}
	 			                        }
			                        }
		                        );
                        </script>
                        <% } %>
                        <table class="custom" rules="all" border="1">
                            <tr>
                                <td colspan="2" align="center">
                                    <h4>
                                        Cornerstone Church Service Times</h4>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <h6>
                                        Each Sunday</h6>
                                </td>
                                <td>
                                    <ul>
                                        <li>Early Service: 8:30a - 10:00a CST</li>
                                        <li>Sunday School: 10:15a - 10:45a CST</li>
                                        <li>Late Service: 11:00a - 12:30p CST</li>
                                        <li>Evening Service: 06:30p - 08:00p CST</li>
                                    </ul>
                                    <h6>
                                        * All Times Are Subject To Change
                                    </h6>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <a href="<%: Url.Action("OnlineProgramming") %>" class="returnToOnlineProgrammingLink">
        Return to Online Programming Page </a>
</asp:Content>
