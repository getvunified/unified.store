﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Why Christians Should Support Israel
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .customOrderedList {list-style-type: none;}
        .customOrderedList li:before {content: counter(section, lower-latin) ") ";}
        .customOrderedList li { counter-increment: section;}
    </style>
    <h1>
        Why Christians Should Support Israel</h1>
    <h2>
        The Apple of HIS Eye...</h2>
    <p>
        Everything Christians do should be based upon the Biblical text. Here are seven
        solid Bible reasons why Christians should support Israel.</p>
    <ol>
        <li>
            <p>
                Genesis 12:3 "And I will bless them that bless thee and curse him that curseth thee;
                and in thee shall all nations of the earth be blessed." Point: God has promised
                to bless the man or nation that blesses the Chosen People. History has proven beyond
                reasonable doubt that the nations that have blessed the Jewish people have had the
                blessing of God; the nations that have cursed the Jewish people have experienced
                the curse of God.
            </p>
        </li>
        <li>
            <p>
                St. Paul recorded in Romans 15:27 "For if the Gentiles have shared in their (the
                Jews) spiritual things, they are indebted to minister to them also in material things."
            </p>
            <p>
                Christians owe a debt of eternal gratitude to the Jewish people for their contributions
                that gave birth to the Christian faith. Jesus Christ, a prominent Rabbi from Nazareth
                said, "Salvation is of the Jews!" (St. John 4:22) consider what the Jewish people
                have given to Christianity:
            </p>
                    <ol class="customOrderedList">
                        <li>The Sacred Scripture</li>
                        <li>The Prophets</li>
                        <li>The Patriarchs</li>
                        <li>Mary, Joseph, and Jesus Christ of Nazareth</li>
                        <li>The Twelve Disciples</li>
                        <li>The Apostles</li>
                    </ol>
            <p>
                It is not possible to say, "I am a Christian" and not love the Jewish people. The
                Bible teaches that love is not what you say, but what you do. (1 John 3:18) "A bell
                is not a bell until you ring it, a song is not a song until you sing it, love is
                not love until you share it."
            </p>
        </li>
        <li>
            <p>
                While some Christians try to deny the connection between Jesus of Nazareth and the
                Jews of the world, Jesus never denied his Jewishness. He was born Jewish, He was
                circumcised on the eighth day in keeping with Jewish tradition, He had his Bar Mitzvah
                on his 13th birthday, He kept the law of Moses, He wore the Prayer Shawl Moses commanded
                all Jewish men to wear, He died on a cross with an inscription over His head, "King
                of the Jews!
            </p>
            <p>
                Jesus considered the Jewish people His family. Jesus said (Matthew 25:40) "Verily
                I say unto you, Inasmuch as you have done it unto one of the least of these my brethren
                (the Jewish people… Gentiles were never called His brethren), ye have done it unto
                me.
            </p>
        </li>
        <li>
            <p>
                "Pray for the peace of Jerusalem, they shall prosper that love thee." (Psalm 122:6)
                the scriptural principle of prosperity is tied to blessing Israel and the city of
                Jerusalem.
            </p>
        </li>
        <li>
            <p>
                Why did Jesus Christ go to the house of Cornelius in Capernaum and heal his servant,
                which was ready to die? What logic did the Jewish elders use with Jesus to convince
                Him to come into the house of a Gentile and perform a miracle?
            </p>
            <p>
                The logic they used is recorded in Luke 7:5; "For He loveth our nation, and He hath
                built us a synagogue." The message? This Gentile deserves the blessing of God because
                he loves our nation and has done something practical to bless the Jewish people.
            </p>
        </li>
        <li>
            <p>
                Why did God the Father select the house of Cornelius in Caesarea (Acts Chapter 10)
                to be the first Gentile house in Israel to receive the Gospel? The answer is given
                repeatedly in Acts 10.
            </p>
            <p>
                Acts 10:2 "a devout man, (Cornelius) and one that feared God with all his house,
                which gave much alms to the people, and prayed to God always." Who were the people
                to whom Cornelius gave these alms? They were the Jews
            </p>
            <p>
                Again is Acts 10:4 "… thy prayers and thine alms are come up for a memorial before
                God."
            </p>
            <p>
                Again in Acts 10:31 "… and thine alms are had in remembrance in the sight of God."<br />
                The point is made three times in the same chapter. A godly Gentile who expressed
                his unconditional love for the Jewish people in a practical manner was divinely
                selected by heaven to be the first Gentile house to receive the Gospel and the first
                to receive the outpouring of the Holy Spirit.
            </p>
            <p>
                These combined Scriptures verify that PROSPERITY (Genesis 12:3 and Psalm 122:6),
                HEALING (Luke 7:1-5) and the OUTPOURING OF THE HOLY SPIRIT came first to Gentiles
                that blessed the Jewish people and the nation of Israel in a practical manner.
            </p>
        </li>
        <li>
            <p>
                We support Israel because all other nations were created by an act of men, but Israel
                was created by an act of God! The Royal Land Grant that was given to Abraham and
                his seed through Isaac and Jacob with an everlasting and unconditional covenant.
                (Genesis 12:1-3, 13:14-18, 15:1-21, 17:4-8, 22:15-18, 26:1-5 and Psalm 89:28-37.)
            </p>
        </li>
    </ol>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
