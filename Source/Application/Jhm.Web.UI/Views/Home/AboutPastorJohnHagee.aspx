﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Pastor Hagee
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <img id="Img1" style="padding-bottom: 10px; padding-left: 10px; padding-right: 10px; float: right; padding-top: 10px"
        border="0" hspace="5" alt="Pastor Matthew Hagee"
        vspace="5" runat="server" src="~/Content/Images/PastorJohnHagee_resized.jpg"
        width="300" height="452" />
    <div>
        <div>
            <h1>
                PASTOR JOHN C. HAGEE
            </h1>
            <h2>
                Senior Pastor Cornerstone Church
            </h2>
        </div>
        
        <p>
            <b>Pastor John C. Hagee is the founder and Senior Pastor of Cornerstone Church in San Antonio, Texas,</b> a non-denominational evangelical church with more than <b><i>20,000 active members</i></b>. Pastor Hagee has served the Lord in the gospel ministry for over 57 years. 
		</p>
		<p>
		    Pastor Hagee is founder and President of John Hagee Ministries, which telecasts his national radio and television teachings throughout America and the nations of the world, and is now seen worldwide twenty-four hours a day, seven days a week on <b><i><a href="http://GETV.org">GETV.org</a></i></b>.
		</p>
		<p>
		    Pastor John Hagee received a Bachelor of Science degree from Southwestern Assemblies of God University in Waxahachie, Texas and a second Bachelor of Science degree from Trinity University in San Antonio, Texas.  He earned his Master’s Degree in Educational Administration from the University of North Texas in Denton, Texas.   
		</p>
		<p>
            Pastor Hagee has received Honorary Doctorates from Oral Roberts University, Canada Christian College, and Netanya Academic College of Israel. 
		</p>
		<p>
            He is the author of 34 major books, many of which were on the New York Times Best Seller’s List including “Four Blood Moons”, which is now featured as a successful docudrama film.  His most recent book “The Three Heavens” will be released this spring by Worthy Publishing.  Among his other works are commentary study Bibles, novels and numerous devotionals.
		</p>
		<p>
		    Over the years, John Hagee Ministries has given more than $85 million toward humanitarian causes in Israel.  In 2006, Dr. Hagee founded, and is the National Chairman of Christians United For Israel, a grass roots national association through which every pro—Israel Christian ministry, para-church organization, or individual in America can speak and act with one voice in support of Israel and the Jewish people.   
		</p>
		<p>
            Dr. Hagee gains support for this worthy cause by conducting <b><i>A Night to Honor Israel</i></b> in every major city in America and by organizing an annual <b><i>Washington D.C.-Israel Summit</i></b> where thousands of CUFI delegates from every state in the union have the opportunity to meet the members of congress face to face on behalf of Israel.  
		</p>
        <p>
            Christians United for Israel has grown to become the largest Christian pro-Israel organization in the United States – with <b><i>over 2.1 million members</i></b>-- and one of the leading Christian grassroots movements in the world. Christians United for Israel has recently opened offices in the United Kingdom and Canada to combat anti-Semitism. 
        </p>
		<p>
		    Pastor Hagee and his wife Diana are blessed with five children and thirteen grandchildren.  
		</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
