﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    The Difference
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%" cellspacing="5" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td>
                    <div style="text-align: left">
                        <img border="0" runat="server" src="~/Content/Images/DifferenceHeader.png" width="502" height="184"
                            style="Filter: Alpha(Opacity=120, FinishOpacity=20, Style=3, StartX=0, StartY=0, FinishX=0, FinishY=0)"/>&nbsp;
                    </div>
                    <div class="PageBody">
                        <p>
                            The Difference is a faith-based lifestyle show where biblical truths and  everyday life intertwine in a fun and insightful way. On each episode  Pastor Matthew and Kendal discuss trends, culture, and most importantly  how faith applies to every person's life every day.
                        </p>
                        <p>
                           The Difference with Matthew and Kendal Hagee Wednesdays @ 8pm CT on <a href="http://www.getv.org">www.getv.org</a>
                        </p>

                    <%--
                        <p>
                            Pastor Matthew Hagee has started a new and exciting program known as THE DIFFERENCE.
                        </p>
                        <p>
                            THE DIFFERENCE is an interactive multimedia ministry targeted at drawing all
                            generations back to the foundations of faith that America was established on.
                            THE DIFFERENCE takes a new approach in sharing the Gospel by using the latest technologies
                            (such as the internet and podcasting) along with a national broadcast.
                        </p>
                        <p>
                            The weekly broadcasts that are taped live at Cornerstone Church are a spirit-filled
                            combination of extravagant praise and worship along with the uncompromised Word
                            of God shared by Pastor Matthew Hagee.
                        </p>
                        <p>
                            You can watch THE DIFFERENCE on:<br/>
                            Sunday 10:30pm eastern<br/>
                            Sky Angel Channel 262<br/>
                            <br/>
                            Sunday evenings at 6:00pm EST<br/>
                            GOD TV-USA DirecTV channel 365
                        </p>--%>
                    </div>
                </td>
                <td style="text-align: center; width: 200px; vertical-align: top">
                    <img width="300" vspace="5" hspace="5" border="0"  runat="server" src="~/Content/images/The-Difference-325.jpg" 
                        alt="Pastor Matthew Hagee" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
