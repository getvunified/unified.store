﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.HomeViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/RotatingBanner.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery.cycle.all.min.js") %>"></script>
        
    <script src="<%:Url.Content("~/Scripts/jquery.prettyPhoto.js")%>" type="text/javascript"></script>
    <link href="<%:Url.Content("~/Content/css/prettyPhoto.css")%>" rel="stylesheet" />
    <script type="text/javascript">
        //jQuery.noConflict();
        jQuery(document).ready(function () {
            //jQuery('#slideshow').cycle({ fx: 'fade', timeout: 5000, speed: 1000 });
            jQuery('#slideshow .link').click(function () {
                var rel = jQuery(this).attr('rel');
                if (rel != null) {

                    if (jQuery(this).attr('target') == '_blank') {
                        window.open(rel, '_newtab');
                    }
                    else {
                        document.location.href = rel;
                    }
                }
            }).css('cursor', 'pointer');
            
            jQuery.fn.prettyPhoto({ social_tools: false, opacity: 0.6 });

            // Only show LiveMessage Popup if one is populated
             <%if(Model.ShouldShowLiveMessage) { %>

                jQuery.prettyPhoto.open('<%=Model.ActivePopup.PopupImagePath%>', '<%=Model.ActivePopup.PopupTitle%>', '<%=Model.ActivePopup.PopupText%>');
                jQuery('div.pp_fade').each(function () {
                    jQuery(this).click(function () {
                        location.href = '<%=Model.ActivePopup.TargetUrl%>';
                        });
                        this.style.cursor = 'pointer';
                });

                // Set the Description text color
                jQuery('p.pp_description').css('color', '#555');

                //Style the Close Text
                jQuery('a.pp_close').css('color', '#000000');
                jQuery('a.pp_close').css('font-size', 'x-small');
                jQuery('a.pp_close').css('right', '20px');
                jQuery('a.pp_close').css('text-indent', '0');


            <% } %>

        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h3>Sanctuary of Hope Checkout</h3>

    </div>
   <%-- <div class="home-banner">
        <div class="pics" id="slideshow">
            <div id="navHolder">
                <div id="nav">
                </div>
                <p id="banner_title">
                </p>
                <p id="banner_subtitle">
                </p>
            </div>
       <%--     <% foreach (var bannerItem in Model.HomeBannerItems)
               { %>
                <%= Html.BannerItemImage(bannerItem)%>
            <%  } %>
            --%>
          
        </div>
      <%--  <%= Html.ExternalHtmlFor(Model.HomeBannerItems) %>--%>
    </div>
</asp:Content>
