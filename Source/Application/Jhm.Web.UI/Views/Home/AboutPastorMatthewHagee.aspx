﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Pastor Matthew Hagee
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <img style="padding-bottom: 10px; padding-left: 10px; padding-right: 10px; float: right;
        padding-top: 10px" border="0" hspace="5" alt="Pastor Matthew Hagee" vspace="5" runat="server"
        src="~/Content/Images/PastorMatthewHagee2015.jpg" width="300" height="402" />
    <div>
        <div>
            <h1>
                Pastor Matthew Hagee</h1>
            <h2>
                Executive Pastor Cornerstone Church</h2>
        </div>
        <p>
            Pastor Matthew Charles Hagee is the sixth generation in the Hagee family to carry 
            the mantel of Gospel ministry. 
        </p>
        <p>
            He serves as the Executive Pastor of the 20,000 member Cornerstone Church in 
            San Antonio, Texas where he partners with his father, founder Pastor John Hagee. 
            Pastor Matthew Hagee’s teachings are telecast over world-wide television through 
            John Hagee Ministries. He is fervently committed to preaching all the Gospel to 
            all the world and to every generation.
        </p>
        <p>
            As a graduate of Oral Roberts University School of Business he is an accomplished 
            vocalist and is the author of “Shaken, Not Shattered” and recently released 
            "Response-Able" both published by Strang Communications.
        </p>
        <p>
            Pastor Matthew and his wife Kendal are blessed with four children, Hannah Rose, 
            John William, Joel Charles and Madison Katherine.
        </p>
        <p>
            As their family and ministry grow, Pastor Matthew and Kendal have a desire 
            equip the church of tomorrow to be the distinguished treasure God designed 
            them to be.  Together they seek to fulfill their divine destiny with the 
            passion and purpose that can only come from the power of family tradition 
            and the anointed call of God on their lives.<br>
        </p>
        <p>
            &nbsp;</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
