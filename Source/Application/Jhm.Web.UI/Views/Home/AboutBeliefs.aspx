﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Beliefs
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Beliefs</h1>
    <h6>
        The Lord Jesus Christ</h6>
    <p>
        &nbsp; We believe in the deity of Jesus Christ as the only begotten Son of God.&nbsp;
        We believe in His substitutionary death for all men, His resurrection, and His eventual
        return to judge the world.</p>
    <h6>
        Salvation</h6>
    <p>
        &nbsp; We believe all men are born with a sinful nature and that the work of the
        Cross was to redeem man from the power of sin.&nbsp; We believe that this salvation
        is available to all who will receive it.</p>
    <h6>
        The Holy Spirit<br>
    </h6>
    <p>
        We believe in the existence of the Holy Spirit as the third person of the Trinity
        and in His interaction with man.&nbsp; We believe in the baptism of the Holy Spirit
        as manifested by the fruit and the gifts of the Spirit.</p>
    <h6>
        The Sacred Scripture<br>
    </h6>
    <p>
        We believe in the scripture as the inspired Word of God and that it is the complete
        revelation of God's will for mankind.&nbsp; We believe in the absolute authority
        of the scripture to govern the affairs of men.</p>
    <h6>
        Stewardship<br>
    </h6>
    <p>
        We believe that every man is the steward of his life and resources which ultimately
        belong to God.&nbsp; We believe that tithing is a measure of obedience to the scriptural
        principles of stewardship.</p>
    <h6>
        The Church<br>
    </h6>
    <p>
        We believe in the Church as the eternal and universal Body of Christ consisting
        of all those who have accepted the work of the atonement. We believe in the need
        for a local assembly of believers for the purpose of evangelism and edification.</p>
    <h6>
        Prayer and Praise<br>
    </h6>
    <p>
        We believe in the worship of the Lord through singing, clapping, and the lifting
        of hands. We believe in the authority of the believer to ask freely of the Lord
        for his needs.</p>
    <h6>
        Body Ministry<br>
    </h6>
    <p>
        We believe in the ministry of the Holy Spirit to the Church body through the anointing
        of oil by the elders of the church.</p>
    <h6>
        Evangelism<br>
    </h6>
    <p>
        We believe that evangelism is the obligation of every follower of Jesus Christ.
        The Lord commands us to go out and make disciples of all the earth. We believe that
        each person is first responsible to evangelism in their own family as the Holy Spirit
        leads them and gives them the ability.</p>
    <h6>
        Water Baptism<br>
    </h6>
    <p>
        We believe in the ordinance of water baptism by immersion in obedience to the Word
        of God. All those who have accepted Jesus Christ as their personal savior should
        be baptized in water as a public profession of their faith in Christ and to experience
        what the Bible calls the "circumcision of the Spirit."</p>
    <h6>
        Our Commitment to Israel<br>
    </h6>
    <p>
        We believe in the promise of Genesis 12:3 regarding the Jewish people and the nation
        of Israel. We believe that this is an eternal covenant between God and the seed
        of Abraham to which God is faithful.</p>
    <h6>
        The Priesthood of the Believer<br>
    </h6>
    <p>
        We believe that every believer has a unique relationship to the Lord. As His children,
        every Christian has immediate access to the throne of Grace and the ability to manifest
        the power of the Lord Jesus Christ in ministry.
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
