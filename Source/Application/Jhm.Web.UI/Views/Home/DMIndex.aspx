﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.DMHomePageViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript" src="<%: Url.Content("~/Content/js/RotatingBanner.js") %>"></script>
<script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery.cycle.all.min.js") %>"></script>
<script type="text/javascript" src="<%: Url.Content("~/content/dm/scripts/index.js")%>"></script>


<link href="<%: Url.Content("~/content/dm/css/index.css")%>" media="screen" rel="stylesheet" type="text/css" />
<link href="<%: Url.Content("~/content/dm/css/customScrollBar.css")%>" media="screen" rel="stylesheet" type="text/css" />
<style type="text/css">
    #artistBoxLSlider ul li 
    {
        list-style: none;   
    }
</style>
<script type="text/javascript">

    //jQuery.noConflict();
    jQuery(document).ready(function () {
        //jQuery('#slideshow').cycle({ fx: 'fade', timeout: 5000, speed: 1000 });
        jQuery('#slideshow .link').click(function () {
            var rel = jQuery(this).attr('rel');
            if (rel != null) {

                if (jQuery(this).attr('target') == '_blank') {
                    window.open(rel, '_newtab');
                } else {
                    document.location.href = rel;
                }
            }
        }).css('cursor', 'pointer');
    });


    function replaceURLWithHTMLLinks(text) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, "<a target='_blank' href='$1'>$1</a>");
    }


    jQuery(document).ready(function () {
        jQuery('.socialItem').each(function () {
            var content = jQuery(this).html();
            jQuery(this).html(replaceURLWithHTMLLinks(content));
        });
    });
        
   
    
</script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <%:Html.Partial("PartialViews/DifferenceMedia/_HomeIndex", Model) %>
</asp:Content>