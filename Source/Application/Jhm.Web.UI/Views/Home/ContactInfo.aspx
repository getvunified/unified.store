﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Contact Info
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Contact Information</h2>
    <div class="block">
        <h2 class="title">
            <img id="usaFlag" alt="USA flag" src="<%: Url.Content("~/Content/images/usa.png") %>" />
            John Hagee Ministries - United States
        </h2>
        <div class="content">
            <table>
                <col width="28"/>
                <tr>
                    <td align="right">
                        <img alt="Phone" src="<%: Url.Content("~/Content/images/phone-icon-black.gif") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.PhoneNumber]%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="Mail" src="<%: Url.Content("~/Content/images/mail-icon-24.png") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                        <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine2] %>
                    </td>
                </tr>
            </table>
            <p>
                <%: Html.ActionLink("Go to Contact Us form","ContactUs") %>
            </p>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            <img alt="Canada flag" src="<%: Url.Content("~/Content/images/canada.png") %>" />
            John Hagee Ministries - Canada
        </h2>
        <div class="content">
            <table>
                <col width="28"/>
                <tr>
                    <td align="right">
                        <img alt="Phone" src="<%: Url.Content("~/Content/images/phone-icon-black.gif") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.PhoneNumber]%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="Mail" src="<%: Url.Content("~/Content/images/mail-icon-24.png") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                        <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine2] %>
                    </td>
                </tr>
            </table>
            <p>
                <%: Html.ActionLink("Go to Contact Us form","ContactUs") %>
            </p>
        </div>
    </div>
    <div class="block">
        <h2 class="title">
            <img alt="UK flag" src="<%: Url.Content("~/Content/images/uk.png") %>" />
            John Hagee Ministries - United Kingdom
        </h2>
        <div class="content">
            <table>
                <col width="28"/>
                <tr>
                    <td align="right">
                        <img alt="Phone" src="<%: Url.Content("~/Content/images/phone-icon-black.gif") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.PhoneNumber]%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="Mail" src="<%: Url.Content("~/Content/images/mail-icon-24.png") %>" />
                    </td>
                    <td>
                        <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                        <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine2] %>
                    </td>
                </tr>
            </table>
            <p>
                <%: Html.ActionLink("Go to Contact Us form","ContactUs") %>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
