﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Diana Hagee
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <img style="padding-bottom: 10px; padding-left: 10px; padding-right: 10px; float: right;
        padding-top: 10px" border="0" hspace="5" alt="Diana Hagee" vspace="5" runat="server"
        src="~/Content/Images/photo-diana.jpg" height="452" />
    <div>
        <div>
            <h1>
                Diana Castro Hagee</h1>
        </div>
        <p>
            <b>Diana Hagee</b> is the wife of Pastor John Hagee, founder and senior pastor of
            Cornerstone Church in San Antonio, Texas. She coordinates all special events for
            John Hagee Ministries, Cornerstone Church, Cornerstone Christian Schools, Christians
            United for Israel and leads the Women's Ministries at Cornerstone Church.
        </p>
        <p>
            Diana is the author of the best-selling book <i>The King's Daughter</i>, which was
            awarded the Retailers Choice Award, <i>The King’s Daughter Workbook</i>, and <i>Not
                by Bread Alone</i>, a cookbook encouraging creative ministry through food as
            well as a book she co-authored with her husband entitled, <i>What Every Man Wants in
                a Woman—What Every Woman Wants in a Man</i>. Her most recent release is <i>Ruth, the
                    Romance of Redemption – A Love Story</i>.
        </p>
        <p>
            She is founder and director of the King’s Daughter, Becoming a Woman of God National
            Conferences. She was presented the prestigious <i>Lion of Judah</i> award by the
            Jewish Federation of Greater Houston for the long-standing work she and Pastor Hagee
            do on behalf of Israel and the Jewish people. Diana is very active with Christians
            United for Israel, which Pastor Hagee founded in 2006.</p>
        <p>
            Diana holds a Bachelor’s Degree in Science from Trinity University in San Antonio,
            Texas. She and Pastor John Hagee have five children and twelve grandchildren.</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
