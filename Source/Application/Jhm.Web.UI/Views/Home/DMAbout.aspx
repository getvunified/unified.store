﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    About Us
</asp:Content>
<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content">
        <img src="<%: Url.Content("~/Content/dm/images/about_us.jpg") %>" width="380" class="imgRight">
        <h1>
            About Difference Media</h1>
            <br/>
        <p>
            John Hagee grew up in a pastor’s home that was constantly filled with music, especially the familiar sounds of his mother at the piano preparing for the next Sunday’s specials. John continued this tradition in his own home and his son, Matthew, remembers his father could often be found sitting at the family piano while his sisters learned harmonies. Sharing a deep love for music with all of his children, John Hagee and Matthew Hagee founded Difference Media with the belief that Christ can make a difference in the lives of many through the use of modern media. Comprised of a team of seasoned professionals with impressive credentials and some of the greatest skill sets in the industry bar none, Difference Media is home to artists such as John Hagee, Matthew Hagee, Canton Junction, The Hagees, Aaron & Amanda Crabb, Ricardo Sanchez, Eamonn McCrystal and the Cornerstone Sanctuary Choir. Difference Media will soon become a fan favorite of anyone who loves great music. The precision style and perfect execution of each note will make you want to keep their CDs on your top ten list. New projects and artists will soon be added to Difference Media’s roster so be sure to check our site often for updates and blogs welcoming new members.
<%--John Hagee grew up in a pastor’s home that was constantly filled with music, especially the familiar sounds of his mother at the piano preparing for the next Sunday’s specials. John continued this tradition in his own home and his son, Matthew, remembers his father could often be found sitting at the family piano while his sisters learned harmonies.  Sharing a deep love for music with all of his children, John Hagee and Matthew Hagee founded Difference Media with the belief that Christ can make a difference in the lives of many through the use of modern media. Comprised of a team of seasoned professionals with impressive credentials and some of the greatest skill sets in the industry bar none, Difference Media is home to artists such as Canton Junction, The Hagees, Aaron & Amanda Crabb and the Cornerstone Sanctuary Choir. Difference Media will soon become a fan favorite of anyone who loves great gospel music. The precision style and perfect execution of each note will make you want to keep their CDs on your top ten list. New projects and artists will soon be added to Difference Media’s roster so be sure to check our site often for updates, concert dates and blogs welcoming new members.--%> 
        </p>
    </div>
</asp:Content>
