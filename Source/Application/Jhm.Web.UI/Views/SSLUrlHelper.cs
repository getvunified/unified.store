﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Jhm.Web.UI.Views
{
    public static class SSLUrlHelper
    {
        public static string ConvertToSafeUrl(this UrlHelper helper,string url)
        {
            
            url = Regex.Replace(url, "http://|https://", string.Empty);

            return (helper == null || helper.RequestContext.HttpContext.Request.IsSecureConnection ? "https://" : "http://") + url;
        }
    }
}