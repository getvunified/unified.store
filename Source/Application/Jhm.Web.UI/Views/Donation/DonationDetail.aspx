﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.DonationFormViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Giving Opportunities -
    <%: Model.Title %>
</asp:Content>
<%--<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script src="<%: Url.Content("~/Content/js/jquery.prettyPhoto.js") %>" type="text/javascript" charset="utf-8"></script>

</asp:Content>--%>
<asp:Content ID="DonationFormPage" ContentPlaceHolderID="MainContent" runat="server">
    <% var formLink = Url.RouteUrl(MagicStringEliminator.Routes.Donation_Form.RouteName, new { donationCode = Model.DonationCode });
       var hasPdf = Model.PdfVersion.IsNot().NullOrEmpty();
    %>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title"><span style="color:#fff">MICAH WAS HERE!!!</span>
                <%: Model.Title %></h2>
            <div class="product-photo">
                <% if (Model.DonationCode == "SanctuaryOfHope")
                   { %>
                    <iframe width="573" height="322" src="https://www.youtube.com/embed/HWLoVqT0iVM" frameborder="0" allowfullscreen></iframe>
                <% }
                    else if (Model.DonationCode == "Salt_Covenant")
                   { %>
                    <iframe id="videoIframe" width="573" height="322" style="margin: 0; padding: 0; background: black url(/Content/images/loading-animation.gif) no-repeat center 50%"
                    scrolling="no" src="https://www.getv.org/Videos/EmbedWithSize/36bffd77-b1e4-4060-b7a4-0a6fd770b334?width=573&height=322"
                    frameborder="0" allowfullscreen="true"></iframe>
                <%--<script type="text/javascript">
                          var iframe = document.getElementById("videoIframe");

                          function restyle() {
                              var body = iframe.contentDocument.body;
                              body.style.padding = 0;
                              body.style.margin = 0;
                          }

                          iframe.onload = restyle;
                          restyle();
                      </script>--%>
                <% }
                   else if (Model.DetailViewImageUrl.IsNot().NullOrEmpty())
                   { %>
                <div>
                    <img alt="<%: Model.Title %>" src="<%: ResolveUrl(Model.DetailViewImageUrl) %>" />
                </div>
                <% } %>
                <center>
                    <div class="easybutton">
                        <% if (hasPdf)
                           { %>
                        <a class="buy first" href="<%: ResolveUrl(Model.PdfVersion)  %>"><span class="">PDF</span></a>
                        <% } %>
                        <a class="buy <%: "{0} last".FormatWith(hasPdf ? string.Empty : "first") %>" href="<%:formLink%>">
                            <span class="icon">
                                <%:  Model.DonationCode == "Salt_Covenant" ? "Become a partner or Donate Now" : "Donate Now" %></span></a>
                    </div>
                </center>
                <div class="content">
                    <div class="product-content">
                        <%= DonationOption.ReplaceDonationFormat(Model.Body,Client) %>
                        <%--<hr />
                        <div class="easybutton">
                            <% if (hasPdf)
                               { %>
                            <a class="buy first" href="<%: ResolveUrl(Model.PdfVersion)  %>"><span class="">PDF</span></a>
                            <% } %>
                            <a class="buy <%: "{0} last".FormatWith(hasPdf ? string.Empty : "first") %>" href="<%:formLink%>">
                                <span class="icon">Donate Now</span></a>
                        </div>--%>
                    </div>
                </div>
                <% if (Model.DonationCode == "Salt_Covenant")
                   { %>
                <center>
                    <div class="easybutton">
                        <% if (hasPdf)
                           { %>
                        <a class="buy first" href="<%: ResolveUrl(Model.PdfVersion)  %>"><span class="">PDF</span></a>
                        <% } %>
                        <a class="buy <%: "{0} last".FormatWith(hasPdf ? string.Empty : "first") %>" href="<%:formLink%>">
                            <span class="icon">Become a partner or Donate Now</span>
                        </a>
                    </div>
                </center>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Donation/Opportunities.ascx"); %>
</asp:Content>
