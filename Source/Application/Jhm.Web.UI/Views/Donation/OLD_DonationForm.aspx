﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.DonationFormViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Giving Opportunities -
    <%: Model.Title %>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".user-donation").focus(function () {
                if (this.value == '0.00') {
                    this.value = '';
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="DonationFormPage" ContentPlaceHolderID="MainContent" runat="server">
    <%  var detailLink = Url.RouteUrl(MagicStringEliminator.Routes.Donation_Detail.RouteName, new { donationCode = Model.DonationCode }); %>
    <div id="content-header">
        <h2 class="section-title">
            Giving Opportunities</h2>
    </div>
    <div class="region-content-bottom">
        <div class="block">
            <h2 class="title">
                <%: Model.Title %></h2>
            <% if (Model.FormViewImageUrl.IsNot().NullOrEmpty())
               { %>
            <div class="product-photo">
                <img alt="<%: Model.Title %>" src="<%: ResolveUrl(Model.FormViewImageUrl) %>" />
            </div>
            <% } %>
            <div class="content">
                <div class="node node-type-product node-teaser clearfix">
                    <div class="product-content">
                        <%= DonationOption.ReplaceDonationFormat(Model.Body, Client).TruncateAt(240, @"... <span class=""more-link""><a href=""{0}"">Read More</a></span>".FormatWith(detailLink))%>
                        <div class="product-content">
                            <br />
                            <br />
                            <%--  <h5>
                        Please fill out the form below:<a id="DonationForm"></a></h5>--%>
                            <% using (Html.BeginForm(new { returnToUrl = Request.RawUrl }))
                               {%>
                            <%:Html.ValidationSummary(false, "Adding to Cart was unsuccessful. Please correct the errors and try again.", new {@class = "messages error"})%>
                            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                            <%
                                int i = 1;
                                foreach (var option in Model.DonationOptions)
                                { %>
                            <div class="<%: i == 1 ? "donation-wrapper one" : "donation-wrapper auto" %>">
                                <div class="content">
                                    <% if (Model.DonationOptions.Count().Is().GreaterThan(1))
                                       { %>
                                        <input id="<%= DonationFormViewModel.SelectedChoiceName+option.SortOrder %>" name="<%= DonationFormViewModel.SelectedChoiceName %>" type="radio" value="<%= option.Id %>" tabindex="<%= option.SortOrder %>" <%= ViewData[DonationFormViewModel.SelectedChoiceName] != null && ViewData[DonationFormViewModel.SelectedChoiceName].ToString() == option.Id.ToString()? "checked" : String.Empty %> />
                                    <% }
                                       else
                                       { %>
                                    <%: Html.Hidden(DonationFormViewModel.SelectedChoiceName, option.Id)%>
                                    <%} %>
                                    <%= Html.RenderPartialEditByTypeName(option, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/")%>
                                </div>
                            </div>
                            <% i++;
                                } %>
                            <%-- <p>
                                    <hr />--%>
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton(MagicStringEliminator.EasyButtonLabel.AddToCart, icon: MagicStringEliminator.EasyButtonIcon.Buy))%>
                            <%--</p>--%>
                            <%
                               
                                }%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">    
   <%-- <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Donation/Opportunities.ascx"); %> --%>       
</asp:Content>
<%--<div class="block">
        <h2 class="title">
            Join our social networks</h2>
        <% Html.RenderPartial("PartialViews/RightMenu/SocialMedia/SocialMediaListing.ascx"); %>
    </div>
    <div id="search-form" class="block">
        <h2 class="title">
            Search All Products</h2>
        <% Html.RenderPartial("PartialViews/RightMenu/Catalog/SearchAllProducts.ascx"); %>
    </div>--%>