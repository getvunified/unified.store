﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.DonationIndexViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Giving Opportunities
</asp:Content>
<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="content-header">
        <h2 class="section-title">
            Sanctuary of Hope Giving Opportunities</h2>
    </div>   
   	
    <%
        var opportunities = Model.DonationOpportunities.ToLIST();
        for (int i = 0; i < opportunities.Count(); i++)
        {
            var opportunity = opportunities[i];
            var link = Url.RouteUrl(MagicStringEliminator.Routes.Donation_Detail.RouteName, new { donationCode = opportunity.DonationCode });
            var formLink = Url.RouteUrl(MagicStringEliminator.Routes.Donation_Form.RouteName, new { donationCode = opportunity.DonationCode });
    %>
    <div class="region-content-bottom">
        <div class="block">
            <h2 class="title">
                <%: opportunity.Title %></h2>
            <div class="content">
                <div class="node node-type-product node-teaser clearfix">
                    <% if (opportunity.ListViewImageUrl.IsNot().NullOrEmpty())
                       { %>
                        <div class="product-photo">
                        <img alt="<%: opportunity.Title %>" src="<%: ResolveUrl("{0}".FormatWith(opportunity.ListViewImageUrl)) %>" />
                    </div>
                    <% } %>
                    <div class="product-content">
                        <p>
                            <%= DonationOption.ReplaceDonationFormat(opportunity.Body, Client).TruncateToClosestPuntuationMarkAt(300, @"...</p> <div class=""more-link""><a href=""{0}"">Read More</a></div>".FormatWith(link))%>
                        </p>
                    </div>
                    <div class="easybutton">
                        <% if (opportunity.PdfVersion.IsNot().NullOrEmpty())
                           { %>
                        <a href="<%: ResolveUrl("{0}".FormatWith(opportunity.PdfVersion)) %>" target="_blank">
                            <span class="icon">PDF</span> </a>
                        <% } %>
                        <a class="buy last" href="<%:formLink %>"><span class="icon">Donate Now</span> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <% } %>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">    
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Donation/Opportunities.ascx"); %>        
</asp:Content>
