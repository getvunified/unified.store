﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Views
{
	public class CompanyCapableViewUserControl: ViewUserControl
	{
		public Company ClientCompany { get { return Client == null ? Company.JHM_UnitedStates : Client.Company; } }

		public Client Client { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]); } }
	}

	public class CompanyCapableViewUserControl<TModel> : ViewUserControl<TModel>
	{
		public Company ClientCompany { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]).Company; } }

		public Client Client { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]); } }

		protected string GetCurrencySymbol()
		{
			return ClientCompany.GetCurrencySymbol();
		}
	}

	public class LoggedInUserCapableControl<TModel>: ViewUserControl<TModel>
	{
		public IPrincipal CurrentUser { get { return Context.User; } }
	}
}