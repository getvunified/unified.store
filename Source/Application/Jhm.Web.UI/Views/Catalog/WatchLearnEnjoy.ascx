﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>

<div id="content-header">
</div>
<div id="content-area">
    <div class="store-banner">

          <% if (ClientCompany.Equals(Company.JHM_Canada) || ClientCompany.Equals(Company.JHM_UnitedKingdom))
               { %>
                    <a href='<%: Url.Action("JhmMagazine","Resources") %>'><img alt="View Digital Magazine" src="<%: Url.Content("~/Content/images/JHM-Magazine-ViewDigital-banner.jpg") %>" /></a>          
             <% 
               }
                else
              {    %>
                     <a href ="http://www.jhm.org/Donation/TheThreeHeavens/Form"><img alt="Read Listen Learn image" src="<%: Url.Content("~/Content/images/ThreeHeavensDonationOpp/3HShopBanner.jpg") %>" style="width:100%;" /></a>
                     <%--<img alt="Read Listen Learn image" src="<%: Url.Content("~/Content/images/JHM_Digital_Magazine_and_Catalog.jpg") %>" usemap="#imgmap2013123114139" />
                     <map id="imgmap2013123114139" name="#imgmap2013123114139">
                        <area shape="rect" alt="Digital Magazine" title="" coords="159,66,318,115" href="http://www.jhm.org/Resources/JhmMagazine" target="_self" />
                        <area shape="rect" alt="Circulation Subscription" title="" coords="341,66,504,114" href="http://www.jhm.org/Resources/JhmMagazineSubscription" target="_self" />
                    </map>--%> 
            <% } %>
       
    </div>

</div>
