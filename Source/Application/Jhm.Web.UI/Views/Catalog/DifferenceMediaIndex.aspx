﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.ProductListingViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Online Catalog
</asp:Content>
<asp:Content ID="pageHead" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        #bodyContentWrap #bodyContent .block 
        {
            padding: 15px;
            width: 97%;
        }
        
    </style>
</asp:Content>

<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("Store") %>
    <%: Html.Partial("PartialViews/DifferenceMedia/Store/_ProductListing", Model) %>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    
</asp:Content>
