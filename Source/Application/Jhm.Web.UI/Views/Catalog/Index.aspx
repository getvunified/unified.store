﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CatalogIndexViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Online Catalog
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <% Html.RenderPartial("~/Views/Catalog/WatchLearnEnjoy.ascx"); %>
    <div class="region-content-bottom">
        <%--<% if (Model.FeaturedItems.ToLIST().Count().Is().GreaterThan(0))
           {
               var featuredItem = Model.FeaturedItems.ToLIST()[new Random().Next(0, Model.FeaturedItems.Count())];%>
        <div class="block">
            <h2 class="title">
                Featured <span class="more-link"><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_SpecialCaseType.RouteName, new{type="Featured"})%>">
                    View All</a></span></h2>
            <div class="content">
                <div class="node node-type-product node-teaser clearfix">
                    <% if (featuredItem.AlternativeImages.IsNot().Empty())
                       { %>
                    <div class="product-photo">
                        <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = featuredItem.Code, productTitle= featuredItem.Title.ToSafeForUrl()}) %>">
                            <img alt="<%: featuredItem.Title %>" src="<%: Url.Content(featuredItem.AlternativeImages.ToArray()[0]) %>"
                                height="172" width="125" />
                        </a>
                    </div>
                    <% } %>
                    <div class="product-content">
                        <h1>
                            <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = featuredItem.Code, productTitle= featuredItem.Title.ToSafeForUrl()}) %>">
                                <%: featuredItem.Title %></a></h1>
                        <ul class="meta">
                            <li><strong>Product Number:</strong>
                                <%: featuredItem.Code %></li>
                            <li><strong>Section:</strong> Featured</li>
                            <li><strong>Category:</strong>
                                <%: featuredItem.Categories.ToStringJoin(",") %></li>
                            <li><strong>Subject(s):</strong>
                                <%: featuredItem.Subjects.ToStringJoin(",") %></li>
                        </ul>
                        <h2>
                            Product Description</h2>
                        <p>
                            <%= featuredItem.Description.TruncateAt(175, "...") %></p>
                        <div class="more-link">
                            <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = featuredItem.Code, productTitle= featuredItem.Title.ToSafeForUrl()}) %>">
                                Read More</a></div>
                    </div>--%>
                    <%--<%--<div class="easybutton">--%>
                    <%--TODO:  Add the ability to check if download or watch now is available--%>
                    <%-- <a class="watch first" href="#"><span class="icon">Watch</span></a> 
                        <a class="listen" href="#"><span class="icon">Listen</span></a> --%>
                    <%--<a class="buy last" href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = featuredItem.Code, productTitle = featuredItem.Title}) %>"><span class="icon">Buy</span></a>--%>
                    <%--<a class="easybutton buy right" href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = featuredItem.Code, productTitle = featuredItem.Title.ToSafeForUrl()}) %>">
                        <span class="icon">Buy</span></a>--%>
                    <%--<%--</div>--%>
                <%--</div>
            </div>
        </div>
        <%
            }%>--%>
        <%--<% if (Model.LatestReleaseItems().IsNot().Empty())
           {   %>
        <div class="block">
            <h2 class="title">
                Latest Releases <span class="more-link"><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_SpecialCaseType.RouteName, new{type="Latest Releases"})%>">
                    View All</a></span></h2>
            <div class="content">
                <div class="view view-product-list node-type-product clearfix">
                    <% Html.RenderPartial("~/Views/Catalog/ProductListView.ascx", Model.LatestReleaseItems()); %>
                </div>
            </div>
        </div>
        <%} %>--%>
         <% if (Model.FeaturedOnTelevisionItems.IsNot().Empty())
           {   %>
        <div class="block">
            <h2 class="title">
               Featured on Television <span class="more-link"><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_SpecialCaseType.RouteName, new{type="Featured on Television"})%>">
                    View All</a></span></h2>
            <div class="content">
                <div class="view view-product-list node-type-product clearfix">
                    <% Html.RenderPartial("~/Views/Catalog/ProductListView.ascx", Model.FeaturedOnTelevisionItems); %>
                </div>
            </div>
        </div>
        <%} %>
         <% if (Model.FeaturedOnGetvItems.IsNot().Empty())
           {   %>
        <div class="block">
            <h2 class="title">
                Featured on GETV <span class="more-link"><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_SpecialCaseType.RouteName, new{type="Featured on GETV"})%>">
                    View All</a></span></h2>
            <div class="content">
                <div class="view view-product-list node-type-product clearfix">
                    <% Html.RenderPartial("~/Views/Catalog/ProductListView.ascx", Model.FeaturedOnGetvItems); %>
                </div>
            </div>
        </div>
        <%} %>

    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/WholeRightMenu.ascx"); %>
</asp:Content>
