﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CatalogProductViewModel>" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Model.Title %>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <% const string changeMeString = "CHANGEME"; %>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var link1 = "<%= String.Format("javascript:alert('Sorry, this product is {0}.');",Model.AvailabilityAsString) %>";
            <% ViewData["disableAddToCartButton"] = true; 
                if ( Model.Availability != AvailabilityTypes.Out_of_Stock )
               {
                    ViewData["disableAddToCartButton"] = false; %>
                    link1 = "<%=Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName,new{sku = Model.Code,mediaType =Model.StockItems.FirstOrDefault().MediaType.CodeSuffix,returnToUrl = Request.RawUrl})%>";
            <% } %>
            SetProductLink(link1);
            $('select[name="<%=Model.Code %>"]').live('change', function (e) {

                var link = '#';
                <% if (Model != null && Model.Code != null){ %>
                    link = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = Model.Code, mediaType=changeMeString, returnToUrl = Request.RawUrl}) %>";
                <% } %>
                var selectedValue = e.target.options[e.target.selectedIndex].value;
                var newLink = link.replace("CHANGEME", selectedValue);
                SetProductLink(newLink);
            });

            function SetProductLink(Newlink) {
                $('a[name="<%=Model.Code %>Link"]').attr("href", Newlink);

            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("PartialViews/DifferenceMedia/Store/_ProductDetails",Model) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
