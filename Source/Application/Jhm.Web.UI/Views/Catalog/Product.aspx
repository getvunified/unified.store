﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CatalogProductViewModel>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    <%: Model.Title %>
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <% const string changeMeString = "CHANGEME"; %>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var link1 = "<%= String.Format("javascript:alert('Sorry, this product is {0}.');",Model.AvailabilityAsString) %>";
            <% ViewData["disableAddToCartButton"] = true; 
                if ( Model.Availability != AvailabilityTypes.Out_of_Stock )
               {
                    ViewData["disableAddToCartButton"] = false; %>
                    link1 = "<%=Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName,new{sku = Model.Code,mediaType =Model.StockItems.FirstOrDefault().MediaType.CodeSuffix,returnToUrl = Request.RawUrl})%>";
            <% } %>
            SetProductLink(link1);
            $('select[name="<%=Model.Code %>"]').live('change', function (e) {

                var link = '#';
                <% if (Model != null && Model.Code != null){ %>
                    link = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = Model.Code, mediaType=changeMeString, returnToUrl = Request.RawUrl}) %>";
                <% } %>
                var selectedValue = e.target.options[e.target.selectedIndex].value;
                var newLink = link.replace("CHANGEME", selectedValue);
                SetProductLink(newLink);
            });

            function SetProductLink(Newlink) {
                $('a[name="<%=Model.Code %>Link"]').attr("href", Newlink);

            }
            $(window).load(function () {
                var height = $(".view-product-gallery").height();
                $(".view-product-gallery .item").height(height + 20);
            });
            
        });
    </script>
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <div style="background: #ffffff;">
    <div id="content-header">
        <h2 class="title">
            <%: String.Format("{0} - {1}",Model.Title,Model.Code) %></h2>
    </div>
    <div id="content-area">
        <div class="node node-type-product clearfix">
            <div class="product-photo">
                <%
                    if (Model.AlternativeImages.IsNot().Empty())
                    { %>
                <img src="<%: Url.Content(Model.AlternativeImages.ToArray()[0])%>" height="220" alt="<%: Model.Title %>" />
                <%} %>
                <% if (Model.Trailer.IsNot().NullOrEmpty())
                   { %>
                <a class="easybutton watch" href="<%: Model.Trailer %>" title="<%: "{0} - Trailer".FormatWith(Model.Title) %>">
                    <span class="icon">Watch the Trailer</span> </a>
                <% } %>
            </div>
            <div class="product-content" style="padding-right:10px;">
                <h1>
                    <%: Model.Title %></h1>
                <ul class="meta">
                    <%--TODO: Link to Author Page--%>
                    <%= (!Model.AuthorDescription.IsNullOrEmpty()) ? "<li>By: {0}</li>".FormatWith(Model.AuthorDescription) : string.Empty%>
                    <li>
                        <%: Model.PublisherDescription%></li>
                </ul>
                <br/>
                <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style" style="display:inline" >
            <a class="addthis_button_facebook_like" ></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_counter addthis_pill_style" style="display:inline;"></a>
        </div>
        
        <%  
            bool showMP3Warning = false;
            foreach (var stockItem in Model.StockItems)
            {
                if (stockItem.MediaType.CodeSuffix.Contains("AF"))
                {
                    showMP3Warning = true;
                }
            }
                        
            if (showMP3Warning)
            { %> 

                <div>
                        <h5 style="color:grey;">* NOTE: MP3 downloads require a PC or a Mac</h5>
                </div>

           <% }
        %>
                
        
        <script type="text/javascript">var addthis_config = { "data_track_addressbar": false /* when set to true it adds funky characters to the url */ };</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e20ccc60077564"></script>
    <!-- AddThis Button END -->
                <div class="fivestar-form-item" style="display: none;">
                    <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/RatingHelper/StarDisplay.ascx", Model.OriginalProduct);%>
                    <div class="description">
                        <span>
                            <%: Model.Reviews.Count()%></span> reviews</div>
                </div>
                <hr />
                <a href="#" name="<%=Model.Code %>Link" class="<%: String.Format("easybutton buy right {0}",((bool)ViewData["disableAddToCartButton"])? "disabled": String.Empty) %>">
                    <span class="<%: String.Format("icon {0}",((bool)ViewData["disableAddToCartButton"])? "disabled": String.Empty) %>">
                        Add to Cart</span></a>
                <div class="select" style="<%: String.Format("display:{0}",((bool)ViewData["disableAddToCartButton"])? "none": String.Empty) %>">
                    <select class="mySelect" name="<%=Model.Code%>">
                        <%
                            foreach (var stockItem in Model.StockItems)
                            {
                                var isProductOnSalePrice = stockItem.IsOnSale();
                                var itemSalesPrice = stockItem.SalePrice;
                                var itemBasePrice = stockItem.BasePrice;
                                var formattedPrice = string.Empty;
                                var plainPrice = isProductOnSalePrice ? itemSalesPrice : itemBasePrice;
                                if (isProductOnSalePrice)
                                {
                                    formattedPrice = Model.FormatWithCompanyCulture(
                                    "{0} <span style='text-decoration: line-through; color: red'>{1:C}</span> {2:C}",
                                    stockItem.MediaType.DisplayName, itemBasePrice, itemSalesPrice);
                                }
                                else
                                {
                                    formattedPrice = Model.FormatWithCompanyCulture(
                                        "{0} {1:C}",
                                        stockItem.MediaType.DisplayName, itemBasePrice);
                                }
                               
                        %>
                        <option value="<%= stockItem.MediaType.CodeSuffix %>" formattedText="<%=formattedPrice%>">
                            <%
                               
                            %>
                            <%= Model.FormatWithCompanyCulture("{0} {1:C}", stockItem.MediaType.DisplayName, plainPrice)%>
                        </option>
                        <%
                            }
                        %>
                    </select>
                    <% if (Model.OnSale)
                       {%>
                    <div class="price-adjustments">
                        This Product Is On Sale!</div>
                    <%
                       }%>
                </div>
                <br />
                <ul class="meta">
                    <li><strong>Availability:</strong>
                        <%: Model.AvailabilityAsString %></li>
                    <li><strong>Stock Number:</strong>
                        <%: Model.Code %></li>
                </ul>
                <hr />
                <h2>
                    Product Description</h2>
                <p>
                    <%=Model.Description %>
                </p>
            </div>
        </div>
    </div>
    <div class="region-content-bottom">
        <% if (Model.RelatedItems.IsNot().Empty())  //NOTE:  Put this here to skip until async method for getting image etc is put in place
           {%>
        <div class="block">
            <h2 class="title">
                Related Items<%--<span class="more-link"><a href="#">View All</a></span>--%></h2>
            <div class="content">
                <div class="view view-product-gallery clearfix">
                    <% var relatedItems = Model.RelatedItems.Where(x => x.Image.IsNot().NullOrEmpty() && x.Title.IsNot().NullOrEmpty()).ToLIST();
                       for (var i = 0; i < relatedItems.Count(); i++)
                       {
                           if (i > 3) break;
                           var relatedItem = relatedItems[i];
                           var rowend = i == 3 ? "rowend" : string.Empty; %>
                    <div class="item <%: rowend %>">
                        <div class="product-photo">
                            <img width="110" src="<%: Url.Content(relatedItem.Image) %>" alt="<%: relatedItem.Title %>" />
                        </div>
                        <div class="product-title">
                            <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = relatedItem.Code, productTitle= relatedItem.Title.ToSafeForUrl()}) %>">
                                <%: relatedItem.Title %></a></div>
                        <div class="product_description">
                            <%= relatedItem.Description.TruncateAt(100,"...") %>
                        </div>
                        <a href="<%: Url.Action("Product", new {sku=relatedItem.Code}) %>" class="easybutton">View Details</a>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
        <%} %>
        <% if (Model.Reviews.IsNot().Empty())
           {%>
        <div class="block" style="display: none;">
            <h2 class="title" style="display: none;">
                Customer Reviews <span>(Average Rating:
                    <%: RatingHelper.GetRatingString(Model.Rating) %>
                    stars)</span> <span class="more-link"><a href="#">View All</a></span></h2>
            <div class="content">
                <div class="view view-product-reviews clearfix" >
                    <% foreach (var review in Model.Reviews)
                       { %>
                    <div class="item clearfix">
                        <div class="product-rating" >
                            <div class="review-date">
                                <%: review.Date.ToString("MMM dd, yyyy") %></div>
                            <div class="fivestar-form-item" style="display: none;">
                                <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/RatingHelper/StarDisplay.ascx", Model.OriginalProduct);%>
                            </div>
                        </div>
                        <div class="product-review" style="display: none;">
                            <h4>
                                Reviewed by
                                <%: review.ReviewedBy.Username %></h4>
                            <p>
                                <%= review.Body %></p>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
        <%} %>
    </div>
        </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <%
        Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/WholeRightMenu.ascx");%>
</asp:Content>
