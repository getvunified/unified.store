﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Jhm.Web.Core.Models.IProduct>>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>

<%    if (Session["MatchingDonations"] != null)
    {
        var foundDonations = Session["MatchingDonations"] as List<Donation>;
        if (foundDonations.Any())
        {
            %> 
              <hr/>
              <h4>Online Donations Matching this Search:</h4>
              <ul>
                  
              <%
                foreach (Donation d in foundDonations)
                {
                    string donationLink = String.Format("../../Donation/{0}", d.DonationCode);
                    int len = d.Description.Length;
                    if (len > 100) len = 100;
                    string donationTitle = String.Format(" <b>{0}</b> - {1}...", d.Title, d.Description.Substring(0, len));
                %>
                        <li> <a href="<%= donationLink %>"> <%= donationTitle %></a> </li> <br/> 
                <%
                }
                %> 

            </ul>
            <hr/>          
            <%
        }
        Session["MatchingDonations"] = null;
    }
%>

<%
    const string changeMeString = "CHANGEME";

    foreach (var product in Model)
    {
%>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var link1 = "<%= String.Format("javascript:alert('Sorry, this product is {0}.');",product.GetAvailabilityAsString()) %>";
        <%
            var disableAddToCartButton = product.GetAvailability() == AvailabilityTypes.Out_of_Stock;
            if ( !disableAddToCartButton )
            {
                //disableAddToCartButton = false; 
            %>
                link1 = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = product.Code, mediaType=product.StockItems.FirstOrDefault().MediaType.CodeSuffix, returnToUrl = Request.RawUrl}) %>";
        <%  } %>
        SetProductLink(link1);
        $('select[name="<%=product.Code %>"]').live('change', function (e) {
            var link = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = product.Code, mediaType=changeMeString, returnToUrl = Request.RawUrl}) %>";
            var selectedValue = e.target.options[e.target.selectedIndex].value;
            var newLink = link.replace("CHANGEME", selectedValue);
            SetProductLink(newLink);
        });

        function SetProductLink(Newlink) {
            $('a[name="<%=product.Code %>Link"]').attr("href", Newlink);

        }
    });
</script>
<div class="item clearfix">
    <div class="product-photo">
        <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = product.Code, productTitle= product.Title.ToSafeForUrl()}) %>">
            <% if (product.AlternativeImages.Any())
               {%>
            <img alt="<%:product.Title%>" src="<%:Url.Content(product.AlternativeImages.First())%>"
                height="120" />
            <%
               }
            %>
        </a>
    </div>
    <div class="product-content">
        <h4>
            <%= Html.ActionLink(product.Title ?? string.Empty, MagicStringEliminator.CatalogActions.Product, new { sku = product.Code, productTitle = product.Title.ToSafeForUrl() })%>
            <%--NOTE: Extension Method?--%>
        </h4>
        <ul class="meta">
            <% if (product.StockItems.Any(x => x.IsOnSale()))
               {%>
            <li class="price-adjustments"><strong>This Product Is On Sale!</strong></li>
            <%
               }%>
            <%--TODO: Link to Author Page--%>
            <%= (!product.AuthorDescription.IsNullOrEmpty()) ? "<li>By: {0}</li>".FormatWith(product.AuthorDescription) : string.Empty %>
            <li>
                <%: product.PublisherDescription%>&nbsp;</li>
            <%--Fix to Make sure the spacing is correct--%>
        </ul>
        <ul class="meta">
            <li><strong>Availability: </strong>
                <%: product.GetAvailabilityAsString() %></li>
            <li><strong>Product Number: </strong>
                <%: product.Code%></li>
        </ul>
    </div>
    <div class="product-data">
        <div class="select" style="<%: String.Format("display:{0}",disableAddToCartButton? "none": String.Empty) %>">
            <select class="mySelect" id="<%=product.Code %>" name="<%=product.Code %>">
                <%
var i = 1;
foreach (var stockItem in product.StockItems)
{
    var isProductOnSalePrice = stockItem.IsOnSale();
    var itemSalesPrice = stockItem.SalePrice;
    var itemBasePrice = stockItem.BasePrice;
    var formattedPrice = string.Empty;
    var plainPrice = isProductOnSalePrice? itemSalesPrice : itemBasePrice;
    if (isProductOnSalePrice)
    {
        formattedPrice = FormatHelper.FormatWithClientCulture(
            (Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],
            "{0} <span style='text-decoration: line-through; color: red'>{1:C}</span> {2:C}",
            stockItem.MediaType.DisplayName, itemBasePrice, itemSalesPrice);
    }
    else
    {
        formattedPrice = FormatHelper.FormatWithClientCulture(
             (Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "{0} {1:C}",
             stockItem.MediaType.DisplayName, itemBasePrice);
    }

    if (i == 1)
    {
                %>
                <option value="<%=stockItem.MediaType.CodeSuffix%>" selected="selected" formattedText="<%=formattedPrice%>">
                    <%
    }
    else
    {
                    %>
                    <option value="<%= stockItem.MediaType.CodeSuffix %>" formattedText="<%=formattedPrice%>"> 
                        <%
    }

   
                       
                        %>
                        
                        <%= FormatHelper.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "{0} {1:C}", stockItem.MediaType.DisplayName, plainPrice)%>
                    </option>
                    <%
i++;
}
                    %>
            </select>
        </div>
        <div class="fivestar-form-item" style="display: none;">
            <%= Html.RenderPartial(product, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/RatingHelper/StarDisplay.ascx")%>
            <div class="description">
                <span style="display: none;"><%: product.Reviews.Count()%></span> reviews</div>
        </div>
        <br />
    </div>
    <a href="#" class="<%: disableAddToCartButton? "easybutton buy disabled": "easybutton buy" %>"
        name="<%=product.Code %>Link"><span class="<%: disableAddToCartButton? "icon disabled": "icon" %>">
            Add to Cart</span></a>
</div>
<% }
    if (Model.Count().Equals(0))
    { %>
<h4>
    No products found</h4>
<% } %>
