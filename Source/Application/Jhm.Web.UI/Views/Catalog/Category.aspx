﻿<%@ Page Title="" ValidateRequest="false" Language="C#" MasterPageFile="~/Views/Shared/Site.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.ProductListingViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Online Catalog
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <%--<% Html.RenderPartial("~/Views/Catalog/WatchLearnEnjoy.ascx"); %>--%>
    <% Html.RenderPartial("~/Views/Catalog/ProductListing.ascx", Model); %>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/WholeRightMenu.ascx"); %>
</asp:Content>
