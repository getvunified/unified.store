using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.UI.Controllers;

namespace Jhm.Web.UI.Views
{
    public static class MVCExtensions_EasyButton
    {
        public static MvcHtmlString FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript(this HtmlHelper helper)
        {
            return FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript(helper, String.Empty);
        }

        public static MvcHtmlString FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript(this HtmlHelper helper, string hiddenFieldName)
        {
            return MvcHtmlString.Create(@"<script type=""text/javascript"">
                            function EasyButtonSubmitForm"+hiddenFieldName+@"(hiddenFieldName, hiddenFieldValue) {
                                    var hiddenFieldName = '#'+hiddenFieldName;
                                    $(hiddenFieldName).val(hiddenFieldValue);
                                    $(hiddenFieldName).closest('form').submit();
                            }
                            $(document).ready(function() {
                                if ( $('.easyButtonDialogModal').length > 0 ){
                                    $('.easyButtonDialogModal').dialog({
				                        autoOpen: false,
				                        bgiframe: true,
                                        modal: true,
                                        resizable: false,
                                        width: 350,
                                        title: 'Temporary System Message'
                                    });
                                }
                            });
                        </script>");
        }

        public static MvcHtmlString FormKeyValueMatchAttribute_EasyButtonLink(this HtmlHelper helper, EasyButton easyButton, string hiddenFieldName = FormKeyValueMatchAttributes.ActionBottonClicked, string onClickValidationFunction = null)
        {
            var spanClass = easyButton.Icon.IsNot().NullOrEmpty() ? "icon" : string.Empty;
            var iconName = easyButton.Icon.IsNot().NullOrEmpty() ? easyButton.Icon : string.Empty;

            TagBuilder tag = new TagBuilder("a");
            if (easyButton.HtmlAttributes != null)
            {
                tag.MergeAttributes(new RouteValueDictionary(easyButton.HtmlAttributes));
            }
            
            tag.Attributes.Add("class", "easybutton {0}".FormatWith(iconName));
            tag.InnerHtml = @"<span class=""{0}"">{1}</span>".FormatWith(spanClass, easyButton.Text);
            string easyButtonDialogModal = string.Empty;
            if ( easyButton.DisplayTemporaryMessage )
            {
                tag.Attributes.Add("href", "javascript:void(0);");
                if (onClickValidationFunction.IsNullOrEmpty())
                {
                    tag.Attributes.Add("OnClick", "$('#easyButtonDialogModal').dialog('option','title','{0}'); $('#easyButtonDialogModal').dialog('open'); EasyButtonSubmitForm{3}('{1}','{2}'); return true;".FormatWith(easyButton.TemporaryMessageText, hiddenFieldName, easyButton.HiddenFieldValue, hiddenFieldName == FormKeyValueMatchAttributes.ActionBottonClicked ? String.Empty : hiddenFieldName));
                }
                else
                {
                    tag.Attributes.Add("OnClick", "if ( {4} ) {{ $('#easyButtonDialogModal').dialog('option','title','{0}');$('#easyButtonDialogModal').dialog('open'); EasyButtonSubmitForm{3}('{1}','{2}'); return true; }} return false;".FormatWith(easyButton.TemporaryMessageText, hiddenFieldName, easyButton.HiddenFieldValue, hiddenFieldName == FormKeyValueMatchAttributes.ActionBottonClicked ? String.Empty : hiddenFieldName, onClickValidationFunction));
                }
                easyButtonDialogModal = string.Format("<div id='easyButtonDialogModal' class='easyButtonDialogModal'>{0} please wait. The system is currently processing your request.</div>", easyButton.TemporaryMessageText);
            }
            else
            {
                tag.Attributes.Add("href", "javascript:EasyButtonSubmitForm{0}('{1}','{2}');".FormatWith(hiddenFieldName == FormKeyValueMatchAttributes.ActionBottonClicked? String.Empty : hiddenFieldName ,hiddenFieldName, easyButton.HiddenFieldValue));
            }

            return MvcHtmlString.Create(tag.ToString() + easyButtonDialogModal);
        }

        public static MvcHtmlString FormKeyValueMatchAttribute_EasyButtonDiv(this HtmlHelper helper, IList<EasyButton> easyButtons,  string hiddenFieldName = FormKeyValueMatchAttributes.ActionBottonClicked)
        {
            const string divContainer = @"<div class=""easybutton"">{0}</div>";
            var linksHtml = new StringBuilder();

            for (int i = 0; i < easyButtons.Count(); i++)
            {
                var easyButton = easyButtons[i];
                
                var spanClass = easyButton.Icon.IsNot().NullOrEmpty() ? "icon" : string.Empty;
                var iconName = easyButton.Icon.IsNot().NullOrEmpty() ? easyButton.Icon : string.Empty;
                var firstOrLast = i == 0 ? "first" : i + 1 == easyButtons.Count() ? "last" : string.Empty;

                TagBuilder tag = new TagBuilder("a");
                if (easyButton.HtmlAttributes != null)
                {
                    tag.MergeAttributes(new RouteValueDictionary(easyButton.HtmlAttributes));
                }
                tag.InnerHtml = @"<span class=""{0}"">{1}</span>".FormatWith(spanClass, easyButton.Text);
                tag.Attributes.Add("href","javascript:EasyButtonSubmitForm('{0}','{1}');".FormatWith(hiddenFieldName, easyButton.HiddenFieldValue));
                var cssClass = "{0} {1}".FormatWith(iconName, firstOrLast);
                tag.AddCssClass(cssClass);

                string easyButtonDialogModal = string.Empty;
                if (easyButton.DisplayTemporaryMessage)
                {
                    tag.Attributes.Add("OnClick", "$('#easyButtonDialogModal{0}').dialog('option','title','{1}');$('#easyButtonDialogModal{0}').dialog('open'); return true;".FormatWith(i,easyButton.TemporaryMessageText));
                    easyButtonDialogModal = string.Format("<div id='easyButtonDialogModal{0}' class='easyButtonDialogModal'>{1} please wait. The system is currently processing your request.</div>",i, easyButton.TemporaryMessageText);
                }

                linksHtml.AppendLine(tag.ToString());
                linksHtml.AppendLine(easyButtonDialogModal);
            }

            return MvcHtmlString.Create(divContainer.FormatWith(linksHtml));
        }
    }

    public class EasyButton
    {
        public EasyButton(string buttonText, string hiddenFieldValue = null, string icon = null)
            : this(buttonText, hiddenFieldValue, icon, null)
        {
        }

        public EasyButton(string buttonText, string hiddenFieldValue, string icon, object htmlAttributes)
            : this(buttonText, false, null, hiddenFieldValue, icon, htmlAttributes)
        {
        }

        public EasyButton(string buttonText, bool displayTemporaryMessage, string temporaryMessageText)
            : this(buttonText, displayTemporaryMessage, temporaryMessageText, null,null,null)
        {}

        public EasyButton(string buttonText, bool displayTemporaryMessage, string temporaryMessageText = null, string hiddenFieldValue = null, string icon = null, object htmlAttributes = null)
        {
            HiddenFieldValue = hiddenFieldValue ?? string.Empty;
            Text = buttonText;
            Icon = icon;
            HtmlAttributes = htmlAttributes;
            TemporaryMessageText = temporaryMessageText ?? buttonText;
            DisplayTemporaryMessage = displayTemporaryMessage;
        }

        public string HiddenFieldValue { get; private set; }
        public string Text { get; private set; }
        public string Icon { get; private set; }
        public string TemporaryMessageText { get; private set; }
        public object HtmlAttributes { get; private set; }
        public bool DisplayTemporaryMessage { get; private set; }
    }
}