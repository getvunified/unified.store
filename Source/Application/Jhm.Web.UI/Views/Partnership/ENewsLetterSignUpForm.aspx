﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
         Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.ENewsLetterSignUpViewModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    E-Newsletter Sign-up
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="section-checkout">
        <div class="block">
            <h2 class="title">
                E-Newsletter Sign-up form</h2>
            <div class="content">
                <% using (Html.BeginForm())
                   {%>
                    <%: Html.ValidationSummary(false,
                                              "Registration was unsuccessful. Please correct the errors and try again.",
                                              new {@class = "messages error"})%>
                    <%: Html.FormKeyValueMatchAttributeHiddenField() %>
                    <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
                   
                    <h3 style="color:red"><%: ViewBag.ErrorMessage %></h3> 
                    <h5>* Denotes Required Fields</h5>
                    <table>
						<tr>
							<td colspan="2">
								<h6>Please Select Your Subsciptions:</h6>
							</td>
						</tr>
						<% foreach (var email in Model.EmailLists)
						 { %>
							<tr>
								<td colspan="2">
									<input type="checkbox" name="SelectedEmailLists" value="<%:email.Code %>" id="cb<%:email.Code %>" /> <label for="cb<%:email.Code %>"><%:email.Title %></label>
								</td>
							</tr>
						<% } %>
                        <tr>
							<td colspan="2">
								<h6>Please Enter Your Information:</h6>
							</td>
						</tr>
						<tr>
                            <td style="width: 80px;">
                                <%: Html.LabelFor(model => model.FirstName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.FirstName) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.FirstName) %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(model => model.LastName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.LastName) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.LastName) %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(model => model.Email) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.Email) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.Email) %></span>
                            </td>
                        </tr>
                    </table>
                    <p>
                        <hr />
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Register"))%>
                    </p>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
