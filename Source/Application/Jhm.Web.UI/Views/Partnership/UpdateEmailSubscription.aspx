﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" 
         Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.ENewsLetterSignUpViewModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    E-Newsletter Sign-up
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script language="Javascript">

        var toggleeverything = function () {           
            var uncheckme = document.getElementsByName("SelectedEmailLists");
            $(uncheckme).each(function() {
                this.checked = false;
            });

            EasyButtonSubmitForm('ActionBottonClicked', '');
            
        };
    </script>
    <div id="section-checkout">
        <div class="block">
            <h2 class="title">
                E-Newsletter Update Form</h2>
            <div class="content">
                <% using (Html.BeginForm())
                   {%>
                    <%: Html.ValidationSummary(false,
                                              "Registration was unsuccessful. Please correct the errors and try again.",
                                              new {@class = "messages error"})%>
                    <%: Html.FormKeyValueMatchAttributeHiddenField() %>
                    <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
                        
                    <h3 style="color:red"><%: ViewBag.ErrorMessage %></h3> 
                    <h5>* Denotes Required Fields</h5>
                    <table>
						<tr>
							<td colspan="2">
								<h4>Your Current Email Subscriptions:</h4>
                                <p>Please check or uncheck subscriptions below.</p>
                              </td>
						</tr>
                        
						<% foreach (var listCode in Model.EmailLists)
						 { %>
							<tr>
								<td colspan="2">
									<input type="checkbox" name="SelectedEmailLists" value="<%:listCode.Code %>" id="cb<%:listCode.Code %>" <%= Model.SelectedEmailLists.Contains(listCode.Code)? "checked" : string.Empty %> /> <label for="cb<%:listCode.Code %>"><%:listCode.Title %></label>
									<% if ( !string.IsNullOrEmpty(listCode.Description) && listCode.Description != listCode.Title) { %>
									<a href="javasrcipt:void(0);" class="tooltip">
										<img src="<%: Url.Content("~/Content/images/Icons/help-icon16.jpg") %>" />
										<span>
											<img class="callout" src="<%: Url.Content("~/Content/images/Icons/callout.gif") %>" />
											<strong><%: listCode.Title %></strong><br />
											<%: listCode.Description %>
										</span>
									</a>
									<% } %>
								</td>
							</tr>
						<% } %>
                        <tr>
							<td colspan="2">
								<h6>Please Update your Subscriber Information:</h6>
							</td>
						</tr>
						<tr>
                            <td style="width: 80px;">
                                <%: Html.LabelFor(model => model.FirstName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.FirstName) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.FirstName) %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%: Html.LabelFor(model => model.LastName) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.LastName) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.LastName) %></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <%: Html.LabelFor(model => model.Email) %>
                            </td>
                            <td>
                                <%: Html.TextBoxFor(model => model.Email) %>
                                <span style="color: #900;">*<%: Html.ValidationMessageFor(model => model.Email) %></span>
                            </td>
                        </tr>
                        
                    </table>
                    <p>
                        <hr />
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Update"))%>
                        <h6 align="right"><a href="#" style="text-decoration: underline" onclick="toggleeverything()">Click HERE to UNSUBSCRIBE from ALL email lists</a></h6>

                    </p>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
		a.tooltip {outline:none; }
		a.tooltip strong {line-height:30px;}
		a.tooltip:hover {text-decoration:none;} 
		a.tooltip span {
			z-index:10;display:none; padding:14px 20px;
			margin-top:-30px; margin-left:28px;
			width:240px; line-height:16px;
		}
		a.tooltip:hover span{
			display:inline; position:absolute; color:#111;
			border:1px solid #DCA; background:#fffAF0;}
		.callout {z-index:20;position:absolute;top:30px;border:0;left:-12px;}
    
		/*CSS3 extras*/
		a.tooltip span
		{
			border-radius:4px;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
        
			-moz-box-shadow: 5px 5px 8px #CCC;
			-webkit-box-shadow: 5px 5px 8px #CCC;
			box-shadow: 5px 5px 8px #CCC;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
