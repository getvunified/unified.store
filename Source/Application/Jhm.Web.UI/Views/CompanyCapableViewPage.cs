﻿using System.Web.Mvc;
using Jhm.Web.Core;
using Jhm.Web.Core.Models.Modules;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Views
{
    public class CompanyCapableViewPage : ViewPage
    {
        protected Company ClientCompany { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]).Company; } }

        protected Client Client { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]); } }
    }

    public class CompanyCapableViewPage<TModel> : ViewPage<TModel>
    {
        protected Company ClientCompany { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]).Company; } }

        protected Client Client { get { return ((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]); } }

        protected string GetCurrencySymbol()
        {
            return ClientCompany.GetCurrencySymbol();
        }
    }
}