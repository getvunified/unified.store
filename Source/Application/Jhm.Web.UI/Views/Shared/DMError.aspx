﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<HandleErrorInfo>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<asp:Content ID="errorTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Error
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(window).keypress(function (event) {
                if (!(event.ctrlKey && event.shiftKey && (event.which == 17 || event.which == 81 || event.which == 23)) && !(event.which == 19)) return true;
                jQuery('#err').show();
                event.preventDefault();
                return false;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
     <%: Html.DifferenceMediaTitleHeader("Error") %>
     <br/>
    <h1>
        Sorry, an error occurred while processing your request.
    </h1>
    <br/>
    <br/>
    <div id="err" style="display:none">
        <%if (Model != null)
        { %>
            <div class="dividerline"></div>
            <h4><%:Model.Exception.Message%></h4>
            <p>
                <%:Model.Exception.StackTrace%>
            </p>
            <%if (Model.Exception.InnerException != null)
            {%>
                <div class="dividerline"></div>
                <h4><%:Model.Exception.InnerException.Message%></h4>
                <p>
                    <%:Model.Exception.InnerException.StackTrace%>
                </p>
            <%}
        }%>
    </div>
</asp:Content>
