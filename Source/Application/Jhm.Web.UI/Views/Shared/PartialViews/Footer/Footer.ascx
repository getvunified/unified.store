﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl" %>
<%@ Import Namespace="Jhm.Web" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<div class="section clearfix">
    <div class="region-footer clearfix">
        <div class="block">
            <a href="<%=Url.Action("Index", "Catalog")%>">
                <img id="Img1" runat="server" src="~/Content/images/footer-link-bookstore.jpg" />
                <h6>Shop JHM</h6>
            </a>
        </div>
        <div class="block">
            <a href='<%: Url.Action("UpcomingEvents", "Resources") %>'>
                <img id="Img2" src='<%: Url.Content("~/Content/images/footer-link-events.jpg") %>' />
                <h6>News &amp; Events</h6>
            </a>
        </div>
        <div class="block">
            <a href='<%: Url.Action("PrayerRequest","Resources") %>'>
                <img id="Img3" src='<%: Url.Content("~/Content/images/footer-link-prayer.jpg") %>' />
                <h6>Prayer Request</h6>
            </a>
        </div>
        <div class="block">
            <a id="A1" href='<%: Url.Action("Index", "Donation") %>'>
                <img id="Img4" src='<%: Url.Content("~/Content/images/footer-link-donations.png") %>' />
                <h6>Online Donations</h6>
            </a>
        </div>
        <div class="block">
            <% if (ClientCompany.Equals(Company.JHM_Canada) || ClientCompany.Equals(Company.JHM_UnitedKingdom))
               { %>
                    <a href='<%: Url.Action("JhmMagazine","Resources") %>'>
                        <img src='<%: Url.Content("~/Content/images/JHM_Magazine_ViewDigital.jpg") %>' width='157px' height='90px'>
                        <h6>View Digital Magazine</h6>
                    </a>                    
              <% } 
               else 
               { %>
                <a href='<%: Url.Action("JhmMagazineSubscription","Resources") %>'>
                    <img src='<%: Url.Content("~/Content/images/JHM_Magazine_SocialNet.jpg") %>' width='157px' height='90px'>
	      		    <h6>Magazine Subscription</h6>
      		    </a>     
              <% } %>
      	</div>

        <div class="clear-block"></div>
    </div>
    <div id="footer-message">
        <p class="right">
            <%--<a href="#">SITE MAP</a>--%>
        </p>
        <p style="text-align:center;">
            <% var emailAddress = (ClientCompany.Equals(Company.JHM_UnitedKingdom)) ? "uksupport@jhm.org" : "support@jhm.org"; %>
            <a href="mailto:<%: emailAddress %>" > <%: emailAddress %> </a> | <%: ClientCompany.AdditionalData[Company.DataKeys.FullAddress] %> | <%: Html.ActionLink("Terms & Privacy Policy", "PrivacyPolicy", "Resources")%> 
            <% if (ApplicationManager.ApplicationData.JobPostings.Any())
               { %>
                    | <%: Html.ActionLink("Careers", "Careers", "Resources") %>
            <% } %>
            <br/>
            Copyright &copy; 2000-<%: DateTime.Now.Year %> John Hagee Ministries. All rights reserved. <%: ClientCompany.AdditionalData[Company.DataKeys.PhoneNumber] %>
         </p>
    </div>
</div>
