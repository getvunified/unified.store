﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Artist>" %>
 <script type="text/javascript" src="<%: Url.Content("~/Content/js/flowplayer-3.2.11.min.js") %>"></script>
 <script src="<%: Url.Content("~/Content/js/youtube_callplayer.js") %>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#videoPlayer').hide();
        var flowplayerIsLoaded = document.getElementById("musicPlayer") != null;
        if (flowplayerIsLoaded) {
            flowplayer("musicPlayer", '<%: Url.Content("~/Content/media/flash/flowplayer.commercial-3.2.11.swf") %>', {
                plugins: {
                    controls: {
                        fullscreen: false
                    },
                    audio: {
                        url: '<%: Url.Content("~/Content/media/flash/flowplayer.audio-3.2.10.swf") %>'
                    }
                },
                onBeforeClick: function () {
                    var url = $('#musicPlayer img').attr('src');
                    alert(url);
                    $f().onLoad(function () {
                        $f().getPlugin("canvas").css({ background: 'url(' + url + ') no-repeat 0 0' });
                    });
                    return true;
                }
            });
        }
        $('.music').click(function () {
            if ($(this).hasClass('active')) {
                return;
            }
            $('#videoPlayer').hide();
            callPlayer('videoPlayerWraper', 'stopVideo');
            $('#musicPlayer').show();
            $('#songList a').removeClass('active');
            var player = this;
            $(player).addClass('active');

            if (!$f().isLoaded()) {
                $f().onLoad(function () {
                    $f().getPlugin("canvas").css({ background: 'url(' + player.getAttribute("cover") + ') no-repeat 0 0' });
                });
                $f().load();
            }

            $f().play(player.getAttribute("url"));
            var clip = $f().getClip();
            if (clip) {
                clip.onBegin(function () {
                    $f().getPlugin("canvas").css({ background: 'url(' + player.getAttribute("cover") + ') no-repeat 0 0' });
                });
            }
        });
        $('.movie').click(function () {
            if ($(this).hasClass('active')) {
                return;
            }
            $('#songList a').removeClass('active');
            var player = this;
            $(player).addClass('active');
            if (flowplayerIsLoaded && $f().isLoaded()) {
                $f().stop();
            }
            $('#musicPlayer').hide();
            $('#videoPlayer').show();
            $('#videoPlayer').attr("src", "http://www.youtube.com/embed/" + player.getAttribute("mediaid") + "?autoplay=1&rel=0&enablejsapi=1");
        });
    });
</script>
<%--<%if (Model.Music.Any())
  {%>--%>

<div id="musicPlayer" style="width:446px; height:300px;">
    <img src="<%: Model.ProfileThumbnailUrl %>" alt="<%: Model.Name %>" width="446" height="296"/>
</div>

<%--<% }
  if (Model.Videos.Any())--%>
 <%-- {%>--%>
    <div id="videoPlayerWraper">
  <%--      <img src="<%: Model.ProfileThumbnailUrl %>" alt="<%: Model.Name %>" width="446" height="296"/>--%>
        <iframe id="videoPlayer" width="446" height="300" frameborder="0" allowfullscreen></iframe>
    </div>
<%--  <%} %>--%>



