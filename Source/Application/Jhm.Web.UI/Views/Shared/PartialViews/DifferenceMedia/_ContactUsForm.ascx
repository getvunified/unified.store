﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.ContactInfoDifferenceMediaViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script src="<%: Url.Content("~/Content/dm/scripts/contact.js?version=1")%>" type="text/javascript"></script>
<link href="<%: Url.Content("~/Content/dm/css/contact.css?version=1")%>" media="screen" rel="stylesheet" type="text/css" />

<title>Difference Media</title>
<!-- START Assets for Contact page -->


<%: Html.DifferenceMediaTitleHeader("Contact Us") %>

<div id="right">
    <h1>Send Us a Message</h1>
    <%
        using (
            Html.BeginForm("ContactUsDifferenceMedia", "Home", FormMethod.Post, new {id = "ContactUsDifferenceMedia"})
            )
        {%>
        <%--<form id="form1" name="form1" method="post" action="">--%>
        <p>
            <%: Html.LabelFor(model => model.Name) %>
            <%: Html.TextBoxFor(model => model.Name) %>
        </p>
        <p>
            <%: Html.LabelFor(model => model.Phone) %>
            <%: Html.TextBoxFor(model => model.Phone, new {maxlength = 20}) %>
        </p>
        <p>
            <%: Html.LabelFor(model => model.Email) %>
            <%: Html.TextBoxFor(model => model.Email) %>
        </p>
        <p>
            <%: Html.LabelFor(model => model.Message) %>
            <%: Html.TextAreaFor(model => model.Message,
                                          new {cols = 20, rows = 10, style = "resize: none;"}) %>
        </p>
        <p>&nbsp;</p>
        <p>
            <input type="submit" name="send" id="send" value="Send Message" />
        </p>
    <% } %>
    <%--</form>--%>
</div>
	
<div id="left">
    <h1>Contact Difference Media</h1>
    <p>&nbsp;</p>
    <p><strong>Email</strong></p>
    <ul>
        <li>
            <p>
                For merchant / retailer product discounts, booking requests, general information or tour dates, please email <a href="mailto:info@differencemedia.org">info@differencemedia.org</a>
            </p>      
        </li>
        <li>
            <p>
                To send correspondence to your favorite Difference Media artist, email <a href="mailto:fanmail@differencemedia.org">fanmail@differencemedia.org</a>
            </p>
        </li>
        <li>
            <p>
                For official statements, photos and information for an article or news piece or to be added to the reviewer’s mailing list send an email to <a href="mailto:press@differencemedia.org">press@differencemedia.org</a>
            </p>
        </li>
        <li>
            <p>
                DJ’s and On-Air personalities only: to have your station added to our mailing list for the latest and greatest singles and releases, email <a href="mailto:radio@differencemedia.org">radio@differencemedia.org</a>
            </p>
        </li>
    </ul>
    
    <p>&nbsp;</p>
    <p><strong>Address</strong></p>
    <p style="text-indent: 20px;">P.O. Box 1400, San Antonio, Texas 78295</p>
    <p>&nbsp;</p>
    <p><strong>Legal</strong></p>
    <p style="padding-bottom: 15px; text-indent: 20px;">
        <a id="inline"  href="#data">Legal Document</a>
    </p>
</div>

<div style="display: none;">
    <div id="data" style="text-indent: 20px; width: 600px; height: 400px; padding-left: 10px; padding-right: 10px;">
        <h1>Legal Statement</h1>
        <br/>
        <p>
            At times where Difference Media has specifically invited or requested submissions, we encourage you to submit content created for our consideration ("User Submissions"). When requested or invited, User Submissions remain the intellectual property of the individual user. By submitting these User Submissions, you expressly grant Difference Media, and its agents and subsidiaries, a non-exclusive, perpetual, irrevocable, royalty-free, worldwide, fully sub-licensable right to use, reproduce, modify, publish, translate, create derivative works from, distribute, transmit, perform and display such content, as well as your name, voice, and/or likeness, in whole or in part, and in any form throughout the world in any media or technology, whether now known or hereafter discovered, including all forms of promotion, advertising, marketing, merchandising, publicity and any other ancillary uses thereof, and including the unlimited right to sublicense such rights, in perpetuity throughout the universe. Such User Submissions are deemed non-confidential, and Difference Media shall be under no obligation to maintain the confidentiality of any information, in whatever form, contained in any User Submission.
        </p>
        <br/>
        <p>
            We do not encourage or seek User Submissions that result from any activity that may create a risk of harm, loss, injury, emotional distress, damage, or disability to any person; or may constitute a crime or tort. You warrant that you have not engaged in any of the foregoing activities in connection with producing your submission. Without limiting the foregoing, you agree that in conjunction with your submission, you will not inflict emotional distress, humiliate, assault or threaten other people, will not enter onto private property without permission, and will not otherwise engage in any activity that may result in injury, death, property damage, and/or liability of any kind. Difference Media may, at its sole discretion, reject any submissions. If notified of a submission that allegedly violates any provision of these terms of use, Difference Media reserves the right to determine, in its sole discretion, if such a violation has occurred, and, at its discretion, destroy any such submission.
        </p>
        <br/>
        <p>
            Except where specifically requested, Difference Media does not accept or consider ideas, suggestions, or materials. This is designed to avoid misunderstandings if content developed by Difference Media's professional staff seem, in the opinion of others, to be similar to their own creative work. Accordingly, Difference Media requests that your comments relate to services and products currently offered by Difference Media, and that you not submit any ideas, suggestions, or materials except where specifically requested or solicited. If you do send us an unsolicited submission regardless, you agree not to assert any ownership right of any kind in the unsolicited submission (including, but not limited to copyright, trademark, unfair competition, moral rights, or implied contract), you hereby grant Difference Media a nonexclusive, perpetual, worldwide license to the unsolicited submission in every media and for every purpose now known or hereinafter discovered and you waive the right to receive any financial or other form of consideration in connection with such unsolicited submission including, but not limited to, credit. You release Difference Media (and our agents, subsidiaries and employees) from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of, or in any way connected with, your unsolicited submissions, including without limitation all claims for theft of ideas or copyright infringement.
        </p>
    </div>
</div>
    
<div style="clear: both;"></div>




