﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.UI.Models.DMHomePageViewModel>" %>
<script type="text/javascript">
    $(function () {
        $("#mcs_container").mCustomScrollbar("vertical", 400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
        $("#mcs_container2").mCustomScrollbar("vertical", 400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);


        $(window).load(function () {
            $("#mcs_container").mCustomScrollbar("vertical", 400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
            $("#mcs_container2").mCustomScrollbar("vertical", 400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);
        });

        $('#box2').css("display", "none");

        $("a.featuredPhoto").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        });

        $('#artistBoxLSlider').width(600 * <%= Model.BannerItems.Count %>);
    });

</script>
<div id="artistBoxWrap">
    <div id="previous">
        <a href="#" onclick="prevPic(1); return false;">
            <img src="/content/dm/images/artistBoxL_left_btn.png" alt="previous" />
        </a>
    </div>
    <div id="next">
        <a href="#" onclick="nextPic(1); return false;">
            <img src="/content/dm/images/artistBoxL_right_btn.png" alt="next"/>
        </a>
    </div>
    <div id="dots">
    <%  for (int i = 0; i < Model.BannerItems.Count; i++)
        {%>
            <li class="<%= i==0? "active": String.Empty %>"></li>
      <%}%>
    </div>
    <div id="artistBoxL">
        <div id="artistBoxLSlider">
            <ul style="list-style-image: none;">
        <% foreach (var bannerItem in Model.BannerItems)
           {%>
                <li style="list-style-image: none; margin-left: 0px;">
                    <img src="<%: bannerItem.PictureUrl %>" alt="<%: bannerItem.Title %>"/>
                </li>
           <%}%>
           </ul>
        </div>
    </div>
    <div id="artistBoxR">
        <div id="artistBoxRWrap">
            <!-- START Each Artist info ---->
            
            <% foreach (var bannerItem in Model.BannerItems)
               {%>
            <div class="artistR">
                <h1><%: bannerItem.Title %></h1>
                <p><%= bannerItem.Body %></p>
                <div class="social">
                    <a href="<%: bannerItem.LinkUrl %>"><img src="/content/dm/images/artistBoxR_learnMore_btn.png" class="learnMore" alt="learn more" /></a>
                    <%--<a href="#"><img src="/content/dm/images/artistBoxR_facebook.png" alt="facebook" /></a>
                    <a href="#"><img src="/content/dm/images/artistBoxR_twitter.png" alt="twitter" /></a>--%>
                </div>
            </div>
            <%} %>    
            <!-- END Each Artist Info ----->
        </div>
    </div>
</div>
    
<div id="bottomWrap">
    <div id="bottom">
        <div id="bottomL">
            <div class="title">Featured Media</div>
            <div class="buttons">
                <a href="#" onclick="changeMedia(1, this); return false; " class='featuredActive'>Videos</a> &nbsp;|&nbsp;
                <a href="#" onclick=" changeMedia(2, this); return false; ">Photos</a>
            </div>
            <div class="bottomMedia" id="box1">
                <% foreach (var video in Model.FeaturedVideos)
                   { %>
                        <a href="<%: video.MediaUrl %>" target="_blank" style="text-decoration: none; padding: 0px;">
                            <img style="width: 181px !important; height: 108px !important;" src="<%: video.ImageUrl %>" alt="Featured Videos"/>
                        </a>
                <% } %>
            </div>
            <div class="bottomMedia" id="box2" style="display: none;">
                <div id="mcs_container2">
                    <div class="customScrollBox">
                        <div class="container">
                            <div class="content">
                                <% foreach (var photo in Model.FeaturedPhotos)
                                   { %>
                                        <a class="featuredPhoto" href="<%: photo.PhotoUrl %>"><img src="<%: photo.ThumbnailUrl %>" alt="Featured Photo" /></a>
                                <% } %>
                            </div>
                        </div>
                        <div class="dragger_container">
                            <div class="dragger"></div>
                        </div>
                    </div>
                    <div class="scrollUpBtn"></div>
                    <div class="scrollDownBtn"></div>
                </div>
            </div>
        </div>
        <div id="bottomR">
            <div class="title">Social Media</div>
            <div id="bottomRContent">
                	
                <!-- START Custom Scroll Box -->
                <div id="mcs_container">
                    <div class="customScrollBox">
                        <div class="container">
                            <div class="content">
                                <!--START Scroll Box Content -->
                                <% foreach (var item in Model.SocialMedia)
                                   { %>
                                        <div class="socialItem <%: item.Type %>">
                                            <p><%: item.Text %></p>
                                            <div class="date"><%: item.DateCreated.ToString("MMMM dd, yyyy") %></div>
                                        </div>
                                <% } %>
                                <br/>
                            </div>
                        </div>
                        <div class="dragger_container">
                            <div class="dragger"></div>
                        </div>
                    </div>
                    <div class="scrollUpBtn"></div>
                    <div class="scrollDownBtn"></div>
                </div>

                <!-- END Custom Scroll Box --> 
                
                
            </div>
        </div>
    </div>
</div>
  
    
<div style="clear: both;"></div>