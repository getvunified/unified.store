﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Jhm.Web.Core.Models.IProduct>>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%
    const string changeMeString = "CHANGEME";

    foreach (var product in Model)
    { %>
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                var link1 = "<%= String.Format("javascript:alert('Sorry, this product is {0}.');",product.GetAvailabilityAsString()) %>";
                <%
                    var disableAddToCartButton = product.GetAvailability() == AvailabilityTypes.Out_of_Stock;
                    if ( !disableAddToCartButton )
                    {
                        //disableAddToCartButton = false; 
                    %>
                        link1 = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = product.Code, mediaType=product.StockItems.FirstOrDefault().MediaType.CodeSuffix, returnToUrl = Request.RawUrl}) %>";
                <%  } %>
                SetProductLink(link1);
                $('select[name="<%=product.Code %>"]').live('change', function (e) {
                    var link = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = product.Code, mediaType=changeMeString, returnToUrl = Request.RawUrl}) %>";
                    var selectedValue = e.target.options[e.target.selectedIndex].value;
                    var newLink = link.replace("CHANGEME", selectedValue);
                    SetProductLink(newLink);
                });

                function SetProductLink(Newlink) {
                    $('a[name="<%=product.Code %>Link"]').attr("href", Newlink);

                }
            });
        </script>
        <div class="item clearfix">
            <div class="itemL">
                <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = product.Code, productTitle= product.Title.ToSafeForUrl()}) %>">
                <% if (product.AlternativeImages.Count() > 0)
                   {%>
                        <img alt="<%:product.Title%>" src="<%:Url.Content(product.AlternativeImages.First())%>" height="180" class="albumShadow" />
                <% } %> 
                </a>
            </div>
            <div class="itemR">
                <h1><%= Html.ActionLink(product.Title ?? string.Empty, MagicStringEliminator.CatalogActions.Product, new { sku = product.Code, productTitle = product.Title.ToSafeForUrl() })%></h1>
                <%= (!product.AuthorDescription.IsNullOrEmpty()) ? "<h2>By: {0}</h2>".FormatWith(product.AuthorDescription) : string.Empty%>
                <%= (!product.Description.IsNullOrEmpty()) ? "<p>{0}</p>".FormatWith(product.Description.TruncateToClosestPuntuationMarkAt(200, "...")) : string.Empty%>
                <% switch (product.GetAvailability())
                    {
                        case AvailabilityTypes.In_Stock: %>
                            <img src="<%= Url.Content("~/content/dm/images/view_inStock.jpg") %>" class="inStock"> 
                        <%  break;
                        case AvailabilityTypes.Backordered: %>
                            <img src="<%= Url.Content("~/content/dm/images/view_backorder.jpg") %>" class="inStock"> 
                        <%  break;
                        case AvailabilityTypes.Out_of_Stock: %>
                            <img src="<%= Url.Content("~/content/dm/images/view_outOfStock.jpg") %>" class="inStock"> 
                        <%  break;
                    } 
                %>
                <%--<a href="#"><img src="/store/assets/btn_itunes.jpg" class="iTunes"></a>--%>
                <div class="addToCart">
                    <select class="typeSelect" id="Select1" name="<%=product.Code %>">
                        <%
                            var i = 1;
                            foreach (var stockItem in product.StockItems)
                            {
                                var isProductOnSalePrice = stockItem.IsOnSale();
                                var itemSalesPrice = stockItem.SalePrice;
                                var itemBasePrice = stockItem.BasePrice;
                                var formattedPrice = string.Empty;
                                var plainPrice = isProductOnSalePrice? itemSalesPrice : itemBasePrice;
                                if (isProductOnSalePrice)
                                {
                                    formattedPrice = FormatHelper.FormatWithClientCulture(
                                        (Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],
                                        "{0} <span style='text-decoration: line-through; color: red'>{1:C}</span> {2:C}",
                                        stockItem.MediaType.DisplayName, itemBasePrice, itemSalesPrice);
                                }
                                else
                                {
                                    formattedPrice = FormatHelper.FormatWithClientCulture(
                                         (Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "{0} {1:C}",
                                         stockItem.MediaType.DisplayName, itemBasePrice);
                                }

                                var selected = i == 1? "selected='selected'": string.Empty; %>
                                <option value="<%=stockItem.MediaType.CodeSuffix%>" <%= selected %> formattedText="<%=formattedPrice%>">
                                    <%= FormatHelper.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "{0} {1:C}", stockItem.MediaType.DisplayName, plainPrice)%>
                                </option>
                            <%  i++;
                            } %>
                    </select>
                    <a href="#" name="<%=product.Code %>Link"><img src="<%= Url.Content("~/content/dm/images/btn_addToCart.jpg") %>"></a>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>




<% }
    if (Model.Count().Equals(0))
    { %>
<h4>
    No products found</h4>
<% } %>
