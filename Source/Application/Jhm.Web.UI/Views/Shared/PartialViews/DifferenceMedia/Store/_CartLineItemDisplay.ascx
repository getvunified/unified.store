﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<tr>
    <%--remove column--%>
    <%--description column for CartItem by TYPE --%>
    <%: Html.Partial("PartialViews/DifferenceMedia/Store/_CartStockItemDisplay", Model.Item as StockItem)%>
    <%--StockItem columns--%>
    <% if (Model.Item is StockItem)
       { %>
    <td>
        <%: Model.Quantity %>
    </td>
    <td>
        <%: Model.Item.Price.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
    <%} %>
    <%--Line Total column--%>
    <td>
        <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "C") %>
    </td>
</tr>
