﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.ProductListingViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<div class="block">
    <div class="content">
        <div class="controls top clearfix">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Catalog/Headers/ProductListingSummary.ascx", Model); %>
        </div>
        <div class="controls bottom clearfix">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Catalog/Headers/ProductListingPaging.ascx", Model); %>
        </div>
        <div>
            <h5>* NOTE: MP3 downloads require a PC or a Mac</h5>
        </div>
        <div id="storeBoxWrap">
            <%: Html.Partial("PartialViews/DifferenceMedia/Store/_ProductListView", Model.Products) %>
        </div>
        <div class="controls bottom clearfix">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Catalog/Headers/ProductListingPaging.ascx", Model); %>
        </div>
        <div class="controls top clearfix">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Catalog/Headers/ProductListingSummary.ascx", Model); %>
        </div>
    </div>
</div>
