﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.CatalogProductViewModel>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<% const string changeMeString = "CHANGEME"; %>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            var link1 = "<%= String.Format("javascript:alert('Sorry, this product is {0}.');",Model.AvailabilityAsString) %>";
            <% ViewData["disableAddToCartButton"] = true; 
                if ( Model.Availability != AvailabilityTypes.Out_of_Stock )
               {
                    ViewData["disableAddToCartButton"] = false; %>
                    link1 = "<%=Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName,new{sku = Model.Code,mediaType =Model.StockItems.FirstOrDefault().MediaType.CodeSuffix,returnToUrl = Request.RawUrl})%>";
            <% } %>
            SetProductLink(link1);
            $('select[name="<%=Model.Code %>"]').live('change', function (e) {

                var link = '#';
                <% if (Model != null && Model.Code != null){ %>
                    link = "<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, new{ sku = Model.Code, mediaType=changeMeString, returnToUrl = Request.RawUrl}) %>";
                <% } %>
                var selectedValue = e.target.options[e.target.selectedIndex].value;
                var newLink = link.replace("CHANGEME", selectedValue);
                SetProductLink(newLink);
            });

            function SetProductLink(Newlink) {
                $('a[name="<%=Model.Code %>Link"]').attr("href", Newlink);

            }
        });
    </script>
    <style type="text/css">
        p {
            padding-bottom: 10px;
        }
    </style>
<%: Html.DifferenceMediaTitleHeader("Store - {0} - {1}".FormatWith(Model.Title, Model.Code)) %>
<div id="viewWrap">
    <div id="viewL">
        <%if (Model.AlternativeImages.IsNot().Empty())
          { %>
            <img src="<%: Url.Content(Model.AlternativeImages.ToArray()[0])%>" alt="<%: Model.Title %>"  width="140" class="albumShadow" />
        <%} %>
    </div>
    <div id="viewR">
        <%--<div style="float:right;"><a href="#"><img src="/store/assets/btn_itunes.jpg" /></a></div>--%>
        <h1><%: Model.Title %></h1>
        <%= (!Model.AuthorDescription.IsNullOrEmpty()) ? "<h2>By: {0}</h2>".FormatWith(Model.AuthorDescription) : string.Empty%>
        <br/>
        <div style="position:relative;width: 640px;">
            <div class="addToCart">
                <select class="typeSelect" name="<%=Model.Code%>">
                    <%  foreach (var stockItem in Model.StockItems)
                        {
                            var isProductOnSalePrice = stockItem.IsOnSale();
                            var itemSalesPrice = stockItem.SalePrice;
                            var itemBasePrice = stockItem.BasePrice;
                            var formattedPrice = string.Empty;
                            var plainPrice = isProductOnSalePrice ? itemSalesPrice : itemBasePrice;
                            if (isProductOnSalePrice)
                            {
                                formattedPrice = Model.FormatWithCompanyCulture("{0} <span style='text-decoration: line-through; color: red'>{1:C}</span> {2:C}",stockItem.MediaType.DisplayName, itemBasePrice, itemSalesPrice);
                            }
                            else
                            {
                                formattedPrice = Model.FormatWithCompanyCulture("{0} {1:C}",stockItem.MediaType.DisplayName, itemBasePrice);
                            } %>
                            <option value="<%= stockItem.MediaType.CodeSuffix %>" formattedText="<%=formattedPrice%>">
                                <%= Model.FormatWithCompanyCulture("{0} {1:C}", stockItem.MediaType.DisplayName, plainPrice)%>
                            </option>
                    <%  } %>
                </select>
                <a href="#" name="<%=Model.Code %>Link"><img src="<%= Url.Content("~/content/dm/images/btn_addToCart.jpg") %>" alt="Add to Cart" /></a>
             </div>

            <%  
            bool showMP3Warning = false;
            foreach (var stockItem in Model.StockItems)
            {
                if (stockItem.MediaType.CodeSuffix.Contains("AF"))
                {
                    showMP3Warning = true;
                }
            }
                        
            if (showMP3Warning)
            { %> 
                <div>
                        <h5 style="color:grey;">* NOTE: MP3 downloads require a PC or a Mac</h5>
                </div>

           <% }
              
        %>
         </div>
         <br/>

        <p><%= Model.Description %></p>
        <p>&nbsp;</p>
        <%--<h1>Music</h1>--%>
        <!-- START Jplayer -->
        <%--<div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <div id="jp_container_1" class="jp-audio">
            <div id="playerControls">
                        
                <div class="jp-type-single">
                <div class="jp-gui jp-interface">
                    <div class="jp-controls">
                    <a href="javascript:;" class="jp-previous"><img src="/artist/assets/player_btn_back.png" /></a>
                    <a href="javascript:;" class="jp-play"><img src="/artist/assets/player_btn_play.png" /></a>
                    <a href="javascript:;" class="jp-pause"><img src="/artist/assets/player_btn_pause.png" /></a>
                    <a href="javascript:;" class="jp-next"><img src="/artist/assets/player_btn_forward.png" /></a>
                    <a href="javascript:;" class="jp-mute"><img src="/artist/assets/player_btn_mute.png" /></a>
                    <a href="javascript:;" class="jp-unmute"><img src="/artist/assets/player_btn_mute.png" /></a>
                    </div>
                </div>
                </div>
                                  
                </div>
                        
                   
            <div id="songList">
                <div class="jp-playlist">
                        <ul>
                            <li></li>
                        </ul>
                </div>  
            </div>
                      
        </div>--%>
        <!-- END Jplayer -->
        <div style="position:relative;width: 640px;">
            <div class="addToCart">
                <select class="typeSelect" name="<%=Model.Code%>">
                    <%  foreach (var stockItem in Model.StockItems)
                        {
                            var isProductOnSalePrice = stockItem.IsOnSale();
                            var itemSalesPrice = stockItem.SalePrice;
                            var itemBasePrice = stockItem.BasePrice;
                            var formattedPrice = string.Empty;
                            var plainPrice = isProductOnSalePrice ? itemSalesPrice : itemBasePrice;
                            if (isProductOnSalePrice)
                            {
                                formattedPrice = Model.FormatWithCompanyCulture("{0} <span style='text-decoration: line-through; color: red'>{1:C}</span> {2:C}",stockItem.MediaType.DisplayName, itemBasePrice, itemSalesPrice);
                            }
                            else
                            {
                                formattedPrice = Model.FormatWithCompanyCulture("{0} {1:C}",stockItem.MediaType.DisplayName, itemBasePrice);
                            } %>
                            <option value="<%= stockItem.MediaType.CodeSuffix %>" formattedText="<%=formattedPrice%>">
                                <%= Model.FormatWithCompanyCulture("{0} {1:C}", stockItem.MediaType.DisplayName, plainPrice)%>
                            </option>
                    <%  } %>
                </select>
                <a href="#" name="<%=Model.Code %>Link"><img src="<%= Url.Content("~/content/dm/images/btn_addToCart.jpg") %>"></a>
             </div>
         </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
</div>