﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.StockItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>

<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div>
            <a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = Model.ProductCode, productTitle= Model.ProductTitle.ToSafeForUrl()}) %>">
                <%: Model.Title %></a>
        </div>
        <div >
            <%= String.IsNullOrEmpty(Model.AuthorDescription)? String.Empty : "By: "+ Model.AuthorDescription %>
            <% if (Model.IsDownloadableProduct())
               {%>
                <span style="font-size: 12px; font-weight:bold;"><br />A download link will be sent to your email address after purchase of this product.</span>
            <%}%></div>
        <div class="Image">
            <%--<%: Model.Item.ImageUrl %>--%>  <%--TODO:  Image?--%>
        </div>
    </div>
</td>
