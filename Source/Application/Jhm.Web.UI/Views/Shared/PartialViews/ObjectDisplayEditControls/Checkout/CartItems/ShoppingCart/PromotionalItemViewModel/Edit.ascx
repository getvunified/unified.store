﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.PromotionalItemViewModel>" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<tr>
    <td>
        <div class="ShoppingCart-CartItem-Description">
            <div style="font-weight: bold; font-size: 1.8em; color: #0C3249;">
                <%= Model.Product.Title %>
            </div>
            <div>
                <%= Model.GetDescription(150) %>
            </div>
        </div>
    </td>
    <td>
        <%= Model.Quantity %>
    </td>
    <td>
        <%: ((decimal)0).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
    <td>
        <%: ((decimal)0).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
</tr>
