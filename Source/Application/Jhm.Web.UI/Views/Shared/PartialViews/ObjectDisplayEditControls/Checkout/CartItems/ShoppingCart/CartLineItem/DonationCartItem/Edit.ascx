﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<% var item = Model.Item as DonationCartItem;
   if (item == null) throw new ArgumentNullException("Model.Item"); %>
<%--remove column--%>
<td style="text-align: center;">
    <div>
        <%: Html.CheckBox(CheckoutShoppingCartViewModel.SelectedCheckBoxName(item.Id))%>
    </div>
</td>
<td>
    <% var link = Url.RouteUrl(Jhm.Web.Core.MagicStringEliminator.Routes.Donation_Detail.RouteName, new { donationCode = item.Code }); %>
    <div class="ShoppingCart-CartItem-Description">
        <div class="header">
            <%= StringExtensions.FormatWith(@"<a href=""{0}"" >{1}</a>",link, item.Title)%>
        </div>
        <div>            
            <%= StringExtensions.FormatWith(@"<a href=""{0}"">{1}</a>",link,item.Description) %>
        </div>
        <div>
            Frequency :
            <%: item.DonationFrequency %></div>
        <% if (item.CertificateName.IsNot().NullOrEmpty())
           {%>
        <div>
            <%: item.CertificateLabelName %> Name:
            <%:
                       item.CertificateName%>
        </div>
        <%
            }%>
        <%
            if (item.IncludedStockItems.IsNot().Null() && item.IncludedStockItems.Count > 0)
            {%>
        <div>
            Product(s) Included:
            <ul>
                <% 
                foreach (var includedStockItems in item.IncludedStockItems)
                   {
                    %>
                <li>
                    <%= Html.RenderPartialEditByTypeName(includedStockItems, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/")%>
                </li>
                <%}%>
            </ul>
        </div>
        <%
            }
        %>
        <% 
            if (item.StockItemChosen.IsNot().Null())
            {%>
        <div>
            Product Chosen:
            <%: 
                item.StockItemChosen.Title%></div>
        <% } %>
        <div class="Image">
            <%--<%: Model.Item.ImageUrl %>--%>
        </div>
    </div>
</td>
<%--Line Total column--%>
<td>
    <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "C") %>
</td>