﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<% var item = Model.Item as GiftCardCartItem;
   if (item == null) throw new ArgumentNullException("Model.Item"); %>
<%--remove column--%>
<td style="text-align: center;">
    <div>
        <%: Html.CheckBox(CheckoutShoppingCartViewModel.SelectedCheckBoxName(item.Id))%></div>
</td>
<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div class="Image" style="float:right; position: relative; left: -20px">
            <img height="48" src="<%: Model.Item.ImageLocation %>"/>
        </div>
        <h4 class="header">
            <a href="<%= Url.Action("Order","GiftCards", new{id= item.GiftCardFormInfo.GiftCardId}) %>">
                <%: item.Title %></a>
        </h4>
        <div style="padding-right: 100px;">
            <%= item.Description %>
        </div>
    </div>
</td>
<td>
    <%: Html.TextBox(CheckoutShoppingCartViewModel.QuantityTextBoxName(item.Id), Model.Quantity, new { style = "width: 40px;" })%>
</td>
<td>
    <%: item.Price.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
</td>
<%--Line Total column--%>
<td>
    <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "C") %>
</td>