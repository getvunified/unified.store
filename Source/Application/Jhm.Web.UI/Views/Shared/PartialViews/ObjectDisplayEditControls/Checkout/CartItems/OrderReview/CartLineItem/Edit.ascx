﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<tr>
    <% if (Model.Item is SubscriptionCartItem || Model.Item is GiftCardCartItem)
       { %>
    <%= Html.RenderPartialByTypeName(Model.Item,"Display", "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/CartLineItem/")%>
    <%}
       else
       { %>
        <td>
            <h4>
                <% if (Model.MatchesDiscountCriteria)
                   { %>
                        <img height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
                <% } %>
                 <%: Model.Item.Title %>
            </h4>
        </td>
    <%} %>
    <%--StockItem columns--%>
    <% if (Model.Item is StockItem || Model.Item is GiftCardCartItem)
       { %>
    <td>
        <%: Model.Quantity%>
    </td>
    <td>
        <%: Model.Item.Price.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
    <%}
       else
       { %>
    <%--DonationCartItem columns--%>
    <td>
        &nbsp;
    </td>
    <td>
        &nbsp;
    </td>
    <%} %>
    <%--Line Total column--%>
    <td class="cost">
        <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
</tr>
