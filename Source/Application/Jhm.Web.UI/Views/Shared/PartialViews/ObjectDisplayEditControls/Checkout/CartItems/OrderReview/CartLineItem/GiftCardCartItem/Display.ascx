﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.ECommerce.GiftCardCartItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>

<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div class="Image" style="float:right; position: relative; left: -20px">
            <img height="48" src="<%: Model.ImageLocation %>" class="no-print"/>
        </div>
        <h4 class="header">
            <a href="<%= Url.Action("Order","GiftCards",new{id = Model.GiftCardFormInfo.GiftCardId}) %>">
                <%: Model.Title %></a>
        </h4>
        <div style="padding-right: 110px;">
            <%= Model.Description %>
        </div>
        
    </div>
</td>
