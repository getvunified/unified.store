﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<% var item = Model.Item as StockItem;
   if (item == null) throw new ArgumentNullException("Model.Item"); %>
<%--remove column--%>
<td style="text-align: center;">
    <div>
        <%: Html.CheckBox(CheckoutShoppingCartViewModel.SelectedCheckBoxName(item.Id))%>
        <% if (Model.MatchesDiscountCriteria)
           { %>
                <img style="float:right" height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
        <% } %>
    </div>
</td>
<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div class="header">
            <a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = item.ProductCode, productTitle= item.ProductTitle.ToSafeForUrl()}) %>">
                </a>
            <%: item.Title %>
        </div>
        <div >
            <%= item.GetDescription(150) %>
            <% if (item.IsDownloadableProduct())
               {%><span style="font-size: 12px;">A download link will be sent to your email address after purchase of this product.</span><%
               }%></div>
        <div class="Image">
            <%--<%: Model.Item.ImageUrl %>--%>  <%--TODO:  Image?--%>
        </div>
    </div>
</td>
<td>
    <%: Html.TextBox(CheckoutShoppingCartViewModel.QuantityTextBoxName(item.Id), Model.Quantity, new { style = "width: 40px;" })%>
</td>
<td>
    <%: item.Price.FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
</td>
<%--Line Total column--%>
<td>
    <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "C") %>
</td>