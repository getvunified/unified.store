﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<% var item = Model.Item as SubscriptionCartItem;
   if (item == null) throw new ArgumentNullException("Model.Item"); %>
<%--remove column--%>
<td style="text-align: center;">
    <div>
        <%: Html.CheckBox(CheckoutShoppingCartViewModel.SelectedCheckBoxName(item.Id))%></div>
</td>

<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div class="header">
            <a href="<%= Url.Action("JhmMagazineSubscription","Resources") %>">
                <%: item.Title %></a>
        </div>
        <div >
            <%= item.Description %>
        </div>
        <div class="Image">
            <%--<%: Model.Item.ImageUrl %>--%>  <%--TODO:  Image?--%>
        </div>
    </div>
</td>
<%--Line Total column--%>
<td>
    <%: (Model.Item.Price * Model.Quantity).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client], "C") %>
</td>