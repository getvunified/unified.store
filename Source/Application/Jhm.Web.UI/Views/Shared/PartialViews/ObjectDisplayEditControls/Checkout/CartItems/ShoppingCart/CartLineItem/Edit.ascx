﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CartLineItem>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<tr>
    <%-- columns for CartItem by TYPE --%>
    <%= Html.RenderPartialByTypeName(Model, Model.Item.GetType(),"Edit", "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/CartLineItem/")%>
    
</tr>
