﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.SubscriptionCartItem>" %>
<%@ Import Namespace="Jhm.Common" %>
<%@ Import Namespace="Jhm.Web.Core" %>

<%--description column--%>
<td>
    <div class="ShoppingCart-CartItem-Description">
        <div class="header">
            <a href="<%= Url.Action("JhmMagazineSubscription","Resources") %>">
                <%: Model.Title %></a>
        </div>
        <div >
            <%= Model.Description %>
        </div>
        <div class="Image">
            <%--<%: Model.Item.ImageUrl %>--%>  <%--TODO:  Image?--%>
        </div>
    </div>
</td>
