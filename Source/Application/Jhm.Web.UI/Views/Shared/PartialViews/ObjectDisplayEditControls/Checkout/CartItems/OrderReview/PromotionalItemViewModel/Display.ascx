﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.PromotionalItemViewModel>" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<tr>
    <td>
        <h4> <%= Model.Product.Title %></h4>
    </td>
    <td>
        <%= Model.Quantity %>
    </td>
    <td>
        <%: ((decimal)0).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
    <td>
        <%: ((decimal)0).FormatWithClientCulture((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client],"C")%>
    </td>
</tr>
