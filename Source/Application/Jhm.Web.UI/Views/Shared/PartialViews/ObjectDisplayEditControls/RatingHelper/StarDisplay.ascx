﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.IProduct>" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<div class="fivestar-widget clear-block fivestar-widget-5">
    <div class="star star-1 star-odd star-first on">
        <a style="width: <%: RatingHelper.FirstOfFiveStarWidth(Model.Rating()) %>%;" title="Poor">Poor</a></div>
    <div class="star star-2 star-even on">
        <a style="width: <%: RatingHelper.SecondOfFiveStarWidth(Model.Rating()) %>%;" title="Okay">Okay</a></div>
    <div class="star star-3 star-odd on">
        <a style="width: <%: RatingHelper.ThirdOfFiveStarWidth(Model.Rating()) %>%;" title="Good">Good</a></div>
    <div class="star star-4 star-even on">
        <a style="width: <%: RatingHelper.FourthOfFiveStarWidth(Model.Rating()) %>%;" title="Great">Great</a></div>
    <div class="star star-5 star-odd on star-last">
        <a style="width: <%: RatingHelper.FifthOfFiveStarWidth(Model.Rating()) %>%;" title="Awesome">Awesome</a></div>
</div>
