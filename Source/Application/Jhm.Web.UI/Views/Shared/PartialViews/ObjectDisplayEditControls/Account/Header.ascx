﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="Jhm.Web.Core" %>

<div class="login-menu-wrapper">
	<ul id="login-menu">
		<li class="<%: ((AccountManagementTabs.MainTab)ViewData["MainTab"]) == AccountManagementTabs.MainTab.LogOn ? "active" : String.Empty %>"><a href="<%: Url.Action("LogOn", "Account") %>"><span class="left"></span><span class="text">Login</span><span class="right"></span></a></li>
        <% if (Request.IsAuthenticated)
           {%>
		    <li class="<%: ((AccountManagementTabs.MainTab)ViewData["MainTab"]) == AccountManagementTabs.MainTab.MyAccount ? "active" : "last" %>"><a href="<%: Url.Action("MyAccount", "Account") %>"><span class="left"></span><span class="text">My Account</span><span class="right"></span></a></li>

        <%
           } %>
	</ul>
</div>
<div class="tabs">
	<ul class="tabs primary clearfix">
        <% if (((AccountManagementTabs.MainTab)ViewData["MainTab"]) == AccountManagementTabs.MainTab.LogOn)
           {
                if (Request.IsAuthenticated)
                {
                %>
	                <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.ChangePassword ? "active" : String.Empty %>"><a href="<%: Url.Action("ChangePassword", "Account") %>"><span class="tab">Change Password</span></a></li>
                    <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.ChangeEmail ? "active" : String.Empty %>"><a href="<%: Url.Action("ChangeEmailAddress", "Account") %>"><span class="tab">Change Email</span></a></li>
                    <%--<li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.ChangeQuestion ? "active" : String.Empty %>"><a href="<%: Url.Action("ChangeSecurityQuestion", "Account") %>"><span class="tab">Change Password Question</span></a></li>--%>
                <%
                }
                else
                { %>
                    <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.SignIn ? "active" : String.Empty %>"><a href="<%: Url.Action("LogOn", "Account") %>"><span class="tab">Sign In</span></a></li>
	                <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.Register ? "active" : String.Empty %>"><a href="<%: Url.Action("Register", "Account") %>"><span class="tab">Register</span></a></li>            
                    <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.ForgotPassword ? "active" : String.Empty %>"><a href="<%: Url.Action("ForgotPassword", "Account") %>"><span class="tab">Forgot Password</span></a></li>
                    <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.ForgotUsername ? "active" : String.Empty %>"><a href="<%: Url.Action("ForgotUsername", "Account") %>"><span class="tab">Forgot Username</span></a></li>
       <%       }
           }
           else
           { %>
               <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.MyAccount ? "active" : String.Empty %>"><a href="<%: Url.Action("MyAccount", "Account") %>"><span class="tab">My Account</span></a></li>
               <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.Downloads ? "active" : String.Empty %>"><a href="<%: Url.Action("MyDownloads", "Account") %>"><span class="tab">My Downloads</span></a></li>
              <%-- <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.Preferences ? "active" : String.Empty %>"><a href="<%: Url.Action("Preferences", "Account") %>"><span class="tab">Preferences</span></a></li>--%>
             <%-- <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.MyTransactions ? "active" : String.Empty %>"><a href="<%: Url.Action("MyTransactions", "Account") %>"><span class="tab">My Transactions</span></a></li>--%>
              <li class="<%: ((AccountManagementTabs.SecondaryTab)ViewData["SecondaryTab"]) == AccountManagementTabs.SecondaryTab.MySubscriptions ? "active" : String.Empty %>"><a href="<%: Url.Action("MySubscriptions", "Account") %>"><span class="tab">My Subscriptions</span></a></li>

          <%  } %>
	</ul>
</div>
