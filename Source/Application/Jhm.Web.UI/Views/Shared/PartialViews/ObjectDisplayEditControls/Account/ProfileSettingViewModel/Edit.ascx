﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.ProfileSettingViewModel>" %>
<tr>
    <td><%: Html.DisplayTextFor(m => m.Title) %></td>
    <td><%: Html.DisplayTextFor(m => m.Description) %></td>
    <td><%: Html.CheckBoxFor(m => m.IsActive) %></td>
</tr>