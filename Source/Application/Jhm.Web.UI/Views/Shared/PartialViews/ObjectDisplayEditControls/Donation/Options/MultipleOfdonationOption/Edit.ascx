﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.MultipleOfdonationOption>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>

<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/IdTitleImageBodyStockItems.ascx")%>
<%: Html.TextBox(DonationFormViewModel.QuantityTextBoxName(Model.Id), ViewData[DonationFormViewModel.QuantityTextBoxName(Model.Id)], new { style = "width:50px;text-align:right;" })%>
<strong>X <%: Model.PricePerMultiple.FormatWithClientCulture(Client, "C")%></strong>
<% if (Model.CertificateIsRelatedToMultiple)
   { %>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/CertificateRelatedToMultiple.ascx")%>
<% }
   else
   { %>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/Certificate.ascx")%>
<%} %>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/DonationFrequency.ascx")%>
