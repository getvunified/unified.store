﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.FixedAmountDonationOption>" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>

<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/IdTitleImageBodyStockItems.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/Certificate.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/DonationFrequency.ascx")%>
