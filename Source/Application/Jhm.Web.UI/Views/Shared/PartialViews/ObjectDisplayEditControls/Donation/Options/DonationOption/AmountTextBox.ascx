﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.DonationOption>" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%= GetCurrencySymbol() %>
<%= Html.TextBox(DonationFormViewModel.AmountTextBoxName(Model.Id),
                             (ViewData[DonationFormViewModel.AmountTextBoxName(Model.Id)] != null)
                               ? ViewData[DonationFormViewModel.AmountTextBoxName(Model.Id)].TryConvertTo<decimal>().ToString("0.00")
                                : "0.00",
                                     new { @class = "user-donation", @onfocus="document.getElementById(\'"+DonationFormViewModel.SelectedChoiceName + Model.SortOrder+"\').checked = true;" })%>  
                    
                     
