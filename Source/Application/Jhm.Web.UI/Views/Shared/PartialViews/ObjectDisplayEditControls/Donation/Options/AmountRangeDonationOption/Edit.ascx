﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.AmountRangeDonationOption>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%= Html.RenderPartial(Model,"~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/IdTitleImageBodyStockItems.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/AmountTextBox.ascx")%>
Amount must be between <strong> <%: Model.MinAmount.FormatWithClientCulture(Client, "C")%> and <%: Model.MaxAmount.FormatWithClientCulture(Client, "C")%></strong>.
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/Certificate.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/DonationFrequency.ascx")%>