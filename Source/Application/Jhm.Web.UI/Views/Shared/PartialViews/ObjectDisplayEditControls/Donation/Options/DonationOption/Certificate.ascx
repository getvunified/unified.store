﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.DonationOption>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%
    if (Model.OptionFieldIsEnabled())
    {
%>
        <div class="donation-option-certificate">
            <br />
            <%: Model.OptionField.LabelText %>
            <%:Html.TextBox(
                DonationFormViewModel.CertificateTextBoxName(Model.Id),
                ViewData[DonationFormViewModel.CertificateTextBoxName(Model.Id)] ?? string.Empty,
                Model.OptionField.MaxLength > 0 ? new{@maxlength=Model.OptionField.MaxLength.ToString()} : null
             )%>
            <%
                if (Model.OptionFieldIsRequired())
                {%>
            <span class="form-required">*</span>
            <%
                }%>
        </div>
<%  } %>
