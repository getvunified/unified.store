﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.DonationOption>" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<% if (Model is FixedAmountDonationOption && Model.Amount.Is().GreaterThan(0))
   {%>
<%: Model.Amount.FormatWithClientCulture(Client, "C")%>
-
<%} %>
<label for="<%: DonationFormViewModel.SelectedChoiceName + Model.SortOrder %>"><%= DonationOption.ReplaceDonationFormat(Model.Title,Client) %></label>
<% if (Model.ImageUrl.IsNot().NullOrEmpty())
   { %>
<div class="donation-option-image">
    <img src="<%:Model.ImageUrl %>" alt="<%: Model.Title %>" /></div>
<%} %>
<p>
    <%= DonationOption.ReplaceDonationFormat(Model.Description, Client)%></p>
<% if (Model.IncludedStockItems.IsNot().Empty())
   {%>
<div class="donation-option-stockItems">
    For Your Donation, You Will Receive:
    <ul>
        <% foreach (var includedStockItems in Model.IncludedStockItems)
           {%>
        <li>
            <%= 
                Html.RenderPartialEditByTypeName(includedStockItems, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/")%></li>
        <%}%>
    </ul>
</div>
<%} %>
<% if (Model.StockItemChoices.IsNot().Empty())
   { %>
<div class="donation-option-stockItemChoices">
    Please Choose one of the following:
    <%: Html.DropDownList(DonationFormViewModel.SelectedStockItemChoiceListName(Model.Id), 
                          Model.StockItemChoices.Select(stockItemChoice => new SelectListItem() 
                                                        {Selected = ViewData[DonationFormViewModel.SelectedStockItemChoiceListName(Model.Id)].TryConvertTo<Guid>() == stockItemChoice.Id,
                                                         Text = stockItemChoice.Title, 
                                                         Value = stockItemChoice.Id.ToString()}).AddNoSelection()) %><span class="form-required">*</span>
</div>
<% } %>
