﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.DonationOption>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<% if (Model.OptionFieldIsEnabled())
   {
       var certValue = ViewData[DonationFormViewModel.CertificateTextBoxName(Model.Id)].IsNot().Null() ? ViewData[DonationFormViewModel.CertificateTextBoxName(Model.Id)].ToString() : string.Empty;
       var certValues = certValue.IsNot().NullOrEmpty() ? certValue.TrimEnd(',').Split(',') : new string[] { };
%>
<div class="donation-option-certificate">
    <br />
    <%: Model.OptionField.LabelText %>
    <br />
    <div>
        <ol id="certificates">
            <%
                foreach (var item in certValues)
                { %>
                    <li>
                        <div class="donation-option-certificate-input">
                            <%: 
                            Html.TextBox(DonationFormViewModel.CertificateTextBoxName(Model.Id), item,
                Model.OptionField.MaxLength > 0 ? new { @maxlength = Model.OptionField.MaxLength.ToString() } : null)%>
                            <% if (Model.OptionFieldIsRequired())
                               { %>
                            <span class="form-required">*</span>
                            <% } %>
                            <a name="Remove" href="#" class="easybutton mini" style="position:inherit;">Remove</a>
                        </div>
                    </li>
                <% 
                }
                if (ViewData["ShouldShowDefaultInput"] == null || ((bool)ViewData["ShouldShowDefaultInput"])){ %>
                    <li>
                        <div class="donation-option-certificate-input">
                            <%: 
                        Html.TextBox(DonationFormViewModel.CertificateTextBoxName(Model.Id), string.Empty,
                Model.OptionField.MaxLength > 0 ? new { @maxlength = Model.OptionField.MaxLength.ToString() } : null)%>
                            <% if (Model.OptionFieldIsRequired())
                               { %>
                            <span class="form-required">*</span>
                            <% } %>
                            <a name="Remove" class="easybutton mini" style="position:inherit;" href="#">Remove</a>
                        </div>
                    </li>
            <% } %>
        </ol>
    </div>
    <div class="donation-option-AddNewCertificate">
        <a name="AddNew" href="#" class="easybutton mini" style="position:inherit;">Add New</a>
    </div>
</div>
<div class="donation-option-certificate-add">
</div>
<script type="text/javascript">
    var quantityInput = $('input[name=<%=DonationFormViewModel.QuantityTextBoxName(Model.Id)%>]');
    quantityInput.css('border', 'none');
    quantityInput.css('font-weight', 'bold');
    quantityInput.attr("readonly", true);
    quantityInput.val($(".donation-option-certificate-input").size());

    $("a[name='AddNew']").live('click', function () {
        AddNew();
        IncrementCount();

        return false;
    });

    $("a[name='Remove']").live('click', function () {
        RemoveItem($(this));
        IncrementCount();
        return false;
    });

    function IncrementCount() { quantityInput.val($(".donation-option-certificate-input").size()); }
    function AddNew() {
        $('#certificates').append('<li><div class="donation-option-certificate-input"> <%: Html.TextBox(DonationFormViewModel.CertificateTextBoxName(Model.Id), string.Empty,Model.OptionField.MaxLength > 0 ? new{@maxlength=Model.OptionField.MaxLength.ToString()} : null) %> <span class="form-required">*</span> <a name="Remove" href="#" class="easybutton mini" style="position:inherit;">Remove</a> </div></li>');
    }

    function RenameRemove(obj) {
        obj.attr("name", "Remove");
        obj.text("Remove");
    }
    function RemoveItem(obj) {
        if ($(".donation-option-certificate-input").size() > 1) {
            obj.parent().parent().remove();
        }
    }

</script>
<%} %>
