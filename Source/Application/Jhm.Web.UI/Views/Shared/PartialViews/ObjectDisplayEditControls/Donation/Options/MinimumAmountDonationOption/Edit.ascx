﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.MinimumAmountDonationOption>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/IdTitleImageBodyStockItems.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/AmountTextBox.ascx")%>
Amount must be minimum of <strong> <%: Model.MinAmount.FormatWithClientCulture(Client, "C") %></strong> to receive gift(s). 
    <%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/Certificate.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/DonationFrequency.ascx")%>