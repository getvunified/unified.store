﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl<Jhm.Web.Core.Models.AnyAmountDonationOption>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>

<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/IdTitleImageBodyStockItems.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/AmountTextBox.ascx")%> 
<p>Amount must be greater than <strong><%: ((decimal)0.00).FormatWithClientCulture(Client,"C") %></strong></p>
 <%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/Certificate.ascx")%>
<%= Html.RenderPartial(Model, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Donation/Options/DonationOption/DonationFrequency.ascx")%>
