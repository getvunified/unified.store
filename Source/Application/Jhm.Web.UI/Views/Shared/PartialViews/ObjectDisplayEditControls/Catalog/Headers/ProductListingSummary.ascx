﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.ProductListingViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>

<div class="left">
    <strong>Displaying:</strong>
    <%= StringExtensions.FormatWith("{0} - {1}",(((Model.PagingInfo.CurrentPage-1) * Model.PagingInfo.PageSize)+1).ToString(),
            (Model.PagingInfo.TotalResults < (Model.PagingInfo.CurrentPage * Model.PagingInfo.PageSize)) ? Model.PagingInfo.TotalResults : (Model.PagingInfo.CurrentPage * Model.PagingInfo.PageSize))%></div>
<div class="right">
    <strong>Total Results:</strong>
    <%= Model.PagingInfo.TotalResults.ToString() %>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Items per page:</strong>
    <%= Model.PagingInfo.PageSize.ToString() %>
</div>
