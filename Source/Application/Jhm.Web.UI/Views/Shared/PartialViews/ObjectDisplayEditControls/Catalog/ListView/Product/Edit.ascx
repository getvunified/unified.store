﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Product>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<tr>
    <%--image column--%>
    <td style="text-align: center;">
        <div>
            <% 
                if (Model.AlternativeImages.IsNot().Empty())
                {%>
            <img src="<%: Model.AlternativeImages.ToLIST()[0] %>" alt="<%:Model.Title %>" />
            <%} %>
        </div>
    </td>
    <%--description column  --%>
    <td>
        <h1>
            <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new{ sku = Model.Code}) %>">
                <%: Model.Title %></a></h1>
        <div>
            <%: Model.Author %></div>
        <% var inStock = Model.StockItems.Where(x => x.QuantityInStock.Is().GreaterThan(0)).Count().Is().GreaterThan(0);
           var allowBackOrder = Model.StockItems.Where(x => x.AllowBackorder).Count().Is().GreaterThan(0);
        %>
        <div>
            Availability:
            <%: "{0}".FormatWith(inStock ? "In Stock" : (allowBackOrder ? "Backordered" : "Out of Stock") )   %></div>
        <div>
            <% if (Model.Reviews.IsNot().Empty())
               { %>
            <div>
                <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/RatingHelper/StarDisplay.ascx"); %>
                <div>
                    <%:Model.Reviews.Count() %>
                    Reviews</div>
            </div>
            <%}
               else
               {%>
            Be the first to review this product.
            <%} %>
        </div>
        </>
    </td>
</tr>
