﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.Modules.ECommerce.ProductListingViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<div class="pager left">
    <%
        var curPage = Model.PagingInfo.CurrentPage;
        var totPages = Model.PagingInfo.PageCount;
        var maxPageSizeLinks = 8;
        var startPage = 1;
        var endPage = totPages;
        var curActionName = Model.PagingInfo.CurrentActionName;
        var curControllerName = Model.PagingInfo.CurrentControllerName;
        
        if (curPage > 1)
       {%>
    <%= Html.ActionLink("<<", curActionName, curControllerName, new { page = 1 }, new { @class = "button" })%>
    <%= Html.ActionLink("Prev", curActionName, curControllerName, new { page = (curPage - 1).ToString() }, new { @class = "button" })%>
    <%
        }

        if (curPage >= maxPageSizeLinks)
            startPage = curPage;
        if(totPages>maxPageSizeLinks)
            endPage = startPage + maxPageSizeLinks-1;

        
       //for (int i = 1; i <= totPages; i++)
       for (int i = startPage; i <= endPage && i <= totPages; i++)
       {

           var linkTag = (curPage == i)
                             ? Html.ActionLink(i.ToString(), curActionName, curControllerName, new { page = i.ToString() }, new { @class = "current" })
                             : Html.ActionLink(i.ToString(), curActionName, curControllerName, new { page = i.ToString() }, null);
    %>
    <%= linkTag %>
    <%
        } %>
    <%= " of {0}".FormatWith(totPages) %>
    <% if (curPage < totPages)
       {%>
    <%= Html.ActionLink("Next", curActionName, curControllerName, new { page = (curPage + 1).ToString() }, new { @class = "button" })%>
    <%= Html.ActionLink(">>", curActionName, curControllerName, new { page = totPages.ToString() }, new { @class = "button" })%>
    <%
        }%>
</div>
<div class="right">
    <%--<strong>View:</strong> <a href="#">Gallery</a> / <a href="#">List</a>--%>
</div>
