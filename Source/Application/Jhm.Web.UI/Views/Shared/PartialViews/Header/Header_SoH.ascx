﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<% Html.RenderPartial("~/Views/Shared/PartialViews/Header/GenericPopupMessage.ascx", TempData[MagicStringEliminator.ViewDataKeys.PopupMessage] ?? new PopupMessageViewModel()); %>

<style type="text/css">
    .addthis_toolbox a {
        margin-right: 5px;
    }
</style>
<script type="text/javascript">
    var addthis_config =
    {
        ui_508_compliant: true,
        ui_cobrand: "none"
    }
</script>
<div class="section clearfix">
    <div id="logo">
        <a href="<%=Url.Action("Index", "Home")%>" title="John Hagee Ministries : Home" rel="home">
            <img src="<%: Url.Content("~/Content/images/Header.png") %>"
                alt="Sanctuary of Hope" /></a>
    </div>
    <div class="region-header clearfix">
         <div id="secondary-menu">
     <%-- %>       <ul class="links">
                <li class="first">
                    <div id="media-icon">
                    </div>
                    <a href='http://www.getv.org'>GETV</a>
                    <div class="background"></div>
                </li>
                <li class="middle" style="cursor: pointer;" onclick="location.href='<%: Url.Action("ContactInfo", "Home", new { Area = String.Empty }, null)%>';">
                    <%--<div id="mail-icon">
                    </div>
                    <div id="down-arrow-icon"></div>
                    <%: Html.ActionLink("Contact Info", "ContactInfo", "Home", new { Area = String.Empty }, null)%>
                    <ul>
                        <li class="first-sub">
                            <p>
                                John Hagee Ministries<br />
                                United States
                                <img alt="US Flag" id="usaFlag" runat="server" src="~/Content/images/usa.png" /><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                        <li class="middle-sub">
                            <p>
                                John Hagee Ministries<br />
                                Canada
                                <img alt="CND Flag" id="canadaFlag" runat="server" src="~/Content/images/canada.png" /><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                        <li class="last-sub">
                            <p>
                                John Hagee Ministries<br />
                                United Kingdom
                                <img alt="UK Flag" id="ukFlag" runat="server" src="~/Content/images/uk.png" /><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                    </ul>
                    <div class="background"></div>
                </li>
                <%--<li class="middle">
                    $1$<div id="donate-icon">
                    </div>#1#
                    <%: Html.ActionLink("Donate Now", MagicStringEliminator.Routes.Donation_Form.Action, MagicStringEliminator.Routes.Donation_Form.Controller, new { donationCode = "One_Time", Area = String.Empty }, null)%>
                    <div class="background"></div>
                </li>--%>
 <%--               <li class="last">
                    <div id="dm-icon"></div>
                    <a href="http://www.differencemedia.org" target="_blank">Difference Media</a>
                    <div class="background"></div>
                </li>
            </ul>
        </div>--%>
        <!-- /#secondary-menu -->
    </div>
    <!-- /.region-header -->
    <div id="navigation">
        <div class="section clearfix">
            <div id="main-menu">
              <ul>
                    <li class="last">
                        <%: Html.ActionLink("Online Donations", "Index", "Donation", new { Area = String.Empty }, null)%>                  
                    </li>
                </ul>
            </div>
            <!-- /#main-menu -->
        </div>
    </div>
    <!-- /.section, /#navigation -->
</div>
