﻿<%@ Control Language="C#" Inherits="Jhm.Web.UI.Views.CompanyCapableViewUserControl" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<% Html.RenderPartial("~/Views/Shared/PartialViews/Header/GenericPopupMessage.ascx", TempData[MagicStringEliminator.ViewDataKeys.PopupMessage] ?? new PopupMessageViewModel()); %>

<style type="text/css">
    .addthis_toolbox a {
        margin-right: 5px;
    }
</style>
<script type="text/javascript">
    var addthis_config =
    {
        ui_508_compliant: true,
        ui_cobrand: "none"
    }
</script>
<div class="section clearfix">
    <div id="logo">
        <a href="<%=Url.Action("Index", "Home")%>" title="John Hagee Ministries : Home" rel="home">
            <%-- There needs to be a logo for each supported culture, eg: logo_en-US.png --%>
            <% var theme = ApplicationManager.ApplicationData.CurrentTheme; %>
            <img src="<%: Url.Content(String.Format("~/Content/images/Themes/{1}/logo_{0}.png",FormatHelper.GetCultureName((Client)ViewData[MagicStringEliminator.ViewDataKeys.Client]), theme)) %>"
                alt="John Hagee Ministries Logo" /></a>
    </div>
    <div class="region-header clearfix">
        <div class="social-media" style="top:-15px;width:248px;">
            
            <!-- AddThis Follow END -->
            <%--<div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_compact" href='<%= Url.ConvertToSafeUrl(@"www.addthis.com/bookmark.php?v=250&amp;pubid=ra-4dad7fc05d2229b1") %>'>
                    Share
                </a> <a target="_blank" href='<%= Url.ConvertToSafeUrl(@"www.facebook.com/JohnHageeMinistries") %>'>
                        <img id="Img1" runat="server" src="~/Content/images/icon-facebook.png" title="Facebook"
                            alt="Facebook" /></a> <a target="_blank" href='<%= Url.ConvertToSafeUrl(@"twitter.com/#!/JohnHageeMin") %>'>
                                <img id="Img2" runat="server" src="~/Content/images/icon-twitter.png" title="Twitter"
                                    alt="Twitter" /></a> <a target="_blank" href='<%= Url.ConvertToSafeUrl(@"www.youtube.com/user/HAGEEMINISTRIES?feature=mhum") %>'>
                                        <img id="Img3" runat="server" src="~/Content/images/icon-youtube.png" title="YouTube"
                                            alt="YouTube" /></a>
                <g:plusone size="medium" href="http://www.jhm.org/" count="false"></g:plusone>
                <div class="atclear"></div>
            </div>--%>
        </div>
        <div class="user-menu">
            <div style="width: 100%; position: relative; float: right" class="donateNow">
                <%: Html.ActionLink("Donate Now", MagicStringEliminator.Routes.Donation_Form.Action, MagicStringEliminator.Routes.Donation_Form.Controller, new { donationCode = "One_Time", Area = String.Empty }, null)%>
            </div>
            <ul class="links" style="display: block; float: right;">
                <% Html.RenderPartial("~/Views/Shared/PartialViews/Header/LogOnUserControl.ascx"); %>
                <% 
                    if (ViewData.ModelAs<CartViewModel>().IsNot().Null())
                    { %>
                <li class="last">
                    <a href="<%: Url.RouteUrl(MagicStringEliminator.Routes.Checkout_ShoppingCart.RouteName) %>">
                        <span class="icon">(<%=ViewData.ModelAs<CartViewModel>().NumberOfItemsInCart%>)</span></a>
                </li>
                <%} %>
            </ul>

        </div>
        <div id="secondary-menu">
            <ul class="links">
                <li class="first">
                    <div id="media-icon">
                    </div>
                    <a href='http://www.getv.org'>GETV</a>
                    <div class="background"></div>
                </li>
                <li class="middle" style="cursor: pointer;" onclick="location.href='<%: Url.Action("ContactInfo", "Home", new { Area = String.Empty }, null)%>';">
                    <%--<div id="mail-icon">
                    </div>--%>
                    <div id="down-arrow-icon"></div>
                    <%: Html.ActionLink("Contact Info", "ContactInfo", "Home", new { Area = String.Empty }, null)%>
                    <ul>
                        <li class="first-sub">
                            <p>
                                John Hagee Ministries<br />
                                United States
                                <img alt="US Flag" id="usaFlag" runat="server" src="~/Content/images/usa.png" /><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_UnitedStates.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                        <li class="middle-sub">
                            <p>
                                John Hagee Ministries<br />
                                Canada
                                <img alt="CND Flag" id="canadaFlag" runat="server" src="~/Content/images/canada.png" /><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_Canada.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                        <li class="last-sub">
                            <p>
                                John Hagee Ministries<br />
                                United Kingdom
                                <img alt="UK Flag" id="ukFlag" runat="server" src="~/Content/images/uk.png" /><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.PhoneNumber] %><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine1] %><br />
                                <%: Company.JHM_UnitedKingdom.AdditionalData[Company.DataKeys.AddressLine2] %>
                            </p>
                        </li>
                    </ul>
                    <div class="background"></div>
                </li>
                <%--<li class="middle">
                    $1$<div id="donate-icon">
                    </div>#1#
                    <%: Html.ActionLink("Donate Now", MagicStringEliminator.Routes.Donation_Form.Action, MagicStringEliminator.Routes.Donation_Form.Controller, new { donationCode = "One_Time", Area = String.Empty }, null)%>
                    <div class="background"></div>
                </li>--%>
                <li class="last">
                    <div id="dm-icon"></div>
                    <a href="http://www.differencemedia.org" target="_blank">Difference Media</a>
                    <div class="background"></div>
                </li>
            </ul>
        </div>
        <!-- /#secondary-menu -->
    </div>
    <!-- /.region-header -->
    <div id="navigation">
        <div class="section clearfix">
            <div id="main-menu">
                <ul class="links">
                    <li class="first">
                        <%: Html.ActionLink("Home", "Index", "Home", new { Area = String.Empty }, null)%>
                    </li>
                    <li>
                        <%: Html.ActionLink("About Us", "About", "Home", new { Area = String.Empty }, null)%>
                        <ul>
                            <li class="first">
                                <%: Html.ActionLink("Our Mission", "About", "Home", new { Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Pastor John Hagee", "About", "Home", new { id = "PastorJohnHagee", Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Pastor Matthew Hagee", "About", "Home", new { id = "PastorMatthewHagee", Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Diana Hagee", "About", "Home", new { id = "DianaHagee", Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Contact Us", "ContactUs", "Home", new { Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Beliefs", "About", "Home", new { id = "Beliefs", Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Support Israel", "About", "Home", new { id = "WhySupportIsrael", Area = String.Empty }, null)%>
                            </li>
                            <%--<li><a href="#">TV / Radio Schedule</a></li>--%>
                            <li>
                                <%: Html.ActionLink("The Difference", "About", "Home", new { id = "TheDifference", Area = String.Empty }, null)%>
                            </li>
                            <li class="last">
                                <%: Html.ActionLink("FAQ's", "Index", "FAQs", new { Area = String.Empty }, null)%>
                            </li>
                        </ul>
                    </li>
                    <li><a href="<%: Url.Action("JhmBlog", "Resources", new { Area = String.Empty }, null)%>">
                        News &amp; Events</a>
                        <ul>
                            <%--<li class="first">
                                <%: Html.ActionLink("Latest News", "LatestNews", "Resources", new { Area = String.Empty }, null)%></li>--%>
                            <li class="first">
                                <%: Html.ActionLink("Upcoming Events", "UpcomingEvents", "Resources", new { Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Daily Devotionals", "DailyDevotionals", "Resources", new { Area = String.Empty }, null)%>
                            </li>
                            <%--<li><a href="#">Calendar</a></li>
                            <li class="last"><a href="#">Search</a></li>--%>
                        </ul>
                    </li>
                    <li><a href="<%: Url.Action("JhmBlog", "Resources", new { Area = String.Empty }, null)%>">
                        Resources</a>
                        <ul>
                            <li class="first">
                                <%: Html.ActionLink("Praise Report Form", "PraiseReport", "Resources", new { Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Prayer Requests", "PrayerRequest", "Resources", new { Area = String.Empty }, null)%>
                            </li>
                            <li>
                                <%: Html.ActionLink("Salvation Message", "SalvationMessage", "Resources", new { Area = String.Empty }, null)%>
                            </li>

                            <%--<li><%: Html.ActionLink("Online Programming", "OnlineProgramming", "Resources", new { Area = String.Empty }, null)%></li>--%>
                            <li>
                                <a href="/Content/media/TVScheduleJHT.pdf" target="_blank"><span>Ways to Watch
                                </span></a>
                            </li>

                            <li>
                                <%: Html.ActionLink("JHM Digital Magazine", "JhmMagazine", "Resources", new { Area = String.Empty }, null)%>
                            </li>

                            <%--<li>
                                <%: Html.ActionLink("JHM Magazine Subscription", "JhmMagazineSubscription", "Resources", new { Area = String.Empty }, null)%>
                            </li>--%>
                            <%--<li>
                                <%: Html.ActionLink("Forty Days of Prayer", "FortyDaysOfPrayer", "Resources", new { Area = String.Empty}, null) %>
                            </li>--%>

                            <li>
                                <%: Html.ActionLink("JHM Blog", "JhmBlog", "Resources", new { Area = String.Empty }, null)%>
                            </li>

                            <%--<li><a href="#">TV / Radio Schedule</a></li>--%>
                            <li class="last">
                                <%: Html.ActionLink("Email List Sign-up", "ENewsLetterForm", "Partnership", new { Area = String.Empty }, null)%>
                            </li>
                        </ul>
                    </li>
                    <li class="shopNowText">
                        <%: Html.ActionLink("Shop JHM", "Index", "Catalog", new { Area = String.Empty }, null)%>
                        <%--<ul>
                            <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/Categories.ascx"); %></ul>--%>
                    </li>
                    <li class="last">
                        <%: Html.ActionLink("Online Donations", "Index", "Donation", new { Area = String.Empty }, null)%>
                        <%--<a id="A1" runat="server" href="~/Donation/Opportunities">Online Donations</a>--%>
                        <%--<ul>
                            <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Donation/Opportunities.ascx"); %>
                        </ul>--%>
                    </li>
                </ul>
            </div>
            <!-- /#main-menu -->
        </div>
    </div>
    <!-- /.section, /#navigation -->
</div>
