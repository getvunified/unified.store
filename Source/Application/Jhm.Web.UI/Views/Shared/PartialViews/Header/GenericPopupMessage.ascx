﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.PopupMessageViewModel>" %>
<% 
if (!String.IsNullOrEmpty(Model.Message))
{
%>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        function updateCookie(name,value, days) {
            var expires = '';
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        }
        
        $(document).ready(function () {
            var dialog = $('<div />').html("<%= Model.Message %>").dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                title: '<%= Model.Title %>',
                buttons: { "Ok": function () { $(this).dialog('close'); } }
            });
            <%
            var sb = new StringBuilder();
            if ( Model.Buttons.Count > 0 )
            {
                sb = new StringBuilder("{ ");
                foreach (var link in Model.Buttons)
                {
                    var comma = sb.Length == 2 ? String.Empty : ",";
                    sb.AppendFormat("{0}'{1}': function() {{ {2} }}", comma,link.InnerText,link.HRef);
                }
                sb.Append(" }");
            }
            %>
            <%= sb.Length > 0 ? String.Format("dialog.dialog( 'option', 'buttons', {0} );",sb) : String.Empty %>
            dialog.dialog('open');
        });
        
    </script>
<% } %>

