﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Jhm.Web" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%
    if (Request.IsAuthenticated)
    {%>
        <li class="dropdown first">
            <% var securityUser = SessionManager.SecurityUser; %>
            <%: Html.ActionLink(securityUser == null || (securityUser != null && String.IsNullOrEmpty((securityUser.FirstName + securityUser.LastName).Trim())) ? "My Account" : String.Format("{0} {1}",securityUser.FirstName,securityUser.LastName), "MyAccount", "Account", new { Area = String.Empty }, new { @class = "easyui-splitbutton", @menu = "#mySplitButtonMenu" })%>
            <div id="mySplitButtonMenu" style="width:150px;">
                <div><%: Html.ActionLink("My Account", "MyAccount", "Account", new { Area = String.Empty }, null)%></div>
                <div><%: Html.ActionLink("My Downloads", "MyDownloads", "Account", new { Area = String.Empty }, null)%></div>
                <div><%: Html.ActionLink("My Preferences", "Preferences", "Account", new { Area = String.Empty }, null)%></div>
            </div>
        </li>
        <li>
            <%: Html.ActionLink("Log Off", "LogOff", "Account", new { Area = String.Empty }, null)%>
        </li>   
        <% if (Context.User.IsInRole(Role.KnownRoles.SuperAdmin) || 
               Context.User.IsInRole(Role.KnownRoles.Admin) || 
               Context.User.IsInRole(Role.KnownRoles.ContentEditor) ||
               Context.User.IsInRole(Role.KnownRoles.CustomerService))
           { %>
                <li>
                    <a href="/Admin">Admin</a>
                </li>
        <% } %>
<%
    }
    else
    {
%>
        <li class="first">
            <%: Html.ActionLink("Sign in", "LogOn", "Account", new { Area = String.Empty }, null)%>
        </li>
        <li class="last">
            <%: Html.ActionLink("Register", "Register", "Account", new { Area = String.Empty }, null)%>
        </li>
<%
    }
%>
