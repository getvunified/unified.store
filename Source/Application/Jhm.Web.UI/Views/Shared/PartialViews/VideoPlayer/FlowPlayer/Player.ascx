﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.UI.Models.FlowPlayerViewModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Models" %>

<div style="width: 600px; height: 360px;" id="container">
    <img src='<%: (Model.StreamingIsActive ? Model.ThumbnailImagePath : Model.OfflineThumbnailImagePath)?? "/Content/images/not-streaming.jpg" %>' alt="JHM Cornerstone video player" />
</div>
<% if (Model.StreamingIsActive)
   {
       switch (Model.PlayerMode)
       {
           case FlowPlayerViewModel.PlayerModes.Mutibitrate_SD_Streaming: %>
                <script type="text/javascript">
                    $f('container', '/Content/media/flash/flowplayer.commercial-3.2.7.swf', {
                        key: '#@f6e30063d14332a76f4',
                        clip: {
                            url: 'FOT2011@17189',
                            live: true,
                            provider: 'rtmp'
                        },
                        plugins: {
                            rtmp: {
                                netConnectionUrl: 'rtmp://cp141681.live.edgefcs.net/live',
                                url: '/Content/media/flash/flowplayer.rtmp-3.2.3.swf',
                                subscribe: true
                            }
                        },
                        onError: function (errorCode, errorMessage) {
                            this.play('<%: String.IsNullOrEmpty(Model.ErrorImagePath) ? "/Content/images/player-error.jpg" : Model.ErrorImagePath  %>');
		                }
                    }).ipad();
		       </script>
           <%  break;
           case FlowPlayerViewModel.PlayerModes.Mutibitrate_HD_Streaming: %>
                <script type="text/javascript" language="JavaScript">
                    var player = $f
		                        (
			                        "container",
			                        {
			                            src: "/Content/media/flash/flowplayer.commercial-3.2.7.swf"
			                        },

			                        {
			                            key: '#@f6e30063d14332a76f4',
			                            // Apparently debug mode does not work on IE
			                            debug: true,
			                            log:
		     	                        	{
		     	                        	    level: 'warn'
		     	                        	},
			                            clip:
		     	                        	{
		     	                        	    live: true,
		     	                        	    autoPlay: true,
		     	                        	    provider: 'akamai',
		     	                        	    type: 'video',
		     	                        	    baseUrl: 'http://getvhdflash-f.akamaihd.net2/'
		     	                        	},
			                            plugins:
	 			                        {
	 			                            controls:
	        		                        	{
	        		                        	    url: '/Content/media/flash/flowplayer.controls-3.2.5.swf'
	        		                        	},
	 			                            akamai:
	           		                        	{
	           		                        	    url: '/Content/media/flash/AkamaiFlowPlugin.swf'
								                    , akamaiMediaType: 'akamai-hdn-multi-bitrate'

	           		                        	    //, tokenService:{url:escape(url here)}

	           		                        	    //, subClip:{clipBegin:400}
	           		                        	    //, useNetSession:true
	           		                        	    //, netsessionMode:'never'
	           		        	                    , mbrObject:
	           		                	            [
	           		                        	    	{ src: "GETV_HDFLASH_1_PRIMARY_400@36799", width: 626, height: 352, bitrate: "400" },
	           		                            		{ src: "GETV_HDFLASH_1_PRIMARY_800@36799", width: 626, height: 352, bitrate: "800" },
		           		                            	{ src: "GETV_HDFLASH_1_PRIMARY_1200@36799", width: 960, height: 542, bitrate: "1200" },
		           		                            	{ src: "GETV_HDFLASH_1_PRIMARY_2400@36799", width: 1280, height: 720, bitrate: "2400" }
	        	   		                            ]

		           		                            , mbrStartingBitrate: 2400000,
	           		                        	    onError: function (errorCode, errorMessage) {
	           		                        	        this.play('<%: String.IsNullOrEmpty(Model.ErrorImagePath) ? "/Content/images/player-error.jpg" : Model.ErrorImagePath  %>');
	           		                        	    }
	           		                        	    //, mbrStartingIndex:1

	           		                        	    /*, retryLive: true
	           		                        	    , retryInterval: 5
	           		                        	    , liveTimeout: 5
	           		                        	    , connectionAttemptInterval: 5*/

	           		                        	    //, connectAuthParams:'auth=connectionAuthToken'
	           		                        	    //, streamAuthParams:'auth=livestreamAuthToken'
	           		                        	    //, primaryToken:'1336218178_ee70588889d6e859ffcd58c49c3872be'
	           		                        	    //, akamaiMediaData:{}


	           		                        	    //, useNetSession:false
	           		                        	    //, enableNetSessionDiscovery:false
	           		                        	    //, enableAlternateServerMapping:false
	           		                        	    //, enableEndUserMapping:false
	           		                        	    //, playerVerificationChallenge:'somesalt'
	           		                        	    //, genericNetStreamProperty:{propertyName:'enableAlternateServerMapping', value:true}
	           		                        	}
	 			                        },
			                            onError: function (errorCode, errorMessage) {
			                                alert("error" + errorCode);
			                                this.play("/Content/images/player-error.jpg");
			                            }
			                            /*onLoad: function () {
			                            this.play("/Content/images/player-error.jpg");
			                            }*/
			                        }
		                        );
                        </script>
           <%  break;
           default:
               throw new ArgumentOutOfRangeException();
       } 
   } %>