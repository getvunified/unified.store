﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<div id="accountOptionsSideMenu" class="block">
    <h2 class="title">
        Account Menu</h2>
    <ul class="links">
        <% if (!Request.IsAuthenticated)
           { %>
                <%--NOT AUTHENTICATED--%>
                <li class="first"><a href="<%: Url.Action("LogOn", "Account")%>">Sign In</a> </li>
                <li class="last"><a href="<%: Url.Action("Register", "Account")%>">Register</a></li>
                <li class="last"><a href="<%: Url.Action("ForgotPassword", "Account")%>">Forgot Password</a></li>
                <li class="last"><a href="<%: Url.Action("ForgotUsername", "Account")%>">Forgot Username</a></li>                
        <% }
           else
           {%>
                <%--IS AUTHENTICATED--%>
                <li class="first"><a href="<%: Url.Action("MyAccount", "Account")%>">Account Information</a></li>
                <li><a href="<%: Url.Action("MyDownloads", "Account")%>">My Downloads</a></li>
                <%--<li><a href="<%: Url.Action("MyTransactions", "Account")%>">My Transactions</a></li>--%>
                <li><a href="<%: Url.Action("Preferences", "Account")%>">Preferences</a></li>
                <li><a class="parentLinkMenu" href="javascript:void(0);">Account Security Options</a>
                    <ul>
                        <li class="first"><a href="<%: Url.Action("ChangePassword", "Account")%>">Change Password</a></li>
                        <li><a href="<%: Url.Action("ChangeEmailAddress", "Account")%>">Change Email</a></li>
                        <%--<li class="last"><a href="<%: Url.Action("ChangeSecurityQuestion", "Account")%>">Change Question</a></li>--%>
                    </ul>
                </li>
                <li><a href="<%: Url.Action(MagicStringEliminator.CheckoutActions.Cart, "Checkout")%>">Cart</a></li>     
        <%
            }%>
    </ul>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        artSetVMenusActLinks();
        $('.block ul.links .parentLinkMenu').bind("click", function (e) {
            var spanElement = this.previousSibling;
            $(spanElement).toggleClass('closed').toggleClass('open');
            {
                $(spanElement).parent('li').toggleClass('').toggleClass('current');
            }
            $(spanElement).parents('li:first').children('ul:first').slideToggle('slow');
        });
    });

    function artSetVMenusActLinks() {
        var menuParent = document.getElementById("accountOptionsSideMenu");
        var menus = artGetElementsByClassName("links", menuParent, "ul");
        for (var i = 0; i < menus.length; i++) artSetVMenuActLink(menus[i]);
    }

    function artGetElementsByClassName(clsName, parentEle, tagName) {
        var elements = null;
        var found = [];
        var s = String.fromCharCode(92);
        var re = new RegExp('(?:^|' + s + 's+)' + clsName + '(?:$|' + s + 's+)');
        if (!parentEle) parentEle = document;
        if (!tagName) tagName = '*';
        elements = parentEle.getElementsByTagName(tagName);
        if (elements) {
            for (var i = 0; i < elements.length; ++i) {
                if (elements[i].className.search(re) != -1) {
                    found[found.length] = elements[i];
                }
            }
        }
        return found;
    }

    function artSetVMenuActLink(menu) {
        if (!menu) return;
        menu = (typeof menu == "object" ? menu : document.getElementById(menu));
        var aTags = menu.getElementsByTagName("a");
        var activeLinks = [];
        var activeLinksCount = 0;
        for (var i = 0; i < aTags.length; i++) {
            var fromUrl = window.location.href.replace("#", "");
            var fromLink = aTags[i].href.replace("#", "");
            if (artIsIncluded(fromLink, fromUrl)) {
                activeLinks[activeLinksCount] = aTags[i];
                activeLinksCount++;
                break;
            }
        }
        if (activeLinks == null || activeLinks.length == 0) return;

        for (var i = 0; i < activeLinks.length; i++) {
            var el = activeLinks[i];
            var liParent = el.parentNode;
            do {
                if (liParent.tagName.toLowerCase() == "li") {
                    liParent.className = "current";
                    if (liParent.firstChild != null) {
                        $(liParent).children('ul:first').slideToggle('slow');
                    }
                }
                liParent = liParent.parentNode;
            }
            while (liParent != null && liParent.className != "links");

            /*if (liParent == null) continue;        
            liParent.className = "current";
            if (liParent.firstChild == null) continue;
            $(liParent).children('ul:first').slideToggle('slow');*/
        }
    }

    function artIsIncluded(href1, href2) {
        if (href1 == null || href2 == null)
            return href1 == href2;
        if (href1.indexOf("?") == -1 || href1.split("?")[1] == "")
            return href1.split("?")[0] == href2.split("?")[0];
        if (href2.indexOf("?") == -1 || href2.split("?")[1] == "")
            return href1.replace("?", "") == href2.replace("?", "");
        if (href1.split("?")[0] != href2.split("?")[0])
            return false;
        var params = href1.split("?")[1];
        params = params.split("&");
        var i, par1, par2, nv;
        par1 = new Array();
        for (i in params) {
            if (typeof (params[i]) == "function")
                continue;
            nv = params[i].split("=");
            if (nv[0] != "FormFilter")
                par1[nv[0]] = nv[1];
        }
        params = href2.split("?")[1];
        params = params.split("&");
        par2 = new Array();
        for (i in params) {
            if (typeof (params[i]) == "function")
                continue;
            nv = params[i].split("=");
            if (nv[0] != "FormFilter")
                par2[nv[0]] = nv[1];
        }
        for (i in par1)
            if (par1[i] != par2[i])
                return false;
        return true;
    } 
</script>
