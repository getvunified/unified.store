﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<div class="content">
    <p>
        <a class="social-link" target="_blank" href="<%= Url.ConvertToSafeUrl("http://www.facebook.com/#!/johnhageeministries") %>">
            <img title="Facebook" alt="Facebook" src="~/Content/Images/logo-facebook.png" runat="server" />
        </a>
        <a class="social-link" target="_blank" href="<%= Url.ConvertToSafeUrl("http://twitter.com/#!/JohnHageeMin") %>">
            <img title="Twitter" alt="Twitter" src="~/Content/Images/logo-twitter.png" runat="server" />
        </a>
        <a class="social-link" target="_blank" href="<%= Url.ConvertToSafeUrl("http://www.youtube.com/user/HAGEEMINISTRIES?feature=mhum") %>">
            <img title="YouTube" alt="YouTube" src="~/Content/Images/logo-youtube.png" runat="server" />
        </a>
    </p>
</div>
