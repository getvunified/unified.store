﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<li class="first"><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog.RouteName)%>">
    Featured/Latest Releases</a></li>
<li><a class="parentLinkMenu" href="javascript:void(0);">Author/Speaker</a>
    <ul>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Author.RouteName, new { author =  "JOHNH" })%>">
            Pastor John Hagee</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Author.RouteName, new { author =  "MATTH" })%>">
            Pastor Matthew Hagee</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Author.RouteName, new { author =  "DIANAH" })%>">
            Diana Hagee</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Author.RouteName, new { author =  "GUEST" })%>">
            Guest Author/Speaker</a></li>
    </ul>
</li>
<li><a class="parentLinkMenu" href="javascript:void(0);">Browse JHM Store</a>
    <ul>
        <% foreach (var category in Category.GetAllSortedByDisplayName<Category>())
           {%>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Category.RouteName, new { categories = category.Value})%>">
            <%= category.DisplayName %>
        </a></li>
        <%} %>
    </ul>
</li>
<li><a class="parentLinkMenu" href="javascript:void(0);">Subject</a>
    <ul>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "AMERICA" })%>">
            America</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "COVENANT" })%>">
            Covenant</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "CUREVENTS" })%>">
            Current Events</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "EMOTIONS" })%>">
            Emotions</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "FAITH" })%>">
            Foundations of the Faith</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "INSPIRAT" })%>">
            Family</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "FINANCE" })%>">
            Finances and Career</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "HEALING" })%>">
            Healing</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "INSPIRAT" })%>">
            Inspiration</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "ISRAEL" })%>">
            Israel</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "LEADER" })%>">
            Leadership</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "LIVING" })%>">
            Daily Living</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "MARRIAGE" })%>">
            Marriage</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "PRAYER" })%>">
            Prayer and Fasting</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "PROPHECY" })%>">
            Prophecy</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "SALVATION" })%>">
            Salvation</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "SPIRITGROW" })%>">
            Spiritual Growth</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "SPIRITWAR" })%>">
            Spiritual Warfare</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Subject.RouteName, new { subjects =  "WORSHIP" })%>">
            Worship</a></li>
    </ul>
</li>
<li class="last"><a class="parentLinkMenu" href="javascript:void(0);">Media Format</a>
    <ul>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.Book.CodeSuffix })%>">
            Book</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.AudioFileDownload.CodeSuffix })%>">
            MP3 *NEW*</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.DigitalFileDownload.CodeSuffix })%>">
            Digital Files *NEW*</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.CD.CodeSuffix })%>">
            CD</a></li>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.DVD.CodeSuffix })%>">
            DVD</a></li>
        <%--<li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.VideoDownloadStandardDefinition.CodeSuffix })%>">
            Standard Definition Video Download</a></li> --%>
        <%--<li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.VideoDownloadHighDefinition.CodeSuffix })%>">
           High Definition Video Download</a></li>--%>
        <%--<li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.OnDemand.CodeSuffix })%>">
            On Demand</a></li>--%>
        <li><a href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, new { mediaTypes =  MediaType.Misc.CodeSuffix })%>">
            Miscellaneous</a></li>
    </ul>
</li>
<li><a class="parentLinkMenu" href="<%= Url.RouteUrl(MagicStringEliminator.Routes.Catalog_OnSale.RouteName, new { OnSale = "OnSale" })%>">On Sale</a></li>
<script type="text/javascript">

    $(document).ready(function () {
        artSetVMenusActLinks();
        $('.block ul.links .parentLinkMenu').bind("click", function (e) {
            $(this).toggleClass('closed').toggleClass('open');
            $(this).parent().find('ul').eq(0).toggle('fast');
        });
    });

    function artSetVMenusActLinks() {
        var menuParent = document.getElementById("categoriesSideMenu");
        var menus = artGetElementsByClassName("links", menuParent, "ul");
        for (var i = 0; i < menus.length; i++) artSetVMenuActLink(menus[i]);
    }

    function artGetElementsByClassName(clsName, parentEle, tagName) {
        var elements = null;
        var found = [];
        var s = String.fromCharCode(92);
        var re = new RegExp('(?:^|' + s + 's+)' + clsName + '(?:$|' + s + 's+)');
        if (!parentEle) parentEle = document;
        if (!tagName) tagName = '*';
        elements = parentEle.getElementsByTagName(tagName);
        if (elements) {
            for (var i = 0; i < elements.length; ++i) {
                if (elements[i].className.search(re) != -1) {
                    found[found.length] = elements[i];
                }
            }
        }
        return found;
    }

    function artSetVMenuActLink(menu) {
        if (!menu) return;
        menu = (typeof menu == "object" ? menu : document.getElementById(menu));
        var aTags = menu.getElementsByTagName("a");
        var activeLinks = [];
        var activeLinksCount = 0;
        for (var i = 0; i < aTags.length; i++) {
            var fromUrl = window.location.href.replace("#", "");
            var fromLink = aTags[i].href.replace("#", "");
            if (artIsIncluded(fromLink, fromUrl)) {
                activeLinks[activeLinksCount] = aTags[i];
                activeLinksCount++;
                break;
            }
        }
        if (activeLinks == null || activeLinks.length == 0) return;

        for (var i = 0; i < activeLinks.length; i++) {
            var el = activeLinks[i];
            var liParent = el.parentNode;
            do {
                if (liParent.tagName.toLowerCase() == "li") {
                    liParent.className = "current";
                    if (liParent.firstChild != null) {
                        $(liParent).children('ul:first').eq(0).stop(true, true).show("fast");
                    }
                }
                liParent = liParent.parentNode;
            }
            while (liParent != null && liParent.className != "links");

            /*if (liParent == null) continue;        
            liParent.className = "current";
            if (liParent.firstChild == null) continue;
            $(liParent).children('ul:first').slideToggle('slow');*/
        }
    }

    function artIsIncluded(href1, href2) {
        if (href1 == null || href2 == null)
            return href1 == href2;
        if (href1.indexOf("?") == -1 || href1.split("?")[1] == "")
            return href1.split("?")[0] == href2.split("?")[0];
        if (href2.indexOf("?") == -1 || href2.split("?")[1] == "")
            return href1.replace("?", "") == href2.replace("?", "");
        if (href1.split("?")[0] != href2.split("?")[0])
            return false;
        var params = href1.split("?")[1];
        params = params.split("&");
        var i, par1, par2, nv;
        par1 = new Array();
        for (i in params) {
            if (typeof (params[i]) == "function")
                continue;
            nv = params[i].split("=");
            if (nv[0] != "FormFilter")
                par1[nv[0]] = nv[1];
        }
        params = href2.split("?")[1];
        params = params.split("&");
        par2 = new Array();
        for (i in params) {
            if (typeof (params[i]) == "function")
                continue;
            nv = params[i].split("=");
            if (nv[0] != "FormFilter")
                par2[nv[0]] = nv[1];
        }
        for (i in par1)
            if (par1[i] != par2[i])
                return false;
        return true;
    } 
</script>
