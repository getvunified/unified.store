﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="content">
  <% using (Html.BeginForm("Index_ProductSearch", "Catalog"))
           { %>
    <label for="search-field">
        Search</label>
        <%: Html.TextBox("queryString", string.Empty, new { id = "search-field", size="25", tabindex="5" })%>
        <input class="search-button" type="submit" value="" tabindex="6" />
        <%} %>
</div>
