﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div id="search-form" class="block">
    <h2 class="title">
        Search All Products</h2>
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/SearchAllProducts.ascx"); %>
</div>
<div id="categoriesSideMenu" class="block">
    <h2 class="title">
        Menu</h2>
    <ul class="links">
        <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Catalog/Categories.ascx"); %>
    </ul>
</div>
<%--<div class="block">
    <h2 class="title">
        Join our social networks</h2>
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/SocialMedia/SocialMediaListing.ascx"); %>
</div>--%>

