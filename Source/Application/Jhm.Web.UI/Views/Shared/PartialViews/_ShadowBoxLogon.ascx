﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.UI.Models.LogOnModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>

<div class="logonDialogModal" title="Log On" style="display:none">
    <table style="height: 320px; font-size:12px;">
        <tr>
            <td>
                <h4>Returning User</h4>
                <div class="content">
                    <% using (Html.BeginForm("Logon", "Account", new { ReturnUrl = "/Home" }, FormMethod.Post, new { id = "sbLogon" }))
                       { %>
                        <p>
                            If you already have a JHM account please enter your username and password to continue.</p>
                        <br />
                        <%: Html.LabelFor(m => m.UserName) %><br/>
                        <%: Html.TextBoxFor(m => m.UserName, new {tabindex = "1", id = "ShadowBoxUserName"}) %>
                        <%: Html.ValidationMessageFor(m => m.UserName) %><span>&nbsp;</span>
                        <%: (Html.ActionLink("Forgot Username?", "ForgotUsername", "Account",null,new{style="color:#90550A;"})) %>
                        <br />
                        <br />
                        <%: Html.LabelFor(m => m.Password) %><br/>
                        <%: Html.PasswordFor(m => m.Password, new {tabindex = "2", id = "ShadowBoxPassword"}) %><span>&nbsp;</span>
                        <%: Html.ActionLink("Forgot Password?", "ForgotPassword", "Account",null,new{style="color:#90550A;"}) %>
                        <%: Html.ValidationMessageFor(m => m.Password) %>
                        <br />
                        <br />
                        <%: Html.CheckBoxFor(m => m.RememberMe, new {@checked = "checked"}) %>
                        <span>&nbsp;</span>
                        <%: Html.LabelFor(m => m.RememberMe) %>
                        <br />
                        <span id="ajaxLogonMessage" class="field-validation-error"></span>
                        <br />
                        <br />
                        <a class="easybutton" style="width: 100px; height: auto; cursor:pointer" onclick="doAjaxLogon(); return false;">Log On</a>
                  <% } %>
                </div>
            </td>
            <td style="background-color:rgb(222, 231, 234)">
                <div class="mysidebar right" style="margin-top: 0; height: 320px; margin-right: 0;padding-left:10px;">
                    <br />
                    <h4>
                        New to JHM?</h4>
                    <p>
                        <br />
                        If you don't have a JHM account please click the button below to create a new account.
                    </p>
                    <br />
                    
                    <br />
                    <br />
                    <%:Html.ActionLink( "Create New Account", "Register", "Account", null, new { @class = "easybutton", id="registerButton" }) %>
                </div>
                <br style="clear: left" />
            </td>
        </tr>
    </table>
</div>
<div id='easyButtonDialogModal_ForLogon'>Signing you in... Please wait. The system is currently processing your request.</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".logonDialogModal").dialog({
            autoOpen: false,
            bgiframe: true,
            modal: true,
            resizable: false,
            width: 620,
            height: 400
        });
        
        $('#easyButtonDialogModal_ForLogon').dialog({
            autoOpen: false,
            bgiframe: true,
            modal: true,
            resizable: false,
            width: 350,
            title: 'Signing you in...'
        });
    });

    function showLogonShadowBox(targetUrl) {
        jQuery(".logonDialogModal").dialog('open');
        jQuery("#sbLogon").attr("action", '<%:Url.Action("LogOn","Account")%>?ReturnUrl=' + escape(targetUrl));
        $("#registerButton").attr("href", '<%:Url.Action("Register","Account")%>?ReturnUrl=' + escape(targetUrl));
    }

    function showLogonShadowBox(targetUrl, openTargetInNewWindow) {
        jQuery(".logonDialogModal").dialog('open');
        var theForm = jQuery("#sbLogon");
        theForm.attr("action", '<%:Url.Action("LogOn","Account")%>?ReturnUrl=' + escape(targetUrl));
        $("#registerButton").attr("href", '<%:Url.Action("Register","Account")%>?ReturnUrl=' + escape(targetUrl));
        if (openTargetInNewWindow) {
            theForm.attr("target", "_new");
            $("#registerButton").attr("target", "_new");
        }
    }

</script>
<script type="text/javascript" language="JavaScript">
    
    function doAjaxLogon() {
        var timer = setTimeout(function () {
            $('#easyButtonDialogModal_ForLogon').dialog('open');
        },1000);
        var data = {
            Username: jQuery('#ShadowBoxUserName').val(),
            Password: jQuery('#ShadowBoxPassword').val(),
            RememberMe: jQuery('#RememberMe').is(':checked')
        };
        jQuery.ajax({
            url: '/Account/AjaxLogOn',
            type: "POST",
            data: data,
            success: function (result) {
                if (result.success) {
                    jQuery("#sbLogon").submit();
                } else {
                    clearTimeout(timer);
                    $('#easyButtonDialogModal_ForLogon').dialog('close');
                    jQuery("#ajaxLogonMessage").html('*' + result.message);
                }
            },
            error: function (req, status, error) {
                $('#easyButtonDialogModal_ForLogon').dialog('close');
                alert("An unexpected error ocurred. Please try again later: " + error);
            }
        });

    }
</script>