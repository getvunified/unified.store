﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.LogOnModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Log On
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#Password').keyup(function(event) {
                if (event.keyCode == 13) {
                    jQuery("form").submit();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { Class = "login-form" }))
                   { %>
                    <%: Html.Hidden("returnUrl", (ViewData["ReturnUrl"] ?? String.Empty).ToString()) %>
                    <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                    <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                    <%: Html.ValidationSummary(false, "Login was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div>
                    <h2>Log On <span>(Please enter your username and password.)</span></h2>
                    <hr />
                    <div class="login-username">
                        <strong style="padding-left:61px;"><%: Html.LabelFor(m => m.UserName) %></strong>
                        <%: Html.TextBoxFor(m => m.UserName) %>
                        <%: Html.ActionLink("Forgot Username?", "ForgotUsername", null, new { @title = "Click here if you forgot your username" })%>
                    </div>
                    <div class="login-password">
                        <strong><%: Html.LabelFor(m => m.Password) %></strong>
                        <%: Html.PasswordFor(m => m.Password) %>
                        <%: Html.ActionLink("Forgot Password?","ForgotPassword",null,new{@title="Click here if you forgot your password"}) %>
                    </div>
                    <div style="padding: 7px 0 0 205px;">
                        <%: Html.CheckBoxFor(m => m.RememberMe) %>
                        <%: Html.LabelFor(m => m.RememberMe) %>
                        <br />
                    </div>
                    <div style="padding: 7px 0 0 165px;">
                        <strong>New to <%= Model.IsDifferenceMediaPage? "Difference Media":"JHM" %>? </strong><%: Html.ActionLink("Click here to create a free account","Register",null,new{@title="Click here to create a new account"}) %>
                        <br/>
                        <br/>
                    </div>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Sign In", true, "Signing you in..."))%>
                        </div>
                        <br />
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
