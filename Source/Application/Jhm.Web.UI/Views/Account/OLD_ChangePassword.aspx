﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.ChangePasswordModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field{}
        .myForm-field label 
        {
            width:136px; display:inline-block; text-align:right;
            font-weight:bold;
        }
        .myForm-field input,select
        {
            width:200px;
        }
        
        .field-validation-error{
            color: #900;
            font-size:12px;
        }
    </style>
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>
                    Change Password</h2>
                <p>
                    Use the form below to change your password.
                </p>
                <p>
                    New passwords are required to be a minimum of
                    <%: ViewData["PasswordLength"] %>
                    characters in length.
                </p>
                <% using (Html.BeginForm())
                   { %>
                    <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                    <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                    <%: Html.ValidationSummary(true, "Password change was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div>
                    <% if (Session["ResetToken"] == null)
                       { %>
                       <div class="myForm-field">
                            <%: Html.LabelFor(m => m.CurrentPassword) %>
                            <%: Html.PasswordFor(m => m.CurrentPassword) %>
                            <%: Html.ActionLink("Forgot your current password?", "RequestPasswordChange") %>
                            <%: Html.ValidationMessageFor(m => m.CurrentPassword) %>
                        </div>
                    <% } %>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.NewPassword) %>
                        <%: Html.PasswordFor(m => m.NewPassword) %>
                        <%: Html.ValidationMessageFor(m => m.NewPassword) %>
                    </div>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.ConfirmPassword) %>
                        <%: Html.PasswordFor(m => m.ConfirmPassword) %>
                        <%: Html.ValidationMessageFor(m => m.ConfirmPassword) %>
                    </div>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Change Password", true, "Updating your password..."))%>
                        </div>
                        <br />
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
