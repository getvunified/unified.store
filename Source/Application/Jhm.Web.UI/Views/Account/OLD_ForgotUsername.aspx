﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.ForgotUsernameModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Forgot Username
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field
        {
        }
        .myForm-field label
        {
            width: 136px;
            display: inline-block;
            text-align: right;
            font-weight: bold;
        }
        .myForm-field input, select
        {
            width: 200px;
        }
        
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>
                    Forgot Username?</h2>
                <% using (Html.BeginForm())
                   { %>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                <%: Html.ValidationSummary(true, "Username recovery was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div>
                    <%--Initial view: Email is prompted--%>
                    <% if (String.IsNullOrEmpty(Model.Email))
                       { %>
                        <p>
                            Use the form below to recover your username.
                        </p>
                        <p>
                            An email with your username will be sent to the email address registered in your
                            profile. If this is not the email address you registered with, you will not receive an email.
                        </p>
                        <div class="myForm-field">
                            <label for="Email">Enter your email:</label>
                            <%: Html.TextBoxFor(m => m.Email)%> <span class="field-validation-error">*</span>
                            <%: Html.ValidationMessageFor(m => m.Email) %>
                        </div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Recover Username", true, "Finding your account..."))%>
                            <br />
                            <br />
                        </div>
                    <% }
                       else // final view: Email sent notification
                       { %>
                       <br />
                       <div style="padding-left: 10px;">
                            An email with your username was sent to
                            <%: Model.Email %>. If this is not the email address you registered with, you will not receive an email.
                        </div>
                            <br />
                            <br />
                    <% } %>
                </div>
                <% } %>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
