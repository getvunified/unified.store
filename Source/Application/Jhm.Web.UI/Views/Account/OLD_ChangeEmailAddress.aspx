﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.ChangeEmailModel>" %>

<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Change Email Address
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field{}
        .myForm-field label 
        {
            width:136px; display:inline-block; text-align:right;
            font-weight:bold;
        }
        .myForm-field input,select
        {
            width:200px;
        }
        
        .field-validation-error{
            color: #900;
            font-size:12px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>
                    Change Email Address</h2>
                <p>
                    Use the form below to change your email address.
                </p>
                <p>
                    New emails are required to be confirmed before the email change can take effect.
                    So please make sure you can access the new email inbox before changing your current
                    email in our system.
                </p>
                <% using (Html.BeginForm())
                   { %>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                <%: Html.ValidationSummary(true, "Password change was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div class="compact-form">
                    <div class="myForm-field">
                        <label>Your current email:</label>
                        <span style="width: 288px; display: inline-block; text-align: left;">
                            <%: Html.TextBoxFor(m => m.CurrentEmail, new { disabled = "disabled", style = "border:none;" }) %>
                        </span>
                    </div>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.NewEmail)%>
                        <%: Html.TextBoxFor(m => m.NewEmail)%>
                        <%: Html.ValidationMessageFor(m => m.NewEmail)%>
                    </div>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.ConfirmEmail)%>
                        <%: Html.TextBoxFor(m => m.ConfirmEmail)%>
                        <%: Html.ValidationMessageFor(m => m.ConfirmEmail)%>
                    </div>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Change Email", true, "Updating your email..."))%>
                        </div>
                        <br />
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
