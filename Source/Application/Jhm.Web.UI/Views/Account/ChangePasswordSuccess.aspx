﻿<%@  Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CartViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>
<asp:Content ID="changePasswordSuccessContent" ContentPlaceHolderID="MainContent"
    runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>
                    Change Password</h2>
                <p>
                    Your password has been changed successfully.
                </p>
            </div>
        </div>
    </div>
</asp:Content>
