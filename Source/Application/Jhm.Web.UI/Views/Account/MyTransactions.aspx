﻿<%@ Page Title="" Language="C#" Inherits="CompanyCapableViewPage<Jhm.Web.UI.Models.UserTransactionsViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    My Transactions
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .donationAmount
        {
            text-align: right;
        }
    </style>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        function callGetTransactionByDocumentNumberAction(documentNumber) {
            jQuery('.donationInfo').remove();
            jQuery('.donationAmount').remove();

            var url = '<%=Url.Action("GetTransactionByDocumentNumber", "Account")%>';
            var donationsTable = jQuery('#donationsTableBody');
            donationsTable.empty();
            var subscriptionsTable = jQuery('#subscriptionsTableBody');
            subscriptionsTable.empty();
            var orderTable = jQuery('#ordersTableBody');
            orderTable.empty();

            jQuery('#CompanyAddress').html('<strong>John Hagee Ministries<strong/> <br/> <%: ClientCompany.AdditionalData[Company.DataKeys.LegalName] %> <br/> <%: ClientCompany.AdditionalData[Company.DataKeys.FullAddress] %> <br/> Tel: <%: ClientCompany.AdditionalData[Company.DataKeys.PhoneNumber] %>');
            <% if (!String.IsNullOrEmpty(ClientCompany.AdditionalData[Company.DataKeys.TaxId]))
               { %>
            jQuery('#taxId').html('<br/><br/><%: ClientCompany.AdditionalData[Company.DataKeys.TaxIdLabel] %> <%: ClientCompany.AdditionalData[Company.DataKeys.TaxId] %> <br/>');
            jQuery('#taxMessage').html('The Internal Revenue Code permits you to deduct the amount you gift to John Hagee Ministries that exceeds the value of materials received. We have provided goods and services consisting solely of intangible religious benefits, except as will be indicated in a premium amount. A good faith estimate of the value of any goods or services received has been included in the premium amount as required by the IRS.');
            <% } %>

            var documentNumberToAction = { documentNumber: documentNumber };
            jQuery.post(url, documentNumberToAction, function (result) {
                var container = jQuery('#transactionDetailPopup');
                if (typeof result != 'object') {
                    result = jQuery.parseJSON(result);
                }
                if (result.Result == "OK") {
                    var transaction = result.transactions;
                    var shippingCost = 0;
                    jQuery('#transactionId').html('<br/>Transaction Id:' + documentNumber);
                    jQuery('#transactionDate').html('<br/>Transaction Date:' + transaction.Date);
                    var buttons = jQuery('#transactionDetailPopup').dialog('option', 'buttons');

                    if (transaction.Donations == 0) {
                        container.find('#donationsTableHeader').hide();
                        container.find('#donationsContainer').hide();
                        donationsTable.append(jQuery('<tr>').append(jQuery('<td>').html("No Donations Available")));
                        jQuery(":button:contains('Print Donation Only')").hide();
                    }
                    else {
                        container.find('#donationsContainer').show();
                        container.find('#donationsTableHeader').show();
                        jQuery(transaction.Donations).each(function () {
                            donationsTable.append(jQuery('<tr>').addClass('donations')
                                .append(jQuery('<td>').addClass('donationInfo').html(this.ProjectCode))
                                .append(jQuery('<td>').addClass('donationAmount').html(this.Amount))
                            );
                        });
                        jQuery(":button:contains('Print Donation Only')").show();
                    }


                    if (transaction.Subscriptions == 0) {
                        container.find('#subscriptionsTableHeader').hide();
                        container.find('#subscriptionsContainer').hide();
                        subscriptionsTable.append(jQuery('<tr>').append(jQuery('<td>').html("No Subsctiptions Available")));
                    }
                    else {
                        container.find('#subscriptionsContainer').show();
                        container.find('#subscriptionsTableHeader').show();
                        jQuery(transaction.Subscriptions).each(function () {
                            subscriptionsTable.append(jQuery('<tr>').addClass('subscriptions')
                                .append(jQuery('<td>').addClass('donationInfo').html(this.Description))
                                .append(jQuery('<td>').addClass('donationAmount').html(this.Amount))
                            );
                        });
                    }

                    if (transaction.Orders == 0) {
                        container.find('#orderTableHeader').hide();
                        container.find('#productsContainer').hide();
                        orderTable.append(jQuery('<tr>').append(jQuery('<td>').html("No Orders Available")));
                    }
                    else {
                        container.find('#productsContainer').show();
                        container.find('#orderTableHeader').show();
                        jQuery(transaction.Orders).each(function () {
                            var order = this;
                            jQuery(this.Details).each(function () {
                                var orderSatus = "";
                                switch (this.Status) {
                                case "A":
                                    orderSatus = "Processed";
                                    break;
                                case "B":
                                    orderSatus = "Backordered";
                                    break;
                                case "C":
                                    orderSatus = "Cancelled";
                                    break;
                                }
                                orderTable.append(jQuery('<tr>').addClass('orderRows')
                                    .append(jQuery('<td>').html(this.ProductDescription))
                                    //.append(jQuery('<td>').html(orderSatus))
                                    .append(jQuery('<td>').html(this.Quantity))
                                    .append(jQuery('<td>').html(this.OrderAmount))
                                );
                            });
                            shippingCost += order.ShippingAmount;
                        });
                    }
                    jQuery('#shipping').html('<br/>Shipping Amount:&nbsp;&nbsp;' + shippingCost + '<br/>');
                    jQuery('#total').html('<b>Total for this order:&nbsp;&nbsp;&nbsp;' + transaction.OrderTotal + '&nbsp;<b/>');
                    jQuery('#transactionDetailPopup').dialog("option", "title", 'Transactions Detail');
                    jQuery('#transactionDetailPopup').dialog('open');
                }
            });
            jQuery(document).ready(function () {
                jQuery('#transactionDetailPopup').dialog({
                    autoOpen: false,
                    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 700,
                    buttons: {
                        'Print Transaction': function (printLink) {
                            printLink.parentNode = "hidden";
                            var w = window.open('', '', 'status=0,scrollbars=1,width=800,height=500');
                            w.document.open("text/html");
                            w.document.write("<div id='content'><div id='transactionDetailPopup'>" + document.getElementById('transactionDetailPopup').innerHTML + "</div></div>");
                            addStyleSheet(w, "/content/css/print-html-reset.css");
                            addStyleSheet(w, "/content/css/defaults.css");
                            addStyleSheet(w, "/content/css/print-layout-fixed.css");
                            addStyleSheet(w, "/content/css/print-pages.css");
                            addStyleSheet(w, "/content/css/print-ecommerce.css");
                            addStyleSheet(w, "/content/css/print-blocks.css");
                            addStyleSheet(w, "/content/css/print.css");
                            w.document.close();
                            w.print();
                            printLink.parentNode.style.visibility = "visible";
                        },

                        'Print Donation Only': function (printLink) {
                            printLink.parentNode = "hidden";
                            var w = window.open('', '', 'status=0,scrollbars=1,width=800,height=500');
                            w.document.open("text/html");
                            w.document.write("<div id='content'><div id='donationsPrintArea'>" + document.getElementById('donationsPrintArea').innerHTML + "</div></div>");
                            addStyleSheet(w, "/content/css/print-html-reset.css");
                            addStyleSheet(w, "/content/css/defaults.css");
                            addStyleSheet(w, "/content/css/print-layout-fixed.css");
                            addStyleSheet(w, "/content/css/print-pages.css");
                            addStyleSheet(w, "/content/css/print-ecommerce.css");
                            addStyleSheet(w, "/content/css/print-blocks.css");
                            addStyleSheet(w, "/content/css/print.css");
                            w.document.close();
                            w.print();
                            printLink.parentNode.style.visibility = "visible";
                        },
                        Cancel: function () {
                            jQuery(this).dialog('close');
                        }
                    }
                });
            });
        }
        jQuery(document).ready(function () {
            jQuery('#transactionsDetail').hide();
            
        });
        function addStyleSheet(w, cssFilePath) {
            var fileref = w.document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", cssFilePath);
            w.document.getElementsByTagName("head")[0].appendChild(fileref);
        }


    </script>

    <script runat="server" type="text/C#">
        private string LoadSelectedAddressPopupCallback(AddressViewModel address)
        {
            return string.Format("addressEditorPopUp({0}); return false;", address.ToJSON());
        }
    </script>
    <style type="text/css">
        a.clickable
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Account/_EditAddressPartialView.ascx", new AddressViewModel()); %>
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            
            <div class="tab-content">   
            <% if (Model.User.Profile.PrimaryAddress == null)
               { %>
                    
                        <h2>We noticed that you don't have an address on file, please click the button to continue.</h2>
                        <hr />
                        <div style="padding-left: 10px; padding-bottom: 5px;">
                            <a onclick="newAddressEditorPopUp(false);return true;" href="#" class="easybutton">Add New Address</a>
                        </div>
                    
            <% }
               else
               {%>
                <h2>Annual Tax Receipts</h2>
                <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <%: Html.ActionLink("2012 Annual Tax Receipt","AnnualTaxReceipt", "Account") %>
                    <br/><br/>
                    
                    <hr/>
                
                    <h2>Transactions History</h2>
                    <p>Please note, that it may take 24-48 hours for any new transactions to appear for newly registered accounts.</p>
                    <% if (Model.Transactions == null || !Model.Transactions.Any())
                       { %>
                        <h2>Sorry, you do not have any transactions at this time.</h2>
                    <% } %>

                    <table>
                    
                        
                            <% if (Model.Transactions != null && Model.Transactions.Any())
                               { %>
                                <thead>
                                    <tr>
                                        <td>
                                            Transaction Date
                                        </td>
                                       <%-- <td>
                                            Transaction Details
                                        </td>--%>
                                        <td>
                                            Transaction Amount
                                        </td>
                                    </tr>
                                </thead>
                            <tbody>
                            

                                <% foreach (var transaction in Model.Transactions)
                                   {
                                %>
                            <tr>
                                <td>
                                    <%: transaction.Date.ToShortDateString() %>
                                </td>
                               <%-- <td>
                                    <a id="link" style="background-image: none; width: 100%; cursor: pointer" onclick="callGetTransactionByDocumentNumberAction('<%: transaction.DocumentNumber %>')">
                                        Click here for details</a>
                                </td>--%>
                                <td>
                                    <%: transaction.TotalAmount.ToString("C") %>
                                </td>
                            </tr>
                            <% } %>
                                 </tbody>   
                               <% } %>
                          
                             
                    
                    </table>
                    <div id="transactionDetailPopup" style="display: none">
                        <%=Html.HiddenFor(model => model.Client.Company.DonorStudioEnvironment)%>

                        <%  if (Model.Transactions != null && Model.Transactions.Any())
                            {%>
                            <div id="donationsPrintArea">
                            <table with="500" id="ordersReceipt" class="standardtable">
                                <tr>
                                    <td>
                                        <span style="">
                                            <img src="/Content/images/Black-JHM-LOGO.jpg" />
                                        </span>
                                    </td>
                                    <td id="CompanyAddress" style="text-align: center">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <br />
                                        Account Number:
                                        <%:Model.User.AccountNumber%>
                                    </td>
                                    <td id="taxId" style="text-align: right">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <%:Model.User.Profile.Title %>
                                        <%:Model.User.Profile.FirstName%>
                                        <%:Model.User.Profile.LastName%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%:Model.User.Profile.PrimaryAddress.Address1 ?? String.Empty%>
                                        <br />
                                        <%:Model.User.Profile.PrimaryAddress.City ?? String.Empty%>,
                                        <%:Model.User.Profile.PrimaryAddress.StateCode ?? String.Empty%>
                                        <%:Model.User.Profile.PrimaryAddress.PostalCode ?? String.Empty%>
                                        <br />
                                        <%:Model.User.Profile.PrimaryAddress.Country ?? String.Empty%>
                                    </td>
                                </tr>
                                <br />
                                <tr>
                                    <td id="transactionDate">
                                    </td>
                                    <td id="transactionId" style="text-align: right">
                                    </td>
                                </tr>
                            </table>
                            <div id="donationsContainer" style="display:none;">
                                <h2 style="border-top: solid; border-top-color: black">
                                        Donations
                                    </h2>
                                <table with="500" id="donationDataTable" class="standardtable">
                                    <thead id="donationsTableHeader">
                                        <tr>
                                            <th>
                                                Donation Detail
                                            </th>
                                            <th class="donationAmount">
                                                Amount
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="donationsTableBody">
                                    </tbody>
                                    <tr valign="bottom">
                                        <td id="signature" style="text-align: right">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <% } %>
                        <br />
                        <div id="subscriptionsContainer" style="display:none;">
                            <h2 style="border-top: solid; border-top-color: black">
                                    Subscriptions
                                </h2>
                            <table with="500" id="subscriptionsDataTable" class="standardtable">
                                <thead id="subscriptionsTableHeader">
                                    <tr>
                                        <th>
                                            Subscription Detail
                                        </th>
                                        <th>
                                            Cost
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="subscriptionsTableBody">
                                </tbody>
                            </table>
                        </div>
                        <br />
                        <div id="productsContainer" style="display:none;">
                            <h2 style="border-top: solid; border-top-color: black">
                                Products
                            </h2>
                            <table with="500" id="ordersDataTable" class="standardtable">
                                <thead id="orderTableHeader">
                                    <tr>
                                        <th>
                                            Description
                                        </th>
                                        <%--<th>
                                            Status
                                        </th>--%>
                                        <th>
                                            Quantity
                                        </th>
                                        <th class="donationAmount">
                                            Amount
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="ordersTableBody">
                                </tbody>
                            </table>
                        </div>
                        <table>
                           <tr>
                                <td id="shipping" style="text-align: right">
                                </td>
                            </tr>
                            <tr>
                                <td id="total" style="text-align: right; border-width: 2px; border-style: solid;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             border-color: black">
                                </td>
                            </tr> 
                        </table>
                        <hr />
                        <div id="taxMessage" style="font-size: 9px">
                        </div>
                    </div>
                
            <%}%>
            </div>
        </div>
    </div>
        
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
