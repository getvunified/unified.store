﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Async="true"
    Inherits="CompanyCapableViewPage<Jhm.Web.Core.Models.DownloadsViewModel>" %>

<%@ Import Namespace="Jhm.Web.UI.Views" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FileDownloadManager
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="container_wrapper" style="text-align: center; margin-left: auto; margin-right: auto; width:900px; float:left;">
        <div class="container container_6 tableHeader">
            <div class="grid_1 productDescription" style="width: 20%;"><span style="text-align:center;">Product</span>
                <div class="headerGradient"></div>
            </div>
            <div class="grid_1 activeStatus" style="width: 20%;" ><span style="text-align:center;">Status</span>
                <div class="headerGradient"></div>
            </div>
            <div class="grid_1" style="width: 20%;"><span style="text-align:center;">Attempts Left</span>
                <div class="headerGradient"></div>
            </div>
            <div class="grid_1" style="width: 20%;"><span style="text-align:center;">Expires In:</span>
                <div class="headerGradient"></div>
            </div>
            <%--<div class="grid_1 downloadLink" style="width: 28%;"><span style="text-align:center;">Download Link</span>
                <div class="headerGradient"></div>
            </div>--%>
            <div class="grid_1 fileSize" style="width: 20%;" ><span style="text-align:center;">File Size</span>
                <div class="headerGradient"></div>
            </div>
        </div>
        <div class="bodyInfo container container_6" style="text-align: center;">
        <% var download = Model.Download;
           var isIosDevice = false;
           var userAgent = Request.UserAgent.ToLower();
           if (userAgent.Contains("iphone") || userAgent.Contains("ipad") ||
                                userAgent.Contains("ipod"))
           {
               isIosDevice = true;
           }
           %>
        <div class="grid_1 productDescription" style="width: 20%;">
            <span style="text-align:center;">
                <%: Model.StockItems[download.Id].StockItem.ProductTitle %> <br /> <%: Model.StockItems[download.Id].StockItem.MediaType.DisplayName %>
            </span>
            <div class="gradient"></div>
        </div>
        <div class="grid_1 activeStatus" style="width: 20%;">
            <span style="text-align:center;">
                <%:download.IsActive() ? "Active" : "Expired" %>
            </span>
            <div class="gradient"></div>
        </div>
        <div class="grid_1" style="width: 20%;">
            
            <span id="attemptsLeft" style="<%if(!download.IsActive() || download.DownloadAttemptsRemaining == 1){%> color: red; <%}%>" attemptsLeft="<%: download.DownloadAttemptsRemaining %>"><%: download.DownloadAttemptsRemaining %> Left</span>
            <%--<div id="progressbarWrapper" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" style="height: 20px;">
                <div style="height: 100%;" id="progressbar" class="ui-progressbar-value"></div>
            </div>--%>
            <div class="gradient"></div>
        </div>
        <div class="grid_1" style="width: 20%;">
            <span id="expirationDate" expiration="<%: (download.ExpirationDate - DateTime.Today).Days %>"
                    creation="<%: (download.ExpirationDate - download.CreatedDate).Days %>">
                    <% if ((download.ExpirationDate - DateTime.Today).Days == 0)
                       {%> Last Day <% }
                       else
                       {%> <%: (download.ExpirationDate - DateTime.Today).Days %> Days<% }%>
            </span>
            <div id="progressbarWrapperExpiration" class="ui-progressbar-expiration ui-widget ui-widget-content ui-corner-all" style="height: 20px;">
                <div div style="height: 100%;" id="progressbarExpiration" class="ui-progressbar-value-expiration"></div>
            </div>
            <div class="gradient"></div>
        </div>
        <%--<div class="grid_1 downloadLink" style="width: 28%;">
            <span id="downloadInProgress" style="display: none;">Please Wait For Download To Complete</span>
            <%if(download.IsActive())
              {%>
            <span>
                <a href="#" id="downloadLink"
                 <% if(!isIosDevice){ %> style='width:auto; background:none; color:Black; text-decoration:underline;'
                onclick="DownloadOnClick('<%: String.Format("http://{0}/DigitalMedia/Download/{1}",
                                       Request.ServerVariables["HTTP_HOST"], download.Id) %>', '<%: download.Id %>', '<%: Request.ServerVariables["HTTP_HOST"] %>')" >
                Click here to download this purchase <% } else{ %> style='width:auto; background:none; color:Black; text-decoration:none;'> Please Download Your Content <br/> From A Non-Mobile Device <% } %>
                </a>
            </span>
                <% }else
              { %>
                    <span>Expired</span>
                    <% } %>
            <div class="gradient"></div>
        </div>--%>
        <div class="grid_1 fileSize" style="width: 20%;">
            <span style="text-align:center;">
                 <%: Math.Round(Model.StockItems[download.Id].StockItem.FileLength, 1, MidpointRounding.ToEven) %> MB
            </span>
            <div class="gradient"></div>
        </div>
        </div>
        <div class="product-display grid_6">
            <div class="grid_2 product_image" style="margin-top: 10px;">
                <img src="<%: (Model.StockItems[download.Id].ImagePath??string.Empty).Replace("http://","https://") %>" alt="<%: Model.StockItems[download.Id].StockItem.ProductTitle %> Display Image" />
            </div>
            <div class="grid_2" >
                <span class="product_header">
                   Product Description:
                </span>
                <span class="product_description">
                    <%= Model.StockItems[download.Id].StockItem.Description %>
                </span>
            </div>
            <div class="grid_2" style="margin-left: 25%; margin-right: 25%; width: 450px;">
                 <%if(download.IsActive() && !isIosDevice)
              {%>
            <span>
                <a href="javascript:void(0);" id="downloadLink" 
                 <% if(!isIosDevice){ %> style='width:auto; background:none; color:Black; text-decoration:underline;'
                onclick="DownloadOnClick('<%: String.Format("https://{0}/DigitalMedia/Download/{1}",
                                       Request.ServerVariables["HTTP_HOST"], download.Id) %>', '<%: download.Id %>', '<%: Request.ServerVariables["HTTP_HOST"] %>')" >
                    <img class="downloadButton" alt="Download Button"/>
                <% } else{ %> style='width:auto; background:none; color:Black; text-decoration:none;'>
                    <% } %>
                </a>
            </span>
                <% }else
              { %>
                    <img class="downloadButton" alt="Download Button"/>
                    <% } %>
                <img id="downloadInProgress" src="<%: Url.Content("~/Content/images/Downloading.png") %>" alt="Download In Progress" style="display: none;"/>
            </div>
            <div class="gradient"></div>
        </div>
        
    </div>
    
    <table style="width: 900px; text-align: center;">
        <tr style="display: none;" class="downloadBar">
            <td colspan="3" id="progress">
                <h4 id="dlp">Download Progress</h4>
                    <div id="outerProgress" class="ui-progressbar-progress ui-widget ui-widget-content ui-corner-all" style="height: 22px;">
                        <div style="height: 100%;" id="innerProgress" class="ui-progressbar-value-progress"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <a href="<%: Url.Action("MyDownloads") %>">
                    <img class="returnToMyDownloads" alt="Return to My Downloads"/>
                </a>
            </td>
        </tr>
    </table>
    <br/>
    <br style="clear: both;" />
    <iframe src="" style="display: none;" id="frame"></iframe>

    <div class="sectionGradient"></div>
       
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
     <%    var download = Model.Download;
           var isIosDevice = false;
           var userAgent = Request.UserAgent.ToLower();
           if (userAgent.Contains("iphone") || userAgent.Contains("ipad") ||
                                userAgent.Contains("ipod"))
           {
               isIosDevice = true;
           }
           %>
    <link href="<%:Url.Content("~/Content/css/jquery.ui.progressbar.css") %>" type="text/css"/>
    <link href="<%:Url.Content("~/Content/css/DownloadsGridSixColumns.css") %>" type="text/css" rel="stylesheet"/>
    <script src="<%:Url.Content("~/Content/js/jquery-ui-1.7.2.custom.min.js") %>"></script>
    <script type="text/javascript">

        var checkStatus;
        var time = 0;

        function clearSetInterval() {
            
            clearInterval(checkStatus);
        }

        function bindReturnToMyDownloadsButton() {
            jQuery(".returnToMyDownloads").attr("src", "<%: Url.Content("~/Content/images/Back_My_Downloads_Active.png") %>")
                .hover(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Back_My_Downloads_Hover.png") %>");
                })
                .mousedown(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Back_My_Downloads_Pressed.png") %>");
                }).mouseleave(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Back_My_Downloads_Active.png") %>");
                }).mouseup(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Back_My_Downloads_Active.png") %>");
                });
        }

        function bindDownloadButton()
        {
            jQuery(".downloadButton").attr("src", "<%: Url.Content("~/Content/images/Download_Now_Active.png") %>")
                .hover(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Download_Now_Hover.png") %>");
                })
                .mousedown(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Download_Now_Press.png") %>");
                }).mouseleave(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Download_Now_Active.png") %>");
                }).mouseup(function () {
                    jQuery(this).attr("src", "<%: Url.Content("~/Content/images/Download_Now_Active.png") %>");
                });
        }

        function unbindDownloadButton() {
            jQuery(".downloadButton").unbind('hover').unbind('mousedown').unbind('mouseleave').unbind('mouseup');
        }

        function taskComplete(data, mediaId) {
            clearSetInterval();

            jQuery.ajax({
                url: '<%: Url.Action("UpdateDownloadTime", "DigitalMedia") %>',
                type: 'POST',
                data: { mediaId: mediaId, seconds: time },
                success: function (data) {
                    if (data.status == "complete") {
                        time = 0;
                        jQuery("#dlp").html("Your Download is Complete");

                        jQuery("#outerProgress").hide();
                        jQuery("#downloadInProgress").hide();
                        jQuery("#downloadLink").show();

                        bindDownloadButton();

                        jQuery("#attemptsLeft").html(data.attempts + " Left");
                        
                        jQuery("#highlightCell").effect("highlight", {}, 3000);
                        jQuery("#progress").effect("highlight", {}, 3000);

                        setTimeout(function() {
                            jQuery("#progress").hide();
                        }, 3500);

                        if (data.attempts == 0) {
                            setTimeout(function() {
                                document.location.reload(true);
                            }, 2000);
                            
                        }
                        
                    } else if (data.status == "error") {
                        taskFailed("We're sorry, but there was an error downloading your file, please try again later.");
                    }
                },
                error: function (jqHXR, textStatus, errorThrown) {
                    taskFailed("We're sorry, but there was an error downloading your file, please try again later.");
                }
            });
        }

        function taskFailed(message) {
            jQuery("#dlp").html(message).css("color", "red");

            jQuery("#outerProgress").hide();
            jQuery("#downloadInProgress").hide();
            jQuery("#downloadLink").show();

            jQuery("#progress").effect("highlight", {}, 3000);

            bindDownloadButton();
        }

        function CheckTask(mediaId) {
            jQuery.ajax({
                url: '<%: Url.Action("UpdateDownloadAttempts", "DigitalMedia") %>',
                type: 'POST',
                data: { mediaId: mediaId, seconds: time },
                success: function (data) {
                    if (data.status == "complete")
                    {
                        taskComplete(data, mediaId);
                    } else if (data.status == "error") {
                        taskFailed("We're sorry, but there was an error downloading your file, please try again later.");
                    } else {
                        setTimeout(function () { CheckTask(mediaId); }, 1000);
                    }
                },
                error: function (jqHXR, textStatus, errorThrown) {
                    setTimeout(function () { CheckTask(mediaId); }, 1000);
                }
            });
  
        }

        function DownloadOnClick(downloadUrl, mediaId)
        {
        	window.location = downloadUrl;
            //jQuery("#frame").attr("src", downloadUrl);
                
            unbindDownloadButton();
            
            jQuery("#dlp").html("Your Download is in Progress");

            jQuery("#downloadLink").hide();
            jQuery("#downloadInProgress").show();
            jQuery("#outerProgress").show();
            jQuery(".downloadBar").show();

            jQuery(".downloadStatus").attr("title", "inProgress");
            
            checkStatus = setInterval(function() {
                time++;
            }, 1000);

            CheckTask(mediaId);
        }

        jQuery(document).ready(function () {
            bindReturnToMyDownloadsButton();
            <% if(!download.IsActive())
               {%>
                    jQuery(".downloadButton").attr("src", "<%: Url.Content("~/Content/images/Expired.png") %>");
            <% }
               else if(isIosDevice)
               {%>
                    jQuery(".downloadButton").attr("src", "<%: Url.Content("~/Content/images/NoIOS.png") %>");
            <% }
               else
               {%>
                    bindDownloadButton();
            <% }%>
  
            jQuery(".ui-progressbar").css({ 'background': 'url(/Content/images/lt-blue-40x100.jpg) #ffffff repeat-x 50% 50%' });
            jQuery(".ui-progressbar-value").css({ 'background': 'url(/Content/images/dustyblue-1x100.jpg) #cccccc repeat-x 50% 50%' });

            jQuery(".ui-progressbar-expiration").css({ 'background': 'url(/Content/images/lt-blue-40x100.jpg) #ffffff repeat-x 50% 50%' });
            jQuery(".ui-progressbar-value-expiration").css({ 'background': 'url(/Content/images/dustyblue-1x100.jpg) #cccccc repeat-x 50% 50%' });
            
            jQuery("#innerProgress").css({ 'background': 'url(/Content/images/pbar-ani.gif)' }).css({'height': '100%'});
            jQuery(".ui-progressbar-value-progress").css({ 'background': 'url(/Content/images/pbar-ani.gif)' }).css({ 'height': '100%' });

            
            var timeToExpire = jQuery("#expirationDate").attr("expiration");
            var dateCreated = jQuery("#expirationDate").attr("creation");
           
            jQuery("#progressbarExpiration").progressbar({
                value: timeToExpire * (100 / dateCreated)
            });

            jQuery("#innerProgress").progressbar({ value: 100 });

            jQuery(".row > div:odd").css("background-color", "#ECE6EB");
            jQuery(".row > div:even").css("background-color", "#FFFFFF");
        });

    </script>
    <style type="text/css">
        
        .ui-progressbar-value { background-image: url(/Content/images/lt-blue-40x100.jpg); }
        .ui-progressbar-value-expiration { background-image: url(/Content/images/lt-blue-40x100.jpg); }

        .ui-progressbar-progress { background-image: url(/Content/images/pbar-ani.gif);height: 100%; }
        #innerProgress div { background-image: url(/Content/images/pbar-ani.gif);height: 100%; }

        .container_wrapper {
            position: relative;
            height: 100%;
            margin-left: 4%;
            margin-top: 10px;
            background-color: rgba(255, 255, 255, 0.7);
        }

        .sectionGradient {
            position: absolute;
            top: 0px;
            left: 0px;
            /*background-color: #ECBABB !Important;*/
            padding-bottom: 20px !Important;
            opacity: 0.75;
            width: 100%;
            height: 100%;
            z-index: -1;
        }

        .product_description {
            width: 500px;

            /*background-color: rgba(255,255,255, 0.7);*/
            padding: 10px;
            display: block;
            margin-top: 5px;
            margin-left: 50px;

        }

        .product_header {
            font-weight: bolder;
            width: 500px;

            /*background-color: rgba(255,255,255, 0.7);*/
            padding: 5px;
            display: block;
            margin-top: 10px;
            margin-left: 50px;
            
        }

        .product_image {
            padding: 5px;
            background-color: white;

        }

        .container {
            position: relative;
            height: 100%;

        }

        .container_6 {
            width: 100%;
            margin-left: 0;
            margin-right: 0;
            color: black;
        }

        .product-display {
            width: 99.5%; 
            min-height: 250px; 
             
            position: relative; 
            margin-left: 0; 
            margin-top: 1px;
        }

        .product-display > .grid_6 {
            margin-left: 0;
        }

        .product-display > .gradient {
            position: absolute;
            top: 0px;
            left: 0px;
            margin: 1px;
            height: 100%;
            width: 100%;
            background-color: white;
            opacity:0.6;
            filter:alpha(opacity=60);
            z-index: -1;
        }

        .tableHeader > div > span {
            font-weight: bold;
        }

        .activeStatus, .fileSize {
            width: 19.25% !Important;
        }
        

        #content {
            width: 100% !Important;
        }

        .grid_1 {
            margin: 1px;
            padding: 5px 0px 5px 0px;
            height: 100%;
        }
        
        .bodyInfo > .grid_1 > span {
            position: relative;
            display: block;
            margin-top: 20px;
            font-weight: bold !Important;
            text-align: center;
            width: 100%;
            font-size: 11px;
            text-wrap: normal;
        }

        #progressbarWrapper, #progressbarWrapperExpiration {
            margin: 0px 5px 0px 5px;
        }

        .tableHeader > .grid_1 > .headerGradient {
            top: 0px;
            left: 0px;

            padding-bottom: 5px;

            border-bottom: thin;
            border-bottom-style: solid;
            border-bottom-color: darkcyan;

            position: absolute;
            margin: 1px;
            width: 100%;
            background: url(/Content/images/grid_background_active.jpg) repeat-x;
            color: #000000;
            height: 100%;
/*            opacity:0.95;
            filter:alpha(opacity=95);*/
            z-index: -1;
        }

        .bodyInfo > .grid_1 > .gradient {
            position: absolute;
            top: 0px;
            left: 0px;
            margin: 1px;
            min-height: 70px !Important;
            background: url(/Content/images/grid_background_info_active.png) repeat-x transparent;
            color: #000000;
            
            width: 100%;
            opacity:0.4;
            filter:alpha(opacity=40);
            z-index: -1;
        }

        .bodyInfo > .grid_1 > span
        {
            position: relative;
            padding: 0px 5px 0px 5px;
            opacity:1;
            filter:alpha(opacity=100);

        }

        .bodyInfo > .grid_1 > a
        {
            font-size: 11px;
        }

        .sixth > span {
            padding-top: 5px;
            vertical-align: text-bottom !Important;
            font-size: larger;
            display: block;
        }

    </style>
</asp:Content>