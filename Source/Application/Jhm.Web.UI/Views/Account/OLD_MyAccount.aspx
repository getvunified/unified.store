﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.ProfileViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    My Account
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        //        $(document).ready(function () {
        //            populateStateOrProvince('Country', 'State', '<%= Model.State %>');
        //            $(".label-textbox").each(function (index) {
        //                var $this = $(this);
        //                if (this.value == this.getAttribute('label')) {
        //                    $this.val($this.attr("label"));
        //                    $this.addClass("initial");
        //                }
        //                $this.focusin(function () {
        //                    if (this.value == this.getAttribute('label')) {
        //                        this.value = '';
        //                        $(this).removeClass("initial");
        //                        $(this).addClass("in-use");
        //                    }
        //                });
        //                $this.focusout(function () {
        //                    if (this.value == '') {
        //                        this.value = this.getAttribute('label');
        //                        $(this).removeClass("in-use");
        //                        $(this).addClass("initial");
        //                    }
        //                });
        //            });
        //        });
        //        function populateStateOrProvince(ddCountryId, ddStateId, selectedState) {
        //            set_city_state(document.getElementById(ddCountryId), ddStateId);
        //            $('#' + ddStateId).val(selectedState);
        //        }

      
      
    </script>
    <script runat="server" type="text/C#">
        private string LoadSelectedAddressPopupCallback(AddressViewModel address)
        {
            return string.Format("addressEditorPopUp({0}); return false;", address.ToJSON());
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <% Html.RenderPartial("~/Views/Account/_EditAddressPartialView.ascx", new AddressViewModel()); %>
            <% using (Html.BeginForm("MyAccount","Account"))
               {%>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <%: Html.ValidationSummary(true) %>
            <div class="tab-content">
                <div class="account user-divider">
                    <%: Html.ValidationSummary(false, "Account creation was unsuccessful. Please correct the errors and try again.", new { @class = "messages error"})%>
                    <h2>
                        My Account</h2>
                    <div class="user-form-item">
                        <strong><%: Html.LabelFor(model => model.UserName) %>:</strong> <span style="width: 288px;
                                display: inline-block; text-align: left;">
                                <%: Html.DisplayTextFor(model => model.UserName) %></span>
                    </div>
                    <div class="user-form-item">
                        <strong><%: Html.LabelFor(model => model.Email) %>:</strong> 
                        <span style="width: 288px; display: inline-block; text-align: left;">
                        <%: Html.DisplayTextFor(model => model.Email) %> <%: Html.ActionLink("Change Email","ChangeEmailAddress") %>
                            <%: Html.HiddenFor(model => model.Email) %>
                        </span>
                    </div>
                        <div class="user-form-item">
                        <strong>Your Password:</strong> 
                        <span style="width: 288px; display: inline-block; text-align: left;">
                        <%: Html.ActionLink("Change Password","ChangePassword") %>
                        <%: Html.HiddenFor(model => model.Email) %>
                        </span>
                    </div>
                </div>
                <div class="personal-info user-divider">
                    <h2>
                        Personal Information</h2>
                    <p class="user-description">
                        Enter your billing address and information here.</p>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.Title) %><span class="form-required">*</span></strong>
                        <%: Html.DropDownListFor(model => model.Title, new SelectList(new[]{"","Mr.","Mrs.","Ms.","Pastor","Rev."}))%>
                        <span style="width: 222px; display: inline-block;">&nbsp;</span>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.FirstName) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(model => model.FirstName,null, new { readOnly = "true"}) %>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.LastName) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(model => model.LastName,null, new { readOnly = "true"}) %>
                    </div>
                    <div style="padding-left: 86px; margin: 6px;">
                        <strong>
                            <%: Html.LabelFor(model => model.Gender) %></strong>
                        <div style="width: 200px; display: inline-block;">
                            <%= Html.RadioButtonListFor(model => model.GenderRadioButtonList, RepeatDirectionTypes.Horizontal)%>
                        </div>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.Company) %></strong>
                        <%: Html.TextBoxFor(model => model.Company) %>
                    </div>
                    <%--<div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.Occupation) %></strong>
                        <%: Html.TextBoxFor(model => model.Occupation)%>
                    </div>--%>
                    <div class="user-form-item">
                        <strong>Phone Number<span class="form-required">*</span>:</strong>
                        <%: Html.DropDownListFor(model => model.PhoneCountryCode, new SelectList(Country.GetAll<Country>().Select(x => x.PhoneCode).Distinct().OrderBy(x => x),"+1"), new { @style = "width:60px" })%>
                        <%: Html.TextBoxFor(m => m.PhoneAreaCode, new { @style = "width:60px", @size = "4", @maxlength = "4", @class = "label-textbox", @label = "Area code" })%>
                        <%: Html.TextBoxFor(m => m.PhoneNumber, new { @style = "width:156px", @size = "10", @maxlength = "10", @class = "label-textbox", @label = "Phone number" })%>
                    </div>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Submit", true, "Updating your account..."))%>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
            <div class="tab-content">
                <div class="personal-info user-divider">
            <% if (Model.Addresses.Any()){ %>
            
                    <h2>
                        My Billing Addresses</h2>
                    <table class="standardtable">
                        <thead>
                            <tr>
                                <th style="width: 80px;">
                                    Action
                                </th>
                                <th style="width: 400px;" scope="col">
                                    Address 
                                </th>
                                <th style="width: 80px;">
                                    Primary Address
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                var orderByPrimary = Model.Addresses.OrderByDescending(address => address.DsIsPrimary);
                                foreach (var address in orderByPrimary)
                               {%>
                            <tr>
                                <td>
                                    <a style="background-image: none; display: inline" href="javascript: void(0);" onclick='<%=LoadSelectedAddressPopupCallback(AddressViewModel.CreateFrom(address))%>'>
                                        <img src="/Content/Images/Icons/pencil.png" width="16" height="16" />
                                    </a>&nbsp;&nbsp;&nbsp;
                                    <% if (!address.DsIsPrimary)
                                       {%>
                                    <a id="deleteAddress" style="background-image: none; display: inline" onclick="return confirm('Are you sure you want to delete this address from your profile?');"
                                        href="<%= (Url.Action("DeleteAddressAccount", "Account", new {address.RecordId, address.A02RecordId, address.DSAddressId, address.DsIsPrimary}) )%>">
                                        <img src="/Content/Images/Icons/delete.png" width="16" height="16" />
                                    </a>
                                    <% } %>
                                </td>
                                <td>
                                    <%:address.Address1%>
                                    <br/>
                                    <%:address.City%>
                                    <%:address.StateCode%>
                                    <%:address.PostalCode%>
                                    <%:address.Country%>
                                </td>
                                <td>
                                    <% if (address.DsIsPrimary)
                                       {%>
                                           <img src="/Content/Images/Icons/ok.gif" width="16" height="16" />
                                       <%}
                                       else
                                       {%>
                                           <img src="/Content/Images/Icons/error_16.png" width="16" height="16" />
                                       <%}%>
                                    
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                    
               
            <% } %>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <a onclick="newAddressEditorPopUp(<%= Model.Addresses.Any().ToString().ToLower() %>);return true;" href="#" class="easybutton">Add New Address</a>
                        </div>
                    </div>
                 </div>
            </div>
                
            <div class="tab-content">
                <div class="personal-info user-divider">
            <% if (Model.CreditCards.Any())
               { %>
            
                    <h2>
                        My Credit Cards</h2>
                    <table class="standardtable">
                        <thead>
                            <tr>
                                <th style="width: 80px;">
                                    Delete
                                </th>
                                <th style="width: 80px;" scope="col">
                                    Type 
                                </th>
                                <th style="width: 400px;">
                                    Number
                                </th>
                                <th style="width: 80px;">
                                    Expires
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var creditCard in Model.CreditCards)
                               {%>
                            <tr>
                                <td>
                                    <a id="A1" style="background-image: none; display: inline" onclick="return confirm('Are you sure you want to delete this credit card?');"
                                        href="<%= (Url.Action("DeleteCreditCard", "Account", new {creditCard.Id}) )%>">
                                        <img src="/Content/Images/Icons/delete.png" width="16" height="16" />
                                    </a>
                                </td>
                                <td>
                                    <img alt="<%: creditCard.CardType %>" title="<%: creditCard.CardType %>" src="<%: Url.Content("/Content/images/credit-{0}.gif".FormatWith(creditCard.CardType.Code)) %>" />
                                </td>
                                <td>
                                    ******************<%:creditCard.EndingWith%>
                                </td>
                                <td>
                                    <%:creditCard.ExpirationMonth %> / <%:creditCard.ExpirationYear %>
                                </td>
                            </tr>
                            <%} %>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                    
               
            <% } %>
                    
                 </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
