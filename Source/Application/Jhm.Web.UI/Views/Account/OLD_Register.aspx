﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.UI.Models.RegisterModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Register
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] { "PhoneCode" })%>
    <script type="text/javascript">
        $(document).ready(function () {
            populateStateOrProvince('Country', 'State', '<%= Model.State %>');
            $(".label-textbox").each(function (index) {
                var $this = $(this);
                if ($this.val() == '' || this.value == this.getAttribute('label')) {
                    $this.val($this.attr("label"));
                    $this.addClass("initial");
                }
                else {
                    $(this).addClass("in-use");
                }
                $this.focusin(function () {
                    if (this.value == this.getAttribute('label')) {
                        this.value = '';
                        $(this).removeClass("initial");
                        $(this).addClass("in-use");
                    }
                });
                $this.focusout(function () {
                    if (this.value == '') {
                        this.value = this.getAttribute('label');
                        $(this).removeClass("in-use");
                        $(this).addClass("initial");
                    }
                });
            });
        });
        function populateStateOrProvince(ddCountryId, ddStateId, selectedState) {
            set_city_state(document.getElementById(ddCountryId), ddStateId);
            if (selectedState == null || selectedState == '') {
                $('#' + ddStateId).get(0).selectedIndex = 0;
            }
            else {
                $('#' + ddStateId).val(selectedState);
            }
        }
        /*function highlighGender(ddTitle) {            
        var title = $(ddTitle).val();
        var shouldRunEffect = false;
        var radioButtonId = null;
        if (title == 'Mr.' && !$('#GenderRadioButtonList_Male').attr("checked")) {
        shouldRunEffect = true;
        radioButtonId = '#GenderRadioButtonList_Male';
        }
        else if ((title == 'Mrs.' || title == 'Ms.')  && !$('#GenderRadioButtonList_Female').attr("checked") ) {
        shouldRunEffect = true;
        radioButtonId = '#GenderRadioButtonList_Female';
        }

        if (shouldRunEffect) {
        $('#genderInput').effect("highlight", {}, 2000, function () { highlighGenderAgain(radioButtonId); });
        }
        }

        function highlighGenderAgain(radioButtonId) {
        $('#genderInput').effect("highlight", {}, 2000);
        $(radioButtonId).attr("checked", "checked");
        $(radioButtonId).effect("pulsate", {}, 200);
        }*/
    </script>
        <style type="text/css">
            .user-form-item label{
                text-align: left !Important;
            }
            .user-from-item input {
                text-align: right !Important;
            }
        </style>

</asp:Content>
<asp:Content ID="registerContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <% using (Html.BeginForm())
               { %>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <div class="tab-content">
                <div class="account user-divider">
                    <h2>
                        Account Information</h2>
                    <p class="login-email">
                        <strong>Create a New Account</strong> (Use the form below to create a new account.)
                        <br /><br/>
                        <% if (!Model.IsDifferenceMediaPage)
                           { %>
                        <strong><a target="_blank" href="http://www.youtube.com/watch?v=nJYIc1h4Umk">Click here to watch a video on how to register an account.</a></strong>
                        <% } %>
                    </p>                    
                    <%
                        var htmlStr = Html.ValidationSummary(false, "Account creation was unsuccessful. Please correct the errors and try again.", new {@class = "messages error"});
                        var validation = htmlStr == null ? String.Empty : htmlStr.ToHtmlString();
                        if (!String.IsNullOrEmpty(validation))
                        {
                            validation = Regex.Replace(validation, "&gt;", ">");
                            validation = Regex.Replace(validation, "&lt;", "<");
                        }
                    %>
                    <%= validation %>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.UserName) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(m => m.UserName) %>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.Email) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(m => m.Email) %>
                    </div>
                    <div class="user-form-item">
                        <span style="font-size: .7em;">(Must be minimum of
                            <%: ViewData["PasswordLength"] %>
                            characters in length.) </span>
                        <br />
                        <strong>
                            <%: Html.LabelFor(m => m.Password) %><span class="form-required">*</span></strong>
                        <%: Html.PasswordFor(m => m.Password) %>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.ConfirmPassword) %><span class="form-required">*</span></strong>
                        <%: Html.PasswordFor(m => m.ConfirmPassword) %>
                    </div>
                    <%--<div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.PasswordQuestion) %><span class="form-required">*</span></strong>
                        <%=Html.DropDownListFor(m => m.PasswordQuestion, RegistrationPasswordQuestion.AllSortedByDisplayName().ToMVCSelectList().AddNoSelection(), new { @style = "width:292px" })%>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.PasswordAnswer) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(m => m.PasswordAnswer)%>
                    </div>--%>
                </div>
                <div class="personal-info user-divider">
                    <h2>Personal Information</h2>
                    <p>This information is required to create your account</p>
                    <br/>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.Title) %><span class="form-required">*</span></strong>
                        <%: Html.DropDownListFor(model => model.Title, new SelectList(new[]{"","Mr.","Mrs.","Ms.","Pastor","Rev."}))%>
                        <span style="width: 222px; display: inline-block;">&nbsp;</span>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.FirstName) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(model => model.FirstName) %>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.LastName) %><span class="form-required">*</span></strong>
                        <%: Html.TextBoxFor(model => model.LastName) %>
                    </div>
                    <div style="padding-left:86px; margin:6px;" id="registrationGender">
                        <strong>
                            <%: Html.LabelFor(model => model.Gender) %><span class="form-required">*</span></strong>
                        <div style="width: 140px; display: inline-block;">
                            <%= Html.RadioButtonListFor(model => model.GenderRadioButtonList, RepeatDirectionTypes.Horizontal)%>
                        </div>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(model => model.Gender) %></span>
                    </div>
                    <div class="user-form-item">
                        <strong>Phone Number<span class="form-required">*</span>:</strong>
                        <%: Html.DropDownListFor(model => model.PhoneCountryCode, new SelectList(Country.GetAll<Country>().Select(x => x.PhoneCode).Distinct().OrderBy(x => x),"+1"), new { @style = "width:60px" })%>
                        <%: Html.TextBoxFor(m => m.PhoneAreaCode, new { @style = "width:60px", @size = "4", @maxlength = "4", @class="label-textbox", @label="Area code" })%>
                        <%: Html.TextBoxFor(m => m.PhoneNumber, new { @style = "width:156px", @size = "10", @maxlength = "10", @class = "label-textbox", @label = "Phone number" })%>
                    </div>
                </div>
                <div class="personal-info user-divider">
                    <h2>
                        Optional Information</h2>
                    <p>This information is optional to create your account</p>
                    <br/>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.Company) %></strong>
                        </div>
                            <%: Html.TextBoxFor(model => model.Company) %>
                    </div>
                    <%--<div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(model => model.Occupation) %></strong>
                        <%: Html.TextBoxFor(model => model.Occupation)%>
                    </div>--%>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.Address1) %></strong>
                        </div>
                        <%: Html.TextBoxFor(model => model.Address1) %>
                    </div>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.Address2) %></strong>
                        </div>
                        <%: Html.TextBoxFor(model => model.Address2) %>
                    </div>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.City) %></strong>
                        </div>
                        <%: Html.TextBoxFor(model => model.City) %>
                    </div>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.Country) %></strong>
                        </div>
                        <%: Html.DropDownListFor
                                (
                                    m => m.Country,
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name", ClientCompany.DefaultCountry.Iso),
                                    String.Empty,
                                    new
                                        {
                                            @style = "width:292px",
                                            @onchange = "set_city_state__and_phoneCode(this,'State',countryCodes,'PhoneCountryCode')"
                                        }
                                )%>
                    </div>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.State) %></strong>
                        </div>
                        <%: Html.DropDownListFor(model => model.State, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @style = "width:292px" })%>
                    </div>
                    <div class="user-form-item">
                        <div style="float: left; margin-left: 40px">
                            <strong><%: Html.LabelFor(model => model.Zip) %></strong>
                        </div>
                        <%: Html.TextBoxFor(model => model.Zip) %>
                    </div>
                    <br />
                    <div style="text-align: left;" id="newsLetterSignUp">
                        <div style="float: left;">
                            <strong><label for="NewsLetterOptIn" style="margin-left: 40px; margin-right: 15px; text-align: left;">Join email list:</label></strong>
                        </div>
                        <%: Html.CheckBoxFor(m => m.NewsLetterOptIn, new {@checked = "checked"})%>
                    </div>
                    <%--<div style="text-align: left;" id="newsLetterSignUp">
                        <strong><span style="margin-left: 32px; margin-right: 15px; text-align: left;">Sign up for the</span>
                        <br /> 
                        <span style="margin-left: 33px; margin-right: 25px; text-align: left;">news letter:</span></strong>
                        <%: Html.CheckBoxFor(m => m.NewsLetterOptIn, new {@checked = "checked"})%>
                    </div>--%>
                    <div>
                        <hr />
                        <div style="padding-left: 10px;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Register", true, "Creating account..."))%>
                        </div>
                    </div>
                </div>
        </div>
        <% } %>
    </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
