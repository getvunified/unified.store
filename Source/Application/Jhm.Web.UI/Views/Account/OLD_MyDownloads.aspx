<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.DownloadsViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Digital Media Downloads
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <link href="<%:Url.Content("~/Content/css/DownloadsGridSixColumns.css") %>" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <%--<script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".dispalyInfo > div").mouseover(function() {
                jQuery(this).find(".gradient_1").css("background", "url(/Content/images/grid_background_info_hover.png) repeat-x transparent");
            });
            
            jQuery(".dispalyInfo > div").mouseout(function () {
                jQuery(this).find(".gradient_1").css("background", "url(/Content/images/grid_background_info_active.png) repeat-x transparent");
            });
            

        });
    </script>--%>
    <style type="text/css">
        
        .tab-content{
            height: 100%;
        }

        .grid_3 {
            position: relative;
        }

        .displayInfo > div > span {
            /*font-size: 15px;*/
            text-align: center;
            padding: 10px 0px 10px 0px;
            /*background-color: #DCDCDC;*/
            color: black;
            z-index: 20000;
        }

        .displayInfo > div > .gradient_1 {
            position: absolute;
            top: 0px;
            left: 0px;
            margin: 2px;
            height: 100% !Important;
            max-height: 38px !Important;
            background: url(/Content/images/grid_background_info_active.png) repeat-x transparent;
            color: #000000;
            
            width: 100%;
            opacity:0.4;
            filter:alpha(opacity=40);
            z-index: -1;
        }

        .displayInfo > .gradient_1 {
            position: absolute;
            top: 0px;
            left: 0px;
            
            height: 100% !Important;
            max-height: 38px !Important;
            background: url(/Content/images/grid_background_info_active.png) repeat-x transparent;
            color: #000000;
            
            opacity:0.4;
            filter:alpha(opacity=40);
            z-index: 0;
            height: 30px !Important;
            padding-left: 10px;
            padding-bottom: 5px;
            width: 320px;
        }

        .displayHeader > div > .gradient_1 {
            position: absolute;

            top: 0px;
            left: 0px;

            padding-bottom: 5px;

            border-bottom: thin;
            border-bottom-style: solid;
            border-bottom-color: darkcyan;
            
            margin: 2px;
            width: 100%;
            background: url(/Content/images/grid_background_active.jpg) repeat-x;

            color: #000000;

            height: 38px !Important;
            max-height: 38px !Important;
            opacity:0.4;
            filter:alpha(opacity=40);
            z-index: -1;
        }

        .cont {
            /*background-color: slategray;*/
            width: 98%;
            margin-top: 20px;
            margin-left: 2%;
            padding-bottom: 15px;
            position: relative;
            display: block;
        }

        .grid_2 {
            position: relative;
            margin: 1px;
            z-index: 0;
        }

        .grid_3 {
            width: 48.7% !Important;
            margin-left: 0% !Important;
            margin-right: 1px;
            margin-top: 0.25% !Important;

        }

        

        .displayHeader > div > span {
            font-weight: bold;
            /*font-size: large;*/
            text-align: center;
            padding: 15px 0px 15px 0px;
            /*background-color: #96AABB;*/
            color: black;
        }

        .myForm-field label
        {
            width: 136px;
            display: inline-block;
            text-align: right;
            font-weight: bold;
        }
        .myForm-field input, select
        {
            width: 200px;
        }
        
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="displayDownloadLinks" ContentPlaceHolderID="MainContent" runat="server">
    <% if(Model.IsDifferenceMediaPage){ %>
        <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <% } %>
        <br/>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <% using (Html.BeginForm())
               {%>
            <%: Html.FormKeyValueMatchAttributeHiddenField() %>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
            <div class="tab-content">
                <div style="min-height: 250px;display: inline-block;margin-bottom: 20px;">
                    <h2>Available Downloads for <%: Model.Name %></h2>
                    <%--Initial view: Username is prompted--%>
                    
                        
                    <div class="cont container_6" style="max-width: 900px !Important; width: 900px;">
                        <% if (Model.User.Downloads.Any())
                       {%>
                        <div class="grid_6 displayHeader">
                            <div class="grid_2"><span style="display: block;">Purchase Date</span>
                                <div class="gradient_1"></div>
                            </div>
                            <div class="grid_2"><span style="display: block;">Product</span>
                                <div class="gradient_1"></div>
                            </div>
                            <div class="grid_2"><span style="display: block;">Download Link</span>
                                <div class="gradient_1"></div>
                            </div>
                        </div>
                        <%
                            foreach (var download in Model.User.Downloads)
                            {
                                if (Model.StockItems[download.Id] == null) continue;%>
                                <div class="grid_6 displayInfo">
                                    <div class="grid_2">
                                        <span style="display: block;">
                                            <%: download.CreatedDate.ToShortDateString() %>
                                        </span>
                                        <div class="gradient_1"></div>
                                    </div>
                                    <div class="grid_2">
                                        <span style="display: block;">
                                            <%: Model.StockItems[download.Id].StockItem.ProductTitle %> - <%: Model.StockItems[download.Id].StockItem.MediaType.DisplayName %>
                                        </span>
                                        <div class="gradient_1"></div>
                                    </div>
                                    <div class="grid_2">
                                        <span style="display: block;">
                                            <a href='<%: String.Format("http://{0}/Account/FileDownloadManager/{1}",
                                       Request.ServerVariables["HTTP_HOST"], download.Id) %>' style='width:auto;background:none; color:Black; text-decoration:underline;' >Click here to download this purchase</a>
                                        </span>
                                        <div class="gradient_1"></div>
                                    </div>
                                </div>

                        <% }%>

                            <% }
                       else
                       {%>
                                <div class="grid_6 displayInfo" style="position: relative;"><span style="display: block; z-index: 500; color: black; padding-left: 5px; font-weight: bold;">Sorry, you haven't purchased any downloads</span>
                                    <%--<div class="gradient_1"></div>--%>
                                </div>
                                <% } %>

                            
                    </div>
                </div>
               <%}%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
