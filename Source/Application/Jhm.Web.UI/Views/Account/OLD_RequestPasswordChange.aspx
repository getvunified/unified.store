﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.ForgotPasswordModel>" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field
        {
        }
        .myForm-field label
        {
            width: 136px;
            display: inline-block;
            text-align: right;
            font-weight: bold;
        }
        .myForm-field input, select
        {
            width: 200px;
        }
        
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2><%: Request.IsAuthenticated ? "Changing Password?" : "Forgot Password?" %></h2>
                <% using (Html.BeginForm())
                   { %>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                <%: Html.ValidationSummary(true, "Password recovery was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div>
                    <p>
                        Use the form below to recover your password.
                    </p>
                    <p>
                        An email with instructions on how to recover password will be sent to your email address. If this is not the email address you registered with, you will not receive an email.
                    </p>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.Email) %>:
                        <% if (Request.IsAuthenticated)
                           { %>
                                <%: Html.DisplayFor(m => m.Email) %>
                        <% }
                           else
                           { %>
                                <%: Html.TextBoxFor(m => m.Email) %>
                        <% } %>
                        <%: Html.ValidationMessageFor(m => m.Email)%>
                        <%--<%: Html.ActionLink("Forgot Username? Click here", "ForgotUsername", "Account", null, new { @title = "Click here if you forgot your username" })%>--%>
                    </div>
                    <hr />
                    <div style="padding-left: 10px;">
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Send Password Change Request", true, "Finding your account..."))%>
                        <br />
                        <br />
                    </div>
                    
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
