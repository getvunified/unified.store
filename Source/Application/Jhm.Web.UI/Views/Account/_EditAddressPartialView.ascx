﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.AddressViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>


<script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] { "PhoneCode" })%>
    <script type="text/javascript">

        $(document).ready(function () {
            populateStateOrProvince('Country', 'State', '<%= Model.State %>');
            $(".label-textbox").each(function (index) {
                var $this = $(this);
                if (this.value == this.getAttribute('label')) {
                    $this.val($this.attr("label"));
                    $this.addClass("initial");
                }
                $this.focusin(function () {
                    if (this.value == this.getAttribute('label')) {
                        this.value = '';
                        $(this).removeClass("initial");
                        $(this).addClass("in-use");
                    }
                });
                $this.focusout(function () {
                    if (this.value == '') {
                        this.value = this.getAttribute('label');
                        $(this).removeClass("in-use");
                        $(this).addClass("initial");
                    }
                });
            });
        });
        function populateStateOrProvince(ddCountryId, ddStateId, selectedState) {
            set_city_state(document.getElementById(ddCountryId), ddStateId);
            $('#' + ddStateId).val(selectedState);
        }
       
        function addressEditorPopUp(jsonAddress) {
            for (var key in jsonAddress) {
                setFieldValue(key, jsonAddress[key]);
            }
            var getPrimaryAdrressCheckBox = jsonAddress.Primary;
            if (getPrimaryAdrressCheckBox) {
                jQuery('#IsPrimaryCheckBoxHolder').hide();
            }
            if (getPrimaryAdrressCheckBox == false) {
                $('#IsPrimaryCheckBoxHolder').show();
            }
            set_city_state(document.getElementById('Country'), 'State');
            setFieldValue('State', jsonAddress.State);
            jQuery('#PopupAddressForm').attr("action", '<%=Url.Action("EditAddress","Account") %>');
            jQuery('#addressPopup').dialog("option", "title", 'Update Billing Address');
            jQuery('#addressPopup').dialog('open');
        }
        function newAddressEditorPopUp(showIsPrimaryCheckbox) {
            if (showIsPrimaryCheckbox) {
                $('#IsPrimaryCheckBoxHolder').show();
            }
            else {
                $('#IsPrimaryCheckBoxHolder').hide();
            }
            setFieldValue('Address1', '');
            setFieldValue('Address2', '');
            setFieldValue('City', '');
            setFieldValue('Country', '');
            setFieldValue('State', '');
            setFieldValue('Zip', '');
            setFieldValue('AddressId', '');
            setFieldValue('Primary', 'false');
            jQuery('#PopupAddressForm').attr("action", '<%= (Url.Action("AddNewAddress", "Account") )%>');
            jQuery('#addressPopup').dialog("option", "title", 'Add New Billing Address');
            jQuery('#addressPopup').dialog('open');
        }
        function setFieldValue(fieldId, value) {
            var fields = document.getElementsByName(fieldId);
            $.each(fields, function () {
                this.value = value;
            });

        }

        function changePrimaryCheckBox() {
            var isChecked = jQuery('#Primary').attr('checked')?true:false;
            if (isChecked) {
                setFieldValue('Primary', 'true');
            } 
            else {
                setFieldValue('Primary', 'false');
            }
            
        }
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function () {

            function validate() {
                var validAddress = true;

                var address1 = jQuery("#Address1").val();
                var city = jQuery("#City").val();
                var country = jQuery("#Country").val();
                var state = jQuery("#State").val();


                if(jQuery.trim(address1).length == 0) {
                    validAddress = false;
                }
                
                if (jQuery.trim(city).length == 0) {
                    validAddress = false;
                }

                if (jQuery.trim(country).length == 0) {
                    validAddress = false;
                }

                if (jQuery.trim(state).length == 0 || state == "Select State/Provice/Division") {
                    validAddress = false;
                }
                
                if(validAddress) {
                    jQuery('#PopupAddressForm').trigger('submit');
                } else {
                    return;
                }
            }

            jQuery('#addressPopup').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                buttons: {
                    'Save Address': function () {
                        validate();
                        //
                    },
                    Cancel: function () {
                        jQuery(this).dialog('close');
                    }
                }
            });
            
            <%= ViewData["ShowNewAddressPopupScript"] %>
        });
    </script>


    <div class="region-content-bottom">
        <div class="content">
            <div id="addressPopup">
                <% using (Html.BeginForm("AddNewAddress", "Account", FormMethod.Post, new { id = "PopupAddressForm" }))
               {%>
                <%: Html.ValidationSummary("There were some problems when adding your address. Please correct the errors and try again.",new{@class="form-required"}) %>
                <div >
                    <strong>
                        <%: Html.LabelFor(model => model.Address1) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.Address1) %>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.Address2) %></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.Address2) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.City) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.City) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.Country) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor
                                (
                                    m => m.Country,
                                                                Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name", "840"),
                                    String.Empty,
                                    new
                                        {
                                            @style = "width:292px",
                                            @onchange = "set_city_state__and_phoneCode(this,'State',countryCodes,'PhoneCountryCode')"
                                        }
                                )%>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.State) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor(model => model.State, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @style = "width:292px" })%>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.Zip) %></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.Zip) %>
                </div>
                <br/>
                  <br/>
                <div id="IsPrimaryCheckBoxHolder">
                    <strong>
                        <%: Html.LabelFor(model => model.Primary) %></strong>
                    <%: Html.CheckBoxFor(model => model.Primary, new { @onClick = "changePrimaryCheckBox()" })%>
                </div>
                 <div class="user-form-item">
                       <%: Html.HiddenFor(model => model.AddressId)%>
                </div>
                <div class="user-form-item">
                    <%: Html.HiddenFor(model => model.A02RecordId)%>
                </div>
                <div class="user-form-item">
                    <%:Html.HiddenFor(model => model.RecordId)%>
                </div>
                  
                <%} %>
            </div>
        </div>
    </div>