﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Impersonate User</h2>
    <br/>
    <% using (Html.BeginForm("ImpersonateUser","Account",FormMethod.Get))
       { %>
        Username: <input type="text" name="username"/>
        <br/>
        <input type="submit" value="Go"/>
    <% } %>
</asp:Content>