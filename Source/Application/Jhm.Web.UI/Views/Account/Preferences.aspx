<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.PreferencesViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Account Preferences
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field
        {
        }
        .myForm-field label
        {
            width: 136px;
            display: inline-block;
            text-align: right;
            font-weight: bold;
        }
        .myForm-field input, select
        {
            width: 200px;
        }
        
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <% using (Html.BeginForm())
               { %>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <%: Html.ValidationSummary(true, "Saving your preferences was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
            <div class="tab-content">
                <div class="personal-info user-divider">
                    <h2>
                        Available Promotions for
                        <%: Model.Profile.FirstName %></h2>
                    <%--Initial view: Username is prompted--%>
                    <table>
                        <col width="100" />
                        <col width="300" />
                        <col width="30" />
                        <thead>
                            <tr>
                                <th>Promotion</th>
                                <th>Description</th>
                                <th />
                            </tr>
                        </thead>
                        <tbody>
                        <%
                           var i = 0;
                           foreach (var profileSettingViewModel in Model.ProfileSettings)
                           { %>
                            <tr>
                                <td align="left"><%: profileSettingViewModel.Title %></td>
                                <td align="left"><%: profileSettingViewModel.Description %></td>
                                <td>
                                    <%: Html.CheckBox(String.Format("ProfileSettings[{0}].IsActive",i), profileSettingViewModel.IsActive)%>
                                    <%: Html.Hidden(String.Format("ProfileSettings[{0}].Id",i), profileSettingViewModel.Id)%>
                                    <%: Html.Hidden(String.Format("ProfileSettings[{0}].PreferenceType", i), profileSettingViewModel.PreferenceType)%>
                                </td>
                            </tr>
                            <%--<%=Html.RenderPartialEditByTypeName(profileSettingViewModel, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/")%>--%>
                        <%
                               i++;
                           } %>
                    </tbody>
                    </table>
                    
                </div>
                <br />
                    <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Save Preferences", true, "Saving your preferences..."))%>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
