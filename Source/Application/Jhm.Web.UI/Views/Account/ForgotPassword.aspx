﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.PasswordRecoveryModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Change Password
</asp:Content>
<asp:Content runat="server" ID="Head" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <style type="text/css">
        .myForm-field
        {
        }
        .myForm-field label
        {
            width: 136px;
            display: inline-block;
            text-align: right;
            font-weight: bold;
        }
        .myForm-field input, select
        {
            width: 200px;
        }
        
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="changePasswordContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("Account") %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>
                    Forgot Password?</h2>
                <% using (Html.BeginForm())
                   { %>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                <%: Html.ValidationSummary(true, "Password recovery was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <div>
                    <%--Initial view: Username is prompted--%>
                    <% if (String.IsNullOrEmpty(Model.UserName))
                       { %>
                    <p>
                        Use the form below to recover your password.
                    </p>
                    <p>
                        An email with your password will be sent to the email address registered in your
                        profile
                    </p>
                    <div class="myForm-field">
                        <%: Html.LabelFor(m => m.UserName) %>
                        <%: Html.TextBoxFor(m => m.UserName)%>
                        <%: Html.ValidationMessageFor(m => m.UserName)%>
                        <%: Html.ActionLink("Forgot Username? Click here", "ForgotUsername", "Account", null, new { @title = "Click here if you forgot your username" })%>
                    </div>
                    <hr />
                    <div style="padding-left: 10px;">
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Recover Password", true, "Finding your account..."))%>
                        <br />
                        <br />
                    </div>
                    <% } // Second view: Security question is displayed and security answer is prompted
                       else if (String.IsNullOrEmpty(Model.Email))
                       { %>
                    <p>
                        Use the form below to recover your password.
                    </p>
                    <p>
                        An email with your password will be sent to the email address registered in your
                        profile
                    </p>
                    <div class="user-form-item">
                        <strong>Username:
                            <%: Html.DisplayTextFor(m => m.UserName)%>
                            <%: Html.HiddenFor(m => m.UserName)%>
                        </strong>
                    </div>
                    <div class="user-form-item">
                        <strong>
                            <%: Html.LabelFor(m => m.PasswordQuestion) %>:
                            <%: Model.PasswordQuestion %>
                        </strong>
                        <%: Html.TextBoxFor(m => m.PasswordAnswer)%>
                        <%: Html.ValidationMessageFor(m => m.PasswordAnswer)%>
                    </div>
                    <div style="padding-left: 10px;">
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Recover Password", true, "Recovering your password..."))%>
                        <br />
                        <br />
                    </div>
                    <% }
                       else // final view: Email sent notification
                       { %>
                            Your password was successfully reset. An email with your temporary password was
                            sent to
                            <%: Model.Email %>.
                            <br />
                            <br />
                    <% } %>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>
