﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.UserSubscriptionsViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.em3.Core" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="changePasswordTitle" ContentPlaceHolderID="TitleContent" runat="server">
    My Subscriptions
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript" src="<%:Url.Content("~/Content/js/jquery.timeago.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery.timeago.settings.allowFuture = true;
            jQuery("span.timeago").timeago();
            
            jQuery('#transactionDetailPopup').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                buttons: {
                    'Cancel Subscription': function () {
                        if ( confirm('Are you sure you want to cancel this subscription?') ) {
                            location.href = '<%: Url.Action("CancelSubscription") %>/' + jQuery('#UpdateSelectedSubscriptionId').val();
                        }
                    },
                    'Renew Subscription': function () {
                        location.href = '<%: Url.Action("JhmMagazineSubscription","Resources") %>';
                    },
                    Close: function () {
                        jQuery(this).dialog('close');
                    }
                }
            });
            
            jQuery('#newAddressPopup').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                buttons: {
                    'Use this Address': function () {
                        validateNewAddressForm();
                        //
                    },
                    Cancel: function () {
                        jQuery(this).dialog('close');
                    }
                }
            });
            
            <% if(Model.SelectedSubscription.Id != 0 )
               { %>
            openSubscriptionDetails(<%= Foundation.HelperExtensions.ToJSON(Model.SelectedSubscription) %>);
            <% } %>
        });
        
        function validateNewAddressForm() {
            var formIsValid = true;

            var address1 = jQuery("#DeliveryAddress_Address1");
            var city = jQuery("#DeliveryAddress_City");
            var country = jQuery("#DeliveryAddress_Country");
            var state = jQuery("#DeliveryAddress_State");
            

            if (!validateJQueryField(address1)) {
                formIsValid = false;
                
            }

            if (!validateJQueryField(city)) {
                formIsValid = false;
            }

            if (!validateJQueryField(country)) {
                formIsValid = false;
            }

            if ( !validateJQueryField(state) || state.val() == "Select State/Provice/Division") {
                state.addClass("input-validation-error");
                formIsValid = false;
            }
            
            if (formIsValid) {
                jQuery('#NewAddressForm').trigger('submit');
            } else {
                jQuery('#popupValidateMessage').show();
                setTimeout(function() { jQuery('#popupValidateMessage').fadeOut(1000, null); }, 4000);
                return;
            }
        }
        
        function validateJQueryField(jQueryField) {
            if (jQuery.trim(jQueryField.val()).length == 0) {
                jQueryField.addClass("input-validation-error");
                return false;
            }
            jQueryField.removeClass("input-validation-error");
            return true;
        }
        
        function openSubscriptionDetails(jsonSub) {
            jQuery('#tdSubscriberName').html(jsonSub.User.Profile.FirstName + " " + jsonSub.User.Profile.LastName);
            jQuery('#tdStartDate').html(jsonSub.StartDateString);
            jQuery('#tdEndDate').html(jsonSub.ExpirationDateString);
            jQuery('#tdDeliveryAddress').html(
                jsonSub.DeliveryAddress.Address1 + " " + jsonSub.DeliveryAddress.Address2 + "<br />" +
                jsonSub.DeliveryAddress.City + " " + jsonSub.DeliveryAddress.StateCode + ",  " + jsonSub.DeliveryAddress.PostalCode + "<br />" +
                jsonSub.DeliveryAddress.Country
            );
            jQuery('#tdNumberOfIssues').html(jsonSub.NumberOfIssues);
            jQuery('#trUserAddresses').hide();
            jQuery("#transactionDetailPopup").dialog('option','width',500);
            jQuery('#transactionDetailPopup').dialog('open');
            jQuery('#UpdateDeliveryAddressId').val(jsonSub.DeliveryAddress.DSAddressId);
            jQuery('#NewDeliveryAddressId').val(jsonSub.DeliveryAddress.DSAddressId);
            jQuery('#UpdateSelectedSubscriptionId').val(jsonSub.Id);
            jQuery('#NewSelectedSubscriptionId').val(jsonSub.Id);
        }

        var addresses = <%= Foundation.HelperExtensions.ToJSON(Model.Addresses) + ";" %>
        
        function populateBillingAddressFields(ddAddress) {
            if (ddAddress == null) {
                return;
            }
            var selectedAddressId = ddAddress.value;
            var address = getAddressById(selectedAddressId);
            if (address == null) {
                $("#spBillingAddress").html('');
                $("#spBillingCity").html('');
                $("#spBillingState").html('');
                $("#spBillingZip").html('');
                $("#spBillingCountry").html('');
                return;
            }
            $("#spBillingAddress").html(address.Address1 + (address.Address2 == '' ? '' : '<br />' + address.Address2));
            $("#spBillingCity").html(address.City);
            $("#spBillingState").html(', ' + address.StateCode);
            $("#spBillingZip").html(' ' + address.PostalCode);
            //var country = $("#Country option[value='" + address.Country + "']").text();
            $("#spBillingCountry").html(address.Country);
        }
        
        function getAddressById(addressId) {
            for (var i = 0; i < addresses.length; i++) {
                var address = addresses[i];
                if (address.DSAddressId == addressId) {
                    return address;
                }
            }
            return null;
        }
        

        function showDeliveryAddressEditor() {
            jQuery('#trUserAddresses').show();
            jQuery("#transactionDetailPopup").dialog('option','width',640);
        }
        
        function showNewAddressPopup() {
            jQuery('#newAddressPopup').dialog('open');
        }

    </script>
    <script runat="server" type="text/C#">
        private string LoadSubscriptionDetailsPopupCallback(UserSubscriptionViewModel sub)
        {
            return string.Format("openSubscriptionDetails({0}); return false;", Foundation.HelperExtensions.ToJSON(sub));
        }
    </script>
    <style type="text/css">
        #detailsTable th {
            text-align:right; 
            width:190px;
            padding-right: 10px;
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Account" : String.Empty) %>
    <div class="region-content-bottom">
        <div class="content">
            <% Html.RenderPartial("~/Views/Shared/PartialViews/ObjectDisplayEditControls/Account/Header.ascx"); %>
            <div class="tab-content">
                <h2>Active Subscriptions</h2>
                <table>
                    <thead>
                        <tr>
                            <td>
                                Subscription Date
                            </td>
                            <td>
                                Subscription
                            </td>
                            <td>
                                Options
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <% foreach (var sub in Model.Subscriptions)
                           { 
                        %>
                        <tr>
                            <td>
                                <span class="timeago" title="<%: sub.StartDate.GetValueOrDefault().ToString("o")%>"><%: sub.StartDate.GetValueOrDefault()%></span>
                            </td>
                            <td>
                                <%: sub.SubscriptionTitle + (String.IsNullOrEmpty(sub.PriceCodeDescription) ? String.Empty : " (" + sub.PriceCodeDescription + ")") %>
                            </td>
                            <td>
                                <a href="javascript:void(0);" onclick='<%= LoadSubscriptionDetailsPopupCallback(sub) %>'>Details</a><%: Html.ActionLink("Renew","JhmMagazineSubscription","Resources") %>
                            </td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="transactionDetailPopup" title="Subscription Details" style="display: none">
        <table id="detailsTable">
            <tr>
                <th>Subscriber:</th>
                <td id="tdSubscriberName"></td>
            </tr>
            <tr>
                <th>Subscription Start Date:</th>
                <td id="tdStartDate"></td>
            </tr>
            <tr>
                <th>Subscription End Date:</th>
                <td id="tdEndDate"></td>
            </tr>
            <tr>
                <th>Delivery Address:</th>
                <td id="tdDeliveryAddress"></td>
            </tr>
            <tr>
                <th/>
                <td>
                    <a href="javascript:void(0);" onclick="showDeliveryAddressEditor();" style="padding: .2em .6em .3em .6em;" class="ui-state-default ui-corner-all ui-state-hover">Change Delivery Address</a>
                    <br/><br/>
                </td>
            </tr>
            <tr id="trUserAddresses" style="display:none;">
                <th>New Delivery Address:</th>
                <td>
                    <% using(Html.BeginForm("UpdateSubscriptionDeliveryAddress","Account"))
                       { %>
                        <%: Html.HiddenFor(m => m.DeliveryAddress.AddressId, new{id="UpdateDeliveryAddressId"}) %>
                        <%: Html.HiddenFor(m => m.SelectedSubscription.Id, new{id="UpdateSelectedSubscriptionId"}) %>
                    <div class="form-item" style="margin-top:0;">
                        <%: Html.DropDownListFor
                             (
                                 m => m.SelectedDeliveryAddressId,
                                 new SelectList(Model.Addresses ?? new List<Address>(), "DSAddressId", "Address1"),
                                 "(Select an address)",
                                 new
                                     {
                                         @class = "form-select required",
                                         @style = "width:220px",
                                         @tabindex = "2",
                                         @onChange = "populateBillingAddressFields(this);"
                                     }
                             ) %> or <a href="javascript:void(0);" onclick="showNewAddressPopup();" style="padding: .2em .6em .3em .6em;" class="ui-state-default ui-corner-all ui-state-hover">Add New Address</a>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.SelectedDeliveryAddressId) %>
                        </span>
                    </div>
                    <div class="form-item">
                        <span id="spBillingAddress"></span><br/>
                        <span id="spBillingCity"></span><span id="spBillingState"></span><span id="spBillingZip"></span><br/>
                        <span id="spBillingCountry"></span><br/>
                    </div>
                    <br />
                    <input type="submit" value="Update Delivery Address"/>
                    <% } %>
                </td>
            </tr>
            <tr>
                <th>Number of Issues:</th>
                <td id="tdNumberOfIssues"></td>
            </tr>
        </table>
    </div>
    <div id="newAddressPopup" title="Change Delivery Address">
                <% using (Html.BeginForm("UpdateSubscriptionDeliveryAddress", "Account", FormMethod.Post, new { id = "NewAddressForm" }))
               {%>
                    <%: Html.HiddenFor(m => m.DeliveryAddress.AddressId, new{id="NewDeliveryAddressId"}) %>
                    <%: Html.HiddenFor(m => m.SelectedSubscription.Id, new{id="NewSelectedSubscriptionId"}) %>
                    
                    <h4>New Delivery Address</h4>
                <div >
                    <strong><%: Html.LabelFor(model => model.DeliveryAddress.Address1) %><span class="form-required">*</span></strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.DeliveryAddress.Address1) %>
                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.Address1) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.DeliveryAddress.Address2) %></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.DeliveryAddress.Address2) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.DeliveryAddress.City) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.DeliveryAddress.City) %>
                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.City) %>
                </div>
                <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.DeliveryAddress.Country) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor
                                (
                                    m => m.DeliveryAddress.Country,
                                                                Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name", "840"),
                                    String.Empty,
                                    new
                                        {
                                            @style = "width:292px",
                                            @onchange = "set_city_state__and_phoneCode(this,'GiftSubscriber_DeliveryAddress_State',countryCodes,'GiftSubscriber_PrimaryPhone_CountryCode')"
                                        }//this,'State',countryCodes,'PhoneCountryCode'
                                )%>
                        <%: Html.ValidationMessageFor(model => model.DeliveryAddress.Country) %>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.DeliveryAddress.State) %><span class="form-required">*</span></strong>
                        </div>
                <div class="user-form-item">
                    <%: Html.DropDownListFor(model => model.DeliveryAddress.State, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @style = "width:292px" })%>
                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.State) %>
                </div>
                  <br/>
                <div>
                    <strong>
                        <%: Html.LabelFor(model => model.DeliveryAddress.Zip) %>
                    </strong>
                </div>
                <div class="user-form-item">
                    <%: Html.TextBoxFor(model => model.DeliveryAddress.Zip) %>
                </div>
                <br/>
                  <br/>
                    <span id="popupValidateMessage" style="color:red;display:none;">* Please fill out required fields and try again</span>
                <%} %>
            </div>
</asp:Content>
<asp:Content ID="SideBaseSecond" ContentPlaceHolderID="SidebarSecondContent" runat="server">
    <% Html.RenderPartial("~/Views/Shared/PartialViews/RightMenu/Account/Account.ascx"); %>
</asp:Content>

