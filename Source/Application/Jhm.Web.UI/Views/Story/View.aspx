﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Story>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Difference Media
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="<%: Url.Content("~/Content/dm/css/news.css?version=1")%>" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">
        var switchTo5x = true;
        stLight.options({ publisher: "0272acbb-4dea-4c0e-83cc-a4212a0a2dc8" });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.DifferenceMediaTitleHeader("Article")%>
    <div class="newsItem" style="padding: 15px;" >
        <h1 class="view"><%: Model.Title %></h1>
        <p class="postedBy">Posted on <%: Model.PublishDate.ToString("MMM d, yyyy")%></p>
 
        <%--<img src="<%: Model.PostPicture %>" class="newsPhoto" width="250" alt="<%: Model.Title %>" />--%>

        <p class="subTitle">
            <%: Model.SubTitle %>
        </p>
        <p>
            <%= Model.Body %>
        </p>
        
        <%--<div class="socialView">
            <span class='st_facebook_hcount' displayText='Facebook'></span>
            <span class='st_twitter_hcount' displayText='Tweet'></span>
            <span class='st_email_hcount' displayText='Email'></span>
            <span class='st_sharethis_hcount' displayText='ShareThis'></span>
            <span class='st_googleplus_hcount' displayText='Google +'></span>
            <span class='st_fblike_hcount' displayText='Facebook Like'></span>
        </div>--%>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
