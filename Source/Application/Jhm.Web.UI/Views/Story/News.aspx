﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.StoryViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	News
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="<%: Url.Content("~/Content/dm/css/news.css?version=1")%>" media="screen" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">
        var switchTo5x = true;
        stLight.options({ publisher: "0272acbb-4dea-4c0e-83cc-a4212a0a2dc8" });
    </script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <%: Html.DifferenceMediaTitleHeader("News") %>
    <div id="newsBoxWrap">

    <div id="newsBox">
        <% foreach (var post in Model.StoryList)
           {%>
    	<div class="newsItem" >
                <h2>
    	        <% if (post.IsExternal)
                   { %>
                        <a target="_blank" href="<%: post.LinkUrl %>"><%: post.Title.ToUpper() %></a>
                 <% }
                   else
                   {%>
                        <a href="<%: Url.Action("View", "Story", new {id = post.Id}) %>"><%: post.Title.ToUpper() %></a>
                   <%}%>   
                </h2>
                <p class="postedBy">Posted  on <% //var time = post.DateCreated.ToShortDateString();
                                                                          //time = time.Substring(0, time.IndexOf("T", System.StringComparison.Ordinal));
                                                                          
                                                                          %><%: post.PublishDate.ToString("MMM d, yyyy")%></p>
                <%--<a href="<%: Url.Action("View", "News", new{id = post.Id}) %>">
                    <img src="<%: post.PostPicture %>" class="newsPhoto" width="250" alt="<%: post.Title %>" />
                </a>--%>
               
                <p style="min-height: 50px;"><%--
               var teaser = post.Body;
                       var length = teaser.Length;
                       length = length/4;

                       teaser = teaser.Substring(0, length) + "...";
                       
                    --%>
                    <%= post.Body.TruncateToClosestPuntuationMarkAt(200, "...") %>
                    
                    </p>
                
                <div class="readMore">
                    <%--<div class="social">
                        <span class='st_facebook_hcount' displayText='Facebook'></span>
                        <span class='st_twitter_hcount' displayText='Tweet'></span>
                        <span class='st_email_hcount' displayText='Email'></span>
                        <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                        <span class='st_googleplus_hcount' displayText='Google +'></span>
                        <span class='st_fblike_hcount' displayText='Facebook Like'></span>
                  </div>--%>
                  <% if(post.IsExternal)
                    { %>
                         <a target="_blank" href="<%: post.LinkUrl %>"><img src="<%: Url.Content("~/Content/dm/images/btn_readMore.jpg") %>" alt="Read More" /></a>
                   <%  }
                       else
                     { %>
                         <a href="<%: Url.Action("View", "Story", new{id = post.Id}) %>"><img src="<%: Url.Content("~/Content/dm/images/btn_readMore.jpg") %>" alt="Read More" /></a>
                   <%  } %>
                  
                </div>
        </div>
            <br />
        <% } %>
    </div>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
