﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.PressKitViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PressKit
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="<%: Url.Content("~/Content/dm/scripts/press.js?version=1") %>" type="text/javascript"></script>
    <link href="<%: Url.Content("~/Content/dm/css/press.css?version=1")%>" media="all" rel="stylesheet" type="text/css" /> 
    <style type="text/css">
        .data {
            background: #FFF;
            color: #000;
            height: 430px;
            width: 720px;
        }

        #leftImg {
            float: left;
            height: 430px;
            width: 420px;
        }

        #rightContent {
            float: right;
            height: 390px;
            padding: 20px;
            width: 250px;
            overflow-y: auto;
            direction: ltr;
        }
        
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <%: Html.DifferenceMediaTitleHeader("Press Kits") %>

    <div id="pressBoxWrap">
        <div id="pressBox">
            <%for (int i = 0; i < Model.PressKitList.Count; i++)
              {
                    var pressKit = Model.PressKitList[i]; %>
               
                <div class="pressItem">
                    <a href="#data<%= i %>" class="inline" >
                    <%--<a href="<%: Url.Action("View", "PressKit", new {id = pressKit.Id}) %>">--%>
                        <img src="<%: pressKit.ThumbnailUrl %>" class="pressPhoto" height="176" width="266" alt="<%: pressKit.Title %>" />
                    <%--</a>--%>
                    </a>
                    <div class="pressItemBtns">
                        <a href="<%: pressKit.DownloadUrl %>">
                            <img src="<%: Url.Content("~/Content/dm/images/btn_download.png") %>" alt="Download" />
                        </a>
                        <a href="#data<%= i %>" class="inline" >
                            <img src="<%: Url.Content("~/Content/dm/images/btn_preview.png") %>" alt="Preview" />
                        </a>
                    </div>
                </div>
                <div style="display:none">
                    <div id="data<%= i %>" class="data">
                        <div id="leftImg" >
                            <img src="<%: pressKit.PictureUrl %>"/>
                        </div>
                        <div id="rightContent" >
                            <h1><%: pressKit.Title %></h1>
                            <p>
                                <%= pressKit.Body%>
                            </p>
                        </div>
                    </div>
                </div>
            
            <% } %>            
            <div style="clear: both;"></div>
        </div>
      
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
