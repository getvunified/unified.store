﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.PressKitItem>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Difference Media
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="<%: Url.Content("~/Content/dm/scripts/press.js?version=1") %>" type="text/javascript"></script>
    <link href="<%: Url.Content("~/Content/dm/css/press.css?version=1")%>" media="all" rel="stylesheet" type="text/css" /> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%: Html.DifferenceMediaTitleHeader(Model.Tile + " Press Kit") %>
    
    <div style="padding: 15px;">
        <img src="<%: Model.PressKitPicture %>" class="pressPhoto" width="250" alt="<%: Model.Tile %>"/>
        <p>
            <%= Model.Body %>
        </p>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
