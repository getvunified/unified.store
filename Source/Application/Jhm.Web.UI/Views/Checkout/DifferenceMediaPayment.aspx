<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.em3.Core" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Checkout
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {%>
    <script type="text/javascript">
        location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
    </script>
    <%
       }
       else
       { 
    %>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/checkout.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    
    <%--<%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] {"PhoneCode" })%>--%>
    <script type="text/javascript">
        var addresses = <%= ViewData["jsonAddressesList"] %>;
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
		    $('#dialogModal').dialog({
				    autoOpen: false,
				    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 500
		    });
              <%= ViewData["ShowLoginScript"] %>
            $('#opener,#billingOpener').click(function () { $('#dialogModal').dialog('open'); return false; });
            <%= ViewData["ShowNewAddressPopupScript"] %>
            populateBillingAddressFields(document.getElementById("BillingAddressId"));
        });
    </script>
    <%}%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("Store") %>
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } %>
    <div id="dialogModal" title="Add New Billing Address">
        <div class="section-checkout">
            <div class="block">
                <p>
                    Complete the form below to add a new billing address.</p>
                <%
                    using (Html.BeginForm())
                    {%>
                <%= Html.ValidationSummary("Adding new address was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                <table>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>Address Line 1:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("Address1",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("Address1") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Address line 2:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("Address2",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>City:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("City", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32", @value = "" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("City") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>Country:
                            </label>
                        </td>
                        <td>
                            <%: Html.DropDownList
                                (
                                    "Country",
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'StateCode')"
                                        }
                                )%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("Country") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>State/Province/Division:
                            </label>
                        </td>
                        <td>
                            <%: Html.DropDownList("StateCode", State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:215px" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("StateCode") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Postal code:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("PostalCode", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("PostalCode")%></span>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <div style="text-align: right;">
                    <%:Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address",true,"Adding new address...", MagicStringEliminator.CheckoutActions.AddAddress), "PopupActionBottonClicked")%>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="easybutton" href="javascript:void(0);"
                        onclick="$('#dialogModal').dialog('close');"><span class="">Cancel</span> </a>
                </div>
                <%        
                        }
                %>
            </div>
        </div>
    </div>
    <div id="storeBoxWrap">
        <div id="storeBox">
            <h1>
                Checkout</h1>
            <%  if (Model.ContainsStockItem())
                {%>
            <table width="100%" border="0" cellpadding="5">
                <thead>
                    <tr>
                        <th class="product">
                            Description
                        </th>
                        <th class="price">
                            Qty.
                        </th>
                        <th class="qty">
                            Price
                        </th>
                        <th class="total">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <%  foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem))
                        {
                            Html.RenderPartial("PartialViews/DifferenceMedia/Store/_CartLineItemDisplay", cartItem);
                        } %>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <% if (Model.DiscountApplied != null)
                       { %>
                    <tr>
                        <td class="cartLabel" colspan="3">
                            Subtotal before discount:
                        </td>
                        <td class="cost vertical-padding-thin">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotal) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="cartLabel" colspan="3">
                            <%: Model.DiscountApplied.ShortDescription %>:
                        </td>
                        <td class="cost vertical-padding-thin" style="width: 120px;">
                            <%: Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1) %>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="cartLabel">
                            Subtotal:
                        </td>
                        <td class="cost">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount) %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="cartLabel">
                            Shipping:
                        </td>
                        <td class="cost">
                            <%:Model.FormatWithCompanyCulture("C", Model.ShippingCost)%>
                        </td>
                    </tr>
                    <tr class="row-total">
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="cartLabel">
                                TOTAL:
                            </td>
                            <td class="cost">
                                <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                            </td>
                        </tr>
                </tfoot>
            </table>
            <hr />
            <% }
                using (Html.BeginForm())
                { %>
            <%:Html.ValidationSummary(false, "Your request was not successful. Please correct the errors and try again.", new {@class = "messages error"})%>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <div class="boxL">
                <h2>
                    Customer Information</h2>
                <p>
                    <strong>First Name:</strong>
                    <%: Html.DisplayTextFor(m => m.FirstName)%>
                </p>
                <p>
                    <strong>Last Name:</strong>
                    <%: Html.DisplayTextFor(m => m.LastName)%>
                </p>
                <p>
                    <strong>Email Address:</strong>
                    <%: Html.DisplayTextFor(m => m.Email)%>
                </p>
                <p>
                    <strong>Phone:</strong>
                    <%: Model.Profile.PrimaryPhone == null ? "none" : String.Format("{0} {1}-{2}",Model.Profile.PrimaryPhone.CountryCode, Model.Profile.PrimaryPhone.AreaCode, Model.Profile.PrimaryPhone.Number)%></p>
            </div>
            <div class="boxR">
                <h2>
                    Billing Address</h2>
                <p>
                    <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Add_Address');" id="billingOpener">Add New Billing Address</a>
                    <%: Html.ValidationMessageFor(m => m.BillingAddressId)%>
                </p>
            </div>
            <%  } %>
            <div style="clear: both;">
            </div>
            <hr />
            <div>
                <div>
                    <h2>
                        Payment Information</h2>
                    <p>
                        Enter your payment information here.
                    </p>
                    <% if (Model.ShouldDisplayPaymentMethods)
                       {
                           //IMPORTANT: This partial view cannot be inside a form since it contains its own forms for each payment type
                           //Payment method forms won't work properly if forms are nested.
                           Html.RenderPartial("PartialViews/_EditCheckoutPayments", Model.PaymentList);
                       }
                       else
                       {%>
                        <div class="description" style="font-size: larger;">
                            Your order does not require a payment.
                        </div>
                    <%}%>
                    <br style="clear: both;" />
                    <hr />
                        
                    <% if (Model.PaymentList.Balance != 0)
                           { %>
                                <div style="clear: both; text-align: right;">
                                    <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Payment_Cancel');">
                                        <img src="<%: Url.Content("~/Content/dm/images/btn_previous.jpg") %>" /></a>
                                    <a href="javascript:void(0);" onclick="alert('Your remaining balance is <%:Model.PaymentList.Balance.ToString("C") %>. Please add or remove a payment method to balance your payment.'); addPaymentType(); highlightRemoveButtons(); return false;">
                                        <img src="<%: Url.Content("~/Content/dm/images/btn_next.jpg") %>" /></a>
                                </div>
                    <% }
                    else
                    { 
                        using (Html.BeginForm())
                        { %>
                            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                            <div style="clear: both; text-align: right;">
                                <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Payment_Cancel');">
                                    <img src="<%: Url.Content("~/Content/dm/images/btn_previous.jpg") %>" /></a>
                                <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Review');">
                                    <img src="<%: Url.Content("~/Content/dm/images/btn_next.jpg") %>" /></a>
                            </div>
                        <% } %>
                    <% } %>
                </div>
            </div>
            
        </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
</asp:Content>
