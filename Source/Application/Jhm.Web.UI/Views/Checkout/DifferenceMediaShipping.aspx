<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.em3.Core" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Checkout
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {%>
    <script type="text/javascript">
        location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
    </script>
    <%
       }
       else
       { 
    %>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] {"PhoneCode" })%>
    <script type="text/javascript">
        var addresses = <%= ViewData["jsonAddressesList"] %>;
        var nopobox = <%= ViewData["jsonNoPOBoxMethods"] %>;
        function PerformPoBoxValidation(ddShippingMethod) {
            if (ddShippingMethod != null && !ShippingMethodAllowsPOBoxes(ddShippingMethod.value)) {
                var pattern = new RegExp('[PO.]*\\s?B(ox)?.*\\d+', 'i');
                var address = $('#ShippingAddress_Address1').val() + ' ' + $('ShippingAddress_Address2').val();
                if (address.match(pattern)) {
                    $('.no-pobox-msg').show();
                    return false;
                }
            }
            $('.no-pobox-msg').hide();
            return true;
        }

        function ShippingMethodAllowsPOBoxes(id) {
            for (var i = 0; i < nopobox.length; i++) {
                if (nopobox[i].Id == id) {
                    return false;
                }
            }
            return true;
        }
    </script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/checkout.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
		    $('#dialogModal').dialog({
				    autoOpen: false,
				    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 500
            });
            $('#opener,#billingOpener').click(function () { $('#dialogModal').dialog('open'); return false; });
            <%= ViewData["ShowNewAddressPopupScript"] %>
            populateBillingAddressFields(document.getElementById("BillingAddressId"));
            populateAddressFields(document.getElementById("ShippingAddressId"),'<%= ClientCompany.InternationalShippingMethod.Value %>','<%= ClientCompany.DefaultCountry.Iso %>',countryCodes);
        });
    </script>
    <%}%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("Store") %>
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } %>
    <div id="dialogModal" title="Add New Shipping Address">
        <div class="section-checkout">
            <div class="block">
                <p>
                    Complete the form below to add a new shipping address.</p>
                <%
                    using (Html.BeginForm())
                    {%>
                <%= Html.ValidationSummary("Adding new address was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                <table>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>Address Line 1:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("Address1",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("Address1") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Address line 2:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("Address2",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>City:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("City", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32", @value = "" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("City") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>Country:
                            </label>
                        </td>
                        <td>
                            <%: Html.DropDownList
                                (
                                    "Country",
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'StateCode')"
                                        }
                                )%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("Country") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                <span class="form-required">*</span>State/Province/Division:
                            </label>
                        </td>
                        <td>
                            <%: Html.DropDownList("StateCode", State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:215px" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("StateCode") %></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Postal code:
                            </label>
                        </td>
                        <td>
                            <%: Html.TextBox("PostalCode", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                            <span class="form-required" style="font-size: small;">
                                <%: Html.ValidationMessage("PostalCode")%></span>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <div style="text-align: right;">
                    <%:Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address",true,"Adding new address...", MagicStringEliminator.CheckoutActions.AddAddress), "PopupActionBottonClicked")%>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="easybutton" href="javascript:void(0);"
                        onclick="$('#dialogModal').dialog('close');"><span class="">Cancel</span> </a>
                </div>
                <%        
                        }
                %>
            </div>
        </div>
    </div>
    <div id="storeBoxWrap">
        <div id="storeBox">
            <h1>
                Checkout</h1>
            <%  if (Model.ContainsStockItem())
                {%>
            <table width="100%" border="0" cellpadding="5">
                <thead>
                    <tr>
                        <th class="product">
                            Description
                        </th>
                        <th class="price">
                            Qty.
                        </th>
                        <th class="qty">
                            Price
                        </th>
                        <th class="total">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <%  foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem))
                        {
                            Html.RenderPartial("PartialViews/DifferenceMedia/Store/_CartLineItemDisplay", cartItem);
                        } %>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <% if (Model.DiscountApplied != null)
                       { %>
                    <tr>
                        <td class="cartLabel" colspan="3">
                            Subtotal before discount:
                        </td>
                        <td class="cost vertical-padding-thin">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotal) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="cartLabel" colspan="3">
                            <%: Model.DiscountApplied.ShortDescription %>:
                        </td>
                        <td class="cost vertical-padding-thin" style="width: 120px;">
                            <%: Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1) %>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="cartLabel">
                            Subtotal:
                        </td>
                        <td class="cost">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount) %>
                        </td>
                    </tr>
                </tfoot>
            </table>
            <% }
                using (Html.BeginForm())
                { %>
            <%:Html.ValidationSummary(false, "Your request was not successful. Please correct the errors and try again.", new {@class = "messages error"})%>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            
            <div style="clear: both;">
            </div>
            <hr />
            <div class="boxL">
                <h2>
                    Shipping Information</h2>
                <div>
                    Select your delivery address and information from the list below.</div>
                <table width="100%" border="0" cellpadding="5">
                    <tr>
                        <td width="48%">
                            <label>
                                Select Address</label>
                        </td>
                        <td width="52%">
                            <%: Html.DropDownListFor
                    (
                    m => m.ShippingAddressId,
                    new SelectList(Model.AddressList?? new List<Address>(), "DSAddressId", "Address1"),
                    "(Select an address)",
                    new
                    {
                        @class = "form-select required",
                        @style = "width:160px",
                        @tabindex = "2",
                        @onChange = "populateAddressFields(this,'"+ ClientCompany.InternationalShippingMethod.Value +"','"+ ClientCompany.DefaultCountry.Iso +"',countryCodes);PerformPoBoxValidation(document.getElementById('ShippingMethodValue'));"
                    }
                    )%>
                            <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Add_Address');" id="opener">
                                Add</a> <span class="form-required">
                                    <%: Html.ValidationMessageFor(m => m.ShippingAddressId)%>
                                </span><span class="form-required no-pobox-msg" style="display: none;">
                                    <br />
                                    * Sorry, No PO Boxes allowed using selected shipping method</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                First Name</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingFirstName, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                            <span class="form-required">
                                <%: Html.ValidationMessageFor(m => m.ShippingFirstName)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Last Name</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingLastName, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                            <span class="form-required">
                                <%: Html.ValidationMessageFor(m => m.ShippingLastName)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Company</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingCompany, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Address Line 1</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingAddress.Address1, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled="disabled" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Address Line 2</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingAddress.Address2, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled="disabled" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Country</label>
                        </td>
                        <td>
                            <%: Html.DropDownListFor
                    (
                        m => m.ShippingAddress.Country,
                        Country.GetAllSortedByDisplayName<Country>().ToMVCSelectList("Iso", "Name", "840"),
                        String.Empty,
                        new
                            {
                                @class = "form-select required", 
                                @style = "width:215px",
                                @onchange = "set_city_state(this,'ShippingAddress_StateCode')",
                                @disabled = "disabled"
                            }
                    )%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                State/Province/Division:</label>
                        </td>
                        <td>
                            <%: Html.DropDownListFor(model => model.ShippingAddress.StateCode, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:210px", @disabled = "disabled" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                City</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingAddress.City, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled = "disabled" })%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Postal Code:</label>
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.ShippingAddress.PostalCode, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled = "disabled" })%>
                        </td>
                    </tr>
                </table>
                <p>
                    &nbsp;</p>
            </div>
            <div class="boxR">
                <h2>
                    Shipping Method</h2>
                <% if (Model.ShouldDisplayShippingMethods)
                   { %>
                <div>
                    Select a Shipping method from the list below
                </div>
                <%:Html.DropDownListFor
                (
                    m => m.ShippingMethodValue,
                    Model.ShippingMethods.ToMVCSelectList("Value", "DisplayName"),
                                                        "Select Shipping Method",
                                                        new { @class = "form-select required", @style = "width:415px;", @onchange = "PerformPoBoxValidation(this);" }
                )%>
                <span class="form-required">
                    <%:Html.ValidationMessageFor(m => m.ShippingMethod)%>
                    <span class="no-pobox-msg" style="display: none;">
                        <br />
                        * Sorry, No PO Boxes allowed using selected shipping method</span> </span>
                <%
                       if (ClientCompany == Company.JHM_Canada)
                       {%>
                <br />
                <br />
                <strong>Note:</strong> Shipping will be based on product availability; please allow
                an additional 2-3 weeks for processing.
                <% }
                   }
                   else
                   {%>
                <div class="description" style="font-size: larger;">
                    Your order has no shipping cost.
                </div>
                <%
                }
                %>
            </div>
            <div style="clear: both;">
            </div>
            <div>
                <div>
                    <br style="clear: both;" />
                    <hr />
                    <div style="clear: both; text-align: right;">
                        <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Payment_Cancel');">
                            <img src="<%: Url.Content("~/Content/dm/images/btn_previous.jpg") %>" /></a>
                        <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Shipping');">
                            <img src="<%: Url.Content("~/Content/dm/images/btn_next.jpg") %>" /></a>
                    </div>
                </div>
            </div>
            <%  } %>
        </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
</asp:Content>
