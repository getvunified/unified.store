﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutShoppingCartViewModel>" %>

<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Shopping Cart
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
	    $(document).ready(function() {
		    $('#dialogModal').dialog({
				    autoOpen: false,
				    bgiframe: true,
                    modal: true,
                    resizable: false,
                    width: 500
		    });
	        <% if (!Request.IsAuthenticated)
               { %>
	        $('#opener').click(function () {
	            $('#dialogModal').dialog('open');
	              return false;
	          });
	       
	        
              <% } %>
	        
	    });
        function validateInputs(){
            if ( $('#UserName').val() == '' ){
                $('#rfvUsername').css("display","");
                return false;
            }
            $('#rfvUsername').css("display","none");
            if ( $('#Password').val() == '' ){
                $('#rfvPassword').css("display","");
                return false;
            }
            $('#rfvPassword').css("display","none");
            return true;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="dialogModal" title="Login or Register">
        <p>
            Sorry, you need to have an account in our system in order to continue to checkout.
            Please login using your username and password or click on Register if you don't
            have a username and password.</p>
        <%
            using (Html.BeginForm("ShoppingCart_Logon"))
            {%>
                <%= Html.ValidationSummary("Logon was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                <dl>
                    <dt>Username:</dt>
                    <dd>
                        <%: Html.TextBoxFor(m => m.UserName)%>
                        <%: Html.ActionLink("Forgot Username? Click here", "ForgotUsername", "Account", null, new { @title = "Click here if you forgot your username", @style = "color:#165C86; font-weight:bold; font-size:14px;" })%>
                        <span id="rfvUsername" style="color: red; display: none;">
                            <br />
                            * Username is required</span>
                    </dd>
                    <dt>Password:</dt>
                    <dd>
                        <%: Html.PasswordFor(m => m.Password)%>
                        <%: Html.ActionLink("Forgot Password? Click here", "ForgotPassword", "Account", null, new { @title = "Click here if you forgot your password", @style = "color:#165C86; font-weight:bold; font-size:14px;" })%>
                        <span id="rfvPassword" style="color: red; display: none;">* Password is required</span>
                    </dd>
                    <dt></dt>
                </dl>
                <br/>
                <a class="DMButton" href="javascript:void(0);" onclick="if ( validateInputs() ) { $('#easyButtonDialogModal').dialog('option','title','Signing you in...');$('#easyButtonDialogModal').dialog('open'); EasyButtonSubmitFormPopupActionBottonClicked('PopupActionBottonClicked','Logon'); return true; } return false;">
                    <img src="<%: Url.Content("~/Content/dm/images/btn_login.jpg") %>" />
                </a>
                OR
                <a class="DMButton" href="javascript:EasyButtonSubmitFormPopupActionBottonClicked('PopupActionBottonClicked','Register');">
                    <img src="<%: Url.Content("~/Content/dm/images/btn_register.jpg") %>" />
                </a>
        <% } %>
    </div>
    <%: Html.DifferenceMediaTitleHeader("Store") %>
    <div id="storeBoxWrap">
    <div id="storeBox">
        <h1>
            Your Cart</h1>
        <%  using (Html.BeginForm())
            {%>
        <%:Html.ValidationSummary(false,"Updating Cart was unsuccessful. Please correct the errors and try again.",new { @class = "messages error" })%>
        <%: Html.FormKeyValueMatchAttributeHiddenField()%>
        <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
        <%  if (Model.ContainsStockItem())
            { %>
        <table style="width: 100%;" border="0" cellpadding="5">
            <thead>
                <tr>
                    <th width="9%">
                        Remove
                    </th>
                    <th width="48%">
                        Description
                    </th>
                    <th width="13%">
                        Qty.
                    </th>
                    <th width="15%">
                        Price
                    </th>
                    <th width="15%">
                        Total
                    </th>
                </tr>
            </thead>
            <tbody valign="top">
                <tr>
                    <td colspan="5">
                        <hr />
                    </td>
                </tr>
                <%
                    foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem))
                    {%>
                <%: Html.Partial("PartialViews/DifferenceMedia/Store/_CartLineItemEdit",cartItem) %>
                <%}%>
                <tr>
                    <td colspan="5">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="cartLabel">
                            Price Subtotal:</div>
                    </td>
                    <td>
                        <div align="center">
                            <%: Model.FormatWithCompanyCulture("C", Model.StockItemsCartSubTotal) %></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="cartLabel">
                            Grand Total:</div>
                    </td>
                    <td>
                        <div align="center"><%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <label for="promoCode" style="margin-top:8px;float:left;">
                            Promotional Code</label>
                        <%: Html.TextBox("DiscountCode", null, new {style="width: 150px;;margin:10px;float:left;"})%>
                        
                        <a class="DMButton" href="javascript:EasyButtonSubmitForm('ActionBottonClicked','ShoppingCart_ApplyDiscountCode');">
                            <img alt="" src="<%: Url.Content("~/Content/dm/images/btn_apply.jpg") %>" />
                         </a><br/>
                         <%: Html.ValidationMessage("DiscountCode", "*", new { style = "color:red" })%>
                         <%: Html.ValidationMessage("DiscountCode",new {style="color:red"}) %>
                    </td>
                    <td valign="top">
                        &nbsp;
                    </td>
                    <td valign="top">
                        <a class="DMButton" href="javascript:EasyButtonSubmitForm('ActionBottonClicked','ShoppingCart_UpdateCart');">
                            <img src="<%: Url.Content("~/Content/dm/images/btn_update.jpg") %>" /></a>
                    </td>
                    <td valign="top">
                        <a class="DMButton" href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Checkout');" id="opener">
                            <img src="<%: Url.Content("~/Content/dm/images/btn_checkout.jpg") %>" />
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
        <%}
            else
            {%>
        <h6>
            Empty</h6>
        <%} %>
        <%--<div class="content">
            <table class="store mycart" style="width: 100%;">
                <tfoot>
                    <% if (Model.DiscountApplied != null)
                       { %>
                    <tr>
                        <td class="subtotal vertical-padding-thin">
                            Subtotal:
                        </td>
                        <td class="cost vertical-padding-thin" style="width: 120px;">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtotal vertical-padding-thin">
                            <%:Model.DiscountApplied.ShortDescription%>
                            (<a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','<%= MagicStringEliminator.CheckoutActions.ClearDiscount %>');">Remove
                                code</a>):
                            <% if (Model.DiscountApplied.MaxAmountOff > 0 && !Model.DiscountApplied.IsFixedAmount)
                               { %>
                            <br />
                            <span style="font-size: 12px; font-style: italic">Maximun Amount Off:
                                <%: Model.FormatWithCompanyCulture("C", Model.DiscountApplied.MaxAmountOff)%>&nbsp;&nbsp;&nbsp;</span>
                            <% } %>
                        </td>
                        <td class="cost vertical-padding-thin" style="width: 120px;">
                            <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td class="subtotal">
                            Grand Total:
                        </td>
                        <td class="cost" style="width: 120px;">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>--%>
        <%--<%if (Model.CartItems.IsNot().Empty())
          { %>
        <div class="content">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">
                        Promotional Code:
                        <%: Html.TextBox("DiscountCode", null, new {style="width: 150px;"})%><%: Html.ValidationMessage("DiscountCode","*",new {style="color:red"}) %>&nbsp;
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Apply code", MagicStringEliminator.CheckoutActions.ApplyDiscountCode, null))%><br />
                        <%: Html.ValidationMessage("DiscountCode",new {style="color:red"}) %>
                    </td>
                    <td style="width: 50%; text-align: right;">
                        <%: Html.FormKeyValueMatchAttribute_EasyButtonDiv
                            (
                                new List<EasyButton>
                                    {
                                        new EasyButton(MagicStringEliminator.EasyButtonLabel.Update, MagicStringEliminator.CheckoutActions.UpdateCart), 
                                        new EasyButton(MagicStringEliminator.EasyButtonLabel.Checkout,true,"Preparing to checkout...", MagicStringEliminator.CheckoutActions.Payment,null,new{@id="opener"})
                                    }
                            )%>
                    </td>
                </tr>
            </table>
        </div>
        <%} %>--%>
        <%}%>
    </div>
    </div>
</asp:Content>
