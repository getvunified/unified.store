﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Order Review
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        <% if (ViewData["RedirectToShoppingCartScript"] != null )
           {%>
           location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
        <%
           }%>           
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } 
    %>
    <div id="section-review">
        <div id="content-area">
            <div class="block">
                <h2 class="title">
                    Please review order and click place order to submit</h2>
                <div class="content clearfix">
                    <div class="col-1">
                        <div class="col-inner">
                            <p id="billing-info" class="">
                                <span>Customer Information:</span><br />
                                
                                <label>
                            First name:</label>
                        <%: Html.DisplayTextFor(m => m.FirstName)%>
                        <br />
                        <label>
                            Last name:</label>
                        <%: Html.DisplayTextFor(m => m.LastName)%>
                        <br />
                        <label>
                            Email Address:</label>
                        <%: Html.DisplayTextFor(m => m.Email)%>
                        <br />
                        <label>
                            Phone:</label>
                        <%: Model.Profile.PrimaryPhone == null ? "none" : String.Format("{0} {1}-{2}",Model.Profile.PrimaryPhone.CountryCode, Model.Profile.PrimaryPhone.AreaCode, Model.Profile.PrimaryPhone.Number)%>

                                
                            </p>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="col-inner">
                            <p id="billing-info">
                                <%--<span>Customer Information:</span>
                                <br />--%>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="section-checkout">
                <div class="block">
                    <h2 class="title">
                        Shipping To <span class="col-2">Shipping Method</span></h2>
                    <div class="content clearfix">
                        <div class="col-1">
                            <div class="col-inner" >
                                <% if (Model.ShippingAddress != null)
                                { %>
                                <p id="shipping-info" class="">
                                    <span>Shipping Information:</span><br />
                                    <%: StringExtensions.FormatWith("{0} {1}",Model.ShippingFirstName,Model.ShippingLastName) %><br />
                                    <%: Model.ShippingAddress.Address1 %><br />
                                    <%= String.IsNullOrEmpty(Model.ShippingAddress.Address2) ? String.Empty : Model.ShippingAddress.Address2 + "<br />"%>
                                    <%: StringExtensions.FormatWith("{0}, {1} {2}", Model.ShippingAddress.City, Model.ShippingAddress.StateCode, Model.ShippingAddress.PostalCode)%><br />
                                    <%: Model.ShippingAddressCountry %><br />
                               </p>
                            <% } %>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="col-inner">
                            <% if (Model.ShouldDisplayShippingMethods)
                                    {%>
                                <label>
                                    Shipping Method:</label>
                                <div class="form-item">
                                    <%:Model.ShippingMethod%>
                                     </div>
                                     <%
                                         
                                    }
                                    else
                                    {%>
                                     <div class="description" style="font-size: larger;">
                                   Your order does not require a Shipping Method.</div>
                                  
                                <%}%>
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="block">
                <h2 class="title">Payment Information</h2>
                <div class="content">
                    
                            <% if (Model.ShouldDisplayPaymentMethods)
                               {
                                   Html.RenderPartial("PartialViews/_ViewCheckoutPayments", Model.PaymentList);
                               }
                               else
                               { %>
                                    <div class="description" style="font-size: larger;">
                                        Your order does not require a payment.
                                    </div>
                            <% } %>
                        
                </div>
            </div>
            

            <div class="block">
                <h2 class="title">Checkout</h2>
                <div class="content">
                    <table class="store checkout">
                        <%  if (Model.ContainsDonations())
                            {%>
                            <thead>
                                <tr>
                                    <td class="product">
                                        Donations
                                    </td>
                                    <td class="price">
                                        &nbsp;
                                    </td>
                                    <td class="qty">
                                        &nbsp;
                                    </td>
                                    <td class="total">
                                        Total
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    foreach (var cartItem in Model.CartItems.Where(x => x.Item is DonationCartItem))
                                    {%>
                                <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                <%}%>
                            </tbody>
                        <% }%>
                            
                        <%  if (Model.ContainsSubscriptionItems())
                            {%>
                            <thead>
                                <tr>
                                    <td class="product">
                                        Subscriptions
                                    </td>
                                    <td class="price">
                                        &nbsp;
                                    </td>
                                    <td class="qty">
                                        &nbsp;
                                    </td>
                                    <td class="total">
                                        Total
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    foreach (var cartItem in Model.CartItems.Where(x => x.Item is SubscriptionCartItem))
                                    {%>
                                        <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                    <%}%>
                            </tbody>
                        <% }%>

                        <%  if (Model.ContainsStockItem() || Model.ContainsGifCards())
                            { %>
                        <thead>
                            <tr style="border: medium none;">
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        
                            <tr>
                                <td class="product">
                                    Products
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem || x.Item is GiftCardCartItem))
                                {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        <%  if (Model.PromotionalItems.Any())
                            { %>
                            <thead>
                        <tr style="border: medium none;">
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        
                            <tr>
                                <td class="product">
                                    Promotional Items
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.PromotionalItems)
                                {%>
                            <%=Html.RenderPartialDisplayByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        <tfoot>
                            <% if (Model.DiscountApplied != null)
                               { %>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    Subtotal before discount:
                                </td>
                                <td class="cost vertical-padding-thin">
                                    <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    <% if (Model.DiscountApplied.ActiveRule.AppliesOnlyToItemsMatchingCriteria)
                                       { %>
                                            <img height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
                                    <% } %>
                                    <%:Model.DiscountApplied.ShortDescription%>:
                                </td>
                                <td class="cost vertical-padding-thin" style="width: 120px;">
                                    <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="subtotal">
                                    Subtotal:
                                </td>
                                <td class="cost">
                                    <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="shipping">
                                    Shipping:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.ShippingCost)%>
                                </td>
                            </tr>
                            <tr class="row-tax">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="tax">
                                    Tax:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.TaxCost)%>
                                </td>
                            </tr>
                            <tr class="row-total">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="order-total">
                                    TOTAL:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <% using (Html.BeginForm())
                   {%>
            <%:Html.FormKeyValueMatchAttributeHiddenField()%>
            <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                <div class="content">
                    <div class="checkout-controls">
                        <%:
                           Html.FormKeyValueMatchAttribute_EasyButtonDiv(new List<EasyButton>
                                                                             {
                                                                                 new EasyButton("Change My Order",
                                                                                                "OrderReview_Cancel"),
                                                                                 new EasyButton("Process",true,"Placing your order...","ProcessOrder")
                                                                             })%>
                    </div>
                </div>
                <%
                   } %>
            </div>
        </div>
    </div>
</asp:Content>
