﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.CreditCardViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="Jhm.em3.Core" %>


<% using (Html.BeginForm("Payment_AddCreditCard", "Checkout", FormMethod.Post, new { id = "addCreditCardForm" }))
   { %>
        <label style="padding-top: 3px;">
            Credit Card:
        </label>
        <% if (!Model.CreditCardsList.Is().Empty())
           {%>
        <div class="form-item" style="width: 480px;">
            <%:Html.DropDownListFor
                        (
                            m => m.SelectedCreditCardId,
                            Model.CreditCardsList.ToMVCSelectList("Value", "DisplayName"),
                                                                "Select Credit Card",
                            new { @class = "form-select required",@onChange="toggleCreditCardForm(false);" }
                        )%>
            <a class="easybutton " href="javascript:toggleCreditCardForm(true);$('#SelectedCreditCardId').valid();" style="min-width: 40px;
                padding: 2px 5px 0; min-height: 20px;"><span class="">Use different card</span></a>
            <span class="form-required">
                <br />
                <%: Html.ValidationMessageFor(m => m.SelectedCreditCardId) %>
            </span>
        </div>
        <% } %>
        <br style="clear: both;" />

        <%: Html.HiddenFor(m => m.IsUsingNewCreditCard)%>
        <%: Html.HiddenFor(m => m.Token)%>
        <div id="creditCardForm" style='<%= Model.CreditCardsList.Is().Empty() || (ViewData["ShouldShowCreditCardForm"] != null && ((bool) ViewData["ShouldShowCreditCardForm"])) ? "": "display:none;" %>'>
            <table style="width: 400px">
                <col width="150" />
                <col />
                <tr>
                    <td class="description" colspan="2">Enter your credit card information here:
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="form-item checkbox">Credit Card Type:
                                                                        <img id="Img1" class="visaLogo" alt="Visa" title="Visa" runat="server" src="~/Content/images/credit-visa.gif" />
                        <img id="Img2" class="mastercardLogo" alt="Mastercard" title="Master Card" runat="server"
                            src="~/Content/images/credit-master.gif" />
                        <img id="Img3" class="discoverLogo" alt="Discover" title="Discover" runat="server"
                            src="~/Content/images/credit-discover.gif" />
                        <img id="Img4" class="amexLogo" alt="Amex" title="American Express" runat="server"
                            src="~/Content/images/credit-amex.gif" />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <span class="form-required">*</span>Name on Card:</label>
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.NameOnCard, new {@class = "form-text required", @size = "32", @maxlength = "32", @style = "width:210px;"}) %>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.NameOnCard) %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <span class="form-required">*</span>Card Number:</label>
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.CreditCardNumber, new {@class = "form-text required", @size = "32", @maxlength = "32", @style = "width:210px;", @autocomplete = "off"}) %>
                        <img id="ccOk" alt="" title="Credit Card Number is correct" src="<%: Url.Content("~/Content/images/ok.gif") %>" />
                        <span id="invalidCreditCard" style="display: none;">
                            <img id="ccInvalid" alt="" title="Credit Card Number is invalid" src="<%: Url.Content("~/Content/images/error_16.png") %>" />
                            <span class="form-required">Credit Card number is invalid. </span></span><span class="form-required">
                                <%: Html.ValidationMessageFor(m => m.CreditCardNumber) %>
                            </span>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                        <label>
                            <span class="form-required">*</span>Card Type:</label>
                    </td>
                    <td>
                        <%: Html.DropDownListFor
                                            (
                                                m => m.CreditCardType,
                                                CreditCardType.GetAll<CreditCardType>().ToMVCSelectList("Value", "DisplayName")
                                            ) %>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.CreditCardType) %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <span class="form-required">*</span>Expiration Date:</label>
                    </td>
                    <td>
                        <%: Html.DropDownListFor
                                            (
                                                m => m.CCExpirationMonth,
                                                new SelectList
                                                    (
                                                    new List<object>
                                                        {
                                                            new {text = String.Empty, value = String.Empty},
                                                            new {text = "01 - January", value = "01"},
                                                            new {text = "02 - February", value = "02"},
                                                            new {text = "03 - March", value = "03"},
                                                            new {text = "04 - April", value = "04"},
                                                            new {text = "05 - May", value = "05"},
                                                            new {text = "06 - June", value = "06"},
                                                            new {text = "07 - July", value = "07"},
                                                            new {text = "08 - August", value = "08"},
                                                            new {text = "09 - September", value = "09"},
                                                            new {text = "10 - October", value = "10"},
                                                            new {text = "11 - November", value = "11"},
                                                            new {text = "12 - December", value = "12"}
                                                        },
                                                    "value",
                                                    "text",
                                                    String.Empty
                                                    ),
                                                new {@class = "form-select required"}
                                            ) %>
                        <%: Html.DropDownListFor(m => m.CCExpirationYear, Model.GetNext10YearsList(), String.Empty) %>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.CCExpirationMonth) %>
                        </span><span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.CCExpirationYear) %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <span class="form-required">*</span>CV2/CID:</label>
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.CID, new {@class = "form-text required", @size = "4", @maxlength = "4", @autocomplete = "off"}) %>
                        <span class="form-required">
                            <%: Html.ValidationMessageFor(m => m.CID) %>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top">
                        <label style="padding-top: 3px;">
                            <span class="form-required">*</span>Billing Address:
                        </label>
                    </td>
                    <td>
                        <div class="form-item" style="margin-top:0;">
                            <%: Html.DropDownListFor
                                (
                                m => m.BillingAddressId,
                                new SelectList(Model.AddressList?? new List<Address>(), "DSAddressId", "Address1"),
                                "(Select an address)",
                                new
                                {
                                    @class = "form-select required",
                                    @style = "width:260px;word-wrap: break-word",
                                    @tabindex = "2",
                                    @onChange = "populateBillingAddressFields(this);"
                                }
                                )%>
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address ", "Add_Address", String.Empty, new { @style = "min-width:40px; padding: 2px 5px 0;min-height: 20px;", @id = "billingOpener" }))%>
                            <span class="form-required" style="word-break:break-all">
                                <%: Html.ValidationMessageFor(m => m.BillingAddressId)%>
                            </span>
                        </div>
                        <div class="form-item">
                            <span id="spBillingAddress"></span><br/>
                            <span id="spBillingCity"></span><span id="spBillingState"></span><span id="spBillingZip"></span><br/>
                            <span id="spBillingCountry"></span><br/>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <%: Html.CheckBoxFor(m => m.ShouldSaveCreditCard) %><label for="ShouldSaveCreditCard"
                            style="float: none; display: inline">Save this credit card for future use</label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="form-item" style="width: 400px;">
            <label style="padding-top: 3px;">
                Payment Amount:
            </label>
            <br />
            <label>
                <input type="checkbox" id="ccUseBalance" onchange="toggleCreditCardAmount(this);" checked="checked" />
                Use this card to pay remaining balance due: <%: Model.FormatWithCompanyCulture("C",Model.Balance) %>
            </label>
            <span id="ccAmountBoxHolder">
                <br />
                Pay Different Amount:<br />
                <%: Html.TextBoxFor(m => m.Amount,new{id="CreditCardAmount"}) %>
                <span class="form-required">
                    <br />
                    <%: Html.ValidationMessageFor(m => m.Amount) %>
                </span>
            </span>
        </div>
        <br />
        <span id="creditCardMessage" class="field-validation-error"></span>
        <br />

        <input id="submitButton" type="button" value="Add Payment" onclick="checkCreditCardAndSubmit();"  />
<% } %>

<script type="text/javascript">
    function toggleCreditCardAmount(checkbox) {
        var amountBox = document.getElementById("CreditCardAmount");
        if (checkbox.checked) {
            amountBox.value = '<%: Model.Balance.ToString ("#.##") %>';
            $("#ccAmountBoxHolder").hide();
        } else {
            amountBox.value = '';
            $("#ccAmountBoxHolder").show();
        }
        
    }

    jQuery(function() {
        $("#addCreditCardForm").validate({
            rules: {
                SelectedCreditCardId: {
                    required: function(element) {
                        return $("#IsUsingNewCreditCard").val() == "False" || $("#IsUsingNewCreditCard").val() == "false";
                    }
                },
                Amount: {
                    required: function (element) {
                        return !($("#ccUseBalance").is(':checked'));
                    },
                    number: true,
                    min: 0.1
                },
                NameOnCard: {
                    required: "#creditCardForm:visible"
                },
                CreditCardNumber: {
                    required: "#creditCardForm:visible",
                    creditcard: true
                },
                CCExpirationMonth: {
                    required: "#creditCardForm:visible"
                },
                CCExpirationYear: {
                    required: "#creditCardForm:visible"
                },
                CID: {
                    required: "#creditCardForm:visible",
                    digits: true
                },
                BillingAddressId: {
                    required: "#creditCardForm:visible"
                }
            },
            messages: {
                SelectedCreditCardId: {
                    required: "Please select a Credit card from the list or use a different card"
                },
                Amount: {
                    required: "Please enter an amount or use remaining balance"
                }
            },
            submitHandler: function (form) {
                $('#CreditCardType').disabled = false;
                form.submit();
            }
        });
        toggleCreditCardAmount(document.getElementById("ccUseBalance"));
    });
    
    function toggleCCSubmitButton(shouldEnable) {
        var mySubmitButton = $("#submitButton");
        if (shouldEnable) {
            mySubmitButton.removeAttr("disabled");
        } else {
            mySubmitButton.attr("disabled","true");
        }
    }
    
    function checkCreditCardAndSubmit() {
        toggleCCSubmitButton(false);
        if ($("#addCreditCardForm").valid()) {
            $.ajax({
                type: "POST",
                url: "<%: Url.Action("GetCreditCardToken") %>",
                data: {
                    NameOnCard: $("#NameOnCard").val(),
                    CreditCardNumber: $("#CreditCardNumber").val(),
                    CreditCardType: $("#CreditCardType").val(),
                    CCExpirationMonth: $("#CCExpirationMonth").val(),
                    CCExpirationYear: $("#CCExpirationYear").val(),
                    CID: $("#CID").val(),
                    BillingAddressId: $("#BillingAddressId").val(),
                    IsUsingNewCreditCard: $("#IsUsingNewCreditCard").val(),
                    SelectedCreditCardId: $("#SelectedCreditCardId").val()
                },
                success: function(data) {
                    if (data.Success) {
                        $("#Token").val(data.Response);
                        var orderBalance = <%= Model.Balance %>;
                        var amount = parseFloat($("#CreditCardAmount").val());
                        if (amount > orderBalance) {
                            $("#CreditCardAmount").val(orderBalance);
                        }
                        $("#addCreditCardForm").submit();
                    } else {
                        toggleCCSubmitButton(true);
                        $("#creditCardMessage").html("Sorry there is a problem with your credit card: " + data.Response + ". Please correct the errors and try again.<br/>");
                    }

                }
            });
        } else {
            toggleCCSubmitButton(true);
        }
    }
</script>
