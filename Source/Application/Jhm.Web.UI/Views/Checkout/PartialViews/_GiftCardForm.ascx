﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.GiftCardViewModel>" %>

<% var currencySymbol = Model.Client.Company.GetCurrencySymbol(); 
   using (Html.BeginForm("Payment_AddGiftCard", "Checkout", FormMethod.Post, new { id = "addGiftCardForm" }))
   { %>
        <div style="float:right; text-align:center;">
            (Back side of your card)<br/>
            <img src="https://media.jhm.org/Images/Web/GiftCards/back.png" width="340">
        </div>
        <div class="form-item" style="width: 400px;">
            <label style="padding-top: 3px;">
                Number:
            </label>
            <%: Html.TextBoxFor(m => m.Number, new{style="width:300px;"}) %>
            <span class="form-required">
                <br />
                <%: Html.ValidationMessageFor(m => m.Number) %>
            </span>
        </div>
        <div class="form-item" style="width: 400px;">
            <label style="padding-top: 3px;">
                PIN:
            </label>

            <%: Html.TextBoxFor(m => m.Pin, new{style="width:80px;"}) %>
            <span class="form-required">
                <br />
                <%: Html.ValidationMessageFor(m => m.Pin) %>
            </span>
        </div>
        <div class="form-item" style="width: 400px;">
            <label style="padding-top: 3px;">
                Payment Amount:
            </label>
            <br/>
            <label><input type="radio" name="amountOption" id="gcUseOrderBalance" onclick="toggleGiftCardAmmount(this,'order');"/><span>Use remaining order balance of <%: Model.FormatWithCompanyCulture("C", Model.Balance) %></span></label>
            <br/>
            <label><input type="radio" name="amountOption" id="gcUseGifCardBalance" onclick="toggleGiftCardAmmount(this,'giftCard');"/><span>Use Gift Card balance</span></label>
            <br/>
            <label><input type="radio" name="amountOption" id="OtherAmount" />Different Amount:<br/>
        
                <%= currencySymbol %> <%: Html.TextBoxFor(m => m.Amount,new{id="GiftCardAmount"}) %> 
                <span class="form-required">
                    <br />
                    <%: Html.ValidationMessageFor(m => m.Amount) %>
                </span>
            </label>
        </div>
        <span id="giftCardMessage" class="field-validation-error"></span>
        <br />
        <input id="mySubmitButton" type="button" value="Add Payment" onclick="checkAndSubmitForm();" />
<% } %>

<script type="text/javascript">
    
    jQuery(function() {
        jQuery.validator.addMethod("giftCardLength", function (value, element) {
            return value.length == 19;
        }, "Please enter a valid gift card number");
        
        $("#addGiftCardForm").validate({
            rules: {
                Number: {
                    required: true,
                    digits: true,
                    giftCardLength: true
                },
                Pin: {
                    required: true,
                    digits: true
                },
                Amount: {
                    required: function (element) {
                        return !($("#gcUseOrderBalance").is(':checked'));
                    },
                    number: true,
                    min: 0.1
                },
                
            },
            messages: {
                Number: {
                    required: "Gift Card number is required. The number can be found in the back side of your card."
                },
                Pin: {
                    required: "Pin number is required. The pin number can be found in the back side of your card."
                },
                Amount: {
                    required: "Please enter an amount or use remaining balance"
                }
            }
        });
    });
    

    function toggleGiftCardAmmount(checkbox, balanceType) {
        var msg = $("#giftCardMessage");
        msg.hide();
        
        var amountBox = document.getElementById("GiftCardAmount");
        if (checkbox.checked && balanceType == 'order') {
            amountBox.value = '<%: Model.Balance.ToString ("#.##") %>';
        }
        else if (checkbox.checked && balanceType == 'giftCard') {
            checkAndPopulateBalance();
            amountBox.value = '<%: Model.Balance.ToString ("#.##") %>'; 
        }
        else {
            amountBox.value = '';
        }
    }
    
    function toggleSubmitButton(shouldEnable) {
        var mySubmitButton = $("#mySubmitButton");
        if (shouldEnable) {
            mySubmitButton.removeAttr("disabled");
        } else {
            mySubmitButton.attr("disabled","true");
        }
    }
    
    function checkAndSubmitForm() {
        toggleSubmitButton(false);
        if ($("#addGiftCardForm").valid()) {
            $.ajax({
                type: "POST",
                url: "<%: Url.Action("GetGiftCardBalance") %>",
                data: { number: $("#Number").val(), pin: $("#Pin").val() },
                success: function(data) {
                    var msg = $("#giftCardMessage");
                    msg.hide();
                    toggleSubmitButton(true);

                    if (data.Success) {
                        var giftCardBalance = parseFloat(data.Balance);
                        var amount = parseFloat($("#GiftCardAmount").val());

                        if (giftCardBalance >= amount) {
                            var orderBalance = <%= Model.Balance %>;
                            if (amount > orderBalance) {
                                $("#GiftCardAmount").val(orderBalance);
                            }
                            $("#addGiftCardForm").submit();
                        } else {
                            msg.html("The gift card balance is less than the entered amount. Current gift card balance is <%= currencySymbol %>" + giftCardBalance.toFixed(2) + ". Please adjust the amount or select one of the options above<br/>");
                            msg.show();
                        }
                    } else {
                        msg.html(data.Response + "<br/>");
                        msg.show();
                    }
                }
            });
        } else {
            toggleSubmitButton(true);
        }
    }
    
    function checkAndPopulateBalance() {
        toggleSubmitButton(false);
        var numberIsValid = $('#Number').valid();
        var pinIsValid = $('#Pin').valid();
        if ( numberIsValid && pinIsValid ) {
            $.ajax({
                type: "POST",
                url: "<%: Url.Action("GetGiftCardBalance") %>",
                data: { number: $("#Number").val(), pin: $("#Pin").val() },
                success: function(data) {
                    toggleSubmitButton(true);
                    var msg = $("#giftCardMessage");
                    msg.hide();
                    if (data.Success) {
                        var amountBox = document.getElementById("GiftCardAmount");
                        var giftCardBalance = parseFloat(data.Balance);
                        if (giftCardBalance == 0) {
                            msg.html("Sorry this gift card has been fully redeemed. The gift card balance is zero<br/>");
                            msg.show();
                        } else {
                            var orderBalance = <%= Model.Balance %>;
                            var redeemValue = 0;
                            if (giftCardBalance > orderBalance) {
                                msg.html("Note: The gift card balance is greater than the remaining order balance. We will redeem only <%= currencySymbol %>" + orderBalance.toFixed(2) + " from your gift card.<br/>");
                                msg.show();
                                redeemValue = orderBalance;

                            } else if (giftCardBalance < orderBalance) {
                                msg.html("Note: The gift card balance is less than the remaining order balance. Current order balance is <%= currencySymbol %>" + orderBalance.toFixed(2) + ". We wil redeem only <%= currencySymbol %>" + giftCardBalance.toFixed(2) + " from your gift card.<br/>");
                                msg.show();
                                redeemValue = (giftCardBalance);
                            } else {
                                redeemValue = (giftCardBalance);
                            }
                            amountBox.value = redeemValue.toFixed(2);
                        }
                    } else {
                        msg.html(data.Response + "<br/>");
                        msg.show();
                    }
                }
            });
        } else {
            toggleSubmitButton(true);
            $("#OtherAmount").click();
        }
    }
</script>