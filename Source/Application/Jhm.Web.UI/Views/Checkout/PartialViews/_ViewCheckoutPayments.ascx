﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.PaymentListViewModel>" %>

<% if (Model.Payments.Any())
   { %>
    <div id="paymentsList">
        <table class="store checkout">
            <thead>
                <tr>
                    <td>Type</td>
                    <td>Number</td>
                    <td style="width:140px;" class="total">Charge Amount</td>
                </tr>
            </thead>
            <tbody>
                <% foreach (var payment in Model.Payments)
                   { %>
                       <tr>
                           <td><%: payment.PaymentType.Name %></td>
                           <td>*****************<%: (payment.PaymentNumber).Substring(Math.Max(0, payment.PaymentNumber.Length - 4)) %></td>
                           <td class="cost">
                               <%:Model.FormatWithCompanyCulture("C", payment.Amount)%>
                           </td>
                       </tr>
                <% } %>
            </tbody>
            <tfoot>
                <% if ( Model.IsDifferenceMediaPage ){ %>
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr>
                <% } %>
                
                <tr>
                    <td colspan="2" class="subtotal cartLabel">
                        Order Total:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="subtotal cartLabel">
                        Payment Total:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.PaymentTotal * -1)%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="subtotal cartLabel">
                        Remaining Balance:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal - Model.PaymentTotal)%>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
<% }
   else
   { %>
       <div class="description" style="font-size: larger;">Your order has no payments methods</div>
<% } %>

