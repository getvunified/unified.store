﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.PaymentListViewModel>" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="getv.donorstudio.core.Utilities" %>

<% if (Model.Balance > 0)
   { %>
    <div>
        <label style="width:100px;">Payment Method:</label>&nbsp;
        <%: Html.DropDownListFor(m => m.SelectedPaymentType, Model.PaymentTypes, String.Empty) %> &nbsp;
        <a class="easybutton" 
            href="javascript:addPaymentType();" 
            style="min-width: 40px;padding: 2px 5px 0; min-height: 20px;">
            <span class="">Add Payment Method</span>
        </a>
        <span id="addPaymentValidator" class="field-validation-error" style="display:none;"><br />* Please select a payment method from the list</span>
    </div>
<% } %>
<br/>
<% if (Model.Payments.Any()){ %>
    <div id="paymentsList">
        <table class="store checkout">
            <thead>
                <tr>
                    <td style="width:80px;"/>
                    <td>Type</td>
                    <td>Number</td>
                    <td style="width:140px;" class="total">Charge Amount</td>
                </tr>
            </thead>
            <tbody>
                <% foreach (var payment in Model.Payments)
                   { %>
                       <tr>
                           <td>
                               <%: Html.ActionLink("Remove","Payment_Remove",new {id=payment.Id},new{onclick="return confirm('Are you sure you want to remove this payment method?');", @class="removePaymentLink"}) %>
                           </td>
                           <td><%: payment.PaymentType.Name %></td>
                           <td>*****************<%: (payment.PaymentNumber).Substring(Math.Max(0, payment.PaymentNumber.Length - 4)) %></td>
                           <td class="cost">
                               <%:Model.FormatWithCompanyCulture("C", payment.Amount)%>
                           </td>
                       </tr>
                <% } %>
            </tbody>
            <tfoot>
                <% if ( Model.IsDifferenceMediaPage ){ %>
                <tr>
                    <td colspan="4">
                        <hr/>
                    </td>
                </tr>
                <% } %>
                <tr>
                    <td/>
                    <td colspan="2" class="subtotal cartLabel">
                        Order Total:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                    </td>
                </tr>
                <tr>
                    <td/>
                    <td colspan="2" class="subtotal cartLabel">
                        Payment Total:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.PaymentTotal * -1)%>
                    </td>
                </tr>
                <tr>
                    <td/>
                    <td colspan="2" class="subtotal cartLabel">
                        Remaining Balance:
                    </td>
                    <td class="cost">
                        <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal - Model.PaymentTotal)%> <img src="/Content/images/ok.gif" style="display:<%: (Model.OrderTotal - Model.PaymentTotal == 0 ) ? String.Empty : "none" %>"/>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
<% } %>

<div id="gcPaymentContainer" class="dialog" title="Add Gift Card Payment">
    <% var giftCardModel = new GiftCardViewModel {Balance = Model.OrderTotal - Model.PaymentTotal};
       giftCardModel.RegisterClient(Model.Client);  %>
    <%: Html.Partial("PartialViews/_GiftCardForm", giftCardModel) %>
</div>
<div id="ccPaymentContainer" class="dialog" title="Add Credit Card Payment">
    <% var creditCardModel = new CreditCardViewModel {CreditCardsList = Model.AvailableCreditCards, AddressList = Model.AddressList, Balance = Model.OrderTotal - Model.PaymentTotal, IsUsingNewCreditCard = !Model.AvailableCreditCards.Any()};
       creditCardModel.RegisterClient(Model.Client);  %>
    <%: Html.Partial("PartialViews/_CreditCardForm", creditCardModel) %>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#ccPaymentContainer").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 500
        });
        
        
        $("#gcPaymentContainer").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 760
        });
    });

    function reopenPaymentDialog() {
    	$(".dialog").dialog("close");
    	addPaymentType();
    }
    
    function addPaymentType() {
        removePaymentTypeValidator();
        
        var paymentType = $("#SelectedPaymentType").val();
        if (paymentType == '<%= DsConstants.PaymentType.CreditCard %>') {
            $("#ccPaymentContainer").dialog("open");
        }
        else if (paymentType == '<%= DsConstants.PaymentType.GiftCard %>') {
            $("#gcPaymentContainer").dialog("open");
        }
        else {
            showPaymentTypeValidator();
            $(".dialog").dialog("close");
        }
    }
    
    function highlightRemoveButtons() {
        $(".removePaymentLink").effect("highlight", {}, 3000);
    }
    
    function removePaymentTypeValidator() {
        $("#addPaymentValidator").hide();
        $("#SelectedPaymentType").removeClass("input-validation-error");
        $("#SelectedPaymentType").unbind("change");
    }
    
    function showPaymentTypeValidator() {
        $("#addPaymentValidator").show();
        $("#SelectedPaymentType").addClass("input-validation-error");
        $("#SelectedPaymentType").bind("change", function() {
            removePaymentTypeValidator();
        });
    }
    
    function checkSelectedPaymentType() {
        var paymentType = $("#SelectedPaymentType").val();
        if (paymentType == '<%= DsConstants.PaymentType.CreditCard %>') {
            return true;
        }
        else if (paymentType == '<%= DsConstants.PaymentType.GiftCard %>') {
            return true;
        }
        else {
            return false;
        }
    }
</script>