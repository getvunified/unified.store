<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.em3.Core" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Checkout
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {%>
    <script type="text/javascript">
        location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
    </script>
    <%
       }
       else
       { 
    %>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] {"PhoneCode" })%>
    <script type="text/javascript">
        var addresses = <%= ViewData["jsonAddressesList"] + ";" %>;
        var nopobox = <%= ViewData["jsonNoPOBoxMethods"] + ";" %>;
        function PerformPoBoxValidation(ddShippingMethod) {
            if (ddShippingMethod != null && !ShippingMethodAllowsPOBoxes(ddShippingMethod.value)) {
                var pattern = new RegExp('[PO.]*\\s?B(ox)?.*\\d+', 'i');
                var address = $('#ShippingAddress_Address1').val() + ' ' + $('ShippingAddress_Address2').val();
                if (address.match(pattern)) {
                    $('.no-pobox-msg').show();
                    return false;
                }
            }
            $('.no-pobox-msg').hide();
            return true;
        }

        function ShippingMethodAllowsPOBoxes(id) {
            for (var i = 0; i < nopobox.length; i++) {
                if (nopobox[i].Id == id) {
                    return false;
                }
            }
            return true;
        }
    </script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/checkout.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#dialogModal').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500
            });
            $('#opener,#billingOpener').click(function () { $('#dialogModal').dialog('open'); return false; });
            <%= ViewData["ShowNewAddressPopupScript"] %>
            populateBillingAddressFields(document.getElementById("BillingAddressId"));
            populateAddressFields(document.getElementById("ShippingAddressId"),'<%= ClientCompany.InternationalShippingMethod.Value %>','<%= ClientCompany.DefaultCountry.Iso %>',countryCodes);
        });
    </script>
    <%}%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } %>
       <div id="dialogModal" title="Add New Billing/Shipping Address">
            <div class="section-checkout">
                <div class="block">
                    <p>
                        Complete the form below to add a new shipping address.</p>
                    <%
                        using (Html.BeginForm())
                        {%>
                    <%= Html.ValidationSummary("Adding new address was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                    <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                    <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                    <table>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Address Line 1:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address1",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Address1") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Address line 2:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address2",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>City:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("City", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32", @value = "" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("City") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Country:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList
                                (
                                    "Country",
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'StateCode')"
                                        }
                                )%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Country") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>State/Province/Division:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList("StateCode", State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:215px" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("StateCode") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Postal code:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("PostalCode", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("PostalCode")%></span>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <div style="text-align: right;">
                        <%:Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address",true,"Adding new address...", MagicStringEliminator.CheckoutActions.AddAddress), "PopupActionBottonClicked")%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="easybutton" href="javascript:void(0);"
                            onclick="$('#dialogModal').dialog('close');"><span class="">Cancel</span> </a>
                    </div>
                    <%        
                        }
                    %>
                </div>
            </div>
        </div>
    <div id="section-checkout">
        <div id="content-area">
            <div class="block">
                <h2 class="title">
                    Checkout</h2>
                <div class="content">
                    <table class="store checkout">
                        <%  if (Model.ContainsDonations())
                            {%>
                        <thead>
                            <tr>
                                <td class="product">
                                    Donations
                                </td>
                                <td class="price">
                                    &nbsp;
                                </td>
                                <td class="qty">
                                    &nbsp;
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is DonationCartItem))
                                {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%
                            }%>
                        <%  if (Model.ContainsStockItem() || Model.ContainsGifCards())
                            { %>
                        <tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>
                        <thead>
                            <tr>
                                <td class="product">
                                    Products
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem || x.Item is GiftCardCartItem))
                                {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        
                        <%  if (Model.ContainsSubscriptionItems())
                            { %>
                        <%--<tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>--%>
                        <thead>
                            <tr>
                                <td class="product">
                                    Subscriptions
                                </td>
                                <td class="price">
                                    &nbsp;
                                </td>
                                <td class="qty">
                                    &nbsp;
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is SubscriptionCartItem))
                                {%>
                                    <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                              <%}%>
                        </tbody>
                        <%} %>

                        <%  if (Model.PromotionalItems.Any())
                            { %>
                        <tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>
                        <thead>
                            <tr>
                                <td class="product">
                                    Promotional Items
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.PromotionalItems)
                                {%>
                            <%=Html.RenderPartialDisplayByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        <tfoot>
                            <% if (Model.DiscountApplied != null)
                               { %>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    Subtotal before discount:
                                </td>
                                <td class="cost vertical-padding-thin">
                                    <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    <% if (Model.DiscountApplied.ActiveRule.AppliesOnlyToItemsMatchingCriteria)
                                       { %>
                                            <img height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
                                    <% } %>
                                    <%:Model.DiscountApplied.ShortDescription%>:
                                </td>
                                <td class="cost vertical-padding-thin" style="width: 120px;">
                                    <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="subtotal">
                                    Subtotal:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <% using (Html.BeginForm())
               { %>
            <%:Html.ValidationSummary(false, "Your request was not successful. Please correct the errors and try again.", new {@class = "messages error"})%>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <div class="block">
                <h2 class="title split">
                    <span class="col-1">Shipping Information</span> <span class="col-2">Shipping Method</span></h2>
                <div class="content clearfix">
                    <div class="col-1">
                        <div class="col-inner">
                            <div class="description">
                                Select your delivery address and information from the list below.</div>
                            <label style="padding-top: 3px;">
                                <span class="form-required">*</span>Select Address:
                            </label>
                            <div class="form-item">
                                <%: Html.DropDownListFor
                                    (
                                    m => m.ShippingAddressId,
                                    new SelectList(Model.AddressList?? new List<Address>(), "DSAddressId", "Address1"),
                                    "(Select an address)",
                                    new
                                    {
                                        @class = "form-select required",
                                        @style = "width:160px",
                                        @tabindex = "2",
                                        @onChange = "populateAddressFields(this,'" + ClientCompany.InternationalShippingMethod.Value + "','" + ClientCompany.DefaultCountry.Iso + "',countryCodes);PerformPoBoxValidation(document.getElementById('ShippingMethodValue'));"
                                    }
                                    )%>
                                <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add", "Add_Address", String.Empty, new { @style = "min-width:40px; padding: 2px 5px 0;min-height: 20px;", @id = "opener" }))%>
                                <span class="form-required">
                                    <%: Html.ValidationMessageFor(m => m.ShippingAddressId)%>
                                </span>
                                <span class="form-required no-pobox-msg" style="display:none;"><br />* Sorry, No PO Boxes allowed using selected shipping method</span>
                            </div>
                            <label>
                                <span class="form-required">*</span>First name:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingFirstName, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                                <span class="form-required">
                                    <%: Html.ValidationMessageFor(m => m.ShippingFirstName)%>
                                </span>
                            </div>
                            <label>
                                <span class="form-required">*</span>Last name:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingLastName, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                                <span class="form-required">
                                    <%: Html.ValidationMessageFor(m => m.ShippingLastName)%>
                                </span>
                            </div>
                            <label>
                                Company:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingCompany, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                            </div>
                            <label>
                                Address line 1:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingAddress.Address1, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled="disabled" })%>
                            </div>
                            <label>
                                Address line 2:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingAddress.Address2, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled="disabled" })%>
                            </div>
                            <label>
                                Country:
                            </label>
                            <div class="form-item">
                                <%: Html.DropDownListFor
                                (
                                    m => m.ShippingAddress.Country,
                                    Country.GetAllSortedByDisplayName<Country>().ToMVCSelectList("Iso", "Name", "840"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'ShippingAddress_StateCode')",
                                            @disabled = "disabled"
                                        }
                                )%>
                            </div>
                            <label>
                                State/Province/Division:
                            </label>
                            <div class="form-item">
                                <%: Html.DropDownListFor(model => model.ShippingAddress.StateCode, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:210px", @disabled = "disabled" })%>
                            </div>
                            <label>
                                City:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingAddress.City, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled = "disabled" })%>
                            </div>
                            <label>
                                Postal code:
                            </label>
                            <div class="form-item">
                                <%: Html.TextBoxFor(m => m.ShippingAddress.PostalCode, new { @class = "form-text required", @size = "32", @maxlength = "32", @disabled = "disabled" })%>
                            </div>
                        </div>
                    </div>
                    <div class="col-2" style="height: 310px;">
                        <div class="col-inner">
                            <% if (Model.ShouldDisplayShippingMethods)
                               {%>
                            <div class="description">
                                Select a Shipping method from the list below<span class="form-required">*</span>:
                            </div>
                            <br />
                            <%:Html.DropDownListFor
                              (
                                  m => m.ShippingMethodValue,
                                  Model.ShippingMethods.ToMVCSelectList("Value", "DisplayName"),
                                                                      "Select Shipping Method",
                                                                     new { @class = "form-select required", @style = "width:415px;", @onchange = "PerformPoBoxValidation(this);" }
                              )%>
                            <span class="form-required">
                                <%:Html.ValidationMessageFor(m => m.ShippingMethodValue)%>
                                <span class="no-pobox-msg" style="display:none;"><br />* Sorry, No PO Boxes allowed using selected shipping method</span>
                            </span>
                            <%
                                   if (ClientCompany == Company.JHM_Canada)
                                   {%>
                                    <br/><br/>
                                        <strong>Note:</strong> Shipping will be based on product availability; please allow an additional 2-3 weeks for processing.
                                  <% }
                               }
                               else
                               {%>
                                    <div class="description" style="font-size: larger;">
                                        Your order has no shipping cost.
                                    </div>  
                            <%
                               }
                             %>
                                
                                
                        </div>
                    </div>
                    <br />
                                <br />
                                <div class="checkout-controls">
                                    <%: Html.FormKeyValueMatchAttribute_EasyButtonDiv(
                                        new List<EasyButton>
                                            {
                                                new EasyButton("Cancel", "Payment_Cancel"), 
                                                new EasyButton("Payment",false,"Checking out your order...", MagicStringEliminator.CheckoutActions.Shipping, null, new { @onClick = "return PerformPoBoxValidation(document.getElementById('ShippingMethodValue'));" })
                                            })%>
                                </div>
                </div>
            </div>
                <%  } %>
            
        </div>
    </div>
    <script type="text/javascript">
        function PerformPoBoxValidation(ddShippingMethod) {
            if ( ddShippingMethod != null && !ShippingMethodAllowsPOBoxes(ddShippingMethod.value) ) {
                var pattern = new RegExp('[PO.]*\\s?B(ox)?.*\\d+', 'i');
                var address = $('#ShippingAddress_Address1').val() + ' ' + $('ShippingAddress_Address2').val();
                if (address.match(pattern)) {
                    $('.no-pobox-msg').show();
                    return false;
                }
            }
            $('.no-pobox-msg').hide();
            return true;
        }
        
        function ShippingMethodAllowsPOBoxes(id) {
            for (var i = 0; i < nopobox.length; i++) {
                if ( nopobox[i].Id == id) {
                    return false;
                }
            }
            return true;
        }
    </script>
</asp:Content>
