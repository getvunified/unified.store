<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.em3.Core" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Checkout
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {%>
    <script type="text/javascript">
        location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
    </script>
    <%
       }
       else
       { 
    %>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <%--<%= Html.GenerateJSArrayFromCountriesList("countryCodes", true, new[] {"PhoneCode" })%>--%>
    <script type="text/javascript">
        var addresses = <%= ViewData["jsonAddressesList"] + ";" %>;
    </script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/checkout.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#dialogModal').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500
            });
            $('#opener,#billingOpener').click(function () { $('#dialogModal').dialog('open'); return false; });
            <%= ViewData["ShowNewAddressPopupScript"] %>
            populateBillingAddressFields(document.getElementById("BillingAddressId"));
        });
    </script>
    <%}%>
    <style type="text/css">
        label.error {
            color: red;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } %>
       <div id="dialogModal" title="Add New Billing Address">
            <div class="section-checkout">
                <div class="block">
                    <p>
                        Complete the form below to add a new billing address.</p>
                    <%
                        using (Html.BeginForm("Payment_AddAddress", "Checkout", new { ReturnToUrl = "Payment" }, FormMethod.Post))
                        {%>
                    <%= Html.ValidationSummary("Adding new address was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                    <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                    <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                    <table>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Address Line 1:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address1",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Address1") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Address line 2:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address2",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>City:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("City", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32", @value = "" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("City") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Country:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList
                                (
                                    "Country",
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'StateCode')"
                                        }
                                )%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Country") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>State/Province/Division:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList("StateCode", State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:215px" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("StateCode") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Postal code:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("PostalCode", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("PostalCode")%></span>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <div style="text-align: right;">
                        <%:Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address",true,"Adding new billing address...", MagicStringEliminator.CheckoutActions.AddAddress), "PopupActionBottonClicked")%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="easybutton" href="javascript:void(0);"
                            onclick="$('#dialogModal').dialog('close');reopenPaymentDialog();"><span class="">Cancel</span> </a>
                    </div>
                    <%        
                        }
                    %>
                </div>
            </div>
        </div>
    <div id="section-review">
        <div id="content-area">
            <div class="block">
                <h2 class="title">
                    Checkout</h2>
                <div class="content">
                    <table class="store checkout">
                        <%  if (Model.ContainsDonations())
                            {%>
                        <thead>
                            <tr>
                                <td class="product">
                                    Donations
                                </td>
                                <td class="price">
                                    &nbsp;
                                </td>
                                <td class="qty">
                                    &nbsp;
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is DonationCartItem))
                                {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%
                            }%>
                        <%  if (Model.ContainsStockItem() || Model.ContainsGifCards())
                            { %>
                        <tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>
                        <thead>
                            <tr>
                                <td class="product">
                                    Products
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem || x.Item is GiftCardCartItem))
                                {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        
                        <%  if (Model.ContainsSubscriptionItems())
                            { %>
                        <%--<tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>--%>
                        <thead>
                            <tr>
                                <td class="product">
                                    Subscriptions
                                </td>
                                <td class="price">
                                    &nbsp;
                                </td>
                                <td class="qty">
                                    &nbsp;
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is SubscriptionCartItem))
                                {%>
                                    <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                              <%}%>
                        </tbody>
                        <%} %>

                        <%  if (Model.PromotionalItems.Any())
                            { %>
                        <tr style="border: none;">
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                            <td class="no-border">
                                &nbsp;
                            </td>
                        </tr>
                        <thead>
                            <tr>
                                <td class="product">
                                    Promotional Items
                                </td>
                                <td class="price">
                                    Qty.
                                </td>
                                <td class="qty">
                                    Price
                                </td>
                                <td class="total">
                                    Total
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                foreach (var cartItem in Model.PromotionalItems)
                                {%>
                            <%=Html.RenderPartialDisplayByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                            <%}%>
                        </tbody>
                        <%} %>
                        <tfoot>
                            <% if (Model.DiscountApplied != null)
                               { %>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    Subtotal before discount:
                                </td>
                                <td class="cost vertical-padding-thin">
                                    <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                                </td>
                            </tr>
                            <tr>
                                <td class="subtotal vertical-padding-thin" colspan="3">
                                    <% if (Model.DiscountApplied.ActiveRule.AppliesOnlyToItemsMatchingCriteria)
                                       { %>
                                            <img height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
                                    <% } %>
                                    <%:Model.DiscountApplied.ShortDescription%>:
                                </td>
                                <td class="cost vertical-padding-thin" style="width: 120px;">
                                    <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="subtotal">
                                    Subtotal:
                                </td>
                                <td class="cost">
                                    <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="shipping">
                                    Shipping:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.ShippingCost)%>
                                </td>
                            </tr>
                            <tr class="row-tax">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="tax">
                                    Tax:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.TaxCost)%>
                                </td>
                            </tr>
                            <tr class="row-total">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td class="order-total">
                                    TOTAL:
                                </td>
                                <td class="cost">
                                    <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <% using (Html.BeginForm())
               { %>
            <%:Html.ValidationSummary(false, "Your request was not successful. Please correct the errors and try again.", new {@class = "messages error"})%>
            <%: Html.FormKeyValueMatchAttributeHiddenField()%>
            <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
            <div class="block">
                <h2 class="title split">
                    <span class="col-1">Customer Information</span> <span class="col-2"></span>
                    <span class="more-link"><%: Html.ActionLink("Change", "OrderReview_EditProfile")%></span></h2>
                <div class="content clearfix">
                    <div class="col-1">
                        <label>
                            First name:</label>
                        <%: Html.DisplayTextFor(m => m.FirstName)%>
                        <br />
                        <label>
                            Last name:</label>
                        <%: Html.DisplayTextFor(m => m.LastName)%>
                        <br />
                        <label>
                            Email Address:</label>
                        <%: Html.DisplayTextFor(m => m.Email)%>
                        <br />
                        <label>
                            Phone:</label>
                        <%: Model.Profile.PrimaryPhone == null ? "none" : String.Format("{0} {1}-{2}",Model.Profile.PrimaryPhone.CountryCode, Model.Profile.PrimaryPhone.AreaCode, Model.Profile.PrimaryPhone.Number)%>
                    </div>                    
                </div>
            </div>
            <%  } %>
            <div class="block">
                <h2 class="title">
                    Payment Information</h2>
                <div class="content clearfix">
                    <div class="col-1" style="width: 564px;">
                        <div class="col-inner">
                            <% if (Model.ShouldDisplayPaymentMethods)
                               {
                                   //IMPORTANT: This partial view cannot be inside a form since it contains its own forms for each payment type
                                   //Payment method forms won't work properly if forms are nested.
                                   //Updating addresses list for the payment list model
                                   Html.RenderPartial("PartialViews/_EditCheckoutPayments", Model.PaymentList);
                               }
                               else
                               { %>
                                    <div class="description" style="font-size: larger;">
                                        Your order does not require a payment.
                                    </div>
                            <% } %>
                        </div>
                    </div>
                    <br style="clear: both;" />
                    <br />
                    <br />
                    <br />
                    <div class="checkout-controls">
                        
                        <% if (Model.PaymentList.Balance != 0)
                           { %>
                               <div class="easybutton">
                                    <a class=" first" href="javascript:EasyButtonSubmitForm('ActionBottonClicked','Payment_Cancel');"><span class="">Cancel</span></a>
                                    <a onclick="alert('Your remaining balance is <%:Model.PaymentList.Balance.ToString("C") %>. Please add or remove a payment method to balance your payment.'); addPaymentType();highlightRemoveButtons(); return false;" class=" last disabled" href="javascript:void(0);"><span>Review Order</span></a>
                               </div>
                        <% }
                           else
                           { %>
                                <%: Html.FormKeyValueMatchAttribute_EasyButtonDiv(
                                    new List<EasyButton>
                                        {
                                            new EasyButton("Cancel", "Payment_Cancel"), 
                                            new EasyButton("Review Order",true,"Checking out your order...", MagicStringEliminator.CheckoutActions.Review)
                                        })%>
                            <% } %>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
