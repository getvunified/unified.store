﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    OrderReceipt
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader(Model.IsDifferenceMediaPage? "Store" : null) %>
    <div id="section-receipt">
        <div id="section-checkout">
            <div id="content-area">
                <div class="block">
                    <h2 class="title">
                        Thank you for your purchase!</h2>
                    <div class="content clearfix" style="border:1px solid black">
                        <p class="order-message">
                            Your order has been submitted for processing. You should receive a confirmation
                            of payment by e-mail, as well as information on when to expect to receive your order.
                            Please print this page for your records.
                        </p>
                        <div>                       
                        <% if (Model.ContainsDownloadableItem())
                           {%>
							<p class="order-message">
								You will receive an email with a link to download your digital purchases.
							</p>
							<%}%>
							<table>
								<tr>
									<td style="width:300px; padding-left:20px;">
										
											<label class="cartLabel">
												Approval Code #:
											</label>
											<%: Model.ApprovalCode %>
											<br/>
											<label class="cartLabel">
												Order date:
											</label>
											<%: Model.OrderDate %>
										
									</td>
									<td style="padding-right:20px;">
										<% if (Model.IsFirstTimeSaltDonation) { %>
											<h6>New Salt Partner</h6>
											Thank you for your interest in partnering with John Hagee Ministries.  To set up a monthly gift  and the details of your account as a Salt Partner with JHM, we recommend you contact the JHM Partners Department at 1-877-546-7258  or email partners@jhm.org.  One of our Partner Dept. team members will gladly assist you. We appreciate your part in taking all the gospel to all the world and to every generation.
											<br /><br />
											Blessings to You and those you love.
										<%}%>
									</td>
								</tr>
							</table>
								<p class="order-message"></p>
                        </div>
                        <div class="block">
                            <div class="content clearfix">
                                <table class="receiptTable">
                                    <tr>
                                        <td class="col-inner">
                                            <div id="billing-info" style="padding-left: 25px;">
                                                <span class="cartLabel">Billing Info:</span><br />
                                                <%: StringExtensions.FormatWith("{0} {1}",Model.FirstName,Model.LastName) %><br />
                                                <% if (Model.BillingAddress != null)
                                                   { %>
                                                        <%: Model.BillingAddress.Address1 %><br />
                                                        <%= String.IsNullOrEmpty(Model.BillingAddress.Address2) ? String.Empty : Model.BillingAddress.Address2 + "<br />" %>
                                                        <%: StringExtensions.FormatWith("{0}, {1} {2}",Model.BillingAddress.City, Model.BillingAddress.StateCode, Model.BillingAddress.PostalCode) %><br />
                                                        <%: Model.BillingAddressCountry %><br />
                                                <% } %>
                                                <%= String.IsNullOrEmpty(Model.BillingPhone) ? String.Empty : Model.BillingPhone + "<br />"%>
                                                <%: Model.Email %>
                                            </div>
                                        </td>
                                        <td class="col-inner">
                                            <div id="shipping-info" class="col-2" style="padding-left: 25px;">
                                                <span class="cartLabel">Shipping Info:</span><br />
                                                <% if (Model.ShippingAddress != null)
                                                   { %>
                                                    <%: StringExtensions.FormatWith("{0} {1}",Model.ShippingFirstName, Model.ShippingLastName) %><br />
                                                    <%: Model.ShippingAddress.Address1 %><br />
                                                    <%= String.IsNullOrEmpty(Model.ShippingAddress.Address2)
                                                            ? String.Empty
                                                            : Model.ShippingAddress.Address2 + "<br />" %>
                                                    <%: StringExtensions.FormatWith("{0}, {1} {2}",Model.ShippingAddress.City,
                                                                                    Model.ShippingAddress.StateCode,
                                                                                    Model.ShippingAddress.PostalCode) %><br />
                                                    <%: Model.ShippingAddressCountry %><br />
                                                <% } 
                                                   else { %>
                                                       Not Required
                                                <% } %>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="block">
                            <h2 class="title">Payment Information</h2>
                            <div class="content">
                    
                                        <% if (Model.ShouldDisplayPaymentMethods)
                                           {
                                               Html.RenderPartial("PartialViews/_ViewCheckoutPayments", Model.PaymentList); %>
                                               <p>*This transaction will appear on your bill as: <%: ClientCompany.CommerceDescriptor %></p>
                                         <%  }
                                           else
                                           { %>
                                                <div class="description" style="font-size: larger;">
                                                    Your order does not require a payment.
                                                </div>
                                        <% } %>
                        
                            </div>
                        </div>
                        <div class="content" style="border-top: 1px; border-color: #000000;
                                border-style: solid">
                            <table class="store checkout" style="width:778px">
                                <%  if (Model.ContainsDonations())
                                    {%>
                                        <thead style="background-color: #dfe0e3">
                                            <tr >
                                                <td class="product">
                                                    Payment For
                                                </td>
                                                <td class="price">
                                                    &nbsp;
                                                </td>
                                                <td class="qty">
                                                    &nbsp;
                                                </td>
                                                <td class="total">
                                                    Total
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    <%
                                    foreach (var cartItem in Model.CartItems.Where(x => x.Item is DonationCartItem))
                                    {%>
                                        <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                    <%}%>
                                </tbody>
                                <%
                                    }%>
                                <%  if (Model.ContainsStockItem() || Model.ContainsGifCards())
                                    { 
                                        if ( !Model.IsDifferenceMediaPage )
                                        {%>
                                        <tr style="border: medium none;">
                                            <td colspan="4">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <%} %> 
                                        <thead>
                                            <tr>
                                                <td class="product">
                                                    Products
                                                </td>
                                                <td class="price">
                                                    Qty.
                                                </td>
                                                <td class="qty">
                                                    Price
                                                </td>
                                                <td class="total">
                                                    Total
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                            foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem || x.Item is GiftCardCartItem))
                                            {%>
                                                <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                            <%}
                                                if ( Model.IsDifferenceMediaPage )
                                                {%>
                                                <tr>
                                                    <td colspan="4"><hr /></td>
                                                </tr>   
                                                <%} %>  
                                        </tbody>
                                <%} %>  
                                
                                <%  if (Model.ContainsSubscriptionItems())
                                    {%>
                                        <thead style="background-color: #dfe0e3">
                                            <tr >
                                                <td class="product">
                                                    Subscriptions
                                                </td>
                                                <td class="price">
                                                    &nbsp;
                                                </td>
                                                <td class="qty">
                                                    &nbsp;
                                                </td>
                                                <td class="total">
                                                    Total
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    <%
                                    foreach (var cartItem in Model.CartItems.Where(x => x.Item is SubscriptionCartItem))
                                    {%>
                                        <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                    <%}%>
                                </tbody>
                                <%
                                    }%>
                                
                                <%  if (Model.PromotionalItems.Any())
                                    { %>
                                        <tr style="border: medium none;">
                                            <td colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <thead>
                                            <tr>
                                                <td class="product">
                                                    Promotional Items
                                                </td>
                                                <td class="price">
                                                    Qty.
                                                </td>
                                                <td class="qty">
                                                    Price
                                                </td>
                                                <td class="total">
                                                    Total
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                            foreach (var cartItem in Model.PromotionalItems)
                                            {%>
                                                <%=Html.RenderPartialDisplayByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/OrderReview/")%>
                                            <%}%>
                                        </tbody>
                                <%} %>

                                <tfoot>
                                    <% if (Model.DiscountApplied != null)
                                       { %>
                                        <tr>
                                            <td class="subtotal vertical-padding-thin cartLabel" colspan="3">
                                                Subtotal before discount:
                                            </td>
                                            <td class="cost vertical-padding-thin">
                                                <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="subtotal vertical-padding-thin cartLabel" colspan="3">
                                                <%:Model.DiscountApplied.ShortDescription%>:
                                            </td>
                                            <td class="cost vertical-padding-thin" style="width: 120px;">
                                                <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                                            </td>
                                        </tr>
                                    <% } %>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="subtotal cartLabel">
                                            Subtotal:
                                        </td>
                                        <td class="cost">
                                            <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="shipping cartLabel">
                                            Shipping:
                                        </td>
                                        <td class="cost">
                                            <%:Model.FormatWithCompanyCulture("C", Model.ShippingCost)%>
                                        </td>
                                    </tr>
                                    <tr class="row-tax">
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="tax cartLabel">
                                            Tax:
                                        </td>
                                        <td class="cost">
                                            <%:Model.FormatWithCompanyCulture("C", Model.TaxCost)%>
                                        </td>
                                    </tr>
                                    <tr class="row-total">
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td class="order-total cartLabel">
                                            TOTAL:
                                        </td>
                                        <td class="cost">
                                            <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="content">
                            <div class="checkout-controls">
                                <div class="easybutton">
                                    <a href="javascript:void(0);" class="first" onclick="openPrinterFriendlyWindow(this);">
                                        <span class="icon" style="background: url(/content/images/printIcon.gif) no-repeat scroll left top transparent;">
                                            Print My Order </span></a><a href="<%: Url.Action("Index","Home") %>" class="last">Return
                                                To Home</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
        function openPrinterFriendlyWindow(printLink) {
            printLink.parentNode.style.visibility = "hidden";
            jQuery(".no-print").hide();
            var w = window.open('', '', 'status=0,scrollbars=1,width=800,height=500');

            w.document.open("text/html");
            w.document.write("<div id='content'><div id='section-receipt'>" + document.getElementById('section-receipt').innerHTML + "</div></div>");

            addStyleSheet(w, "/content/css/print-html-reset.css");
            addStyleSheet(w, "/content/css/defaults.css");
            addStyleSheet(w, "/content/css/print-layout-fixed.css");
            addStyleSheet(w, "/content/css/print-pages.css");
            addStyleSheet(w, "/content/css/print-ecommerce.css");
            addStyleSheet(w, "/content/css/print-blocks.css");
            addStyleSheet(w, "/content/css/print.css");

            w.document.close();
            w.print();
            jQuery(".no-print").show();
            printLink.parentNode.style.visibility = "visible";
        }

        function addStyleSheet(w, cssFilePath) {
            var fileref = w.document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", cssFilePath);
            w.document.getElementsByTagName("head")[0].appendChild(fileref);
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
