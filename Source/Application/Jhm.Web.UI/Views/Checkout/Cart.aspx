﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="Jhm.Web.UI.Views.CompanyCapableViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutShoppingCartViewModel>" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Shopping Cart
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            <% if (!Request.IsAuthenticated)
               { %>
                    $('#opener').click(function () {
                        showLogonShadowBox('<%: Url.Action("Shipping") %>');
                        return false;
                    });
               <% } %>
        });
        function validateInputs() {
            if ($('#UserName').val() == '') {
                $('#rfvUsername').css("display", "");
                return false;
            }
            $('#rfvUsername').css("display", "none");
            if ($('#Password').val() == '') {
                $('#rfvPassword').css("display", "");
                return false;
            }
            $('#rfvPassword').css("display", "none");
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("PartialViews/_ShadowBoxLogon",new LogOnModel()) %>

    <div class="content-area">
        <div class="block">
            <h2 class="title collapse-control">
                <span class="arrow">&#x025BC;</span>My Donations <span class="more-link"><a href="<%: Model.DonationsReturnToUrl %>">
                    Return to Donations</a></span></h2>
            <div class="content collapse-content">
                
                <%
                    using (Html.BeginForm())
                    {%>
                <%:Html.ValidationSummary(false,"Updating Cart was unsuccessful. Please correct the errors and try again.",new { @class = "messages error" })%>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript() %>
                <%  if (Model.ContainsDonations())
                    { %>
                <table class="store mycart" style="width: 100%;">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td style="width: 100px; text-align: center;">
                                Remove
                            </td>
                            <td>
                                Description
                            </td>
                            <td style="width: 100px;">
                                Total
                            </td>
                        </tr>
                    </thead>
                    <tbody valign="top">
                        <%
                        foreach (var cartItem in Model.CartItems.Where(x => x.Item is DonationCartItem))
                        {%>
                        <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/")%>
                        <%}%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="subtotal">
                                Donations Subtotal:
                            </td>
                            <td class="cost">
                                <%: Model.FormatWithCompanyCulture("C", Model.DonationstemsCartSubTotal ) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%}
                    else
                    {%>
                <h6>
                    Empty</h6>
                <%} %>
                <div style="width: 100%; text-align: right;">
                    Add a One-Time Donation:
                    <%= GetCurrencySymbol() %>
                    <%: Html.TextBox("OneTimeDonation",string.Empty, new {style="width: 50px;"})%>.00
                    <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton(MagicStringEliminator.EasyButtonLabel.AddToCart, MagicStringEliminator.CheckoutActions.AddOneTimeDonation, MagicStringEliminator.EasyButtonIcon.Buy))%>
                </div>
            </div>
            <div class="content">
            </div>
            <h2 class="title collapse-control">
                <span class="arrow">&#x025BC;</span>My Products <span class="more-link"><a href="<%: Model.CatalogReturnToUrl %>">
                    Continue Shopping</a></span></h2>
            <div class="content collapse-content">
                <%  if (Model.ContainsStockItem())
                    { %>
                <table class="store mycart" style="width: 100%;">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td style="width: 100px; text-align: center;">
                                Remove
                            </td>
                            <td>
                                Description
                            </td>
                            <td style="width: 75px;">
                                Qty.
                            </td>
                            <td style="width: 75px;">
                                Price
                            </td>
                            <td style="width: 100px;">
                                Total
                            </td>
                        </tr>
                    </thead>
                    <tbody valign="top">
                        <%
                        foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem))
                        {%>
                        <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/")%>
                        <%}%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="subtotal" colspan="3">
                                Products Subtotal:
                            </td>
                            <td class="cost">
                                <%: Model.FormatWithCompanyCulture("C", Model.StockItemsCartSubTotal) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <%}
                    else
                    {%>
                <h6>
                    Empty</h6>
                <%} %>
            </div>
            
            
            <%  if (Model.ContainsGifCards())
                { %>
                    <div class="content">
                    </div>
                    <h2 class="title collapse-control">
                        <span class="arrow">&#x025BC;</span>Gift Cards<span class="more-link"><%: Html.ActionLink("Add Another Gift Card","Index","GiftCards") %></span>
                    </h2>
                    <div class="content collapse-content">
                        <table class="store mycart" style="width: 100%;">
                            <thead>
                                <tr style="font-weight: bold;">
                                    <td style="width: 100px; text-align: center;">
                                        Remove
                                    </td>
                                    <td>
                                        Description
                                    </td>
                                    <td style="width: 75px;">
                                        Qty.
                                    </td>
                                    <td style="width: 75px;">
                                        Price
                                    </td>
                                    <td style="width: 100px;">
                                        Total
                                    </td>
                                </tr>
                            </thead>
                            <tbody valign="top">
                                <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is GiftCardCartItem))
                                {%>
                                <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/")%>
                                <%}%>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td class="subtotal" colspan="3">
                                        Gift Cards Subtotal:
                                    </td>
                                    <td class="cost">
                                        <%: Model.FormatWithCompanyCulture("C", Model.GiftCardsCartSubTotal) %>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                <%} %>
                    
            
            
            <!-- SUBSCRIPTION ITEMS -->
            <%  if (Model.ContainsSubscriptionItems())
                { %>
            <div class="content">
            </div>
            <h2 class="title collapse-control">
                <span class="arrow">&#x025BC;</span>Subscriptions<span class="more-link"><%: Html.ActionLink("Add Another Subscription","JhmMagazineSubscription","Resources") %></span></h2>
            <div class="content collapse-content">
                <table class="store mycart" style="width: 100%;">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td style="width: 100px; text-align: center;">
                                Remove
                            </td>
                            <td>
                                Description
                            </td>
                            <td style="width: 100px;">
                                Total
                            </td>
                        </tr>
                    </thead>
                    <tbody valign="top">
                        <%
                        foreach (var cartItem in Model.CartItems.Where(x => x.Item is SubscriptionCartItem))
                        {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/")%>
                        <%}%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="subtotal">
                                Subscriptions Subtotal:
                            </td>
                            <td class="cost">
                                <%: Model.FormatWithCompanyCulture("C", Model.SubscriptionsCartSubTotal ) %>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <%} %>
            <!-- END SUBSCRIPTION ITEMS -->

                
                 <!-- DISCOUNT ITEMS -->
            <%  if (Model.DiscountItems.Any())
                { %>
            <div class="content">
            </div>
            <h2 class="title collapse-control">
                <span class="arrow">&#x025BC;</span>Promotional Items<span class="more-link"><a href="<%: Model.CatalogReturnToUrl %>">Continue Shopping</a></span></h2>
            <div class="content collapse-content">
                <table class="store mycart" style="width: 100%;">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td>
                                Description
                            </td>
                            <td style="width: 75px;">
                                Qty.
                            </td>
                            <td style="width: 75px;">
                                Price
                            </td>
                            <td style="width: 100px;">
                                Total
                            </td>
                        </tr>
                    </thead>
                    <tbody valign="top">
                        <%
                        foreach (var cartItem in Model.DiscountItems)
                        {%>
                            <%=Html.RenderPartialEditByTypeName(cartItem, "~/Views/Shared/PartialViews/ObjectDisplayEditControls/Checkout/CartItems/ShoppingCart/")%>
                        <%}%>
                        <%--<tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="subtotal" colspan="3">
                                Products Subtotal:
                            </td>
                            <td class="cost">
                                <%: Model.FormatWithCompanyCulture("C", Model.StockItemsCartSubTotal) %>
                            </td>
                        </tr>--%>
                    </tbody>
                </table>
            </div>
            <%} %>
            <!-- END DISCOUNT ITEMS -->
            
            <div class="content">
                <table class="store mycart" style="width: 100%;">
                    <tfoot>
                        <% if (Model.DiscountApplied != null)
                           { %>
                        <tr>
                            <td class="subtotal vertical-padding-thin">
                                Subtotal:
                            </td>
                            <td class="cost vertical-padding-thin" style="width: 120px;">
                                <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                            </td>
                        </tr>
                        <tr>
                            <td class="subtotal vertical-padding-thin" style="padding-top:4px;">
                                <% if (Model.DiscountApplied.ActiveRule.AppliesOnlyToItemsMatchingCriteria)
                                   { %>
                                        <img height="16" src="<%: Url.Content("~/Content/images/Icons/tag_blue.png") %>"/>
                                <% } %>
                                <%:Model.DiscountApplied.ShortDescription%>
                                (<a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','<%= MagicStringEliminator.CheckoutActions.ClearDiscount %>');">Remove
                                    code</a>):
                                <% if (Model.DiscountApplied.ActiveRule.MaxAmountOff > 0 && !Model.DiscountApplied.ActiveRule.IsFixedAmount)
                                   { %>
                                <br />
                                <span style="font-size: 12px; font-style: italic">Maximun Amount Off:
                                    <%: Model.FormatWithCompanyCulture("C", Model.DiscountApplied.ActiveRule.MaxAmountOff)%>&nbsp;&nbsp;&nbsp;</span>
                                <% } %>
                            </td>
                            <td class="cost vertical-padding-thin" style="width: 120px;">
                                <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                            </td>
                        </tr>
                        <% } %>
                        <tr>
                            <td class="subtotal">
                                Grand Total:
                            </td>
                            <td class="cost" style="width: 120px;">
                                <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <%if (Model.CartItems.IsNot().Empty())
              { %>
            <div class="content">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 50%;">
                            Promotional Code:
                            <%: Html.TextBox("DiscountCode", null, new {style="width: 150px;"})%><%: Html.ValidationMessage("DiscountCode","*",new {style="color:red"}) %>&nbsp;
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Apply code", MagicStringEliminator.CheckoutActions.ApplyDiscountCode, null))%><br />
                            <%: Html.ValidationMessage("DiscountCode",new {style="color:red"}) %>
                        </td>
                        <td style="width: 50%; text-align: right;">
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonDiv
                            (
                                new List<EasyButton>
                                    {
                                        new EasyButton(MagicStringEliminator.EasyButtonLabel.Update, MagicStringEliminator.CheckoutActions.UpdateCart), 
                                        new EasyButton(MagicStringEliminator.EasyButtonLabel.Checkout,false,null, MagicStringEliminator.CheckoutActions.Checkout,null,new{@id="opener"})
                                    }
                            )%>
                        </td>
                    </tr>
                </table>
            </div>
            <%} %>
            <%}%>
        </div>
    </div>
</asp:Content>
