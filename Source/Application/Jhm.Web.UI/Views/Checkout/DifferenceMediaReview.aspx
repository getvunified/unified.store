﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.Modules.ECommerce.CheckoutOrderReviewViewModel>" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Order Review
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        <% if (ViewData["RedirectToShoppingCartScript"] != null )
           {%>
           location.href = '<%: Url.Content("~/"+ViewData["RedirectToShoppingCartScript"]) %>';
        <%
           }%>           
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% if (ViewData["RedirectToShoppingCartScript"] != null)
       {
           return;
       } 
    %>
    <%: Html.DifferenceMediaTitleHeader("Store") %>
    <div id="storeBoxWrap">
        <div id="storeBox">
            <h1>
                Your Cart</h1>
            <div class="boxL">
                <h2>
                    Customer Information</h2>
                <p>
                    <label>
                            First name:</label>
                        <%: Html.DisplayTextFor(m => m.FirstName)%>
                        <br />
                        <label>
                            Last name:</label>
                        <%: Html.DisplayTextFor(m => m.LastName)%>
                        <br />
                        <label>
                            Email Address:</label>
                        <%: Html.DisplayTextFor(m => m.Email)%>
                        <br />
                        <label>
                            Phone:</label>
                        <%: Model.Profile.PrimaryPhone == null ? "none" : String.Format("{0} {1}-{2}",Model.Profile.PrimaryPhone.CountryCode, Model.Profile.PrimaryPhone.AreaCode, Model.Profile.PrimaryPhone.Number)%>
                </p>
            </div>
            <div class="boxR">
                
            </div>
            <div style="clear: both;">
            </div>
            <hr />
            <div class="boxL">
                <h2>
                    Shipping To:</h2>
                <p>
                    <%: String.Format("{0} {1}", Model.ShippingFirstName,Model.ShippingLastName) %><br />
                    <%: Model.ShippingAddress.Address1 %><br />
                    <%= String.IsNullOrEmpty(Model.ShippingAddress.Address2) ? String.Empty : Model.ShippingAddress.Address2 + "<br />"%>
                    <%: String.Format("{0}, {1} {2}", Model.ShippingAddress.City, Model.ShippingAddress.StateCode, Model.ShippingAddress.PostalCode)%><br />
                    <%: Model.ShippingAddressCountry %><br />
                </p>
            </div>
            <div class="boxR">
                <h2>
                    Shipping Method</h2>
                <p>
                    <% if (Model.ShouldDisplayShippingMethods)
                       {%>
                    <label>
                        Shipping Method:</label>
                    <div class="form-item">
                        <%:Model.ShippingMethod%>
                    </div>
                    <%}
                       else
                       {%>
                    <div class="description" style="font-size: larger;">
                        Your order does not require a Shipping Method.</div>
                    <%}%>
                    <br />
                    <br />
                    <br />
                </p>
            </div>
            <div style="clear: both;">
            </div>
            <hr />
            <h2>Payment Information:</h2>
            <% if (Model.ShouldDisplayPaymentMethods)
                {
                    Html.RenderPartial("PartialViews/_ViewCheckoutPayments", Model.PaymentList);
                }
                else
                { %>
                    <div class="description" style="font-size: larger;">
                        Your order does not require a payment.
                    </div>
            <% } %>
            <div style="clear: both;">
            </div>
            <hr />
            <h2>Checkout:</h2>
            <table width="100%" border="0" cellpadding="5">
                <%  if (Model.ContainsStockItem())
                    { %>
                <thead>
                    <tr>
                        <th class="product">
                            Description
                        </th>
                        <th class="price">
                            Qty.
                        </th>
                        <th class="qty">
                            Price
                        </th>
                        <th class="total">
                            Total
                        </th>
                    </tr>
                    <tr>
                        <td colspan="4"><hr /></td>
                      </tr>
                </thead>
                <tbody>
                    <%
                                foreach (var cartItem in Model.CartItems.Where(x => x.Item is StockItem))
                                {
                                    Html.RenderPartial("PartialViews/DifferenceMedia/Store/_CartLineItemDisplay", cartItem);
                                }%>
                    <tr>
                        <td colspan="4"><hr /></td>
                    </tr>   
                </tbody>
                <%} %>
                <tfoot>
                    <% if (Model.DiscountApplied != null)
                       { %>
                    <tr>
                        <td class="subtotal vertical-padding-thin cartLabel" colspan="3">
                            Subtotal before discount:
                        </td>
                        <td class="cost vertical-padding-thin">
                            <%:Model.FormatWithCompanyCulture("C", Model.CartSubTotal)%>
                        </td>
                    </tr>
                    <tr>
                        <td class="subtotal vertical-padding-thin cartLabel" colspan="3">
                            <%:Model.DiscountApplied.ShortDescription%>:
                        </td>
                        <td class="cost vertical-padding-thin" style="width: 120px;">
                            <%:Model.FormatWithCompanyCulture("C", Model.DiscountTotal*-1)%>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="subtotal cartLabel">
                            Subtotal:
                        </td>
                        <td class="cost">
                            <%: Model.FormatWithCompanyCulture("C", Model.CartSubTotalWithDiscount)%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="shipping cartLabel">
                            Shipping:
                        </td>
                        <td class="cost">
                            <%:Model.FormatWithCompanyCulture("C", Model.ShippingCost)%>
                        </td>
                    </tr>
                    <tr class="row-tax">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="tax cartLabel">
                            Tax:
                        </td>
                        <td class="cost">
                            <%:Model.FormatWithCompanyCulture("C", Model.TaxCost)%>
                        </td>
                    </tr>
                    <tr class="row-total">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="order-total cartLabel">
                            TOTAL:
                        </td>
                        <td class="cost">
                            <%:Model.FormatWithCompanyCulture("C", Model.OrderTotal)%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><hr /></td>
                    </tr>  
                </tfoot>
            </table>
        <% using (Html.BeginForm())
           {%>
        <%:Html.FormKeyValueMatchAttributeHiddenField()%>
        <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
        <div align="right">
            <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','OrderReview_Cancel');"><img src="<%: Url.Content("~/Content/dm/images/btn_previous.jpg") %>" /></a>
            <a href="javascript:EasyButtonSubmitForm('ActionBottonClicked','ProcessOrder');" onclick="$('#easyButtonDialogModal1').dialog('option','title','Placing your order...');$('#easyButtonDialogModal1').dialog('open'); return true;"><img src="<%: Url.Content("~/Content/dm/images/btn_submitPayment.jpg") %>" /></a>
        </div>
        <%
                   } %>
    </div>
    </div> 
    <div id='easyButtonDialogModal1' class='easyButtonDialogModal'>Placing your order... please wait. The system is currently processing your request.</div>
</asp:Content>
