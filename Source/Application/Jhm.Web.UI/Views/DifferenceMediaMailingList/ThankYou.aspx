﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.DifferenceMediaMailingListModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Thank You
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("E-Mail Signup") %>
    <div id="content">
    	<h1>Thank You!</h1>
        <p>You have successfully joined our mailing list!</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
