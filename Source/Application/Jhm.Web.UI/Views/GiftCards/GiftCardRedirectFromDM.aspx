﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Gift Cards Unavailable
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <%: Html.DifferenceMediaTitleHeader("JHM Gift Cards Unavailable") %>
    <br/>
    <h2>Sorry this feature is not available on this website. <a href="https://www.jhm.org/GiftCards">Click here to go to JHM Gift Cards page</a>.</h2>
    <br/>
    <br/>
</asp:Content>
