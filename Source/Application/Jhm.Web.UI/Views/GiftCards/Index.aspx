﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Jhm.Web.Core.Models.ECommerce.GiftCard>>" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Gift Cards
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .giftCardList li {
            position: relative;
            border-bottom: 1px solid #DBDBDB;
            display: inline-block;
            text-align: center;
            width: 220px;
            padding: 10px 14px;
            padding-top: 30px;
            margin-left: -4px;
            height: auto;
        }

        .giftCardList {
            text-align: left;
        }

        .giftCardList li .title {
            padding-top: 2px;
            font-size: 14px;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        jQuery(function() {
            $(".giftcard-ordernow").css("opacity","0");
            $("ul.giftCardList > li").hover(function () {
                $(this).find('.giftcard-ordernow').animate({ opacity: 1 },"fast");
                $(this).css("background-color", "#EEE");
            }, function () {
                $(this).find('.giftcard-ordernow').animate({ opacity: 0 }, "fast");
                $(this).css("background-color", "#FFF");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <h2>JHM Gift Cards</h2>
    <p>Do you have an unsaved neighbor? A family member or loved one who enjoys John Hagee Ministries or great gospel music? A friend who is down and out about losing their job or just going through a tough time right now? The Bible promises that the Word does not return void, and a gift card from John Hagee Ministries is the perfect gift for that special someone in your life. Are you looking for a unique item for a birthday or anniversary but not sure what to get? Let your friend or loved one pick an inspirational item that will bless them for years to come! Order a gift card today and know that GREAT things are on the way!</p>
    <div class="block">
        <h2 class="title">Gift Cards
            <span class="more-link"><%: Html.ActionLink("Check Gift Card Balance","CheckBalance") %></span>
        </h2>
        <div class="content">
            <div style="margin-bottom:-15px;">
                <ul class="giftCardList">
                    <% foreach (var gc in Model)
                       { %>
                            <li>
                                <a href="<%: Url.Action("Order", new{id= gc.Id}) %>">
                                    <img src="<%: gc.SmallImagePath %>"/>
                                    <br/>
                                    <div class="title"><%: gc.Title %></div>
                                    <br/>
                                    <a class="easybutton giftcard-ordernow" href="<%: Url.Action("Order", new{id= gc.Id}) %>">Choose This Card</a>
                                </a>
                            </li>
                    <% } %>
                </ul>
            </div>
            <div style="background-color:white; position:relative; padding-top:30px; height:40px; text-align: center;">
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style" style="margin: 0 auto; position:relative; left:30%">
                <%--<div class="addthis_toolbox addthis_default_style" style="position:relative; display:inline; margin: 0 auto; left:30%;">--%>
                    <a class="addthis_button_facebook_like"></a>
                    <a class="addthis_button_tweet"></a>
                    <a class="addthis_counter addthis_pill_style" style="display:inline;"></a>
                </div>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e20ccc60077564"></script>
                <!-- AddThis Button END -->
            </div>
        </div>
    </div>
</asp:Content>
