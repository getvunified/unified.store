﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.CheckGiftCardViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Check JHM Gift Card Balance
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server" >
    <div class="block">
        <h2 class="title">Check JHM Gift Card Balance
            <span class="more-link"><%: Html.ActionLink("Purchase a Gift Card","Index") %></span>
        </h2>
        <div class="content">
            <p>The balance in your card ************<%: Model.Number.Substring(Model.Number.Length - 4,4) %> is:</p><br/>
            <b><%: String.Format("{0:C}",Model.Balance) %></b>
            
            <br/>
            <br/>
            <%: Html.ActionLink("Check another card balance","CheckBalance",null,new{@class = "easybutton"}) %>
        </div>
    </div>
</asp:Content>

