﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Gift Cards Unavailable
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h2>Sorry this feature is not yet available in your current location. Please check back soon.</h2>
</asp:Content>
