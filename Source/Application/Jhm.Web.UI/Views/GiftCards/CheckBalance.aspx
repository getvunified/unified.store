﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Models.CheckGiftCardViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Check JHM Gift Card Balance
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server" >
    <div class="block">
        <h2 class="title">Check JHM Gift Card Balance
            <span class="more-link"><%: Html.ActionLink("Purchase a Gift Card","Index") %></span>
        </h2>
        <div class="content">
            <div style="float:right; text-align:center; margin:6px;">
                <strong>Back side of your card</strong>
                <br/>
                <img src="https://media.jhm.org/Images/Web/GiftCards/back.png" width="360"/>
            </div>
            <div style="float:left; width:440px">
            <p>Please enter the credit card number and the pin number from the back side of your card.</p>
            <% using(Html.BeginForm())
               { %>
                    <%: Html.LabelFor(m => m.Number) %><br/>
                    <%: Html.TextBoxFor(m => m.Number, new{style="width:260px;"}) %><br/><br/>
                    <%: Html.LabelFor(m => m.Pin) %><br/>
                    <%: Html.TextBoxFor(m => m.Pin, new{style="width:60px;"}) %><br/><br/><br/>
                    <input type="submit" value="Check Balance"/>
            <% } %>
                </div>
                <br style="clear:both;"/>
        </div>
    </div>
</asp:Content>
