﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.ECommerce.OrderGiftCardViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.ECommerce" %>
<%@ Import Namespace="Jhm.Web.UI.Controllers" %>
<%@ Import Namespace="Jhm.Web.UI.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Views" %>
<%@ Import Namespace="getv.donorstudio.core.Global" %>
<asp:Content ID="registerTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Purchase <%: Model.GiftCard.Title %>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #myFormTable .user-form-item {
            margin: 0;
            text-align: left;
        }
    </style>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/city_state.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <link href="<%: Url.Content("~/Content/css/forms.css") %>" rel="stylesheet" />
    <script type="text/javascript">
        
        <%
            var jsonAddressesList = HelperExtensions.ToJSON(Model.AddressList);
         %>
        var addresses = <%= jsonAddressesList + ";" %>;
        
        function validateGiftCardForm() {
            var formIsValid = true;

            var cardValue = jQuery("#CardValue");
            var value = parseFloat(cardValue.val());
            if (!validateJQueryField(cardValue) || isNaN(value) || value < 10.0) {
                cardValue.addClass("input-validation-error");
                formIsValid = false;
            }

            var deliveryTypeSelected = jQuery("input[name=CardDeliveryType]:checked").val();
            if ( deliveryTypeSelected == '<%= GiftCardDeliveryTypes.ToOwnAddress %>') {
                var addressId = jQuery("#DeliveryAddressId");
                if (!validateJQueryField(addressId)) {
                    formIsValid = false;
                }
            }
            else if (deliveryTypeSelected == '<%= GiftCardDeliveryTypes.ToSomeoneElse %>') {

                var address1 = jQuery("#DeliveryAddress_Address1");
                var city = jQuery("#DeliveryAddress_City");
                var country = jQuery("#DeliveryAddress_Country");
                var state = jQuery("#DeliveryAddress_StateCode");
                var firstName = jQuery("#RecipientFirstName");
                var lastName = jQuery("#RecipientLastName");
                var state = jQuery("#DeliveryAddress_StateCode");

                if (!validateJQueryField(firstName)) {
                    formIsValid = false;
                }

                if (!validateJQueryField(lastName)) {
                    formIsValid = false;
                }

                if (!validateJQueryField(address1)) {
                    formIsValid = false;
                }

                if (!validateJQueryField(city)) {
                    formIsValid = false;
                }

                if (!validateJQueryField(country)) {
                    formIsValid = false;
                }

                if (!validateJQueryField(state) || state.val() == "Select State/Provice/Division") {
                    state.addClass("input-validation-error");
                    formIsValid = false;
                }
            } else {
                formIsValid = false;
                $("#CardDeliveryTypeErrorMessage").show();
            }


            if (formIsValid) {
                return true;
            } else {
                jQuery('#validateMessage').show();
                setTimeout(function () { jQuery('#validateMessage').fadeOut(1000, null); }, 4000);
                return false;
            }
        }

        function validateJQueryField(jQueryField) {
            if (jQuery.trim(jQueryField.val()).length == 0) {
                jQueryField.addClass("input-validation-error");
                return false;
            }
            jQueryField.removeClass("input-validation-error");
            return true;
        }
        
        function toggleDeliveryOptions(deliveryType) {
            $("#CardDeliveryTypeErrorMessage").hide();
            jQuery(".deliveryTypeOption").hide();
            if (deliveryType == '<%= GiftCardDeliveryTypes.ToOwnAddress %>') {
                <% if (!Request.IsAuthenticated)
                   { %>
                        showLogonShadowBox('<%: Url.Action("Order", new {id = Model.GiftCardId}) %>?CardValue=' + $("#CardValue").val() + '&NumberOfCards=' + $("#NumberOfCards").val() + '&CardDeliveryType=' + deliveryType);
                <% }
                   else
                   { %>
                jQuery("#userAddressesContainer").show();
                <% } %>
            } else if (deliveryType == '<%= GiftCardDeliveryTypes.ToSomeoneElse %>') {
                jQuery("#someOneElseContainer").show();
            }
        }

        jQuery(function() {
            toggleDeliveryOptions('<%:Model.CardDeliveryType %>');
            
            $('#dialogModal').dialog({
                autoOpen: false,
                bgiframe: true,
                modal: true,
                resizable: false,
                width: 500,
                open: function(event, ui) {
                    var theForm = jQuery("#addNewAddressForm");
                    theForm.attr("action", '<%:Url.Action("AddNewAddress","Account")%>?returnUrl=' + escape('<%:Url.Action("Order","GiftCards", new {id = Model.GiftCardId})%>?CardValue=' + $("#CardValue").val() + '&NumberOfCards=' + $("#NumberOfCards").val() + '&CardDeliveryType=<%= GiftCardDeliveryTypes.ToOwnAddress %>'));
                }
            });
            
            $('#billingOpener').click(function () { $('#dialogModal').dialog('open'); return false; });
        });
        
        function populateBillingAddressFields(ddAddress) {
            if (ddAddress == null) {
                return;
            }
            var selectedAddressId = ddAddress.value;
            var address = getAddressById(selectedAddressId);
            if (address == null) {
                $("#spBillingAddress").html('');
                $("#spBillingCity").html('');
                $("#spBillingState").html('');
                $("#spBillingZip").html('');
                $("#spBillingCountry").html('');
                return;
            }
            $("#spBillingAddress").html(address.Address1 + (address.Address2 == '' ? '' : '<br />' + address.Address2));
            $("#spBillingCity").html(address.City);
            $("#spBillingState").html(', ' + address.StateCode);
            $("#spBillingZip").html(' ' + address.PostalCode);
            /*var country = $("#Country option[value='" + address.Country + "']").text();
            $("#spBillingCountry").html(country);*/
        }
        
        function getAddressById(addressId) {
            for (var i = 0; i < addresses.length; i++) {
                var address = addresses[i];
                if (address.DSAddressId == addressId) {
                    return address;
                }
            }
            return null;
        }
    </script>
</asp:Content>
<asp:Content ID="pageBody" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("PartialViews/_ShadowBoxLogon",new LogOnModel()) %>
    
    <div id="dialogModal" title="Add New Billing/Shipping Address" style="display:none;">
            <div class="section-checkout">
                <div class="block">
                    <p>
                        Complete the form below to add a new shipping address.</p>
                    <%
                        using (Html.BeginForm("AddNewAddress", "Account", FormMethod.Post, new { id = "addNewAddressForm" }))
                        {%>
                    <%= Html.ValidationSummary("Adding new address was unsuccessful. Please correct the errors and try again.", new { @class = "messages error" })%>
                    <%:Html.FormKeyValueMatchAttributeHiddenField("PopupActionBottonClicked")%>
                    <%:Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript("PopupActionBottonClicked")%>
                    <table>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Address Line 1:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address1",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Address1") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Address line 2:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("Address2",String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "64", @value="" })%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>City:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("City", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32", @value = "" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("City") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>Country:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList
                                (
                                    "Country",
                                    Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] { "United States", "Canada", "United Kingdom" }, "------------------------------------").ToMVCSelectList("Iso", "Name"),
                                    String.Empty,
                                    new
                                        {
                                            @class = "form-select required", 
                                            @style = "width:215px",
                                            @onchange = "set_city_state(this,'StateCode')"
                                        }
                                )%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("Country") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <span class="form-required">*</span>State/Province/Division:
                                </label>
                            </td>
                            <td>
                                <%: Html.DropDownList("State", State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new { @class = "form-select required", @style = "width:215px" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("State") %></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    Postal code:
                                </label>
                            </td>
                            <td>
                                <%: Html.TextBox("PostalCode", String.Empty, new { @class = "form-text required", @size = "32", @maxlength = "32" })%>
                                <span class="form-required" style="font-size: small;">
                                    <%: Html.ValidationMessage("PostalCode")%></span>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <div style="text-align: right;">
                        <%:Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("Add New Address",true,"Adding new address...", MagicStringEliminator.CheckoutActions.AddAddress), "PopupActionBottonClicked")%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="easybutton" href="javascript:void(0);"
                            onclick="$('#dialogModal').dialog('close');"><span class="">Cancel</span> </a>
                    </div>
                    <%        
                        }
                    %>
                </div>
            </div>
        </div>

    <div class="block">
        <h2 class="title">Purchase <%: Model.GiftCard.Title %></h2>
        <div class="content">
            <% using (Html.BeginForm("Order", "GiftCards", FormMethod.Post, new { onSubmit = "return validateGiftCardForm();" }))
               { %>
                <%: Html.FormKeyValueMatchAttributeHiddenField()%>
                <%: Html.FormKeyValueMatchAttribute_EasyButton_ValueSet_FormSubmitJavascript()%>
                    
                <%: Html.HiddenFor(m => m.GiftCardId) %>
                <table id="myFormTable">
                    <tr>
                        <td style="width:300px; text-align:center; vertical-align:top">
                            <img src="<%: Model.GiftCard.LargeImagePath %>"/>
                            <br/>
                            <h4><%: Model.GiftCard.Title %></h4>
                            
                            <br/>
                            <br/>
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style" style="margin: 0 auto; position:relative; left:24px">
                            <%--<div class="addthis_toolbox addthis_default_style" style="position:relative; display:inline; margin: 0 auto; left:30%;">--%>
                                <a class="addthis_button_facebook_like"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_counter addthis_pill_style" style="display:inline;"></a>
                            </div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50e20ccc60077564"></script>
                            <!-- AddThis Button END -->
                        </td>
                        <td style="width:170px; vertical-align:top; padding-right:10px;">
                            <h4>Card Details</h4>
                            <strong><%: Html.LabelFor(m => m.CardValue) %></strong>
                            <div class="user-form-item">
                            $ <%: Html.TextBoxFor(m => m.CardValue,new{style="width:60px;"}) %><br/>
                            <span style="color:lightcoral; font-size:12px;">* Minimun value is $10.00 USD</span>
                            </div>
                            
                            <br/>
                            <strong><%: Html.LabelFor(m => m.NumberOfCards) %></strong> <br/>
                            <%: Html.DropDownListFor(m => m.NumberOfCards, Enumerable.Range(1, 50).Select(x => new SelectListItem {Value = x.ToString(), Text = x.ToString()})) %>
                        </td>
                        <td style="width:300px; vertical-align:top;">
                            <h4>Delivery Options</h4>
                            <div>
                                <ul style="list-style-type:none;margin-left: -34px;">
                                    <li><%: Html.RadioButtonFor(m => m.CardDeliveryType, GiftCardDeliveryTypes.ToOwnAddress, new{id=GiftCardDeliveryTypes.ToOwnAddress, onClick="toggleDeliveryOptions(this.value)"}) %><%: Html.Label(GiftCardDeliveryTypes.ToOwnAddress.ToString(),"Send to one of my addresses.") %> </li>
                                    <li style="margin-top:10px;"><%: Html.RadioButtonFor(m => m.CardDeliveryType, GiftCardDeliveryTypes.ToSomeoneElse, new{id=GiftCardDeliveryTypes.ToSomeoneElse, onClick="toggleDeliveryOptions(this.value)"}) %><%: Html.Label(GiftCardDeliveryTypes.ToSomeoneElse.ToString(), "Send to someone else.") %> </li>
                                </ul>
                                <span class="form-required" style="display:none;" id="CardDeliveryTypeErrorMessage">* Please select an option from above</span>
                            </div>
                            <div id="someOneElseContainer" class="deliveryTypeOption" style="display:none;">
                                <div >
                                    <strong><%: Html.LabelFor(model => model.RecipientFirstName) %><span class="form-required">*</span></strong>
                                </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.RecipientFirstName) %>
                                    <%: Html.ValidationMessageFor(model => model.RecipientFirstName) %>
                                </div>
                                <br/>
                                <div >
                                    <strong><%: Html.LabelFor(model => model.RecipientLastName) %><span class="form-required">*</span></strong>
                                </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.RecipientLastName) %>
                                    <%: Html.ValidationMessageFor(model => model.RecipientLastName) %>
                                </div>
                                <br/>
                                <div >
                                    <strong><%: Html.LabelFor(model => model.DeliveryAddress.Address1) %><span class="form-required">*</span></strong>
                                </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.DeliveryAddress.Address1) %>
                                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.Address1) %>
                                </div>
                                <br/>
                                <div>
                                    <strong>
                                        <%: Html.LabelFor(model => model.DeliveryAddress.Address2) %></strong>
                                        </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.DeliveryAddress.Address2) %>
                                </div>
                                <br/>
                                <div>
                                    <strong>
                                        <%: Html.LabelFor(model => model.DeliveryAddress.City) %><span class="form-required">*</span></strong>
                                        </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.DeliveryAddress.City) %>
                                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.City) %>
                                </div>
                                <br/>
                                <div>
                                    <strong>
                                        <%: Html.LabelFor(model => model.DeliveryAddress.Country) %><span class="form-required">*</span></strong>
                                        </div>
                                <div class="user-form-item">
                                    <%: Html.DropDownListFor
                                            (
                                                m => m.DeliveryAddress.Country,
                                                Country.GetAllSortedByDisplayNameWithPreferenceAtTop<Country>(new string[] {"United States", "Canada", "United Kingdom"}, "------------------------------------").ToMVCSelectList("Iso", "Name", "840"),
                                                String.Empty,
                                                new
                                                    {
                                                        @style = "width:292px",
                                                        @onchange = "set_city_state(this,'DeliveryAddress_StateCode')"
                                                    } //this,'State',countryCodes,'PhoneCountryCode'
                                            ) %>
                                        <%: Html.ValidationMessageFor(model => model.DeliveryAddress.Country) %>
                                </div>
                                  <br/>
                                <div>
                                    <strong>
                                        <%: Html.LabelFor(model => model.DeliveryAddress.StateCode) %><span class="form-required">*</span></strong>
                                        </div>
                                <div class="user-form-item">
                                    <%: Html.DropDownListFor(model => model.DeliveryAddress.StateCode, State.UnitedStates.ToMVCSelectList("Code", "Name"), String.Empty, new {@style = "width:292px"}) %>
                                    <%: Html.ValidationMessageFor(model => model.DeliveryAddress.StateCode) %>
                                </div>
                                  <br/>
                                <div>
                                    <strong>
                                        <%: Html.LabelFor(model => model.DeliveryAddress.PostalCode) %>
                                    </strong>
                                </div>
                                <div class="user-form-item">
                                    <%: Html.TextBoxFor(model => model.DeliveryAddress.PostalCode) %>
                                </div>
                            </div>
                            <div id="userAddressesContainer" class="deliveryTypeOption" style="display:none;">
                                <label style="padding-top: 3px;">
                                    <span class="form-required">*</span>Delivery Address:
                                </label>
                                <div class="form-item">
                                    <%: Html.DropDownListFor
                                        (
                                        m => m.DeliveryAddressId,
                                        new SelectList(Model.AddressList, "DSAddressId", "Address1"),
                                        "(Select an address)",
                                        new
                                        {
                                            @class = "form-select required",
                                            @style = "width:200px;word-wrap: break-word",
                                            @onChange = "populateBillingAddressFields(this);"
                                        }
                                        )%>
                                    <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton("+ Add", "Add_Address", String.Empty, new { @style = "min-width:40px; padding: 2px 5px 0;min-height: 20px;", @id = "billingOpener" }))%>
                                    <span class="form-required" style="word-break:break-all">
                                        <%: Html.ValidationMessageFor(m => m.DeliveryAddressId)%>
                                    </span>
                                </div>
                                <div class="form-item">
                                    <span id="spBillingAddress"></span><br/>
                                    <span id="spBillingCity"></span><span id="spBillingState"></span><span id="spBillingZip"></span><br/>
                                    <span id="spBillingCountry"></span><br/>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td/>
                        <td colspan="2" style="text-align:right; padding-right:36px;">
                            <span id="validateMessage" class="form-required" style="display:none;">* Please fill out required fields and try again<br/></span>
                            <br/>
                            <%: Html.FormKeyValueMatchAttribute_EasyButtonLink(new EasyButton(MagicStringEliminator.EasyButtonLabel.AddToCart, icon: MagicStringEliminator.EasyButtonIcon.Buy))%>
                        </td>
                    </tr>
                </table>
            <% } %>
        </div>
    </div>
</asp:Content>

