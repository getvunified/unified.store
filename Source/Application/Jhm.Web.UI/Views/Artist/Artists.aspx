﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.ArtistViewModel>" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Difference Media
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <%--<%:Html.Partial("PartialViews/DifferenceMedia/Artists/_ArtistsIndex") %>--%>
    
    
    <%: Html.DifferenceMediaTitleHeader("Artists") %>

    <div style="margin-left: 20px; margin-top: 10px;">
        <h1>Our Artists</h1>
        <p>Please select the artist that you would like to view below</p>
        <p>&nbsp;</p>
        
        <% foreach (var artist in Model.Artists)
           {%>

            <div class="artboxWrap">
                <a href="<%: Url.Action("View", "Artist", new{id = artist.Id}) %>">
                    <img src="<%: artist.ProfilePictureUrl %>" width="170" height="113" alt="<%: artist.Name %>"/>
                </a>
                <h2>
                    <a href="<%: Url.Action("View", "Artist", new{id = artist.Id}) %>"><%: artist.Name %></a>
                </h2>
            </div>
        <%}%>
         

        <%--<div class="artboxWrap">
            <a href="<%: Url.Action("View", "Artist") %>"><img src="<%: Url.Content("~/Content/dm/images/uploads/artistPage/photos/tmp1.jpg") %>" width="170" /></a>
            <h2><a href="<%: Url.Action("View", "Artist") %>">Tanya Goodman Sykes</a></h2>
        </div>--%>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div style="clear: both;">&nbsp;</div>
    </div>
    

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="<%: Url.Content("~/Content/dm/scripts/artist.js?version=1") %>" type="text/javascript"></script>
    <link href="<%: Url.Content("~/Content/dm/css/artist.css?version=1")%>" media="all" rel="stylesheet" type="text/css" /> 
    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
