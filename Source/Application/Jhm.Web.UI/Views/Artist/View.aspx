﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/DifferenceMedia.Master"
    Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Artist>" %>

<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Difference Media
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="<%: Url.Content("~/Content/dm/css/artist.css?version=1") %>" media="all"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">
        stLight.options({ publisher: "0272acbb-4dea-4c0e-83cc-a4212a0a2dc8" });
    </script>
    <script src="<%: Url.Content("~/Content/dm/scripts/artist.js?version=1") %>" type="text/javascript"></script>
    <link href="<%: Url.Content("~/content/dm/css/customScrollBar.css")%>" media="screen" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #mcs_container {
            width: 428px;
        }
        
        #mcs_container .customScrollBox .container {
            width: 408px; /* -20 from parent */
        }
        
        #bodyContent
        {
            padding-bottom: 0px !important;
        }
        
        #photosList a {
            text-decoration: none;
        }
    </style>
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery.cycle.all.min.js") %>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/content/dm/scripts/index.js")%>"></script>
    <script type="text/javascript" src="<%: Url.Content("~/content/dm/scripts/customScroll/jquery.mCustomScrollbar.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            $("#mcs_container").mCustomScrollbar("vertical", 400, "easeOutCirc", 1.05, "auto", "yes", "yes", 10);            
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.DifferenceMediaTitleHeader("Artists") %>
    <div id="artistBio">
        <div id="artistBioL">
            <div id="largePhoto">
                <a href="<%: Model.ProfilePictureUrl %>" rel="pics" title="<%: Model.Name %>">
                    <img src="<%: Model.ProfileThumbnailUrl %>" alt="<%: Model.Name %>" />
                </a>
                <%--<a href="#">
                    <img src="<%: Url.Content("~/Content/dm/images/social_facebook.png") %>" alt="facebook icon" /></a>
                <a href="#">
                    <img src="<%: Url.Content("~/Content/dm/images/social_twitter.png") %>" alt="twitter icon" /></a>--%>
                
            </div>
            
            <% if (Model.Pictures.Any()) { %>
            <div id="thumbsWrap">
                <div id="btnLeft">
                    <a href="#" onclick="previous(); return false; ">
                        <img src="<%: Url.Content("~/Content/dm/images/photo_previous_btn.png") %>" alt="Previous Photo" />
                    </a>
                </div>
                <div id="btnRight">
                    <a href="#" onclick=" next(); return false; ">
                        <img src="<%: Url.Content("~/Content/dm/images/photo_next_btn.png") %>" alt="Next Photo" />
                    </a>
                </div>
                <h2>
                    Photos</h2>
                <div id="photosListWrap">
                    <div id="photosList">
                        <div>
                            <% for (int i = 0; i < Model.Pictures.Count; ++i)
                               {
                                   if (i%2 == 0)
                                   { %>
                            <a href="<%: Model.Pictures[i].PhotoUrl %>" rel="pics">
                                <img src="<%: Model.Pictures[i].ThumbnailUrl %>" alt="thumbnail" />
                            </a>
                            <% }
                               }%>
                        </div>
                        <div>
                            <% for (int i = 0; i < Model.Pictures.Count; ++i)
                               {
                                   if (i%2 == 1)
                                   {%>
                            <a href="<%: Model.Pictures[i].PhotoUrl %>" rel="pics">
                                <img src="<%: Model.Pictures[i].ThumbnailUrl %>" alt="thumbnail" />
                            </a>
                            <% }
                               }%>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
            <h2>
                Connect With
                <%: Model.Name %></h2>
            <p>
                <% if (!String.IsNullOrEmpty(Model.FacebookUrl))
                   { %>
                    <a href="<%: Model.FacebookUrl %>" style="text-decoration: none !important;">
                        <img src="<%: Url.Content("~/Content/dm/images/social_facebook.png") %>" alt="facebook icon" />
                    </a>
                <% } %>
                <% if (!String.IsNullOrEmpty(Model.TwitterUrl))
                   { %>
                    <a href="<%: Model.TwitterUrl %>" style="text-decoration: none !important;">
                        <img src="<%: Url.Content("~/Content/dm/images/social_twitter.png") %>" alt="twitter icon"  />
                    </a>
                <% } %>
                <% if (!String.IsNullOrEmpty(Model.WebsiteUrl))
                   { %>
                        <a href="<%: Model.WebsiteUrl %>" target="_blank" class="email"><%: Model.WebsiteUrl %></a>
                <% } 
                   else if (!String.IsNullOrEmpty(Model.EmailAddress))
                   { %>
                        <a href="mailto:<%: Model.EmailAddress %>" class="email"><%: Model.EmailAddress %></a>
                <% } %>
            </p>
        </div>
        <div id="artistBioR">
            <%--<div class="topBtns">
                <a href="#">
                    <img src="<%: Url.Content("~/Content/dm/images/btn_itunes.jpg") %>" alt="" />
                </a><a href="#">
                    <img src="<%: Url.Content("~/Content/dm/images/btn_presskit.jpg") %>" alt="" />
                </a>
            </div>--%>
            <h1>
                <%: Model.Name %></h1>
            <p>
                <%= Model.Biography %>
            </p>
        </div>
        <div style="clear: both;">
        </div>
        <% if (Model.Music.Any() || Model.Videos.Any())
           {%>
        <div id="bottomWrap" style="width: 1138px;">
            <div id="bottom" style="height: auto;">
                <div id="bottomL" style="padding-top: 0;width: 480px;">
                    <%: Html.Partial("PartialViews/DifferenceMedia/Artists/_MediaPlayer",Model) %>
                </div>
                <div id="bottomR" style="width: 428px;">
                    <div class="title" style="margin-bottom: 3px;">Media</div>
                    <br style="clear:both"/>
                    <div id="mcs_container">
                        <div class="customScrollBox">
                            <div class="container">
                                <div id="songList" class="content">
                                    <!--START Scroll Box Content -->
                                    <ul>
                                        <% foreach (var media in Model.Music)
                                           {%>
                                        <li><a href="javascript:void(0);" class="music" url="<%= media.MediaUrl %>" cover="<%= media.ImageUrl %>">
                                            <%: media.Title %></a></li>
                                        <%} %>
                                        <% foreach (var media in Model.Videos)
                                           {%>
                                        <li><a href="javascript:void(0);" class="movie" mediaId="<%= media.MediaId %>" cover="<%= media.ImageUrl %>">
                                            <%: media.Title %></a></li>
                                        <%} %>
                                    </ul>
                                    <!-- END Scroll Box Content -->
                                </div>
                            </div>
                            <div class="dragger_container">
                                <div class="dragger">
                                </div>
                            </div>
                        </div>
                        <div class="scrollUpBtn">
                        </div>
                        <div class="scrollDownBtn">
                        </div>
                    </div>
                    <!-- END Custom Scroll Box -->
                </div>
            </div>
        </div>
        <% } %>
    </div>
    <!-- END Content ------------------------------------------------------------------------------------- -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
