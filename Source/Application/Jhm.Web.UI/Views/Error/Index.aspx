<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/Site.Master" %>
<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
    <%: ViewData["ErrorTitle"] %>
</asp:Content>
<asp:Content runat="server" ID="Main" ContentPlaceHolderID="MainContent">
    <%= ViewData["ErrorMessage"] %>

    <br />
    <br />
    <a href="/">JHM | Home page</a>
</asp:Content>
