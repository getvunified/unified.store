<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/DifferenceMedia.Master" %>
<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<asp:Content runat="server" ID="Title" ContentPlaceHolderID="TitleContent">
    <%: ViewData["ErrorTitle"] %>
</asp:Content>
<asp:Content runat="server" ID="Main" ContentPlaceHolderID="MainContent">
    <%: Html.DifferenceMediaTitleHeader(ViewData["ErrorTitle"] as string)%>
    <div style="padding:10px;">
        <%= ViewData["ErrorMessage"] %>
        <br />
        <br />
        <a href="/">Home page</a>
    </div>
</asp:Content>
