﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.Core.Models.FAQViewModel>" %>

<% using (Html.BeginForm()) {%>
    <table  style="width:370px;">
        <tr>
            <td>
                <div class="editor-label">Search For *</div>
                <div class="editor-field">
                <%: Html.TextBoxFor(model => model.SearchFor, new{size = 30}) %>
                </div>
                <%: Html.ValidationMessageFor(m => m.SearchFor) %>
            </td>
            <td>
                <div class="editor-label">Search Using</div>
                <div class="editor-field">
                <%: Html.DropDownListFor(model => model.SearchUsingAsInt, (IEnumerable<SelectListItem>)ViewData["SearchUsingList"])%>
                </div>
            </td>
            <td valign="bottom">
                <input type="submit" value="Go" />
            </td>
        </tr>
    </table>
    <% if ( (bool)ViewData["ShouldShowResultsCount"] ){ %>
        <h5><%: String.Format("{0}  record{1} found for \"{2}\" using the \"{3}\" search mode.", 
                Model.ResultsCount == 0 ? "No": Model.ResultsCount.ToString(), 
                Model.ResultsCount == 1 ? String.Empty : "s", 
                ViewData["SearchForText"],
                ViewData["SearchUsing"])%>
        </h5>
    <% } %>
<% } %>