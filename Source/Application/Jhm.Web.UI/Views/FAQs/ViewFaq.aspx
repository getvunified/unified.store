﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.FAQ>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	FAQs
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        pre 
        {
            font-family: Helvetica,Arial,"Nimbus Sans L",sans-serif;
            font-size: 0.875em;
            line-height: 1.286em;
        }
    </style>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title">
                <%: String.IsNullOrEmpty(Model.ForIndexQuestion) ? Model.Question : Model.ForIndexQuestion%></h2>
            <div class="content">
                <h5><span style="font-size:large; font-weight:bold">Q.</span> <%: Model.Question %></h5>
                <span style="font-size:large; font-weight:bold; display:inline">A.</span>
                <pre class="display-field" style="display:inline;"><%= Server.HtmlDecode(Model.Answer) %></pre>
            </div>
        </div>
   </div>
    <p>
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>

