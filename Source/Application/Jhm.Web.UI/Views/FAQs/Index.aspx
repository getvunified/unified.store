﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.FAQViewModel>" %>

<%@ Import Namespace="Jhm.Common.Utilities.HTMLHelpers" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQs
</asp:Content>
<asp:Content ContentPlaceHolderID="HeadContent" runat="server">
     <style type="text/css">
        .field-validation-error
        {
            color: #900;
            font-size: 12px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Frequently Asked Questions</h2>
    <div class="region-content-top">
        <div class="block">
            <h2 class="title">
                Select from the options below to restrict your search.</h2>
            <div class="content">
                <% Html.RenderPartial("~/Views/FAQs/SearchForm.ascx"); %>
                <ul>
                    <% Html.Repeater<FAQ>(Model.GetAll(), "row", "row-alt", (faq, css) =>
                   { %>
                        <li>
                            <%: Html.ActionLink(String.IsNullOrEmpty(faq.ForIndexQuestion)? faq.Question : faq.ForIndexQuestion, "View", "FAQs",new{Id = faq.Id},null)%>
                        </li>
                    <% 
                   }); %>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>

