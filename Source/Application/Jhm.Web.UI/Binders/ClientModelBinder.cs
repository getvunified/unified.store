﻿using System;
using System.Configuration;
using System.Threading;
using System.Web.Mvc;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Data;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Jhm.Web.UI.IBootStrapperTasks;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI
{
    public class ClientModelBinder : IModelBinder
    {
        public const string clientSessionKey = "myClient";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.Model != null)
                throw new InvalidOperationException("Cannot update instances..  ClientModelBinder");

            var client = LoadClientInContext(controllerContext);
            return client;

        }

        public Client LoadClientInContext(ControllerContext controllerContext)
        {
            Client client = (Client)controllerContext.HttpContext.Session[clientSessionKey];
            if (client == null)
            {
                if (controllerContext.Controller is IWebUserHelper)
                {
                    var userHelper = controllerContext.Controller as IWebUserHelper;
                    var user = userHelper.GetLoggedInUser();
                    if (user != null && !String.IsNullOrEmpty(user.DsEnvironment))
                    {
                        client = GetNewClient(user.DsEnvironment);
                    }
                }
                if (client == null)
                {
                    client =
                        GetNewClient(
                            controllerContext.RequestContext.HttpContext.Request.ServerVariables[
                                MagicStringEliminator.ServerVariables.RemoteIP],
                            controllerContext.RequestContext.HttpContext.Request.Url.Host);
                }
                controllerContext.HttpContext.Session[clientSessionKey] = client;
            }
            controllerContext.Controller.TempData[clientSessionKey] = client;
            
            return client;
        }

        private static Client GetNewClient(string dsEnvironment)
        {
            var client = CreateDefaultClient();
            var company = Company.GetCompanyByDSEnvironment(dsEnvironment);
            client.Company = company;
            client.Country = company.DefaultCountry;
            SetCulture(client);
            return client;
        }

        private static Client GetNewClient(string clientIP, string host)
        {
            var client = CreateDefaultClient();


            //Check IP for which country
            var ipNumber = IPAddressToNumber(clientIP);
            //var ipNumber = IPAddressToNumber("198.235.15.70");

            //
            // Uncomment this section to have country set by ip address
            //
            SetClientCountryByIpAddress(client, ipNumber);

            //Bypass the above country for beta sites
            if (host.ToLower().Equals("canadabeta.test.jhm.org") || host.ToLower().Equals("canadabeta.jhm.org"))
            {
                client.Country = Country.Canada;
                client.Company = Company.JHM_Canada;
            }
            else if (host.ToLower().Equals("unitedkingdombeta.test.jhm.org") || host.ToLower().Equals("unitedkingdombeta.jhm.org"))
            {
                client.Country = Country.UnitedKingdom;
                client.Company = Company.JHM_UnitedKingdom;
            }
            else if (host.ToLower().StartsWith("localhost"))
            {

                //client.Country = Country.UnitedKingdom;
                //client.Company = Company.JHM_UnitedKingdom;

                //client.Country = Country.Canada;
                //client.Company = Company.JHM_Canada;
            }
            else if (host.ToLower().Equals("test.differencemedia.org") || host.ToLower().Equals("differencemedia.org"))
            {
                client.Country = Country.UnitedStates;
                client.Company = Company.JHM_UnitedStates;
            }
            SetCulture(client);

            return client;
        }

        private static Client CreateDefaultClient()
        {
            //DEFAULT TO U.S.
            var client = new Client() { Cart = new Cart(), Company = Company.JHM_UnitedStates, Country = Country.UnitedStates, PagingInfo = new PagingInfo() };
            client.Cart.CartUpdated += (sender, args) =>
            {
                if (System.Web.HttpContext.Current.Session != null)
                {
                    var checkoutData = System.Web.HttpContext.Current.Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                    if (checkoutData != null)
                    {
                        checkoutData.PaymentList = null;
                        System.Web.HttpContext.Current.Session.Remove("PaymentInfo");
                    }
                }

            };
            return client;
        }





        public static double IPAddressToNumber(string IPaddress)
        {
            if (IPaddress == "::1")
            {
                IPaddress = "127.0.0.1";
            }
            int i;
            string[] arrDec;
            double num = 0;
            if (IPaddress == "")
            {
                return 0;
            }
            else
            {
                arrDec = IPaddress.Split('.');
                for (i = arrDec.Length - 1; i >= 0; i = i - 1)
                {
                    num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
                }
                return num;
            }
        }

        private static void SetCulture(Client client)
        {
            Thread.CurrentThread.CurrentCulture = client.Company.Culture;
            Thread.CurrentThread.CurrentUICulture = client.Company.Culture;
        }

        private static void SetClientCountryByIpAddress(Client client, double ipNumber)
        {
            try
            {
                using (var dataSession = CreateDataSession())
                {
                    var dataReader = dataSession.ExecuteReader("select CountryCode from [IpToCountry] where IPFrom <= @Ip and IPTo >= @Ip", ipNumber);

                    if (dataReader.Read())
                    {
                        var x = dataReader["CountryCode"].ToString().Trim();

                        // New Code
                        var country = Country.FindByISOCode(x);
                        if (country != null)
                        {
                            client.Company = country.Company;
                            client.Country = country;
                        }
                        else
                        {
                            client.Company = Company.JHM_UnitedStates;
                            client.Country = Country.UnitedStates;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                client.Company = Company.JHM_UnitedStates;
                client.Country = Country.UnitedStates;

                Log.For(typeof(ClientModelBinder)).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
            }
        }


        private static IDataSession CreateDataSession()
        {

            var environment = ConfigurationManager.AppSettings["ConnectionType"];
            var dataSession = new DataSession(environment);

            return dataSession;
        }
    }
}