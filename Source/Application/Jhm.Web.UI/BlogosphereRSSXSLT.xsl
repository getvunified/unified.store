<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:content="http://purl.org/rss/1.0/modules/content/">
  <xsl:template match="/">
    <xsl:apply-templates select="rss/channel/item"/>
  </xsl:template>

  <xsl:template name="getDate">
    <xsl:param name="dateTime" />
    <xsl:value-of select="substring($dateTime,9,4)" />
    <xsl:value-of select="substring($dateTime,6,2)" />
    <xsl:text>,</xsl:text>
    <xsl:value-of select="substring($dateTime,12,5)" />
  </xsl:template>
  
  <xsl:template match="item">
    <div>
      <h5/>
      <h3>       
        <xsl:value-of select="title"/>
      </h3>
    </div>
    <div>
      <p>Posted on:
      <xsl:call-template name="getDate">
        <xsl:with-param name="dateTime" select="pubDate" />
      </xsl:call-template></p>
    </div>
    <div>
      <h4>
        <xsl:value-of select="category"/>
      </h4>
    </div>    
    <div>
    <xsl:value-of disable-output-escaping="yes" select="content:encoded"/>
    </div>   
  </xsl:template>
</xsl:stylesheet>