﻿$(document).ready(function () {
    var CardIssuers = new function () {
        var issuerInput = $("#CreditCardType");
        var ccOkImage = $("#ccOk");

        function cardIssuer(name, pattern, logo) {
            this.Name = name;
            this.Logo = logo;
            this.Pattern = pattern;
        };

        var issuers = new Array(
            new cardIssuer("amex", /^3[47][0-9]{13}$/, jQuery(".amexLogo")),
            new cardIssuer("visa", /^4[0-9]{12}(?:[0-9]{3})?$/, jQuery(".visaLogo")),
            new cardIssuer("master", /^5[1-5][0-9]{14}$/, jQuery(".mastercardLogo")),
            new cardIssuer("discover", /^6(?:011|5[0-9]{2})[0-9]{12}$/, jQuery(".discoverLogo"))
        );


        function resetIssuer() {
            //issuerInput.attr("disabled", "disabled");
            issuerInput.val("");
            ccOkImage.attr("style", "display:none");
            $('#invalidCreditCard').attr("style", "display:none");
            for (var i = 0; i < issuers.length; i++) {
                issuers[i].Logo.css({ opacity: 0.15 });
            }
        };

        function validateLuhnChecksum(cardNumber) {
            if (cardNumber.length > 19)
                return (false);

            var sum = 0; var mul = 1; var l = cardNumber.length;
            for (var i = 0; i < l; i++) {
                var digit = cardNumber.substring(l - i - 1, l - i);
                var tproduct = parseInt(digit, 10) * mul;
                if (tproduct >= 10)
                    sum += (tproduct % 10) + 1;
                else
                    sum += tproduct;
                if (mul == 1)
                    mul++;
                else
                    mul--;
            }

            return ((sum % 10) == 0);
        }

        var timer;
        function startDelayedInvalidMessage() {
            clearTimeout(timer);
            timer = setTimeout(showInvalidCCMessage, 3000);
        }

        function cancelRunningDelayedMessage() {
            clearTimeout(timer);
        }

        function showInvalidCCMessage() {
            $('#invalidCreditCard').attr("style", "display:''");
        }

        var ValidateNumber = function (cardNumber) {
            resetIssuer();

            if (!validateLuhnChecksum(cardNumber)) {
                startDelayedInvalidMessage();
                return false;
            }

            for (issuer in issuers) {
                if (issuers[issuer].Pattern != null && issuers[issuer].Pattern.test(cardNumber)) {
                    issuers[issuer].Logo.css({ opacity: 1.0 });
                    issuerInput.val(issuers[issuer].Name);
                    ccOkImage.attr("style", "display:''");
                    cancelRunningDelayedMessage();
                    return true;
                }
            }
            return false;
        };

        resetIssuer();
        return { ValidateNumber: ValidateNumber };
    };

    $("#CreditCardNumber").bind("change keyup", function () {
        CardIssuers.ValidateNumber($(this).val());
    });

    /*var ddAddress = $('#ShippingAddressId').get(0);
    if (ddAddress.options.length == 2) {
    ddAddress.selectedIndex = 1;
    populateAddressFields(ddAddress);
    }*/

    $("#CreditCardNumber").keyup();
});


var usOnlyOptions;
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function populateAddressFields(ddAddress, internationalShippingMethod, companyDefaultCountryIso, countryCodes) {
    if (ddAddress == null) {
        return;
    }
    var field = null;
    var selectedAddressId = ddAddress.value;
    var ddShippingOptions = document.getElementById('ShippingMethodValue');
    if (selectedAddressId == null || selectedAddressId == '') {
        setFieldValue("ShippingAddress_Country", '');
        setFieldValue("ShippingAddress_Address1", '');
        setFieldValue("ShippingAddress_Address2", '');
        setFieldValue("ShippingAddress_City", '');
        field = document.getElementById("ShippingAddress_StateCode");
        field.selectedIndex = -1;
        setFieldValue("ShippingAddress_PostalCode", '');
        if (ddShippingOptions) {
            ddShippingOptions.selectedIndex = 0;
        }
        return;
    }

    var address = getAddressById(selectedAddressId);
    if (address == null) {
        return;
    }

    setFieldValueAndDisable("ShippingAddress_Country", address.Country);
    set_city_state__and_phoneCode(document.getElementById('ShippingAddress_Country'), 'ShippingAddress_StateCode', countryCodes, 'ShippingPhoneCountryCode');
    setFieldValueAndDisable("ShippingAddress_Address1", address.Address1);
    setFieldValueAndDisable("ShippingAddress_Address2", address.Address2);
    setFieldValueAndDisable("ShippingAddress_City", address.City);
    setFieldValueAndDisable("ShippingAddress_StateCode", address.StateCode);
    setFieldValueAndDisable("ShippingAddress_PostalCode", address.PostalCode);
    var officeCountry = companyDefaultCountryIso;
    if (ddShippingOptions && $('#ShippingAddress_Country').val() == officeCountry) {
        if (usOnlyOptions) {
            for (var j = 0; j < Object.size(usOnlyOptions); j++) {
                var option = usOnlyOptions[j];
                //option = new Option(option.text,option.value);
                ddShippingOptions.options.add(option, 0);
            }
            usOnlyOptions = null;
        }
    }
    else {
        if (ddShippingOptions != null && usOnlyOptions == null) {
            usOnlyOptions = Object();
            var usOnlyOptionsCounter = 0;
            for (var j = ddShippingOptions.options.length - 1; j >= 0; j--) {
                if (ddShippingOptions.options[j].value != internationalShippingMethod) { // TODO: Add a property to Company with the default International Shipping method to be used here
                    usOnlyOptions[usOnlyOptionsCounter] = ddShippingOptions.options[j];
                    ddShippingOptions.remove(j);
                    usOnlyOptionsCounter++;
                }
            }
        }
    }


}

function populateBillingAddressFields(ddAddress) {
    if (ddAddress == null) {
        return;
    }
    var selectedAddressId = ddAddress.value;
    var address = getAddressById(selectedAddressId);
    if (address == null) {
        $("#spBillingAddress").html('');
        $("#spBillingCity").html('');
        $("#spBillingState").html('');
        $("#spBillingZip").html('');
        $("#spBillingCountry").html('');
        return;
    }
    $("#spBillingAddress").html(address.Address1 + (address.Address2 == '' ? '' : '<br />' + address.Address2));
    $("#spBillingCity").html(address.City);
    $("#spBillingState").html(', ' + address.StateCode);
    $("#spBillingZip").html(' ' + address.PostalCode);
    var country = $("#Country option[value='" + address.Country + "']").text();
    $("#spBillingCountry").html(country);
}
function getAddressById(addressId) {
    for (var i = 0; i < addresses.length; i++) {
        var address = addresses[i];
        if (address.DSAddressId == addressId) {
            return address;
        }
    }
    return null;
}

function setFieldValue(fieldId, value) {
    var field = document.getElementById(fieldId);
    if (field != null) {
        field.value = value;
    }
}

function setFieldValueAndDisable(fieldId, value) {
    var field = document.getElementById(fieldId);
    if (field != null) {
        field.value = value;
        field.disabled = true;
    }
}


function toggleCreditCardForm(showForm) {
    $('#IsUsingNewCreditCard').val(showForm ? "True" : "False");
    $('#creditCardForm').css("display", showForm ? "" : "none");
    if (showForm == true) {
        $('#SelectedCreditCardId').val("");
    }
}

