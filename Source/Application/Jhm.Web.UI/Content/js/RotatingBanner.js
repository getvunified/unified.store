﻿// FUNCTION TO APPLY FADE EFFECT TO BANNER SLIDESHOW
$(function () {
    $('#slideshow').cycle({
        fx: 'fade',
        speed: '1500',
        timeout: 6000,
        pager: '#nav',
        slideExpr: 'img',
        pause: 1,
        after: function () {
            $('#banner_title').html(this.name);
            $('#banner_subtitle').html(this.alt);
        }
    });
});