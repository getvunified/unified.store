
/*
 * jQuery Functions
 * ----------------------------------------------------------------------------
 * 
 */ 
$(document).ready(function() {
	
	/* CALL JQUERY PLUGIN TO HIDE/SHOW TEXT IN TEXT INPUT FIELDS */
	$("#search-form .content label").inFieldLabels();
/*
 * Horizontal Primary Menu: 
 * Dynamic drop down menu with active and parent classes added on the fly.
 * 
 */
	// menu drop down 
	$("#main-menu ul li").hoverIntent(function () {
		$(this).find("ul").eq(0).stop(true,true).show("fast");
	},function() {
		$(this).find("ul").eq(0).stop(true,true).hide("fast");
	});	
	
	// adds menu .active class to elements
$("#main-menu ul ul").hoverIntent(function () {
		$(this).parent().find("a").eq(0).addClass("active");
	},function() {
		if (!$(this).parent().find("a").eq(0).hasClass("current")) {
			$(this).parent().find("a").eq(0).removeClass("active");
		}
	});	
	
	// adds menu .parent class to li with a nested ul
	$("#main-menu ul li ul").parent("li").addClass("parent");
	
	// SHOW AND HIDE CONTACT INFO MENU
	$("#secondary-menu ul li.middle").hoverIntent(function() {
		$(this).find("ul").eq(0).stop(true,true).slideDown('fast');
	},function() {
		$(this).find("ul").eq(0).stop(true,true).slideUp("fast");
	});

    // no-spaces restriction in username fields
    $('#UserName').keyup(function () {
        var text = $(this).val();
        $(this).val(text.replace(/[\s]/g, ""));
    });
	
/*
 * Verticle Block Menu: 
 * Dynamic drop down menu with active and parent classes added on the fly.
 * 
 */ 	
	
	
    
    
    // prepend the .menu_nodes where desired
    $('.block ul.links li').prepend('<span class="menu_node"></span>');
  
    // activate .menu_nodes and hide child menus
    $('.block ul.links ul').css('display','none').parent('li').children('.menu_node').addClass('closed');
  
    // close child uls
    $('.block ul.links ul ul').css('display','none');
    
    // set .menu_node actions
    $('.block ul.links .menu_node.closed, .block ul.links .menu_node.open').bind("click", function(e){
    	$(this).toggleClass('closed').toggleClass('open');
    	$(this).parent().find('ul').eq(0).toggle('fast');
    });

	
    //open the nav from the open node
    $('.block ul.links li.current').parents('ul').css('display','block');
    $('.block ul.links li.current').parents('li').find('> a .menu_node').toggleClass('closed').toggleClass('open');
    $('.block ul.links li.current').children('ul').css('display','block');
    $('.block ul.links li.current:has("ul")').find('a:first .menu_node').toggleClass('closed').toggleClass('open');
	
	
	
	
	

	
	// Shopping Cart
    
    $('h2.title.collapse-control .arrow').each(function() {
    	$(this).data('collapsed',0);
    	
    	$(this).click(function() {
    		var collapsed = $(this).data('collapsed');
    		
    		if (collapsed == 0) {
    			$(this).html('&#x025BA;');
    			$(this).parent().next('.collapse-content').slideUp("slow");
    			$(this).data('collapsed',1);
    		} else {
    			$(this).html('&#x025BC;');
    			$(this).parent().next('.collapse-content').slideDown("slow");
    			$(this).data('collapsed',0);
    		}
    	});
    });
	
    
   // MENU SELECT STYLING
    var options = {
		styleClass: "selectDark",
		jScrollPane: 1
	}

    $(".mySelect").styleSelect(options);
       
});	



