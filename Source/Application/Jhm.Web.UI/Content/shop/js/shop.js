jQuery(document).ready(function(jQ) {
    //tab functionality
    jQ('.tab').click(function () {
        jQ('ul > li.active').removeClass('active');
        jQ(this).parent().parent().addClass('active');
        jQ('.tab-content').removeClass('tab-content-active');
        jQ(this.rel).addClass('tab-content-active');
        return false;
    });
    
    //slider functionality (accordion)
    jQ(".slide-title h3").wrapInner('<a />');
    jQ(".slide-title h3:last").addClass('last-slide');
    jQ(".slide-content").hide();
    jQ(".slide-wrapper .slide-group").each(function(index) {
        var group_select = 'group' + index;
        jQ('.slide-content', this).addClass(group_select);
        jQ("h3 a", this).click(function(index) {
            jQ('.slide-content').slideUp(300);
            if(jQ(this).hasClass('active') === false) {
                jQ('div.' + group_select).slideDown(300);
                jQ(this).addClass('active');
            } else {
                jQ('h3 a').removeClass('active');
            }
            return false;
        });
    });

    jQ("[name='stepActive']").each(function (index) {
        jQ('.slide-content', this).show();
        jQ('h3 a', this).addClass('active');
    });

    jQ("[name='lastStep']").each(function (index) {
        jQ('h3 a', this).addClass('active');
    });

    var hash = window.location.hash;

    if (hash == '') {
        if (jQ(".main-tab-content:eq(0)").length > 0) {
            jQ(".main-tab-content:eq(0)").addClass("tab-content-active");
            var optionId = jQ(".main-tab-content:eq(0)")[0].id;
            jQ("[rel='#" + optionId + "']").parent().parent().addClass("active");
        }
    }
    else {
        jQ(hash).addClass("tab-content-active");
        jQ("[rel='" + hash + "']").parent().addClass("active");
    }

    location.hash = '';
    
});