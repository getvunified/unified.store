// JavaScript Document
$(document).ready(function () {

    $("a.inline").fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'titlePosition': 'over',
        'width': 720,
        'height': 430,
        'autoDimensions': 'false',
        'margin': 0,
        'padding': 0
    });
});