// JavaScript Document
$(document).ready(function(){
	
	$("a[rel=pics]").fancybox({
				'transitionIn'		: 'elastic',
				'transitionOut'		: 'elastic',
				'titlePosition' 	: 'over',
				'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
					return '<span id="fancybox-title-over"> ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
				}
	});
	
	maxWidth = 0 - $('#photosList').width();
	windowWidth = 259;
	left = 0;
	
	/*
	$('#photosList div a').click(function() {
		image = $(this).find("img").attr('src');
		$("#largePhoto a img").attr('src', image);
		$("#largePhoto a").attr('href', image);
	});
	*/
});

function next (){
	if(left > maxWidth){
		newLeft = left - windowWidth;
		
		$('#photosList').animate({
			'margin-left': newLeft
	  	}, 1000, function() {
			// Animation complete.
	  	});
		left = newLeft;
	}
}

function previous(){
	if(left < 0){
		newLeft = left + windowWidth;
		
		$('#photosList').animate({
			'margin-left': newLeft
	  	}, 1000, function() {
			// Animation complete.
	  	});
		left = newLeft;
	}
}