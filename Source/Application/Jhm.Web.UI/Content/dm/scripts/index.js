// JavaScript Document
$(document).ready(function () {
    $("a[rel=pics]").fancybox({
        'titlePosition': 'over',
        'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
            return '<span id="fancybox-title-over"> ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
        }
    });
});

/* START Artist Box --------------------------------------------------------------------------------------------- */
var slideShowHolderWrap = "#artistBoxLSlider";  //The ID of the container holding your slide show pics
var artistBoxHolderWrap = "#artistBoxRWrap";
var controlBox = "#dots"
var slideShowDelay = 5000;

var leftWindowWidth = 600;
var rightWindowHeight = 377;

var numOfArtists = 0;

$(document).ready(function () {
    /* START Center the dots  */
    dotsWidth = $('#artistBoxWrap #dots').width();
    dotsLeft = 311;
    dotsNewLeft = dotsLeft - (dotsWidth / 2);
    $('#artistBoxWrap #dots').css('left', dotsNewLeft + 'px');
    /* END Center the dots */

    numOfArtists = $(slideShowHolderWrap + " li").size();
    $(controlBox + " li").click(function () {
        if (intval) { clearInterval(intval); intval = null; }
        index = $(this).index();
        slideTo(index);

        $(controlBox + ' li').removeClass('active');
        $(controlBox + ' li:eq(' + index + ')').addClass('active');
    });

    intval = window.setInterval("nextPic(0)", slideShowDelay);
});



function slideTo(slide) {
    newLeft = slide * leftWindowWidth;
    newLeft = newLeft - (newLeft * 2);

    newLeftArtist = slide * rightWindowHeight;
    newLeftArtist = newLeftArtist - (newLeftArtist * 2);

    $(slideShowHolderWrap).animate({
        'margin-left': newLeft
    }, 1000, function () {
        // Animation complete.
    });
    $(artistBoxHolderWrap).animate({
        top: newLeftArtist
    }, 1000, function () {
        // Animation complete.
    });
}

function nextPic(fromUser) {
    if (fromUser == 1) {
        if (intval) { clearInterval(intval); intval = null; }
    }
    currentIndex = $(controlBox + " li.active").index();
    if ((numOfArtists - 1) > currentIndex) {
        newIndex = currentIndex + 1;
        slideTo(newIndex);
        $(controlBox + ' li:eq(' + currentIndex + ')').removeClass('active');
        $(controlBox + ' li:eq(' + newIndex + ')').addClass('active');
    } else {
        newIndex = 0;
        slideTo(newIndex);
        $(controlBox + ' li:eq(' + currentIndex + ')').removeClass('active');
        $(controlBox + ' li:eq(' + newIndex + ')').addClass('active');
    }
}

function prevPic(fromUser) {
    if (fromUser == 1) {
        if (intval) { clearInterval(intval); intval = null; }
    }
    currentIndex = $(controlBox + " li.active").index();
    if (currentIndex > 0) {
        newIndex = currentIndex - 1;
        slideTo(newIndex);
        $(controlBox + ' li:eq(' + currentIndex + ')').removeClass('active');
        $(controlBox + ' li:eq(' + newIndex + ')').addClass('active');
    };
}
/* END Artist Box --------------------------------------------------------------------------------------------- */
/* START Bottom Box */
curBox = 1;
function changeMedia(box, btn) {
    if (box != curBox) {
        $('#bottomL .buttons a').removeClass('featuredActive');
        $(btn).addClass('featuredActive');
        $('#box' + curBox).fadeOut(1000);
        $('#box' + box).fadeIn(1000);
        curBox = box;
    }
}
/* EDN Bottom Box */