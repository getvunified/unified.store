//*******************************
LongTail Video Terms & Conditions
//*******************************
"The Akamai Advanced JW Player Provider plugin should only be used with the JW Player licensed under the JW Player Commercial License. (To purchase a JW Player Commercial License if you are not already licensed, visit http://www.longtailvideo.com/order/


//*******************************
Akamai License Information
//*******************************
The Akamai Advanced JW Player Provider plugin is licensed under the Akamai Commercial License (HD Player Component license).