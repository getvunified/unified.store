﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Xsl;
using AlexJamesBrown.JoeBlogs.Structs;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using System.Net.Mail;
using System.Collections.Specialized;
using Jhm.Web.UI.Admin.Models;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using Jhm.Web.UI.Recaptcha;
using AlexJamesBrown.JoeBlogs;
using iTextSharp.text.pdf;
using System.Configuration;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("section-store", "sidebar-second")]
    //[RequireHttps(RequireSecure = false)]
    public class ResourcesController : BaseController<IServiceCollection>
    {
        private static bool IsAuthenticated
        {
            get { return System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        private readonly StringIgnoreCaseComparer<String> _ignoreCaseComparer = new StringIgnoreCaseComparer<string>();

        public ResourcesController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult SalvationMessage(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
        }

        [Authorize]
        public ActionResult TvBroadcast300k(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View(new CartViewModel { LightCart = cart });
        }

        [Authorize]
        public ActionResult TvBroadcast100k(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View(new CartViewModel { LightCart = cart });
        }

        [Authorize]
        public ActionResult TvBroadcast32kAudio(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View(new CartViewModel { LightCart = cart });
        }

        [Authorize]
        public ActionResult ChurchBroadcastFlash(Client client)
        {
            return RedirectToAction("OnlineProgramming");
        }

        public ActionResult PraiseReport(Client client)
        {
            return View("PraiseReportForm", new PraiseReportViewModel { LightCart = client.Cart });
        }

        [HttpPost]
        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult PraiseReport(Client client, PraiseReportViewModel reportInfo, bool captchaIsValid)
        {
            if (!captchaIsValid)
            {
                ModelState.AddModelError(MagicStringEliminator.Captcha.ValidationErrorFieldKey,
                                         "You did not type the verification words correctly. Please try again.");
            }
            if (ModelState.IsValid)
            {
                return TryOnEvent(() =>
                                      {
                                          try
                                          {
                                              var body =
                                                  System.IO.File.ReadAllText(
                                                      Server.MapPath("/EmailTemplates/praise.html"));
                                              var replacements = new ListDictionary
                                                                     {
                                                                         {
                                                                             "<%FIRST_NAME%>",
                                                                             reportInfo.FirstName ?? String.Empty
                                                                             },
                                                                         {"<%EMAIL%>", reportInfo.Email ?? String.Empty},
                                                                         {
                                                                             "<%IMAGES_PATH%>",
                                                                             "http://" +
                                                                             HttpContext.Request.ServerVariables[
                                                                                 "HTTP_HOST"] +
                                                                             Url.Content("/EmailTemplates/")
                                                                             }
                                                                     };

                                              var md = new MailDefinition
                                                           {
                                                               From = EmailHelper.EmailFromAccount,
                                                               IsBodyHtml = true,
                                                               Subject =
                                                                   "John Hagee Ministries - Praise Report Confirmation"
                                                           };

                                              var msg = md.CreateMailMessage(reportInfo.Email, replacements, body,
                                                                             new System.Web.UI.Control());
                                              // Sending email to client
                                              service.EmailService.SendEmail(msg);

                                              var sb = new StringBuilder();
                                              sb.AppendLine("<h4>Praise Report Form</h4>");
                                              sb.Append(EmailHelper.MapObjectToString(reportInfo,
                                                                                      typeof(PraiseReportViewModel)));
                                              msg = new MailMessage(reportInfo.Email,
                                                                    EmailHelper.PraiseEmailAccount, "New Praise Report",
                                                                    sb.ToString());
                                              msg.IsBodyHtml = true;
                                              // Sending email to representatives
                                              service.EmailService.SendEmail(msg);
                                          }
                                          catch (Exception ex)
                                          {
                                              Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                                          }

                                          return View("PraiseReportResponse", new CartViewModel { LightCart = client.Cart });
                                      });
            }
            return MapCartRenderView(client, "PraiseReportForm", reportInfo);
        }

        public ActionResult PrayerRequest(Client client, PageTypes pageType = PageTypes.Default)
        {
            var view = View("PrayerRequestForm", new PrayerRequestViewModel { LightCart = client.Cart });
            view.MasterName = pageType.ToString();
            return view;
        }

        [HttpPost]
        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult PrayerRequest(Client client, PrayerRequestViewModel requestInfo, bool captchaIsValid, PageTypes pageType = PageTypes.Default)
        {
            if (!captchaIsValid)
            {
                ModelState.AddModelError(MagicStringEliminator.Captcha.ValidationErrorFieldKey,
                                         "You did not type the verification words correctly. Please try again.");
            }
            if (ModelState.IsValid)
            {
                return TryOnEvent(() =>
                                      {
                                          var view = View("PrayerRequestResponse", new CartViewModel { LightCart = client.Cart });
                                          try
                                          {
                                              var body =
                                                  System.IO.File.ReadAllText(
                                                      Server.MapPath("/EmailTemplates/prayer.html"));
                                              var replacements = new ListDictionary
                                                                     {
                                                                         {
                                                                             "<%FIRST_NAME%>",
                                                                             requestInfo.FirstName ?? String.Empty
                                                                             },
                                                                         {
                                                                             "<%EMAIL%>", requestInfo.Email ?? String.Empty
                                                                             },
                                                                         {
                                                                             "<%IMAGES_PATH%>",
                                                                             "http://" +
                                                                             HttpContext.Request.ServerVariables[
                                                                                 "HTTP_HOST"] +
                                                                             Url.Content("/EmailTemplates/")
                                                                             }
                                                                     };

                                              var md = new MailDefinition
                                                           {
                                                               From = EmailHelper.EmailFromAccount,
                                                               IsBodyHtml = true,
                                                               Subject =
                                                                   "John Hagee Ministries - Prayer Request Confirmation"
                                                           };

                                              var msg = md.CreateMailMessage(requestInfo.Email, replacements, body,
                                                                             new System.Web.UI.Control());
                                              // Sending email to client
                                              service.EmailService.SendEmail(msg);

                                              var sb = new StringBuilder();
                                              sb.AppendLine("<h4>Prayer Request Form</h4>");
                                              sb.Append(EmailHelper.MapObjectToString(requestInfo,
                                                                                      typeof(PrayerRequestViewModel)));

                                              msg = new MailMessage(requestInfo.Email,
                                                                    EmailHelper.PrayerEmailAccount,
                                                                    "New Prayer Request", sb.ToString());
                                              msg.IsBodyHtml = true;
                                              service.EmailService.SendEmail(msg);
                                          }
                                          catch (FormatException ex)
                                          {
                                              ModelState.AddModelError("Email", "Email address is invalid");
                                              view = MapCartRenderView(client, "PrayerRequestForm", requestInfo);
                                              view.MasterName = pageType.ToString();
                                              return view;
                                          }
                                          catch (Exception ex)
                                          {
                                              Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                                              ModelState.AddModelError(String.Empty, ex.Message);
                                              view = MapCartRenderView(client, "PrayerRequestForm", requestInfo);
                                              view.MasterName = pageType.ToString();
                                              return view;
                                          }
                                          view.MasterName = pageType.ToString();
                                          return view;
                                      });
            }
            var view2 = MapCartRenderView(client, "PrayerRequestForm", requestInfo);
            view2.MasterName = pageType.ToString();
            return view2;
        }


        public ActionResult ViewNews(Client client, string id)
        {
            return RedirectToAction("JhmBlog");
            //var list = new LatestNewsViewModel();
            //var news = list.GetById(id);
            //if (news.IsNull())
            //{
            //    return RedirectToAction("LatestNews");
            //}
            //return MapCartRenderView(client, "ViewNews", news);
        }

        public ActionResult LatestNews(Client client)
        {
            return RedirectToAction("JhmBlog");
            //return TryOnEvent(() =>
            //                      {
            //                          var searhUsingTypes =
            //                              from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
            //                              select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
            //                          ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name",
            //                                                                       SearchUsingTypes.AllWords);
            //                          ViewData["ShouldShowResultsCount"] = false;
            //                          var model = new LatestNewsViewModel();

            //                          return MapCartRenderView(client, "LatestNewsIndex", model);
            //                      });
        }

        [HttpPost]
        public ActionResult LatestNews(Client client, LatestNewsViewModel searchData)
        {
            return TryOnEvent(() =>
                                  {
                                      var searhUsingTypes =
                                          from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
                                          select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
                                      ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name",
                                                                                   searchData.SearchUsing.ToString());

                                      if (searchData.SearchFor.IsNullOrEmpty())
                                      {
                                          ModelState.AddModelError("SearchFor", "Search for text cannot be empty");
                                          //searchData.SetNewsResults(searchData.GetAll());
                                          ViewData["ShouldShowResultsCount"] = false;
                                      }
                                      if (ModelState.IsValid)
                                      {
                                          searchData.SearchUsing = (SearchUsingTypes)searchData.SearchUsingAsInt;

                                          var allNews = searchData.GetAll();

                                          var filteredNews = GetFilteredList(allNews, searchData);


                                          ViewData["ShouldShowResultsCount"] = true;
                                          ViewData["SearchForText"] = searchData.SearchFor;
                                          ViewData["SearchUsing"] = searchData.SearchUsing;

                                          searchData.SetNewsResults(new List<XmlSyndicationRecordViewModel>(filteredNews));
                                      }

                                      return MapCartRenderView(client, "LatestNewsIndex", searchData);
                                  });
        }

        private IEnumerable<XmlSyndicationRecordViewModel> GetFilteredList(
            IEnumerable<XmlSyndicationRecordViewModel> allItems, SearchableViewModel searchData)
        {
            IEnumerable<XmlSyndicationRecordViewModel> filteredList = null;
            const string splitWordsPattern = @"\W";
            var searchWords = Regex.Split(searchData.SearchFor, splitWordsPattern);


            switch (searchData.SearchUsing)
            {
                case SearchUsingTypes.AllWords:
                    filteredList = allItems.Where(x =>
                                                      {
                                                          var questionWords = Regex.Split(x.Title, splitWordsPattern);
                                                          var answerWords = Regex.Split(x.Content, splitWordsPattern);
                                                          bool questionContainsSearch =
                                                              questionWords.ContainsAllOf(searchWords,
                                                                                          _ignoreCaseComparer);
                                                          bool answerContainsSearch =
                                                              answerWords.ContainsAllOf(searchWords, _ignoreCaseComparer);
                                                          return questionContainsSearch || answerContainsSearch;
                                                          //return questionWords.ContainsAllOf(searchWords) || answerWords.ContainsAllOf(searchWords);
                                                      });
                    break;
                case SearchUsingTypes.AnyWord:
                    filteredList = allItems.Where(x =>
                                                      {
                                                          var questionWords = Regex.Split(x.Title, splitWordsPattern);
                                                          var answerWords = Regex.Split(x.Content, splitWordsPattern);
                                                          return
                                                              questionWords.ContainsAnyOf(searchWords,
                                                                                          _ignoreCaseComparer) ||
                                                              answerWords.ContainsAnyOf(searchWords, _ignoreCaseComparer);
                                                      });
                    break;
                case SearchUsingTypes.ExactPhrase:
                    filteredList =
                        allItems.Where(
                            x =>
                            x.Title.IndexOf(searchData.SearchFor, StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                            x.Content.IndexOf(searchData.SearchFor, StringComparison.InvariantCultureIgnoreCase) >= 0);
                    break;
                default:
                    filteredList = allItems;
                    break;
            }
            return filteredList;
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult SocialNetworking(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
        }

        public ActionResult UpcomingEvents(Client client)
        {
            return TryOnEvent(() =>
                                  {
                                      string xsltPath =
                                          (Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) +
                                           @"\UpcomingEventsTemplate.xsl").Replace(@"file:\", "");
                                      xsltPath = xsltPath.Replace(@"\bin", String.Empty);

                                      XslTransform xslt = new XslTransform();
                                      xslt.Load(xsltPath);
                                      XPathDocument xDoc =
                                          new XPathDocument(
                                              "http://feeds.jhm.org/ME2/Console/XmlSyndication/Display/XML.asp?xsid=74D9E94A768C4BFFA982F2ECE0F91EA0");
                                      StringWriter writer = new StringWriter();
                                      xslt.Transform(xDoc, null, writer);
                                      writer.Close();

                                      ViewData["Events"] = writer.GetStringBuilder().ToString();

                                      //StreamReader stream = new StreamReader("Books.html");
                                      //Console.Write(stream.ReadToEnd());
                                      return View(new CartViewModel { LightCart = client.Cart });
                                  });
        }


        public ActionResult ViewDevotional(Client client, string id)
        {
            DailyDevotionalViewModel list = new DailyDevotionalViewModel();
            var news = list.GetById(id);
            if (news.IsNull())
            {
                return RedirectToAction("DailyDevotionals");
            }
            return MapCartRenderView(client, "ViewDevotional", news);
        }

        public ActionResult DailyDevotionals(Client client)
        {
            return TryOnEvent(() =>
                                  {
                                      var searhUsingTypes =
                                          from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
                                          select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
                                      ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name",
                                                                                   SearchUsingTypes.AllWords);
                                      ViewData["ShouldShowResultsCount"] = false;
                                      DailyDevotionalViewModel model = new DailyDevotionalViewModel();

                                      return MapCartRenderView(client, "DailyDevotionalsIndex", model);
                                  });
        }

        [HttpPost]
        public ActionResult DailyDevotionals(Client client, DailyDevotionalViewModel searchData)
        {
            if (String.IsNullOrEmpty(searchData.SearchFor))
            {
                ModelState.AddModelError("SearchFor", "Please type some text to search for");
            }
            var searhUsingTypes = from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
                                  select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
            ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name",
                                                         searchData.SearchUsing.ToString());
            ViewData["SearchForText"] = searchData.SearchFor;
            ViewData["SearchUsing"] = searchData.SearchUsing;
            ViewData["ShouldShowResultsCount"] = false;

            if (!ModelState.IsValid)
            {
                return MapCartRenderView(client, "DailyDevotionalsIndex", searchData);
            }
            return TryOnEvent(() =>
                                  {
                                      searchData.SearchUsing = (SearchUsingTypes)searchData.SearchUsingAsInt;

                                      List<XmlSyndicationRecordViewModel> allNews = searchData.GetAll();

                                      IEnumerable<XmlSyndicationRecordViewModel> filteredList = GetFilteredList(
                                          allNews, searchData);

                                      searchData.SetDevsResults(new List<XmlSyndicationRecordViewModel>(filteredList));
                                      ViewData["ShouldShowResultsCount"] = true;

                                      return MapCartRenderView(client, "DailyDevotionalsIndex", searchData);
                                  });
        }

        //[RequireHttps(RequireSecure = false)]
        public ActionResult OnlineProgramming(Client client)
        {
            SetAnonymousCheckoutScript();
            return View("OnlineProgrammingNew", new CartViewModel { LightCart = client.Cart });
        }

        public ActionResult PrivacyPolicy(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
        }

        private void SetAnonymousCheckoutScript()
        {
            if (!IsAuthenticated)
            {
                ViewData["ShowLoginScript"] =
                    @"$('.opener').click(function() { 
                        $('#PageType').val($(this).attr('pageType'));
                        $('#dialogModal').dialog('open');
                        return false;
                    });";
            }
        }

        [Authorize]
        public ActionResult TvBroadcast(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View("TvBroadcastIndex",new CartViewModel { LightCart = client.Cart });
        }

        [Authorize]
        public ActionResult ChurchBroadcast(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View("ChurchBroadcastNew", new CartViewModel { LightCart = client.Cart });
        }



        [Authorize]
        public ActionResult JhmRadio(Client client)
        {
            return RedirectToAction("OnlineProgramming");
            //return View("JhmRadioIndex", new CartViewModel { LightCart = client.Cart });
        }

        [HttpPost]
        //[RequireHttps(RequireSecure = true)]
        [FormKeyValueMatch(MatchFormKey = "PopupActionBottonClicked", MatchFormValue = "Logon")]
        public ActionResult OnlinePrograming_Logon(Client client, FormCollection form)
        {
            SetAnonymousCheckoutScript();
            return TryOnEvent(() =>
                                  {
                                      ViewData["PageType"] = form["PageType"];
                                      form["UserName"] = form["UserName"].Trim();
                                      if (form["UserName"].Contains(" "))
                                      {
                                          ModelState.AddModelError("UserName",
                                                                   "Username cannot contain spaces. Please remove the spaces and try again");
                                      }
                                      if (ModelState.IsValid)
                                      {
                                          if (service.MembershipService.ValidateUser(form["UserName"], form["Password"]))
                                          {
                                              var action =
                                                  DoLogon(
                                                      new LogOnModel { UserName = form["UserName"], Password = form["Password"] },
                                                      client);

                                              if (action != null)
                                              {
                                                  return action;
                                              }
                                              return RedirectToAction(form["PageType"]);
                                          }
                                          ViewData["UserName"] = form["UserName"];
                                          ModelState.AddModelError("",
                                                                   "The user name or password provided is incorrect.");
                                      }

                                      ViewData["ShowLoginScript"] = ViewData["ShowLoginScript"] +
                                                                    "$('#dialogModal').dialog('open');";
                                      return MapCartRenderView(client, "OnlineProgramming",
                                                               new CartViewModel { LightCart = client.Cart });
                                  });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = "PopupActionBottonClicked", MatchFormValue = "Register")]
        public ActionResult OnlinePrograming_Register(Client client, FormCollection form)
        {
            Session[MagicStringEliminator.SessionKeys.RedirectTo] = new
                                                                        {
                                                                            controller = "Resources",
                                                                            action = form["PageType"]
                                                                        };
            return TryOnEvent(() => Redirect("~/Account/Register"));
        }

        [Authorize]
        public ActionResult NewsletterArchive(Client client)
        {
            return View("NewsletterArchive", new CartViewModel { LightCart = client.Cart });
        }

        public ActionResult HappyVeteransDay(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
        }

        //C:\ProjectCodes\JHM\Source\Application\Jhm.Web.UI\BlogosphereRSSXSLT.xsl

        public ActionResult JhmMagazine(Client client)
        {
            return TryOnEvent(() =>
            {
                var model = new JhmMagazineListingViewModel();
                model.LightCart = client.Cart;
                model.Issues = service.JhmMagazineListingService.GetAllActive();
                return View(model);
            });
        }



        [HttpPost]
        [PageCss("page-front", "no-sidebars")]
        public ActionResult FortyDaysOfPrayer(FortyDaysOfPrayerModel model)
        {
            if (ModelState.IsValid)
            {
                var data = new ENewsLetterSignUpViewModel
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    AccountNumber = -999,
                    EmailListCode = "FDP"
                };

                if (EMailSignUp(data))
                {
                    AddPopupMessage(new PopupMessageViewModel { Message = "Congratulations, you have been successfully added!", Title = "Congratulations" });

                    try
                    {
                        var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/FortyDaysOfPrayer/EmailBody.txt"));

                        var replacements = new ListDictionary
                                       {
                                           {"<%CONTENT%>", body},
                                           {"<%EMAIL%>", model.Email},
                                           {"<%HTTP_HOST%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                           {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/FortyDaysOfPrayer/")}
                                       };


                        var mailDefinition = new MailDefinition
                        {
                            From = EmailHelper.EmailFromAccount,
                            IsBodyHtml = true,
                            Subject = "40 Days of Prayer sign up"
                        };

                        var message = mailDefinition.CreateMailMessage(model.Email, replacements, body, new System.Web.UI.Control());
                        message.IsBodyHtml = true;
                        // Sending email to client
                        service.EmailService.SendEmail(message);
                    }
                    catch (Exception ex)
                    {
                        Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                    }

                }
                else
                {
                    AddPopupMessage(new PopupMessageViewModel { Message = "Sorry, there was an error adding your e-mail address", Title = "Error" });
                }
            }

            return View(model);
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult FortyDaysOfPrayer()
        {
            return View(new FortyDaysOfPrayerModel());
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult LondonConferenceEvent()
        {
            return View("EM3_LondonConferenceEvent");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult LondonEvent2014()
        {
            return View("EM3_LondonConferenceEvent2014");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult LondonEvent2015()
        {
            return View("EM3_LondonConferenceEvent2015");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult EM3_LondonConferenceEvent2015()
        {
            return View("EM3_LondonConferenceEvent2015");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult Branson2013()
        {
            return View("EM3_Branson2013");
        }

		[PageCss("page-front", "no-sidebars")]
		public ActionResult Branson2014()
		{
			return View("EM3_Branson2014");
		}

        [PageCss("page-front", "no-sidebars")]
        public ActionResult Branson2015()
        {
            return View("EM3_Branson2015");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult SATour2015()
        {
            return View("SATour2015");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult CJEHTour2015()
        {
            return View("CJEHTour2015");
        }

        private int postLoadingRate = 5;
        private int intialPostLoad = 5;

        public ActionResult JhmBlog(Client client)
        {

            var model = new BlogViewModel { LightCart = client.Cart };
            model.LoadMorePosts = intialPostLoad;

            LoadWordPressPosts(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult JhmBlog(BlogViewModel model)
        {
            model.LoadMorePosts = model.LoadMorePosts + postLoadingRate;
            LoadWordPressPosts(model);

            return View(model);
        }


        private void LoadWordPressPosts(BlogViewModel model)
        {
            string Url = "https://amomentwithmatthewhagee.wordpress.com/xmlrpc.php";
            //string Url = "https://allthegospeltoalltheblogosphere.wordpress.com/xmlrpc.php";
            string User = "Allthegospeltoalltheblogosphere";
            string Password = "PJ&PMrtb";
            //string Url = ConfigurationManager.AppSettings["WordPressBlogUrl"];
            //string User = ConfigurationManager.AppSettings["WordPressBlogUser"];
            //string Password = ConfigurationManager.AppSettings["WordPressBlogPass"];

            WordPressWrapper wrapper = new WordPressWrapper(Url, User, Password);
            var userBlogs = wrapper.GetUserBlogs();

            var BlogCategory = wrapper.GetCategories().ToLIST().Where(x => x.categoryName != "Uncategorized");


            var blogPosts = wrapper.GetRecentPosts(model.LoadMorePosts);
            var youtubePattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
            var vimeoPattern = @"vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)";
            var wholeUrlPattern = @"\[(youtube|vimeo)(.*?)\]|http(s?)://www\.(" + youtubePattern + "|" + vimeoPattern + @")([^\s]+)";
            List<Jhm.Web.Core.Models.Post> postsList = new List<Jhm.Web.Core.Models.Post>();
            foreach (var item in blogPosts)
            {
                var post = BlogViewModel.ConvertPost(item);
                var source = item.description;
                var youtubeMatch = Regex.Match(source, youtubePattern);
                var vimeoMatch = Regex.Match(source, vimeoPattern);
                string videoId = string.Empty;
                if (youtubeMatch.Success)
                {
                    videoId = youtubeMatch.Groups[1].Value;
                    var replacement = @"<iframe width='578' height='315' src='http://www.youtube.com/embed/" + videoId + "' frameborder='0' allowfullscreen></iframe>";
                    var newPattern = Regex.Replace(source, wholeUrlPattern, replacement);
                    post.Description = newPattern;
                }

                if (vimeoMatch.Success)
                {
                    videoId = vimeoMatch.Groups[1].Value;
                    var replacement = @"<iframe src='http://player.vimeo.com/video/" + videoId + "?title=0&amp;byline=0&amp;portrait=0' width='578' height='315' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
                    var newPattern = Regex.Replace(source, wholeUrlPattern, replacement);
                    post.Description = newPattern;
                }
                postsList.Add(post);
            }
            model.Posts = postsList;
            model.Category = BlogCategory;
            if (!ModelState.IsValid)
            {
                throw new ApplicationException("We are sorry for the inconvenience there is a communication error with our blog service please try again latter thank you.");
            }
        }

        public ActionResult PropheticBlessing(Client client)
        {
            //return View(new CartViewModel { LightCart = client.Cart });
            Response.StatusCode = 301;
            Response.RedirectLocation = "/Catalog/Product/B180/The Power Of The Prophetic Blessing";
            return new ContentResult();

        }


        public ActionResult PropheticBlessingEvent(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
            //Response.StatusCode = 301;
            //Response.RedirectLocation = "/Catalog/Product/B180/The Power Of The Prophetic Blessing";
            //return new ContentResult();
        }

        //[RequireHttps(RequireSecure = true)]
        public ActionResult JhmMagazineSubscription(Client client, string selectedMagazine)
        {
            return TryOnEvent(() =>
            {
                var model = new JhmMagSubscriptionViewModel();
                model.AvailableSubscriptions = service.SubscriptionService.GetAvailableMagazineSubscriptions(client.Company.DonorStudioEnvironment);
                model.LightCart = client.Cart;
                model.SelectedPriceCode = selectedMagazine;
                if (Request.IsAuthenticated)
                {
                    var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                    if (user.AccountNumber > 0)
                    {
                        var dsEnvironment = DetermineDSEnvironment(user, client);
                        var dsUser = service.AccountService.GetUser(user.Username, user.ApplicationName, dsEnvironment);
                        var activeSubscriptions = service.AccountService.GetActiveAccountSubscriptions(dsUser.AccountNumber, dsEnvironment);
                        var availableSubscriptions = model.AvailableSubscriptions as List<ISubscription> ??
                                                     model.AvailableSubscriptions.ToList();
                        foreach (var activeSubscription in activeSubscriptions)
                        {
                            if (availableSubscriptions.Any(s => s.SubscriptionCode == activeSubscription.SubscriptionCode))
                            {
                                model.ActiveSubscription = activeSubscription;
                            }
                        }
                    }
                }
                return View(model);
            });
        }

        [Authorize]
        [HttpPost]
        [PageCss("page-front", "no-sidebars")]
        //[RequireHttps(RequireSecure = true)]
        public ActionResult JhmMagazineSubscription(Client client, JhmMagSubscriptionViewModel model)
        {
            return TryOnEvent(() =>
            {
                model.IsGiftSubscription = false;
                var subscription = model.MapThis();
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                UserSubscriptionViewModel activeSubscription = null;
                subscription.User = user;
                var dsEnvironment = DetermineDSEnvironment(user, client);
                if (user.AccountNumber > 0)
                {

                    var dsUser = service.AccountService.GetUser(user.Username, user.ApplicationName, dsEnvironment);
                    var activeSubscriptions = service.AccountService.GetActiveAccountSubscriptions(dsUser.AccountNumber, dsEnvironment);
                    activeSubscription = activeSubscriptions.SingleOrDefault(x => x.Id == model.ActiveSubscription.Id);
                    subscription.User = dsUser;
                }
                subscription.IsRenewal = activeSubscription != null;
                var donorSubscription = service.SubscriptionService.GetSubscription(dsEnvironment, model.SubsciptionCode);
                var subscriberProfile = subscription.User.Profile;
                if (activeSubscription != null)
                {
                    subscription.StartDate = activeSubscription.StartDate.GetValueOrDefault().AddMonths(activeSubscription.NumberOfIssues * 2);
                }
                subscription.SubscriptionTitle = donorSubscription.Description + (subscription.IsRenewal ? " (Renewal)" : string.Empty);
                var subscriptionPrice = donorSubscription.Prices.SingleOrDefault(x => x.PriceCode == model.SelectedPriceCode) ?? new SubscriptionPrice();
                subscription.SubscriptionPrice = subscriptionPrice.Price;
                subscription.ShoppingCartDescription = String.Format(
                    "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>(Not selected yet)<br/><b>Number of Issues: </b>{2} Issues ({3})<br/><b>Start Date: </b>{4}",
                    subscriberProfile.FirstName,
                    subscriberProfile.LastName,
                    subscriptionPrice.NumberOfIssues,
                    subscriptionPrice.Description,
                    subscription.StartDate.GetValueOrDefault().ToShortDateString()
                );
                subscription.DeliveryAddress = subscriberProfile.PrimaryAddress;
                subscription.NumberOfIssues = (int)subscriptionPrice.NumberOfIssues;
                subscription.NumberOfIssuesRemaining = (int)subscriptionPrice.NumberOfIssues;
                subscription.PriceCodeDescription = subscriptionPrice.Description;
                var cartItem = new SubscriptionCartItem { UserSubscription = subscription };
                cartItem.MapCartItem(subscription);
                try
                {
                    client.Cart.AddItem(cartItem);
                }
                catch (SubscriptionCartItemExistsException ex)
                {
                    var msg =
                        @"Updating Cart was unsuccessful. Please correct the errors and try again.
                                                    <ul><li>{0}</li><ul>".FormatWith(ex.Message);
                    TempData[MagicStringEliminator.Messages.ErrorMessage] = msg;

                    //BUG:  Should model state be set to false and message instead?
                }
                return RedirectToCartPage(eMessage: null, catalogRtrToUrl: null, donationsRtrToUrl: null);
            });
            //if (!model.IsGiftSubscription)
            //{
            //    subscription.User = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
            //}

        }

        public ActionResult AddGiftSubscriptionToCart(Client client, JhmMagSubscriptionViewModel model)
        {
            return TryOnEvent(() =>
            {
                model.IsGiftSubscription = true;
                var subscription = model.MapThis();
                var donorSubscription = service.SubscriptionService.GetSubscription(client.Company.DonorStudioEnvironment, model.SubsciptionCode);
                var subscriber = subscription.User.Profile;
                subscription.SubscriptionTitle = donorSubscription.Description;
                var subscriptionPrice = donorSubscription.Prices.SingleOrDefault(x => x.PriceCode == model.SelectedPriceCode) ?? new SubscriptionPrice();
                subscription.SubscriptionPrice = subscriptionPrice.Price;
                subscription.ShoppingCartDescription = String.Format(
                    "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}<br/><b>Number of Issues: </b>{6} Issues ({7})",
                    subscriber.FirstName,
                    subscriber.LastName,
                    subscriber.PrimaryAddress.Address1,
                    subscriber.PrimaryAddress.City,
                    subscriber.PrimaryAddress.StateCode,
                    subscriber.PrimaryAddress.PostalCode,
                    subscriptionPrice.NumberOfIssues,
                    subscriptionPrice.Description
                );
                subscription.DeliveryAddress = subscriber.PrimaryAddress;
                subscription.NumberOfIssues = (int)subscriptionPrice.NumberOfIssues;
                subscription.NumberOfIssuesRemaining = (int)subscriptionPrice.NumberOfIssues;
                subscription.PriceCodeDescription = subscriptionPrice.Description;
                var cartItem = new SubscriptionCartItem { UserSubscription = subscription };
                cartItem.MapCartItem(subscription);
                try
                {
                    client.Cart.AddItem(cartItem);
                }
                catch (SubscriptionCartItemExistsException ex)
                {
                    var msg =
                        @"Updating Cart was unsuccessful. Please correct the errors and try again.
                                                    <ul><li>{0}</li><ul>".FormatWith(ex.Message);
                    TempData[MagicStringEliminator.Messages.ErrorMessage] = msg;

                    //BUG:  Should model state be set to false and message instead?
                }
                return RedirectToCartPage(eMessage: null, catalogRtrToUrl: null, donationsRtrToUrl: null);
            });
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult JacksonVilleEvent()
        {
            return View("EM3_JacksonVilleEvent2013");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult TorontoEvent()
        {
            return View("EM3_Toronto2013");
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult HerseyEvent()
        {
            return View("EM3_Hersey2013");
        }

		[PageCss("page-front", "no-sidebars")]
		public ActionResult Sacramento2014()
		{
			return View("EM3_Sacramento2014");
		}

        [PageCss("page-front", "no-sidebars")]
        public ActionResult Sacramento2014ThankYou()
        {
            return View("EM3_Sacramento2014_ThankYou");
        }

        [PageCss("", "no-sidebars")]
        public ActionResult Careers()
        {
            var model = ApplicationManager.ApplicationData.JobPostings.Values
                .Where(x => x.StartDate <= DateTime.Now)
                .Where(x => x.EndDate > DateTime.Now)
                .ToList();
            return View("CareersIndex",model);
        }

        [PageCss("", "no-sidebars")]
        public ActionResult CareerDetail(string id)
        {
            var model = ApplicationManager.ApplicationData.JobPostings.Values
                                          .SingleOrDefault(x => x.Id == id);
            if (model == null)
            {
                AddPopupMessage("Sorry, Job Posting not found!","Careers & Job Opportunities");
                return RedirectToAction("Careers");
            }
            return View("CareerDetail", model);
        }

        public ActionResult RefreshCareersCache()
        {
            ApplicationManager.ApplicationData.SetJobPostings();
            AddPopupMessage("Careers cache successfully refreshed", "Refresh Careers Cache");
            return RedirectToAction("Careers");
        }

        public ActionResult DownloadPdfApplicationForm(string id)
        {
            var jobPosting = ApplicationManager.ApplicationData.JobPostings.Values
                                          .SingleOrDefault(x => x.Id == id);
            if (jobPosting == null)
            {
                throw new Exception("No Job posting found with id:"+id);
            }

            Response.ContentType = "application/pdf";
            Response.AppendHeader(
              "Content-Disposition",
              "attachment;filename=EmploymentApplication.pdf"
            );
            var r = new PdfReader(
              new RandomAccessFileOrArray(Request.MapPath("/Content/media/EmploymentApplication.pdf")), null
            );
            using (var ps = new PdfStamper(r, Response.OutputStream))
            {
                var af = ps.AcroFields;
                af.SetField("PositionAppliedfor", jobPosting.Title);
                //ps.FormFlattening = true;
            }
            Response.End();
            return null;
        }

        [PageCss("section-store", "no-sidebars")]
        public ActionResult HageeResponseCityOrdinance(Client client)
        {
            return View("HageeResponseCityOrdinance", new CartViewModel { LightCart = client.Cart });
        }
    }


}



