﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class ArtistController: BaseController<IServiceCollection>
    {
        //
        // GET: /ArtistDifferenceMedia/

        public ArtistController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) 
            : base(service, unitOfWork)
        {}


        public ActionResult Index(Client client)
        {
            var artistList = new ArtistViewModel();
            artistList.Artists = new List<Artist>( ApplicationManager.ApplicationData.Artists.Values);
            return MapCartRenderView(client, "Artists", artistList);
        }

        public ActionResult View (Client client, string id)
        {
            if (!ApplicationManager.ApplicationData.Artists.ContainsKey(id))
            {
                return RedirectToAction("Index");
            }
            var artist = ApplicationManager.ApplicationData.Artists[id];
            if ( artist == null )
            {
                throw new Exception("Artist not found");
            }
            return MapCartRenderView(client, "View", artist);
        }
        
        public ActionResult ReloadAll()
        {
            ApplicationManager.ApplicationData.ReloadAllData();
            return RedirectToAction("Index");
        }
    }
}
