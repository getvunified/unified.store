﻿#region

using System;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;

#endregion

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class DifferenceMediaMailingListController : BaseController<IServiceCollection>
    {
        public DifferenceMediaMailingListController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        
        [HttpPost]
        public ActionResult ThankYou(Client client, string signup)
        {
            if (ModelState.IsValid)
            {
                var data = new ENewsLetterSignUpViewModel
                    {
                        Email = signup,
                        AccountNumber = 0,
                        EmailListCode = "DMG"
                    };

                if (EMailSignUp(data))
                {
                    try
                    {
                        string body =
                            System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/DifferenceMedia/Generic.html"));

                        var replacements = new ListDictionary
                            {
                                {"<%CONTENT%>", "Thank you for signing up for the Difference Media Mailing List."},
                                {"<%EMAIL%>", signup},
                                {"<%HTTP_HOST%>", "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                {
                                    "<%IMAGES_PATH%>",
                                    "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] +
                                    Url.Content("/EmailTemplates/DifferenceMedia/images/")
                                }
                            };


                        var mailDefinition = new MailDefinition
                            {
                                From = EmailHelper.EmailFromAccount,
                                IsBodyHtml = true,
                                Subject = "Difference Media Mailing List"
                            };

                        var message = mailDefinition.CreateMailMessage(signup, replacements, body,
                                                                       new System.Web.UI.Control());
                        message.IsBodyHtml = true;
                        // Sending email to client
                        service.EmailService.SendEmail(message);
                    }
                    catch (Exception ex)
                    {
                        Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                    }
                }
                else
                {
                    AddPopupMessage(new PopupMessageViewModel
                        {Message = "Sorry, there was an error adding your e-mail address", Title = "Error"});
                }
            }

            return MapCartRenderView(client, "ThankYou", new DifferenceMediaMailingListModel());
        }
    }
}
