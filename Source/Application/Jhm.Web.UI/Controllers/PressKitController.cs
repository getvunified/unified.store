﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class PressKitController : BaseController<IServiceCollection>
    {
        public PressKitController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) 
            : base(service, unitOfWork)
        {}

        public ActionResult Index(Client client)
        {
            var pressKit = new PressKitViewModel();
            pressKit.PressKitList = new List<PressKitItem>(ApplicationManager.ApplicationData.PressKits.Values);
            
            return MapCartRenderView(client, "PressKit", pressKit);
        }

        public ActionResult View(Client client, string id)
        {
            var pressKit = ApplicationManager.ApplicationData.PressKits[id];

            return MapCartRenderView(client, "View", pressKit);
        }

        public ActionResult Preview(Client client, string id)
        {
            var pressKit = ApplicationManager.ApplicationData.PressKits[id];

            return MapCartRenderView(client, "PreviewPressKit", pressKit);
        }

    }
}
