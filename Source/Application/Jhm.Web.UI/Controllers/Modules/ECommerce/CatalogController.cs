﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("section-store", "sidebar-second")]
    //[RequireHttps(RequireSecure = false)]
    public class CatalogController : BaseController<IServiceCollection>
    {
        public CatalogController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ViewResult Index(Client client, int page = 1)
        {
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            SetupDeliveryDeadlineMessage();
            if ( IsDM )
            {
                return (ViewResult)TryOnEvent(() =>
                {
                    const string publisher = "DM";
                    var totalResults = client.PagingInfo.TotalResults;
                    client.PagingInfo.CurrentPage = page;
                    var products = service.CatalogService.GetProductsByPublisher(client.Company.DonorStudioEnvironment, publisher, client.PagingInfo, ref totalResults);
                    client.PagingInfo.TotalResults = totalResults;
                    client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Index;
                    client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                    var model = ProductListingViewModel.With(null, publisher, null, null, null, products, client.PagingInfo);
                    //var model = CatalogIndexViewModel.With(products, new BindingList<IProduct>(), new BindingList<IProduct>());
                    model.RegisterClient(client);
                    return MapCartRenderView(client, "DifferenceMediaIndex", model);
                });
            }
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Index;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                //NOTE: dummy list to avoid changing CatalogIndexViewModel.With() method with 2 parameters
                var featuredProducts = new List<IProduct>();
                //var featuredProducts = service.CatalogService.GetFeaturedItems(client.Company.DonorStudioEnvironment, client.PagingInfo, ref totalResults);
                //var latestReleaseItems = service.CatalogService.GetLatestReleasedProducts(client.Company.DonorStudioEnvironment, new PagingInfo() { CurrentPage = 1, PageSize = 5 }, ref totalResults);
                var featuredOnTelevision = service.CatalogService.GetFeaturedOnTelevisionProducts(client.Company.DonorStudioEnvironment, new PagingInfo() { CurrentPage = 1, PageSize = 3 }, ref totalResults);
                var featuredOnGetv = service.CatalogService.GetFeaturedOnGetvProducts(client.Company.DonorStudioEnvironment, new PagingInfo() { CurrentPage = 1, PageSize = 3 }, ref totalResults);

                var model = CatalogIndexViewModel.With(featuredProducts, featuredOnGetv, featuredOnTelevision);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Index", model);
            });
        }



        [HttpPost]
        public ActionResult Index_ProductSearch(FormCollection formCollection)
        {
            return TryOnEvent(() => RedirectToAction("Search", new { queryString = formCollection["queryString"] }));
        }

        public ViewResult Author(Client client, string author, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
                                              {
                                                  //TODO:  Check if 'Guest' and show all other products with an author besides the other 3
                                                  var totalResults = client.PagingInfo.TotalResults;
                                                  client.PagingInfo.CurrentPage = page;
                                                  var products = service.CatalogService.GetProductsByAuthor(client.Company.DonorStudioEnvironment, author, client.PagingInfo, ref totalResults);
                                                  client.PagingInfo.TotalResults = totalResults;
                                                  client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Author;
                                                  client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;
                                                  var model = ProductListingViewModel.With(author, null, null, null, null, products, client.PagingInfo);
                                                  model.ProductListingHeader = "By Author: {0}".FormatWith(author);
                                                  model.RegisterClient(client);
                                                  return MapCartRenderView(client, "Category", model);
                                              });

        }
        public ViewResult Category(Client client, string categories, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.GetProductsByCategory(client.Company.DonorStudioEnvironment, categories, client.PagingInfo, ref totalResults);
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Category;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, categories, null, null, null, products, client.PagingInfo);
                model.ProductListingHeader = "By Category: {0}".FormatWith(categories);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Category", model);
            });

        }

        public ViewResult SpecialCaseType(Client client, string type, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                IEnumerable<IProduct> products = null;
                switch (type)
                {
                    case "Featured on Television":
                        products = service.CatalogService.GetFeaturedOnTelevisionProducts(client.Company.DonorStudioEnvironment, client.PagingInfo, ref totalResults);
                        break;
                    case "Featured on GETV":
                        products = service.CatalogService.GetFeaturedOnGetvProducts(client.Company.DonorStudioEnvironment, client.PagingInfo, ref totalResults);
                        break;
                }
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.SpecialCaseType;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, null, null, null, null, products, client.PagingInfo);
                model.ProductListingHeader = "{0}".FormatWith(type);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Category", model);
            });

        }

        public ViewResult Subject(Client client, string subjects, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.GetProductsBySubject(client.Company.DonorStudioEnvironment, subjects, client.PagingInfo, ref totalResults);
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Subject;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, null, subjects, null, null, products, client.PagingInfo);
                model.ProductListingHeader = "By Subject: {0}".FormatWith(subjects);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Category", model);
            });
        }


        public ViewResult OnSale(Client client, string onSale, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.GetProductsOnSale(client.Company.DonorStudioEnvironment, onSale, client.PagingInfo, ref totalResults);
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.OnSale;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, null, null, onSale, null, products, client.PagingInfo);
                model.ProductListingHeader = "By On Sale: {0}".FormatWith(onSale);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Category", model);
            });
        }


        public ViewResult MediaTypeLookup(Client client, string mediaTypes, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.GetProductsByMediaTypes(client.Company.DonorStudioEnvironment, mediaTypes, client.PagingInfo, ref totalResults);
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.MediaTypeLookup;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, null, null, mediaTypes, null, products, client.PagingInfo);
                model.ProductListingHeader = "By Media Type: {0}".FormatWith(mediaTypes);
                model.RegisterClient(client);
                return MapCartRenderView(client, "Category", model);
            });
        }


        public ActionResult Product(Client client, string sku)
        {
            SetupDeliveryDeadlineMessage();
            return TryOnEvent(() =>
                                  {
                                      var product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, sku);
                                      if (product.IsNull() || product.StockItems.Count() == 0)
                                      {
                                          TempData[MagicStringEliminator.Messages.ErrorMessage] = "Product Was Missing From Our Inventory, Please Select A New Product.";
                                          return RedirectToRoute(MagicStringEliminator.Routes.Catalog);
                                      }

                                      var model = CatalogProductViewModel.With(product);
                                      //var x = model.StockItems.First().BasePrice.ToString("C", client.Company.Culture);
                                      model.RegisterClient(client);
                                      return MapCartRenderView(client, IsDM? "DifferenceMediaProduct" : "Product", model);

                                  });
        }
        public ActionResult Search(Client client, string queryString, int page = 1)
        {
            SetupDeliveryDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;

            // also search active Donations - 5/15/2015  JFE
            var allDonations = new Jhm.Web.Repository.Modules.Account.MockDonationRepository();

            var activeDonations = allDonations.GetActiveDonationOpportunities();
            List<Donation> matchingDonations = new List<Donation>();
            if (!queryString.IsNullOrEmpty())
            {
                foreach (var d in activeDonations)
                {
                    if (d.Description.Contains(queryString) || d.Title.Contains(queryString) ||
                        d.Body.Contains(queryString))
                    {
                        matchingDonations.Add(d);
                    }
                }
            }
            Session["MatchingDonations"] = matchingDonations;

            return TryOnEvent(() =>
                                  {
                                      var totalResults = client.PagingInfo.TotalResults;
                                      client.PagingInfo.CurrentPage = page;
                                      var products = service.CatalogService.FindProducts(client.Company.DonorStudioEnvironment, queryString, client.PagingInfo, ref totalResults);
                                      client.PagingInfo.TotalResults = totalResults;
                                      client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Search;
                                      client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                                      var model = ProductListingViewModel.With(null, null, null, null, queryString, products, client.PagingInfo);
                                      model.ProductListingHeader = "Search For: {0}".FormatWith(queryString);
                                      model.RegisterClient(client);
                                      return View("Search", model);
                                  });
        }

        public ActionResult AddProduct(Client client, string sku, string mediaType, string returnToUrl)
        {
            return TryOnEvent(() =>
            {
                var product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, sku);
                if (product.IsNull())
                {
                    Log.For(this).Warn("Unable to find a stock item of code [{0}]".FormatWith(sku));
                    return RedirectToCartPage("Unable to find a stock item of code [{0}]".FormatWith(sku));
                }
                var mType = MediaType.All.FirstOrDefault(x => x.CodeSuffix == mediaType);
                if (mType.IsNot().Null())
                {
                    var stockItem = product.StockItems.First(x => x.MediaType == mType);
                    if (stockItem.IsNot().Null())
                    {
                        stockItem.SetAuthorDescription(product.AuthorDescription);
                        //var cart = service.CartService.GetCart(User);
                        client.Cart.AddItem(stockItem, 1);
  
                        // Added by Alex 2/26/2013
                        service.DiscountCodeService.RecalculateAppliedDiscount(client);
                        //service.CartService.SaveCart(cart);
                        return RedirectToCartPage(eMessage: null, catalogRtrToUrl: returnToUrl, donationsRtrToUrl: null);
                    }
                }

                return RedirectToCartPage("Unable to find a stock item of MediaType [{0}]".FormatWith(mediaType));
            });
        }

        

        protected override void HandleUnknownAction(string actionName)
        {
            switch (actionName.ToLower())
            {
                case "detail.asp":
                    if (String.IsNullOrEmpty(Request.QueryString["code"]))
                    {
                        Response.Redirect("~/Catalog");
                    }
                    else
                    {
                        var productCode = Regex.Replace(Request.QueryString["code"], @"[^\d]*$", String.Empty);
                        Response.Redirect("~/Catalog/Search/" + productCode);
                    }

                    break;
                case "featured.asp":
                    Response.Redirect("~/Catalog/SpecialCaseType/Featured");
                    break;
                default:
                    base.HandleUnknownAction(actionName);
                    break;
            }

        }

        private void SetupDeliveryDeadlineMessage()
        {
            var message =
                "<div style='color:red;background-color:yellow; font-weight:bold;'><h4>***ATTENTION***:</h4>For estimated delivery date by December 24th, last day to select Rush Shipping is December 18 before Noon CST. Thank you.</div>";
            var expiration = new DateTime(2013, 12, 22, 23, 59, 59);
            var cookieName = "JHM_ChritsmasRushDeliveryMessage3";
            SetupCustomPopupMessage(expiration, message, cookieName);
        }

        private void SetupCustomPopupMessage(DateTime messageExpiration, string message, string cookieName)
        {
            if (DateTime.Now > messageExpiration)
            {
                return;
            }
            TempData[MagicStringEliminator.Messages.Status] = message;
            if (Request == null)
            {
                return;
            }
            var cookie = Request.Cookies[cookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(cookieName, "true");
                cookie.Expires = messageExpiration;
                Response.Cookies.Add(cookie);
            }
            else
            {
                if (cookie.Value == "false")
                {
                    return;
                }
            }

            var popupMessage = new PopupMessageViewModel();
            popupMessage.Title = "Important Message";
            popupMessage.Message = message;
            popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Ok", HRef = "$(this).dialog('close');" });
            popupMessage.Buttons.Add(new HtmlAnchor
            {
                InnerText = "Don\\'t show this message again",
                HRef =
                    "updateCookie('" + cookieName + "','false',20);$(this).dialog('close');"
            });
            AddPopupMessage(popupMessage);
        }

        [HttpPost]
        public JsonResult SearchProducts(Client client, string query)
        {
            try
            {
                IEnumerable<IProduct> products = new List<IProduct>();
                TryWithTransaction(() =>
                {
                    var totalResults = 0;
                    products = service.CatalogService.FindProducts(client.Company.DonorStudioEnvironment, query, client.PagingInfo, ref totalResults);
                });
                var results = products.Select(
                    p => new
                    {
                        p.Title,
                        Image = Url.AbsoluteContent(p.AlternativeImages.First()),
                        p.Code,
                        p.Description,
                        Price = p.StockItems.Aggregate(p.StockItems.Any() ? String.Empty : ", ", (text, item) => text + String.Format("{0}: {1:C}, ", item.MediaType.DisplayName, item.Price), res => res.Remove(res.Length - 2)),
                        p.Author,
                        ProductURL = Url.AbsoluteContent(Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new { sku = p.Code, productTitle = p.Title.ToSafeForUrl() }))
                    });
                return Json(new { Result = "OK", Count = results.Count(), Products = results });
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("[SearchProducts]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}",ex.Message, ex.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SearchProductByCode(Client client, string code)
        {
            try
            {
                IProduct product = new Product(code);
                TryWithTransaction(() =>
                {
                    product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, code);
                });
                var results = new
                {
                    product.Title,
                    Image = Url.AbsoluteContent(product.AlternativeImages.First()),
                    product.Code,
                    product.Description,
                    Price = product.StockItems.Aggregate(product.StockItems.Any() ? String.Empty : ", ", (text, item) => text + String.Format("{0}: {1:C}, ", item.MediaType.DisplayName, item.Price), res => res.Remove(res.Length - 2)),
                    product.Author,
                    ProductURL = Url.AbsoluteContent(Url.RouteUrl(MagicStringEliminator.Routes.Catalog_Product.RouteName, new { sku = product.Code, productTitle = product.Title.ToSafeForUrl() }))
                };
                return Json(new { Result = "OK", Product = results });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", ex.Message });
            }
        }
    }
}
