﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Service;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("section-online-donations", "sidebar-second")]
    //[RequireHttps(RequireSecure = false)]
    public class DonationController : BaseController<IServiceCollection>
    {
        public DonationController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ViewResult Index(Client client)
        {
            ViewData["Company"] = client.Company.DisplayName;

            SetupGiftDeadlineMessage();
            return (ViewResult)TryOnEvent(() =>
                                               {
                                                   var donationOpportunities =
                                                       service.DonationService.GetActiveDonationOpportunities(client.Company);
                                                   var model = DonationIndexViewModel.With(donationOpportunities);
                                                   return MapCartRenderView(client, "Opportunities", model);
                                               });
        }

        private void SetupGiftDeadlineMessage()
        {
            var messageExpiration = new DateTime(2013, 1, 2, 23, 59, 59);
            if (DateTime.Now > messageExpiration)
            {
                return;
            }
            const string cookieName = "showDonationDeadlineMessage";
            const string message = "<h4>***ATTENTION***</h4><p>IN ORDER FOR YOUR ONLINE GIFT TO POST IN 2012, GIFTS MUST BE RECEIVED BEFORE 11:59 PM CST DECEMBER 31<sup>st</sup>.</p>";
            TempData[MagicStringEliminator.Messages.Status] = message;
            if (Request == null)
            {
                return;
            }
            var cookie = Request.Cookies[cookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(cookieName, "true");
                cookie.Expires = messageExpiration;
                Response.Cookies.Add(cookie);
            }
            else
            {
                if (cookie.Value == "false")
                {
                    return;
                }
            }

            var popupMessage = new PopupMessageViewModel();
            popupMessage.Title = "Important Message";
            popupMessage.Message = message;
            popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Ok", HRef = "$(this).dialog('close');" });
            popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Don\\'t show this message again", HRef = "updateCookie('" + cookieName + "','false',20);$(this).dialog('close');" });
            AddPopupMessage(popupMessage);
        }

        public ViewResult DonationDetail(Client client, string donationCode)
        {
            SetupGiftDeadlineMessage();
            return (ViewResult)TryOnEvent(() =>
                                               {
                                                   var donation = service.DonationService.GetDonation(client.Company, donationCode);
                                                   if (donation == null)
                                                   {
                                                       return Index(client);
                                                   }
                                                   var model = DonationFormViewModel.With(donation);
                                                   return MapCartRenderView(client, "DonationDetail", model);
                                               });
        }

        [PageCss("section-donate", "sidebar-second")]
        public ViewResult DonationForm(Client client, string donationCode)
        {
            SetupGiftDeadlineMessage();
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return (ViewResult)TryOnEvent(() =>
                                               {


                                                   var donation = service.DonationService.GetDonation(client.Company, donationCode);
                                                   if (donation == null)
                                                   {
                                                       return MapCartRenderView(client, "InvalidDonation", new CartViewModel());
                                                   }
                                                   var setDonationOptions = donation.DonationOptions;
                                                   if (MockDonationRepository.SaltCovenantId == donation.DonationId)
                                                   {
                                                        setDonationOptions = SetDonationOptions(client, donation);
                                                   }
                                                   if (donation == null)
                                                   {
                                                       return Index(client);
                                                   }

                                                   var model = DonationFormViewModel.With(donation);
                                                   model.DonationOptions = setDonationOptions;
                                                   return MapCartRenderView(client, "DonationForm", model);
                                               });
        }

        public List<DonationOption> SetDonationOptions(Client client, IDonation donation)
        {
            var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
            var dsEnviroment = DetermineDSEnvironment(user, client);
            var isUserASaltPartner = service.AccountService.IsUserASaltPartner(client, user, dsEnviroment);
            List<DonationOption> donationOptions = donation.DonationOptions.ToLIST();
            if (isUserASaltPartner)
            {
                donationOptions = donation.DonationOptions.ToLIST().FindAll(o => o.Id == MockDonationRepository.ExistingSaltPartnerOptionId);
            }
           
            return donationOptions;
        }

        [HttpPost]
        [PageCss("section-donate", "sidebar-second")]
        public ActionResult DonationForm(Client client, string donationCode, FormCollection formCollection, string returnToUrl = null)
        {
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return TryOnEvent(() =>
                                  {
                                      var donation = service.DonationService.GetDonation(client.Company, donationCode);
                                      
                                      var model = DonationFormViewModel.With(donation);
                                      if (model.IsValid(ViewData, formCollection))
                                      {
                                          var donationCartItem = DonationCartItemFactory.CreateFrom(donation, formCollection);
                                          //var cart = service.CartService.GetCart(User);
                                          try
                                          {
                                              client.Cart.AddItem(donationCartItem);
                                          }
                                          catch (DonationCartItemExistsException ex)
                                          {

                                              var msg =
                                                  @"Updating Cart was unsuccessful. Please correct the errors and try again.
                                                    <ul><li>{0}</li><ul>".FormatWith(ex.Message);
                                              TempData[MagicStringEliminator.Messages.ErrorMessage] = msg;

                                              //BUG:  Should model state be set to false and message instead?
                                          }
                                          //service.CartService.SaveCart(cart);                                          
                                          return RedirectToAction("Index", "Cart");
                                      }
                                      ViewData["ShouldShowDefaultInput"] = ModelState.IsValid;

                                      return MapCartRenderView(client, "DonationForm", model);
                                  });
        }

        public ActionResult InvalidDonation(Client client)
        {
            return MapCartRenderView(client, "InvalidDonation",new CartViewModel());
        }

        protected override void HandleUnknownAction(string actionName)
        {
            Response.Redirect("~/Donation/InvalidDonation");
        }
    }
}
