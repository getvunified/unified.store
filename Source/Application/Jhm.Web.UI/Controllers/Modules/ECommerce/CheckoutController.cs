﻿using System;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using MagicStringEliminator = Jhm.Web.Core.MagicStringEliminator;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("page-template", "no-sidebars")]
    //[RequireHttps(RequireSecure = true)]
    public partial class CheckoutController : BaseController<IServiceCollection>
    {
        private string CartViewName { get { return IsDM ? "DifferenceMediaCart" : "Cart"; } }
        public string PaymentViewName { get { return IsDM ? "DifferenceMediaPayment" : "Payment"; } }
        private string ShippingViewName { get { return IsDM ? "DifferenceMediaShipping" : "Shipping"; } }
        private string ReviewViewName { get { return IsDM ? "DifferenceMediaReview" : "Review"; } }

        private static bool IsAuthenticated
        {
            get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        public CheckoutController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = "OrderReview_Cancel")]
        public ActionResult OrderReviewCancelled()
        {
            return RedirectToAction(MagicStringEliminator.CheckoutActions.Cart);
        }


        public ActionResult Review(Client client)
        {
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return TryOnEvent(() =>
            {
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                if (checkoutData == null)
                {
                    ViewData["RedirectToShoppingCartScript"] =
                        MagicStringEliminator.Routes.Checkout_ShoppingCart.Route;
                    checkoutData = new CheckoutOrderReviewViewModel();
                }

                if (checkoutData.PaymentList.Balance != 0)
                {
                    AddPopupMessage(client.Company.FormatWithCulture("The remaining balance in your order is {0}. Please add or removed payments as needed.", checkoutData.PaymentList.Balance), "Checkout Error");
                    return RedirectToAction(PaymentViewName);
                }

                return MapCartRenderView(client, ReviewViewName, checkoutData, false);
            });
        }


        public ActionResult OrderReview_EditProfile()
        {
            Session[MagicStringEliminator.SessionKeys.RedirectTo] =
                new
                {
                    controller = MagicStringEliminator.Routes.Checkout_Review.Controller,
                    action = MagicStringEliminator.Routes.Checkout_Review.Action
                };
            return RedirectToAction(MagicStringEliminator.Routes.MyAccount.Action,
                                    MagicStringEliminator.Routes.MyAccount.Controller);
        }



        public ActionResult Receipt(Client client)
        {
            return TryOnEvent(() =>
            {
                ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                if (checkoutData == null)
                {
                    return RedirectToAction(MagicStringEliminator.CheckoutActions.Cart);
                }

                SingleUseDiscountCodes singleUseDiscountCodeUsed = null;


                if (client.Cart.DiscountApplied != null)
                {
                    var discountCode = service.DiscountCodeService.GetDiscountCode(client.Cart.DiscountApplied.Id);
                    if (discountCode.SingleUseDiscountCodes.Any())
                    {
                        singleUseDiscountCodeUsed = discountCode.SingleUseDiscountCodes.FirstOrDefault();
                    }
                }

                if (singleUseDiscountCodeUsed != null)
                {
                    service.DiscountCodeService.MarkSingleUseDiscountCodeAsUsed(singleUseDiscountCodeUsed, client.Cart.DiscountApplied, SessionManager.CurrentUser.Username);
                }

                var cartCopy = (Cart)client.Cart.Clone();
                client.Cart.Clear();
                checkoutData.RegisterClient(client);
                checkoutData.MapCart(cartCopy, false);
                Session.Remove("CheckoutData");
                Session.Remove("PaymentInfo");
                checkoutData.IsDifferenceMediaPage = IsDM;

                if (checkoutData.BillingAddress == null && SessionManager.CurrentUser != null)
                {
                    checkoutData.BillingAddress = SessionManager.CurrentUser.Profile.PrimaryAddress;
                }

                return View("Receipt", checkoutData);
            });
        }



        #region Private Methods

        private void SetupChritsmasRushDeliveryMessages()
        {
            var message =
                "<div style='color:red;background-color:yellow; font-weight:bold;'><h4>***ATTENTION***:</h4>For estimated delivery date by December 24th, last day to select Rush Shipping is December 18 before Noon CST. Thank you.</div>";
            var expiration = new DateTime(2013, 12, 22, 23, 59, 59);
            var cookieName = "JHM_ChritsmasRushDeliveryMessage3";
            SetupCustomPopupMessage(expiration, message, cookieName);
        }

        private void SetupCustomPopupMessage(DateTime messageExpiration, string message, string cookieName)
        {
            if (DateTime.Now > messageExpiration)
            {
                return;
            }
            TempData[MagicStringEliminator.Messages.Status] = message;
            if (Request == null)
            {
                return;
            }
            var cookie = Request.Cookies[cookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(cookieName, "true");
                cookie.Expires = messageExpiration;
                Response.Cookies.Add(cookie);
            }
            else
            {
                if (cookie.Value == "false")
                {
                    return;
                }
            }

            var popupMessage = new PopupMessageViewModel();
            popupMessage.Title = "Important Message";
            popupMessage.Message = message;
            popupMessage.Buttons.Add(new HtmlAnchor { InnerText = "Ok", HRef = "$(this).dialog('close');" });
            popupMessage.Buttons.Add(new HtmlAnchor
                {
                    InnerText = "Don\\'t show this message again",
                    HRef =
                        "updateCookie('" + cookieName + "','false',20);$(this).dialog('close');"
                });
            AddPopupMessage(popupMessage);
        }

        #endregion


        protected void ReloadUserAddresses(User user, CheckoutOrderReviewViewModel model, string donorStudioEnvironment)
        {
            if (user.AccountNumber != 0 && model.ShouldReloadAddressList)
            {
                model.AddressList = service.AccountService.GetUserAddresses(user.Username, user.ApplicationName, donorStudioEnvironment);

                // If user has only one address that one will be taken as the Primary address
                var primaryAddress = model.AddressList.Count() == 1
                                         ? model.AddressList.First()
                                         : model.AddressList.FirstOrDefault(x => x.DsIsPrimary);

                // If no primary address was found then select the first one as primary
                if (primaryAddress == null)
                {
                    primaryAddress = model.AddressList.First();
                }

                model.BillingAddressId = primaryAddress.DSAddressId;

                model.ShouldReloadAddressList = false;
                Session["CheckoutData"] = model;
            }
        }


        protected override void HandleUnknownAction(string actionName)
        {
            Response.Redirect("~/Checkout/Cart");
        }

    }
}