﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Foundation;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using HelperExtensions = Jhm.Common.Framework.Extensions.HelperExtensions;
using MagicStringEliminator = Jhm.Web.Core.MagicStringEliminator;
using StringExtensions = Jhm.Common.Framework.Extensions.StringExtensions;

namespace Jhm.Web.UI.Controllers
{
    public partial class CheckoutController
    {
        public ViewResult Cart(Client client, string errorMessage = null, string catalogReturnToUrl = null,
                               string donationsReturnToUrl = null)
        {
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            

            return (ViewResult)TryOnEvent(() =>
            {
                if (!IsDM)
                {
                    LoadPromotionalItemsToClient(client);
                    client.Cart.Refresh();
                }
                User user;
                if (Request.IsAuthenticated)
                {
                    user = SessionManager.CurrentUser;
                }
                else
                {
                    user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                }
                if (user != null && !user.IsTransient() && !IsDM)
                {
                    var dsEnviroment = DetermineDSEnvironment(user, client);
                    //check to see if user is a salt partner
                    AddDiscountIfUserIsSalt(client, dsEnviroment, user);
                }
                if (errorMessage.IsNotNullOrEmpty())
                    ViewData.ModelState.AddModelError("", errorMessage);

                return MapCartRenderView(client, CartViewName, new CheckoutShoppingCartViewModel(catalogReturnToUrl, donationsReturnToUrl), false);
            });
        }



        private void LoadPromotionalItemsToClient(Client client)
        {
            client.Cart.AvailablePromotionalItems = service.PromotionalItemService.GetAllActive().Select(MapPromotionalItemToModel).ToList();
        }

        private void LoadDiscountItemsToClient(Client client, DiscountCode discountCode)
        {
            client.Cart.AvailableDiscountItems = service.DiscountItemService.GetItemsRelatedToDiscountCode(discountCode).Select(x => MapDiscountItemToModel(client, service, x)).ToList();
        }

        private static PromotionalItemViewModel MapPromotionalItemToModel(PromotionalItem item)
        {
            var product = new Product(item.ProductCode);
            product.SetTitle(item.ProductTitle);
            product.SetDescription(item.ProductDescription);
            return new PromotionalItemViewModel
            {
                Product = product,
                Quantity = item.Quantity,
                DonationCode = item.DonationCode,
                MinimumPurchaseAmount = item.MinimumPurchaseAmount,
                MinimumDonationAmount = item.MinimumDonationAmount
            };
        }

        private static DiscountItemViewModel MapDiscountItemToModel(Client client, IServiceCollection service, DiscountCodeItems item)
        {
            var product = new Product(item.ProductCode);
            var dsProd = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, item.ProductCode);
            product.SetTitle(dsProd.Title);
            product.SetDescription(dsProd.Description);


            return new DiscountItemViewModel
            {
                Product = product,
                Quantity = item.Quantity,
                MinimumPurchaseAmount = item.MinPurchaseAmount,
                ProductWarehouse = item.ProductWarehouse,
            };
        }

        public void AddDiscountIfUserIsSalt(Client client, string dsEnviroment, Core.Models.User user)
        {

            var isUserASaltPartner = service.AccountService.IsUserASaltPartner(client, user, dsEnviroment);
            if (isUserASaltPartner)
            {
                //if truth assign discount code to codeName
                const string codeName = "83ABFC98-6276-4833-9317-2B6C2C1F5C54";
                //get discount value and info from db
                /*var discount = service.CartService.GetDiscountByCodeName(codeName);*/
                string faultCode = "";
                //apply discount to products on cart
                service.DiscountCodeService.ApplyDiscountCode(
                    service.DiscountCodeService.ValidateDiscountCode(codeName, client, out faultCode),
                    client, faultCode);

                if (faultCode.IsNotNullOrEmpty())
                {
                    ViewData.ModelState.AddModelError("DiscountCode", FormatHelper.ReplaceCurrencyFormat(faultCode, client));
                    ViewData.Add("DiscountCode", codeName);
                }
            }

        }

        

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = "PopupActionBottonClicked", MatchFormValue = "Logon")]
        public ActionResult ShoppingCart_Logon(Client client, CheckoutShoppingCartViewModel model)
        {
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
            return TryOnEventWithoutTransaction(() =>
            {
                if (String.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("UserName", "Username is required");
                }

                if (String.IsNullOrEmpty(model.Password))
                {
                    ModelState.AddModelError("Password", "Password is required");
                }
                if (model.UserName != null) model.UserName = model.UserName.Trim();
                if (model.UserName != null && model.UserName.Contains(" "))
                {
                    ModelState.AddModelError("UserName",
                                            "Username cannot contain spaces. Please remove the spaces and try again");
                }
                if (ModelState.IsValid)
                {
                    var result = new ApiMessage();

                    result = service.MembershipService.ValidateSecurityUser(model.UserName, model.Password);

                    if (result.IsSuccessful)
                    {
                        model.GlobalUserId = service.SecurityService.LoadUserByUsername(model.UserName).Data.Id;
                        ActionResult action = new EmptyResult();
                        TryWithTransaction(() =>
                        {
                            action = DoSecurityUserLogon(model, client);
                        });

                        if (action != null)
                        {
                            return action;
                        }

                        return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
                    }
                    else
                    {
                        ModelState.AddModelError("",
                                                "The user name or password provided is incorrect.");
                    }
                }

                ViewData["ShowLoginScript"] = ViewData["ShowLoginScript"] + "$('#dialogModal').dialog('open');";
                return MapCartRenderView(client, CartViewName, model, false);
            });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = "PopupActionBottonClicked", MatchFormValue = "Register")]
        public ActionResult ShoppingCart_Register(Client client, CheckoutShoppingCartViewModel model)
        {
            Session[MagicStringEliminator.SessionKeys.RedirectTo] = new
            {
                controller = MagicStringEliminator.Routes.Checkout_Review.Controller,
                action = MagicStringEliminator.Routes.Checkout_Review.Action
            };
            return TryOnEvent(() => Redirect("~/Account/Register"));
        }

        
        //TODO: NEED TO WRITE VALID SPEC FOR THIS - WAITING FOR APPROPRIATE SERVICE METHOD DEFINITION
        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.AddOneTimeDonation)]
        public ActionResult ShoppingCart_AddOneTimeDonation(Client client, FormCollection formCollection, string returnToUrl = null)
        {
            return TryOnEvent(() =>
            {
                var donationAmount = HelperExtensions.TryConvertTo<decimal>(formCollection["OneTimeDonation"]);
                if (donationAmount > 0)
                {
                    //TODO - this need to be refactored to get the onetime donation 'donation' out of the DB and then use it to set the values and return a DonationCartItem
                    try
                    {
                        var donationCartItem = new DonationCartItem();
                        donationCartItem.SetDonationFrequency(DonationFrequency.OneTime);
                        donationCartItem.SetPrice(donationAmount);
                        donationCartItem.SetCode("One_Time");
                        donationCartItem.SetDescription("One-Time donation");
                        donationCartItem.SetTitle("One-Time donation");
                        donationCartItem.SetProjectCode("JHM");
                        //var cart = service.CartService.GetCart(User);
                        client.Cart.AddItem(donationCartItem);
                    }
                    catch (DonationCartItemExistsException ex)
                    {
                        ViewData.ModelState.AddModelError("OneTimeDonation", ex.Message);
                    }
                    //service.CartService.SaveCart(cart);
                }
                else
                {
                    ViewData.ModelState.AddModelError("OneTimeDonation", StringExtensions.FormatWith("Amount must be greater than {0}.", 0.ToString("C", Thread.CurrentThread.CurrentCulture)));
                    ViewData.Add("OneTimeDonation", 0.ToString("00.00"));
                }

                return MapCartRenderView(client, CartViewName, new CheckoutShoppingCartViewModel(returnToUrl), false);
            });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.ApplyDiscountCode)]
        public ActionResult ShoppingCart_ApplyDiscountCode(Client client, FormCollection formCollection,
                                                           string returnToUrl = null)
        {
            //SetAnonymousCheckoutScript();
            return TryOnEvent(() =>
            {
                var codeName = formCollection["DiscountCode"];
                
                if (!String.IsNullOrEmpty(codeName))
                {
                    var faultCode = "";
                    var discountCodeService = service.DiscountCodeService;
                    DiscountCode discountCode = discountCodeService.ValidateDiscountCode(codeName, client, out faultCode);
                    discountCodeService.ApplyDiscountCode(discountCode, client, faultCode);                   
                }
                return MapCartRenderView(client, CartViewName, new CheckoutShoppingCartViewModel(returnToUrl), false);
            });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.ClearDiscount)]
        public ActionResult ShoppingCart_ClearDiscount(Client client, FormCollection formCollection,
                                                       string returnToUrl = null)
        {
            return TryOnEvent(() =>
            {
                client.Cart.ClearDiscount();


                return MapCartRenderView(client, CartViewName,
                                         new CheckoutShoppingCartViewModel(returnToUrl), false);
            });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.UpdateCart)]
        public ViewResult ShoppingCart_UpdateCart(Client client, FormCollection formCollection,
                                                  string returnToUrl = null)
        {
            return (ViewResult)TryOnEvent(() =>
            {
                string invalidField;
                if (!QuantityForItemsInListIsValid(formCollection, out invalidField))
                {
                    ViewData.ModelState.AddModelError(invalidField, String.Format("Quantity value '{0}' was invalid.", formCollection[invalidField]));
                }
                else if (client.Cart.Items.Any())
                {
                    RemoveItemsFromList(formCollection, client);
                    UpdateQuantityForItemsInList(formCollection, client);

                    // Added by Alex 2/26/2013
                    service.DiscountCodeService.RecalculateAppliedDiscount(client);
                    //service.CartService.SaveCart(cart);
                }
                return MapCartRenderView(client, CartViewName, new CheckoutShoppingCartViewModel(returnToUrl), false);
            });
        }


        #region Private Methods

        private void UpdateQuantityForItemsInList(FormCollection formCollection, Client client)
        {
            foreach (var key in formCollection.AllKeys.Where(x => x.Contains("Quantity")))
            {
                CartLineItem cartLineItem =
                    MiscExtensions.ToLIST(client.Cart.Items).Find(x => x.Item.Id.ToString() == key.Replace("-Quantity", ""));

                if (cartLineItem != null)
                    client.Cart.ChangeQuantity(cartLineItem.Item, HelperExtensions.ConvertTo<int>(formCollection[key]));
            }
        }

        private bool QuantityForItemsInListIsValid(FormCollection formCollection, out string invalidField)
        {
            invalidField = null;
            foreach (var key in formCollection.AllKeys.Where(x => x.Contains("Quantity")))
            {
                int q;
                if (!Int32.TryParse(formCollection[key], out q))
                {
                    invalidField = key;
                    return false;
                }
            }
            return true;
        }

        private void RemoveItemsFromList(FormCollection formCollection, Client client)
        {
            foreach (var key in formCollection.AllKeys.Where(x => x.Contains("SelectedCheckBoxName")))
            {
                if (formCollection[key] == "false") continue;

                CartLineItem cartLineItem =
                    MiscExtensions.ToLIST(client.Cart.Items).Find(x => x.Item.Id.ToString() == key.Replace("-SelectedCheckBoxName", ""));

                if (cartLineItem != null)
                    client.Cart.RemoveItem(cartLineItem.Item);
            }
        }



        #endregion

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.Checkout)]
        public ActionResult ShoppingCart_Checkout(FormCollection formCollection)
        {
            return TryOnEvent(() => RedirectToAction(MagicStringEliminator.CheckoutActions.Shipping));
        }
    }
}