﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using getv.donorstudio.core.Global;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.UI.Controllers
{
    public partial class CheckoutController
    {
        public ActionResult Shipping(Client client)
        {
            SetupChritsmasRushDeliveryMessages();
            return TryOnEvent(() =>
            {
                // Redirect to Cart view if user is not authenticated or cart is empty
                if (!IsAuthenticated || client.Cart == null || client.Cart.Items.Count() == 0)
                {
                    ViewData["RedirectToShoppingCartScript"] = MagicStringEliminator.Routes.Checkout_ShoppingCart.Route;
                    //ViewData["ShowLoginScript"] = ViewData["ShowLoginScript"] + "$('#dialogModal').dialog('open');";

                    return MapCartRenderView(client, PaymentViewName, new CheckoutOrderReviewViewModel());
                }

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData;
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                //if (user.AccountNumber == 0)
                //{
                //    Session[MagicStringEliminator.SessionKeys.RedirectTo] = new
                //    {
                //        controller = MagicStringEliminator.Routes.Checkout_Payment.Controller,
                //        action = MagicStringEliminator.Routes.Checkout_Payment.Action
                //    };
                //    ViewData["RedirectToShoppingCartScript"] =
                //        MagicStringEliminator.Routes.MyAccount.Route;
                //    return MapCartRenderView(client, PaymentViewName, new CheckoutOrderReviewViewModel());
                //}

                var dsEnviroment = DetermineDSEnvironment(user, client);

                //SetupCustomPopupMessage(client);
                if (user.AccountNumber > 0)
                {
                    user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name, GetApplicationName(), dsEnviroment);

                    //AddDiscountIfAutoApply(client);
                    //check to see if user is a salt partner
                    if (!IsDM)
                    {
                        service.DiscountCodeService.ApplyAutoApplyCodes(client);
                        if (!String.IsNullOrEmpty(client.Cart.DiscountErrorMessage))
                        {
                            ViewData.ModelState.AddModelError("DiscountCode", FormatHelper.ReplaceCurrencyFormat(client.Cart.DiscountErrorMessage, client));                            
                        }
                        AddDiscountIfUserIsSalt(client, dsEnviroment, user);
                    }
                }


                if (model == null)
                {
                    // Creates a new model
                    model = CheckoutOrderReviewViewModel.InitializeWith(user, client);
                }
                else
                {
                    // updates model
                    model.InitializeUsing(user, client);
                }

                //Edit 7/14/15 Jon Boyles - Quick fix to knock out shipping costs for JHM Cruise T-Shirts
                bool isShippingNA = !model.Cart.Items.Any(x => !x.Item.Code.ToLower().Contains("k596ta"));

                if (!client.Cart.HasShippableItems() || isShippingNA)
                {
                    model.ShippingMethod = ShippingMethod.TryGetFromValue<ShippingMethod>(ShippingMethod.NotApplicable.Value);
                    client.Cart.SetShippingMethod(model.ShippingMethod);
                    model.ShippingAddress = user.Profile.PrimaryAddress;
                    model.ShippingCost = client.Cart.ShippingMethod.Price;
                    model.OrderTotal = client.Cart.Total();
                    Session["CheckoutData"] = model;
                    return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
                }


                ReloadUserAddresses(user, model, dsEnviroment);

                var jsonAddressesList = HelperExtensions.ToJSON(model.AddressList);
                ViewData["jsonAddressesList"] = jsonAddressesList;

                var jsonNoPOBoxMethods = HelperExtensions.ToJSON(model.ShippingMethods.Where(m => m.ShouldAllowPOBoxes == false).Select(m => new { Id = m.Value }));
                ViewData["jsonNoPOBoxMethods"] = jsonNoPOBoxMethods;

                return MapCartRenderView(client, ShippingViewName, model, false);
            });
        }


        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.Shipping)]
        public ActionResult Shipping(Client client, CheckoutOrderReviewViewModel model, FormCollection formCollection)
        {
            return TryOnEvent(() =>
            {
                var isShippingRequired = true;
                var hasItemsWithShippingCost = client.Cart.HasItemsWithShippingCost();
                if (!hasItemsWithShippingCost)
                {
                    formCollection["ShippingMethodValue"] = ShippingMethod.NotApplicable.Value;
                    isShippingRequired = false;
                }

                if (!client.Cart.HasShippableItems())
                {
                    ModelState.Remove("ShippingAddressId");
                }

                if (!String.IsNullOrEmpty(formCollection["ShippingMethodValue"]))
                {
                    model.ShippingMethod = ShippingMethod.TryGetFromValue<ShippingMethod>(formCollection["ShippingMethodValue"]);
                    ModelState.Remove("ShippingMethodValue");
                    ModelState.Remove("ShippingMethod");
                }

                // Validating invalid usage of the FREE shipping method
                if (
                    hasItemsWithShippingCost &&
                    model.ShippingMethod != null &&
                    model.ShippingMethod.Price == 0 &&
                    model.ShippingMethod.DSValue.Contains("FREE")
                    // This extra condition is needed since when using a coupon with free standard shipping the price of STANDAR shipping gets set to 0.00
                    )
                {
                    ModelState.AddModelError("ShippingMethodValue", "The shipping method field is required.");
                }

                if (ModelState.IsValid)
                {
                    var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                    if (checkoutData == null)
                    {
                        var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                        model.InitializeUsing(user, client);
                    }
                    

                    IEnumerable<Country> countries;
                    Country country;

                    if (client.Cart.HasShippableItems() || model.ContainsSubscriptionItems())
                    {
                        // Validating shipping method for selected country
                        var shippingAddress = service.AccountService.GetUserAddressById(model.ShippingAddressId, client.Company.DonorStudioEnvironment);
                        model.ShippingAddress = shippingAddress;
                        model.ShippingAddressId = shippingAddress.DSAddressId;
                        
                        countries = Country.GetAll<Country>().Where(x => x.Iso.ToLower() == shippingAddress.Country.ToLower()).ToList();

                        if (countries.Count() != 1)
                        {
                            ModelState.AddModelError("ShippingAddressId","It looks like there is a problem with the country in your shipping address");
                            return CreateOrderPaymentView(client, model, PaymentViewName);
                        }

                        country = countries.First();
                        model.ShippingAddressCountry = country.DisplayName;

                        if (isShippingRequired && !Equals(country, client.Company.DefaultCountry) &&
                            (!Equals(model.ShippingMethod, ShippingMethod.USPSInternational) &&
                             !Equals(model.ShippingMethod, ShippingMethod.InternationalCanada) &&
                             !Equals(model.ShippingMethod, ShippingMethod.UKInternational)))
                        {
                            ModelState.AddModelError("ShippingMethodValue",
                                                     StringExtensions.FormatWith("International Shipping is required for orders outside {0}.", client.Company.DefaultCountry.Name));
                            return CreateOrderPaymentView(client, model, PaymentViewName);
                        }

                        // Getting own subscription
                        var subscriptionItem = client.Cart.Items.SingleOrDefault(x =>
                              x.Item is SubscriptionCartItem && !((SubscriptionCartItem)x.Item).UserSubscription.IsGiftSubcription
                          );

                        // Updating Delivery Address on Own subscription with selected one
                        if (subscriptionItem != null)
                        {
                            var subscription = ((SubscriptionCartItem)subscriptionItem.Item).UserSubscription;
                            subscription.DeliveryAddress = shippingAddress;
                            subscriptionItem.Item.SetDescription(String.Format(
                              "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}<br/><b>Number of Issues: </b>{6} Issues ({7})",
                              subscription.User.Profile.FirstName,
                              subscription.User.Profile.LastName,
                              shippingAddress.Address1,
                              shippingAddress.City,
                              shippingAddress.StateCode,
                              shippingAddress.PostalCode,
                              subscription.NumberOfIssues,
                              subscription.PriceCodeDescription
                          ));
                        }
                    }

                    client.Cart.SetShippingMethod(model.ShippingMethod);
                    model.ShippingCost = client.Cart.ShippingMethod.Price;
                    model.OrderTotal = client.Cart.Total();

                    if (checkoutData != null)
                    {
                        checkoutData.ShippingAddress = model.ShippingAddress;
                        checkoutData.ShippingAddressId = model.ShippingAddressId;
                        checkoutData.ShippingAddressCountry = model.ShippingAddressCountry;
                        checkoutData.ShippingCost = model.ShippingCost;
                        checkoutData.OrderTotal = model.OrderTotal;
                        checkoutData.ShippingFirstName = model.ShippingFirstName;
                        checkoutData.ShippingLastName = model.ShippingLastName;
                        checkoutData.ShippingCompany = model.ShippingCompany;
                        checkoutData.ShippingMethod = model.ShippingMethod;
                        
                        model = checkoutData;
                    }

                    Session["CheckoutData"] = model;
                    return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
                }
                return CreateOrderPaymentView(client, model, ShippingViewName);
            });
        }
    }
}