﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Jhm.CommerceIntegration;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.em3.Core;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Controllers
{
    public partial class CheckoutController
    {
        public ViewResult Payment(Client client)
        {
            SetupChritsmasRushDeliveryMessages();
            return (ViewResult)TryOnEvent(() =>
            {
                // Redirect to Cart view if user is not authenticated or cart is empty
                if (!IsAuthenticated || client.Cart == null || !client.Cart.Items.Any())
                {
                    ViewData["RedirectToShoppingCartScript"] = MagicStringEliminator.Routes.Checkout_ShoppingCart.Route;
                    //ViewData["ShowLoginScript"] = ViewData["ShowLoginScript"] + "$('#dialogModal').dialog('open');";
                    return MapCartRenderView(client, PaymentViewName, new CheckoutOrderReviewViewModel(), false);
                }

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                if (checkoutData == null)
                {
                    ViewData["RedirectToShoppingCartScript"] =
                        MagicStringEliminator.Routes.Checkout_ShoppingCart.Route;
                    checkoutData = new CheckoutOrderReviewViewModel();
                    // No need to do heavy mapping here since we are redirecting to shopping cart which does heavy mapping
                    return MapCartRenderView(client, ReviewViewName, checkoutData, true);
                }

                var model = checkoutData;
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);

                // THIS SHOULD BE HANDLED NOW IN THE SHIPPING ACTION
                //if (user.AccountNumber > 0)
                //{
                //    var dsEnviroment = DetermineDSEnvironment(user, client);

                //    user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name, GetApplicationName(), client.Company.DonorStudioEnvironment);

                //    //AddDiscountIfAutoApply(client);
                //    //check to see if user is a salt partner
                //    if (!IsDM)
                //    {
                //        AddDiscountIfAutoApply(client);
                //        AddDiscountIfUserIsSalt(client, dsEnviroment, user);
                //    }
                //}

                // updates model
                model.InitializeUsing(user, client);

                var dsEnviroment = DetermineDSEnvironment(user, client);
                ReloadUserAddresses(user, model, dsEnviroment);

                var jsonAddressesList = HelperExtensions.ToJSON(model.AddressList);
                ViewData["jsonAddressesList"] = jsonAddressesList;

                var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? model.PaymentList;
                paymentInfo.OrderTotal = model.OrderTotal;
                model.PaymentList = paymentInfo;
                Session["PaymentInfo"] = paymentInfo;

                
                return MapCartRenderView(client, PaymentViewName, model, false);
            });
        }

        




        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = "PopupActionBottonClicked",
            MatchFormValue = MagicStringEliminator.CheckoutActions.AddAddress)]
        public ActionResult Payment_AddAddress(Client client, FormCollection formCollection, string returnToUrl = null)
        {
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var addressValues = formCollection;
                var newAddress = new Address(
                    addressValues["Address1"],
                    addressValues["Address2"],
                    addressValues["City"],
                    addressValues["StateCode"],
                    addressValues["PostalCode"],
                    addressValues["Country"]);
                if (String.IsNullOrEmpty(newAddress.Address1))
                {
                    ModelState.AddModelError("Address1", "Street address is required");
                }
                if (string.IsNullOrEmpty(newAddress.City))
                {
                    ModelState.AddModelError("City", "City is required");
                }
                if (string.IsNullOrEmpty(newAddress.Country))
                {
                    ModelState.AddModelError("Country", "Country is required");
                }
                if (string.IsNullOrEmpty(newAddress.StateCode))
                {
                    ModelState.AddModelError("StateCode", "State or Province is required");
                }

                var countries = Country.GetAll<Country>().ToList();
                var country = countries.Find(x => x.Iso == newAddress.Country);
                if (country == null)
                {
                    ModelState.AddModelError("Country", "Country not found");
                }
                else if (!Core.CountryListHelper.CountriesWithoutZipCodes.Contains(country.Iso3) &&
                         String.IsNullOrEmpty(newAddress.PostalCode))
                {
                    ModelState.AddModelError("PostalCode",
                                             StringExtensions.FormatWith("Postal code is required in {0}", country.DisplayName));
                }

                if (ModelState.IsValid)
                {
                    long dsAddressId = 0;
                    if (user.AccountNumber == 0)
                    {
                        var securityUser = service.SecurityService.LoadUserById(Guid.Parse(user.GlobalId));
                        user.Profile.FirstName = securityUser.Data.FirstName;
                        user.Profile.LastName = securityUser.Data.LastName;
                        user.Email = securityUser.Data.EmailAddress;
                        user.Username = securityUser.Data.Username;
                        user.GlobalId = securityUser.Data.Id;

                        if (user.Profile.PrimaryAddress == null || String.IsNullOrEmpty(user.Profile.PrimaryAddress.Address1))
                        {
                            newAddress.DsIsPrimary = true;
                            user.Profile.PrimaryAddress = newAddress;
                        }
                        CheckAndSetDSEnvironment(user, client);
                        dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(user.DsEnvironment, user.AccountNumber).AddressId;
                    }
                    else
                    {
                        dsAddressId = service.AccountService.AddAddressToUser(user.AccountNumber, newAddress, user.DsEnvironment);
                    }
                    //SetupCustomPopupMessage(client);
                    newAddress.DSAddressId = dsAddressId;
                }
                else
                {
                    ViewData["ShowNewAddressPopupScript"] = "$('#dialogModal').dialog('open');";
                }

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData ?? CheckoutOrderReviewViewModel.InitializeWith(user, client, returnToUrl);

                // if Shipping address has not been set use the new one as default
                if (model.ShippingAddressId <= 0)
                {
                    model.ShippingAddress = newAddress;
                    model.ShippingAddressId = newAddress.DSAddressId;
                    // Updating Delivery Address on Own subscription
                    if (model.ContainsSubscriptionItems())
                    {
                        var subscriptionItem = client.Cart.Items.SingleOrDefault(x => x.Item is SubscriptionCartItem &&
                                                                                 !((SubscriptionCartItem) x.Item)
                                                                                      .UserSubscription
                                                                                      .IsGiftSubcription
                            );
                        if (subscriptionItem != null)
                        {
                            var subscription = ((SubscriptionCartItem) subscriptionItem.Item).UserSubscription;
                            subscription.DeliveryAddress = newAddress;
                            subscriptionItem.Item.SetDescription(String.Format(
                                "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}<br/><b>Number of Issues: </b>{6} Issues ({7})",
                                subscription.User.Profile.FirstName,
                                subscription.User.Profile.LastName,
                                newAddress.Address1,
                                newAddress.City,
                                newAddress.StateCode,
                                newAddress.PostalCode,
                                subscription.NumberOfIssues,
                                subscription.PriceCodeDescription
                                                                     ));
                        }
                    }
                }
                model.ShouldReloadAddressList = true;
				model.PaymentList.AddressList = model.AddressList;
                Session["CheckoutData"] = model;
                var viewName = returnToUrl;
                return CreateOrderPaymentView(client, model, viewName);
            });
        }

        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = "Payment_Cancel")]
        public ActionResult PaymentCancelled()
        {
            return RedirectToAction(MagicStringEliminator.CheckoutActions.Cart);
        }


        [HttpPost]
        [FormKeyValueMatch(MatchFormKey = FormKeyValueMatchAttributes.ActionBottonClicked,
            MatchFormValue = MagicStringEliminator.CheckoutActions.Review)]
        public ActionResult Payment(Client client, CheckoutOrderReviewViewModel model, FormCollection formCollection)
        {
            return TryOnEvent(() =>
            {
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                if (checkoutData == null)
                {
                    ViewData["RedirectToShoppingCartScript"] =
                        MagicStringEliminator.Routes.Checkout_ShoppingCart.Route;
                    checkoutData = new CheckoutOrderReviewViewModel();
                    // No need to do heavy mapping here since we are redirecting to shopping cart which does heavy mapping
                    return MapCartRenderView(client, ReviewViewName, checkoutData);
                }


                // Populating fields from previous model
                model.ShippingAddress = checkoutData.ShippingAddress;
                model.ShippingMethodValue = checkoutData.ShippingMethodValue;
                model.ShippingMethod = checkoutData.ShippingMethod;
                model.ShippingFirstName = checkoutData.ShippingFirstName;
                model.ShippingLastName = checkoutData.ShippingLastName;
                model.ShippingCost = checkoutData.ShippingCost;
                model.ShippingMethod = checkoutData.ShippingMethod;
                ModelState.Remove("ShippingMethodValue");
                ModelState.Remove("ShippingMethod");
                ModelState.Remove("ShippingFirstName");
                ModelState.Remove("ShippingLastName");


                var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? new PaymentListViewModel();
                paymentInfo.RegisterClient(client);
                model.PaymentList = paymentInfo;
                Session["PaymentInfo"] = paymentInfo;
                

                // Ignore missing credit card fields if order has no cost or a previously saved credit card was selected
                if (model.PaymentList.Balance > 0)
                {
                    ModelState.AddModelError(String.Empty, client.Company.FormatWithCulture("There is a remaining balance of {0:C}. Please add a payment method to pay off this balance.",model.PaymentList.Balance));
                }
                else if (model.PaymentList.Balance < 0)
                {
                    ModelState.AddModelError(String.Empty, client.Company.FormatWithCulture("There is a negative balance of {0:C}. Please remove a payment method to avoid overcharging.",model.PaymentList.Balance));
                }


                //string reason;
                //if (client.Cart.HasItemsWithCost() && model.IsUsingNewCreditCard && !CreditCard.IsValid(model, out reason))
                //{
                //    ModelState.AddModelError("CreditCardNumber", reason);
                //    ViewData["ShouldShowCreditCardForm"] = true;
                //}

                //if (model.IsUsingNewCreditCard && model.CreditCardNumber != null && model.CreditCardNumber.Equals("4444333322221111") &&
                //    !(HttpContext.Request.Url.Host.EndsWith("test.jhm.org") ||
                //      HttpContext.Request.Url.Host.StartsWith("localhost")))
                //{
                //    ModelState.AddModelError("CreditCardNumber",
                //                             "Credit card number has been denied");
                //    ViewData["ShouldShowCreditCardForm"] = true;
                //}

                if (ModelState.IsValid)
                {
                    var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                    model.InitializeUsing(user, client);
                    model.FirstName = user.Profile.FirstName;
                    model.LastName = user.Profile.LastName;
                    model.Email = user.Email;
                    model.Profile = user.Profile;
                    model.RegisterClient(client);

                    //model.ShippingAddressCountry = c.DisplayName;

                    var primaryAddress = user.Profile.PrimaryAddress;
                    IEnumerable<Country> countries = Country.GetAll<Country>().Where(x => x.Iso.ToLower() == primaryAddress.Country.ToLower()).ToList();

                    if (countries.Count() != 1)
                    {
                        ModelState.AddModelError(String.Empty,
                                                 "It looks like there is a problem with the country in your primary address");
                        return CreateOrderPaymentView(client, model, PaymentViewName);
                    }

                    Country country = countries.First();
                    //Note: if user has no primary address the Billing address will be set to null
                    model.BillingAddress = primaryAddress;
                    model.BillingAddressCountry = country.DisplayName;

                    model.OrderTotal = client.Cart.Total();

                    Session["CheckoutData"] = model;
                    return RedirectToAction(MagicStringEliminator.CheckoutActions.Review);
                }
                return CreateOrderPaymentView(client, model, PaymentViewName);
            });
        }


        private ActionResult CreateOrderPaymentView(Client client, CheckoutOrderReviewViewModel model, string viewName)
        {
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                model.InitializeUsing(user, client);
                if (user.AccountNumber > 0)
                {
                    model.AddressList = service.AccountService.GetUserAddresses(user.Username,
                                                                                user
                                                                                    .ApplicationName,
                                                                                client.Company.
                                                                                       DonorStudioEnvironment);

                    // If user has only one address that one will be taken as the Primary address
                    var primaryAddress = model.AddressList.Count() == 1
                                             ? model.AddressList.First()
                                             : model.AddressList.FirstOrDefault(x => x.DsIsPrimary);

                    // If no primary address was found then select the first one as primary
                    if (primaryAddress == null && model.AddressList.Any())
                    {
                        primaryAddress = model.AddressList.First();
                    }

                    model.BillingAddressId = primaryAddress.DSAddressId;
                }
                var jsonAddressesList = HelperExtensions.ToJSON(model.AddressList);
                ViewData["jsonAddressesList"] = jsonAddressesList;

                var jsonNoPOBoxMethods =
                    HelperExtensions.ToJSON(model.ShippingMethods.Where(m => m.ShouldAllowPOBoxes == false).Select(
                            m => new { Id = m.Value }));
                ViewData["jsonNoPOBoxMethods"] = jsonNoPOBoxMethods;

				model.PaymentList.AddressList = model.AddressList;
                return MapCartRenderView(client, viewName, model, false);
            });
        }

        [HttpPost]
        public ActionResult Payment_AddCreditCard(CreditCardViewModel model)
        {
            return TryOnEvent(() =>
            {
                // Validating selected credit card
                if (!model.SelectedCreditCardId.IsNullOrEmpty() && !model.IsUsingNewCreditCard)
                {
                    var selectedCreditCard =
                        service.AccountService.GetSavedCreditCard(SessionManager.CurrentUser.Username,
                                                                  model.SelectedCreditCardId,
                                                                  SessionManager.CurrentUser.ApplicationName);
                    if (selectedCreditCard == null)
                    {
                        AddPopupMessage("The selected credit card was not found. Please select a credit card from the list or add a new credit card.","Credit Card Error");
                        return RedirectToAction(PaymentViewName);
                    }
                    model.SelectedCreditCard = selectedCreditCard;
                    model.ShouldSaveCreditCard = false;
                    model.CCExpirationMonth = selectedCreditCard.ExpirationMonth.ToString();
                    model.CCExpirationYear = selectedCreditCard.ExpirationYear.ToString();
                    model.CreditCardNumber = selectedCreditCard.EndingWith;
                    model.CreditCardType = selectedCreditCard.CardType.Code;
                }

                var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? new PaymentListViewModel();
                paymentInfo.AddCreditCard(model);
                Session["PaymentInfo"] = paymentInfo;


                return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
            });
        }

        [HttpPost]
        public ActionResult Payment_AddGiftCard(GiftCardViewModel model)
        {
            var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? new PaymentListViewModel();
            paymentInfo.AddGiftCardPayment(model);
            Session["PaymentInfo"] = paymentInfo;

            return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
        }

        public ActionResult Payment_Remove(Guid id)
        {
            var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? new PaymentListViewModel();
            paymentInfo.RemovePayment(id);
            Session["PaymentInfo"] = paymentInfo;

            return RedirectToAction(MagicStringEliminator.CheckoutActions.Payment);
        }

        [HttpPost]
        public JsonResult GetGiftCardBalance(string number, string pin)
        {
            var paymentInfo = Session["PaymentInfo"] as PaymentListViewModel ?? new PaymentListViewModel();
            if (paymentInfo.Payments.Any(x => x.IsGifCard() && x.GiftCard.Number == number))
            {
                return new JsonResult { Data = new { Success = false, Balance = 0, Response = "The gift card number entered is already being used in this order and cannot be added again. Please use a different gift card." } };
            }

            var balance = service.GiftCardService.GetBalance(number, pin);
            return new JsonResult{Data = new {Success = true, Balance = balance}};
        }

        [HttpPost]
        public ActionResult GetCreditCardToken(Client client, CreditCardViewModel model)
        {
            return TryOnEvent(() =>
            {
                if (model.IsUsingNewCreditCard)
                {
                    var selectedAddress = service.AccountService.GetUserAddressById(model.BillingAddressId, client.Company.DonorStudioEnvironment);
                    model.SelectedAddress = selectedAddress;

                    //string response;
                    //var isSuccessfull = GetCreditCardToken(model, SessionManager.CurrentUser, client.Company, out response);
                    //return new JsonResult { Data = new { Success = isSuccessfull, Response = response } };

                    var isSuccessfull = true;  
                    return new JsonResult { Data = new { Success = isSuccessfull, Response = "" } };
                }

                var selectedCreditCard = service.AccountService.GetSavedCreditCard(SessionManager.CurrentUser.Username,model.SelectedCreditCardId,SessionManager.CurrentUser.ApplicationName);
                if (selectedCreditCard == null)
                {
                    return new JsonResult { Data = new { Success = false, Response = "The selected credit card was not found. Please select a credit card from the list or add a new credit card." } };
                }
                return new JsonResult { Data = new { Success = true, Response = selectedCreditCard.Token } };
            });
        }

        private bool GetCreditCardToken(CreditCardViewModel creditCard, User user, Company company, out string result)
        {
            //TODO:  If IsTestCharge then supersede the Account and Transaction key with test info.


            var request = new PaymentRequest(
                company.CommerceAccountId,
                company.TransactionKey,
                false,
                user.Profile.FirstName,
                user.Profile.LastName,
                creditCard.SelectedAddress.Address1 + (String.IsNullOrEmpty(creditCard.SelectedAddress.Address2) ? String.Empty : " " + creditCard.SelectedAddress.Address2),
                creditCard.SelectedAddress.City,
                creditCard.SelectedAddress.StateCode,
                creditCard.SelectedAddress.PostalCode,
                creditCard.SelectedAddress.Country,
                "Zero dollars auth",
                creditCard.CreditCardNumber,
                String.Format("{0}{1}", creditCard.CCExpirationMonth, creditCard.TwoDigitsExpirationYear),
                creditCard.CID,
                // Doing 1.0 Pound Auth for UK company
                company == Company.JHM_UnitedKingdom ? 1 : 0,
                IsDM ? "DM" : company.SiteTag,
                company.Currency,
                BlueFinPaymentService.BluefinTransactionType.Auth
                );
            var response = service.CommerceService.BlueFinPaymentService.Process(request);

            result = response.IsAuthorized ? response.TransactionID : String.IsNullOrEmpty(response.AuthMessage) ? "Unknown reason": response.AuthMessage.Replace("+", " ");

            return response.IsAuthorized;
        }
    }
}