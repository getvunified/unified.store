﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using System.Threading;
using getv.donorstudio.core.eCommerce;
using Jhm.em3.Core;
using getv.donorstudio.core.Global;
using System.Text;
using System.Web.UI.WebControls;
using Jhm.Common.Utilities;
using System.Collections.Specialized;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.CommerceIntegration;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class CartController : BaseController<IServiceCollection>
    {
        const string CompanyCultureFormat = "{0}{1:C}";

        public CartController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
            ViewBag.CompanyCultureFormat = CompanyCultureFormat;
        }

        public ActionResult Index(Client client, int page = 1)
        {
            return TryOnEvent(() =>
            {
                return MapCartRenderView(client, "Index", new CartViewModel(), false);
            });
        }

        public JsonResult AddOneTimeDonation(Client client, string OneTimeDonationValue)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var success = true;
                var message = "";
                var oneTimeDonation = "";
                var totalSummary = "";
                var baseViewModel = new BaseViewModel();
                var donationAmount = HelperExtensions.TryConvertTo<decimal>(OneTimeDonationValue);
                if (donationAmount > 0)
                {
                    try
                    {
                        var donationCartItem = new DonationCartItem();
                        donationCartItem.SetDonationFrequency(DonationFrequency.OneTime);
                        donationCartItem.SetPrice(donationAmount);
                        donationCartItem.SetCode("One_Time");
                        donationCartItem.SetDescription("One-Time donation");
                        donationCartItem.SetTitle("One-Time donation");
                        donationCartItem.SetProjectCode("JHM");
                        client.Cart.AddItem(donationCartItem);
                    }
                    catch (DonationCartItemExistsException ex)
                    {
                        success = false;
                        message = ex.Message;
                    }

                    oneTimeDonation = baseViewModel.FormatWithCompanyCulture("{0}{1:C}", string.Empty, donationAmount);

                }
                else
                {
                    oneTimeDonation = baseViewModel.FormatWithCompanyCulture("{0}{1:C}", string.Empty, 0.00);
                    success = false;
                    message = "Amount must be greather than " + oneTimeDonation;
                }

                totalSummary = baseViewModel.FormatWithCompanyCulture("{0}{1:C}", string.Empty, client.Cart.SubTotal(true));

                return Json(
                    new
                    {
                        success = success,
                        message = message,
                        OneTimeDonation = oneTimeDonation,
                        TotalSummary = totalSummary
                    },
                    JsonRequestBehavior.AllowGet
                );
            });
        }

        public JsonResult ApplyDiscountCode(Client client, string PromoCode)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                if (!String.IsNullOrEmpty(PromoCode))
                {
                    var faultCode = "";
                    var discountCodeService = service.DiscountCodeService;
                    DiscountCode discountCode = discountCodeService.ValidateDiscountCode(PromoCode, client, out faultCode);
                    discountCodeService.ApplyDiscountCode(discountCode, client, faultCode);
                }
                var model = new CartViewModel();

                var response = new
                {
                    success = true,
                    discount = model.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, client.Cart.DiscountTotal),
                    total = model.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, client.Cart.SubTotal(true))
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        public ActionResult Checkout(Client client)
        {
            return (ViewResult)TryOnEvent(() =>
            {
                // Redirect to Cart view if  cart is empty
                if (client.Cart == null || client.Cart.Items.Count() == 0)
                {
                    return RedirectToAction("Index");
                }

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData;

                User user = null;

                if (model == null)
                {
                    model = new CheckoutOrderReviewViewModel();
                }

                if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);

                    var dsEnviroment = DetermineDSEnvironment(user, client);

                    if (user.AccountNumber > 0)
                    {
                        user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name, GetApplicationName(), dsEnviroment);
                        //check to see if user is a salt partner
                        if (!IsDM)
                        {
                            service.DiscountCodeService.ApplyAutoApplyCodes(client);
                            if (!String.IsNullOrEmpty(client.Cart.DiscountErrorMessage))
                            {
                                ViewData.ModelState.AddModelError("DiscountCode", FormatHelper.ReplaceCurrencyFormat(client.Cart.DiscountErrorMessage, client));
                            }
                            AddDiscountIfUserIsSalt(client, dsEnviroment, user);
                        }
                    }

                    if (string.IsNullOrWhiteSpace(model.FirstName))
                    {
                        model.InitializeUsing(user, client);
                    }

                    bool isShippingNA = !model.Cart.Items.Any(x => !x.Item.Code.ToLower().Contains("k596ta"));

                    if (!client.Cart.HasShippableItems() || isShippingNA)
                    {
                        model.ShippingMethod = ShippingMethod.TryGetFromValue<ShippingMethod>(ShippingMethod.NotApplicable.Value);
                        client.Cart.SetShippingMethod(model.ShippingMethod);
                        model.ShippingAddress = user.Profile.PrimaryAddress;
                        model.ShippingCost = client.Cart.ShippingMethod.Price;
                        model.OrderTotal = client.Cart.Total();
                        ViewBag.CurrentStep = 4;
                    }
                    else
                    {
                        if (model.ShippingAddressId > 0 && !string.IsNullOrWhiteSpace(model.ShippingMethodValue))
                        {
                            ViewBag.CurrentStep = 4;
                        }
                        else if (model.ShippingAddressId == 0)
                        {
                            ViewBag.CurrentStep = 3;
                        }
                        else if (string.IsNullOrWhiteSpace(model.ShippingMethodValue))
                        {
                            ViewBag.CurrentStep = 2;
                        }
                    }

                    model.ShouldReloadAddressList = true;
                    ReloadUserAddresses(user, model, dsEnviroment);

                    var jsonAddressesList = HelperExtensions.ToJSON(model.AddressList);
                    ViewData["jsonAddressesList"] = jsonAddressesList;

                    var jsonNoPOBoxMethods = HelperExtensions.ToJSON(model.ShippingMethods.Where(m => m.ShouldAllowPOBoxes == false).Select(m => new { Id = m.Value }));
                    ViewData["jsonNoPOBoxMethods"] = jsonNoPOBoxMethods;

                    model.PaymentList.AddressList = model.AddressList;
                }
                else
                {
                    ViewBag.CurrentStep = 1;
                }

                if (model.PaymentList.Payments.Count == 0)
                {
                    model.PaymentList.AddCreditCard(new CreditCardViewModel
                    {
                        IsUsingNewCreditCard = true,
                        AddressList = model.AddressList,
                        ShouldSaveCreditCard = false
                    });
                }

                Session["CheckoutData"] = model;

                return MapCartRenderView(client, "Checkout", model, false);
            });
        }

        public ActionResult UpdateCart(Client client, string code, string quantity, string mediaType)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                CartLineItem cartLineItem = null;
                if (client.Cart.Items.Any())
                {
                    var mType = MediaType.All.FirstOrDefault(x => x.CodeSuffix == mediaType);
                  
                    cartLineItem = MiscExtensions.ToLIST(client.Cart.Items).
                        Where(m => m.Item is StockItem).
                        FirstOrDefault(x => x.Item.ConvertTo<StockItem>().ProductCode == code && x.Item.ConvertTo<StockItem>().MediaType == mType);                

                    if (cartLineItem != null)
                    {
                        client.Cart.ChangeQuantity(cartLineItem.Item, HelperExtensions.ConvertTo<int>(quantity));
                    }
                }

                var tempModel = new CartViewModel();
                tempModel.RegisterClient(client);
                tempModel.MapCart(client.Cart, false);

                var price = (cartLineItem == null) ? 0.00M : cartLineItem.Item.Price * HelperExtensions.ConvertTo<int>(quantity);

                var response = new
                {
                    itemRemoved = true,
                    itemsInCart = tempModel.NumberOfItemsInCart,
                    itemTotal = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, price),
                    subTotal = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotal - tempModel.DonationstemsCartSubTotal),
                    total = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotalWithDiscount)
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [HttpGet]
        public ActionResult RemoveDonation(Client client, string code)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                CartLineItem cartLineItem =
                    MiscExtensions.ToLIST(client.Cart.Items).Where(m => m.Item is DonationCartItem).FirstOrDefault(x => x.Item.ConvertTo<DonationCartItem>().ProjectCode.ToString() == code);

                client.Cart.RemoveItem(cartLineItem.Item);

                var tempModel = new CartViewModel();
                tempModel.RegisterClient(client);
                tempModel.MapCart(client.Cart, false);

                var response = new
                {
                    success = true,
                    showDonationTable = tempModel.ContainsDonations(),
                    OneTimeDonation = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.DonationstemsCartSubTotal),
                    subTotal = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotal - tempModel.DonationstemsCartSubTotal),
                    total = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotalWithDiscount)
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [HttpGet]
        public ActionResult RemoveItem(Client client, string sku, string mediaType)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, sku);
                if (product.IsNull())
                {
                    return RedirectToAction("Index", "Cart");
                }

                var mType = MediaType.All.FirstOrDefault(x => x.CodeSuffix == mediaType);
                var cart = client.Cart;
                if (mType.IsNot().Null())
                {
                    var stockItem = product.StockItems.First(x => x.MediaType == mType);
                    if (stockItem.IsNot().Null())
                    {
                        cart.RemoveItem(stockItem);
                    }
                }

                // A temporary model is created to avoid calculating sub-total and total outside the MapCart method.
                var tempModel = new CartViewModel();
                tempModel.RegisterClient(client);
                tempModel.MapCart(cart, false);

                var response = new
                {
                    itemRemoved = true,
                    itemsInCart = tempModel.CartItems.Where(ci => ci.Item is StockItem).Count(),
                    subTotal = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotal - tempModel.DonationstemsCartSubTotal),
                    total = tempModel.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, tempModel.CartSubTotalWithDiscount)
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        private void AddDiscountIfUserIsSalt(Client client, string dsEnviroment, Core.Models.User user)
        {

            var isUserASaltPartner = service.AccountService.IsUserASaltPartner(client, user, dsEnviroment);
            if (isUserASaltPartner)
            {
                //if truth assign discount code to codeName
                const string codeName = "83ABFC98-6276-4833-9317-2B6C2C1F5C54";
                //get discount value and info from db
                string faultCode = "";
                //apply discount to products on cart
                service.DiscountCodeService.ApplyDiscountCode(
                    service.DiscountCodeService.ValidateDiscountCode(codeName, client, out faultCode),
                    client, faultCode);

                if (!string.IsNullOrWhiteSpace(faultCode))
                {
                    ViewData.ModelState.AddModelError("DiscountCode", FormatHelper.ReplaceCurrencyFormat(faultCode, client));
                    ViewData.Add("DiscountCode", codeName);
                }
            }
        }

        private void ReloadUserAddresses(User user, CheckoutOrderReviewViewModel model, string donorStudioEnvironment)
        {
            if (user.AccountNumber != 0 && model.ShouldReloadAddressList)
            {
                model.AddressList = service.AccountService.GetUserAddresses(user.Username, user.ApplicationName, donorStudioEnvironment);

                // If user has only one address that one will be taken as the Primary address
                var primaryAddress = model.AddressList.Count() == 1
                                         ? model.AddressList.First()
                                         : model.AddressList.FirstOrDefault(x => x.DsIsPrimary);

                // If no primary address was found then select the first one as primary
                if (primaryAddress == null)
                {
                    primaryAddress = model.AddressList.First();
                }

                model.BillingAddressId = primaryAddress.DSAddressId;

                model.PaymentList.AddressList = model.AddressList;

                model.ShouldReloadAddressList = false;
            }
        }

        [HttpPost]
        public ActionResult AddAddress(Client client, Address newAddress, string addressType)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var dsEnviroment = DetermineDSEnvironment(user, client);

                long dsAddressId = 0;
                if (user.AccountNumber == 0)
                {
                    var securityUser = service.SecurityService.LoadUserById(Guid.Parse(user.GlobalId));
                    user.Profile.FirstName = securityUser.Data.FirstName;
                    user.Profile.LastName = securityUser.Data.LastName;
                    user.Email = securityUser.Data.EmailAddress;
                    user.Username = securityUser.Data.Username;
                    user.GlobalId = securityUser.Data.Id;

                    if (user.Profile.PrimaryAddress == null || String.IsNullOrEmpty(user.Profile.PrimaryAddress.Address1))
                    {
                        newAddress.DsIsPrimary = true;
                        user.Profile.PrimaryAddress = newAddress;
                    }
                    CheckAndSetDSEnvironment(user, client);
                    dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(user.DsEnvironment, user.AccountNumber).AddressId;
                }
                else
                {
                    dsAddressId = service.AccountService.AddAddressToUser(user.AccountNumber, newAddress, user.DsEnvironment);
                }

                newAddress.DSAddressId = dsAddressId;

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData ?? CheckoutOrderReviewViewModel.InitializeWith(user, client);

                //if Shipping address has not been set use the new one as default
                if (addressType == "ShippingAddress")
                {
                    model.ShippingAddress = newAddress;
                    model.ShippingAddressId = newAddress.DSAddressId;
                    // Updating Delivery Address on Own subscription
                    if (model.ContainsSubscriptionItems())
                    {
                        var subscriptionItem = client.Cart.Items.SingleOrDefault(x => x.Item is SubscriptionCartItem &&
                                                                                    !((SubscriptionCartItem)x.Item)
                                                                                        .UserSubscription
                                                                                        .IsGiftSubcription
                            );
                        if (subscriptionItem != null)
                        {
                            var subscription = ((SubscriptionCartItem)subscriptionItem.Item).UserSubscription;
                            subscription.DeliveryAddress = newAddress;
                            subscriptionItem.Item.SetDescription(String.Format(
                                "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}<br/><b>Number of Issues: </b>{6} Issues ({7})",
                                subscription.User.Profile.FirstName,
                                subscription.User.Profile.LastName,
                                newAddress.Address1,
                                newAddress.City,
                                newAddress.StateCode,
                                newAddress.PostalCode,
                                subscription.NumberOfIssues,
                                subscription.PriceCodeDescription
                            ));
                        }
                    }
                }
                else
                {
                    model.BillingAddress = newAddress;
                    model.BillingAddressId = newAddress.DSAddressId;
                }

                model.ShouldReloadAddressList = true;
                ReloadUserAddresses(user, model, dsEnviroment);
                Session["CheckoutData"] = model;

                var response = new
                {
                    address = newAddress
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [HttpGet]
        public ActionResult UpdateAddress(Client client, long addressId, string addressType)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var dsEnviroment = DetermineDSEnvironment(user, client);

                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData ?? CheckoutOrderReviewViewModel.InitializeWith(user, client);
                ReloadUserAddresses(user, model, dsEnviroment);
                var address = model.AddressList.FirstOrDefault(a => a.DSAddressId == addressId);
                //if Shipping address has not been set use the new one as default
                if (addressType == "ShippingAddress")
                {
                    model.ShippingAddress = address;
                    model.ShippingAddressId = addressId;
                    model.ShippingAddressCountry = address.Country;

                    // Updating Delivery Address on Own subscription
                    if (model.ContainsSubscriptionItems())
                    {
                        var subscriptionItem = client.Cart.Items.SingleOrDefault(x => x.Item is SubscriptionCartItem &&
                                                                                    !((SubscriptionCartItem)x.Item)
                                                                                        .UserSubscription
                                                                                        .IsGiftSubcription
                            );
                        if (subscriptionItem != null)
                        {
                            var subscription = ((SubscriptionCartItem)subscriptionItem.Item).UserSubscription;
                            subscription.DeliveryAddress = model.ShippingAddress;
                            subscriptionItem.Item.SetDescription(String.Format(
                                "<b>Subscriber: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}<br/><b>Number of Issues: </b>{6} Issues ({7})",
                                subscription.User.Profile.FirstName,
                                subscription.User.Profile.LastName,
                                model.ShippingAddress.Address1,
                                model.ShippingAddress.City,
                                model.ShippingAddress.StateCode,
                                model.ShippingAddress.PostalCode,
                                subscription.NumberOfIssues,
                                subscription.PriceCodeDescription
                            ));
                        }
                    }
                }
                else
                {
                    model.BillingAddress = address;
                    model.BillingAddressId = addressId;
                }

                ReloadUserAddresses(user, model, dsEnviroment);
                Session["CheckoutData"] = model;

                return new JsonResult { Data = new { error = string.Empty }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [HttpGet]
        public ActionResult SelectShippingMethod(Client client, string shippingMethodId, int shippingAddressId)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model = checkoutData ?? CheckoutOrderReviewViewModel.InitializeWith(user, client);

                var isShippingRequired = true;
                var hasItemsWithShippingCost = client.Cart.HasItemsWithShippingCost();
                if (!hasItemsWithShippingCost)
                {
                    shippingMethodId = ShippingMethod.NotApplicable.Value;
                    isShippingRequired = false;
                }

                if (!client.Cart.HasShippableItems())
                {
                    model.ShippingAddressId = 0;
                }

                if (!String.IsNullOrEmpty(shippingMethodId))
                {
                    model.ShippingMethod = ShippingMethod.TryGetFromValue<ShippingMethod>(shippingMethodId);
                    model.ShippingMethodValue = model.ShippingMethod.DSValue;
                }

                // Validating invalid usage of the FREE shipping method
                if (
                    hasItemsWithShippingCost &&
                    model.ShippingMethod != null &&
                    model.ShippingMethod.Price == 0 &&
                    model.ShippingMethod.DSValue.Contains("FREE")
                    // This extra condition is needed since when using a coupon with free standard shipping the price of STANDAR shipping gets set to 0.00
                    )
                {
                    ModelState.AddModelError("ShippingMethodValue", "The shipping method field is required.");
                }

                IEnumerable<Country> countries;
                Country country;

                if (client.Cart.HasShippableItems() || model.ContainsSubscriptionItems())
                {
                    // Validating shipping method for selected country
                    var shippingAddress = service.AccountService.GetUserAddressById(shippingAddressId, client.Company.DonorStudioEnvironment);
                    model.ShippingAddress = shippingAddress;
                    model.ShippingAddressId = shippingAddress.DSAddressId;

                    countries = Country.GetAll<Country>().Where(x => x.Iso.ToLower() == shippingAddress.Country.ToLower()).ToList();

                    if (countries.Count() != 1)
                    {
                        var errorResponse = new { error = "It looks like there is a problem with the country in your shipping address" };
                        return new JsonResult { Data = errorResponse, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }

                    country = countries.First();
                    model.ShippingAddressCountry = country.DisplayName;

                    if (isShippingRequired && !Equals(country, client.Company.DefaultCountry) &&
                        (!Equals(model.ShippingMethod, ShippingMethod.USPSInternational) &&
                            !Equals(model.ShippingMethod, ShippingMethod.InternationalCanada) &&
                            !Equals(model.ShippingMethod, ShippingMethod.UKInternational)))
                    {
                        var errorResponse = new { error = StringExtensions.FormatWith("International Shipping is required for orders outside {0}.", client.Company.DefaultCountry.Name) };
                        return new JsonResult { Data = errorResponse, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }

                    client.Cart.SetShippingMethod(model.ShippingMethod);
                    model.ShippingCost = client.Cart.ShippingMethod.Price;
                    model.OrderTotal = client.Cart.Total();
                }

                Session["CheckoutData"] = model;

                var response = new
                {
                    error = string.Empty,
                    shippingMethodName = model.ShippingMethod.Description,
                    shippingCost = model.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, model.ShippingCost),
                    total = model.FormatWithCompanyCulture(CompanyCultureFormat, string.Empty, model.OrderTotal)
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [HttpPost]
        public ActionResult GetCreditCardToken(Client client, CreditCardViewModel creditCard)
        {
            return TryOnEvent(() =>
            {
                if (!IsCreditCardValid(creditCard.CreditCardNumber))
                    return new JsonResult { Data = new { error = "Credit card is not valid or is not supported" } };


                var selectedAddress = service.AccountService.GetUserAddressById(creditCard.BillingAddressId, client.Company.DonorStudioEnvironment);
                creditCard.SelectedAddress = selectedAddress;

                var checkoutData             = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var model                    = checkoutData;
                creditCard.Amount            = model.OrderTotal;
                model.PaymentList            = new PaymentListViewModel();
                model.PaymentList.AddCreditCard(creditCard);
                Session["CheckoutData"]      = model;

                return new JsonResult { Data = new { error = string.Empty, Response = "", lastNumbers = CreditCardViewModel.LastFour(creditCard.CreditCardNumber) } };
            });
        }

        private bool GetCreditCardToken(CreditCardViewModel creditCard, User user, Company company, out string result)
        {
            //TODO:  If IsTestCharge then supersede the Account and Transaction key with test info.

            var request = new PaymentRequest(
                company.CommerceAccountId,
                company.TransactionKey,
                false,
                user.Profile.FirstName,
                user.Profile.LastName,
                creditCard.SelectedAddress.Address1 + (String.IsNullOrEmpty(creditCard.SelectedAddress.Address2) ? String.Empty : " " + creditCard.SelectedAddress.Address2),
                creditCard.SelectedAddress.City,
                creditCard.SelectedAddress.StateCode,
                creditCard.SelectedAddress.PostalCode,
                creditCard.SelectedAddress.Country,
                "Zero dollars auth",
                creditCard.CreditCardNumber,
                String.Format("{0}{1}", creditCard.CCExpirationMonth, creditCard.TwoDigitsExpirationYear),
                creditCard.CID,
                // Doing 1.0 Pound Auth for UK company
                company == Company.JHM_UnitedKingdom ? 1 : 0,
                IsDM ? "DM" : company.SiteTag,
                company.Currency,
                BlueFinPaymentService.BluefinTransactionType.Auth
                );
            var response = service.CommerceService.BlueFinPaymentService.Process(request);

            result = response.IsAuthorized ? response.TransactionID : String.IsNullOrEmpty(response.AuthMessage) ? "Unknown reason" : response.AuthMessage.Replace("+", " ");

            return response.IsAuthorized;
        }

        public ActionResult ProcessOrder(Client client)
        {
            return TryOnEvent(() =>
            {
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;

                if (checkoutData == null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    checkoutData.IsFirstTimeSaltDonation = IsFirstTimeSaltDonation(client, SessionManager.CurrentUser, client.Company.DonorStudioEnvironment);

                    var phoneNumber = checkoutData.Profile.PrimaryPhone ?? new Phone();
                    var customerPhoneNumber = phoneNumber.AreaCode + "-" + phoneNumber.Number;
                    checkoutData.OrderTotal = checkoutData.CartSubTotalWithDiscount +
                                            checkoutData.ShippingCost + checkoutData.TaxCost;
                    checkoutData.OrderDate = DateTime.Now;


                    //Redeeming gifcards
                    //foreach (var payment in checkoutData.PaymentList.Payments.Where(x => x.IsGifCard()))
                    //{
                    //    var giftCard = payment.GiftCard;
                    //    service.GiftCardService.Redeem(giftCard.Number, giftCard.Pin, payment.Amount, SessionManager.SecurityUser.Id);
                    //    payment.IsAlreadyProcessed = true;
                    //}

                    //// Storing credit cards
                    //foreach (var payment in checkoutData.PaymentList.Payments.Where(x => x.IsCreditCard()))
                    //{
                    //    if (payment.CreditCard.ShouldSaveCreditCard)
                    //    {
                    //        //checkoutData.CCExpirationYear = fullExpirationYear;
                    //        var user = checkoutData.Profile.User;
                    //        var creditCard = Jhm.Web.Core.Models.CreditCard.Map(payment.CreditCard, user);
                    //        service.AccountService.AddCreditCardToUser(user.Username, creditCard, user.ApplicationName);
                    //    }
                    //}

                    checkoutData.CurrencyCode = client.Company.Currency;
                    checkoutData.DsSourceCode = IsDM ? "DIFFWEB" : "INET";

                    #region Magazine Subscriptions

                    var giftSubscriptions = new List<UserSubscriptionViewModel>();
                    SubscriptionCartItem mySubscription = null;
                    if (checkoutData.ContainsSubscriptionItems())
                    {
                        foreach (var item in checkoutData.Cart.Items.Where(x => x.Item is SubscriptionCartItem))
                        {
                            var subItem = item.Item as SubscriptionCartItem;
                            if (subItem == null) continue;

                            if (subItem.UserSubscription.IsGiftSubcription)
                            {
                                //var country = 
                                var dsEnvironment = client.Company.DonorStudioEnvironment;
                                var user = subItem.UserSubscription.User;
                                var dsAccountNumber =
                                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(
                                        dsEnvironment, DsAccountMapper.Map(user));
                                var dsAddressId =
                                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(
                                        dsEnvironment, dsAccountNumber).AddressId;
                                user.AccountNumber = dsAccountNumber;
                                user.DsEnvironment = dsEnvironment;
                                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                                user.Profile.ApplicationName = GetApplicationName();
                                giftSubscriptions.Add(subItem.UserSubscription);
                            }
                            else
                            {
                                subItem.UserSubscription.DeliveryAddress = checkoutData.ShippingAddress;
                                mySubscription = subItem;
                            }
                        }
                    }

                    #endregion

                    #region Gift Card Purchased for someone else

                    // If they are purchasing gift cards
                    if (checkoutData.ContainsGifCards())
                    {
                        foreach (var item in checkoutData.Cart.Items.Where(x => x.Item is GiftCardCartItem))
                        {
                            var subItem = item.Item as GiftCardCartItem;
                            if (subItem == null) continue;

                            if (subItem.GiftCardFormInfo.CardDeliveryType == GiftCardDeliveryTypes.ToSomeoneElse)
                            {
                                var newUser = new User();
                                newUser.Profile.FirstName = subItem.GiftCardFormInfo.RecipientFirstName;
                                newUser.Profile.LastName = subItem.GiftCardFormInfo.RecipientLastName;
                                //newUser.Email = subItem.GiftCardFormInfo.Email;
                                newUser.Profile.PrimaryAddress = new Address
                                {
                                    Address1 = subItem.GiftCardFormInfo.DeliveryAddress.Address1,
                                    Address2 = subItem.GiftCardFormInfo.DeliveryAddress.Address2,
                                    City = subItem.GiftCardFormInfo.DeliveryAddress.City,
                                    StateCode = subItem.GiftCardFormInfo.DeliveryAddress.StateCode,
                                    Country = subItem.GiftCardFormInfo.DeliveryAddress.Country,
                                    PostalCode = subItem.GiftCardFormInfo.DeliveryAddress.PostalCode
                                };
                                //newUser.Profile.PrimaryPhone = new Phone
                                //{
                                //    AreaCode = subItem.GiftCardFormInfo.PrimaryPhone.AreaCode,
                                //    CountryCode = subItem.GiftCardFormInfo.PrimaryPhone.CountryCode,
                                //    Number = subItem.GiftCardFormInfo.PrimaryPhone.Number
                                //};

                                var dsEnvironment = client.Company.DonorStudioEnvironment;
                                var dsAccountNumber =
                                    service.DsServiceCollection.DsAccountTransactionService.CreateAccount(
                                        dsEnvironment, DsAccountMapper.Map(newUser));
                                var dsAddressId =
                                    service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(
                                        dsEnvironment, dsAccountNumber).AddressId;
                                newUser.AccountNumber = dsAccountNumber;
                                newUser.DsEnvironment = dsEnvironment;
                                newUser.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                                newUser.Profile.ApplicationName = GetApplicationName();

                                subItem.GiftCardFormInfo.User = newUser;
                            }
                            else
                            {
                                subItem.GiftCardFormInfo.User = SessionManager.CurrentUser;
                            }
                        }
                    }

                    #endregion

                    try
                    {
                        var transactionId = (long)0;  // disabled card processing and Donor processing temporarily JFE
                        //var transactionId = service.CartService.SaveTransaction(client.Company.DonorStudioEnvironment, checkoutData);

                        checkoutData.ApprovalCode = transactionId.ToString();
                        checkoutData.TransactionNumber = transactionId.ToString();


                        GenerateDownloadLinks(checkoutData);
                        if (checkoutData.Cart.DiscountApplied != null && checkoutData.Cart.DiscountApplied.CodeName.ToUpper() == "CYBER14")
                        {
                            SendCyberModayThankYouEmail(checkoutData);
                        }

                        if (checkoutData.BillingAddress == null && SessionManager.CurrentUser != null)
                        {
                            checkoutData.BillingAddress = SessionManager.CurrentUser.Profile.PrimaryAddress;
                        }
                        checkoutData.RegisterClient(client);
                        SendOrderReceiptEmail(checkoutData);
                        SendEmailForDonations(checkoutData);
                        if (mySubscription != null)
                        {
                            SendUserSubscriptionEmail(client, checkoutData.Profile.User, mySubscription.UserSubscription);
                        }
                        foreach (var giftSubscription in giftSubscriptions)
                        {
                            SendGiftSubscriptionEmail(client, checkoutData.Profile.User, giftSubscription);
                        }

                        if (checkoutData.Cart.DiscountApplied != null)
                        {
                            service.DiscountCodeService.IncreaseUseCount(checkoutData.Cart.DiscountApplied.Id);
                        }

                        return RedirectToAction("Receipt");
                    }
                    catch (Exception e)
                    {
                        //Redeeming gifcards
                        foreach (var payment in checkoutData.PaymentList.Payments.Where(x => x.IsGifCard() && x.IsAlreadyProcessed))
                        {
                            var giftCard = payment.GiftCard;
                            service.GiftCardService.Refund(giftCard.Number, giftCard.Pin, payment.Amount, "CheckoutController.ProcessOrder: " + e.Message, SessionManager.SecurityUser.Id);
                            payment.IsAlreadyProcessed = false;
                        }


                        TempData[MagicStringEliminator.Messages.ErrorMessage] = "Sorry, an error occurred while processing your order. Please try again in a few minutes.";
                        var cartItems = String.Empty;
                        cartItems = client.Cart.Items.Aggregate(cartItems,
                                                                (current, item) =>
                                                                current +
                                                                ("{Code: " + item.Item.Code + ", Title: " +
                                                                 item.Item.Title + "}\n"));
                        Log.For(this).Erroneous("[TryOnEvent1]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}\nCart Items: \n{4}",
                            e.Message, e.StackTrace, HttpContext.Request.Url.Host, HttpContext.Request.Url, cartItems);
                        return RedirectToAction("Checkout");
                    }
                }
            });
        }

        private bool SendUserSubscriptionEmail(Client client, Core.Models.User user, UserSubscriptionViewModel userSubscription)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/generic.html"));

                var content = new StringBuilder();
                content.Append("<p>");
                content.AppendFormat("Greetings {0},", user.Profile.FirstName);
                content.Append("<br/>");
                content.Append("<br/>");
                content.AppendFormat(@"Thank you so much for subscribing to the John Hagee Ministries magazine.  
                    We are delighted to have you on board, and value your commitment to this ministry.  
                    Inside the pages of this full-color, national magazine, you will find articles written by Pastor Hagee and Pastor Matt on hot Biblical topics that impact our lives today.   
                    This publication will give you a sneak peak of things to come and a recap of important ministry events.  
                    We thank God for sending you our way, and relish your comments.  
                    If you have something you would like to know more about or a prayer request, you can contact us at {0}.  
                    God bless you, and thank you for helping us spread all the Gospel to all the world.",
                    client.Company.AdditionalData[Company.DataKeys.PhoneNumber]);
                content.Append("<br/>");
                content.Append("<br/>");
                content.AppendFormat("Here is the information we have in our records regarding your subscription. Please review the following information and contact us to {0} if you find any inaccuracy:", client.Company.AdditionalData[Company.DataKeys.PhoneNumber]);
                content.Append("</p>");
                content.Append("<br/>");
                content.Append("<table border='0'>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Subscriber:</td>");
                content.AppendFormat("<td>{0} {1}</td>", user.Profile.FirstName, user.Profile.LastName);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Delivery Address:</td>");
                content.AppendFormat("<td>{0} {1}</td>", userSubscription.DeliveryAddress.Address1, userSubscription.DeliveryAddress.Address2);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>City:</td>");
                content.AppendFormat("<td>{0}</td>", userSubscription.DeliveryAddress.City);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>State:</td>");
                content.AppendFormat("<td>{0}</td>", userSubscription.DeliveryAddress.StateCode);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Postal Code:</td>");
                content.AppendFormat("<td>{0}</td>", userSubscription.DeliveryAddress.PostalCode);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Country:</td>");
                content.AppendFormat("<td>{0}</td>", Country.FindByISOCode(userSubscription.DeliveryAddress.Country).DisplayName);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Phone Number:</td>");
                content.AppendFormat("<td>{0}-{1}-{2}</td>", user.Profile.PrimaryPhone.CountryCode, user.Profile.PrimaryPhone.AreaCode, user.Profile.PrimaryPhone.Number);
                content.Append("</tr>");

                content.Append("</table>");

                content.Append("<p>");
                content.AppendFormat("Please call us to {0} if you see any inaccurate information.", client.Company.AdditionalData[Company.DataKeys.PhoneNumber]);
                content.Append("</p>");
                content.Append("<br/>");

                var replacements = new ListDictionary
                                       {
                                           {"<%CONTENT%>", content.ToString()},
                                           {"<%EMAIL%>", user.Email},
                                           {"<%HTTP_HOST%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                           {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/")}
                                       };

                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - " + userSubscription.SubscriptionTitle + " Subscription"
                };

                var msg = md.CreateMailMessage(user.Email, replacements, body, new System.Web.UI.Control());
                service.EmailService.SendEmail(msg);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                TempData[MagicStringEliminator.Messages.ErrorMessage] = ex.Message;
                return false;
            }
        }

        private bool SendGiftSubscriptionEmail(Client client, Jhm.Web.Core.Models.User purchaserUser, UserSubscriptionViewModel giftSubscription)
        {
            // Subscriber email is optional
            if (Foundation.StringExtensions.IsNullOrEmpty(giftSubscription.User.Email))
            {
                return false;
            }
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/generic.html"));

                var content = new StringBuilder();
                content.Append("<br/>");
                content.Append("<p>");
                content.AppendFormat("Dear {0},", giftSubscription.User.Profile.FirstName);
                content.Append("<br/>");
                content.Append("<br/>");
                content.AppendFormat(
                    "This email has been sent to you to inform you that your friend {0} {1} has graciously given you a one-year subscription to the {2}. We pray that the content blesses you and that you use it as a catalyst to further the Gospel to your friends and loved ones in your own community.",
                    purchaserUser.Profile.FirstName, purchaserUser.Profile.LastName, giftSubscription.SubscriptionTitle);
                content.Append("<br/>");
                content.Append("<br/>");
                content.Append("Blessings to you,<br/>John Hagee Ministries");
                content.Append("<br/>");
                content.Append("<br/>");
                content.AppendFormat("Here is the information we have in our records regarding your gift subscription. Please review the following information and contact us to {0} if you find any inaccuracy:", client.Company.AdditionalData[Company.DataKeys.PhoneNumber]);
                content.Append("</p>");
                content.Append("<br/>");
                content.Append("<table border='0'>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Subscriber:</td>");
                content.AppendFormat("<td>{0} {1}</td>", giftSubscription.User.Profile.FirstName, giftSubscription.User.Profile.LastName);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Delivery Address:</td>");
                content.AppendFormat("<td>{0} {1}</td>", giftSubscription.User.Profile.PrimaryAddress.Address1, giftSubscription.User.Profile.PrimaryAddress.Address2);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>City:</td>");
                content.AppendFormat("<td>{0}</td>", giftSubscription.User.Profile.PrimaryAddress.City);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>State:</td>");
                content.AppendFormat("<td>{0}</td>", giftSubscription.User.Profile.PrimaryAddress.StateCode);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Postal Code:</td>");
                content.AppendFormat("<td>{0}</td>", giftSubscription.User.Profile.PrimaryAddress.PostalCode);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Country:</td>");
                content.AppendFormat("<td>{0}</td>", Country.FindByISOCode(giftSubscription.User.Profile.PrimaryAddress.Country).DisplayName);
                content.Append("</tr>");

                content.Append("<tr>");
                content.Append("<td style='font-weight:bold; text-align: right'>Phone Number:</td>");
                content.AppendFormat("<td>{0}-{1}-{2}</td>", giftSubscription.User.Profile.PrimaryPhone.CountryCode, giftSubscription.User.Profile.PrimaryPhone.AreaCode, giftSubscription.User.Profile.PrimaryPhone.Number);
                content.Append("</tr>");

                content.Append("</table>");

                content.Append("<br/>");
                content.Append("<p>");
                content.AppendFormat("Please call us to {0} if you see any inaccurate information.", client.Company.AdditionalData[Company.DataKeys.PhoneNumber]);
                content.Append("</p>");

                var replacements = new ListDictionary
                                       {
                                           {"<%CONTENT%>", content.ToString()},
                                           {"<%EMAIL%>", giftSubscription.User.Email},
                                           {"<%HTTP_HOST%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                           {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/")}
                                       };

                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - " + giftSubscription.SubscriptionTitle + " Gift Subscription"
                };

                var msg = md.CreateMailMessage(giftSubscription.User.Email, replacements, body, new System.Web.UI.Control());
                service.EmailService.SendEmail(msg);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                TempData[MagicStringEliminator.Messages.ErrorMessage] = ex.Message;
                return false;
            }
        }

        private void SendCyberModayThankYouEmail(CheckoutOrderReviewViewModel checkoutData)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath(StringExtensions.FormatWith("/EmailTemplates/CyberMonday/{0}", "CM_ThankYou.html")));

                var replacements = new ListDictionary
                    {
                        {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/CyberMonday/")}
                    };
                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - Cyber Monday"
                };

                var msg = md.CreateMailMessage(checkoutData.Email, replacements, body, new System.Web.UI.Control());
                service.EmailService.SendEmail(msg);
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
            }
        }


        private void SendEmailForDonations(CheckoutOrderReviewViewModel checkoutData)
        {
            var donations = checkoutData.CartItems.Where(i => i.Item is DonationCartItem).Select(x => service.DonationService.GetDonation(x.Item.Code));
            foreach (var donation in donations.Where(d => !String.IsNullOrEmpty(d.CustomEmailBody)))
            {
                SendDonationEmail(checkoutData, donation);
            }
        }

        private void SendDonationEmail(CheckoutOrderReviewViewModel checkoutData, Jhm.Web.Core.Models.IDonation donation)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath(StringExtensions.FormatWith("/EmailTemplates/{0}", donation.CustomEmailTemplateFileName ?? "generic.html")));


                var replacements = new ListDictionary
                                       {
                                           {"<%CONTENT%>", donation.CustomEmailBody},
                                           {"<%EMAIL%>", checkoutData.Email},
                                           {"<%OrderDate%>", checkoutData.OrderDate.ToShortDateString()},
                                           {"<%HTTP_HOST%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                           {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/")}
                                       };

                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - " + donation.Title
                };

                var msg = md.CreateMailMessage(checkoutData.Email, replacements, body, new System.Web.UI.Control());
                service.EmailService.SendEmail(msg);
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
            }
        }

        private Dictionary<string, DigitalDownload> _digitalDownloads;

        public Dictionary<string, DigitalDownload> DigitalDownloads
        {
            get
            {
                if (_digitalDownloads == null)
                {
                    _digitalDownloads = new Dictionary<string, DigitalDownload>();
                }

                return _digitalDownloads;
            }
        }

        private void GenerateDownloadLinks(CheckoutOrderReviewViewModel checkoutData)
        {
            if (checkoutData.ContainsDownloadableItem())
            {
                foreach (
                    var cartItem in
                        checkoutData.CartItems.Where(
                            x => x.Item is StockItem && ((StockItem)x.Item).IsDownloadableProduct()))
                {
                    var stockItem = ((StockItem)cartItem.Item);
                    var digitalDownload = new DigitalDownload(checkoutData.Profile.User, stockItem);
                    service.DigitalDownloadService.SaveDigitalDownload(digitalDownload);
                    DigitalDownloads.Add(stockItem.Code, digitalDownload);
                }
            }
        }

        private void SendOrderReceiptEmail(CheckoutOrderReviewViewModel checkoutData)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath(IsDM ? "/EmailTemplates/DifferenceMedia/Receipt.html" : "/EmailTemplates/OrderReceipt.html"));

                #region Billing & Shipping
                //Note: This is a hack to prevent NullReferenceException (possibly caused by user not having primary address)
                checkoutData.BillingAddress = checkoutData.BillingAddress ?? new Address();

                var billingInfo =
                            String.Format(
                                @"{0} {1}<br>
                        {2}<br>
                        {3}                                            
                        {4}, {5} {6}<br>
                        {7}<br>                                            
                        {8}<br>                                            
                        {9}",
                                checkoutData.FirstName, checkoutData.LastName,
                                checkoutData.BillingAddress.Address1,
                                String.IsNullOrEmpty(checkoutData.BillingAddress.Address2) ? String.Empty : checkoutData.BillingAddress.Address2 + "<br />",
                                checkoutData.BillingAddress.City, checkoutData.BillingAddress.StateCode,
                                checkoutData.BillingAddress.PostalCode,
                                checkoutData.BillingAddressCountry,
                                String.IsNullOrEmpty(checkoutData.BillingPhone)
                                    ? String.Empty
                                    : checkoutData.BillingPhone + "<br />",
                                checkoutData.Email);
                var shippingInfo = "Not required";
                if (checkoutData.ShippingAddress != null)
                {
                    shippingInfo = String.Format(
                        @"{0} {1}<br>
                        {2}<br>
                        {3}
                        {4}, {5} {6}<br>
                        {7}<br>
                        {8}-{9}-{10}",
                        checkoutData.ShippingFirstName, checkoutData.ShippingLastName,
                        checkoutData.ShippingAddress.Address1,
                        String.IsNullOrEmpty(checkoutData.ShippingAddress.Address2)
                            ? String.Empty
                            : checkoutData.ShippingAddress.Address2 + "<br />",
                        checkoutData.ShippingAddress.City, checkoutData.ShippingAddress.StateCode,
                        checkoutData.ShippingAddress.PostalCode,
                        checkoutData.ShippingAddressCountry,
                        checkoutData.ShippingPhoneCountryCode, checkoutData.ShippingPhoneAreaCode,
                        checkoutData.ShippingPhoneNumber);
                }

                var paymentSb = new StringBuilder("<tbody>");
                foreach (var payment in checkoutData.PaymentList.Payments)
                {
                    var html =
                            checkoutData.FormatWithCompanyCulture(
                                @"                                   
                                    <tr>
                                        <td>
                                           {0}
                                        </td> 
                                        <td>
                                            ************{1}
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    <td class='cost'>
                                        {2:C}
                                    </td>
                                </tr>",
                                (payment.IsCreditCard() ? "**" : String.Empty) + payment.PaymentType.Name,
                                (payment.PaymentNumber).Substring(Math.Max(0, payment.PaymentNumber.Length - 4)),
                                payment.Amount);
                    paymentSb.Append(html);
                }
                paymentSb.Append(
                        @"<tr class='space'> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                </tr> 
                            </tbody>");
                var paymentTableBody = paymentSb.ToString();
                //var creditCardTypeImage = String.Format("credit-{0}.gif", checkoutData.CreditCardType);
                #endregion

                #region Donations
                var donationsHeaderStyle = "style='display:none;'";
                var donationsTableBody = String.Empty;
                if (checkoutData.ContainsDonations())
                {
                    donationsHeaderStyle = String.Empty;
                    var sb = new StringBuilder("<tbody>");
                    foreach (var cartItem in checkoutData.CartItems.Where(x => x.Item is DonationCartItem))
                    {
                        var donationsHtml =
                            checkoutData.FormatWithCompanyCulture(
                                @"                                   
                                    <tr>
                                        <td>
                                           <h4> {0}</h4>
                                        </td> 
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    <td class='cost'>
                                        {1:C}
                                    </td>
                                </tr>",
                                cartItem.Item.Title, cartItem.Item.Price);
                        sb.Append(donationsHtml);
                    }
                    sb.Append(
                        @"<tr class='space'> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                </tr> 
                            </tbody>");
                    donationsTableBody = sb.ToString();
                }
                #endregion

                #region Products
                var productsHeaderStyle = "style='display:none;'";
                var productsTableBody = String.Empty;
                if (checkoutData.ContainsStockItem())
                {
                    productsHeaderStyle = String.Empty;
                    var sb = new StringBuilder("<tbody>");
                    foreach (var cartItem in checkoutData.CartItems.Where(x => x.Item is StockItem))
                    {
                        var itemHtml = checkoutData.FormatWithCompanyCulture(
                            @"                                   
                                <tr>
                                    <td>
                                       <h4> {0}</h4>
                                    </td> 
                                    <td align='right'>
                                        {1}
                                    </td>
                                    <td align='right'>
                                       {2:C}
                                    </td>
                                <td align='right' class='cost'>
                                    {3:C}
                                </td>
                            </tr>",
                            cartItem.Item.Title, cartItem.Quantity, cartItem.Item.Price,
                            cartItem.Item.Price * cartItem.Quantity);
                        sb.Append(itemHtml);

                        if (DigitalDownloads.ContainsKey(cartItem.Item.Code))
                        {
                            var dd = DigitalDownloads[cartItem.Item.Code];
                            sb.AppendFormat(
                                @"                                   
                                <tr>
                                    <td>
                                       <a href='<%HTTP_HOST%>/Account/MyDownloads/'>Click here to download this purchase</a>
                                    </td> 
                                 
                            </tr>");
                        }
                    }
                    sb.Append("</tbody>");
                    productsTableBody = sb.ToString();
                }
                #endregion

                #region Promotional Items
                var promotionalItemsHeaderStyle = "style='display:none;'";
                var promotionalItemsTableBody = String.Empty;
                if (checkoutData.Cart.PromotionalItems.Any())
                {
                    promotionalItemsHeaderStyle = String.Empty;
                    var sb = new StringBuilder("<tbody>");
                    foreach (var cartItem in checkoutData.Cart.PromotionalItems)
                    {
                        var htmlRow =
                            checkoutData.FormatWithCompanyCulture(
                                @"                                   
                                    <tr>
                                        <td>
                                           <h4> {0}</h4>
                                        </td> 
                                        <td>
                                            {1}
                                        </td>
                                        <td>
                                            {2}
                                        </td>
                                    <td class='cost'>
                                        {3:C}
                                    </td>
                                </tr>",
                                cartItem.Product.Title, cartItem.Quantity, 0, 0);
                        sb.Append(htmlRow);
                    }
                    sb.Append(
                        @"<tr class='space'> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                </tr> 
                            </tbody>");
                    promotionalItemsTableBody = sb.ToString();
                }
                #endregion

                #region Subscriptions
                var subscriptionsHeaderStyle = "style='display:none;'";
                var subscriptionsTableBody = String.Empty;
                if (checkoutData.ContainsSubscriptionItems())
                {
                    subscriptionsHeaderStyle = String.Empty;
                    var sb = new StringBuilder("<tbody>");
                    foreach (var cartItem in checkoutData.CartItems.Where(x => x.Item is SubscriptionCartItem))
                    {
                        var subscriptionsHtml =
                            checkoutData.FormatWithCompanyCulture(
                                @"                                   
                                    <tr>
                                        <td>
                                           <h4> {0}</h4>
                                        </td> 
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    <td class='cost'>
                                        {1:C}
                                    </td>
                                </tr>",
                                cartItem.Item.Title, cartItem.Item.Price);
                        sb.Append(subscriptionsHtml);
                    }
                    sb.Append(
                        @"<tr class='space'> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td> 
                                </tr> 
                            </tbody>");
                    subscriptionsTableBody = sb.ToString();
                }
                #endregion

                #region Discounts
                var discountRows = String.Empty;
                if (checkoutData.DiscountApplied != null)
                {
                    discountRows = StringExtensions.FormatWith(@"
                        <tr class='row-subtotal'>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class='subtotal'>
                                Subtotal before discount:
                            </td>
                            <td class='cost'>
                                {0}
                            </td>
                        </tr>
                        <tr class='row-subtotal'>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class='subtotal'>
                                {1}:
                            </td>
                            <td class='cost'>
                                {2}
                            </td>
                        </tr>", checkoutData.FormatWithCompanyCulture("C", checkoutData.CartSubTotal),
                            checkoutData.DiscountApplied.ShortDescription,
                            checkoutData.FormatWithCompanyCulture("C", checkoutData.DiscountTotal * (-1))
                        );
                }
                #endregion

                var firstSaltDonationInfo = !checkoutData.IsFirstTimeSaltDonation ? string.Empty :
                    @"<h4>New Salt Partner</h4>
					Thank you for your interest in partnering with John Hagee Ministries.  To set up a monthly gift  and the details of your account as a Salt Partner with JHM, we recommend you contact the JHM Partners Department at 1-877-546-7258  or email partners@jhm.org.  One of our Partner Dept. team members will gladly assist you. We appreciate your part in taking all the gospel to all the world and to every generation.
					<br /><br />
					Blessings to You and those you love.";

                var fullVirtualWebSitePath = "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"];

                var replacements = new ListDictionary
                {
                    {"<%EMAIL%>", checkoutData.Email},
                    {"<%ApprovalCode%>", checkoutData.ApprovalCode ?? "???"},
                    {"<%OrderDate%>", checkoutData.OrderDate.ToShortDateString()},
                    {"<%BillingInfo%>", billingInfo},
                    {"<%ShippingInfo%>", shippingInfo},
                    {"<%PaymentTableBody%>", paymentTableBody},
                    {"<%FirstSaltDonationInfo%>", firstSaltDonationInfo},
                    {"<%CompanyCommerceDescriptor%>", checkoutData.Client.Company.CommerceDescriptor},
                    //{"<%CreditCardType%>", checkoutData.CreditCardType},
                    //{"<%CreditCardTypeImage%>", creditCardTypeImage},
                    //{"<%LastFourOfCC%>",checkoutData.CreditCardNumber.Substring(checkoutData.CreditCardNumber.Length - 4)},
                    //{"<%NameOnCard%>", checkoutData.NameOnCard ?? string.Empty},
                    //{"<%ExpirationDate%>",String.Format("{0}/{1}", checkoutData.CCExpirationMonth,checkoutData.CCExpirationYear)},
                    {"<%ShippingMethod%>", checkoutData.ShippingMethod.DisplayName},
                    {"<%DonationsHeaderStyle%>", donationsHeaderStyle},
                    {"<%DonationsTableBody%>", donationsTableBody},
                    {"<%SubscriptionsHeaderStyle%>", subscriptionsHeaderStyle},
                    {"<%SubscriptionsTableBody%>", subscriptionsTableBody},
                    {"<%PromotionalItemsHeaderStyle%>", promotionalItemsHeaderStyle},
                    {"<%PromotionalItemsTableBody%>", promotionalItemsTableBody},
                    {"<%ProductsHeaderStyle%>", productsHeaderStyle},
                    {"<%ProductsTableBody%>", productsTableBody},
                    {"<%DiscountRows%>", discountRows},
                    {"<%CartSubtotal%>",checkoutData.FormatWithCompanyCulture("C",checkoutData.CartSubTotalWithDiscount)},
                    {"<%OrderShippingCost%>",checkoutData.FormatWithCompanyCulture("C", checkoutData.ShippingCost)},
                    {"<%OrderTaxCost%>",checkoutData.FormatWithCompanyCulture("C", checkoutData.TaxCost)},
                    {"<%OrderTotal%>",checkoutData.FormatWithCompanyCulture("C", checkoutData.OrderTotal)},
                    {"<%HTTP_HOST%>", fullVirtualWebSitePath},
                    {"<%WEBSITE_URL%>", fullVirtualWebSitePath},
                    {"<%IMAGES_PATH%>", fullVirtualWebSitePath + (IsDM? Url.Content("/EmailTemplates/DifferenceMedia/images/"): Url.Content("/Content/images/"))},
                };

                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - Order Receipt: " + checkoutData.ApprovalCode
                };

                var msg = md.CreateMailMessage(checkoutData.Email, replacements, body, new System.Web.UI.Control());
                if (IsDM)
                {
                    service.EmailService.SendDMEmail(msg);
                }
                else
                {
                    service.EmailService.SendEmail(msg);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
            }
        }

        public bool IsFirstTimeSaltDonation(Client client, User user, string dsEnvironment)
        {
            var isUserASaltPartner = service.AccountService.IsUserASaltPartner(client, user, dsEnvironment);
            return !isUserASaltPartner && client.Cart.Items.Any(x => x.Item.Code == "Salt_Covenant");
        }

        public ActionResult Receipt(Client client)
        {
            return TryOnEvent(() =>
            {
                var checkoutData = Session["CheckoutData"] as CheckoutOrderReviewViewModel;
                var cartCopy = (Cart)client.Cart.Clone();
                client.Cart.Clear();
                checkoutData.RegisterClient(client);
                checkoutData.MapCart(cartCopy, false);
                Session.Remove("CheckoutData");
                checkoutData.IsDifferenceMediaPage = IsDM;

                if (checkoutData.BillingAddress == null && SessionManager.CurrentUser != null)
                {
                    checkoutData.BillingAddress = SessionManager.CurrentUser.Profile.PrimaryAddress;
                }

                return View("Receipt", checkoutData);
            });
        }

        public ActionResult BuildSideMenuCart(Client client)
        {
            return (PartialViewResult)TryOnEvent(() =>
            {

                // A temporary model is created to avoid calculating sub-total and total outside the MapCart method.
                var tempModel = new CartViewModel();

                var cart = client.Cart;

                tempModel.MapCart(cart, false);

                return PartialView("_SideMenuCart", tempModel);
            });
        }

        private bool IsCreditCardValid(string cardNumber)
        {
            return CreditCardType.GetAll<CreditCardType>().Any(cc => Regex.IsMatch(cardNumber, cc.RegexPattern));
        }
    }
}