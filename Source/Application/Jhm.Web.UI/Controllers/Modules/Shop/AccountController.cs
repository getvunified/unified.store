﻿using System;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using Foundation;
using Getv.Security;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Common.Utilities.HTMLHelpers;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce.ViewModels;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using Jhm.em3.Core;
using getv.donorstudio.core.Global;
using MagicStringEliminator = Jhm.Web.Core.MagicStringEliminator;
using StringExtensions = Foundation.StringExtensions;

namespace Jhm.Web.UI.Controllers
{

    public partial class AccountController : BaseController<IServiceCollection>
    {       

        [HttpGet]
        public ActionResult Login(Client client)
        {
            return PartialView("_Login",SessionManager.CurrentUser);
        }

        public ActionResult Register(Client client, string returnUrl = null)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Register(Client client, RegisterModel register, string returnUrl = null)
        {
            ViewBag.returnUrl = returnUrl;
            return View(register);
        }

        [HttpGet]
        public ActionResult LogOut(Client client)
        {
            service.FormsService.SignOut();
            ClearRegisteredClient();
            SessionManager.ClearCurrentUser();
            Session.RemoveAll();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult GetCountryCodes() 
        {
            var data = new List<object>();
            foreach (Country c in Country.GetAll<Country>())
            {
                data.Add(new { Country = c.DisplayName, PhoneCode = c.PhoneCode });
            }

            var response = new
            {
                countryCodes = data.ToArray()
            };

            return new JsonResult() { Data = response , JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult RegisterNewUser(Client client, RegisterModel model, string returnUrl)
        {
            MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
            model.IsDifferenceMediaPage = IsDM;
            return TryOnEventWithoutTransaction(() =>
            {
                model.UserName = model.UserName.Trim();
                model.Password = model.Password.Trim();

                if (model.NewsLetterOptIn)
                {
                    if (!StringExtensions.IsNullOrEmpty(model.FirstName) && !StringExtensions.IsNullOrEmpty(model.LastName) && !StringExtensions.IsNullOrEmpty(model.Email))
                    {
                        var data = new ENewsLetterSignUpViewModel
                        {
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            CompanyName = model.Company,
                            State = model.State,
                            Country = model.Country,
                            Zip = model.Zip,
                            Email = model.Email,
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            AccountNumber = 0,
                            EmailListCode = "WUP"
                        };

                        try
                        {
                            EMailSignUp(data);
                        }
                        catch (Exception ex)
                        {
                            // Came up due to the database not being accessible..  
                            Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                        }


                    }
                }

                if (model.PhoneAreaCode == "Area code") model.PhoneAreaCode = String.Empty;
                if (model.PhoneNumber == "Phone number") model.PhoneNumber = String.Empty;

                var addressIsValid = ValidateAddress(model);

                if (ModelState.IsValid)
                {
                    var result = service.SecurityService.CreateUser(model.UserName, model.Password, model.Email, model.FirstName, model.LastName, model.NewsLetterOptIn);
                    if (result.IsSuccessful)
                    {
                        ApiMessage<Getv.Security.IUser> globalUser = null;
                        globalUser = service.SecurityService.LoadUserByUsername(model.UserName);
                        TryWithTransaction(() =>
                        {
                            createStatus = service.MembershipService.CreateUserWithGlobalId(globalUser.Data.Id, model.UserName, model.Password, model.Email, "PasswordQuestion", "PasswordAnswer");
                        });

                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            TryWithTransaction(() =>
                            {
                                var user = service.AccountService.GetUser(model.UserName, GetApplicationName());
                                if (user.IsTransient())
                                {
                                    throw new SecurityException("Username Does Not Exist");
                                }

                                model.MapToUser(user);
                                user.AccountNumber = 0;
                                user.DsEnvironment = client.Company.DonorStudioEnvironment;

                                if (addressIsValid)
                                {
                                    var dsAccountNumber = service.DsServiceCollection.DsAccountTransactionService.CreateAccount(client.Company.DonorStudioEnvironment, DsAccountMapper.Map(user));
                                    var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(client.Company.DonorStudioEnvironment, dsAccountNumber).AddressId;
                                    user.AccountNumber = dsAccountNumber;
                                    user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                                }
                                else
                                {
                                    user.Profile.PrimaryAddress = null;
                                }

                                user.Profile.ApplicationName = GetApplicationName();
                                service.AccountService.SaveUser(user);
                            });
                            service.FormsService.SignIn(model.UserName, false/* createPersistentCookie */);
                            SendAccountCreatedEmail(model);
                            var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];
                            if (redirectRoute != null)
                            {
                                Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                                return RedirectToRoute(redirectRoute);
                            }
                            if (!String.IsNullOrEmpty(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            return RedirectToAction("Index", "Home");
                        }
                        ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                    }
                    else
                    {
                        ModelState.AddModelError(AuthenticationStatusToPropertyName((SecurityErrorCode)result.Error.ErrorCode), AuthenticationStatusToUserMessage((SecurityErrorCode)result.Error.ErrorCode));
                    }
                    // If we got this far, something failed, redisplay form
                    return View("Register", ReturnToViewWithError(model, client));
                }

                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));

                // If we got this far, something failed, redisplay form
                return View("Register", ReturnToViewWithError(model, client));
            });
        }

        [HttpGet]
        public JsonResult getCartQuantityItems(Client client)
        {
            var response = new
            {
                Quantity = client.Cart.Items.Count() 
            };

            return new JsonResult(){ Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        [Authorize]
        [HttpPost]
        public virtual ActionResult AjaxChangeEmailAddress(ChangeEmailModel model)
        {
            var error = true;
            if (model.NewEmail != model.ConfirmEmail)
            {
                model.Message = "New email and confirm email should be the same";
            }

            if (ModelState.IsValid)
            {
                var user = SessionManager.CurrentUser;
                
                if (user == null)
                {
                    throw new Exception("User not found");
                }
                model.CurrentEmail = user.Email;
                var hostUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

                var confirmationUrl = hostUrl + VirtualPathUtility.ToAbsolute("~/Account/ConfirmEmailAddressChange/{0}");
                var status = service.SecurityService.InitiateEmailAddressChange(user.Email, model.NewEmail, confirmationUrl);

                if (status.IsSuccessful)
                {
                    error = false;
                    model.Message = "An email with instructions on how to confirm your new email is on its way to you.";
                }
                else if (status.Error.ErrorCode == (int)SecurityErrorCode.ProviderError)
                {
                    model.Message = "Sorry, there was a problem sending instructions to your email. Please try again later";
                }
                else if (status.Error != null)
                {
                    model.Message = "Sorry, there was a problem sending instructions to your email: " + status.Error.UserMessage.SeparateCapitalizedWords();
                }
            }
            return new JsonResult { Data = new { error = error, message = model.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public virtual ActionResult AjaxRequestPasswordChange(ForgotPasswordModel model)
        {
            var error = true;

            if (ModelState.IsValid)
            {
                var hostUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

                var confirmationUrl = hostUrl + VirtualPathUtility.ToAbsolute("~/Account/ConfirmPasswordReset/{0}");
                var status = service.SecurityService.InitiatePasswordReset(model.Email, confirmationUrl);

                if (status.IsSuccessful)
                {
                    error = false;
                    model.Message = "An email with instructions on how to change your password is on its way to you. If this is not the email address you registered with, you will not receive an email.";
                    Session["DoNotResendTokenEmail"] = true;
                }
                else if (status.Error.ErrorCode == (int)SecurityErrorCode.ProviderError)
                {
                    model.Message = "Sorry, there was a problem sending instructions to your email. Please try again later";
                }
                else if (status.Error != null)
                {
                    model.Message = "Sorry, there was a problem sending instructions to your email: " + status.Error.UserMessage.SeparateCapitalizedWords();
                }
            }

            return new JsonResult { Data = new { error = error, message = model.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult AjaxAddAddress(Client client, AddressViewModel model)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var newAddress = new Address();
                model.MapToAddress(newAddress);

                if (user.AccountNumber == 0 || user.Profile.PrimaryAddress == null || String.IsNullOrEmpty(user.Profile.PrimaryAddress.Address1))
                {
                    var securityUser = service.SecurityService.LoadUserById(Guid.Parse(user.GlobalId));
                    user.Profile.FirstName = securityUser.Data.FirstName;
                    user.Profile.LastName = securityUser.Data.LastName;
                    user.Email = securityUser.Data.EmailAddress;
                    user.Username = securityUser.Data.Username;
                    user.GlobalId = securityUser.Data.Id;

                    newAddress.DsIsPrimary = true;
                    user.Profile.PrimaryAddress = newAddress;

                    CheckAndSetDSEnvironment(user, client);
                }
                else
                {
                    if (model.Primary)
                    {
                        var country = GetAndSetCountry(model);
                        var dsEnvironment = DetermineDSEnvironment(user, client);

                        if (country != null)
                        {
                            var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(client.Company.DonorStudioEnvironment, user.AccountNumber).AddressId;
                            user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                            service.AccountService.SaveUser(user, false, dsEnvironment: dsEnvironment);
                        }
                    }
                    var dsEnviroment = DetermineDSEnvironment(user, client);
                    var addressId = service.AccountService.AddAddressToUser(user.AccountNumber, newAddress, user.DsEnvironment);
                    newAddress.DSAddressId = addressId;
                }

                var response = new
                {
                    address = newAddress
                };

                return new JsonResult { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        [Authorize]
        [HttpGet]
        public ActionResult AjaxUpdateDeliveryAddress(Client client,  long addressId, long subscriptionId)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var error = string.Empty;
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    error = "No user was found with the provided User name.";
                    return new JsonResult { Data = new { error = error }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                var dsEnviroment = DetermineDSEnvironment(user, client);
                var subscriptions = service.AccountService.GetActiveAccountSubscriptions(user.AccountNumber, dsEnviroment);
                var sub = subscriptions.SingleOrDefault(x => x.Id == subscriptionId);
                if (sub == null)
                {
                    error = "Subscription not found.";
                    return new JsonResult { Data = new { error = error }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                    
                sub.DeliveryAddress = service.AccountService.GetUserAddressById(addressId, dsEnviroment);
                service.AccountService.UpdateAccountSubscription(dsEnviroment, sub);
                return new JsonResult { Data = new { error = string.Empty }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }
    }
}