﻿using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;
using Jhm.Web.UI.Models.Shop;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Shop = Jhm.Web.UI.Models.Shop;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.Modules.ECommerce;
using System.Dynamic;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class ShopController : BaseController<IServiceCollection>
    {        
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ShopController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)        
        {
        }

        // GET: Shop
        public ActionResult Index()
        {
            return View();
        }

        private HeaderModel GetHeaderModelFromSession(string siteMode)
        {
            dynamic siteModeModel = Session[MagicStringEliminator.SessionKeys.SiteMenu];

            // Header model is reloaded if it has never been loaded or if the sitemode changes.
            if (siteModeModel == null || (siteModeModel != null && siteModeModel.SiteMode != siteMode))
            {                
                var model = new HeaderModel();
                LoadFromJson(model, siteMode);

                siteModeModel = new ExpandoObject();
                siteModeModel.HeaderModel = model;
                siteModeModel.SiteMode = siteMode;

                Session[MagicStringEliminator.SessionKeys.SiteMenu] = siteModeModel;
            }

            return siteModeModel.HeaderModel;
        }

        public ActionResult BuildHeader(string siteMode = "JHM")
        {
            return (PartialViewResult)TryOnEvent(() =>
            {
                return PartialView("_Header", GetHeaderModelFromSession(siteMode));
            });
        }

        public ActionResult BuildSideMenuMenu(string siteMode = "JHM")
        {
            return (PartialViewResult)TryOnEvent(() =>
            {
                //var headerModel = new HeaderModel();
                //LoadFromJson(headerModel, siteMode);

                return PartialView("_SideMenuMenu", GetHeaderModelFromSession(siteMode));
            });
        }

        public ActionResult BuildSlider(string siteMode = "JHM")
        {
            return (PartialViewResult)TryOnEvent(() =>
            {
                 dynamic session = Session[MagicStringEliminator.SessionKeys.SiteMenu];
                 if (session != null)
                 {
                     siteMode = session.SiteMode;
                 }
                return PartialView("_Slider", GetSliderModel(siteMode));
            });
        }

        public ActionResult Featured(Client client)
        {
            return (ViewResult)TryOnEvent(() =>
            {
                int totalResults = 0;
                var noPaginationPagingInfo = new PagingInfo
                {
                    CurrentPage = 1,
                    PageSize = 8,
                    TotalResults = 1000
                };

                IEnumerable<IProduct> products = new List<IProduct>();
                products = service.CatalogService.GetFeaturedItems(client.Company.DonorStudioEnvironment, noPaginationPagingInfo, ref totalResults);

                var model = GetListModel(products, null, totalResults);

                return View(model);
            });
        }

        public ActionResult Categories(Client client, string id, string orderDirection = "asc")
        {
            return (ViewResult)TryOnEvent(() =>
            {
                List<GroupViewModel> categories = new List<GroupViewModel>();
                var categoriesFile = ConfigurationManager.AppSettings["CategoriesFile"];
                try
                {
                    categories = FillGroup(categoriesFile);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }
                if (string.IsNullOrWhiteSpace(id))
                {
                    return View(categories);
                }

                var category = categories.Find(c => c.Code.Equals(id, StringComparison.InvariantCultureIgnoreCase));

                if (category == null)
                {
                    log.Error("Unable to find a category item of code [{0}]".FormatWith(id));
                    throw new ArgumentException(string.Format("Invalid argument:{0}", id));
                }

                int totalResults = 0;
                var noPaginationPagingInfo = new PagingInfo
                {
                    CurrentPage = 1,
                    PageSize = 1000,
                    TotalResults = 1000
                };

                IEnumerable<IProduct> products = new List<IProduct>();
                products = service.CatalogService.GetProductsByCategoryOrderedBy(client.Company.DonorStudioEnvironment, id, noPaginationPagingInfo, ref totalResults, orderDirection);

                var model = GetListModel(products, category, totalResults);

                return View("Details", model);
            });
        }

        public ActionResult Authors(Client client, string id, string orderDirection = "asc")
        {
            return (ViewResult)TryOnEvent(() =>
            {
                List<GroupViewModel> authors = new List<GroupViewModel>();
                var authorsFile = ConfigurationManager.AppSettings["AuthorsFile"];
                try
                {
                    authors = FillGroup(authorsFile);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }

                if (string.IsNullOrWhiteSpace(id))
                {
                    return View(authors);
                }

                var author = authors.Find(c => c.Code.Equals(id, StringComparison.InvariantCultureIgnoreCase));

                if (author == null)
                {
                    log.Error("Unable to find an author item of code [{0}]".FormatWith(id));
                    throw new ArgumentException(string.Format("Invalid argument:{0}", id));
                }

                int totalResults = 0;
                var noPaginationPagingInfo = new PagingInfo
                {
                    CurrentPage = 1,
                    PageSize = 1000,
                    TotalResults = 1000
                };

                IEnumerable<IProduct> products = new List<IProduct>();
                products = service.CatalogService.GetProductsByAuthorOrderedBy(client.Company.DonorStudioEnvironment, id, noPaginationPagingInfo, ref totalResults, orderDirection);

                var model = GetListModel(products, author, totalResults);

                return View("Details", model);
            });
        }

        public ActionResult Subjects(Client client, string id, string orderDirection = "asc")
        {
            return (ViewResult)TryOnEvent(() =>
            {
                List<GroupViewModel> subjects = new List<GroupViewModel>();
                var subjectsFile = ConfigurationManager.AppSettings["SubjectsFile"];
                try
                {
                    subjects = FillGroup(subjectsFile);
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message);
                }

                if (string.IsNullOrWhiteSpace(id))
                {
                    return View(subjects);
                }

                var subject = subjects.Find(c => c.Code.Equals(id, StringComparison.InvariantCultureIgnoreCase));

                if (subject == null)
                {
                    log.Error("Unable to find a subject item of code [{0}]".FormatWith(id));
                    throw new ArgumentException(string.Format("Invalid argument:{0}", id));
                }

                int totalResults = 0;
                var noPaginationPagingInfo = new PagingInfo
                {
                    CurrentPage = 1,
                    PageSize = 1000,
                    TotalResults = 1000
                };

                IEnumerable<IProduct> products = new List<IProduct>();

                products = service.CatalogService.GetProductsBySubjectOrderedBy(client.Company.DonorStudioEnvironment, id, noPaginationPagingInfo, ref totalResults, orderDirection);
                var model = GetListModel(products, subject, totalResults);

                return View("Details", model);
            });
        }

        public ActionResult Search(Client client, string queryString, int page = 1)
        {
            //SetupDeliveryDeadlineMessage();
            //ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;

            // also search active Donations - 5/15/2015  JFE
            var allDonations = new Jhm.Web.Repository.Modules.Account.MockDonationRepository();

            var activeDonations = allDonations.GetActiveDonationOpportunities();
            List<Donation> matchingDonations = new List<Donation>();
            if (!queryString.IsNullOrEmpty())
            {
                foreach (var d in activeDonations)
                {
                    if (d.Description.Contains(queryString) || d.Title.Contains(queryString) ||
                        d.Body.Contains(queryString))
                    {
                        matchingDonations.Add(d);
                    }
                }
            }
            Session["MatchingDonations"] = matchingDonations;

            return TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.FindProducts(client.Company.DonorStudioEnvironment, queryString, client.PagingInfo, ref totalResults);
                client.PagingInfo.TotalResults = totalResults;
                client.PagingInfo.CurrentActionName = MagicStringEliminator.CatalogActions.Search;
                client.PagingInfo.CurrentControllerName = MagicStringEliminator.Controller.Catalog;

                var model = ProductListingViewModel.With(null, null, null, null, queryString, products, client.PagingInfo);
                model.ProductListingHeader = "Search For: {0}".FormatWith(queryString);
                model.RegisterClient(client);
                return View("Search", model);
            });
        }

        [HttpGet]
        public ActionResult SearchAjax(Client client, string queryString, int page)
        {
            return (JsonResult)TryOnEvent(() =>
            {
                var totalResults = client.PagingInfo.TotalResults;
                client.PagingInfo.CurrentPage = page;
                var products = service.CatalogService.FindProducts(client.Company.DonorStudioEnvironment, queryString, client.PagingInfo, ref totalResults);
                return new JsonResult { Data = new { products = products }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            });
        }

        #region Helpers

        private GroupProductListViewModel GetListModel(IEnumerable<IProduct> productList, GroupViewModel group, int totalResults)
        {
            var listOfProducts = new List<GroupProductViewModel>();

            foreach (var p in productList)
            {
                var groupProductVm = new GroupProductViewModel
                {
                    Title     = p.Title,
                    Author    = p.AuthorDescription,
                    Sku       = p.Code
                };

                if (p.AlternativeImages != null && p.AlternativeImages.Any())
                    groupProductVm.ImagePath = Url.AbsoluteContent(p.AlternativeImages.First());

                listOfProducts.Add(groupProductVm);
            }

            var model = new GroupProductListViewModel
            {
                Name = group == null ? string.Empty : group.DisplayName,
                TotalResults = totalResults,
                Products = listOfProducts
            };

            return model;
        }

        private List<GroupViewModel> FillGroup(string fileName)
        {
            var groupItems = new List<GroupViewModel>();
            using (StreamReader r = new StreamReader(Server.MapPath(fileName)))
            {
                string json = r.ReadToEnd();
                dynamic array = JsonConvert.DeserializeObject(json);

                foreach (var item in array)
                {
                    var s = new GroupViewModel { Code = item.Value, DisplayName = item.DisplayName, GroupName = this.ControllerContext.RouteData.Values["action"].ToString() };
                    groupItems.Add(s);
                }
            }
            return groupItems;
        }

        // ToDo: Create a new object and map its properties to use strong type deserialization.
        private void LoadFromJson(HeaderModel model, string siteMode)
        {
            var fileName = System.Configuration.ConfigurationManager.AppSettings["MenuFile"];
            var baseItemUrlkey = string.Format("MenuItemsBaseUrl-{0}", siteMode);
            var menuItemBaseUrl = System.Configuration.ConfigurationManager.AppSettings[baseItemUrlkey];

            try
            {
                using (StreamReader r = new StreamReader(Server.MapPath(fileName)))
                {
                    string json = r.ReadToEnd();
                    dynamic array = JsonConvert.DeserializeObject(json);

                    // Menu
                    var menus = FindSiteMenu(array[0].sitemenus, siteMode);
                    foreach (var menu in menus)
                    {
                        var children = GetChildren(menu.Value.children, menuItemBaseUrl);
                        var itemUrl = GetItemUrl(menu, menuItemBaseUrl);
                        var menuItem = new Shop.MenuItem
                        {
                            Title = menu.Value.title,
                            Href = itemUrl,
                            Children = children
                        };
                        model.TopLevelMenuItems.Add(menuItem);
                    }

                    // Dropdown
                    var logos = FindSiteLogos(array[1].sitelogos, siteMode);
                    model.MainLogo = LoadMainLogo(logos, menuItemBaseUrl);

                    foreach (var logo in logos)
                    {
                        if (logo.Value.displayorder != 1)
                        {
                            var logoItem = new Shop.LogoModel
                            {
                                Title = logo.Value.title,
                                Description = logo.Value.description,
                                ImageUrl = logo.Value.imageurl,
                                DestinationUrl = logo.Value.destinationurl,
                                DisplayOrder = logo.Value.displayorder
                            };
                            model.Logos.Add(logoItem);
                        }
                    }
                    model.Logos.OrderBy(l => l.DisplayOrder);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
        }

        private object GetItemUrl(dynamic menu, string menuItemBaseUrl)
        {

            var url = string.Empty;
            if (menu.Value != null && menu.Value.absoluteUrl != null)
            {
                url = menu.Value.absoluteUrl;
            }
            else if (menu.Value == null)
            {
                url = string.Concat(menuItemBaseUrl, menu.href);
            }
            else            
            {
                url = string.Concat(menuItemBaseUrl, menu.Value.href);
            }

            return url;
        }        

        private dynamic FindSiteMenu(dynamic sitemenus, string siteMode)
        {
            foreach (var sm in sitemenus)
            {
                if (siteMode.Equals(sm.sitemode.Value, StringComparison.InvariantCultureIgnoreCase))
                {
                    return sm.menu;
                }
            }

            return null;
        }

        private LogoModel LoadMainLogo(dynamic sitelogos, string menuItemBaseUrl)
        {
            var mainLogo = new LogoModel();

            foreach (var logo in sitelogos)
            {
                if (logo.Value.displayorder == 1)
                {
                    mainLogo = new Shop.LogoModel
                    {
                        Title = logo.Value.title,
                        Description = logo.Value.description,
                        ImageUrl = logo.Value.imageurl,
                        DestinationUrl = string.Concat(menuItemBaseUrl, logo.Value.destinationurl),
                        DisplayOrder = logo.Value.displayorder
                    };

                    break;
                }
            }

            return mainLogo;
        }

        private dynamic FindSiteLogos(dynamic sitelogos, string siteMode)
        {
            foreach (var sl in sitelogos)
            {
                if (siteMode.Equals(sl.sitemode.Value, StringComparison.InvariantCultureIgnoreCase))
                {
                    return sl.logo;
                }
            }

            return null;
        }

        private IEnumerable<Shop.MenuItem> GetChildren(dynamic childs, string menuItemBaseUrl)
        {
            var menuItems = new List<Shop.MenuItem>();            
            foreach (var menu in childs)
            {
                var itemUrl = GetItemUrl(menu, menuItemBaseUrl);
                var menuItem = new Shop.MenuItem
                {
                    Title = menu.title,
                    Href = itemUrl
                };
                menuItems.Add(menuItem);
            }

            return menuItems;
        }

        private List<SliderViewModel> GetSliderModel(string siteMode)
        {
            var model = new List<SliderViewModel>();
            var fileName = System.Configuration.ConfigurationManager.AppSettings["SliderFile"];
            try
            {
                using (StreamReader r = new StreamReader(Server.MapPath(fileName)))
                {
                    string json = r.ReadToEnd();
                    dynamic array = JsonConvert.DeserializeObject(json);

                    foreach (var item in array)
                    {
                        if (item.sitemode == siteMode)
                        {
                            foreach (var s in item.sliders)
                            {
                                var slider = new SliderViewModel
                                {
                                    BackgroundImage = s.backgroundImage,
                                    Title = s.title,
                                    Subtitle = s.subtitle,
                                    Description = s.description,
                                    Destinationurl = s.destinationUrl,
                                    ButtonText = s.buttonText
                                };
                                model.Add(slider);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }

            return model;           
        }

        #endregion
    }

}