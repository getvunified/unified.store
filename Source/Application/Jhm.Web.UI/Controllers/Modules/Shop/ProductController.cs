﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;
using Jhm.Web.UI.Models;
using Jhm.Web.UI.Models.Shop;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.Modules.ECommerce;
using log4net;

namespace Jhm.Web.UI.Controllers.Modules.Shop
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class ProductController : BaseController<IServiceCollection>
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProductController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ActionResult Detail(Client client, string id, string group)
        {
            return (ViewResult)TryOnEvent(() =>
            {
                ViewBag.CartNotificationDelayInSeconds = System.Configuration.ConfigurationManager.AppSettings["CartNotificationDelayInSeconds"];

                var product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, id);
                if (product.IsNull())
                {
                    log.Error("Unable to find a stock item of code [{0}]".FormatWith(id));
                    TempData[MagicStringEliminator.Messages.ErrorMessage] = "Product Was Missing From Our Inventory, Please Select A New Product.";
                    return RedirectToRoute(MagicStringEliminator.Routes.Catalog);
                }

                foreach(var relatedItem in product.RelatedItems)
                {
                    var productRelated = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, relatedItem.Code);
                    if (productRelated != null)
                    {
                        relatedItem.Title = productRelated.Title;
                        relatedItem.Code = relatedItem.Code + "|" + productRelated.AuthorDescription;

                        if (productRelated.AlternativeImages != null && productRelated.AlternativeImages.Any())
                            relatedItem.Image = Url.AbsoluteContent(productRelated.AlternativeImages.First());

                        //relatedItem.Image = productRelated.AlternativeImages != null && productRelated.AlternativeImages.Count() > 0 && !string.IsNullOrWhiteSpace(productRelated.AlternativeImages.First()) ? productRelated.AlternativeImages.First() : string.Empty;
                    }

                }

                var model = CatalogProductViewModel.With(product);
                model.RegisterClient(client);

                ViewBag.GroupName = group;
                ViewBag.Parts = null;

                return MapCartRenderView(client, "Detail", model);
            });
        }

        public ActionResult AddToCart(Client client, string sku, string mediaType, int quantity)
        {
            return TryOnEvent(() =>
            {
                var product = service.CatalogService.GetProduct(client.Company.DonorStudioEnvironment, sku);
                if (product.IsNull())
                {
                    log.Error("Unable to find a stock item of code [{0}]".FormatWith(sku));                    
                    return RedirectToAction("Index", "Cart");
                }
                var mType = MediaType.All.FirstOrDefault(x => x.CodeSuffix == mediaType);
                if (mType.IsNot().Null())
                {
                    var stockItem = product.StockItems.First(x => x.MediaType == mType);
                    if (stockItem.IsNot().Null())
                    {
                        stockItem.SetImageLocation(product.AlternativeImages.FirstOrDefault());
                        stockItem.SetAuthorDescription(product.AuthorDescription);
                        client.Cart.AddItem(stockItem, quantity);
                        service.DiscountCodeService.RecalculateAppliedDiscount(client);

                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                }

                return Json("Unable to find a stock item of MediaType [{0}]".FormatWith(mediaType), JsonRequestBehavior.AllowGet);
                //return RedirectToCartPage("Unable to find a stock item of MediaType [{0}]".FormatWith(mediaType));
            });
        }
    }
}