﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Foundation;
using Getv.Security;
using Jhm.Common.Utilities.HTMLHelpers;
using Jhm.Web.UI.Models;

namespace Jhm.Web.UI.Controllers
{
	public partial class AccountController
	{
        private static string AuthenticationStatusToPropertyName(SecurityErrorCode createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.

            var model = new RegisterModel();
            switch (createStatus)
            {
                case SecurityErrorCode.DuplicateUsername:
                    return MyHtmlHelper.ExposeProperty(() => model.UserName);


                case SecurityErrorCode.EmailAddressCannotBeEmpty:
                    return MyHtmlHelper.ExposeProperty(() => model.Email);

                case SecurityErrorCode.NameCannotBeEmpty:
                    return MyHtmlHelper.ExposeProperty(() => model.FirstName);

                case SecurityErrorCode.UsernameCannotBeEmpty:
                    return MyHtmlHelper.ExposeProperty(() => model.UserName);

                case SecurityErrorCode.UsernameIsTooLong:
                    return MyHtmlHelper.ExposeProperty(() => model.UserName);

                case SecurityErrorCode.PasswordDoesNotContainMinimumRequiredNonAlphanumericCharacters:
                    return MyHtmlHelper.ExposeProperty(() => model.Password);

                case SecurityErrorCode.PasswordTooWeak:
                    return MyHtmlHelper.ExposeProperty(() => model.Password);

                case SecurityErrorCode.PasswordTooShort:
                    return MyHtmlHelper.ExposeProperty(() => model.Password);

                case SecurityErrorCode.EmailAddressIsTooLong:
                    return MyHtmlHelper.ExposeProperty(() => model.Email);

                case SecurityErrorCode.DuplicateEmailAddress:
                    return MyHtmlHelper.ExposeProperty(() => model.Email);

                case SecurityErrorCode.EmailAddressIsInvalid:
                    return MyHtmlHelper.ExposeProperty(() => model.Email);

                case SecurityErrorCode.NameIsTooLong:
                    return MyHtmlHelper.ExposeProperty(() => model.FirstName);

                case SecurityErrorCode.Unauthorized:
                    return MyHtmlHelper.ExposeProperty(() => model.Password);
                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        private string AuthenticationStatusToUserMessage(SecurityErrorCode securityErrorCode)
        {
            var message = securityErrorCode.SplitIntoWords();
            switch (securityErrorCode)
            {
                case SecurityErrorCode.UsernameContainsInvalidCharacters:
                    message = "No Spaces and No Dashes allowed on Username";
                    break;
                case SecurityErrorCode.InvalidPassword:
                    break;
                case SecurityErrorCode.InvalidQuestion:
                    break;
                case SecurityErrorCode.InvalidAnswer:
                    break;
                case SecurityErrorCode.EmailAddressIsInvalid:
                    break;
                case SecurityErrorCode.DuplicateUsername:
                    message = "You or someone else already have this username in an existing account.";
                    break;
                case SecurityErrorCode.DuplicateEmailAddress:
                    message = "<p>You already have this email in an existing account. <br/> <a href=" + Url.Action("ForgotUsername") + " target=_blank>Click here to recover your username</a>.  Then, <br/><a href=" + Url.Action("RequestPasswordChange") + " target=_blank>Click here to reset your password</a>, and finally<br> <a href=" + Url.Action("LogOn") + ">Click here to log in</a> successfully.</p>";
                    break;
                case SecurityErrorCode.UserRejected:
                    break;
                case SecurityErrorCode.InvalidProviderUserKey:
                    break;
                case SecurityErrorCode.DuplicateProviderUserKey:
                    break;
                case SecurityErrorCode.ProviderError:
                    break;
                case SecurityErrorCode.EmailAddressChangeCancelled:
                    break;
                case SecurityErrorCode.PasswordChangeCancelled:
                    break;
                case SecurityErrorCode.UsernameCannotBeEmpty:
                    break;
                case SecurityErrorCode.UsernameIsTooLong:
                    break;
                case SecurityErrorCode.EmailAddressCannotBeEmpty:
                    break;
                case SecurityErrorCode.EmailAddressIsTooLong:
                    break;
                case SecurityErrorCode.NameCannotBeEmpty:
                    break;
                case SecurityErrorCode.NameIsTooLong:
                    break;
                case SecurityErrorCode.NameContainsInvalidCharacters:
                    message = "No Spaces and No Dashes allowed";
                    break;
                case SecurityErrorCode.UsernameDoesNotExist:
                    break;
                case SecurityErrorCode.Failure:
                    break;
                case SecurityErrorCode.InvalidRequest:
                    break;
                case SecurityErrorCode.PasswordDoesNotContainMinimumRequiredNonAlphanumericCharacters:
                    break;
                case SecurityErrorCode.PasswordTooShort:
                    break;
                case SecurityErrorCode.PasswordTooWeak:
                    break;
                case SecurityErrorCode.RequiredValuesCannotBeEmpty:
                    break;
                case SecurityErrorCode.Unauthorized:
                    message = "The user name or password provided is incorrect.";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("securityErrorCode");
            }
            return message;
        }
	}
}