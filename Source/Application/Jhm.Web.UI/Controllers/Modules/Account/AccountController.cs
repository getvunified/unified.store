﻿using System;
using System.Linq;
using System.Security;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using Foundation;
using Getv.Security;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Common.Utilities.HTMLHelpers;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce.ViewModels;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using System.Collections.Specialized;
using System.Text;
using System.Collections.Generic;
using Jhm.em3.Core;
using getv.donorstudio.core.Global;
using MagicStringEliminator = Jhm.Web.Core.MagicStringEliminator;
using StringExtensions = Foundation.StringExtensions;
using log4net;


namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("page-template", "sidebar-second")]
    //[RequireHttps(RequireSecure = true)]
    public partial class AccountController : BaseController<IServiceCollection>
    {
        private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static bool IsAuthenticated
        {
            get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        public AccountController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        #region Authentication Management

        // **************************************
        // URL: /Account/LogOn
        // **************************************

        public ActionResult LogOn(Client client)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.SignIn;
            ViewData["ReturnUrl"] = Request.QueryString["ReturnUrl"] ?? String.Empty;
            return View(new LogOnModel
            {
                LightCart = client.Cart, 
                IsDifferenceMediaPage = IsDM
            });
        }

        [HttpPost]
        public ActionResult LogOn(Client client, LogOnModel model, string returnUrl)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.SignIn;

            return TryOnEventWithoutTransaction(() =>
            {
                model.IsDifferenceMediaPage = IsDM;
                if (!String.IsNullOrEmpty(model.UserName))
                {
                    model.UserName = model.UserName.Trim();
                    if (model.UserName.Contains(" "))
                    {
                        ModelState.AddModelError("UserName",
                                                "Username cannot contain spaces. Please remove the spaces and try again");
                    }
                }

                if (ModelState.IsValid)
                {
                    model.Password = model.Password.Trim();
                    
                    var result = service.MembershipService.ValidateSecurityUser(model.UserName,model.Password);
                    
                    if (result.IsSuccessful)
                    {
                        model.GlobalUserId = service.SecurityService.LoadUserByUsername(model.UserName).Data.Id;
                        ActionResult action = new EmptyResult();
                        TryWithTransaction(() =>
                        {
                            action = DoSecurityUserLogon(model, client);
                        });
                        if (action != null)
                        {
                            return action;
                        }

                        if (!String.IsNullOrEmpty(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError(AuthenticationStatusToPropertyName((SecurityErrorCode)result.Error.ErrorCode), AuthenticationStatusToUserMessage((SecurityErrorCode)result.Error.ErrorCode));
                    }
                }

                // If we got this far, something failed, redisplay form
                return MapCartRenderView(client, "Logon", model);
            });
        }

        public ActionResult AjaxLogOn(LogOnModel model)
        {
            if (String.IsNullOrEmpty(model.UserName))
            {
                return new RenderJsonResult { Result = new { success = false, message = "Username is required" } };
            }

            if (String.IsNullOrEmpty(model.Password))
            {
                return new RenderJsonResult { Result = new { success = false, message = "Password is required" } };
            }

            return TryOnEventWithoutTransaction(() =>
            {
                var shadowBoxPassword = model.Password.Trim();

                var logOn = service.MembershipService.ValidateSecurityUser(model.UserName, model.Password.Trim());

                if (logOn.IsSuccessful)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    return new RenderJsonResult { Result = new { success = true } };
                }

                var friendlyMessage = AuthenticationStatusToUserMessage((SecurityErrorCode)logOn.Error.ErrorCode);
                return new RenderJsonResult { Result = new { success = false, message = friendlyMessage } };
            });
        }


        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {
            service.FormsService.SignOut();
            ClearRegisteredClient();
            SessionManager.ClearCurrentUser();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }

        // **************************************
        // URL: /Account/Register
        // **************************************

        //[HttpGet]
        //public ActionResult Register(Client client, string returnUrl = null)
        //{
        //    ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
        //    ViewData["PasswordLength"] = service.MembershipService.MinPasswordLength;
        //    ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.Register;

        //    return View(new RegisterModel
        //    {
        //        LightCart = client.Cart, 
        //        NewsLetterOptIn = true,
        //        IsDifferenceMediaPage = IsDM
        //    });
        //}

        //[HttpPost]
        //public ActionResult Register(Client client, RegisterModel model, string returnUrl)
        //{
        //    MembershipCreateStatus createStatus = MembershipCreateStatus.ProviderError;
        //    model.Gender = model.GenderRadioButtonList.SelectedValue;
        //    model.IsDifferenceMediaPage = IsDM;
        //    return TryOnEventWithoutTransaction(() =>
        //    {
        //        /*var countries = Country.GetAll<Country>().ToList();
        //        var country = countries.Find(x => x.Iso == model.Country);
        //        if (country == null)
        //        {
        //            ModelState.AddModelError("Country", "Country not found");
        //        }
        //        else if (!Core.CountryListHelper.CountriesWithoutZipCodes.Contains(country.Iso3) && String.IsNullOrEmpty(model.Zip))
        //        {
        //            ModelState.AddModelError("Zip",StringExtensions.FormatWith("Postal Code code is required in {0}", country.DisplayName));
        //        }*/

        //        if (!EmailAddressIsValid(model.Email))
        //        {
        //            ModelState.AddModelError("Email",StringExtensions.FormatWith("The provided email address is not valid: {0}", model.Email));
        //        }

        //        if (model.Gender == Gender.None)
        //        {
        //            ModelState.AddModelError("Gender", "Please select a gender");
        //        }

        //        if (!String.IsNullOrEmpty(model.UserName))
        //        {
        //            model.UserName = model.UserName.Trim();
        //            if (model.UserName.Contains(" "))
        //            {
        //                ModelState.AddModelError("UserName","Username cannot contain spaces. Please remove the spaces and try again");
        //            }
        //        }
        //        if (!String.IsNullOrEmpty(model.Password))
        //            model.Password = model.Password.Trim();

        //        if (model.NewsLetterOptIn)
        //        {
        //            if (!StringExtensions.IsNullOrEmpty(model.FirstName) && !StringExtensions.IsNullOrEmpty(model.LastName) && !StringExtensions.IsNullOrEmpty(model.Email))
        //            {
        //                var data = new ENewsLetterSignUpViewModel
        //                {
        //                    Address1 = model.Address1,
        //                    Address2 = model.Address2,
        //                    City = model.City,
        //                    CompanyName = model.Company,
        //                    State = model.State,
        //                    Country = model.Country,
        //                    Zip = model.Zip,
        //                    Email = model.Email,
        //                    FirstName = model.FirstName,
        //                    LastName = model.LastName,
        //                    AccountNumber = 0,
        //                    EmailListCode = "WUP"
        //                };




        //                try
        //                {
        //                    EMailSignUp(data);
        //                }
        //                catch (Exception ex)
        //                {
        //                    // Came up due to the database not being accessible..  
        //                    Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
        //                }


        //            }
        //        }

        //        if ( model.PhoneAreaCode == "Area code" ) model.PhoneAreaCode = String.Empty;
        //        if (model.PhoneNumber == "Phone number") model.PhoneNumber = String.Empty;

        //        var addressIsValid = ValidateAddress(model);

        //        if (ModelState.IsValid)
        //        {
        //            var result = service.SecurityService.CreateUser(model.UserName, model.Password, model.Email, model.FirstName, model.LastName, model.NewsLetterOptIn);
        //            if (result.IsSuccessful)
        //            {
        //                ApiMessage<Getv.Security.IUser> globalUser = null;
        //                globalUser = service.SecurityService.LoadUserByUsername(model.UserName);
        //                TryWithTransaction(() =>
        //                {
        //                    createStatus = service.MembershipService.CreateUserWithGlobalId(globalUser.Data.Id, model.UserName, model.Password, model.Email, "PasswordQuestion", "PasswordAnswer");
        //                });

        //                if (createStatus == MembershipCreateStatus.Success)
        //                {
        //                    TryWithTransaction(() =>
        //                    {
        //                        var user = service.AccountService.GetUser(model.UserName,GetApplicationName());
        //                        if (user.IsTransient())
        //                        {
        //                            throw new SecurityException("Username Does Not Exist");
        //                        }

        //                        model.MapToUser(user);
        //                        user.AccountNumber = 0;
        //                        user.DsEnvironment = client.Company.DonorStudioEnvironment;

        //                        if (addressIsValid)
        //                        {
        //                            var dsAccountNumber = service.DsServiceCollection.DsAccountTransactionService.CreateAccount(client.Company.DonorStudioEnvironment, DsAccountMapper.Map(user));
        //                            var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(client.Company.DonorStudioEnvironment, dsAccountNumber).AddressId;
        //                            user.AccountNumber = dsAccountNumber;
        //                            user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
        //                        }
        //                        else
        //                        {
        //                            user.Profile.PrimaryAddress = null;
        //                        }

        //                        user.Profile.ApplicationName = GetApplicationName();
        //                        service.AccountService.SaveUser(user);
        //                    });
        //                    service.FormsService.SignIn(model.UserName, false/* createPersistentCookie */);
        //                    SendAccountCreatedEmail(model);
        //                    var redirectRoute =Session[MagicStringEliminator.SessionKeys.RedirectTo];
        //                    if (redirectRoute != null)
        //                    {
        //                        Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
        //                        return RedirectToRoute(redirectRoute);
        //                    }
        //                    if (!String.IsNullOrEmpty(returnUrl))
        //                    {
        //                        return Redirect(returnUrl);
        //                    }
        //                    return RedirectToAction("Index", "Home");
        //                }
        //                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(AuthenticationStatusToPropertyName((SecurityErrorCode)result.Error.ErrorCode), AuthenticationStatusToUserMessage((SecurityErrorCode)result.Error.ErrorCode));
        //            }
        //            // If we got this far, something failed, redisplay form
        //            return View(ReturnToViewWithError(model, client));
        //        }
                
        //        ModelState.AddModelError("",AccountValidation.ErrorCodeToString(createStatus));

        //        // If we got this far, something failed, redisplay form
        //        return View(ReturnToViewWithError(model, client));
        //    });
        //}

        private bool ValidateAddress(RegisterModel model)
        {
            if (String.IsNullOrEmpty(model.Address1))
            {
                return false;
            }

            bool isValid = true;
            if ( String.IsNullOrEmpty(model.City))
            {
                ModelState.AddModelError("City", "If Street Address was provided you must enter a City");
                isValid = false;
            }

            var countries = Country.GetAll<Country>().ToList();
            var country = countries.Find(x => x.Iso == model.Country);
            if (country == null)
            {
                ModelState.AddModelError("Country", "If Street Address was provided you must enter a Country, Country not found");
                isValid = false;
            }
            else if (!Enumerable.Contains(Core.CountryListHelper.CountriesWithoutZipCodes, country.Iso3) && String.IsNullOrEmpty(model.Zip))
            {
                ModelState.AddModelError("Zip", StringExtensions.FormatWith("If Street Address was provided you must enter a Postal Code, Postal Code code is required in {0}", country.DisplayName));
                isValid = false;
            }

            if (String.IsNullOrEmpty(model.State))
            {
                ModelState.AddModelError("State", "If Street Address was provided you must enter a State");
                isValid = false;
            }

            return isValid;
        }

        private bool EmailAddressIsValid(string emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                return false;
            }
            //@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$"
            return Regex.IsMatch(emailAddress,@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$");
        }

        private RegisterModel ReturnToViewWithError(RegisterModel model, Client client)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.Register;
            ViewData["PasswordLength"] = service.MembershipService.MinPasswordLength;
            model.MapCart(client.Cart,true);
            return model;
        }


        private void SendAccountCreatedEmail(RegisterModel model)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath(IsDM ? "/EmailTemplates/DifferenceMedia/NewAccount.html" : "/EmailTemplates/generic.html"));

                var sb = new StringBuilder();
                if (!IsDM)
                {
                    sb.Append("<h3>New JHM Account Created</h3>");
                    sb.AppendLine("<br />");
                    sb.AppendFormat(
                        @"
                    <p>
                        Thank you for visiting the John Hagee Ministries website. This email indicates that
                        you now have an account with our website. In the near future, when you return to
                        www.jhm.org you can now log in with your special user name and password to do such
                        things as: the track your orders, make a wish list, share the wish list with your
                        friends and family, update your contact information and much more.
                    </p>
                    <p>
                        Your user name is: {0}<br />
                    </p>
                    <p>    To login using your user name and password click on the link below:<br />
                        <a href='{1}'>{1}</a><br />
                    </p>
                    <p>
                        We thank you for your continued prayers that we may share “All the gospel to all
                        the world and to all generations” through the wonderful teachings of Pastor John
                        Hagee and Pastor Matthew Hagee.
                    </p>
                    <p>
                        We pray the Lord bless you, your family and those you love,<br />
                        John Hagee Ministries
                    </p>
                    ",
                        model.UserName, FullVirtualWebSitePath + Url.Action("LogOn"));
                }

                var replacements = new ListDictionary
                {
                    {"<%EMAIL%>", model.Email},
                    {"<%WEBSITE_URL%>", FullVirtualWebSitePath},
                    {"<%LOGON_URL%>", FullVirtualWebSitePath + Url.Action("LogOn")},
                    {"<%USERNAME%>", model.UserName},
                    {"<%IMAGES_PATH%>", FullVirtualWebSitePath + (IsDM? Url.Content("/EmailTemplates/DifferenceMedia/images/"): Url.Content("/EmailTemplates/"))},
                    {"<%CONTENT%>", sb.ToString()}
                };


                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = IsDM ? "Difference Media - New Account Created" : "John Hagee Ministries - New Account Created"
                };

                var msg = md.CreateMailMessage(model.Email, replacements, body, new System.Web.UI.Control());
                // Sending email to client
                if (IsDM)
                {
                    service.EmailService.SendDMEmail(msg);
                }
                else
                {
                    service.EmailService.SendEmail(msg);
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
            }
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************

        public ActionResult ChangePassword(Client client)
        {
            var resetToken = Session["ResetToken"] as string;
            if (String.IsNullOrWhiteSpace(resetToken))
            {
                return RedirectToAction("RequestPasswordChange");
            }
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangePassword;
            ViewData["PasswordLength"] = service.MembershipService.MinPasswordLength;
            return View(new ChangePasswordModel { LightCart = client.Cart });
        }

        [HttpPost]
        public ActionResult ChangePassword(Client client, ChangePasswordModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangePassword;
            ViewData["PasswordLength"] = service.MembershipService.MinPasswordLength;
            return TryOnEvent(() =>
                                  {
                                      var resetToken = Session["ResetToken"] as string;
                                      if (!String.IsNullOrWhiteSpace(resetToken))
                                      {
                                          model.ResetToken = resetToken;
                                          Session.Remove("ResetToken");
                                      }
                                      if (ModelState.IsValid)
                                      {
                                          if (!String.IsNullOrEmpty(model.ResetToken))
                                          {

                                              var status = service.SecurityService.ConfirmPasswordReset(model.NewPassword, model.ResetToken);
                                              if (status.IsSuccessful)
                                              {
                                                  ViewData["Message"] = "New Password is Confirmed! Please login using your new password.";
                                                  //AddPopupMessage("New Password is Confirmed! Please login using your new password.", "Change Password");
                                                  if (Request.IsAuthenticated)
                                                  {
                                                      return RedirectToAction("MyAccount", "Account");
                                                  }
                                                  else
                                                  {
                                                      return RedirectToAction("LogOn", "Account");
                                                  }
                                              }
                                              ViewData["Message"] = "Could not confirm your password change: " + status.Error.UserMessage.SeparateCapitalizedWords();
                                              //AddPopupMessage("Could not confirm your password change: " + status.Error.UserMessage.SeparateCapitalizedWords(), "Change Password");
                                          }
                                          else if (SessionManager.SecurityUser != null)
                                          {
                                              var status = service.SecurityService.UpdatePassword(SessionManager.SecurityUser.Id, model.CurrentPassword,model.NewPassword);
                                              if (status.IsSuccessful)
                                              {
                                                  ViewData["Message"] = "Your password has been updated!";
                                                  //AddPopupMessage("Your password has been updated!", "Change Password");
                                              }
                                              else
                                              {
                                                  ViewData["Message"] = "Could not confirm your password change: " +status.Error.UserMessage.SeparateCapitalizedWords();
                                                  //AddPopupMessage("Could not confirm your password change: " +status.Error.UserMessage.SeparateCapitalizedWords(),"Change Password");
                                              }
                                              //ModelState.AddModelError("","The current password is incorrect or the new password is invalid.");
                                          }
                                          else
                                          {
                                              return RedirectToAction("RequestPasswordChange");
                                          }
                                      }
                                      return View(model);
                                      //if (ModelState.IsValid)
                                      //{
                                      //    model.OldPassword = model.OldPassword.Trim();
                                      //    model.NewPassword = model.NewPassword.Trim();
                                      //    if (service.MembershipService.ChangePassword(User.Identity.Name,
                                      //                                                 model.OldPassword,
                                      //                                                 model.NewPassword))
                                      //    {
                                      //        var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];
                                      //        if (redirectRoute != null)
                                      //        {
                                      //            Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                                      //            return RedirectToRoute(redirectRoute);
                                      //        }
                                      //        return RedirectToAction("ChangePasswordSuccess");
                                      //    }
                                      //    else
                                      //    {
                                      //        ModelState.AddModelError("",
                                      //                                 "The current password is incorrect or the new password is invalid.");
                                      //    }
                                      //}

                                      //// If we got this far, something failed, redisplay form
                                      //ViewData["PasswordLength"] = service.MembershipService.MinPasswordLength;
                                      //model.MapCart(client.Cart);
                                      //return View(model);
                                  });
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************
        [Authorize]
        public ActionResult ChangePasswordSuccess(Client client)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangePassword;
            return RedirectToAction("LogOn");
            /*return View("LogOn", new CartViewModel
            {
                Cart = client.Cart, 
                IsDifferenceMediaPage = IsDM
            });*/
        }

        // **************************************
        // URL: /Account/MyAccount
        // **************************************
        [Authorize]
        public ActionResult MyAccount(Client client)
        {
            return LoadAndRenderMyAccountView(client);
        }

        private ActionResult LoadAndRenderMyAccountView(Client client)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MyAccount;
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                ProfileViewModel model = null;
                if (user.AccountNumber > 0)
                {
                    var dsEnvironment = DetermineDSEnvironment(user, client);
                    var dsUser = service.AccountService.GetUser(user.Username, user.ApplicationName, dsEnvironment);
                    var userAddresses = service.AccountService.GetUserAddresses(user.Username, user.ApplicationName, dsEnvironment);
                    model = ProfileViewModel.With(dsUser, client);
                    model.Addresses = userAddresses.ToList();
                }
                //Note: Removed warning message since now we handle missing DS Account on Checkout as of 11/17/2012
                //else if (user.AccountNumber <= 0)
                //{
                //    TempData[MagicStringEliminator.Messages.Warning] =
                //        "Note: In order to continue using our services it is required that you provide us with a billing address. Please click on the 'Add New Address' button at the bottom to proceed.";
                //    model = ProfileViewModel.With(user, client);
                //}
                else
                {
                    //TempData[MagicStringEliminator.Messages.Warning] =
                    //    "Note: We experienced an unexpected failure in the system while your account was being created. As a result some information in your profile was not properly saved in our records. In order to correct this problem and to ensure a wonderful user experience in our website, we ask you to please review and update your account by submitting this form. Once you update your account we will fix all the problems encountered in your profile.";
                    model = ProfileViewModel.With(user, client);
                }
                var securityUser = SessionManager.SecurityUser;
                if (securityUser.Id.IsNotNullOrEmpty())
                {
                    model.FirstName = securityUser.FirstName;
                    model.LastName = securityUser.LastName;
                }

                model.MapCart(client.Cart,true);
                return View("MyAccount",model);
            });
        }


        [Authorize]
        [HttpPost]
        public ActionResult MyAccount(Client client, ProfileViewModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MyAccount;

            return TryOnEvent(() =>
            {
                model.UserName = SessionManager.SecurityUser.Username;

                if (ModelState.IsValid)
                {
                    var user = service.AccountService.GetUserByGlobalId(SessionManager.SecurityUser.Id);
                    if (user.AccountNumber > 0)
                    {
                        var dsEnvironment = DetermineDSEnvironment(user, client);
                        var userAddresses = service.AccountService.GetUserAddresses(user.Username, user.ApplicationName,
                                                                                    client.Company.
                                                                                        DonorStudioEnvironment);
                        model.Addresses = userAddresses.ToList();

                        user = service.AccountService.GetUser(user.Username, GetApplicationName(), dsEnvironment);
                        model.MapToUser(user);
                        service.AccountService.SaveUser(user, dsEnvironment: dsEnvironment);
                    }
                    else
                    {
                        model.MapToUser(user);
                        service.AccountService.SaveUser(user,false);
                    }

                    var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];

                    if (redirectRoute != null)
                    {
                        Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                        return RedirectToRoute(redirectRoute);
                    }

                    TempData[MagicStringEliminator.Messages.Status] =
                        "Account was successfully updated";
                }

                return LoadAndRenderMyAccountView(client);
            });
        }

        public ActionResult EditAddress(Client client, AddressViewModel model)
        {
            return TryOnEvent(() =>
                                  {
                                      var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                                      var dsEnvironment = DetermineDSEnvironment(user, client);
                                      var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];
                                      var address = new Address();
                                      model.MapToAddress(address);
                                      var isPrimary = address.DsIsPrimary;
                                      if (isPrimary)
                                      {
                                          var country = GetAndSetCountry(model);
                                          if (country != null)
                                          {
                                              var userEnvironmentWasChanged = CheckIfUserChangeCompany(client, model,dsEnvironment);

                                              user = service.AccountService.GetUser(user.Username, GetApplicationName(),dsEnvironment);

                                              if (user.AccountNumber == 0)
                                              {
                                                  model.MapToUser(user);

                                                  var dsAccountNumber = service.DsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment, DsAccountMapper.Map(user));
                                                  var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment, dsAccountNumber).AddressId;
                                                  user.AccountNumber = dsAccountNumber;
                                                  user.DsEnvironment = dsEnvironment;
                                                  user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                                                  user.Profile.ApplicationName = GetApplicationName();
                                                  service.AccountService.SaveUser(user);
                                              }
                                              else
                                              {
                                                  model.MapToUser(user);
                                                  //service.AccountService.SaveUser(user, dsEnvironment);
                                                  service.AccountService.UpdateAccountAddress(user.AccountNumber, address,dsEnvironment);
                                              }


                                              service.AccountService.SaveUser(user, dsEnvironment: dsEnvironment);

                                              if (userEnvironmentWasChanged)
                                              {
                                                  if (redirectRoute != null)
                                                  {
                                                      ViewData["RedirectLaterRoute"] = redirectRoute;
                                                  }
                                                  ViewData["PopupMessageText"] =TempData[MagicStringEliminator.Messages.Status];
                                              }
                                          }
                                      }
                                      else
                                      {
                                          service.AccountService.UpdateAccountAddress(user.AccountNumber, address,dsEnvironment);
                                          if (redirectRoute != null)
                                          {
                                              Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                                              return RedirectToRoute(redirectRoute);
                                          }

                                          TempData[MagicStringEliminator.Messages.Status] = "Account was successfully updated";
                                      }

                                      return RedirectToAction("MyAccount");
                                  });
        }

        [HttpPost]
        public ActionResult AddNewAddress(Client client, FormCollection formCollection, AddressViewModel model, string returnUrl = null)
        {
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var newAddress = new Address();
                model.MapToAddress(newAddress);

                if (String.IsNullOrEmpty(newAddress.Address1))
                {
                    ModelState.AddModelError("Address1", "Street address is required");
                }
                if (string.IsNullOrEmpty(newAddress.City))
                {
                    ModelState.AddModelError("City", "City is required");
                }
                if (string.IsNullOrEmpty(newAddress.Country))
                {
                    ModelState.AddModelError("Country", "Country is required");
                }
                if (string.IsNullOrEmpty(newAddress.StateCode))
                {
                    ModelState.AddModelError("State", "State or Province is required");
                }

                ModelState.Remove("AddressId");

                if (ModelState.IsValid)
                {
                    if (user.AccountNumber == 0 || user.Profile.PrimaryAddress == null || String.IsNullOrEmpty(user.Profile.PrimaryAddress.Address1))
                    {
                        var securityUser = service.SecurityService.LoadUserById(Guid.Parse(user.GlobalId));
                        user.Profile.FirstName = securityUser.Data.FirstName;
                        user.Profile.LastName = securityUser.Data.LastName;
                        user.Email = securityUser.Data.EmailAddress;
                        user.Username = securityUser.Data.Username;
                        user.GlobalId = securityUser.Data.Id;
                        
                        newAddress.DsIsPrimary = true;
                        user.Profile.PrimaryAddress = newAddress;
                        
                        CheckAndSetDSEnvironment(user, client);
                    }
                    else
                    {
                        if (model.Primary)
                        {
                            var country = GetAndSetCountry(model);
                            var dsEnvironment = DetermineDSEnvironment(user, client);

                            if (country != null)
                            {
                                var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(client.Company.DonorStudioEnvironment, user.AccountNumber).AddressId;
                                user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
                                service.AccountService.SaveUser(user, false, dsEnvironment: dsEnvironment);
                            }
                        }
                        var dsEnviroment = DetermineDSEnvironment(user, client);
                        var addressId = service.AccountService.AddAddressToUser(user.AccountNumber, newAddress, user.DsEnvironment);
                    }

                    TempData[MagicStringEliminator.Messages.Status] = "New Address was successfully added";
                    var redirectRoute = Session[MagicStringEliminator.SessionKeys.RedirectTo];

                    if (redirectRoute != null)
                    {
                        Session.Remove(MagicStringEliminator.SessionKeys.RedirectTo);
                        return RedirectToRoute(redirectRoute);
                    }

                    if (returnUrl.IsNotNullOrEmpty())
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("MyAccount");
                }

                ViewData["ShowNewAddressPopupScript"] = "$('#addressPopup').dialog('open');";

                return LoadAndRenderMyAccountView(client);
            });
        }

        [Authorize]
        public ActionResult DeleteAddressAccount(Client client, AddressViewModel model)
        {
            return TryOnEvent(() =>
                                  {
                                      var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                                      var dsEnvironment = DetermineDSEnvironment(user, client);
                                      user = service.AccountService.GetUser(user.Username, GetApplicationName(),
                                                                            dsEnvironment);
                                      var address = new Address();
                                      model.MapToAddress(address);
                                      service.AccountService.DeleteAccountAddress(user.AccountNumber, address,
                                                                                  dsEnvironment);
                                      TempData[MagicStringEliminator.Messages.Status] =
                                          "Account was successfully updated";

                                      return RedirectToAction("MyAccount");
                                  });
        }

        public Country GetAndSetCountry(AddressViewModel model)
        {
            var countries = Country.GetAll<Country>().ToList();
            var country = countries.Find(x => x.Iso == model.Country);
            if (country == null)
            {
                ModelState.AddModelError("Country", "Country not found");
            }
            else if (!Core.CountryListHelper.CountriesWithoutZipCodes.Contains(country.Iso3) &&
                     String.IsNullOrEmpty(model.Zip))
            {
                ModelState.AddModelError("Zip",
                                         StringExtensions.FormatWith("Postal Code code is required in {0}", country.DisplayName));
            }
            return country;
        }

        public bool CheckIfUserChangeCompany(Client client, AddressViewModel model, string dsEnvironment)
        {
            var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
            var country = GetAndSetCountry(model);
            var currentUserCompany = Company.GetCompanyByDSEnvironment(user.DsEnvironment);
            var newCompany = Company.GetCompanyByCountry(country);
            var userEnvironmentWasChanged = false;
            if (currentUserCompany != newCompany)
            {
                var popupMessage = new PopupMessageViewModel();
                popupMessage.Title = "Important Message";
                popupMessage.Message =
                    String.Format(
                        "Account was successfully updated. We noticed that you have an address in {0} and so we have updated you to our {1} system.  Future transactions will now be processed by {1} and will be in {2}.  Thank you for your continued support! ",
                        country.Name, newCompany.DisplayName, newCompany.CurrencyFullName);
                AddPopupMessage(popupMessage);
                TempData[MagicStringEliminator.Messages.Status] = popupMessage.Message;
                // To create a user in the new DS environment
                if (model.Primary)
                {
                    user.AccountNumber = 0;
                }
                userEnvironmentWasChanged = true;
                //dsEnvironment = newCompany.DonorStudioEnvironment;
                //userEnvironmentWasChanged = true;
                //ClearRegisteredClient();
                //UpdateClientCompany(newCompany);
            }
            return userEnvironmentWasChanged;
        }

        public ActionResult ForgotPassword()
        {
            return RedirectToAction("RequestPasswordChange");
        }

        /*[HttpPost]
        public ActionResult ForgotPassword(PasswordRecoveryModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ForgotPassword;
            return TryOnEvent(() =>
                {
                    if (String.IsNullOrEmpty(model.UserName))
                    {
                        ModelState.AddModelError("", "The Username field is required.");
                        return View(model);
                    }
                    model.UserName = model.UserName.Trim();
                    var user = service.AccountService.GetUser(model.UserName);
                    if (user == null || user.IsTransient())
                    {
                        ModelState.AddModelError("", "No user was found with the provided User name.");
                        model = new PasswordRecoveryModel();
                        return View(model);
                    }


                    if (String.IsNullOrEmpty(model.PasswordAnswer))
                    {
                        ModelState.Remove("PasswordAnswer");
                        model.PasswordQuestion =
                            RegistrationPasswordQuestion.TryGetFromValue<RegistrationPasswordQuestion>
                                (Guid.Parse(user.PasswordQuestion)).DisplayName;
                        return View(model);
                    }
                    model.PasswordAnswer = model.PasswordAnswer.Trim();

                    try
                    {
                        var newPassword = service.MembershipService.ResetPassword(user.Username,model.PasswordAnswer);
                        //var newPassword = GenerateRandomPassword(8);
                        model.Email = user.Email;
                        SendForgotPasswordEmail(user, newPassword);
                    }
                    catch (Exception e)
                    {
                        ModelState.Remove("PasswordAnswer");
                        ModelState.Remove("UserName");
                        model.PasswordQuestion =
                            RegistrationPasswordQuestion.TryGetFromValue<RegistrationPasswordQuestion>
                                (Guid.Parse(user.PasswordQuestion)).DisplayName;
                        ModelState.AddModelError("", e.Message);
                    }

                    return View(model);
                });
        }*/

        public virtual ActionResult RequestPasswordChange()
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = Request.IsAuthenticated ? AccountManagementTabs.SecondaryTab.ChangePassword : AccountManagementTabs.SecondaryTab.ForgotPassword;
            if (SessionManager.CurrentUser != null)
            {
                var model = new ForgotPasswordModel { Email = SessionManager.CurrentUser.Email };
                //if (Session["DoNotResendTokenEmail"] == null)
                //{
                //    return RequestPasswordChange(model);
                //}
                return View(model);
            }
            Session.Remove("DoNotResendTokenEmail");
            return View();
        }

        [HttpPost]
        public virtual ActionResult RequestPasswordChange(ForgotPasswordModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = Request.IsAuthenticated ? AccountManagementTabs.SecondaryTab.ChangePassword : AccountManagementTabs.SecondaryTab.ForgotPassword;
            if (ModelState.IsValid)
            {
                var hostUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

                var confirmationUrl = hostUrl + VirtualPathUtility.ToAbsolute("~/Account/ConfirmPasswordReset/{0}");
                var status = service.SecurityService.InitiatePasswordReset(model.Email, confirmationUrl);

                if (status.IsSuccessful)
                {
                    model.Message = "An email with instructions on how to change your password is on its way to you. If this is not the email address you registered with, you will not receive an email.";
                    AddPopupMessage("Instructions Successfully Sent.  If this is not the email address you registered with, you will not receive an email.", "Request Password Change");
                    Session["DoNotResendTokenEmail"] = true;
                    return View(model);
                }
                if (status.Error.ErrorCode == (int) SecurityErrorCode.ProviderError)
                {
                    AddPopupMessage("Sorry, there was a problem sending instructions to your email. Please try again later", "Request Password Change");
                    return View(model);
                }
                AddPopupMessage("Sorry, there was a problem sending instructions to your email: " + status.Error.UserMessage.SeparateCapitalizedWords(), "Request Password Change");
                return View(model);
            }
            return View(model);
        }

        public virtual ActionResult ConfirmPasswordReset(string id)
        {
            Session["ResetToken"] = id;
            return RedirectToAction("ChangePassword");
            //return RedirectToAction("ResetPassword",new PasswordResetModel { ResetToken = id });
            //return RedirectToAction(MVC.Account.ResetPassword(new PasswordResetModel { ResetToken = id }));
            //return RedirectToAction(MVC.Account.ResetPassword(),new PasswordResetModel { ResetToken = id });
        }


        public ActionResult ForgotUsername()
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ForgotUsername;
            return View(new ForgotUsernameModel());
        }

        [HttpPost]
        public ActionResult ForgotUsername(ForgotUsernameModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ForgotUsername;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return TryOnEvent(() =>
            {
                model.Email = model.Email.Trim();
                var result = service.SecurityService.RequestForgottenUsername(model.Email);
                if (!result.IsSuccessful)
                {
                    ModelState.AddModelError("", result.Error.UserMessage.SeparateCapitalizedWords());
                    model = new ForgotUsernameModel();
                    return View(model);
                }

                //try
                //{
                //    model.Email = user.Email;
                //    SendForgotUsernameEmail(user);
                //}
                //catch (Exception e)
                //{
                //    ModelState.AddModelError("", e.ToString());
                //}
                AddPopupMessage("Instructions Successfully Sent.  If this is not the email address you registered with, you will not receive an email.", "Request Password Change");
                    
                return View(model);
            });
        }

        private void SendForgotUsernameEmail(Core.Models.User user)
        {
            var body = System.IO.File.ReadAllText(Server.MapPath(IsDM ? "/EmailTemplates/DifferenceMedia/ForgotUsername.html" : "/EmailTemplates/generic.html"));

            var sb = new StringBuilder();
            if (!IsDM)
            {
                sb.AppendFormat(
                    "<h3>Forgot Username</h3><p>Your username is: <input type='text' columns='30' readonly value='{0}'/>",
                    user.Username);
                sb.AppendLine("<br /><br />");
                sb.AppendFormat(
                    @"Please click on the link below to login using your username: <br />");
                sb.AppendFormat(
                    "<a href='{0}'>{0}</a><br /><br />Thank you,<br />The John Hagee Ministries team.<br /><br />",
                    FullVirtualWebSitePath + Url.Action("LogOn"));
            }
            var replacements = new ListDictionary
            {
                {"<%EMAIL%>", user.Email},
                {"<%WEBSITE_URL%>", FullVirtualWebSitePath},
                {"<%LOGON_URL%>", FullVirtualWebSitePath + Url.Action("LogOn")},
                {"<%USERNAME%>",user.Username},
                {"<%IMAGES_PATH%>", FullVirtualWebSitePath + (IsDM? Url.Content("/EmailTemplates/DifferenceMedia/images/"): Url.Content("/EmailTemplates/"))},
                {"<%CONTENT%>", sb.ToString()}
            };


            var md = new MailDefinition
                         {
                             From = EmailHelper.EmailFromAccount,
                             IsBodyHtml = true,
                             Subject = IsDM ? "Difference Media - Forgot Username" : "John Hagee Ministries - Forgot Username"
                         };

            var msg = md.CreateMailMessage(user.Email, replacements, body, new System.Web.UI.Control());
            // Sending email to client
            if (IsDM)
            {
                service.EmailService.SendDMEmail(msg);
            }
            else
            {
                service.EmailService.SendEmail(msg);
            }
        }


        [Authorize]
        public virtual ActionResult ChangeEmailAddress()
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangeEmail;
            var model = new ChangeEmailModel();
            var user = SessionManager.SecurityUser;
            if (user == null)
            {
                throw new Exception("User not found");
            }
            model.CurrentEmail = user.EmailAddress;
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public virtual ActionResult ChangeEmailAddress(ChangeEmailModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangeEmail;
            if (ModelState.IsValid)
            {
                var user = SessionManager.CurrentUser;
                if (user == null)
                {
                    throw new Exception("User not found");
                }
                model.CurrentEmail = user.Email;
                var hostUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);

                var confirmationUrl = hostUrl + VirtualPathUtility.ToAbsolute("~/Account/ConfirmEmailAddressChange/{0}");
                var status = service.SecurityService.InitiateEmailAddressChange(user.Email, model.NewEmail, confirmationUrl);

                if (status.IsSuccessful)
                {
                    model.Message = "An email with instructions on how to confirm your new email is on its way to you.";
                    AddPopupMessage("Instructions Successfully Sent", "Change Email Address");
                    return View("ChangeEmailMessage", model);
                }
                if (status.Error.ErrorCode == (int)SecurityErrorCode.ProviderError)
                {
                    AddPopupMessage("Sorry, there was a problem sending instructions to your email. Please try again later", "Change Email Address");
                    return View(model);
                }
                AddPopupMessage("Sorry, there was a problem sending instructions to your email: " + status.Error.UserMessage.SeparateCapitalizedWords(), "Change Email Address");
                return View(model);
            }
            return View(model);
        }

        public virtual ActionResult ConfirmEmailAddressChange(string id)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangeEmail;
            var token = id;
            FormsAuthentication.SignOut();
            if (!string.IsNullOrEmpty(token))
            {
                var hostUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);
                var confirmationUrl = hostUrl + VirtualPathUtility.ToAbsolute("~/Account/ConfirmEmailAddressChange/{0}");
                var status = service.SecurityService.ConfirmEmailAddressChange(token, confirmationUrl);
                if (status.IsSuccessful)
                {
                    ViewData["Message"] = "New Email Address is Confirmed! Click on the login link at the top right of the page to continue.";
                    //AddPopupMessage("New Email Address is Confirmed!  Please Log In.", "Email Address Change Confirmation");
                    return RedirectToAction("Logon");
                }
                else if (status.Error.ErrorCode == (int)SecurityErrorCode.EmailAddressChangeCancelled)
                {
                    ViewData["Message"] = "Email Address Change Cancelled. Click on the login link at the top right of the page to continue.";
                    //AddPopupMessage("Email Address Change Cancelled.  Please Log In.", "Email Address Change Confirmation");
                }
                else
                {
                    ViewData["Message"] = "Could not confirm your Email Address change: " + status.Error.UserMessage.SeparateCapitalizedWords();
                    //AddPopupMessage("Could not confirm your Email Address change: " + status.Error.UserMessage.SeparateCapitalizedWords() , "Email Address Change Confirmation");
                }
            }

            return View("ConfirmAccount");
        }

        private string _website;

        private string FullVirtualWebSitePath
        {
            get
            {
                if (String.IsNullOrEmpty(_website))
                {
                    _website = "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"];
                }
                return _website;
            }
        }

        private void SendForgotPasswordEmail(Core.Models.IUser user, string newPassword)
        {
            var body = System.IO.File.ReadAllText(Server.MapPath(IsDM ? "/EmailTemplates/DifferenceMedia/ForgotPassword.html" : "/EmailTemplates/generic.html"));

            var sb = new StringBuilder();
            if (!IsDM)
            {
                sb.AppendFormat(
                    "<h3>Temporary Password Reset</h3><p>Your username is: <br /><input type='text' columns='30' readonly value='{0}'/><br/><p>Your new temporary password is: <br /><input type='text' readonly value='{1}'/>",
                    user.Username, newPassword);
                sb.AppendLine("<br /><br />");
                sb.AppendFormat(
                    @"<b>Note: This is a temporary password, it is adviced that you change your password at you earliest convenience.</b> 
                    Once you are logged in you will be redirected to a page to change your password.<br />
                    Please click on the link below to login using your new temporary password: <br />");
                sb.AppendFormat(
                    "<a href='{0}'>{0}</a><br /><br />Thank you,<br />The John Hagee Ministries team.<br /><br />",
                    FullVirtualWebSitePath + Url.Action("LogOn"));
            }

            var replacements = new ListDictionary
            {
                {"<%EMAIL%>", user.Email},
                {"<%WEBSITE_URL%>", FullVirtualWebSitePath},
                {"<%LOGON_URL%>", FullVirtualWebSitePath + Url.Action("LogOn")},
                {"<%USERNAME%>",user.Username},
                {"<%PASSWORD%>", newPassword},
                {"<%IMAGES_PATH%>", FullVirtualWebSitePath + (IsDM? Url.Content("/EmailTemplates/DifferenceMedia/images/"): Url.Content("/EmailTemplates/"))},
                {"<%CONTENT%>", sb.ToString()}
            };


            var md = new MailDefinition
            {
                From = EmailHelper.EmailFromAccount,
                IsBodyHtml = true,
                Subject = IsDM ? "Difference Media - Temporary Password" : "John Hagee Ministries - Temporary Password"
            };

            var msg = md.CreateMailMessage(user.Email, replacements, body, new System.Web.UI.Control());
            // Sending email to client
            if (IsDM)
            {
                service.EmailService.SendDMEmail(msg);
            }
            else
            {
                service.EmailService.SendEmail(msg);
            }
        }

        private void SetTemporaryPassword(Core.Models.User user)
        {
            var password = GenerateRandomPassword(8);
            if (!service.MembershipService.ChangePassword(user.Username, user.Password, password))
            {
                throw new Exception("There was a problem when changing your password");
            }
        }

        [Authorize]
        [Obsolete]
        public ActionResult ChangeSecurityQuestion()
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangeQuestion;

            return View(new ChangePasswordQuestionViewModel());
        }

        [HttpPost]
        [Obsolete]
        public ActionResult ChangeSecurityQuestion(ChangePasswordQuestionViewModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.LogOn;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.ChangeQuestion;

            return TryOnEvent(() =>
                                  {
                                      if (!ModelState.IsValid)
                                      {
                                          return View();
                                      }
                                      var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                                      if (user == null || user.IsTransient())
                                      {
                                          ModelState.AddModelError("", "No user was found with the provided User name.");
                                          return View();
                                      }

                                      if (!service.MembershipService.ValidateUser(user.Username, model.Password))
                                      {
                                          ModelState.AddModelError("", "Provided password did not match our records.");
                                          return View();
                                      }

                                      if (
                                          !service.MembershipService.ChangePasswordQuestion(user.Username,
                                                                                            model.Password,
                                                                                            model.PasswordQuestion,
                                                                                            model.PasswordAnswer))
                                      {
                                          ModelState.AddModelError("", "Password Question change failed.");
                                          return View();
                                      }

                                      TempData[MagicStringEliminator.Messages.Status] =
                                          "Password Question was successfully changed";

                                      return View();
                                  });
        }

        public ActionResult Preferences()
        {
            if (!IsAuthenticated)
            {
                Session[MagicStringEliminator.SessionKeys.RedirectTo] = new
                                                                            {
                                                                                controller =
                                                                                    MagicStringEliminator.Routes.
                                                                                        Account_PromotionSettings.
                                                                                        Controller,
                                                                                action =
                                                                                    MagicStringEliminator.Routes.
                                                                                        Account_PromotionSettings.Action
                                                                            };
                return RedirectToRoute(MagicStringEliminator.Routes.Account_Logon.RouteName);
            }
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.Preferences;
            return TryOnEvent(() =>
                                  {
                                      var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                                      if (user == null || user.IsTransient())
                                      {
                                          ModelState.AddModelError("", "No user was found with the provided User name.");
                                          return View(new PreferencesViewModel());
                                      }

                                      var model = new PreferencesViewModel();
                                      model.Profile = user.Profile;

                                      //var setting = (ProfileSettingViewModel)ProfileSettingViewModel.GoldenTicketSetting.Clone();
                                      //if (!String.IsNullOrEmpty(user.Profile.GoldenTicketCode))
                                      //{
                                      //    setting.IsActive = true;
                                      //}
                                      //model.AddSetting(setting);
                                      //model.AddSetting((ProfileSettingViewModel)ProfileSettingViewModel.JHMEmailSetting.Clone()););
                                      return View(model);
                                  });
        }

        [Authorize]
        [PageCss("page-front", "no-sidebars")]
        public ActionResult MyDownloads(Client client)
        {
            if (!IsAuthenticated)
            {
                Session[MagicStringEliminator.SessionKeys.RedirectTo] = new
                {
                    controller = MagicStringEliminator.Routes.Account_PromotionSettings.Controller,
                    action = MagicStringEliminator.Routes.Account_PromotionSettings.Action
                };
                return RedirectToRoute(MagicStringEliminator.Routes.Account_Logon.RouteName);
            }
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.Downloads;
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    ModelState.AddModelError("", "No user was found with the provided User name.");
                    return View(new DownloadsViewModel { Cart = client.Cart });
                }

                var stockItems = new Dictionary<Guid, ProductImageToStockItemViewModel>();
                var dsEnvironment = DetermineDSEnvironment(user, client);
                foreach (var download in user.Downloads)
                {
                    try
                    {
                        //TODO: Will downloads be tied to a DsEnvironment???
                        var stockItem = service.CatalogService.GetStockItem(dsEnvironment,
                                                                            download.ProductCode,
                                                                            MediaType.
                                                                                GetMediaTypeByCodeSuffix
                                                                                (download.MediaType));

                        if (!stockItem.IsNull())
                        {
                            var imagePath =
                                service.CatalogService.GetProduct(dsEnvironment, download.ProductCode).AlternativeImages.
                                    FirstOrDefault();
                            var insertStockItem = new ProductImageToStockItemViewModel { StockItem = stockItem, ImagePath = imagePath };
                            stockItems.Add(download.Id, insertStockItem);
                        }
                        else
                        {
                            stockItems.Add(download.Id, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }

                return View(new DownloadsViewModel(user, stockItems, client));
            });
        }

        [Authorize]
        public ActionResult MyTransactions(Client client)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MyTransactions;

            if (!IsAuthenticated)
            {
                Session[MagicStringEliminator.SessionKeys.RedirectTo] = 
                new
                {
                    controller = MagicStringEliminator.Routes.Account_PromotionSettings.Controller,
                    action = MagicStringEliminator.Routes.Account_PromotionSettings.Action
                };
                return RedirectToRoute(MagicStringEliminator.Routes.Account_Logon.RouteName);
            }
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MyTransactions;
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    ModelState.AddModelError("", "No user was found with the provided User name.");
                    return View(new UserTransactionsViewModel());
                }
                var howManyMonthsBack = 24;
                var model = new UserTransactionsViewModel();
                var dsEnvironment = DetermineDSEnvironment(user, client);

                var dsUser = service.AccountService.GetUser(user.Username, user.ApplicationName, dsEnvironment);

                var transactionHistory = new List<TransactionViewModel>();
                if (user.AccountNumber > 0)
                {
                    transactionHistory = service.AccountService.GetTransactionsList(dsUser, howManyMonthsBack,
                                                                                    dsEnvironment);
                }

                // NHibernate hack for Proxy error
                var x = user.Profile.FirstName;
                model.User = user;
                model.Client = client;
                model.Transactions = transactionHistory;
                return View(model);
            });
        }

        [Authorize]
        public ActionResult MySubscriptions(Client client, long? id)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MySubscriptions;

            if (!IsAuthenticated)
            {
                Session[MagicStringEliminator.SessionKeys.RedirectTo] =
                new
                {
                    controller = MagicStringEliminator.Routes.Account_PromotionSettings.Controller,
                    action = MagicStringEliminator.Routes.Account_PromotionSettings.Action
                };
                return RedirectToRoute(MagicStringEliminator.Routes.Account_Logon.RouteName);
            }
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    ModelState.AddModelError("", "No user was found with the provided User name.");
                    return View(new UserSubscriptionsViewModel());
                }
                var model = new UserSubscriptionsViewModel();
                var dsEnviroment = DetermineDSEnvironment(user, client);

                var subscriptions = new List<UserSubscriptionViewModel>();
                if (user.AccountNumber > 0)
                {
                    try
                    {
                        subscriptions = service.AccountService.GetActiveAccountSubscriptions(user.AccountNumber,
                                                                            dsEnviroment);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }

                var userAddresses = new List<Address>();
                if (user.Profile.PrimaryAddress != null)
                {
                    userAddresses = service.AccountService.GetUserAddresses(user.Username, user.ApplicationName,
                                                                            dsEnviroment).ToLIST();
                }

                model.Addresses = userAddresses.ToList();

                if ( id != null )
                {
                    model.SelectedSubscription = subscriptions.SingleOrDefault(x => x.Id == id);
                }

                model.User = user;
                model.Client = client;
                model.Subscriptions = subscriptions;
                return View(model);
            });
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateSubscriptionDeliveryAddress(Client client, UserSubscriptionsViewModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MySubscriptions;
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    ModelState.AddModelError("", "No user was found with the provided User name.");
                    return View("MySubscriptions", new UserSubscriptionsViewModel());
                }
                var dsEnviroment = DetermineDSEnvironment(user, client);
                var subscriptions = service.AccountService.GetActiveAccountSubscriptions(user.AccountNumber,dsEnviroment);
                var sub = subscriptions.SingleOrDefault(x => x.Id == model.SelectedSubscription.Id);
                if (sub == null)
                {
                    ModelState.AddModelError("", "Subscription not found.");
                    return View("MySubscriptions", new UserSubscriptionsViewModel());
                }

                if (model.SelectedDeliveryAddressId != null)
                {
                    //sub.DeliveryAddress.DSAddressId = model.SelectedDeliveryAddressId.Value;
                    sub.DeliveryAddress = service.AccountService.GetUserAddressById(model.SelectedDeliveryAddressId.Value,
                                                                                 dsEnviroment);
                }
                else
                {
                    var newAddress = new Address();
                    model.DeliveryAddress.MapToAddress(newAddress);
                    var addressId = service.AccountService.AddAddressToUser(user.AccountNumber, newAddress, dsEnviroment);
                    sub.DeliveryAddress.DSAddressId = addressId;
                }
                service.AccountService.UpdateAccountSubscription(dsEnviroment, sub);
                return RedirectToAction("MySubscriptions");
            });
        }

        [Authorize]
        public ActionResult CancelSubscription(Client client, long id)
        {
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    //ModelState.AddModelError("", "No user was found with the provided User name.");
                    Redirect("/Account/MyAccount#mysubscriptions");
                }
                var dsEnviroment = DetermineDSEnvironment(user, client);
                var subscriptions = service.AccountService.GetActiveAccountSubscriptions(user.AccountNumber, dsEnviroment);
                var sub = subscriptions.SingleOrDefault(x => x.Id == id);
                if (sub == null)
                {
                    //ModelState.AddModelError("", "Subscription not found.");
                    Redirect("/Account/MyAccount#mysubscriptions");
                }
                service.AccountService.CancelAccountSubscription(dsEnviroment, sub);

                return Redirect("/Account/MyAccount#mysubscriptions");
            });
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetTransactionByDocumentNumber(Client client, long documentNumber)
        {
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user == null || user.IsTransient())
                {
                    ModelState.AddModelError("", "No user was found with the provided User name.");
                    return Json(new { Result = "ERROR" });
                }

                var dsEnviroment = DetermineDSEnvironment(user, client);
                var dsTransaction = service.AccountService.GetTransactionByDocumentNumber(dsEnviroment, user,documentNumber);
                var individualTransaction = new
                {
                    Donations =dsTransaction.Donations.Select(d => MapDonation(d, client)),
                    Date =dsTransaction.Date.ToShortDateString(),
                    Orders =dsTransaction.Orders.Select(o => MapOrders(o, client)),
                    dsTransaction.Status,
                    Subscriptions = dsTransaction.Subscriptions.Select(MapSubscription),
                    OrderTotal = dsTransaction.TotalAmount
                };

                return Json(new { Result = "OK", transactions = individualTransaction });
            });
        }

        private object MapSubscription(UserSubscriptionViewModel sub)
        {
            return new
            {
                Description = sub.SubscriptionTitle + (String.IsNullOrEmpty(sub.PriceCodeDescription) ? String.Empty : " (" + sub.PriceCodeDescription + ")"),
                Amount = sub.SubscriptionPrice
            };
        }

        private List<Core.Models.Donation> donationOpportunities;

        private object MapOrders(OrderViewModel orders, Client client)
        {
            var getOrderAmount = Jhm.Common.Framework.Extensions.MiscExtensions.ToLIST(orders.OrderDetails);
            var totalProduct = getOrderAmount.Sum(price => price.Price);
            return new
            {
                ShippingAmount = orders.ShippingAmount.ToString("C", client.Company.Culture),
                Details = orders.OrderDetails.Select(n => MapOrderDetails(n, client)),
                OrderTotalAmount = (totalProduct + orders.ShippingAmount).ToString("C", client.Company.Culture)
            };
        }

        private object MapOrderDetails(OrderDetailViewModel orderDetail, Client client)
        {
            return new
            {
                OrderAmount = orderDetail.Price.ToString("C", client.Company.Culture),
                orderDetail.ProductDescription,
                orderDetail.Quantity,
                orderDetail.Status,
                orderDetail.DiscountAmount,
            };
        }

        private object MapDonation(DonationViewModel donation, Client client)
        {
            return new
            {
                Amount = donation.Amount.ToString("C", client.Company.Culture),
                ProjectCode =
                    UserTransactionsViewModel.GetDonationTitleByProjectCode(donation.ProjectCode,
                                                                            GetAllDonationsList(client))
            };
        }


        private List<Donation> GetAllDonationsList(Client client)
        {
            if (donationOpportunities == null)
            {
                donationOpportunities = Common.Framework.Extensions.MiscExtensions.ToLIST(service.DonationService.GetActiveDonationOpportunities(client.Company).Select(
                        d => new Donation(d.ProjectCode, d.Title)));
            }
            return donationOpportunities;
        }

        //[Authorize]
        //[HttpPost]
        //public ActionResult MyTransactions()
        //{
        //    ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
        //    ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.MyTransactions;
        //     //DsAccountTransactionService myTransactions = new DsAccountTransactionService();
        //    var dsAccountNumber = service.DsServiceCollection.DsAccountTransactionService.GetTransactionHistory(dsEnvironment, DsAccountMapper.Map(user));
        //    var dsAddressId = service.DsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment, dsAccountNumber).AddressId;
        //    user.AccountNumber = dsAccountNumber;
        //    user.DsEnvironment = dsEnvironment;
        //    user.Profile.PrimaryAddress.DSAddressId = dsAddressId;
        //    user.Profile.ApplicationName = GetApplicationName();
        //    service.AccountService.SaveUser(user);
        //    return View();
        //}


        [Authorize]
        [HttpPost]
        public ActionResult Preferences(Client client, PreferencesViewModel model)
        {
            ViewData["MainTab"] = AccountManagementTabs.MainTab.MyAccount;
            ViewData["SecondaryTab"] = AccountManagementTabs.SecondaryTab.Preferences;
            return TryOnEvent(() =>
                                  {
                                      var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                                      if (user == null || user.IsTransient())
                                      {
                                          ModelState.AddModelError("", "No user was found with the provided User name.");
                                          return View(new PreferencesViewModel { LightCart = client.Cart });
                                      }

                                      model.Profile = user.Profile;
                                      foreach (var setting in model.ProfileSettings)
                                      {
                                          //if (setting.Id == ProfileSettingViewModel.GoldenTicketSetting.Id)
                                          //{
                                          //    var dsGoldenTicketCode = "Golden123"; // TODO: Get code from DS
                                          //    user.Profile.GoldenTicketCode = setting.IsActive
                                          //                                        ? dsGoldenTicketCode
                                          //                                        : null;
                                          //    setting.Title = ProfileSettingViewModel.GoldenTicketSetting.Title;
                                          //    setting.Description =
                                          //        ProfileSettingViewModel.GoldenTicketSetting.Description;
                                          //}
                                      }
                                      var dsEnvironment = DetermineDSEnvironment(user, client);
                                      service.AccountService.SaveUser(user, dsEnvironment: dsEnvironment);

                                      TempData[MagicStringEliminator.Messages.Status] =
                                          "Preferences were successfully changed";
                                      model.LightCart = client.Cart;
                                      return View(model);
                                  });
        }

        #endregion

        private static string GenerateRandomPassword(int length)
        {
            const string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
            const string allowedNumberChars = "23456789";
            var chars = new char[length];
            var rd = new Random();

            var useLetter = true;
            for (var i = 0; i < length; i++)
            {
                if (useLetter)
                {
                    chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                }
                else
                {
                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                }
                useLetter = !useLetter;
            }

            return new string(chars);
        }

        public ActionResult ImpersonateUserForm()
        {
            return View();
        }

        public ActionResult ImpersonateUser(string username)
        {
            return TryOnEvent(() =>
            {
                var loggedInUser = GetLoggedInUser();
                if (loggedInUser == null || !(loggedInUser.Roles.Any(x=>x.RoleName == "SuperAdmin")))
                {
                    return RedirectToAction("Index", "Error", new { Id = 404 });
                }
                                      
                service.FormsService.SignIn(username, false);
                var user = service.AccountService.GetUser(username);
                if (user == null || user.Roles.Any(x => x.RoleName == "SuperAdmin"))
                {
                    return RedirectToAction("Index", "Error", new { Id = 401 });
                }
                
                Session[MagicStringEliminator.SessionKeys.LoggedUserFullName] =
                    StringExtensions.FormatWith("{0} {1} {2}", user.Profile.Title, user.Profile.FirstName,
                                            user.Profile.LastName);
                SessionManager.ClearCurrentUser();
                return RedirectToAction("Index", "Home");
            });
        }

        public long documentNumber { get; set; }

        
        [Authorize]
        [PageCss("page-front", "no-sidebars")]
        public ActionResult FileDownloadManager(Client client, Guid downloadId)
        {

            if (Guid.Empty == downloadId)
            {
                return RedirectToAction("MyDownloads");
            }
            return TryOnEvent(() =>
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                var stockItems = new Dictionary<Guid, ProductImageToStockItemViewModel>();

                var dsEnvironment = DetermineDSEnvironment(user, client);
                var download = user.Downloads.FirstOrDefault(pc => pc.Id == downloadId);
                var stockItem =
                        service.CatalogService.GetStockItem(dsEnvironment, download.ProductCode, MediaType.GetMediaTypeByCodeSuffix(download.MediaType));
                if (!stockItem.IsNull())
                {
                    var imagePath =
                        service.CatalogService.GetProduct(dsEnvironment, download.ProductCode).AlternativeImages.FirstOrDefault();

                    var insertStockItem = new ProductImageToStockItemViewModel { StockItem = stockItem, ImagePath = imagePath, Id = downloadId };
                    stockItems.Add(download.Id, insertStockItem);
                }
                var model = new DownloadsViewModel(user, stockItems, download ,client);
                Session[downloadId.ToString()] = true;

                return View(model);
            });
        }

        [Authorize]
        public ActionResult DeleteCreditCard(Guid id)
        {
            return TryOnEvent(() =>
                {
                    service.AccountService.DeleteCreditCard(id, SessionManager.CurrentUser.Id, SessionManager.CurrentUser.ApplicationName);
                    return RedirectToAction("MyAccount");
                });
        }
    }
}