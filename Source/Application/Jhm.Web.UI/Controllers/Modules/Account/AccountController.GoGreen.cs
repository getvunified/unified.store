﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Controllers
{
	public partial class AccountController
	{
        public enum Environment
        {
            US,
            CA,
            GB
        }

        public ActionResult GoPaperlessSignup(Environment environment, string accountNumber)
		{
		    long dsAccountNumber;
		    if (String.IsNullOrEmpty(accountNumber) || !long.TryParse(accountNumber, out dsAccountNumber))
		    {
		        AddPopupMessage(new PopupMessageViewModel
		            {
		                Message = "Sorry, You have provided an invalid Account Number",
                        Title = "Go Paperless Signup"
		            });
		        return RedirectToAction("Index", "Home");
		    }
            if (!Enum.IsDefined(typeof(Environment),environment))
            {
                AddPopupMessage(new PopupMessageViewModel
                {
                    Message = "Sorry, You have provided an invalid agency",
                    Title = "Go Paperless Signup"
                });
                return RedirectToAction("Index", "Home");
            }
            var company = Company.GetCompanyByCountry(Country.FindByISOCode(environment.ToString()));
            if ( company == null )
            {
                AddPopupMessage(new PopupMessageViewModel
                {
                    Message = "Sorry, You have provided an invalid agency",
                    Title = "Go Paperless Signup"
                });
                return RedirectToAction("Index", "Home");
            }
		    return TryOnEvent(() =>
		        {
		            var dsEnvironment = company.DonorStudioEnvironment;
                    service.AccountService.AddGoGreenCode(dsEnvironment, dsAccountNumber);
                    AddPopupMessage(new PopupMessageViewModel
                    {
                        Message = "<br />Thank you for helping us become better stewards of the resources that God has given us by agreeing to join our “Go Paperless” campaign. <br /><br />Every penny saved is money we can put toward taking the Gospel to all the World and to all Generations.  <br /><br />Surely there has never been a more crucial time to reach the lost, and we thank you for joining us in fulfilling the Great Commission. <br /><br /> May God richly bless you for all that you do for His Kingdom!<br />",
                        Title = "Go Paperless Signup"
                    });
                    return RedirectToAction("Index", "Home");
		        });
		}

	    
	}
}