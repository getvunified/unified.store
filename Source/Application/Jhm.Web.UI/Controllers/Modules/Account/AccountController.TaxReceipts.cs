﻿using System;
using System.Threading;
using System.Web.Mvc;
using Jhm.Web.Core.Models.Modules;

namespace Jhm.Web.UI.Controllers
{
    public partial class AccountController
    {


        public FileContentResult AnnualTaxReceipt(Client client)
        {
            using (unitOfWork.Start())
            {
                var user = service.AccountService.GetUser(Thread.CurrentPrincipal.Identity.Name);
                if (user.AccountNumber > 0)
                {
                    var dsEnvironment = DetermineDSEnvironment(user, client);
                    var dsUser = service.AccountService.GetUser(user.Username, user.ApplicationName, dsEnvironment);

                    string extension, mimeType, encoding;

                    var begDate = new DateTime(2012, 1, 1);
                    var endDate = new DateTime(2012, 12, 31);

                    var output = service.DSReportService.GetTaxReceiptReport(begDate, endDate, dsUser.AccountNumber, out extension, out mimeType, out encoding);

                    Response.AddHeader("content-disposition", string.Format("attachment;filename=JHM-2012-Acct{0}.{1}", dsUser.AccountNumber, extension));
                    return new FileContentResult(output, mimeType);
                }
            }
            return new FileContentResult(new byte[0], "PDF");

            
        //    var item = ItemRepo.GetItemById(id);
        //string path = Path.Combine(Server.MapPath("~/App_Data/Items"), item.Path);
        //return File(new FileStream(path, FileMode.Open), "application/octetstream", item.Path);
    
        }


    }
}