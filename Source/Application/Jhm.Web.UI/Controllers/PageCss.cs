using System;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.UI.Controllers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class PageCss : ActionFilterAttribute
    {
        private string bodyId;
        private string bodyCssClass;

        /// <summary>
        /// Sets the BodyID and BodyCssClass values
        /// <para>&#160;</para>
        /// <para>&#160;</para>
        /// </summary>
        /// <param name="bodyId">page-front, page-template</param>
        /// <param name="bodyCssClass">no-sidebars, sidebar-second</param>
        public PageCss(string bodyId, string bodyCssClass)
        {
            this.bodyId = bodyId;
            this.bodyCssClass = bodyCssClass;
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewresult = filterContext.Result as ViewResult;

            if(viewresult.IsNot().Null())
            {
                viewresult.ViewData.Add("BodyId", bodyId);
                viewresult.ViewData.Add("BodyClass", bodyCssClass);
            }
        }
    }
}