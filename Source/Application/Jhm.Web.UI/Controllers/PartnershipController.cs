﻿using System;
using System.Web.Mvc;
using iTextSharp.text.pdf.qrcode;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using getv.donorstudio.core.Global;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jhm.Web.UI.Controllers
{
    //[RequireHttps(RequireSecure = false)]
    public class PartnershipController : BaseController<IServiceCollection>
    {
        //
        // GET: /Partnership/
        private IEnumerable<EmailSubscriptionList> emaillist;

        public PartnershipController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ActionResult ENewsLetterForm()
        {
            ViewBag.ErrorMessage = string.Empty;
			return TryOnEvent(() =>
				{
					ViewData["StateList"] = new SelectList(State.UnitedStates, "ID", "Name");
					var model = new ENewsLetterSignUpViewModel();
					model.IncludeInWeeklyRapidResponse = true;
                    emaillist = service.EmailSubscriptionListService.GetAll().ToList();
                    Session["emaillists"] = emaillist;
                    model.EmailLists = emaillist;
					return View("ENewsLetterSignUpForm", model);
				});
        }

        [HttpPost]
        public ActionResult ENewsLetterForm(ENewsLetterSignUpViewModel data)
        {
            ViewBag.ErrorMessage = string.Empty;

			if (!ModelState.IsValid || data.SelectedEmailLists == null)
            {   
                // TODO: Eventually must Fix problem with session factory and validation  19 Aug 2014 JFE
                // For now, look for stored emaillist.
                if (Session["emaillists"] != null)
                {
                    if (data.SelectedEmailLists == null)
                    {
                        ViewBag.ErrorMessage = "Please Select Subscriptions.";
                    }
                    emaillist = (IEnumerable<EmailSubscriptionList>)Session["emaillists"];
                    data.EmailLists = emaillist;
                    return View("ENewsLetterSignUpForm", data);
                }
                else                
                {
                    ViewBag.ErrorMessage = "Please fill in all fields.";
                    return RedirectToAction("ENewsLetterForm");
                }

			}

            if (!(new EmailAddressAttribute().IsValid(data.Email)))
            {
                emaillist = (IEnumerable<EmailSubscriptionList>)Session["emaillists"];
                data.EmailLists = emaillist;
                ViewBag.ErrorMessage = "Please Enter a Valid Email Address.";
                return View("ENewsLetterSignUpForm", data);
            }

            var subscribedTo = service.EmailSubscriptionListService.GetSubscribedLists(data.Email);
            data.EmailListCode = "WUP";
            data.AccountNumber = 0;
			if (!subscribedTo.SelectedEmailLists.Any())
			{
				service.EmailSubscriptionListService.UpdateSubscription(data);
				AddPopupMessage("Thank you for signing up, you should start getting emails from us soon.","JHM Email Signup");
				return Redirect("/");
			}
            subscribedTo.Email = data.Email;    // JFE not sure why respository method does not populate the Email list used for the request??!!!
			Session["EmailSignup:SubscribedTo"] = subscribedTo;

			return RedirectToAction("UpdateEmailSubscription", new { email = data.Email });
        }

        private bool isValidEmail(string email)
        {

            return true;
        }

        public ActionResult UpdateEmailSubscription(string email)
		{
		    if (Session["EmailSignup:SubscribedTo"] == null){
		        if (email.IsNullOrEmpty())
		        {
		            return RedirectToAction("Index", "Home");
		        }

		    }
		    ViewBag.ErrorMessage = String.Empty;
            ENewsLetterSignUpViewModel model =  new ENewsLetterSignUpViewModel();
            return TryOnEvent(() =>
            {
				    if (Session["EmailSignup:SubscribedTo"] == null)      // added nullcheck for email JFE  11/20/2014
				    {
					    model = service.EmailSubscriptionListService.GetSubscribedLists(email);
				    }
				    else
				    {
                        model = Session["EmailSignup:SubscribedTo"] as ENewsLetterSignUpViewModel;
				    }
			        if (!model.SelectedEmailLists.Any())
			        {
                        // Still nothing found, so just Let it Go, Let it Gooooo
                        return RedirectToAction("Index", "Home");
			        }

			        Session["EmailSignup:SubscribedTo"] = null;
				    ViewData["StateList"] = new SelectList(State.UnitedStates, "ID", "Name");
				    //ENewsLetterSignUpViewModel model = subscribedTo;				
				    model.EmailLists = service.EmailSubscriptionListService.GetAll();
                    // BUGFIX :  Cannot Update User or UNSUBSCRIBEALL with First/LastNames are null
			        if (model.FirstName.IsNullOrEmpty())
			        {
			            model.FirstName = "_";
			        }
			        if (model.LastName.IsNullOrEmpty())
			        {
			            model.LastName = "_";
			        }
			        Session["EmailLists"] = model.EmailLists;
				    return View("UpdateEmailSubscription", model);	        
            });
		}

		[HttpPost]
		public ActionResult UpdateEmailSubscription(ENewsLetterSignUpViewModel data)
		{
		    if (ModelState.IsValid)
		    {
		        ViewBag.ErrorMessage = String.Empty;
		        return TryOnEvent(() =>
		        {
		            //if (!ModelState.IsValid)
		            //{
		            //    return View("UpdateEmailSubscription", data);
		            //}
		            data.EmailLists = service.EmailSubscriptionListService.GetAll();
		            service.EmailSubscriptionListService.UpdateSubscription(data);
		            AddPopupMessage(
		                "Thank you for using our services, your email subscriptions have been successfully updated.",
		                "JHM Email Signup");
		            return Redirect("/");
		        });
		    }
		    else
		    {
		        data.EmailLists = (List<EmailSubscriptionList>)Session["EmailLists"];
		        ViewBag.ErrorMessage = "Please fill in all fields.";
                return View("UpdateEmailSubscription", data);
		    }
		}
    }
}
