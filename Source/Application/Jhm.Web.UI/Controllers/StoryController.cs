﻿#region

using System.Collections.Generic;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;

#endregion

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class StoryController : BaseController<IServiceCollection>
    {
        public StoryController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ActionResult Index(Client client)
        {
            var newsList = new StoryViewModel();
            newsList.StoryList = new List<Story>(ApplicationManager.ApplicationData.Stories.Values);
            return MapCartRenderView(client, "News", newsList);
            
        }

        public ActionResult View(Client client, string id)
        {
            var article = ApplicationManager.ApplicationData.Stories[id];
            return MapCartRenderView(client, "View", article);
        }
    }
}

