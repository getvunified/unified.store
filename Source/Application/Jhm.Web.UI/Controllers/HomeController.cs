﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using System.Text;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.UI.Models;
using Jhm.Web.UI.Recaptcha;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class HomeController : BaseController<IServiceCollection>
    {

        public HomeController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        [PageCss("page-front", "no-sidebars")]
        public ActionResult Index(Client client)
        {
            //ApplicationManager.ApplicationData.SetTheme();
            ViewData["Message"] = "Welcome to ASP.NET MVC!";  //For Testing Only

            return TryOnEvent(() =>
            {
                //if(IsDM)
                //{
                //    var model = new DMHomePageViewModel
                //    {
                //        BannerItems = new List<BannerItem>(ApplicationManager.ApplicationData.Banners.Values),
                //        FeaturedPhotos = new List<FeaturedPhoto>(ApplicationManager.ApplicationData.FeaturedPhotos.Values),
                //        FeaturedVideos = new List<FeaturedVideo>(ApplicationManager.ApplicationData.FeaturedVideos.Values),
                //        SocialMedia = new List<SocialMediaItem>(ApplicationManager.ApplicationData.SocialMediaItems.Values)
                //    };
                //    return View("DMIndex", model);
                //}
                //else
                //{
                //    var model = new HomeViewModel
                //    {
                //        HomeBannerItems = service.HomeBannerService.GetAllActiveByCompany(client.Company),
                //        LightCart = client.Cart,
                //        ActivePopup = ApplicationManager.ApplicationData.ActiveHomePagePopup,
                //    };

                //    model.ShouldShowLiveMessage = model.ActivePopup != null;
                //    return View("Index", model);
                //}
                return RedirectToAction("Index", "Shop");
            });
        }

        public ActionResult About(Client client, string id)
        {
            ActionResult view = null;
            switch (id)
            {
                case "PastorJohnHagee":
                    view = View("AboutPastorJohnHagee", new CartViewModel {LightCart = client.Cart});
                    break;
                case "PastorMatthewHagee":
                    view = View("AboutPastorMatthewHagee", new CartViewModel { LightCart = client.Cart });
                    break;
                case "DianaHagee":
                    view = View("AboutDianaHagee", new CartViewModel { LightCart = client.Cart });
                    break;
                case "Beliefs":
                    view = View("AboutBeliefs", new CartViewModel { LightCart = client.Cart });
                    break;
                case "WhySupportIsrael":
                    view = View("AboutWhySupportIsrael", new CartViewModel { LightCart = client.Cart });
                    break;
                case "TheDifference":
                    view = View("AboutTheDifference", new CartViewModel { LightCart = client.Cart });
                    break;
                default:
                    view = View(IsDM? "DMAbout" : "About",new CartViewModel { LightCart = client.Cart });
                    break;
            }
            return view;
        }

        public ActionResult ContactUsDifferenceMedia(Client client)
        {
            return MapCartRenderView(client, "ContactUsFormDifferenceMedia", new ContactInfoDifferenceMediaViewModel());
        }


        [HttpPost]
        public ActionResult ContactUsDifferenceMedia(Client client, ContactInfoDifferenceMediaViewModel contactInfo)
        {
            if (ModelState.IsValid)
            {
                var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/DifferenceMedia/ContactUs.html"));
                var auth = String.Empty;
                
                var replacements = new ListDictionary
                    {
                        {"<%NAME%>", contactInfo.Name ?? String.Empty},
                        {"<%PHONE%>", contactInfo.Phone ?? String.Empty},
                        {"<%EMAIL%>", contactInfo.Email ?? String.Empty},
                        {"<%MESSAGE%>", contactInfo.Message ?? String.Empty},
                        {"<%AUTHORIZATIONS%>", auth},
                        {
                            "<%IMAGES_PATH%>",
                            "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] +
                            Url.Content("/EmailTemplates/DifferenceMedia/images/")
                        }
                    };

                var mailDefinition = new MailDefinition
                    {
                        From = EmailHelper.EmailFromAccount,
                        IsBodyHtml = true,
                        Subject = "Difference Media - Contact Us Confirmation"
                    };

                var message = mailDefinition.CreateMailMessage(contactInfo.Email, replacements, body, new System.Web.UI.Control());
                message.IsBodyHtml = true;
                // Sending email to client
                service.EmailService.SendDMEmail(message);

                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("<h4>Contact Us Form</h4>");
                stringBuilder.Append(EmailHelper.MapObjectToString(contactInfo, typeof(ContactInfoDifferenceMediaViewModel)));
                stringBuilder.AppendLine("<br /><strong>Sender IP Address: </strong>" +
                              Request.ServerVariables[MagicStringEliminator.ServerVariables.RemoteIP]);
                message = new MailMessage(contactInfo.Email, EmailHelper.ContactUsEmailAccount,
                                      "New Contact Us Form Submitted", stringBuilder.ToString())
                    {
                        IsBodyHtml = true
                    };

                // Sending email to representatives
                service.EmailService.SendDMEmail(message);

                //NEED TO CHANGE TO ContactUsDifferenceMediaResponse
                return View("ContactUsResponseDifferenceMedia", new CartViewModel {LightCart = client.Cart});
            }

            return MapCartRenderView(client, "ContactUsFormDifferenceMedia", new ContactInfoDifferenceMediaViewModel());
        }

        public ActionResult ContactUs(Client client)
        {
            ActionResult cartRender;
            
            if(IsDM)
            {
                cartRender = MapCartRenderView(client, "ContactUsFormDifferenceMedia", new ContactInfoViewModel());
            }
            else
            {
                cartRender = MapCartRenderView(client, "ContactUsForm", new ContactInfoViewModel());
            }

            return cartRender;
        }

        [RecaptchaControlMvc.CaptchaValidatorAttribute]
        [HttpPost]
        public ActionResult ContactUs(Client client, ContactInfoViewModel contactInfo, bool captchaIsValid)
        {
            if (!captchaIsValid)
            {
                ModelState.AddModelError(MagicStringEliminator.Captcha.ValidationErrorFieldKey, "You did not type the verification words correctly. Please try again.");
            }

            if (ModelState.IsValid)
            {
                var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/contact.html"));
                var auth = String.Empty;
                //var auth = contactInfo.AuthorizeToSendPromotionalEmail
                //               ? "·  You have authorized us to email promotional information."
                //               : String.Empty;
                //auth += contactInfo.AuthorizeToShareInformation
                //            ? (String.IsNullOrEmpty(auth) ? String.Empty : "<br />") +
                //              "·  You have authorized us to share this information with our selected promotional partners."
                //            : String.Empty;
                //if (String.IsNullOrEmpty(auth))
                //{
                //    auth = "You have no authorizations";
                //}
                var replacements = new ListDictionary
                                       {
                                           {"<%MESSAGE%>", contactInfo.Comments ?? String.Empty},
                                           {"<%FIRST_NAME%>", contactInfo.FirstName ?? String.Empty},
                                           {"<%LAST_NAME%>", contactInfo.LastName ?? String.Empty},
                                           {
                                            "<%ADDRESS%>",
                                                contactInfo.Address1 +
                                                (String.IsNullOrEmpty(contactInfo.Address2)
                                                    ? String.Empty
                                                    : "<br />" + contactInfo.Address2)
                                            },
                                           {"<%CITY%>", contactInfo.City ?? String.Empty},
                                           {"<%STATE%>", contactInfo.State ?? String.Empty},
                                           {"<%ZIP%>", contactInfo.Zip ?? String.Empty},
                                           {"<%COUNTRY%>", contactInfo.Country ?? String.Empty},
                                           {"<%PHONE%>", contactInfo.Phone ?? String.Empty},
                                           {"<%EMAIL%>", contactInfo.Email ?? String.Empty},
                                           {"<%IMAGES_PATH%>", "http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/")}
                                       };

                var md = new MailDefinition
                             {
                                 From = EmailHelper.EmailFromAccount,
                                 IsBodyHtml = true,
                                 Subject = "John Hagee Ministries - Contact Us Confirmation"
                             };

                var msg = md.CreateMailMessage(contactInfo.Email, replacements, body, new System.Web.UI.Control());
                msg.IsBodyHtml = true;
                // Sending email to client
                service.EmailService.SendEmail(msg);

                var sb = new StringBuilder();
                sb.AppendLine("<h4>Contact Us Form</h4>");
                sb.Append(EmailHelper.MapObjectToString(contactInfo, typeof(ContactInfoViewModel)));
                sb.AppendLine("<br /><strong>Sender IP Address: </strong>" + Request.ServerVariables[MagicStringEliminator.ServerVariables.RemoteIP]);
                msg = new MailMessage(contactInfo.Email, EmailHelper.ContactUsEmailAccount, "New Contact Us Form Submitted", sb.ToString());
                msg.IsBodyHtml = true;
                // Sending email to representatives
                service.EmailService.SendEmail(msg);

                return View("ContactUsResponse", new CartViewModel { LightCart = client.Cart });
            }

            ActionResult cartRender;

            if (IsDM)
            {
                cartRender = MapCartRenderView(client, "ContactUsFormDifferenceMedia", new ContactInfoViewModel());
            }
            else
            {
                cartRender = MapCartRenderView(client, "ContactUsForm", new ContactInfoViewModel());
            }

            return cartRender;
        }
        
        //public string IpAddress()
        //{
        //    string strIpAddress;
        //    strIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (strIpAddress == null)
        //    {
        //        strIpAddress = Request.ServerVariables["REMOTE_ADDR"];
        //    }
        //    return strIpAddress;
        //}


        //public double IPAddressToNumber(string IPaddress)
        //{
        //    int i;
        //    string[] arrDec;
        //    double num = 0;
        //    if (IPaddress == "")
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        arrDec = IPaddress.Split('.');
        //        for (i = arrDec.Length - 1; i >= 0; i--)
        //        {
        //            num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
        //        }
        //        return num;
        //    }
        //}

        public ActionResult ContactInfo(Client client)
        {
            return View(new CartViewModel { LightCart = client.Cart });
        }
    }
}
