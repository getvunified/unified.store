﻿using Jhm.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jhm.Web.UI.Controllers
{
    //[RequireHttps(RequireSecure = false)]
    public class ErrorController : Controller
    {
        public bool IsDM { get
        {
            string hostname = Request.Url.Host;
            
            //DM-SWITCH
            //return true;
            return (hostname.Contains("differencemedia.org"));
        } }
        //
        // GET: /Error/

        public ActionResult Index(string id = null, string item = null, string xsid = null, string remainder = "")
        {
            LoadClient();

            switch (id)
            {
                case "301":
                    Response.StatusCode = 301;
                    switch (remainder.ToLower())
                    {
                        case "default.asp":
                            Response.RedirectLocation = "/";
                            break;
                        case "dirmod.asp":
                            switch (Request.QueryString.ToString())
                            {
                                case "sid=&nm=&type=Commerce&mod=GenComProductCatalog&mid=B4917A2439434EC69D686ECC2AA0136A&SiteId=21B0B031B0D748C79CC5376D4F7EF24C&tier=1":
                                    Response.RedirectLocation = "/Catalog";
                                    break;
                                case "sid=&type=gen&mod=Core+Pages&gid=E9F025178D754AB2AE33193BAF7903B5&SiteID=98614EAC31FA42C498745CE6581F8244":
                                    Response.RedirectLocation = "/Resources/OnlineProgramming";
                                    break;
                                case "SiteID=8112722C039B4E508F0AB8552B898895&sid=5A4949E5333C49F4946CBE082D20D0CA&type=contact&mod=Contact+Form&cfid=F434627D1A494A61A5CBC45A64D7F15F":
                                    Response.RedirectLocation = "/Home/ContactInfo";
                                    break;
                                case "sid=&type=gen&mod=Core+Pages&gid=CFF4455DADAC4DB6A1E518444392C3D7&SiteID=8112722C039B4E508F0AB8552B898895":
                                    Response.RedirectLocation = "/Home/About/Beliefs";
                                    break;
                                case "SiteID=8112722C039B4E508F0AB8552B898895&sid=44B1F4F45A21487384B5908703716A8B&nm=Program+Guide&type=ESpotlight&mod=Directories%3A%3ASpotlight&mid=0C0A17CACB074D638E44FBEB1F6A3CD2&tier=1":
                                    Response.RedirectLocation = "/Resources/OnlineProgramming";
                                    break;
                                case "SiteID=8112722C039B4E508F0AB8552B898895&sid=44B1F4F45A21487384B5908703716A8B&nm=Program+Guide&type=ESpotlight&mod=Directories%3a%3aSpotlight&mid=0C0A17CACB074D638E44FBEB1F6A3CD2&tier=1":
                                    Response.RedirectLocation = "/Resources/OnlineProgramming";
                                    break;
                                case "sid=&type=gen&mod=Core+Pages&gid=4EA7084FB53F4D74980C8D90DB43AA11&SiteID=4AC79C9B25B24DF3AF21C42311BE3921":
                                    Response.RedirectLocation = "/Catalog";
                                    break;
                                case "sid=&nm=&type=Commerce&mod=GenComProductCatalog&mid=B4917A2439434EC69D686ECC2AA0136A&SiteId=21B0B031B0D748C79CC5376D4F7EF24C&tier=2&Lvl1=Featured":
                                    Response.RedirectLocation = "/Catalog";
                                    break;
                                case "SiteID=98614EAC31FA42C498745CE6581F8244&id=185F6B2D6B7047B18E93706D30B0CCD5&level=1&mod=Information+Request&nm=Form&sid=24D0395FBC774DA2B04DF605DFCEFBA0&type=InfoRequest":
                                    Response.RedirectLocation = "/Resources/PrayerRequest";
                                    break;
                                case "SiteID=4AC79C9B25B24DF3AF21C42311BE3921&sid=&nm=&type=news&mod=News&mid=9A02E3B96F2A415ABC72CB5F516B4C10&tier=1":
                                    Response.RedirectLocation = "/Resources/LatestNews";
                                    break;
                                case "sid=&nm=&type=news&mod=News&mid=9A02E3B96F2A415ABC72CB5F516B4C10&tier=3&nid=50E9652485EB4CEA856A447CAF4AF1E9&SiteID=4AC79C9B25B24DF3AF21C42311BE3921":
                                    Response.RedirectLocation = "/Resources/LatestNews";
                                    break;
                                case "sid=AFFD33FE6E484D178BCE8C3C991EEA26&nm=About+Us&type=faqs&mod=FAQs&mid=F1DC381E34834576AFAE6187F8CBC46F&tier=1&SiteID=8112722C039B4E508F0AB8552B898895":
                                    Response.RedirectLocation = "/faqs";
                                    break;
                                default:
                                    Response.RedirectLocation = "/";
                                    break;
                            }
                            
                            break;

                        default:
                            Response.RedirectLocation = "/";
                            break;

                    }
                    return new ContentResult();

                case "400":
                    ViewData["ErrorTitle"] = "Bad request";
                    ViewData["ErrorMessage"] = "<h2>Sorry, the request could not be understood by the server due to malformed syntax.</h2>";
                    break;
                case "401":
                    ViewData["ErrorTitle"] = "Access denied";
                    ViewData["ErrorMessage"] = "<h2>Sorry, access to the requested page has been denied.</h2>";
                    break;
                case "403":
                    ViewData["ErrorTitle"] = "Forbidden";
                    ViewData["ErrorMessage"] = "<h2>Sorry, the requested page or resource is currently forbidden.</h2>";
                    break;
                case "404":
                    //Response.StatusCode = 404;
                    ViewData["ErrorTitle"] = "Page Not Found";
                    ViewData["ErrorMessage"] = "<h2>Sorry, the page you are requesting can not be found or has been moved.</h2>";
                    break;
                case "405":
                    ViewData["ErrorTitle"] = "Method Not Allowed";
                    ViewData["ErrorMessage"] = "<h2>Sorry, the requested method is not allowed.</h2>";
                    break;
                case "406":
                    ViewData["ErrorTitle"] = "MIME type not supported";
                    ViewData["ErrorMessage"] = "<h2>Your browser does not accept the MIME type of the requested page.</h2>";
                    break;
                case "408":
                    ViewData["ErrorTitle"] = "Request timed out";
                    ViewData["ErrorMessage"] = "<h2>Sorry, the requested page has timed out. The server is too busy at this moment, please try again later.</h2>";
                    break;
                case "412":
                    ViewData["ErrorTitle"] = "Precondition failed";
                    ViewData["ErrorMessage"] = "<h2>Sorry, a precondition to execute the requested page or resource has failed.</h2>";
                    break;


                case "dirmod.asp":
                    Response.StatusCode = 301;
                    Response.RedirectLocation = "/";
                    return new ContentResult();
                case "podcast":
                    Response.StatusCode = 301;
                    Response.RedirectLocation = string.Format("http://feeds.jhm.org/Media/PodcastItems/{0}", item);
                    return new ContentResult();

                case "podcastfeed":
                    Response.StatusCode = 301;
                    Response.RedirectLocation = string.Format("http://feeds.jhm.org/ME2/Console/Podcast/{0}?xsid={1}", item, xsid);
                    return new ContentResult();
                case "rss":
                    Response.StatusCode = 301;
                    Response.RedirectLocation = string.Format("http://feeds.jhm.org/ME2/Console/XmlSyndication/Display/{0}?xsid={1}", remainder, xsid);
                    return new ContentResult();

                default:
                    ViewData["ErrorTitle"] = "Unknown Error";
                    ViewData["ErrorMessage"] = "<h2>Sorry, an unknown error ocurred.</h2>";
                    break;
            }
            return View("Index");
        }

        private void LoadClient()
        {
            var client = TempData[ClientModelBinder.clientSessionKey] ?? new ClientModelBinder().LoadClientInContext(ControllerContext); 
            ViewData[MagicStringEliminator.ViewDataKeys.Client] = client;
        }




    }
}
