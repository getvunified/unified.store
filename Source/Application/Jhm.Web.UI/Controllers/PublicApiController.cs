﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Service;
using Jhm.Web.UI.Areas.Admin.Controllers;

namespace Jhm.Web.UI.Controllers
{
    public class PublicApiController : BaseController<IServiceCollection>
    {
        private IUnitOfWorkContainer myUnitOfWork;

        public PublicApiController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
            myUnitOfWork = unitOfWork;
        }
        //
        // GET: /PublicApi/

        public ActionResult ReloadProductsFromAllEnvironments()
        {
            return new RestfulActionsController(service, myUnitOfWork).ReloadProductsFromAllEnvironments();
        }

        public ActionResult ReloadSubscriptionsFromAllEnvironments()
        {
            return new RestfulActionsController(service, myUnitOfWork).ReloadSubscriptionsFromAllEnvironments();
        }

    }
}
