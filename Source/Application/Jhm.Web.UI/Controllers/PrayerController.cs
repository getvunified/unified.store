﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml.XPath;
using System.Xml.Xsl;
using AlexJamesBrown.JoeBlogs.Structs;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Service;
using System.Net.Mail;
using System.Collections.Specialized;
using Jhm.Web.UI.Admin.Models;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using Jhm.Web.UI.Recaptcha;
using AlexJamesBrown.JoeBlogs;
using iTextSharp.text.pdf;
using System.Configuration;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    [PageCss("section-store", "sidebar-second")]
    //[RequireHttps(RequireSecure = false)]
    public class PrayerController: BaseController<IServiceCollection>
    {
        public PrayerController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ActionResult Index()
        {
            return Redirect("/Resources/PrayerRequest ");
        }
    }


}



