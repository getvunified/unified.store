﻿using System;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;
using Jhm.Web.UI.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Controllers
{
    //[RequireHttps(RequireSecure = true)]
    public class GiftCardsController : BaseController<IServiceCollection>
    {
        

        public GiftCardsController(IServiceCollection service, IUnitOfWorkContainer unitOfWork): base(service, unitOfWork) {}


        //
        // GET: /GiftCards/
        public ActionResult Index(Client client)
        {
            if (client.Company != Company.JHM_UnitedStates)
            {
                return View("GiftCardsUnavailable");
            }

            if (IsDM)
            {
                return View("GiftCardRedirectFromDM");
            }

            var giftCards = service.GiftCardService.GetAvailableGiftCards();
            return View(giftCards);
        }

        public ActionResult Order(Client client, Guid id)
        {
            if (client.Company != Company.JHM_UnitedStates)
            {
                return View("GiftCardsUnavailable");
            }

            if (IsDM)
            {
                return View("GiftCardRedirectFromDM");
            }

            return TryOnEvent(()=>
            {
                var giftCard = service.GiftCardService.GetGiftCard(id);

                if (giftCard == null)
                {
                    AddPopupMessage("Sorry, Gift card not found", "JHM Gift Cards - Order Gift Card");
                    return RedirectToAction("Index");
                }

                var model = new OrderGiftCardViewModel {GiftCard = giftCard, GiftCardId = giftCard.Id};

                var user = SessionManager.CurrentUser;
                if (user != null)
                {
                    if (user.AccountNumber != 0)
                    {
                        var dsEnviroment = DetermineDSEnvironment(user, client);
                        model.AddressList = service.AccountService.GetUserAddresses(
                            user.Username, 
                            user.ApplicationName,
                            dsEnviroment);
                    }
                }

                decimal cardValue;
                if (Request.QueryString["CardValue"].IsNot().NullOrEmpty() && Decimal.TryParse(Request.QueryString["CardValue"], out cardValue))
                {
                    model.CardValue = cardValue;
                }

                short numberOfCards;
                if (Request.QueryString["NumberOfCards"].IsNot().NullOrEmpty() && short.TryParse(Request.QueryString["NumberOfCards"], out numberOfCards))
                {
                    model.NumberOfCards = numberOfCards;
                }

                GiftCardDeliveryTypes deliveryType;
                if (Request.QueryString["CardDeliveryType"].IsNot().NullOrEmpty() && Enum.TryParse(Request.QueryString["CardDeliveryType"], out deliveryType))
                {
                    model.CardDeliveryType = deliveryType;
                }

                return View(model);
            });
        }

        [HttpPost]
        public ActionResult Order(Client client, OrderGiftCardViewModel model)
        {
            if (client.Company != Company.JHM_UnitedStates)
            {
                return View("GiftCardsUnavailable");
            }

            if (IsDM)
            {
                return View("GiftCardRedirectFromDM");
            }

            return TryOnEvent(()=>
            {
                var giftCard = service.GiftCardService.GetGiftCard(model.GiftCardId);

                if (giftCard == null)
                {
                    AddPopupMessage("Sorry, Gift card not found", "JHM Gift Cards - Order Gift Card");
                    return RedirectToAction("Index");
                }

                model.GiftCard = giftCard;

                if (ModelState.IsValid)
                {
                    if (model.CardDeliveryType == GiftCardDeliveryTypes.ToOwnAddress)
                    {
                        var dsEnviroment = DetermineDSEnvironment(SessionManager.CurrentUser, client);
                        var dsAddress = service.AccountService.GetUserAddressById(model.DeliveryAddressId.GetValueOrDefault(), dsEnviroment);
                        if (dsAddress == null)
                        {
                            TempData[MagicStringEliminator.Messages.ErrorMessage] = "The selected delivery address was not found";
                            return View(model);
                        }
                        model.DeliveryAddress = dsAddress;
                        model.ShoppingCartDescription = String.Format(
                            "<b>Delivery Address: </b>{0}<br/>{1} {2}, {3}",
                            model.DeliveryAddress.Address1,
                            model.DeliveryAddress.City,
                            model.DeliveryAddress.StateCode,
                            model.DeliveryAddress.PostalCode
                            );
                    }
                    else
                    {
                        model.ShoppingCartDescription = String.Format(
                            "<b>Recipient: </b>{0} {1}<br/><b>Delivery Address: </b>{2}<br/>{3} {4}, {5}",
                            model.RecipientFirstName,
                            model.RecipientLastName,
                            model.DeliveryAddress.Address1,
                            model.DeliveryAddress.City,
                            model.DeliveryAddress.StateCode,
                            model.DeliveryAddress.PostalCode
                            );
                    }

                    var cartItem = new GiftCardCartItem { GiftCardFormInfo = model };
                    cartItem.MapCartItem(model);
                    try
                    {
                        client.Cart.AddItem(cartItem, model.NumberOfCards);
                    }
                    catch (Exception ex)
                    {
                        var msg =
                            @"Updating Cart was unsuccessful. Please correct the errors and try again.
                                                        <ul><li>{0}</li><ul>".FormatWith(ex.Message);
                        TempData[MagicStringEliminator.Messages.ErrorMessage] = msg;

                        //BUG:  Should model state be set to false and message instead?
                    }
                    return RedirectToCartPage(eMessage: null, catalogRtrToUrl: null, donationsRtrToUrl: null);
                }

                var user = SessionManager.CurrentUser;
                if (user != null)
                {
                    if (user.AccountNumber != 0)
                    {
                        var dsEnviroment = DetermineDSEnvironment(user, client);
                        model.AddressList = service.AccountService.GetUserAddresses(
                            user.Username,
                            user.ApplicationName,
                            dsEnviroment);
                    }
                }

                return View(model);
            });
        }


        public ActionResult CheckBalance()
        {
            return View(new CheckGiftCardViewModel());
        }

        [HttpPost]
        public ActionResult CheckBalance(CheckGiftCardViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Balance = 0;
                try
                {
                    model.Balance = service.GiftCardService.GetBalance(model.Number, model.Pin);
                }
                catch (Exception e)
                {
                    Log.For(this).Erroneous(e);

                }
                return View("ViewBalance", model);
            }
            return View(model);
        }
        
    }
}
