﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;

namespace Jhm.Web.UI.Controllers
{
    [HandleError]
    //[RequireHttps(RequireSecure = false)]
    public class DigitalMediaController : BaseController<IServiceCollection>
    {

        private readonly StringIgnoreCaseComparer<String> _ignoreCaseComparer = new StringIgnoreCaseComparer<string>();


        public DigitalMediaController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork)
        {
        }

        [Authorize]
        public ActionResult Download(Client client, string mediaId = null)
        {
            IsIOSDevice = false;
            bool canDownload = Session[mediaId] != null;

            if (ModelState.IsValid && canDownload)
            {
                return TryOnEvent(() =>
                {
                    try
                    {
                        if (mediaId == null)
                        {
                            throw new ApplicationException("Error processing download.  Please check URL.");
                        }

                        var digitalDownload = service.DigitalDownloadService.GetDigitalDownload(new Guid(mediaId));
                        
                        if (digitalDownload == null)
                        {
                            throw new ApplicationException("This download does not exist.");
                        }
                        if (digitalDownload.ExpirationDate < DateTime.Now)
                        {
                            throw new ApplicationException("This download link has expired and is no longer valid.");
                        }
                        if (digitalDownload.DownloadAttemptsRemaining <= 0)
                        {
                            throw new ApplicationException(
                                "No more download attempts are available for this purchase.");
                        }


                        var stockItem = service.CatalogService.GetStockItem(client.Company.DonorStudioEnvironment,
                                                                            digitalDownload.ProductCode,
                                                                            MediaType.GetMediaTypeByCodeSuffix(
                                                                                digitalDownload.MediaType));

                        if (stockItem == null)
                        {
                            var message =
                                String.Format("No StockItem for ProductCode:{0} and MediaType:{1} could be found.",
                                              digitalDownload.ProductCode, digitalDownload.MediaType);
                            throw new ApplicationException(message);
                        }

                        if (Request.UserAgent != null)
                        {
                            var userAgent = Request.UserAgent.ToLower();
                            if (userAgent.Contains("iphone") || userAgent.Contains("ipad") ||
                                userAgent.Contains("ipod"))
                            {
                                IsIOSDevice = true;
                            }
                        }
                        if (!IsIOSDevice)
                        {
                            Session["currentUserMp3DownloadContext"] = HttpContext.Response;
                            DownloadFile(stockItem.MediaPath, HttpContext);
                        }
                    }
                    catch (Exception ex)
                    {
                        Session["currentUserMp3DownloadException"] = "Sorry, " + ex.Message;
                        /*TempData[MagicStringEliminator.Messages.ErrorMessage] = "Sorry, " + ex.Message;
                        var popupMessage = new PopupMessageViewModel();
                        popupMessage.Title = "MP3 Download Failed";
                        popupMessage.Message = "Sorry, " + ex.Message;
                        AddPopupMessage(popupMessage);*/
                        
                        Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                        //throw;

                        return Content(ex.Message);
                    }

                    return null;
                });
            }
            return null;
        }
        
        public void DownloadFile(string name, HttpContextBase context)
        {
            name = name.Replace(@"W:\DOWNLOADS\MP3\", @"\\10.15.10.7\W$\DOWNLOADS\MP3\");

            var file = new FileInfo(name);
            if (!file.Exists)
            {
                throw new ApplicationException("File cannot be found: " + name);
            }

            context.Response.Clear();
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            context.Response.AddHeader("Content-Length", file.Length.ToString(CultureInfo.InvariantCulture));
            context.Response.AddHeader("Accept-Ranges", "bytes");
            //context.Response.AddHeader("Connection", "Keep-Alive");

            context.Response.ContentType = "application/octet-stream";
            context.Response.ContentEncoding = Encoding.UTF8;

            context.Response.TransmitFile(file.FullName);

            context.Response.Flush();
            context.Response.End();
            context.Response.Clear();
            
            if (Session["currentUserMp3DownloadContext"] != context.Response)
            {
                Session["currentUserMp3DownloadContext"] = "complete";
            }
        }


        [HttpPost]
        [Authorize]
        public ActionResult UpdateDownloadAttempts(string mediaId)
        {
            if (Session["currentUserMp3DownloadException"] != null)
            {
                var json = Json(new {status = "error", error = Session["currentUserMp3DownloadException"]});
                Session.Remove("currentUserMp3DownloadException");
                Session.Remove("currentUserMp3DownloadContext");

                return json;
            }
            if (Session["currentUserMp3DownloadContext"] != null && Session["currentUserMp3DownloadContext"].Equals("complete"))
            {
                return Json(new {status = "complete"});
            }

            return Json(new { status = "notComplete" }); ;
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateDownloadTime(string mediaId, int seconds)
        {
            var digitalDownload = new DigitalDownload();
            TryWithTransaction(() =>
            {

                digitalDownload = service.DigitalDownloadService.GetDigitalDownload(new Guid(mediaId));
                digitalDownload.DownloadAttemptsRemaining--;

                int storedSeconds = digitalDownload.AverageDownloadTimeInSeconds;
                storedSeconds += seconds;

                if (digitalDownload.AverageDownloadTimeInSeconds == 0)
                {
                    digitalDownload.AverageDownloadTimeInSeconds = storedSeconds;    
                }
                else
                {
                    digitalDownload.AverageDownloadTimeInSeconds = storedSeconds / 2;
                }


                

                Session["currentUserMp3DownloadContext"] = "newSession";

            });

            return Json(new { attempts = digitalDownload.DownloadAttemptsRemaining, status = "complete" });
        }

        private bool IsIOSDevice { get; set; }
    }
}
