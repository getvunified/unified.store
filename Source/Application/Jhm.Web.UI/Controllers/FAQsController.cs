﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;

namespace Jhm.Web.UI.Controllers
{
    //[RequireHttps(RequireSecure = false)]
    public class FAQsController : BaseController<IServiceCollection>
    {
        //
        // GET: /FAQs/
        private StringIgnoreCaseComparer<String> ignoreCaseComparer = new StringIgnoreCaseComparer<string>();


        public FAQsController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork)
        {
        }

        public ActionResult Index()
        {
            var searhUsingTypes = from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
                                  select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
            ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name", SearchUsingTypes.AllWords);
            ViewData["ShouldShowResultsCount"] = false;

            return View(new FAQViewModel());
        }

        [HttpPost]
        public ActionResult Index(FAQViewModel searchData)
        {
            if ( String.IsNullOrEmpty(searchData.SearchFor) )
            {
                ModelState.AddModelError("SearchFor", "* Search criteria is required");
            }
            IEnumerable<FAQ> filteredFaqs = null;
            if (ModelState.IsValid)
            {
                const string splitWordsPattern = @"\W";
                var searchWords = Regex.Split(searchData.SearchFor, splitWordsPattern);


                var allFaqs = searchData.GetAll();
                

                searchData.SearchUsing = (SearchUsingTypes) searchData.SearchUsingAsInt;

                switch (searchData.SearchUsing)
                {
                    case SearchUsingTypes.AllWords:
                        filteredFaqs = allFaqs.Where(x =>
                                                         {
                                                             var questionWords = Regex.Split(x.Question,
                                                                                             splitWordsPattern);
                                                             var answerWords = Regex.Split(x.Answer, splitWordsPattern);
                                                             bool questionContainsSearch =
                                                                 questionWords.ContainsAllOf(searchWords,
                                                                                             ignoreCaseComparer);
                                                             bool answerContainsSearch =
                                                                 answerWords.ContainsAllOf(searchWords,
                                                                                           ignoreCaseComparer);
                                                             return questionContainsSearch || answerContainsSearch;
                                                             //return questionWords.ContainsAllOf(searchWords) || answerWords.ContainsAllOf(searchWords);
                                                         });
                        break;
                    case SearchUsingTypes.AnyWord:
                        filteredFaqs = allFaqs.Where(x =>
                                                         {
                                                             var questionWords = Regex.Split(x.Question,
                                                                                             splitWordsPattern);
                                                             var answerWords = Regex.Split(x.Answer, splitWordsPattern);
                                                             return
                                                                 questionWords.ContainsAnyOf(searchWords,
                                                                                             ignoreCaseComparer) ||
                                                                 answerWords.ContainsAnyOf(searchWords,
                                                                                           ignoreCaseComparer);
                                                         });
                        break;
                    case SearchUsingTypes.ExactPhrase:
                        filteredFaqs =
                            allFaqs.Where(
                                x =>
                                x.Question.IndexOf(searchData.SearchFor, StringComparison.InvariantCultureIgnoreCase) >=
                                0 ||
                                x.Answer.IndexOf(searchData.SearchFor, StringComparison.InvariantCultureIgnoreCase) >= 0);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            var searhUsingTypes = from SearchUsingTypes s in Enum.GetValues(typeof(SearchUsingTypes))
                                  select new { ID = (int)s, Name = TypeHelper.GetDisplayName(s) };
            ViewData["SearchUsingList"] = new SelectList(searhUsingTypes, "ID", "Name", searchData.SearchUsing.ToString());
            searchData.SearchUsingAsInt = (int) searchData.SearchUsing;
            ViewData["ShouldShowResultsCount"] = ModelState.IsValid;
            ViewData["SearchForText"] = searchData.SearchFor;
            ViewData["SearchUsing"] = searchData.SearchUsing;

            return View("Index",FAQViewModel.With(filteredFaqs));
        }


        

        public ActionResult View(string id)
        {
            var list = new FAQViewModel();
            var faq = list.GetById(id);
            if ( faq.IsNull() )
            {
                return View("Index", list);
            }
            return View("ViewFaq", faq);
        }

        
    }

    

    
}
