using System;
using StructureMap;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class Log
    {
        public static ILog For(object itemThatRequiresLoggingServices)
        {
            return For(ObjectFactory.GetInstance<IContainer>(), itemThatRequiresLoggingServices.GetType());
        }

        public static ILog For(IContainer container, Type type)
        {
            return container.GetInstance<ILogFactory>().CreateFor(type);
        }

        public static ILog For<T>()
        {
            return For(typeof(T));
        }
    }
}