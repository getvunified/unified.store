using System.Web.Profile;
using System.Web.Security;
using Jhm.Web.Repository.Modules.Account;
using StructureMap.Configuration.DSL;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class MembershipRegistry : Registry
    {
        public MembershipRegistry()
        {
            //RegisterMembershipProvideRepositories();


            //For<MembershipProvider>().Use(Membership.Provider);
            //For<RoleProvider>().Use(Roles.Provider);
            //For<ProfileProvider>().Use(ProfileManager.Provider);
        }


        private void RegisterMembershipProvideRepositories()
        {
            For<IUserRepository>().Use<UserRepository>();
            For<IRoleRepository>().Use<RoleRepository>();
            For<IProfileRepository>().Use<ProfileRepository>();
        }
    }
}