﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Jhm.Common.BootStrapper;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using Jhm.Web.UI.IBootStrapperTasks;

namespace Jhm.Web.UI.BootStrapperTasks
{
    [BootstrapperPriority(Priority = 1)]
    public class RegisterSecurityUser : IBootstrapperTask
    {
        private IUnitOfWorkContainer unitOfWork;
        public void Execute()
        {
            var serviceCollection = DependencyResolver.Current.GetService<IServiceCollection>();
            unitOfWork = DependencyResolver.Current.GetService<IUnitOfWorkContainer>();
            var service = serviceCollection.SecurityService;
            service.SuccessfulAuthenticationEvent += (sender, args) =>
            {
                serviceCollection.FormsService.SignIn(args.User.Username, true);

                 
                
                var user = new User();
                TryWithTransaction(() =>
                                       {
                                           user = serviceCollection.AccountService.GetUserByGlobalId(args.User.Id);
                                       });

                if (!user.IsTransient())
                {
                    TryWithTransaction(() =>
                                           {
                                               user.Profile.FirstName = args.User.FirstName;
                                               user.Profile.LastName = args.User.LastName;
                                               user.Email = args.User.EmailAddress;
                                               user.Username = args.User.Username;
                                               user.GlobalId = args.User.Id;
                                               user.AccountNumber = args.User.DonorAccountNumber;
                                               user.Password = args.User.Password;
                                               //Will need to address when we address the issue of changing companies
                                               user.DsEnvironment = args.User.DonorEnvironment;
                                               serviceCollection.AccountService.SaveUser(user, false, user.DsEnvironment);
                                               //NOTE:  Telling system NOT to save global.
                                           });
                }
                else
                {
                    var result = MembershipCreateStatus.UserRejected;
                    TryWithTransaction(() =>
                                           {
                                               result =
                                                   serviceCollection.MembershipService.CreateUserWithGlobalId(
                                                       args.User.Id, args.User.Username, "password",
                                                       args.User.EmailAddress, "question", "answer");

                                           });
                    if (result == MembershipCreateStatus.Success)
                    {
                        TryWithTransaction(() =>
                                               {
                                                   user =
                                                       serviceCollection.AccountService.GetUserByGlobalId(args.User.Id);

                                                   user.Profile.FirstName = args.User.FirstName;
                                                   user.Profile.LastName = args.User.LastName;
                                                   user.Email = args.User.EmailAddress;
                                                   user.Username = args.User.Username;
                                                   user.GlobalId = args.User.Id;
                                                   user.AccountNumber = args.User.DonorAccountNumber;
                                                       //Will need to address when we address the issue of changing companies
                                                   user.DsEnvironment = args.User.DonorEnvironment;
                                                   serviceCollection.AccountService.SaveUser(user, false,
                                                                                             user.DsEnvironment);
                                                       //NOTE:  Telling system NOT to save global.
                                               });
                    }
                }
                
                SessionManager.ClearCurrentUser();
                serviceCollection.FormsService.SignIn(args.User.Username, true);
            };
        }




        public void TryWithTransaction(Action method)
        {
            try
            {
                using (unitOfWork.Start())
                {
                    if (unitOfWork.CurrentUOW.IsInTransaction)
                    {
                        method();
                    }
                    else
                    {
                        using (ITransaction tx = unitOfWork.CurrentUOW.BeginTransaction())
                        {
                            try
                            {
                                method();
                                tx.Commit();
                            }
                            catch
                            {
                                tx.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.For(this).Erroneous(
                    "[TryWithTransaction]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}", e.Message,
                    e.StackTrace, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url);
                throw;
            }
        }
    }
}