﻿using System.Web.Mvc;
using getv.donorstudio.core.Global;
using Jhm.Common.BootStrapper;
using Jhm.Web.Core;
using Jhm.Web.Service;
using Jhm.Web.UI.MVCCommon;

namespace Jhm.Web.UI.BootStrapperTasks
{
    [BootstrapperPriority(Priority = 10)]
    public class LoadAllSubscriptionsToJSON
    {
        protected IServiceCollection Service;

        public void Execute()
        {
            Service = DependencyResolver.Current.GetService<IServiceCollection>();

            LoadAllSubscriptionsUnitedStates();
            LoadAllSubscriptionsCanada();
            LoadAllSubscriptionsUnitedKingdom();
        }

        private void LoadAllSubscriptionsUnitedStates()
        {
            var usSubscriptions = Service.SubscriptionService.LoadAllSubscriptions(Company.JHM_UnitedStates.DonorStudioEnvironment, false);
            usSubscriptions.SaveToCache(Company.JHM_UnitedStates.DonorStudioEnvironment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);
        }

        private void LoadAllSubscriptionsCanada()
        {
            var canadianSubscriptions = Service.SubscriptionService.LoadAllSubscriptions(Company.JHM_Canada.DonorStudioEnvironment, false);
            canadianSubscriptions.SaveToCache(Company.JHM_Canada.DonorStudioEnvironment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);
        }

        private void LoadAllSubscriptionsUnitedKingdom()
        {
            var ukSubscriptions = Service.SubscriptionService.LoadAllSubscriptions(Company.JHM_UnitedKingdom.DonorStudioEnvironment, false);
            ukSubscriptions.SaveToCache(Company.JHM_UnitedKingdom.DonorStudioEnvironment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);
        }
    }
}