﻿using System;
using System.Configuration;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.em3.Core;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository;
using StructureMap.Configuration.DSL;
using getv.donorstudio.core.Global;
using getv.donorstudio.core.eCommerce;
using CreditCard = Jhm.Web.Core.Models.CreditCard;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class FluentNHibernateConfigurationRegistry : Registry
    {
        public FluentNHibernateConfigurationRegistry()
        {
            For<IPersistenceConfigurer>().Use(MsSqlConfiguration.
                                                  MsSql2008
                                                  .ShowSql()
                                                  .UseReflectionOptimizer()
                                                  .Cache(s => s.UseQueryCache())
                                                  .ConnectionString(c => c.FromConnectionStringWithKey(ConfigurationManager.AppSettings["ConnectionType"])));

            For<Action<MappingConfiguration>>().Use(x => ConfigureNHibernate());


        }

        Action<MappingConfiguration> ConfigureNHibernate()
        {
            return map =>
            {
                map.FluentMappings.AddFromAssemblyOf<DefaultEntity>();
                map.AutoMappings.Add(
                    AutoMap.AssemblyOf<DefaultEntity>(new CustomAutomappingConfiguration())

                        .IgnoreBase<DefaultEntity>()

                        .Conventions.Add(

                            PrimaryKey.Name.Is(x => "Id"),
                            DefaultAccess.Property(), //.CamelCaseField(),
                            DefaultLazy.Always(),
                            DefaultCascade.None(),
                            ForeignKey.EndsWith("Id"),

                            ConventionBuilder.HasManyToMany.Always(x =>
                            {
                                x.Access.CamelCaseField();
                            }),
                            ConventionBuilder.HasMany.Always(x =>
                            {
                                x.Access.CamelCaseField();
                                x.Cascade.AllDeleteOrphan();
                            }),

                            //ConventionBuilder.Reference.Always(x => x.Access.CamelCaseField()),

                            ConventionBuilder.Reference.Always(x => x.Access.Property()),
                            ConventionBuilder.Property.Always(
                                x => { if (x.Type == typeof (byte[])) x.Length(2147483647); })



                        )


                        .Override<CartItem>(x => x.DiscriminateSubClassesOnColumn("CartItemType"))


                        .Override<User>(x => x.HasManyToMany(p => p.Roles).Cascade.All().Table("[UserInRoles]"))
                        .Override<Role>(
                            x => x.HasManyToMany(p => p.UsersInRole).Cascade.All().Inverse().Table("[UserInRoles]"))
                        .Override<CreditCard>(x => x.Table("[UserCreditCards]"))

                        .Override<User>(x => x.IgnoreProperty(u => u.TransactionHistory))
                        .Override<DigitalDownload>(x => x.IgnoreProperty(u => u.ExpirationDateFormatted))
                        .Override<User>(x => x.References(p => p.Profile).Cascade.All())
                        //Save/Update/Delete Profile on User
                        .Override<User>(x => x.References(p => p.Cart).Cascade.All())
                        //Save/Update/Delete Profile on User


                        .Override<HomeBannerCompany>(x =>
                        {
                            x.Table("[BannerCompany]");
                            x.IgnoreProperty(c => c.Company);
                            x.HasManyToMany(c => c.BannerItems)
                                .ParentKeyColumn("HomeBannerCompanyId")
                                .ChildKeyColumn("HomeBannerItemId")
                                .Not.LazyLoad()
                                .Cascade.All()
                                .Table("[CompanyHomeBannerItems]");
                        })
                        .IgnoreBase<Company.DataKeys>()
                        .IgnoreBase<Subscription>()
                        .IgnoreBase<SubscriptionPrice>()
                        .IgnoreBase<GiftCard>()

                        .Override<HomeBannerItem>(x =>
                        {
                            x.Table("[HomeBannerItems]");
                            x.HasManyToMany(b => b.Companies)
                                .ParentKeyColumn("HomeBannerItemId")
                                .ChildKeyColumn("HomeBannerCompanyId")
                                .Not.LazyLoad()
                                .Cascade.All()
                                .Inverse()
                                .Table("[CompanyHomeBannerItems]");
                            x.IgnoreProperty(c => c.ShowExpired);
                        })
                        .Override<PromotionalItem>(x => x.Table("[PromotionalItems]"))
                        .Override<DiscountCodeItems>(x => x.Table("[DiscountCodeItems]"))
                        .Override<SingleUseDiscountCodes>(x =>
                        {
                            x.Table("[SingleUseDiscountCodes]");
                            x.HasOne(y => y.DiscountCode).ForeignKey("DiscountCodeId");
                        })
                        .Override<DiscountCodeOptionalCriteria>(x =>
                        {
                            x.Table("[DiscountCodeOptionalCriteria]");
                            x.HasOne(y => y.DiscountCode).ForeignKey("DiscountCodeId");
                            x.IgnoreProperty(y => y.Satisfied);

                            // Ignored because is an enum
                            x.IgnoreProperty(y => y.AppliesToClass);
                        })
                        .IgnoreBase<DiscountCodeOptionalCriteria.ProductOrStock>()
                        .IgnoreBase<DiscountCodeOptionalCriteria.SupportedDataTypes>()
                        .Override<DiscountCodeRequirements>(x =>
                        {
                            x.Table("[DiscountCodeRequirements]");
                            x.HasOne(y => y.DiscountCode).ForeignKey("DiscountCodeId");
                            x.Not.LazyLoad();
                            x.IgnoreProperty(y => y.IsDirty);
                            x.Id(y => y.Id);
                        })


                        .Override<DiscountCode>(x =>
                        {
                            x.HasMany(y => y.Rules);
                            //x.HasOne(y => y.Rules).PropertyRef(y => y.DiscountCodeId).ForeignKey("RulesId");
                            x.IgnoreProperty(y => y.ActiveRule);
                            x.Not.LazyLoad();
                        })

                        .Override<RegistrationPasswordQuestion>(
                            x => x.Id(p => p.Value).Column("RegistrationQuestionId").GeneratedBy.Assigned())
                        .Override<ShippingMethod>(
                            x => x.Id(p => p.Value).Column("ShippingMethodId").GeneratedBy.Assigned())
                        .Override<MediaType>(x => x.Id(p => p.Value).Column("MediaTypeId").GeneratedBy.Assigned())
                        .Override<Category>(x => x.Id(p => p.Value).Column("CategoryId").GeneratedBy.Assigned())
                        .Override<ReviewStatus>(x => x.Id(p => p.Value).Column("ReviewStatusId").GeneratedBy.Assigned())


                        //Enumeration<T> Overrides
                        .Override<DonationCartItem>(
                            x => x.Map(t => t.DonationFrequency).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<Address>(
                            x => x.Map(t => t.DSAddressType).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<Phone>(
                            x => x.Map(t => t.PhoneType).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<Cart>(
                            x => x.Map(t => t.ShippingMethod).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<Cart>(x =>
                        {
                            x.IgnoreProperty(c => c.DiscountApplied);
                            x.IgnoreProperty(c => c.DiscountTotal);
                            x.IgnoreProperty(c => c.DiscountErrorMessage);
                            x.IgnoreProperty(c => c.AvailablePromotionalItems);
                            x.IgnoreProperty(c => c.AvailableDiscountItems);
                            x.IgnoreProperty(c => c.PromotionalItems);
                            x.IgnoreProperty(c => c.DiscountItems);

                        })
                        .Override<StockItem>(
                            x => x.Map(t => t.MediaType).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<Review>(
                            x => x.Map(t => t.ReviewStatus).CustomType(typeof (string)).Access.CamelCaseField())
                        .Override<CreditCard>(x =>
                        {
                            x.Map(t => t.CardType).CustomType(typeof (string)).Access.CamelCaseField();
                            x.IgnoreProperty(p => p.Value);
                            x.IgnoreProperty(p => p.DisplayName);
                        })

																		.Override<EmailSubscriptionList>(x =>
																		{
																			x.Table("[EmailSubscriptionLists]");
																			//x.Not.LazyLoad();
																			x.Id(y => y.Id);
																		})
                        .IgnoreBase<AvailabilityTypes>()
                        .IgnoreBase<SubscriptionCartItem>()
                        .IgnoreBase<GiftCardCartItem>()
                        .IgnoreBase<GiftCardDeliveryTypes>()
                        .IgnoreBase<RelatedItem>()


                        .Override<Product>(x =>
                        {
                            //x.Map(p => p.Categories).CustomType(typeof(List<string>)).Access.CamelCaseField();
                            x.HasMany(p => p.Categories)
                                .Table("ProductCategories")
                                .KeyColumn("ProductId")
                                .Element("Category", m => m.Type<string>())
                                .Access.CamelCaseField();
                            x.HasMany(p => p.Subjects).Table("ProductSubjects").Element("Subject");
                            x.HasMany(p => p.Reviews);
                            x.HasMany(p => p.AlternativeImages).Table("ProductImages").Element("Image");
                            x.IgnoreProperty(p => p.RelatedItems);
                        })
                        .Override<Review>(x => x.References(p => p.ReviewedBy).Cascade.All())





                    //.Override<DonationOption>(x=> x.HasManyToMany(p => p.DonationFrequencyOptions).Table("DonationOptionFrequencies"))
                    //.Override<DonationCriteria>(x =>
                    //                                {
                    //                                    x.Id(p => p.DonationOptionId).Column("DonationOptionId").GeneratedBy.Assigned();
                    //                                    x.HasOne(p => p.DonationFrequency);
                    //                                })
                    //.Override<DonationFrequency>(x => x.Id(p=>p.Value))





                    );
            };
        }
    }


}