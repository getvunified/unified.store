﻿using System.Web.Mvc;
using getv.donorstudio.core.Global;
using Jhm.Common.BootStrapper;
using Jhm.Web.Service;
using Jhm.Web.UI.MVCCommon;

namespace Jhm.Web.UI.BootStrapperTasks
{
    [BootstrapperPriority(Priority = 10)]
    public class LoadAllProductsToJSON : IBootstrapperTask
    {
        protected IServiceCollection Service;

        public void Execute()
        {
            Service = DependencyResolver.Current.GetService<IServiceCollection>();

            LoadAllProductsUnitedStates();
            LoadAllProductsCanada();
            LoadAllProductsUnitedKingdom();
        }

        private void LoadAllProductsUnitedStates()
        {
            var usProducts = Service.CatalogService.GetAllProducts(Company.JHM_UnitedStates.DonorStudioEnvironment);
            usProducts.SaveToCache(Company.JHM_UnitedStates.DonorStudioEnvironment);
        }

        private void LoadAllProductsCanada()
        {
            var canadianProducts = Service.CatalogService.GetAllProducts(Company.JHM_Canada.DonorStudioEnvironment);
            canadianProducts.SaveToCache(Company.JHM_Canada.DonorStudioEnvironment);
        }

        private void LoadAllProductsUnitedKingdom()
        {
            var ukProducts = Service.CatalogService.GetAllProducts(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
            ukProducts.SaveToCache(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
        }
    }
}