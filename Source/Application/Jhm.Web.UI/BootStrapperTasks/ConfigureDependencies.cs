using System.Web.Mvc;
using Jhm.Common.BootStrapper;
using Jhm.Web.UI.BootStrapperTasks;
using StructureMap;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    [BootstrapperPriority(Priority = 0)]
    public class ConfigureDependencies : IBootstrapperTask
    {
        public void Execute()
        {
            //Order of the following methods is important
            ObjectFactory.Configure(x => x.AddRegistry(new FluentNHibernateConfigurationRegistry()));
            ObjectFactory.Configure(x => x.AddRegistry(new DIControllerFactoryRegistry()));
            ObjectFactory.Configure(x => x.AddRegistry(new DonorStudioRegistry()));
            ObjectFactory.Configure(x => x.AddRegistry(new ServicesRegistry()));
            ObjectFactory.Configure(x => x.AddRegistry(new MembershipRegistry()));
            ObjectFactory.Configure(x => x.AddRegistry(new GiftCardRegistry()));
        }
    }
}