﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Foundation.WebApi;
using Getv.Foundation.Infrastructure;
using Jhm.Web.Repository.Modules.ECommerce;
using Jhm.Web.Service.Modules.ECommerce;
using StructureMap;
using StructureMap.Configuration.DSL;


namespace Jhm.Web.UI.BootStrapperTasks
{
    public class GiftCardRegistry : Registry
    {
        public GiftCardRegistry()
        {
            For<IHttpClient>().Use(x => CreateGiftCardHttpClient());

            For<IMessageDispatcher>().Use<HttpMessageDispatcher>();
            For<IGiftCardService>().Use<GiftCardService>();
        }

        static IHttpClient CreateGiftCardHttpClient()
        {
            var getvBaseUrl = ConfigurationManager.AppSettings["GetvWebApiBaseUrl"];
            var httpClient = new HttpClientWrapper { BaseAddress = new Uri(string.Format("{0}{1}", getvBaseUrl, "api/")) };
            return httpClient;
        }

       
    }
}