using System;
using System.Web.Mvc;
using System.Web.Routing;
using Jhm.Common.BootStrapper;
using Jhm.Web.Core;
using StructureMap;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    [BootstrapperPriority(Priority = Int32.MinValue)]
    public class RegisterRoutes : IBootstrapperTask
    {
        private readonly RouteCollection routes;

        [DefaultConstructor]
        public RegisterRoutes()
            : this(RouteTable.Routes)
        {

        }

        public RegisterRoutes(RouteCollection routes)
        {
            this.routes = routes;
        }

        public void Execute()
        {
            RegisterAllAreas();
            AddFileExtensionIgnoreRoutes();
            MapControllerActionRoutes();
        }

        private void RegisterAllAreas()
        {
            AreaRegistration.RegisterAllAreas();
        }

        private void AddFileExtensionIgnoreRoutes()
        {

            routes.IgnoreRoute("{file}.txt");



            //default
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            //Custom
            //http://haacked.com/archive/2008/07/14/make-routing-ignore-requests-for-a-file-extension.aspx
            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
        }

        private void MapControllerActionRoutes()
        {
            routes.MapRoute(MagicStringEliminator.Routes.Donation_Detail.RouteName + "SALT", // Route name
                       "salt", // URL with parameters
                       new
                       {
                           controller = MagicStringEliminator.Routes.Donation_Detail.Controller,
                           action = MagicStringEliminator.Routes.Donation_Detail.Action,
                           donationCode = "Salt_Covenant"
                       }// Parameter defaults
           );

            routes.MapRoute(MagicStringEliminator.Routes.GoPaperless.RouteName, // Route name
                       MagicStringEliminator.Routes.GoPaperless.Route, // URL with parameters
                       new
                       {
                           controller = MagicStringEliminator.Routes.GoPaperless.Controller,
                           action = MagicStringEliminator.Routes.GoPaperless.Action
                       }// Parameter defaults
           );

            var route = routes.MapRoute(
               "Admin",
               "Admin",
               new { controller = "Home", action = "Index", area = "Admin" },
               new[] { typeof(Jhm.Web.UI.Areas.Admin.Controllers.HomeController).Namespace }
            );
            route.DataTokens["area"] = "Admin";

            routes.MapRoute(MagicStringEliminator.Routes.Checkout_ShoppingCart.RouteName, // Route name
                       MagicStringEliminator.Routes.Checkout_ShoppingCart.Route, // URL with parameters
                       new
                       {
                           controller = MagicStringEliminator.Routes.Checkout_ShoppingCart.Controller,
                           action = MagicStringEliminator.Routes.Checkout_ShoppingCart.Action
                       }// Parameter defaults
           );
            
            routes.MapRoute(MagicStringEliminator.Routes.Catalog_AddProduct.RouteName, // Route name
                        MagicStringEliminator.Routes.Catalog_AddProduct.Route, // URL with parameters
                        new
                        {
                            controller = MagicStringEliminator.Routes.Catalog_AddProduct.Controller,
                            action = MagicStringEliminator.Routes.Catalog_AddProduct.Action
                        }// Parameter defaults
            );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_Product.RouteName, // Route name
                        MagicStringEliminator.Routes.Catalog_Product.Route, // URL with parameters
                        new
                        {
                            controller = MagicStringEliminator.Routes.Catalog_Product.Controller,
                            action = MagicStringEliminator.Routes.Catalog_Product.Action,
                            productTitle = UrlParameter.Optional
                        }// Parameter defaults
            );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_Search.RouteName, // Route name
                       MagicStringEliminator.Routes.Catalog_Search.Route, // URL with parameters
                       new
                       {
                           controller = MagicStringEliminator.Routes.Catalog_Search.Controller,
                           action = MagicStringEliminator.Routes.Catalog_Search.Action,
                           queryString = UrlParameter.Optional
                       }// Parameter defaults
           );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_Category.RouteName, // Route name
                      MagicStringEliminator.Routes.Catalog_Category.Route, // URL with parameters
                      new
                      {
                          controller = MagicStringEliminator.Routes.Catalog_Category.Controller,
                          action = MagicStringEliminator.Routes.Catalog_Category.Action,
                          categories = "All",
                          mediaType = UrlParameter.Optional
                      }// Parameter defaults
          );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_SpecialCaseType.RouteName, // Route name
                      MagicStringEliminator.Routes.Catalog_SpecialCaseType.Route, // URL with parameters
                      new
                      {
                          controller = MagicStringEliminator.Routes.Catalog_SpecialCaseType.Controller,
                          action = MagicStringEliminator.Routes.Catalog_SpecialCaseType.Action,
                          type = "All"
                      }// Parameter defaults
          );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_Author.RouteName, // Route name
                     MagicStringEliminator.Routes.Catalog_Author.Route, // URL with parameters
                     new
                     {
                         controller = MagicStringEliminator.Routes.Catalog_Author.Controller,
                         action = MagicStringEliminator.Routes.Catalog_Author.Action,
                         author = "All"
                     }// Parameter defaults
         );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_Subject.RouteName, // Route name
                                MagicStringEliminator.Routes.Catalog_Subject.Route, // URL with parameters
                                new
                                {
                                    controller = MagicStringEliminator.Routes.Catalog_Subject.Controller,
                                    action = MagicStringEliminator.Routes.Catalog_Subject.Action
                                    //subject = UrlParameter.Optional
                                }// Parameter defaults
                    );
            routes.MapRoute(MagicStringEliminator.Routes.Catalog_MediaType.RouteName, // Route name
                                 MagicStringEliminator.Routes.Catalog_MediaType.Route, // URL with parameters
                                 new
                                 {
                                     controller = MagicStringEliminator.Routes.Catalog_MediaType.Controller,
                                     action = MagicStringEliminator.Routes.Catalog_MediaType.Action
                                     //subject = UrlParameter.Optional
                                 }// Parameter defaults
                     );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog_OnSale.RouteName, // Route name
                                 MagicStringEliminator.Routes.Catalog_OnSale.Route, // URL with parameters
                                 new
                                 {
                                     controller = MagicStringEliminator.Routes.Catalog_OnSale.Controller,
                                     action = MagicStringEliminator.Routes.Catalog_OnSale.Action
                                     //subject = UrlParameter.Optional
                                 }// Parameter defaults
                     );

            routes.MapRoute(MagicStringEliminator.Routes.Catalog.RouteName, // Route name
                    MagicStringEliminator.Routes.Catalog.Route, // URL with parameters
                    new
                    {
                        controller = MagicStringEliminator.Routes.Catalog.Controller,
                        action = MagicStringEliminator.Routes.Catalog.Action,
                    }// Parameter defaults
        );

            routes.MapRoute(MagicStringEliminator.Routes.Donation_Opportunities.RouteName, // Route name
                     MagicStringEliminator.Routes.Donation_Opportunities.Route, // URL with parameters
                     new
                     {
                         controller = MagicStringEliminator.Routes.Donation_Opportunities.Controller,
                         action = MagicStringEliminator.Routes.Donation_Opportunities.Action,
                     }// Parameter defaults
         );

            routes.MapRoute(MagicStringEliminator.Routes.Donation_Detail.RouteName, // Route name
                     MagicStringEliminator.Routes.Donation_Detail.Route, // URL with parameters
                     new
                     {
                         controller = MagicStringEliminator.Routes.Donation_Detail.Controller,
                         action = MagicStringEliminator.Routes.Donation_Detail.Action,
                     }// Parameter defaults
         );

            routes.MapRoute(MagicStringEliminator.Routes.Donation_Form.RouteName, // Route name
                     MagicStringEliminator.Routes.Donation_Form.Route, // URL with parameters
                     new
                     {
                         controller = MagicStringEliminator.Routes.Donation_Form.Controller,
                         action = MagicStringEliminator.Routes.Donation_Form.Action,
                     }// Parameter defaults
         );

            routes.MapRoute(MagicStringEliminator.Routes.Account_Logon.RouteName, // Route name
                     MagicStringEliminator.Routes.Account_Logon.Route, // URL with parameters
                     new
                     {
                         controller = MagicStringEliminator.Routes.Account_Logon.Controller,
                         action = MagicStringEliminator.Routes.Account_Logon.Action,
                         ReturnUrl = UrlParameter.Optional
                     }// Parameter defaults
         );

            routes.MapRoute(MagicStringEliminator.Routes.DigitalDownload.RouteName, // Route name
            MagicStringEliminator.Routes.DigitalDownload.Route, // URL with parameters
            new
            {
                controller = MagicStringEliminator.Routes.DigitalDownload.Controller,
                action = MagicStringEliminator.Routes.DigitalDownload.Action,
                mediaId = UrlParameter.Optional
            }// Parameter defaults
            );

            routes.MapRoute(MagicStringEliminator.Routes.FileDownloadManager.RouteName, // Route name
                       MagicStringEliminator.Routes.FileDownloadManager.Route, // URL with parameters
                       new
                       {
                           controller = MagicStringEliminator.Routes.FileDownloadManager.Controller,
                           action = MagicStringEliminator.Routes.FileDownloadManager.Action,
                           productCode = UrlParameter.Optional
                       }// Parameter defaults
           );
            
            routes.MapRoute(MagicStringEliminator.Routes.ReloadDsProducts.RouteName, // Route name
             MagicStringEliminator.Routes.ReloadDsProducts.Route, // URL with parameters
             new
             {
                 controller = MagicStringEliminator.Routes.ReloadDsProducts.Controller,
                 action = MagicStringEliminator.Routes.ReloadDsProducts.Action
             }// Parameter defaults
            );

            routes.MapRoute(
                "ClientErrors", // Route name
                "Error/{id}", // URL with parameters
                new { controller = "Error", action = "Index", id = UrlParameter.Optional } // Parameter defaults
                );


            routes.MapRoute(
                "OldSiteBanner",
                "Media/MediaManager/{image}",
                new { controller = "Error", action = "Index", id = "301", image = UrlParameter.Optional });

            routes.MapRoute(
                "OldSitePodcast",
                "Media/PodcastItems/{item}",
                new { controller = "Error", action = "Index", id = "podcast", item = UrlParameter.Optional });

            routes.MapRoute(
                "OldSitePodcastFeed",
                "ME2/Console/Podcast/{item}",
                new { controller = "Error", action = "Index", id = "podcastfeed", item = UrlParameter.Optional });

            routes.MapRoute(
               "OldSiteRSSFeed",
               "ME2/Console/XmlSyndication/Display/{remainder}",
               new { controller = "Error", action = "Index", id = "rss", remainder = UrlParameter.Optional });

            routes.MapRoute(
                "OldSite",
                "ME2/{remainder}",
                new { controller = "Error", action = "Index", id = "301" });

            routes.MapRoute(
                "OldSiteMedia",
                "Media/{remainder}",
                new { controller = "Error", action = "Index", id = "301" });

            routes.MapRoute(
                           "OldSiteMediaSites",
                           "ME2/Sites/{remainder}",
                           new { controller = "Error", action = "Index", id = "301" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },// Parameter defaults
                new string[] { typeof(Jhm.Web.UI.Controllers.HomeController).Namespace }// Specified name for non Admin duplicated controller names
                );

            routes.MapRoute("Error",
                "{*url}",
                new { controller = "Error", action = "Index", id = "404" });
        }
    }


#if DEBUG
    [BootstrapperPriority(Priority = Int32.MaxValue)]
    public class SetupTestRouteSettings : IBootstrapperTask
    {
        public void Execute()
        {
            //RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

        }
    }
#endif

}