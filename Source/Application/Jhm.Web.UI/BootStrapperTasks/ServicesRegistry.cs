﻿using System.Web.Profile;
using System.Web.Security;
using Getv.Security;
using Jhm.Common;
using Jhm.Common.Repositories;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Repository.Modules.ContentManagement;
using Jhm.Web.Repository.Modules.ECommerce;
using Jhm.Web.Repository.Modules.ECommerce.Mocks;
using Jhm.Web.Service;
using Jhm.Web.Service.Modules.ECommerce;
using Jhm.Web.UI.JhmMembership;
using StructureMap.Configuration.DSL;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            //Order of the following methods is important
            //RegisterMembershipProvideRepositories();

            //For<ICartRepository>().Use(new CartRepository(null));
            For<IDonationRepository>().Use(new MockDonationRepository());

            For(typeof(IRepository<>)).Use(typeof(FluentNHibernateRepository<>));

            //This must happen after all object used in the ServiceCollection are defined.
            For<IServiceCollection>().Singleton().Use<ServiceCollection>();


            //For<DsInventoryService>().Use<DsInventoryService>();
            //For<DsAccountTransactionService>().Use<DsAccountTransactionService>();

            For<ICatalogRepository>().Use<CatalogRepository>();
            For<ICartRepository>().Use<CartRepository>();
            For<IDigitalDownloadRepository>().Use<DigitalDownloadRepository>();
            For<IHomeBannerRepository>().Use<HomeBannerRepository>();
            For<IPromotionalItemRepository>().Use<PromotionalItemRepository>();
            For<IDiscountItemRepository>().Use<DiscountItemRepository>();
            For<ISubscriptionsRepository>().Use<SubscriptionsRepository>();
            For<IJhmMagazineListingRepository>().Use<JhmMagazineListingRepository>();

            For<IDiscountCodeRepository>().Use<DiscountCodeRepository>();
            For<IGiftCardRepository>().Use<MockGiftCardRepository>();

            For<IEmailer>().Use<Emailer>();
            For<ILogFactory>().Use<Log4NetLogFactory>();
            For<ILog>().Use<Log4NetLog>();

            For<ISecurityService>().Singleton().Use<SecurityService>();
            For<IGiftCardService>().Singleton().Use<GiftCardService>();

            For<ICatalogService>().Singleton().Use<CatalogService>();
            //For<ICatalogService>().Singleton().Use<CatalogService>();

            For<IDigitalDownloadRepository>().Use<DigitalDownloadRepository>();
            For<IDigitalDownloadService>().Use<DigitalDownloadService>();

			For<IEmailSubscriptionListRepository>().Use<EmailSubscriptionListRepository>();

            RegisterMembershipProvideRepositories();


            //For<MembershipProvider>().Use<CustomMembershipProvider>();
            //For<RoleProvider>().Use<JHMRoleProvider>();
            //For<ProfileProvider>().Use<JHMProfileProvider>();

            For<MembershipProvider>().Use<CustomMembershipProvider>();
            For<RoleProvider>().Use<JHMRoleProvider>();
            For<ProfileProvider>().Use<JHMProfileProvider>();
        }


        private void RegisterMembershipProvideRepositories()
        {
            For<IUserRepository>().Use<UserRepository>();
            For<IRoleRepository>().Use<RoleRepository>();
            For<IProfileRepository>().Use<ProfileRepository>();
        }

      
      
    }
}