﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Foundation.WebApi;
using StructureMap;
using StructureMap.Configuration.DSL;
using getv.donorstudio;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class DonorStudioRegistry : Registry
    {
        public const string DONOR_STUDIO = "DonorStudio";

        public DonorStudioRegistry()
        {
            For<IRelativeUriResolver>().Use<RelativeUriResolver>();
            For<IWebApiClient>().Use(x => BuildWebApiClient(x)).Named(DONOR_STUDIO);

            For<DsAccountTransactionService>()
                .Use<DsAccountTransactionService>()
                .Ctor<IWebApiClient>()
                .IsNamedInstance(DONOR_STUDIO);

            For<DsInventoryService>()
                .Use<DsInventoryService>()
                .Ctor<IWebApiClient>()
                .IsNamedInstance(DONOR_STUDIO);

            For<DsSystemAdministrationService>()
                .Use<DsSystemAdministrationService>()
                .Ctor<IWebApiClient>()
                .IsNamedInstance(DONOR_STUDIO);

            For<DsReportService>()
                .Use<DsReportService>()
                .Ctor<IWebApiClient>()
                .IsNamedInstance(DONOR_STUDIO);


        }



        static IWebApiClient BuildWebApiClient(IContext x)
        {
            var httpClient = CreateDonorStudioHttpClient();
            return x.GetInstance<IContainer>().With(httpClient).GetInstance<WebApiClient>();
        }


        static HttpClient CreateDonorStudioHttpClient()
        {
            var getvBaseUrl = ConfigurationManager.AppSettings["GetvWebApiBaseUrl"];
            var httpClient = new HttpClient { BaseAddress = new Uri(string.Format("{0}{1}", getvBaseUrl, "api/DonorStudio/")) };
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return httpClient;
        }


    }
}
