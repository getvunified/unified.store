﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Jhm.Common.UOW;
using Jhm.FluentNHibernateProvider;
using StructureMap.Configuration.DSL;

public class DIControllerFactoryRegistry : Registry
{

    public DIControllerFactoryRegistry()
    {
        For<IUnitOfWorkFactory>().Use<FluentNHibernateUnitOfWorkFactory>();
        For<IUnitOfWorkContainer>().Use<UnitOfWork>();
        
        ReplaceDefaultMVCBuilders();
    }
   

    private void ReplaceDefaultMVCBuilders()
    {
        ControllerBuilder.Current.SetControllerFactory(new DIControllerFactory());

        //Commented for future use - needs to be tested with StructureMap and object hydration.
        // ModelBinders.Binders.DefaultBinder = new DIModelBinder();

        
    }

    private class DIControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
                return base.GetControllerInstance(requestContext, controllerType);

            return DependencyResolver.Current.GetService(controllerType) as Controller;
        }
    }

    private class DIModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext,
                                              ModelBindingContext bindingContext, Type modelType)
        {
            return DependencyResolver.Current.GetService(modelType);
        }
    }
}