﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.UI.Models
{
    public class FeaturedPhotoModel
    {
        public string ApiVersion { get; set; }
        public FeaturedPhotoData Data { get; set; }
        public Error Error { get; set; }
    }

    public class FeaturedPhotoData
    {
        private IList<FeaturedPhoto> _items;

        public IList<FeaturedPhoto> Items
        {
            get { return (_items = _items ?? new List<FeaturedPhoto>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class FeaturedPhoto
    {
        public string Id { get; set; }
        public string PhotoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}