﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Newtonsoft.Json;

namespace Jhm.Web
{
    public class StoryModel : CartViewModel
    {
        public string ApiVersion { get; set; }
        public StoryData Data { get; set; }
        public Error Error { get; set; }
    }
    
    public class StoryData
    {
        private IList<Story> _items;
        public IList<Story> Items
        {
            get { return (_items = _items ?? new List<Story>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class StoryViewModel : CartViewModel
    {
        private List<Story> _storyList;
        public List<Story> StoryList
        {
            get { return (_storyList = _storyList ?? new List<Story>()); }
            set { _storyList = value; }
        }
    }

    public class Story : CartViewModel
    {
        private IList<Picture> _pictures;

        public string Body { get; set; }
        public string LinkUrl { get; set; }
        public string Id { get; set; }
        public string ExpirationDate { get; set; }
        public bool IsActive { get; set; }
        public string Title { get; set; }
        public string PictureUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string SubTitle { get; set; }
        public bool IsExternal { get; set; }

        public DateTime PublishDate { get; set; }
    }


}