﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Security;
using Jhm.Common.Utilities;
using Jhm.DonorStudio.Services;
using Jhm.Web.Core.Models.Modules;
using Jhm.em3.Core;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.UI.Models
{

    #region Models
    [PropertiesMustMatch("NewPassword", "ConfirmPassword", ErrorMessage = "The new password and confirmation password do not match.")]
    public class ChangePasswordModel : CartViewModel
    {
        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [DisplayName("New password")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm new password")]
        public string ConfirmPassword { get; set; }

        public string ResetToken { get; set; }

        public string CurrentPassword { get; set; }
    }

    public class LogOnModel : CartViewModel, ILogOnModel
    {
        [Required]
        [DisplayName("User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DisplayName("Keep me logged in")]
        public bool RememberMe { get; set; }

        public string GlobalUserId { get; set; }
    }

    [PropertiesMustMatch("Password", "ConfirmPassword", ErrorMessage = "The password and confirmation password do not match.")]
    public class RegisterModel : CartViewModel
    {
        [Required]
        [DisplayName("User name")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Email address")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [ValidatePasswordLength]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string ConfirmPassword { get; set; }

        //[Required(ErrorMessage = "Must choose a Password Question.")]
        //[DisplayName("Password Question")]
        //public string PasswordQuestion { get; set; }

        //[Required]
        //[DisplayName("Password Answer")]
        //public string PasswordAnswer { get; set; }

        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        public string Company { get; set; }

        [DisplayName("Street Address")]
        public string Address1 { get; set; }

        [DisplayName("")]
        public string Address2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        [DisplayName("State/Province")]
        public string State { get; set; }

        [DisplayName("Postal Code")]
        public string Zip { get; set; }

        [Required]
        [DisplayName("Phone Country Code")]
        public string PhoneCountryCode { get; set; }

        [Required]
        [DisplayName("Phone Area Code")]
        public string PhoneAreaCode { get; set; }

        [Required]
        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Sign up for the news letter")]
        [DefaultValue(true)]
        public bool NewsLetterOptIn { get; set;}

        public void MapToUser(User user)
        {
            user.Profile.FirstName = FirstName;
            user.Profile.LastName = LastName;
            user.Profile.Company = Company;
            user.Profile.Gender = (Gender == Gender.Male) ? "M" : "F";
            user.Profile.PrimaryAddress = new Address(Address1, Address2 ?? string.Empty, City, State, Zip, Country);
            user.Profile.PrimaryPhone = new Phone(PhoneCountryCode, PhoneAreaCode, PhoneNumber);
            user.Profile.Title = Title;
            user.Profile.PrimaryAddress.DsIsPrimary = true;
            user.NewsLetterOptIn = NewsLetterOptIn;
        }
        
        public RadioButtonListViewModel<Gender> GenderRadioButtonList { get; set; }
        [Required]
        public Gender Gender { get; set; }

        [Required]
        public string Title{ get; set; }

        public RegisterModel()
        {
            GenderRadioButtonList = new RadioButtonListViewModel<Gender>
            {
                Id = "Gender",
                ListItems = new System.Collections.Generic.List<RadioButtonListItem<Gender>>
                {
                    new RadioButtonListItem<Gender>{Text = "Male", Value = Gender.Male, Selected = false},
                    new RadioButtonListItem<Gender>{Text = "Female", Value = Gender.Female, Selected = false}
                }
            };
        }
    }

    public class ForgotPasswordModel
    {
        [Required]
        public string Email { get; set; }

        public string Message { get; set; }
    }

    public class PasswordRecoveryModel
    {
        [Required]
        [DisplayName("User name")]
        public string UserName { get; set; }

        public string Email { get; set; }

        [DisplayName("Password Question")]
        public string PasswordQuestion { get; set; }

        [Required]
        [DisplayName("Password Answer")]
        public string PasswordAnswer { get; set; }
    }

    public class ForgotUsernameModel : CartViewModel
    {
        [Required]
        public string Email { get; set; }
    }

    [PropertiesMustMatch("NewEmail", "ConfirmEmail", ErrorMessage = "The new email and confirmation email do not match.")]
    public class ChangeEmailModel
    {
        [Display(Name = "Current email")]
        public string CurrentEmail { get; set; }

        [Required]
        [Display(Name = "New email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        public string NewEmail { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Confirm new email")]
        public string ConfirmEmail { get; set; }

        public string Message { get; set; }
    }

    public class FortyDaysOfPrayerModel
    {
        [Required]
        [DisplayName("E-mail")]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }

    }

    public class JhmMagSubscriptionViewModel: CartViewModel
    {
        private IEnumerable<ISubscription> _availableSubscriptions;
        private GiftSubscriptionViewModel _giftSubscriber;

        public IEnumerable<ISubscription> AvailableSubscriptions
        {
            get { return (_availableSubscriptions = _availableSubscriptions ?? new List<ISubscription>()); }
            set { _availableSubscriptions = value; }
        }

        public string SubsciptionCode { get; set; }
        public string SelectedPriceCode { get; set; }
        public bool IsGiftSubscription { get; set; }
        public UserSubscriptionViewModel ActiveSubscription { get; set; }

        public GiftSubscriptionViewModel GiftSubscriber
        {
            get { return (_giftSubscriber = _giftSubscriber ?? new GiftSubscriptionViewModel()); }
            set { _giftSubscriber = value; }
        }

        public UserSubscriptionViewModel MapThis()
        {
            var to = new UserSubscriptionViewModel();
            to.IsGiftSubcription = IsGiftSubscription;
            to.PriceCode = SelectedPriceCode;
            to.StartDate = DateTime.Now;
            to.SubscriptionCode = SubsciptionCode;

            if ( IsGiftSubscription )
            {
                to.User = new User();
                to.User.Profile.FirstName = GiftSubscriber.FirstName;
                to.User.Profile.LastName = GiftSubscriber.LastName;
                to.User.Email = GiftSubscriber.Email;
                to.User.Profile.PrimaryAddress = new Address
                {
                    Address1 = GiftSubscriber.DeliveryAddress.Address1,
                    Address2 = GiftSubscriber.DeliveryAddress.Address2,
                    City = GiftSubscriber.DeliveryAddress.City,
                    StateCode = GiftSubscriber.DeliveryAddress.State,
                    Country = GiftSubscriber.DeliveryAddress.Country,
                    PostalCode = GiftSubscriber.DeliveryAddress.Zip
                };
                to.User.Profile.PrimaryPhone = new Phone
                {
                    AreaCode = GiftSubscriber.PrimaryPhone.AreaCode,
                    CountryCode = GiftSubscriber.PrimaryPhone.CountryCode,
                    Number = GiftSubscriber.PrimaryPhone.Number
                };
            }
                
            return to;
        }
    }

    public class GiftSubscriptionViewModel
    {
        private AddressViewModel _deliveryAddress;

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Email Address")]
        public string Email { get; set; }

        [DisplayName("Primary Phone Number")]
        public Phone PrimaryPhone { get; set; }

        public AddressViewModel DeliveryAddress
        {
            get { return (_deliveryAddress = _deliveryAddress ?? new AddressViewModel()); }
            set { _deliveryAddress = value; }
        }
    }

    public class UserSubscriptionsViewModel : CartViewModel
    {
        public User User { get; set; }
        public Client Client { get; set; }
        private List<UserSubscriptionViewModel> _subscriptions;
        private UserSubscriptionViewModel _selectedSubscription;

        public List<UserSubscriptionViewModel> Subscriptions
        {
            get { return (_subscriptions = _subscriptions ?? new List<UserSubscriptionViewModel>()); }
            set { _subscriptions = value; }
        }

        public List<Address> Addresses { get; set; }
        public UserSubscriptionViewModel SelectedSubscription
        {
            get { return (_selectedSubscription = _selectedSubscription ?? new UserSubscriptionViewModel()); }
            set { _selectedSubscription = value; }
        }

        public long? SelectedDeliveryAddressId { get; set; }

        public AddressViewModel DeliveryAddress { get; set; }
    }
    #endregion

   

    #region Validation
    public static class AccountValidation
    {
        public static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Username already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A username for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class PropertiesMustMatchAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' and '{1}' do not match.";
        private readonly object _typeId = new object();

        public PropertiesMustMatchAttribute(string originalProperty, string confirmProperty)
            : base(_defaultErrorMessage)
        {
            OriginalProperty = originalProperty;
            ConfirmProperty = confirmProperty;
        }

        public string ConfirmProperty { get; private set; }
        public string OriginalProperty { get; private set; }

        public override object TypeId
        {
            get
            {
                return _typeId;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                OriginalProperty, ConfirmProperty);
        }

        public override bool IsValid(object value)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            object originalValue = properties.Find(OriginalProperty, true /* ignoreCase */).GetValue(value);
            object confirmValue = properties.Find(ConfirmProperty, true /* ignoreCase */).GetValue(value);
            return Object.Equals(originalValue, confirmValue);
        }
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class ValidatePasswordLengthAttribute : ValidationAttribute
    {
        private const string _defaultErrorMessage = "'{0}' must be at least {1} characters long.";
        private readonly int _minCharacters = Membership.Provider.MinRequiredPasswordLength;

        public ValidatePasswordLengthAttribute()
            : base(_defaultErrorMessage)
        {
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,
                name, _minCharacters);
        }

        public override bool IsValid(object value)
        {
            string valueAsString = value as string;
            return (valueAsString != null && valueAsString.Length >= _minCharacters);
        }
    }
    #endregion

    public class UserTransactionsViewModel : CartViewModel
    {
        public User User { get; set; }
        public Client Client { get; set; }
        public List<TransactionViewModel> Transactions { get; set; }
        public TransactionViewModel DocumentNumber { get; set; }
        public TransactionViewModel AccountNumber { get; set; }

        
        public static string GetDonationTitleByProjectCode(string projectCode, List<Jhm.Web.Core.Models.Donation> availableDonations)
        {
            if ( availableDonations == null)
            {
                return null;
            }
            var donation = availableDonations.Find(d => d.ProjectCode == projectCode);
            return donation != null ? donation.Title : String.Empty;
        }

        public static string GetFullOrderStatusByCode(string orderStatus)
        {
           switch (orderStatus)
            {
                case "A":
                    return "Processed";
                case "B":
                    return "Backordered";
                case "C":
                    return "Cancelled";
            }
            return orderStatus;
        }


        //public static string GetFullOrderStatusByCode(string statusCode)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
