﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using AlexJamesBrown.JoeBlogs;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public class BlogViewModel : CartViewModel
    {

        public List<Post> Posts { get; set; }
        //public List<BlogCategory> categories { get; set; }
        public IEnumerable<AlexJamesBrown.JoeBlogs.Structs.Category> Category { get; set; }
        private int _loadMorePosts;

        public int LoadMorePosts
        {
            get { return _loadMorePosts; }
            set { _loadMorePosts = value; }
        }

        public static Post ConvertPost(AlexJamesBrown.JoeBlogs.Structs.Post convertPost)

        {
            Post post = new Post();
            post.blogID = CalculateMD5Hash(String.Format("{0}{1}", convertPost.title, convertPost.dateCreated));
            post.DateCreated = convertPost.dateCreated;
            post.Title = convertPost.title;
            post.Description = convertPost.description;
            post.PostCategories = convertPost.categories.ToList();
            return post;
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }

    public class Post
        {
            public string blogID { get; set; }
            public string Title { get; set; }
            public DateTime DateCreated { get; set; }
            public string Description { get; set; }
            public List<string> PostCategories { get; set; }
        }

    
}
