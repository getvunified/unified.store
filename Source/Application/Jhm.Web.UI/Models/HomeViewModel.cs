﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.UI.Models
{
    public class HomeViewModel : CartViewModel
    {
        public IEnumerable<HomeBannerItem> HomeBannerItems { get; set; }
        public bool ShouldShowLiveMessage { get; set; }
        public HomePagePopupAnnouncement ActivePopup { get; set; }
    }
}