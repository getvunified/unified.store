﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Newtonsoft.Json;

namespace Jhm.Web.UI.Models
{
    public class BannerModel : CartViewModel
    {
        public string ApiVersion { get; set; }
        public BannerData Data { get; set; }
        public Error Error { get; set; }
    }

    public class BannerData
    {
        private IList<BannerItem> _items;

        public IList<BannerItem> Items
        {
            get { return (_items = _items ?? new List<BannerItem>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    

    public class BannerItem : CartViewModel
    {
        public string Body { get; set; }
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public string LinkUrl { get; set; }
        public string PictureUrl { get; set; }
        public string Title { get; set; }
    }
}