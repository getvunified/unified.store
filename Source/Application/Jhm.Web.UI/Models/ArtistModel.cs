﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Newtonsoft.Json;

namespace Jhm.Web
{
    public class ArtistModel : CartViewModel
    {
        [JsonProperty("apiVersion")]
        public string ApiVersion { get; set; }

        [JsonProperty("data")]
        public ArtistData Data { get; set; }

        [JsonProperty("error")]
        public Error Error { get; set; }
    }

    public class Error
    {
        private List<Error2> _errors;

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("errors")]
        public List<Error2> Errors
        {
            get { return (_errors = _errors ?? new List<Error2>()); }
            set { _errors = value; }
        }
    }

    public class Error2
    {

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }
    }

    public class ArtistData : CartViewModel
    {
        private List<Artist> _items;

        public List<Artist> Items
        {
            get { return (_items = _items ?? new List<Artist>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class Picture
    {
        public string PhotoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }

    public class Media
    {
        public string MediaUrl { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string MediaId { get; set; }
    }

    public class ArtistViewModel : CartViewModel
    {
        private List<Artist> _artists;
        public List<Artist> Artists
        {
            get { return (_artists = _artists ?? new List<Artist>()); }
            set { _artists = value; }
        }
    }

    public class Artist : CartViewModel
    {
        private List<Media> _music;
        private List<Media> _videos;
        private List<Picture> _slideShow;
        public string Name { get; set; }
        public string FacebookUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string YouTubeUrl { get; set; }
        public string GooglePlusUrl { get; set; }
        public string EmailAddress { get; set; }
        public string Biography { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string ProfileThumbnailUrl { get; set; }
        public string YouTubePlaylistId { get; set; }
        public List<Media> Music
        {
            get { return (_music = _music ?? new List<Media>()); }
            set { _music = value; }
        }

        public List<Media> Videos
        {
            get { return (_videos = _videos ?? new List<Media>()); }
            set { _videos = value; }
        }

        public string Id { get; set; }
        public bool IsActive { get; set; }
        public List<Picture> Pictures
        {
            get { return (_slideShow = _slideShow ?? new List<Picture>()); }
            set { _slideShow = value; }
        }

        public string WebsiteUrl { get; set; }
    }
}