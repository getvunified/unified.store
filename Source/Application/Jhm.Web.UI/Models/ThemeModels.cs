﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Jhm.Web.UI.Models
{
    public class ThemeCollection
    {
        private DateTime Refresh;
        private Theme ActiveTheme;
        public List<Theme> Themes;

        public string GetActiveTheme(DateTime? date = null, bool forceRefresh = false)
        {
            bool refreshed = false;

            if (date == null)
                date = DateTime.Now;

            if(Themes == null || date > Refresh || forceRefresh)
            {
                try
                {
                    using (FileStream stream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "\\App_Data\\ThemesData.xml", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(ThemeCollection));
                        ThemeCollection newCollection = (ThemeCollection)ser.Deserialize(stream);
                        Themes = newCollection.Themes;
                    }
                    Refresh = DateTime.Now.Date.AddDays(1);
                    refreshed = true;

                    if (ActiveTheme == null || refreshed)
                        ActiveTheme = Themes.Where(x => x.IsValid(date.Value)).OrderByDescending(x=>x.Priority).FirstOrDefault();
                }
                catch (Exception ex)
                {
                }
            }

            return ActiveTheme != null && !string.IsNullOrWhiteSpace(ActiveTheme.Name) ? ActiveTheme.Name : "Classic";
        }
    }

    public class Theme
    {
        public string Name;
        public DateTime StartDate;
        public DateTime EndDate;
        public DateTime? Expiration;
        public int Priority;

        public bool IsValid(DateTime date)
        {
            DateTime start = new DateTime(date.Year, StartDate.Month, StartDate.Day);
            if (StartDate.Month > date.Month && StartDate.Month > EndDate.Month)
                start = start.AddYears(-1);
            DateTime end = new DateTime(date.Year, EndDate.Month, EndDate.Day);
            if (end < start)
                end = end.AddYears(1);

            return (start <= date && date < end);
        }
    }
}