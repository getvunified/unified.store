﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models.Shop
{
    public class GroupViewModel
    {
        public string Code { get; set; }

        public string DisplayName { get; set; }

        public string GroupName { get; set; }
    }
}