﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models.Shop
{
    public class HeaderModel
    {        
        public LogoModel MainLogo { get; set; }
        public IList<MenuItem> TopLevelMenuItems { get; set; }
        public IList<LogoModel> Logos { get; set; }

        public HeaderModel()
        {
            TopLevelMenuItems = new List<MenuItem>();
            Logos = new List<LogoModel>();            
        }
    }

    public class MenuItem
    {
        public string Href { get; set; }
        public string Title { get; set; }
        public IEnumerable<MenuItem> Children { get; set; }

        public MenuItem()
        {
            Children = new List<MenuItem>();
        }
    }

    public class LogoModel
    {
        public string ImageUrl { get; set; }
        public string DestinationUrl { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
    }
}