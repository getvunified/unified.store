﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.UI.Models.Shop
{
    public class GroupProductListViewModel
    {
        public String                               Name            { get; set; }
        public IEnumerable<GroupProductViewModel>   Products        { get; set; }
        public int                                  TotalResults    { get; set; }

        public GroupProductListViewModel()
        {
            Name         = "";
            Products     = new List<GroupProductViewModel>();
            TotalResults = 0;
        }
    }
}