﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models.Shop
{
    public class SliderViewModel
    {
        public string BackgroundImage { get; set; }
        
        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string Description { get; set; }

        public string Destinationurl { get; set; }  

        public string ButtonText { get; set; }
    }
}