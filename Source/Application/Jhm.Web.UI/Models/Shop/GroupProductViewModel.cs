﻿using System;

namespace Jhm.Web.UI.Models.Shop
{
    public class GroupProductViewModel
    {
        public String Title     { get; set; }
        public String ImagePath { get; set; }
        public String Author    { get; set; }
        public String Sku       { get; set; }

        public GroupProductViewModel()
        {
            Title     = "";
            ImagePath = "";
            Author    = "";
            Sku       = "";
        }
    }
}