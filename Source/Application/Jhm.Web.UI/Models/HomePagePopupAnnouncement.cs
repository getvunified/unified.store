﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models
{
    public class HomePagePopupAnnouncement
    {
        public string Name { get; set; }
        public string PopupTitle { get; set; }
        public string PopupImagePath { get; set; }
        public string PopupText { get; set; }
        public string TargetUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Repeats { get; set; }
        public int RepeatFrequencyId { get; set; }
        public int FrequencyAmount { get; set; }
 

    }
}