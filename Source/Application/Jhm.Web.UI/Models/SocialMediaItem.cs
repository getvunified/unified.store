﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.UI.Models
{
    public class SocialMediaViewModel
    {
        private List<SocialMediaItem> _items;
        public List<SocialMediaItem> Items
        {
            get { return (_items = _items ?? new List<SocialMediaItem>()); }
            set { _items = value; }
        }
    }

    public class ApiDataWrapper<T>
    {
        public string ApiVersion { get; set; }
        public ApiData<T> Data { get; set; }
        public Error Error { get; set; }
    }

    public class ApiData<T>
    {
        private IList<T> _items;

        public IList<T> Items
        {
            get { return (_items = _items ?? new List<T>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }


    public class SocialMediaItemModel
    {
        public string ApiVersion { get; set; }
        public SocialMediaItemData Data { get; set; }
        public Error Error { get; set; }
    }

    public class SocialMediaItemData
    {
        private IList<SocialMediaItem> _items;

        public IList<SocialMediaItem> Items
        {
            get { return (_items = _items ?? new List<SocialMediaItem>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class SocialMediaItem
    {
        public string Id { get; set; }
        // facebook | twitter
        public string Type { get { return "twitter"; } }
        public string Text { get; set; }
        public DateTime DateCreated { get; set; }
    }
}