﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.UI.Models
{
    public class DMHomePageViewModel : CartViewModel
    {
        private List<BannerItem> _bannerItemList;
        public List<BannerItem> BannerItems
        {
            get { return (_bannerItemList = _bannerItemList ?? new List<BannerItem>()); }
            set { _bannerItemList = value; }
        }

        private List<FeaturedPhoto> _featuredPhotos;
        public List<FeaturedPhoto> FeaturedPhotos
        {
            get { return (_featuredPhotos = _featuredPhotos ?? new List<FeaturedPhoto>()); }
            set { _featuredPhotos = value; }
        }

        private List<FeaturedVideo> _featuredVideos;
        public List<FeaturedVideo> FeaturedVideos
        {
            get { return (_featuredVideos = _featuredVideos ?? new List<FeaturedVideo>()); }
            set { _featuredVideos = value; }
        }

        private List<SocialMediaItem> _socialMedia;
        public List<SocialMediaItem> SocialMedia
        {
            get { return (_socialMedia = _socialMedia ?? new List<SocialMediaItem>()); }
            set { _socialMedia = value; }
        }
    }
}