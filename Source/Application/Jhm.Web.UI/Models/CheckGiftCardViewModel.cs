﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models
{
    public class CheckGiftCardViewModel
    {
        public string Pin { get; set; }
        public string Number { get; set; }

        public decimal Balance { get; set; }
    }
}