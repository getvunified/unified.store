﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.UI.Models
{
    public class FeaturedVideoViewModel
    {
        private List<FeaturedVideo> _items;
        public List<FeaturedVideo> Items
        {
            get { return (_items = _items ?? new List<FeaturedVideo>()); }
            set { _items = value; }
        }
    }

    public class FeaturedVideoModel
    {
        public string ApiVersion { get; set; }
        public FeaturedVideoData Data { get; set; }
        public Error Error { get; set; }
    }

    public class FeaturedVideoData
    {
        private IList<FeaturedVideo> _items;

        public IList<FeaturedVideo> Items
        {
            get { return (_items = _items ?? new List<FeaturedVideo>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class FeaturedVideo
    {
        public string Id { get; set; }
        public string ImageUrl { get; set; }
        public string MediaUrl { get; set; }
        public string MediaId { get; set; }
        private Video _video;

        public Video Video
        {
            get { return (_video = _video ?? new Video()); }
            set { _video = value; }
        }
    }

    public class Video
    {
        public DateTime Uploaded { get; set; }
    }
}