﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Jhm.Web
{
    public class JobPostingJsonModel
    {
        [JsonProperty("apiVersion")]
        public string ApiVersion { get; set; }

        [JsonProperty("data")]
        public JobPostingData Data { get; set; }

        [JsonProperty("error")]
        public Error Error { get; set; }
    }

    public class JobPostingData
    {
        private List<JobPosting> _items;

        public List<JobPosting> Items
        {
            get { return (_items = _items ?? new List<JobPosting>()); }
            set { _items = value; }
        }

        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class JobPosting
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}