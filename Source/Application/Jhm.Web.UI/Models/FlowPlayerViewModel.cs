﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jhm.Web.UI.Models
{
    public class FlowPlayerViewModel
    {

        public enum PlayerModes
        {
            Mutibitrate_SD_Streaming,
            Mutibitrate_HD_Streaming
        }

        public PlayerModes PlayerMode { get; set; }

        public string ThumbnailImagePath { get; set; }

        public string OfflineThumbnailImagePath { get; set; }

        public string ErrorImagePath { get; set; }

        public bool StreamingIsActive { get; set; }
    }
}