﻿#region
using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Newtonsoft.Json;

#endregion

namespace Jhm.Web
{
    public class PressKitModel : CartViewModel
    {
        public string ApiVersion { get; set; }
        public PressKitData Data { get; set; }
        public Error Error { get; set; }
    }

    public class PressKitData
    {
        private IList<PressKitItem> _items;

        [JsonProperty("items")]
        public IList<PressKitItem> Items
        {
            get { return (_items = _items ?? new List<PressKitItem>()); }
            set { _items = value; }
        }
        public int TotalItems { get; set; }
        public int StartIndex { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class PressKitViewModel : CartViewModel
    {
        private List<PressKitItem> _pressKitList;

        public List<PressKitItem> PressKitList
        {
            get { return (_pressKitList = _pressKitList ?? new List<PressKitItem>()); }
            set { _pressKitList = value; }
        }
    }

    public class PressKitItem : CartViewModel
    {
        public string Id { get; set; }
        public string Body { get; set; }
        public string DownloadUrl { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public string PictureUrl { get; set; }
        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}