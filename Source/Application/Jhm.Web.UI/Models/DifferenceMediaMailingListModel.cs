﻿#region
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jhm.Web.Core.Models.Modules.ECommerce;

#endregion

namespace Jhm.Web
{
    public class DifferenceMediaMailingListModel : CartViewModel
    {
        [Required]
        [DisplayName("E-mail")]
        public string Email { get; set; }
    }
}