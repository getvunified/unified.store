﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.UI.App_Start;
using Jhm.Web.UI.IBootStrapperTasks;

namespace Jhm.Web.UI
{

    public class MvcApplication : HttpApplication
    {

        protected void Application_Start()
        {
            ModelBinders.Binders.Add(typeof(Client), new ClientModelBinder());
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void LogWarning(Exception ex)
        {
            // Log as Warning
            Log.For(this).Warn(
               "[Global Application_Warning]\n\n{0}: {1}\n\nStackTrace: {2}\n\nReferrer: {3}\n\nUrl: {4}\n\nIP: {5}\n\nBrowser: {6}\n",
               ex.GetType(), ex.Message, ex.StackTrace, Request.UrlReferrer, Request.Url, Request.UserHostAddress, Request.Browser);
        }
        protected void LogError(Exception ex)
        {
            Log.For(this).Erroneous(
               "[Global Application_Error]\n\n{0}: {1}\n\nStackTrace: {2}\n\nReferrer: {3}\n\nUrl: {4}\n\nIP: {5}\n\nBrowser: {6}\n",
               ex.GetType(), ex.Message, ex.StackTrace, Request.UrlReferrer, Request.Url, Request.UserHostAddress, Request.Browser);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = ((HttpApplication)sender).Server.GetLastError();
            if (ex == null) return;
            var httpEx = ex as HttpException;
            if (httpEx != null)
            {
                if (httpEx.GetHttpCode() == 404)
                {
                    LogWarning(httpEx);
                }
            }
            else if (ex is System.Web.HttpException && (System.Text.RegularExpressions.Regex.IsMatch(ex.Message, "The file '.*' does not exist.") || System.Text.RegularExpressions.Regex.IsMatch(ex.Message, "The controller for path '.*' was not found or does not implement IController.")))
            {
                LogWarning(ex);
            }
            else
            {
                LogError(ex);
            }
        }
    }
}