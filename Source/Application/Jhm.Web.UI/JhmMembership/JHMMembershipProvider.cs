﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using StructureMap;

namespace Jhm.Web.UI.JhmMembership
{
    public sealed class JHMMembershipProvider : MembershipProvider
    {
        #region Private
        private readonly IServiceCollection service;
        private readonly IUnitOfWorkContainer unitOfWork;

        private int newPasswordLength = 8;
        private string applicationName;
        private bool enablePasswordReset;
        private bool enablePasswordRetrieval;
        private bool requiresQuestionAndAnswer;
        private bool requiresUniqueEmail;
        private int maxInvalidPasswordAttempts;
        private int passwordAttemptWindow;
        private MembershipPasswordFormat passwordFormat;
        private MachineKeySection machineKey;  // Used when determining encryption key values.
        private int minRequiredNonAlphanumericCharacters;
        private int minRequiredPasswordLength;
        private string passwordStrengthRegularExpression;

        #endregion

        public JHMMembershipProvider()
        {
            service = ObjectFactory.GetInstance<IServiceCollection>();
            unitOfWork = ObjectFactory.GetInstance<IUnitOfWorkContainer>();
        }

        //public JHMMembershipProvider(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
        //{
        //    this.service = service;
        //    this.unitOfWork = unitOfWork;
        //}

        //public JHMMembershipProvider(IServiceCollection service, UnitOfWork unitOfWork)
        //{
        //    this.service = service;
        //    this.unitOfWork = unitOfWork;
            
        //    Initialize("{0}MembershipProvider".FormatWith(WebConfigurationManager.AppSettings["ProviderNamePrefix"]), new NameValueCollection(WebConfigurationManager.AppSettings));
        //}

        #region Public Propeties

        

        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }
        public override bool EnablePasswordReset
        {
            get { return enablePasswordReset; }
        }
        public override bool EnablePasswordRetrieval
        {
            get { return enablePasswordRetrieval; }
        }
        public override bool RequiresQuestionAndAnswer
        {
            get { return requiresQuestionAndAnswer; }
        }
        public override bool RequiresUniqueEmail
        {
            get { return requiresUniqueEmail; }
        }
        public override int MaxInvalidPasswordAttempts
        {
            get { return maxInvalidPasswordAttempts; }
        }
        public override int PasswordAttemptWindow
        {
            get { return passwordAttemptWindow; }
        }
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return passwordFormat; }
        }
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return minRequiredNonAlphanumericCharacters; }
        }
        public override int MinRequiredPasswordLength
        {
            get { return minRequiredPasswordLength; }
        }
        public override string PasswordStrengthRegularExpression
        {
            get { return passwordStrengthRegularExpression; }
        }
        #endregion

       
        #region Private Methods

        //single fn to get a membership user by key or username
        private MembershipUser GetMembershipUserByKeyOrUser(bool isKeySupplied, string username, object providerUserKey, bool userIsOnline)
        {
            User user;
            MembershipUser membershipUser = null;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         user = isKeySupplied ? service.AccountService.GetUser((Guid)providerUserKey, applicationName) : service.AccountService.GetUser(username, this.ApplicationName);

                                         if (!user.IsTransient())
                                         {
                                             membershipUser = GetMembershipUserFromUser(user);
                                             if (userIsOnline)
                                             {
                                                 user.LastActivityDate = DateTime.Now;
                                                 service.AccountService.SaveUser(user);
                                             }
                                         }
                                     });
            }
            return membershipUser;
        }

        private User GetUserByUsername(string username)
        {
            User user = null;
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         user = service.AccountService.GetUser(username, ApplicationName);
                                     });
            }
            return user;
        }

        private IList<User> GetUsers()
        {
            IList<User> users = null;
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         users = service.AccountService.GetUsers(ApplicationName);
                                     });
            }
            return users;

        }

        private IList<User> GetUsersLikeUsername(string usernameToMatch)
        {
            IList<User> users = new List<User>();
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         users = service.AccountService.GetUsers(null, usernameToMatch, ApplicationName);
                                     });
            }
            return users;

        }

        private IList<User> GetUsersLikeEmail(string emailToMatch)
        {
            IList<User> users = new List<User>();
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         users = service.AccountService.GetUsers(emailToMatch, null, ApplicationName);
                                     });
            }
            return users;

        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        #endregion

        #region Helper functions

        //Create a Membership user from a IUser class
        private MembershipUser GetMembershipUserFromUser(User usr)
        {
            MembershipUser membershipUser = new MembershipUser(Name,
                                                  usr.Username,
                                                  usr.Id,
                                                  usr.Email,
                                                  usr.PasswordQuestion,
                                                  usr.Comment,
                                                  usr.IsApproved,
                                                  usr.IsLockedOut,
                                                  usr.CreationDate,
                                                  usr.LastLoginDate,
                                                  usr.LastActivityDate,
                                                  usr.LastPasswordChangedDate,
                                                  usr.LastLockedOutDate);

            return membershipUser;
        }

        //Fn that performs the checks and updates associated with password failure tracking
        private void UpdateFailureCount(string username, string failureType)
        {
            DateTime windowStart = new DateTime();
            int failureCount = 0;
            User user = null;

            //using (ISession session = SessionFactory.OpenSession())
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {

                    user = GetUserByUsername(username);

                    if (!user.IsTransient())
                    {
                        if (failureType == "password")
                        {
                            failureCount = user.FailedPasswordAttemptCount;
                            windowStart = user.FailedPasswordAttemptWindowStart;
                        }

                        if (failureType == "passwordAnswer")
                        {
                            failureCount = user.FailedPasswordAnswerAttemptCount;
                            windowStart = user.FailedPasswordAnswerAttemptWindowStart;
                        }
                    }

                    DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

                    if (failureCount == 0 || DateTime.Now > windowEnd)
                    {
                        // First password failure or outside of PasswordAttemptWindow. 
                        // Start a new password failure count from 1 and a new window starting now.

                        if (failureType == "password")
                        {
                            user.FailedPasswordAttemptCount = 1;
                            user.FailedPasswordAttemptWindowStart = DateTime.Now;
                            ;
                        }

                        if (failureType == "passwordAnswer")
                        {
                            user.FailedPasswordAnswerAttemptCount = 1;
                            user.FailedPasswordAnswerAttemptWindowStart = DateTime.Now;
                            ;
                        }
                        service.AccountService.SaveUser(user);
                    }
                    else
                    {
                        if (failureCount++ >= MaxInvalidPasswordAttempts)
                        {
                            // Password attempts have exceeded the failure threshold. Lock out
                            // the user.
                            user.IsLockedOut = true;
                            user.LastLockedOutDate = DateTime.Now;
                            service.AccountService.SaveUser(user);
                        }
                        else
                        {
                            // Password attempts have not exceeded the failure threshold. Update
                            // the failure counts. Leave the window the same.

                            if (failureType == "password")
                                user.FailedPasswordAttemptCount = failureCount;

                            if (failureType == "passwordAnswer")
                                user.FailedPasswordAnswerAttemptCount = failureCount;

                            service.AccountService.SaveUser(user);
                        }
                    }

                });
            }

        }

        //CheckPassword: Compares password values based on the MembershipPasswordFormat.
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
                default:
                    break;
            }

            return pass1 == pass2;
        }

        //EncodePassword:Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        private string EncodePassword(string password)
        {
            var encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword =
                      Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    var hash = new HMACSHA1 { Key = HexToByte(machineKey.ValidationKey) };
                    encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }
            return encodedPassword;
        }

        // UnEncodePassword :Decrypts or leaves the password clear based on the PasswordFormat.
        private string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password =
                      Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        //   Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration.    
        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        #endregion

        #region Public methods

        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from web.config.
                config = new NameValueCollection(WebConfigurationManager.AppSettings);
                if (config == null)
                    throw new ArgumentNullException("config");

            if (string.IsNullOrEmpty(name))
                name = "{0}MembershipProvider".FormatWith(WebConfigurationManager.AppSettings["ProviderNamePrefix"]);

            if (string.IsNullOrEmpty(name))
                name = "JHMMembershipProvider";  //NOTE:  Overriding if the membership name is missing


            // Initialize the abstract base class.
            
            base.Initialize(name, config);

            applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }






            //Encryption skipped
            
            //Configuration cfg = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            //machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");


            machineKey = (MachineKeySection)ConfigurationManager.GetSection("system.web/machineKey");

            if (machineKey.ValidationKey.Contains("AutoGenerate"))
                if (PasswordFormat != MembershipPasswordFormat.Clear)
                    throw new ProviderException("Hashed or Encrypted passwords are not supported with auto-generated keys.");




        }

        // Change password for a user
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            var rowsAffected = false;
            
            if (!ValidateUser(username, oldPwd))
                return false;

            var args = new ValidatePasswordEventArgs(username, newPwd, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Change password canceled due to new password validation failure.");

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         var user = GetUserByUsername(username);

                                         if (!user.IsTransient())
                                         {
                                             user.Password = EncodePassword(newPwd);
                                             user.LastPasswordChangedDate = DateTime.Now;
                                             user.MustChangePasswordOnLogin = false;
                                             user.IsLockedOut = false;
                                             service.AccountService.SaveUser(user);
                                             rowsAffected = true;
                                         }
                                     });
            }

            return rowsAffected;
        }

        // Change Password Question And Answer for a user
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {
            
            var rowsAffected = false;
            if (!ValidateUser(username, password))
                return false;

            //using (ISession session = SessionFactory.OpenSession())
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         var user = GetUserByUsername(username);
                                         if (!user.IsTransient())
                                         {
                                             user.PasswordQuestion = newPwdQuestion;
                                             user.PasswordAnswer = EncodePassword(newPwdAnswer);
                                             service.AccountService.SaveUser(user);
                                             rowsAffected = true;
                                         }
                                     });
            }

            return rowsAffected;
        }

        // Create a new Membership user 
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            var args = new ValidatePasswordEventArgs(username, password, true);
            MembershipCreateStatus myStatus = MembershipCreateStatus.UserRejected;
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser membershipUser = GetUser(username, false);

            if (membershipUser == null)
            {
                var createDate = DateTime.Now;
                using ((unitOfWork.Start()))
                {
                    With.Transaction(() =>
                                         {
                                             var user = new User
                                                            {
                                                                Username = username,
                                                                Password = EncodePassword(password),
                                                                Email = email,
                                                                PasswordQuestion = passwordQuestion,
                                                                PasswordAnswer = EncodePassword(passwordAnswer),
                                                                IsApproved = isApproved,
                                                                Comment = "",
                                                                CreationDate = createDate,
                                                                LastPasswordChangedDate = createDate,
                                                                LastActivityDate = createDate,
                                                                ApplicationName = applicationName,
                                                                IsLockedOut = false,
                                                                LastLockedOutDate = createDate,  //NOTE:  Make this Null or Min Date
                                                                LastLoginDate = createDate,
                                                                FailedPasswordAttemptCount = 0,
                                                                FailedPasswordAttemptWindowStart = createDate,  //NOTE:  Make this Null or Min Date
                                                                FailedPasswordAnswerAttemptCount = 0,
                                                                FailedPasswordAnswerAttemptWindowStart = createDate  //NOTE:  Make this Null or Min Date
                                                            };
                                             service.AccountService.SaveUser(user);
                                             myStatus = user.IsTransient() ? MembershipCreateStatus.UserRejected : MembershipCreateStatus.Success;
                                         });
                }

                status = myStatus;
                return GetUser(username, false);
            }

            status = MembershipCreateStatus.DuplicateUserName;
            return null;
        }

        // Delete a user 
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            int rowsAffected = 0;
            User user;
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         user = GetUserByUsername(username);
                                         if (!user.IsTransient())
                                         {
                                             service.AccountService.DeleteUser(user);
                                             rowsAffected = 1;
                                         }
                                     });
            }

            return rowsAffected > 0;
        }

        // Get all users in db
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            totalRecords = 0;
            IList<User> allusers = null;
            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         int myRecords = service.AccountService.GetAllUserCount(ApplicationName);

                                         if (myRecords <= 0)
                                             return;

                                         allusers = GetUsers();
                                         foreach (User user in allusers.TakeWhile(user => counter < endIndex))
                                         {
                                             if (counter >= startIndex)
                                             {
                                                 var membershipUser = GetMembershipUserFromUser(user);
                                                 users.Add(membershipUser);
                                             }
                                             counter++;
                                         }
                                     });
            }
            return users;
        }

        // Gets a number of online users
        public override int GetNumberOfUsersOnline()
        {
            var onlineSpan = new TimeSpan(0, Membership.UserIsOnlineTimeWindow, 0);
            var compareTime = DateTime.Now.Subtract(onlineSpan);
            var numberOnline = 0;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         numberOnline = service.AccountService.GetNumberOfUsersOnline(compareTime, ApplicationName);
                                     });
            }
            return numberOnline;
        }

        // Get a password for a user
        public override string GetPassword(string username, string answer)
        {
            var password = string.Empty;
            var passwordAnswer = string.Empty;

            if (!EnablePasswordRetrieval)
                throw new ProviderException("Password Retrieval Not Enabled.");

            if (PasswordFormat == MembershipPasswordFormat.Hashed)
                throw new ProviderException("Cannot retrieve Hashed passwords.");

            try
            {
                User user = GetUserByUsername(username);

                if (user.IsTransient())
                    throw new MembershipPasswordException("The supplied user name is not found.");
                if (user.IsLockedOut)
                    throw new MembershipPasswordException("The supplied user is locked out.");

                password = user.Password;
                passwordAnswer = user.PasswordAnswer;
            }
            catch (Exception e)
            {
                //TODO: Log Error
                throw new ProviderException(e.ToString());
            }

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
            {
                UpdateFailureCount(username, "passwordAnswer");
                throw new MembershipPasswordException("Incorrect password answer.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Encrypted)
                password = UnEncodePassword(password);

            return password;
        }

        // Get a membership user by username
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return GetMembershipUserByKeyOrUser(false, username, 0, userIsOnline);
        }

        //  // Get a membership user by key
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return GetMembershipUserByKeyOrUser(true, string.Empty, providerUserKey, userIsOnline);
        }

        //Unlock a user given a username 
        public override bool UnlockUser(string username)
        {
            
            var unlocked = false;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         var user = GetUserByUsername(username);
                                         if (!user.IsTransient())
                                         {
                                             user.LastLockedOutDate = DateTime.Now;
                                             user.IsLockedOut = false;
                                             service.AccountService.SaveUser(user);
                                             unlocked = true;
                                         }
                                     });
            }

            return unlocked;
        }

        //Gets a membership user by email
        public override string GetUserNameByEmail(string email)
        {
            var user = new User();
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                    {
                        var users = service.AccountService.GetUsersByEmail(email, applicationName);
                        if (users.Count >= 1)
                        {
                            user = users[0];
                        }
                    });
            }
            return user.IsTransient() ? string.Empty : user.Username;
        }

        // Reset password for a user
        [Obsolete]
        public override string ResetPassword(string username, string answer)
        {
            var rowsAffected = false;
            User user;

            if (!EnablePasswordReset)
                throw new NotSupportedException("Password reset is not enabled.");

            if (answer == null && RequiresQuestionAndAnswer)
            {
                UpdateFailureCount(username, "passwordAnswer");
                throw new ProviderException("Password answer required for password reset.");
            }

            //var newPassword = Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters);
            if ( newPasswordLength > 32 )
            {
                throw new ProviderException("New password length cannot be greater than 32.");
            }
            var newPassword = Guid.NewGuid().ToString("N").Substring(0, newPasswordLength);
            var args = new ValidatePasswordEventArgs(username, newPassword, true);
            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");

            var passwordAnswer = "";

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {
                    user = GetUserByUsername(username);
                    if (user.IsTransient())
                        throw new MembershipPasswordException(
                            "The supplied user name is not found.");

                    /*if (user.IsLockedOut)
                        throw new MembershipPasswordException(
                            "The supplied user is locked out.");*/
                    passwordAnswer = user.PasswordAnswer;

                    if (RequiresQuestionAndAnswer && EncodePassword(answer) != passwordAnswer)
                    {
                        UpdateFailureCount(username, "passwordAnswer");
                        throw new MembershipPasswordException("Incorrect password answer.");
                    }

                    user.IsLockedOut = false;
                    user.FailedPasswordAnswerAttemptCount = 0;
                    user.Password = EncodePassword(newPassword);
                    user.LastPasswordChangedDate = DateTime.Now;
                    user.Username = username;
                    user.ApplicationName = ApplicationName;
                    user.MustChangePasswordOnLogin = true;
                    service.AccountService.SaveUser(user);
                    rowsAffected = true;
                });
            }

            if (rowsAffected)
                return newPassword;
            
            throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
        }

        // Update a user information 
        public override void UpdateUser(MembershipUser membershipUser)
        {
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {

                    User user = GetUserByUsername(membershipUser.UserName);
                    if (!user.IsTransient())
                    {
                        user.Email = membershipUser.Email;
                        user.Comment = membershipUser.Comment;
                        user.IsApproved = membershipUser.IsApproved;
                        service.AccountService.SaveUser(user);
                    }
                });
            }
        }

        // Validates as user
        public override bool ValidateUser(string username, string password)
        {
            bool isValid = false;
            User user;
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {
                    user = GetUserByUsername(username);
                    if (user.IsTransient() || user.IsLockedOut)
                        return;

                    if (CheckPassword(password, user.Password))
                    {
                        if (user.IsApproved)
                        {
                            isValid = true;
                            user.LastLoginDate = DateTime.Now;
                            service.AccountService.SaveUser(user);
                        }
                    }
                    else
                        UpdateFailureCount(username, "password");
                });
            }
            return isValid;
        }

        // Find users by a name, note : does not do a like search
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            IList<User> allusers;
            var users = new MembershipUserCollection();
            var counter = 0;
            var startIndex = pageSize * pageIndex;
            var endIndex = startIndex + pageSize - 1;
            var myTotalRecords = 0;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {
                    allusers = GetUsersLikeUsername(usernameToMatch);
                    if (allusers == null)
                        return;
                    if (allusers.Count > 0)
                        myTotalRecords = allusers.Count;
                    else
                        return;

                    foreach (User user in allusers)
                    {
                        if (counter >= endIndex)
                            break;
                        if (counter >= startIndex)
                        {
                            var membershipUser = GetMembershipUserFromUser(user);
                            users.Add(membershipUser);
                        }
                        counter++;
                    }
                });
            }

            totalRecords = myTotalRecords;
            return users;
        }

        // Search users by email , NOT a Like match
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            IList<User> allusers;
            var users = new MembershipUserCollection();
            var counter = 0;
            var startIndex = pageSize * pageIndex;
            var endIndex = startIndex + pageSize - 1;
            var myTotalRecords = 0;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         allusers = GetUsersLikeEmail(emailToMatch);
                                         if (allusers == null)
                                             return;
                                         if (allusers.Count > 0)
                                             myTotalRecords = allusers.Count;
                                         else
                                             return;

                                         foreach (User user in allusers)
                                         {
                                             if (counter >= endIndex)
                                                 break;
                                             if (counter >= startIndex)
                                             {
                                                 var membershipUser = GetMembershipUserFromUser(user);
                                                 users.Add(membershipUser);
                                             }
                                             counter++;
                                         }
                                     });
            }
            totalRecords = myTotalRecords;
            return users;
        }

        #endregion
    }
}
