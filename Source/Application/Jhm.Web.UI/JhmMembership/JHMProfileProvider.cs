﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Profile;
using System.Web.Security;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using StructureMap;

namespace Jhm.Web.UI.JhmMembership
{
    public sealed class JHMProfileProvider : ProfileProvider
    {
        public JHMProfileProvider()
        {
            service = ObjectFactory.GetInstance<IServiceCollection>();
            unitOfWork = ObjectFactory.GetInstance<IUnitOfWorkContainer>();
        }

        #region private

        private readonly IServiceCollection service;
        private readonly IUnitOfWorkContainer unitOfWork;
        private string applicationName;

        #endregion


        /*public JHMProfileProvider(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
        {
            this.service = service;
            this.unitOfWork = unitOfWork;
        }*/


        //public JHMProfileProvider(IServiceCollection service, UnitOfWork unitOfWork)
        //{
        //    this.service = service;
        //    this.unitOfWork = unitOfWork;
        //    Initialize("{0}ProfileProvider".FormatWith(WebConfigurationManager.AppSettings["ProviderNamePrefix"]), new NameValueCollection(WebConfigurationManager.AppSettings));
        //}



        #region Properties

        
        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }

        
        #endregion



       


        #region Private Methods
        private Profile GetProfile(string username, bool isAuthenticated)
        {
            bool isAnonymous = !isAuthenticated;
            using ((unitOfWork.Start()))
                return service.AccountService.GetProfile(username, isAnonymous, applicationName);
        }
        private Profile GetProfile(string username)
        {
            using ((unitOfWork.Start()))
            {
                using ((unitOfWork.Start()))
                {
                    return service.AccountService.GetProfile(username, applicationName);
                }
            }
        }
        private Profile GetProfile(Guid userId)
        {
            IProfile profile = null;
            using ((unitOfWork.Start()))
            {
                using ((unitOfWork.Start()))
                {
                    return service.AccountService.GetProfile(userId, applicationName);
                }
            }
        }
        private Profile CreateProfile(string username, bool isAnonymous)
        {
            Profile profile = new Profile();
            bool profileCreated = false;

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         User user = service.AccountService.GetUser(username, applicationName);

                                         if (!user.IsTransient())//membership user exists so create a profile
                                         {
                                             user.AddProfile(profile);
                                             profile.IsAnonymous = isAnonymous;
                                             profile.LastUpdatedDate = DateTime.Now;
                                             profile.LastActivityDate = DateTime.Now;
                                             profile.ApplicationName = this.ApplicationName;
                                             service.AccountService.SaveUser(user);

                                             profileCreated = true;
                                         }
                                         else
                                             throw new ProviderException(
                                                 "Membership User does not exist.Profile cannot be created.");

                                     });

            }

            return (profileCreated) ? profile : null;

        }
        private bool IsMembershipUser(string username)
        {

            bool hasMembership = false;

            //using (ISession session = SessionFactory.OpenSession())
            using ((unitOfWork.Start()))
            {
                User user = service.AccountService.GetUser(username, applicationName);
                if (!user.IsTransient()) //membership user exits so create a profile
                    hasMembership = true;
            }
            return hasMembership;
        }
        private bool IsUserInCollection(MembershipUserCollection uc, string username)
        {
            return (from MembershipUser u in uc where u.UserName.Equals(username) select u).Any();
        }

        // Updates the LastActivityDate and LastUpdatedDate values  when profile properties are accessed by the
        // GetPropertyValues and SetPropertyValues methods. Passing true as the activityOnly parameter will update only the LastActivityDate.

        private void UpdateActivityDates(string username, bool isAuthenticated, bool activityOnly)
        {
            //Is authenticated and IsAnonmous are opposites,so flip sign,IsAuthenticated = true -> notAnonymous
            bool isAnonymous = !isAuthenticated;
            DateTime activityDate = DateTime.Now;

            //using (ISession session = SessionFactory.OpenSession())
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         Profile profile = GetProfile(username, isAuthenticated);
                                         if (profile.IsTransient())
                                             throw new ProviderException("User Profile not found");

                                         if (activityOnly)
                                         {
                                             profile.LastActivityDate = activityDate;
                                             profile.IsAnonymous = isAnonymous;
                                         }
                                         else
                                         {
                                             profile.LastActivityDate = activityDate;
                                             profile.LastUpdatedDate = activityDate;
                                             profile.IsAnonymous = isAnonymous;
                                         }

                                         service.AccountService.SaveUser(profile.User);
                                     });
            }
        }
        private bool DeleteProfile(string username)
        {
            // Check for valid user name.
            if (username == null)
                throw new ArgumentNullException("User name cannot be null.");
            if (username.Contains(","))
                throw new ArgumentException("User name cannot contain a comma (,).");


            var user = service.AccountService.GetUser(username, applicationName);
            if(user.IsTransient())
                throw new Exception("User Could Not Be found!");
            
            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         user.AddProfile(new Profile());
                                         service.AccountService.SaveUser(user);
                                     });
            }

            return true;
        }
        private bool DeleteProfile(Guid userId)
        {
           var user = service.AccountService.GetUser(userId, applicationName);
            if (user.IsTransient())
                throw new Exception("User Could Not Be found!");

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                {
                    user.AddProfile(new Profile());
                    service.AccountService.SaveUser(user);
                });
            }

            return true;
        }
        private int DeleteProfilesbyId(string[] ids)
        {
            return ids.Count(id => DeleteProfile(id));
        }
        private void CheckParameters(int pageIndex, int pageSize)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Page index must 0 or greater.");
            if (pageSize < 1)
                throw new ArgumentException("Page size must be greater than 0.");
        }
        private ProfileInfo GetProfileInfoFromProfile(Profile profile)
        {
            //User user = null;
            //using ((unitOfWork.Start()))
            //{
            //    user = service.AccountService.GetUser(profile.UserId, applicationName);
            //}

            //if (user.IsTransient())
            //    throw new ProviderException("The User Was Not Found");

            using (unitOfWork.Start())
            {
                return new ProfileInfo(profile.User.Username, profile.IsAnonymous, profile.LastActivityDate, profile.LastUpdatedDate, 0);
            }
        }
        #endregion

        #region Public Methods
        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection ppc)
        {
            string username = (string)context["UserName"];
            bool isAuthenticated = (bool)context["IsAuthenticated"];

            Profile profile = GetProfile(username, isAuthenticated);
            // The serializeAs attribute is ignored in this provider implementation.
            SettingsPropertyValueCollection settingsCollection = new SettingsPropertyValueCollection();

            if (profile.IsTransient())
            {
                if (IsMembershipUser(username))
                    profile = CreateProfile(username, false);
                else
                    throw new ProviderException("Profile cannot be created. There is no membership user");
            }


            foreach (SettingsProperty prop in ppc)
            {
                SettingsPropertyValue settings = new SettingsPropertyValue(prop);
                switch (prop.Name)
                {
                    case "IsAnonymous":
                        settings.PropertyValue = profile.IsAnonymous;
                        break;
                    case "LastActivityDate":
                        settings.PropertyValue = profile.LastActivityDate;
                        break;
                    case "LastUpdatedDate":
                        settings.PropertyValue = profile.LastUpdatedDate;
                        break;
                    case "Subscription":
                        settings.PropertyValue = profile.Subscription;
                        break;
                    case "Language":
                        settings.PropertyValue = profile.Language;
                        break;
                    case "FirstName":
                        settings.PropertyValue = profile.FirstName;
                        break;
                    case "LastName":
                        settings.PropertyValue = profile.LastName;
                        break;
                    case "Gender":
                        settings.PropertyValue = profile.Gender;
                        break;
                    case "BirthDate":
                        settings.PropertyValue = profile.BirthDate;
                        break;
                    /*case "Occupation":
                        settings.PropertyValue = profile.Occupation;
                        break;*/
                    case "Website":
                        settings.PropertyValue = profile.Website;
                        break;
                    /*case "Street":
                        settings.PropertyValue = profile.Street;
                        break;
                    case "City":
                        settings.PropertyValue = profile.City;
                        break;
                    case "State":
                        settings.PropertyValue = profile.State;
                        break;
                    case "Zip":
                        settings.PropertyValue = profile.Zip;
                        break;
                    case "Country":
                        settings.PropertyValue = profile.Country;
                        break;*/

                    default:
                        throw new ProviderException("Unsupported property.");
                }

                settingsCollection.Add(settings);
            }

            UpdateActivityDates(username, isAuthenticated, true);
            return settingsCollection;
        }
        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection ppvc)
        {
            Profile profile = null;
            // The serializeAs attribute is ignored in this provider implementation.
            string username = (string)context["UserName"];
            bool isAuthenticated = (bool)context["IsAuthenticated"];

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {

                                         var user = service.AccountService.GetUser(username, applicationName);

                                         if(!user.IsTransient())
                                             

                                         if (user.Profile.IsTransient())
                                             profile = CreateProfile(username, !isAuthenticated);

                                         foreach (SettingsPropertyValue settings in ppvc)
                                         {
                                             switch (settings.Property.Name)
                                             {
                                                 case "IsAnonymous":
                                                     profile.IsAnonymous = (bool)settings.PropertyValue;
                                                     break;
                                                 case "LastActivityDate":
                                                     profile.LastActivityDate = (DateTime)settings.PropertyValue;
                                                     break;
                                                 case "LastUpdatedDate":
                                                     profile.LastUpdatedDate = (DateTime)settings.PropertyValue;
                                                     break;
                                                 case "Subscription":
                                                     profile.Subscription = settings.PropertyValue.ToString();
                                                     break;
                                                 case "Language":
                                                     profile.Language = settings.PropertyValue.ToString();
                                                     break;
                                                 case "FirstName":
                                                     profile.FirstName = settings.PropertyValue.ToString();
                                                     break;
                                                 case "LastName":
                                                     profile.LastName = settings.PropertyValue.ToString();
                                                     break;
                                                 case "Gender":
                                                     profile.Gender = settings.PropertyValue.ToString();
                                                     break;
                                                 case "BirthDate":
                                                     profile.BirthDate = (DateTime)settings.PropertyValue;
                                                     break;
                                                 /*case "Occupation":
                                                     profile.Occupation = settings.PropertyValue.ToString();
                                                     break;*/
                                                 case "Website":
                                                     profile.Website = settings.PropertyValue.ToString();
                                                     break;
                                                 case "Company":
                                                     profile.Company = settings.PropertyValue.ToString();
                                                     break;
                                                 /*case "Street":
                                                     profile.Street = settings.PropertyValue.ToString();
                                                     break;
                                                 case "City":
                                                     profile.City = settings.PropertyValue.ToString();
                                                     break;
                                                 case "State":
                                                     profile.State = settings.PropertyValue.ToString();
                                                     break;
                                                 case "Zip":
                                                     profile.Zip = settings.PropertyValue.ToString();
                                                     break;
                                                 case "Country":
                                                     profile.Country = settings.PropertyValue.ToString();
                                                     break;*/
                                                 default:
                                                     throw new ProviderException("Unsupported property.");
                                             }
                                         }

                                         user.AddProfile(profile);
                                         service.AccountService.SaveUser(user);
                                     });
            }

            UpdateActivityDates(username, isAuthenticated, false);
        }
        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            return profiles.Cast<ProfileInfo>().Count(p => DeleteProfile(p.UserName));
        }
        public override int DeleteProfiles(string[] usernames)
        {
            return usernames.Count(user => DeleteProfile(user));
        }
        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            string userIds = "";
            bool isAnonymous = false;
            switch (authenticationOption)
            {
                case ProfileAuthenticationOption.Anonymous:
                    isAnonymous = true;
                    break;
                case ProfileAuthenticationOption.Authenticated:
                    isAnonymous = false;
                    break;
                default:
                    break;
            }

            using ((unitOfWork.Start()))
            {
                With.Transaction(() =>
                                     {
                                         var profiles = service.AccountService.GetProfiles(userInactiveSinceDate, isAnonymous, applicationName);
                                         if(profiles != null)
                                             userIds = profiles.Aggregate(userIds, (current, profile) => current + (profile.Id.ToString() + ","));
                                     });
            }

            //NOTE:  UGLY
            if (userIds.Length > 0)
                userIds = userIds.Substring(0, userIds.Length - 1);


            return DeleteProfilesbyId(userIds.Split(','));
        }
        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch,int pageIndex,int pageSize,out int totalRecords)
        {
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, usernameToMatch, null, pageIndex, pageSize, out totalRecords);
        }
        public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch,DateTime userInactiveSinceDate,int pageIndex,int pageSize,out int totalRecords)
        {
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, usernameToMatch, userInactiveSinceDate, pageIndex, pageSize, out totalRecords);
        }
        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex,int pageSize,out int totalRecords)
        {
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, null, null, pageIndex, pageSize, out totalRecords);
        }
        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate,int pageIndex,int pageSize,out int totalRecords)
        {
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, null, userInactiveSinceDate, pageIndex, pageSize, out totalRecords);
        }
        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            int inactiveProfiles = 0;
            ProfileInfoCollection profiles = GetProfileInfo(authenticationOption, null, userInactiveSinceDate, 0, 0, out inactiveProfiles);
            return inactiveProfiles;
        }


        // GetProfileInfo
        // Retrieves a count of profiles and creates a 
        // ProfileInfoCollection from the profile data in the 
        // database. Called by GetAllProfiles, GetAllInactiveProfiles,
        // FindProfilesByUserName, FindInactiveProfilesByUserName, 
        // and GetNumberOfInactiveProfiles.
        // Specifying a pageIndex of 0 retrieves a count of the results only.
        //
        private ProfileInfoCollection GetProfileInfo(ProfileAuthenticationOption authenticationOption, string usernameToMatch,object userInactiveSinceDate,int pageIndex,int pageSize,out int totalRecords)
        {

            bool isAnonymous = false;
            ProfileInfoCollection profilesInfoColl = new ProfileInfoCollection();
            switch (authenticationOption)
            {
                case ProfileAuthenticationOption.Anonymous:
                    isAnonymous = true;
                    break;
                case ProfileAuthenticationOption.Authenticated:
                    isAnonymous = false;
                    break;
                default:
                    break;
            }

            //using (ISession session = SessionFactory.OpenSession())
            using ((unitOfWork.Start()))
            {
                try
                {
                    var profiles = (List<Profile>)service.AccountService.GetProfiles((DateTime)userInactiveSinceDate, isAnonymous, ApplicationName);

                   
                    List<Profile> profiles2 = new List<Profile>();

                    if (profiles == null)
                        totalRecords = 0;
                    else if (profiles.Count < 1)
                        totalRecords = 0;
                    else
                        totalRecords = profiles.Count;



                    //IF USER NAME TO MATCH then filter out those
                    //Membership.JHMMembershipProvider us = new JHMProviders.Membership.JHMMembershipProvider();
                    //us.g
                    
                    MembershipUserCollection uc = Membership.FindUsersByName(usernameToMatch);

                    if (usernameToMatch != null)
                    {
                        if (totalRecords > 0)
                        {
                            if(profiles != null)
                                profiles2.AddRange(profiles.Where(profile => IsUserInCollection(uc, usernameToMatch)));

                            if (profiles2.Count < 1)
                                profiles2 = profiles;
                            else
                                totalRecords = profiles2.Count;
                        }
                        else
                            profiles2 = profiles;
                    }
                    else
                        profiles2 = profiles;




                    if (totalRecords <= 0)
                        return profilesInfoColl;

                    if (pageSize == 0)
                        return profilesInfoColl;

                    int counter = 0;
                    int startIndex = pageSize * (pageIndex - 1);
                    int endIndex = startIndex + pageSize - 1;

                    foreach (Profile profile in profiles2.TakeWhile(profile => counter < endIndex))
                    {
                        if (counter >= startIndex)
                        {
                            ProfileInfo pi = GetProfileInfoFromProfile(profile);
                            profilesInfoColl.Add(pi);
                        }
                        counter++;
                    }
                }
                catch (Exception e)
                {
                    //TODO: Log Error
                    throw;
                }
            }
            return profilesInfoColl;
        }

        #endregion
    }
}
