﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Text;
using System.Web.Security;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using StructureMap;


namespace Jhm.Web.UI.JhmMembership
{
    public sealed class JHMRoleProvider : RoleProvider
    {
        private readonly IServiceCollection service;
        private readonly IUnitOfWorkContainer unitOfWork;

        public JHMRoleProvider()
        {
            service = ObjectFactory.GetInstance<IServiceCollection>();
            unitOfWork = ObjectFactory.GetInstance<IUnitOfWorkContainer>();
        }

        //public JHMRoleProvider(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
        //{
        //    this.service = service;
        //    this.unitOfWork = unitOfWork;
        //}

        //public JHMRoleProvider(IServiceCollection service, UnitOfWork unitOfWork)
        //{

        //    this.service = service;
        //    this.unitOfWork = unitOfWork;

        //    Initialize(string.Format("{0}RoleProvider", WebConfigurationManager.AppSettings["ProviderNamePrefix"]), new NameValueCollection(WebConfigurationManager.AppSettings));
        //}


        #region private
        //TODO:  Come look at me..
        private string applicationName = "Jhm.Web";
        #endregion

        #region Properties



        public override string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }
        #endregion

        #region Private Methods

        //get a role by name
        private IRole GetRole(string rolename)
        {
            IRole role = null;
            using (unitOfWork.Start())
            {
                With.Transaction(() => service.AccountService.GetRole(rolename, applicationName));
            }
            return role;
        }

        #endregion

        #region Public Methods

        //adds a user collection to a role collection
        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            IUser user = null;
            foreach (string rolename in rolenames)
                if (!RoleExists(rolename))
                    throw new ProviderException(String.Format("Role name {0} not found.", rolename));

            foreach (string username in usernames)
            {
                if (username.Contains(","))
                    throw new ArgumentException(String.Format("User names {0} cannot contain commas.", username));
                //is user not exiting //throw exception

                foreach (string rolename in rolenames)
                {
                    if (IsUserInRole(username, rolename))
                        throw new ProviderException(String.Format("User {0} is already in role {1}.", username, rolename));
                }
            }

            using (unitOfWork.Start())
            {
                With.Transaction(() => service.AccountService.AddUsersToRoles(usernames, rolenames, applicationName));
            }
        }

        //create  a new role with a given name
        public override void CreateRole(string rolename)
        {
            if (rolename.Contains(","))
                throw new ArgumentException("Role names cannot contain commas.");

            if (RoleExists(rolename))
                throw new ProviderException("Role name already exists.");

            //using (ISession session = SessionFactory.OpenSession())
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         var role = new Role() { RoleName = rolename, ApplicationName = applicationName };
                                         service.AccountService.CreateRole(role);
                                     });
            }
        }

        //delete a role with given name
        public override bool DeleteRole(string rolename, bool throwOnPopulatedRole)
        {
            bool deleted = false;
            if (!RoleExists(rolename))
                throw new ProviderException("Role does not exist.");

            if (throwOnPopulatedRole && GetUsersInRole(rolename).Length > 0)
                throw new ProviderException("Cannot delete a populated role.");

            //using (ISession session = SessionFactory.OpenSession())
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         service.AccountService.DeleteRole(new Role { ApplicationName = applicationName, RoleName = rolename });
                                         deleted = true;
                                     });
            }

            return deleted;
        }

        //get an array of all the roles
        public override string[] GetAllRoles()
        {
            StringBuilder sb = new StringBuilder();
            using (unitOfWork.Start())
            {
                IList<Role> roles = new List<Role>();
                With.Transaction(() =>
                                     {
                                         roles = service.AccountService.GetAllRoles(applicationName).ToArray();
                                     });

                foreach (Role role in roles)
                {
                    sb.Append(role.RoleName + ",");
                }
            }

            if (sb.Length > 0)
            {
                //NOTE:  UGLY
                // Remove trailing comma.
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString().Split(',');
            }

            return new string[0];
        }

        //Get roles for a user by username
        public override string[] GetRolesForUser(string username)
        {
            User user = null;
            List<Role> userRoles = null;
            StringBuilder sb = new StringBuilder();
            //using (ISession session = SessionFactory.OpenSession())
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         user = service.AccountService.GetUser(username, applicationName);

                                         if (!user.IsTransient())
                                         {
                                             userRoles = user.Roles.ToList();
                                             foreach (Role r in userRoles)
                                                 sb.Append(r.RoleName + ",");
                                         }
                                     });
            }

            if (sb.Length > 0)
            {
                //NOTE:  UGLY
                // Remove trailing comma.
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString().Split(',');
            }

            return new string[0];
        }
        //Get users in a given role name
        public override string[] GetUsersInRole(string rolename)
        {
            StringBuilder sb = new StringBuilder();
            //using (ISession session = SessionFactory.OpenSession())
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         Role role = service.AccountService.GetRole(rolename, applicationName);
                                         if (!role.IsTransient())
                                         {
                                             IEnumerable<User> users = role.UsersInRole;
                                             foreach (User user in users)
                                                 sb.Append(user.Username + ",");
                                         }
                                     });
            }

            if (sb.Length > 0)
            {
                //NOTE: UGLY
                // Remove trailing comma.
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString().Split(',');
            }

            return new string[0];
        }

        //determine is a user has a given role
        public override bool IsUserInRole(string username, string rolename)
        {
            bool userIsInRole = false;
            User user = null;
            IList<IRole> userRoles = null;
            StringBuilder sb = new StringBuilder();
            //using (ISession session = SessionFactory.OpenSession())
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         user = service.AccountService.GetUser(username, applicationName);
                                         if (!user.IsTransient())
                                             userIsInRole = user.Roles.Any(userRole => user.Roles.Any(r => r.RoleName.Equals(rolename)));
                                     });
            }
            return userIsInRole;
        }

        //remove users from roles
        public override void RemoveUsersFromRoles(string[] usernames, string[] rolenames)
        {
            foreach (string rolename in rolenames)
                if (!RoleExists(rolename))
                    throw new ProviderException(String.Format("Role name {0} not found.", rolename));

            foreach (string username in usernames)
                foreach (string rolename in rolenames)
                    if (!IsUserInRole(username, rolename))
                        throw new ProviderException(String.Format("User {0} is not in role {1}.", username, rolename));

            using (unitOfWork.Start())
            {
                With.Transaction(() => service.AccountService.RemoveUsersFromRoles(usernames, rolenames, applicationName));
            }

        }

        //bool to check if a role exists given a role name
        public override bool RoleExists(string rolename)
        {
            Role role = new Role();
            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                {
                    role = service.AccountService.GetRole(rolename, applicationName);
                });
                return !role.IsTransient();
            }
        }

        //find users that belong to a particular role , given a username, Note : does not do a LIke search
        public override string[] FindUsersInRole(string rolename, string usernameToMatch)
        {
            StringBuilder sb = new StringBuilder();

            using (unitOfWork.Start())
            {
                With.Transaction(() =>
                                     {
                                         Role role = service.AccountService.GetRole(rolename, applicationName);
                                         if (!role.IsTransient())
                                         {
                                             IEnumerable<User> users = role.UsersInRole;

                                             foreach (var u in users.Where(u => String.Compare(u.Username, usernameToMatch, true) == 0))
                                                 sb.Append(u.Username + ",");
                                         }

                                     });
                if (sb.Length > 0)
                {
                    //NOTE:  UGLY
                    // Remove trailing comma.
                    sb.Remove(sb.Length - 1, 1);
                    return sb.ToString().Split(',');
                }
                return new string[0];
            }
        }



        #endregion


    }
}
