﻿using System;
using System.Web;
using Jhm.Web.Core.Models;

namespace Jhm.Web.UI.Admin.Models
{
    public class JhmMagazineItemViewModel: JhmMagazineListingItem
    {
        public HttpPostedFileBase ImageFile { get; set; }
        public Guid IssueId { get; set; }

        internal JhmMagazineListingItem MapTo(JhmMagazineListingItem to)
        {
            to.Description = Description;
            to.ImagePath = ImagePath;
            to.IsActive = IsActive;
            to.MagazineContentId = MagazineContentId;
            to.SubTitle = SubTitle;
            to.Title = Title;
            to.IsMagazine = IsMagazine;
            to.Rank = Rank;

            return to;
        }

        internal static JhmMagazineItemViewModel MapFrom(JhmMagazineListingItem from)
        {
            var to = new JhmMagazineItemViewModel
                {
                    Description = from.Description,
                    IssueId = from.Id,
                    ImagePath = from.ImagePath,
                    IsActive = from.IsActive,
                    MagazineContentId = from.MagazineContentId,
                    Title = from.Title,
                    SubTitle = from.SubTitle,
                    IsMagazine = from.IsMagazine,
                    Rank = from.Rank
                };
            return to;
        }
    }
}