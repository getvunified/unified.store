﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.UI.Admin.Models
{
    public class JhmMagazineListingViewModel : CartViewModel
    {
        public IList<JhmMagazineListingItem> Issues { get; set; }

        
        public bool ShowOnlyActive { get; set; }
    }
}