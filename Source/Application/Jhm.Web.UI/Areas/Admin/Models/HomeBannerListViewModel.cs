﻿using System.Collections.Generic;
using System.ComponentModel;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Admin.Models
{
    public class HomeBannerListViewModel
    {
        public int SelectCompanyCode { get; set; }

        private List<HomeBannerItem> _results;
        public List<HomeBannerItem> Results
        {
            get { return _results ?? (_results = new List<HomeBannerItem>()); }
            set { _results = value; }
        }

        public IList<HomeBannerItem> Banners { get; set; }

        public IEnumerable<Company> AvailableCompanies { get; set; }

        [DisplayName("Show Only Visible Videos")]
        public bool ShowOnlyVisible { get; set; }

        [DisplayName("Show Only Active Videos")]
        public bool ShowOnlyActive { get; set; }

        [DisplayName("Hide Expired Videos")]
        public bool HideExpired { get; set; }
    }
}