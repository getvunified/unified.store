﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Common;
using Jhm.Web.Core.Models;

namespace Jhm.Web.UI.Admin.Models
{
    public class HomeBannerViewModel
    {
        public Guid Id { get; set; }
        [DisplayName("Banner Image")]
        public string ImageUrl { get; set; }
        [DisplayName("Banner Target Page")]
        public string ImageLinkUrl { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public int Position { get; set; }
        [DisplayName("Is Active")]
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CustomAttributes { get; set; }
        public string ExternalHtml { get; set; }

        [DisplayName("Open Target In New Window")]
        public bool OpenTargetInNewWindow { get; set; }

        [DisplayName("Available Companies")]
        public IEnumerable<HomeBannerCompany> AvailableCompanies { set; get; }
        private Guid[] _selectedCompanies;
        public Guid[] SelectedCompanies
        {
            set { _selectedCompanies = value; }
            get { return (_selectedCompanies = _selectedCompanies ?? new Guid[0]); }
        }

        public HttpPostedFileBase ImageFile { get; set; }
        public IEnumerable<SelectListItem> PositionsList { get; set; }
        public string SelectedPosition { get; set; }

        public static HomeBannerViewModel CreateFrom(HomeBannerItem from)
        {
            var to = new HomeBannerViewModel
            {
                Id = from.Id,
                CustomAttributes = from.CustomAttributes,
                EndDate = from.EndDate,
                ExternalHtml = from.ExternalHtml,
                ImageLinkUrl = from.ImageLinkUrl,
                ImageUrl = from.ImageUrl,
                IsActive = from.IsActive,
                Position = from.Position,
                SelectedCompanies = from.Companies.Select(x => x.Id).ToArray(),
                StartDate = from.StartDate,
                SubTitle = from.SubTitle,
                Title = from.Title,
                OpenTargetInNewWindow = from.OpenTargetInNewWindow
            };
            return to;
        }

        internal void MapTo(HomeBannerItem to)
        {
            to.CustomAttributes = CustomAttributes;
            
            to.EndDate = EndDate == null? (DateTime?)null: EndDate.Value.SetTime(23, 59, 59);
            to.StartDate = StartDate == null ? (DateTime?)null : StartDate.Value.SetTime(0, 0, 0);

            to.ExternalHtml = ExternalHtml;
            to.ImageLinkUrl = ImageLinkUrl;
            to.ImageUrl = ImageUrl;
            to.IsActive = IsActive;
            to.Position = Position;
            to.SubTitle = SubTitle;
            to.Title = Title;
            to.OpenTargetInNewWindow = OpenTargetInNewWindow;
        }
    }
}