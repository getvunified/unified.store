﻿using System;

namespace Jhm.Web.UI.Admin.Models
{
    public class DiscountcodeViewModel
    {
        public Guid Id { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual DateTime LaunchDate { get; set; }
        public virtual bool AutoApply { get; set; }
        public virtual bool FreeGift { get; set; }
        public virtual string ProductCode { get; set; }



    }
}