﻿namespace Jhm.Web.UI.Areas.Admin.Models
{
    public class DiscountCodeParentChildReturnModel
    {
        public string JsonForTreeView { get; set; }
        public string JsonForAllInfo { get; set; }
    }
}