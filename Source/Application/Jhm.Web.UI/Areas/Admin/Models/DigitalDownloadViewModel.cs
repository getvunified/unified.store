﻿using System;

namespace Jhm.Web.UI.Admin.Models
{
    public class DigitalDownloadViewModel
    {
        public Guid Id { get; set; }
        public virtual DateTime ExpirationDate { get; set; }
        public virtual string ExpirationDateFormatted { get { return ExpirationDate.ToShortDateString(); } }
        public virtual int DownloadAttemptsRemaining { get; set; }
    }
}