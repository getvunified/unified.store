﻿using System;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.UOW;
using Jhm.Common.Utilities;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Service;
using Jhm.Web.UI.Admin.Models;
using Jhm.Web.UI.Controllers;
using Jhm.Web.UI.IBootStrapperTasks;
using NHibernate.Linq;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin,CustomerService")]
    public class CustomerServiceController : BaseController<IServiceCollection>
    {
        public CustomerServiceController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork) {}
        //
        // GET: /Admin/CustomerService/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        #region SendPropheticBlessingEmail
        [Authorize(Roles = "SuperAdmin,Admin,ContentEditor,CustomerService")]
        public ActionResult SendPropheticBlessingEmail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendPropheticBlessingEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                TempData[MagicStringEliminator.Messages.ErrorMessage] = "User's Email Address is required";
                return View();
            }

            if (SendTreasureChestEmail(email, service.DonationService.GetDonation(MockDonationRepository.ProphecticBlessingCode)))
            {
                TempData[MagicStringEliminator.Messages.Status] = "Treasure Chest Email was successfully sent to " + email;
            }
            return View();
        }

        private bool SendTreasureChestEmail(string email, IDonation donation)
        {
            try
            {
                var body = System.IO.File.ReadAllText(Server.MapPath("/EmailTemplates/{0}".FormatWith(donation.CustomEmailTemplateFileName ?? "generic.html")));


                var replacements = new ListDictionary
                                       {
                                           {"<%CONTENT%>", donation.CustomEmailBody},
                                           {"<%EMAIL%>", email},
                                           {"<%HTTP_HOST%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"]},
                                           {"<%IMAGES_PATH%>","http://" + HttpContext.Request.ServerVariables["HTTP_HOST"] + Url.Content("/EmailTemplates/")}
                                       };

                var md = new MailDefinition
                {
                    From = EmailHelper.EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = "John Hagee Ministries - " + donation.Title + " Donation"
                };

                var msg = md.CreateMailMessage(email, replacements, body, new System.Web.UI.Control());
                service.EmailService.SendEmail(msg);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                TempData[MagicStringEliminator.Messages.ErrorMessage] = ex.Message;
                return false;
            }
        }
        
        #endregion

        public ActionResult DigitalDownloadsManager()
        {
            var model = new DigitalDownloadManagerViewModel();
            model.DownloadStatuses = new [] {"Active", "Inactive"};
            return View(model);
        }

        [HttpPost]
        public ActionResult DigitalDownloadsManager(DigitalDownloadManagerViewModel model)
        {
            return TryOnEvent(() =>
            {
                var results = service.DigitalDownloadService.GetAllBy(model);
                model.DownloadStatuses = new[] {"Active", "Inactive"};
                results.ForEach(x => x.User.Profile.IsTransient());
                model.Results = results;
                return View(model);
            });
        }

        [HttpPost]
        public ActionResult EditDigitalDownload(DigitalDownloadViewModel model)
        {
            return TryOnEvent(() =>
            {
                var dd = service.DigitalDownloadService.GetDigitalDownload(model.Id);
                if ( dd == null )
                {
                    throw new Exception("DigitalDownload not found!");
                }

                var expirationDate = model.ExpirationDate.SetTime(23, 59, 59);
                if (Foundation.DateTimeUtilities.IsValidSqlDateTime(expirationDate))
                {
                    dd.DownloadAttemptsRemaining = model.DownloadAttemptsRemaining;
                    dd.ExpirationDate = expirationDate;
                    TempData[MagicStringEliminator.Messages.Status] = "Digital Download was successfully updated";
                    return RedirectToAction("DigitalDownloadsManager");
                }
                TempData[MagicStringEliminator.Messages.ErrorMessage] = String.Format("Expiration Date {0} is not a valid date time.", expirationDate);
                return RedirectToAction("DigitalDownloadsManager");
            });
        }
    }
}
