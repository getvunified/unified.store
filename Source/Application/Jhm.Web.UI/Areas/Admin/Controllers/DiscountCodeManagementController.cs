﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Foundation;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using Jhm.Web.UI.Areas.Admin.Models;
using Jhm.Web.UI.Controllers;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    public class DiscountCodeManagementController : BaseController<IServiceCollection>
    {
        //
        // GET: /Admin/DiscountCodeManagement/

        public DiscountCodeManagementController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork) {}

        public ActionResult Index()
        {
            var model = new DiscountCodeParentChildReturnModel();
            return TryOnEvent(() =>
                {
                    var discountCodes = service.DiscountCodeService.GetAllDiscountCodes();
                    model.JsonForAllInfo = discountCodes.ToJSON();
                    model.JsonForTreeView = discountCodes.Select(x => new
                        {
                            label = x.CodeName,
                            id = x.Id,
                            children = x.SingleUseDiscountCodes.Count > 0 ? x.SingleUseDiscountCodes.Select(y => new
                                {
                                    label = y.DiscountCodeSuffix,
                                    id = y.Id
                                }).ToJSON() : string.Empty,
                        }).ToJSON();
                    return View(model);
                });
        }

        [HttpPost]
        public ActionResult ReturnAllDiscountCodes()
        {
            var returnCodes = new List<DiscountCode>();
            
            TryWithTransaction(() =>
            {
                returnCodes = service.DiscountCodeService.GetAllDiscountCodes().ToList();
            });
            return Json(returnCodes);
        }

    }
}
