﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using Jhm.Web.UI.Admin.Models;
using Jhm.Web.UI.Controllers;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin,ContentEditor,CustomerService")]
    public class JhmMagazineListingController : BaseController<IServiceCollection>
    {
        public JhmMagazineListingController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork){}
        
        //
        // GET: /Admin/JhmMagazineListing/

        public ActionResult Index()
        {
            return TryOnEvent(() =>
            {
                var model = new JhmMagazineListingViewModel
                    {
                        Issues = service.JhmMagazineListingService.GetAllActive(),
                        ShowOnlyActive = true
                    };
                return View(model);
            });
        }

        [HttpPost]
        public ActionResult Index(JhmMagazineListingViewModel model)
        {
            return TryOnEvent(() =>
            {
                model.Issues = model.ShowOnlyActive? service.JhmMagazineListingService.GetAllActive() : service.JhmMagazineListingService.GetAll();
                return View(model);
            });
        }

        
        public ActionResult Create()
        {
            return View(new JhmMagazineItemViewModel());
        }

        [HttpPost]
        public ActionResult Create(JhmMagazineItemViewModel model)
        {
            return TryOnEvent(() =>
            {
                var savedFileName = SaveImageFile(model.ImageFile);
                if (!String.IsNullOrEmpty(savedFileName))
                {
                    model.ImagePath = "http://media.jhm.org/Images/Web/JhmMagazines/" + savedFileName;
                }
                var newIssue = new JhmMagazineListingItem();
                newIssue.DateCreated = DateTime.Now;
                service.JhmMagazineListingService.SaveChanges(model.MapTo(newIssue));

                return RedirectToAction("Index");
            });
        }

        public ActionResult Edit(Guid id)
        {
            return TryOnEvent(() =>
            {
                var model = service.JhmMagazineListingService.GetById(id);
                if (model == null)
                {
                    throw new Exception("Magazine issue not found");
                }
                return View(JhmMagazineItemViewModel.MapFrom(model));
            });
        }

        [HttpPost]
        public ActionResult Edit(JhmMagazineItemViewModel model)
        {
            return TryOnEvent(() =>
            {
                var savedFileName = SaveImageFile(model.ImageFile);
                if (!String.IsNullOrEmpty(savedFileName))
                {
                    model.ImagePath = "http://media.jhm.org/Images/Web/JhmMagazines/" + savedFileName;
                }
                var issue = service.JhmMagazineListingService.GetById(model.IssueId);
                service.JhmMagazineListingService.SaveChanges(model.MapTo(issue));

                return RedirectToAction("Index");
            });
        }

        private string SaveImageFile(HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file == null || file.ContentLength <= 0) return null;

            var fileName = Path.GetFileName(file.FileName) ?? Guid.NewGuid().ToString("N") + ".jpg";

            var directory = GetPath(ConfigurationManager.AppSettings["MediaImagesRoot"]) + "Web\\JhmMagazines\\";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var path = Path.Combine(directory, fileName);
            file.SaveAs(path);

            //ResizeImage(path, destinationFolder + videoId + "_small.jpg", 250, 140, false);
            //ResizeImage(path, destinationFolder + videoId + "_large.jpg", 840, 472, false);

            //System.IO.File.Delete(path);
            return fileName;
        }

        private string GetPath(string path)
        {
            return Path.IsPathRooted(path) ? path : Server.MapPath(path);
        }

        public ActionResult Delete(Guid id)
        {
            return TryOnEvent(() =>
            {
                service.JhmMagazineListingService.Delete(id);
                return RedirectToAction("Index");
            });
        }
    }
}
