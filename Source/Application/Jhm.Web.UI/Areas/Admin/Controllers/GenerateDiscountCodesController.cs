﻿using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class GenerateDiscountCodesController : BaseController<IServiceCollection>
    {
        public GenerateDiscountCodesController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
            throw new System.NotImplementedException();
        }

        //
        // GET: /Admin/GenerateDiscountCodes/
        [HttpPost]
        public ActionResult Index()
        {
            return View();
        }

    }
}
