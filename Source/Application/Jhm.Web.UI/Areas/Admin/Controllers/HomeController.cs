﻿using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{

    [Authorize(Roles = "SuperAdmin,Admin,ContentEditor,CustomerService")]
    public class HomeController : BaseController<IServiceCollection>
    {
        //
        // GET: /Admin/Home/

        public HomeController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        
    }
}
