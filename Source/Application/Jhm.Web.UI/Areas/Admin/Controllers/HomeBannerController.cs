﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jhm.Common.UOW;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using Jhm.Web.UI.Admin.Models;
using Jhm.Web.UI.Controllers;
using getv.donorstudio.core.Global;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin,ContentEditor")]
    public class HomeBannerController : BaseController<IServiceCollection>
    {
        //
        // GET: /Admin/HomeBanner/

        public HomeBannerController(IServiceCollection service, IUnitOfWorkContainer unitOfWork) : base(service, unitOfWork){}

        public ActionResult Index()
        {
            return TryOnEvent(() =>
            {
                var model = new HomeBannerListViewModel
                {
                    Banners = service.HomeBannerService.GetAll(),
                    AvailableCompanies = Company.GetAll<Company>(),
                    ShowOnlyVisible = false,
                    HideExpired = true,
                    ShowOnlyActive = true
                };

                model.Banners = model.Banners.Where(x => !x.HasExpired()).Where(y => y.IsActive).ToList();

                return View(model);
            });
        }

        [HttpPost]
        public ActionResult Index(HomeBannerListViewModel model)
        {
            return TryOnEvent(() =>
            {
                model.Banners = model.SelectCompanyCode == 0 ? service.HomeBannerService.GetAll() : service.HomeBannerService.GetAllByCompanyCode(model.SelectCompanyCode);
                if ( model.ShowOnlyVisible )
                {
                    model.Banners = model.Banners.Where(b => b.IsCurrentlyVisible()).ToList();
                }

                if (model.HideExpired)
                {
                    model.Banners = model.Banners.Where(b => !b.HasExpired()).ToList();
                }

                if (model.ShowOnlyActive)
                {
                    model.Banners = model.Banners.Where(b => b.IsActive).ToList();
                }

                model.AvailableCompanies = Company.GetAll<Company>();

                return View(model);
            });
        }

        public ActionResult Create()
        {
            return TryOnEvent(() =>
            {

                var positions = new List<SelectListItem> { new SelectListItem { Text = "First", Value = "First" } };
                var activeBanners = service.HomeBannerService.GetAllActive();
                for (int i = 0; i < activeBanners.Count - 1; i++)
                {
                    var banner = activeBanners[i];
                    positions.Add(new SelectListItem { Text = "After: " + banner.Title, Value = banner.Id.ToString() });
                }
                positions.Add(new SelectListItem { Text = "Last", Value = "Last" });

                var model = new HomeBannerViewModel
                {
                    AvailableCompanies = service.HomeBannerService.GetAllBannerCompanies(),
                    PositionsList = positions
                };

                return View(model);
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(HomeBannerViewModel model)
        {
            return TryOnEvent(() =>
            {
                var banner = new HomeBannerItem();

                var savedFileName = SaveImageFile(model.ImageFile);
                if (!String.IsNullOrEmpty(savedFileName))
                {
                    model.ImageUrl = "http://media.jhm.org/Images/Web/HomeBanners/" + savedFileName;
                }

                model.AvailableCompanies = service.HomeBannerService.GetAllBannerCompanies();
                foreach (var company in model.AvailableCompanies)
                {
                    // if the company was selected
                    if (model.SelectedCompanies.Any(id => company.Id == id))
                    {
                        company.BannerItems.Add(banner);
                        banner.Companies.Add(company);
                        service.HomeBannerService.SaveChanges(company);
                    }
                }

                model.MapTo(banner);

                var allBanners = service.HomeBannerService.GetAll();
                if ( model.SelectedPosition == "First" )
                {
                    banner.Position = 1;
                    foreach (var item in allBanners)
                    {
                        item.Position++;
                    }
                }
                else if (model.SelectedPosition == "Last")
                {
                    banner.Position = allBanners.Count + 1;
                }
                else
                {
                    var previousBanner = allBanners.SingleOrDefault(x => x.Id.ToString() == model.SelectedPosition);
                    if ( previousBanner == null )
                    {
                        throw new Exception("Selected Banner for positioning not found");
                    }
                    banner.Position = previousBanner.Position + 1;
                    foreach (var item in allBanners.Where(x => x.Position > previousBanner.Position))
                    {
                        item.Position++;
                    }
                }

                service.HomeBannerService.SaveChanges(banner);

                return RedirectToAction("Index");
            });
        }

        public ActionResult MoveBannerUp(Guid id)
        {
            return TryOnEvent(() =>
            {
                var allBanners = service.HomeBannerService.GetAll();
                var banner = allBanners.SingleOrDefault(x => x.Id == id);
                if ( banner == null )
                {
                    throw new Exception("Banner not found");
                }

                if ( banner.Position > 1 )
                {
                    foreach (var item in allBanners.Where(x => x.Position == banner.Position-1))
                    {
                        item.Position++;
                    }
                    banner.Position--;
                }
                service.HomeBannerService.SaveChanges();

                return RedirectToAction("Index");
            });
        }

        public ActionResult MoveBannerDown(Guid id)
        {
            return TryOnEvent(() =>
            {
                var allBanners = service.HomeBannerService.GetAll();
                var banner = allBanners.SingleOrDefault(x => x.Id == id);
                if (banner == null)
                {
                    throw new Exception("Banner not found");
                }

                var maxPosition = allBanners.Max(x => x.Position);

                if (banner.Position < maxPosition)
                {
                    foreach (var item in allBanners.Where(x => x.Position == banner.Position + 1))
                    {
                        item.Position--;
                    }
                    banner.Position++;
                }
                service.HomeBannerService.SaveChanges();

                return RedirectToAction("Index");
            });
        }

        public ActionResult Edit(Guid id)
        {
            return TryOnEvent(() =>
            {
                var banner = service.HomeBannerService.GetById(id);
                if (banner == null)
                {
                    throw new Exception("Banner not found");
                }
                var model = HomeBannerViewModel.CreateFrom(banner);
                model.AvailableCompanies = service.HomeBannerService.GetAllBannerCompanies();
                return View(model);
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(HomeBannerViewModel model)
        {
            return TryOnEvent(() =>
            {
                var banner = service.HomeBannerService.GetById(model.Id);
                if (banner == null)
                {
                    throw new Exception("Banner not found");
                }
                var savedFileName = SaveImageFile(model.ImageFile);
                if ( !String.IsNullOrEmpty(savedFileName) )
                {
                    model.ImageUrl = "http://media.jhm.org/Images/Web/HomeBanners/"+savedFileName;
                }

                var oldCompanies = banner.Companies;
                model.AvailableCompanies = service.HomeBannerService.GetAllBannerCompanies();
                //banner.Companies = model.AvailableCompanies.Where(x => model.SelectedCompanies.Contains(x.Id));
                foreach (var company in model.AvailableCompanies)
                {
                    // if the company was selected
                    if (model.SelectedCompanies.Any(id => company.Id == id))
                    {
                        // if the selected company is new to this banner
                        if (!oldCompanies.Any(x => x.Id == company.Id))
                        {
                            company.BannerItems.Add(banner);
                            banner.Companies.Add(company);
                            service.HomeBannerService.SaveChanges(company);
                        }
                    }
                    else
                    {
                        // if the unchecked company was part of this banner
                        if ( oldCompanies.Any(x => x.Id == company.Id) )
                        {
                            company.BannerItems.Remove(banner);
                            banner.Companies.Remove(company);
                            service.HomeBannerService.SaveChanges(company);
                        }
                    }

                    
                }
                model.MapTo(banner);
                
                service.HomeBannerService.SaveChanges(banner);

                
                return View(model);
            });
        }

        private string SaveImageFile(HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file == null || file.ContentLength <= 0) return null;

            var fileName = Path.GetFileName(file.FileName) ?? Guid.NewGuid().ToString("N")+".jpg";
            
            var directory = GetPath(ConfigurationManager.AppSettings["MediaImagesRoot"]) + "Web\\HomeBanners\\";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var path = Path.Combine(directory, fileName);
            file.SaveAs(path);

            //ResizeImage(path, destinationFolder + videoId + "_small.jpg", 250, 140, false);
            //ResizeImage(path, destinationFolder + videoId + "_large.jpg", 840, 472, false);

            //System.IO.File.Delete(path);
            return fileName;
        }

        private string GetPath(string path)
        {
            return Path.IsPathRooted(path) ? path : Server.MapPath(path);
        }
    }
}
