﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Jhm.CommerceIntegration;
using Jhm.Common;
using Jhm.Common.UOW;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Service;
using Jhm.Web.UI.Controllers;
using Jhm.Web.UI.IBootStrapperTasks;
using getv.donorstudio.core.Global;
using Jhm.Web.UI.MVCCommon;

namespace Jhm.Web.UI.Areas.Admin.Controllers
{
    [HandleError]
    //[Authorize(Roles = "SuperAdmin,Admin")]
    public class RestfulActionsController : BaseController<IServiceCollection>
    {

        private readonly StringIgnoreCaseComparer<String> _ignoreCaseComparer = new StringIgnoreCaseComparer<string>();


        public RestfulActionsController(IServiceCollection service, IUnitOfWorkContainer unitOfWork)
            : base(service, unitOfWork)
        {
        }

        public ActionResult ClearThemeCache()
        {
            ConfigurationManager.RefreshSection("appSettings");
            ApplicationManager.ApplicationData.CurrentTheme = null;
            return Content("Themes Cleared and Reloaded");
        }

        public ActionResult SetThemeCache(string theme)
        {
            ApplicationManager.ApplicationData.SetTheme(true);
            return Content("Themes Cleared and Reloaded");
        }

        /**
         * Updates the file size of each mp3 in donor studio.
         * 
         * This function is called every time the JHM products are
         * updated, to ensure that all the entries of mp3s in Donor
         * Studio contain a file size, which is stored in the product's
         * weight field.
         * 
         * @param Company[] $companyList
         *   companyList is an array that contains the Companies we currently
         *   support.  US, UK, and CA.  The array will ensure the ability to
         *   process future companies as well.
         */
        private void UpdateMp3FileSize(Company[] companyList)
        {
            int results = 0;

            /**
             * The first for loop, loops through each company to ensure 
             * that all companies and all future companies will be processed
             */
            for (int index = 0; index < companyList.Length; index++)
            {
                var mp3Products = service.CatalogService.GetProductsByMediaTypes(companyList[index].DonorStudioEnvironment, "AF", new PagingInfo(), ref results);

                /**
                 * The second loop loops through a list of mp3s that
                 * are returned from the query to single out each mp3.
                 */
                foreach (var mp3Product in mp3Products)
                {
                    /**
                     * This is to ensure that if any product is missing it's code, to just skip it.
                     */
                    if (!String.IsNullOrEmpty(mp3Product.Code))
                    {
                        /**
                         * The object that is returned from the 
                         * GetProductsByMediaTypes returns any product 
                         * grouping that may have mp3s as apart of the product.
                         * This loop loops through the stock items and checks
                         * specifically for 
                         */
                        foreach (var item in mp3Product.StockItems)
                        {
                            if (item.MediaType.CodeSuffix == "AF")
                            {
                                if (String.IsNullOrWhiteSpace(item.MediaPath))
                                {
                                    Log.For(this).Erroneous("Product: {0}{1} does not have a MediaPath and is invalid!", item.ProductCode, item.MediaType.CodeSuffix);
                                    break;
                                }
                                var path = item.MediaPath;
                                path = path.Replace(@"W:\DOWNLOADS\MP3\", @"\\10.15.10.7\W$\DOWNLOADS\MP3\");
                                try
                                {
                                    var file = new System.IO.FileInfo(path);
                                    service.CatalogService.UpdateProductWeight(
                                        companyList[index].DonorStudioEnvironment, item.Code, file.Length);
                                }
                                catch (Exception ex)
                                {
                                    Log.For(this).Erroneous("File cannot be found: {0} {1} {2}", path, ex.Message, ex.StackTrace);
                                }
                            }
                        }
                    }
                }
            }
        }

		/**
  * Updates the file size of each Digital File for  -- PDF -- in donor studio.
  * 
  * This function is called every time the JHM products are
  * updated, to ensure that all the entries of Digital Files in Donor
  * Studio contain a file size, which is stored in the product's
  * weight field.
  * 
  * @param Company[] $companyList
  *   companyList is an array that contains the Companies we currently
  *   support.  US, UK, and CA.  The array will ensure the ability to
  *   process future companies as well.
  */
		private void UpdateDFFileSize(Company[] companyList)
		{
			int results = 0;

			/**
			 * The first for loop, loops through each company to ensure 
			 * that all companies and all future companies will be processed for "DF"
			 */
			for (int index = 0; index < companyList.Length; index++)
			{
				var dfProducts = service.CatalogService.GetProductsByMediaTypes(companyList[index].DonorStudioEnvironment, "DF", new PagingInfo(), ref results);

				/**
				 * The second loop loops through a list of DFs that
				 * are returned from the query to single out each DF.
				 */
				foreach (var dfProduct in dfProducts)
				{
					/**
					 * This is to ensure that if any product is missing it's code, to just skip it.
					 */
					if (!String.IsNullOrEmpty(dfProduct.Code))
					{
						/**
						 * The object that is returned from the 
						 * GetProductsByMediaTypes returns any product 
						 * grouping that may have DF as a type of the product.
						 * This loop loops through the stock items and checks
						 * specifically for DF digital Files, PDF's
						 */
						foreach (var item in dfProduct.StockItems)
						{
							if (item.MediaType.CodeSuffix == "DF")
							{
								var path = item.MediaPath;
								path = path.Replace(@"W:\DOWNLOADS\MP3\", @"\\10.15.10.7\W$\DOWNLOADS\MP3\");
								try
								{
									var file = new System.IO.FileInfo(path);
									service.CatalogService.UpdateProductWeight(
										companyList[index].DonorStudioEnvironment, item.Code, file.Length);
								}
								catch (Exception ex)
								{
									Log.For(this).Erroneous("File cannot be found: {0} {1} {2}", path, ex.Message, ex.StackTrace);
								}
							}
						}
					}
				}
			}
		}






        public ActionResult ReloadProductsFromAllEnvironments()
        {
            var companyList = new Company[3];

            companyList[0] = Company.JHM_UnitedStates;
            companyList[1] = Company.JHM_UnitedKingdom;
            companyList[2] = Company.JHM_Canada;

            try
            {
                UpdateMp3FileSize(companyList);
	            UpdateDFFileSize(companyList);

                var usProducts       = service.CatalogService.GetAllProducts(Company.JHM_UnitedStates.DonorStudioEnvironment);
                var ukProducts       = service.CatalogService.GetAllProducts(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
                var canadianProducts = service.CatalogService.GetAllProducts(Company.JHM_Canada.DonorStudioEnvironment);

                usProducts.SaveToCache(Company.JHM_UnitedStates.DonorStudioEnvironment);
                ukProducts.SaveToCache(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
                canadianProducts.SaveToCache(Company.JHM_Canada.DonorStudioEnvironment);

                //System.Web.HttpContext.Current.Application[Company.JHM_UnitedStates.DonorStudioEnvironment]  = service.CatalogService.GetAllProducts(Company.JHM_UnitedStates.DonorStudioEnvironment);
                //System.Web.HttpContext.Current.Application[Company.JHM_UnitedKingdom.DonorStudioEnvironment] = service.CatalogService.GetAllProducts(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
                //System.Web.HttpContext.Current.Application[Company.JHM_Canada.DonorStudioEnvironment]        = service.CatalogService.GetAllProducts(Company.JHM_Canada.DonorStudioEnvironment);
            }
            catch (Exception ex)
            {
               return Content("Error reloading products for all environments: " + ex.Message + ex.StackTrace);
            }


            return Content("All products reloaded for all environments.");
        }

        public ActionResult ReloadSubscriptionsFromAllEnvironments()
        {
            try
            {
                var usSubscriptions       = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_UnitedStates.DonorStudioEnvironment);
                var ukSubscriptions       = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
                var canadianSubscriptions = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_Canada.DonorStudioEnvironment);

                usSubscriptions.SaveToCache(Company.JHM_UnitedStates.DonorStudioEnvironment  + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);
                ukSubscriptions.SaveToCache(Company.JHM_UnitedKingdom.DonorStudioEnvironment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);
                canadianSubscriptions.SaveToCache(Company.JHM_Canada.DonorStudioEnvironment  + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix);

                //System.Web.HttpContext.Current.Application[Company.JHM_UnitedStates.DonorStudioEnvironment  + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix] = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_UnitedStates.DonorStudioEnvironment);
                //System.Web.HttpContext.Current.Application[Company.JHM_UnitedKingdom.DonorStudioEnvironment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix] = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_UnitedKingdom.DonorStudioEnvironment);
                //System.Web.HttpContext.Current.Application[Company.JHM_Canada.DonorStudioEnvironment        + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix] = service.SubscriptionService.UpdateSubscriptionsJson(Company.JHM_Canada.DonorStudioEnvironment);

            }
            catch (Exception ex)
            {
                return Content("Error reloading subscriptions for all environments: " + ex.Message + ex.StackTrace);
            }


            return Content("All subscriptions reloaded for all environments.");
        }

        public ActionResult GetCreditCardTestToken()
        {
            try
            {
                CreditCardViewModel model = new CreditCardViewModel();
                model.Amount              = 1.00M;
                model.Balance             = 1.00M;
                model.BillingAddressId    = 0;
                model.CreditCardNumber    = "5454545454545454";
                model.CreditCardType      = "Mastercard";
                model.CCExpirationMonth   = "12";
                model.CCExpirationYear    = "2020";
                model.CID                 = "123";

                String response;

                var selectedAddress   = service.AccountService.GetUserAddressById(20680430, Company.JHM_UnitedStates.DonorStudioEnvironment);
                model.SelectedAddress = selectedAddress;
                var isSuccessfull     = GetCreditCardTestToken(model, SessionManager.CurrentUser, Company.JHM_UnitedStates, out response);

                return Content(String.Format("Success? :{0} , response: {1}", isSuccessfull, response));
            }
            catch (Exception ex)
            {
                return Content("Error running test for Bluefin: " + ex.Message + ex.StackTrace);
            }

        }

        private bool GetCreditCardTestToken(CreditCardViewModel creditCard, User user, Company company, out string result)
        {
            //TODO:  If IsTestCharge then supersede the Account and Transaction key with test info.


            var request = new PaymentRequest(
                company.CommerceAccountId,
                company.TransactionKey,
                false,
                user.Profile.FirstName,
                user.Profile.LastName,
                creditCard.SelectedAddress.Address1 + (String.IsNullOrEmpty(creditCard.SelectedAddress.Address2) ? String.Empty : " " + creditCard.SelectedAddress.Address2),
                creditCard.SelectedAddress.City,
                creditCard.SelectedAddress.StateCode,
                creditCard.SelectedAddress.PostalCode,
                creditCard.SelectedAddress.Country,
                "Zero dollars auth",
                creditCard.CreditCardNumber,
                String.Format("{0}{1}", creditCard.CCExpirationMonth, creditCard.TwoDigitsExpirationYear),
                creditCard.CID,
                // Doing 1.0 Pound Auth for UK company
                company == Company.JHM_UnitedKingdom ? 1 : 0,
                IsDM ? "DM" : company.SiteTag,
                company.Currency,
                BlueFinPaymentService.BluefinTransactionType.Auth
                );
            var response = service.CommerceService.BlueFinPaymentService.Process(request);

            result = response.IsAuthorized ? response.TransactionID : String.IsNullOrEmpty(response.AuthMessage) ? "Unknown reason" : response.AuthMessage.Replace("+", " ");

            return response.IsAuthorized;
        }
   }
}
