﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Send Prophetic Blessing Email
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
                Send Prophetic Blessing Treasure Chest Email</h2>
        <div class="content">
            <% using (Html.BeginForm())
               {%>
                    <br/>
                    <br/>
                    User's Email Address: <%: Html.TextBox("Email",null,new{style="width:250px"}) %>
                    <br/>
                    <br/>
                    <input type="submit" value="Send Email"/>
            <% } %>
        </div>
    </div>
</asp:Content>