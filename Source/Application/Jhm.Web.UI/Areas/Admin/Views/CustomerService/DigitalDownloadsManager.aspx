﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.Core.Models.DigitalDownloadManagerViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>
<%@ Import Namespace="Jhm.Common.Framework.Extensions" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.UI.Admin.Models" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Banner Management
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Content/css/jquery-ui.css") %>" />
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/jquery-ui.min.js") %>"></script>
    <script type="text/javascript" src="/Content/js/jquery.datepick.min.js"></script>
    <link href="/Content/css/humanity.datepick.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
    <h2 class="title">Digital Downloads Manager</h2>
    <div class="content">
        <% using (Html.BeginForm()){ %>
            <h4>Search By</h4>
            Username: <%= Html.TextBoxFor(m => m.Username)%> First Name: <%= Html.TextBoxFor(m => m.FirstName)%> Last Name: <%= Html.TextBoxFor(m => m.LastName)%> <input type="submit" value="Search"/>
            <br />
            Download Status: <%= Html.DropDownListFor(m => m.SelectedStatus, new SelectList(Model.DownloadStatuses), "All")%>
            <%--<%: Html.CheckBoxFor(m => m.ShowOnlyVisible) %><%: Html.LabelFor(m => m.ShowOnlyVisible) %>--%>
            
        <% } %>
            <p>Records found: <%= Model.Results.Count() %></p>
        <table style="border-collapse:collapse" cellpadding="4" border="1">
            <thead>
                <tr>
                    <th style="width:220px">User (username)</th>
                    <th style="width:120px">Order Date</th>
                    <th style="width:120px">Expiration Date</th>
                    <th>Product</th>
                    <th style="width:60px">Remaining Download Attempts</th>
                    <th style="width:70px">Is Active?</th>
                    <th style="width:40px; text-align:center">Edit</th>
                </tr>
            </thead>
            <tbody>
                <% foreach (var dd in Model.Results)
                   { %>
                    <tr>
                        <td>
                            <%= String.Format("{0} {1} ({2})",dd.User.Profile.FirstName,dd.User.Profile.LastName,dd.User.Username) %>
                        </td>
                        <td>
                            <%= dd.CreatedDate.ToShortDateString() %>
                        </td>
                        <td>
                            <%= dd.ExpirationDate.ToShortDateString() %>
                        </td>
                        <td><%= dd.ProductCode %></td>
                        <td><%= dd.DownloadAttemptsRemaining %></td>
                        <td align="center">
                            <%= dd.IsActive() ? "Yes" : "No" %>
                        </td>
                        <td align="center">
                            <a href="javascript:void(0);" onclick='ShowDDEditor(<%= dd.ToJSON() %>)' title="Edit Download">
                                <img src="<%= Url.Content("~/Content/Images/Icons/cog_edit.png") %>"/>
                            </a>
                        </td>
                    </tr>
                <% } %>
            </tbody>
        </table>
    </div>
    </div>
    <%: Html.Partial("_DigitalDonwloadEditor",new DigitalDownloadViewModel()) %>
</asp:Content>

