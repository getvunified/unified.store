﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Jhm.Web.UI.Admin.Models.DigitalDownloadViewModel>" %>
<script type="text/javascript">
    jQuery(function () {
        jQuery('#ddEditorDialog').dialog({
            autoOpen: false,
            bgiframe: true,
            modal: true,
            resizable: false,
            width: 500,
            buttons: {
                'Save Changes': function () {
                    jQuery('#ddEditorForm').trigger('submit');
                },
                Cancel: function () {
                    jQuery(this).dialog('close');
                }
            }
        });
        jQuery('.date').datepick({ dateFormat: "mm/dd/yyyy", selectDefaultDate: true });
    });
    
    function ShowDDEditor(jsonDD) {
        for (var key in jsonDD) {
            setFieldValue(key, jsonDD[key]);
        }
        setFieldValue('<%: Html.NameFor(m => m.DownloadAttemptsRemaining) %>', jsonDD['<%: Html.NameFor(m => m.DownloadAttemptsRemaining) %>']);
        setFieldValue('<%: Html.NameFor(m => m.ExpirationDate) %>', jsonDD['<%: Html.NameFor(m => m.ExpirationDateFormatted) %>']);
        jQuery('#ddEditorDialog').dialog('open');
    }

    function setFieldValue(fieldId, value) {
        var field = document.getElementById(fieldId);
        if (field != null) {
            if (field.type == "checkbox") {
                field.checked = value;
            }
            else {
                field.value = value;
            }
        }
    }
    
    function updateDownloadAttemptsRemaining(increment) {
        var newValue = parseInt(jQuery('#DownloadAttemptsRemaining').val()) + increment;
        if ( newValue >= 0 ) jQuery('#DownloadAttemptsRemaining').val(newValue);
    }
</script>
<div id="ddEditorDialog" title="Edit Digital Download">
    <% using (Html.BeginForm("EditDigitalDownload", "CustomerService", FormMethod.Post, new{id="ddEditorForm"}))
       { %>
        <%: Html.HiddenFor(m => m.Id) %>
        <div>
            Attempts Remaining: <%: Html.TextBoxFor(m => m.DownloadAttemptsRemaining, new { size = 4 }) %> 
            <a href="javascript:updateDownloadAttemptsRemaining(1);" class="ui-state-default ui-corner-all">
                <img src="<%= Url.Content("~/Content/Images/Icons/add.png") %>"/> More
            </a>&nbsp;
            <a href="javascript:updateDownloadAttemptsRemaining(-1);" class="ui-state-default ui-corner-all">
                <img src="<%= Url.Content("~/Content/Images/Icons/delete.png") %>"/> Less
            </a>
        </div>
        <br />
        <div>
            Expiration Date: <%: Html.TextBoxFor(m => m.ExpirationDate, new { size = 14, @class="date" }) %> 11:59 PM
        </div>
        <br />
        <br />
        <br />
        <br />
    <% } %>
</div>