﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Areas.Admin.Models.DiscountCodeParentChildReturnModel>" %>
<%@ Import namespace="Jhm.Web.Core.Models" %>
<%@ Import Namespace="Jhm.Web.Core.Models.Modules.ECommerce" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% var t = new List<SingleUseDiscountCodes>();%>
<h2>Index</h2>
    
        <div id="tree" style="float: left;">
            
            <%--<select id="DiscountCodes" size="20"></select>
            <select id="SingleUseCodes" size="20"></select>--%>
        </div>
        <div style="float: right;">
            <table style="background-color: gainsboro">
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Start Date</td>
                    <td><input size="40" type="text" id="startDate" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>End Date</td>
                    <td><input size="40" type="text" id="endDate" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Description</td>
                    <td><textarea size="40" draggable="0" cols="37" id="longDescription" ></textarea></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Is This A Fix Amount</td>
                    <td><input size="40" type="text" id="isFixedAmount" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>How much off</td>
                    <td><input size="40" type="text" id="amountOff" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Does This Offer Free Shipping</td>
                    <td><input size="40" type="text" id="freeShipping" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>What is the Minimum Amount Needed</td>
                    <td><input size="40" type="text" id="minAmountOff" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>What is the Maximum Discount</td>
                    <td><input size="40" type="text" id="maxAmountOff" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Does This Auto Apply</td>
                    <td><input size="40" type="text" id="isAutoApply" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Is a Single Use Prefix</td>
                    <td><input size="40" type="text" id="isSingleUsePrefix" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Is This Tied to a Product</td>
                    <td><input size="40" type="text" id="isTiedToProduct" /></td>
                </tr>
                <tr style="border: thin; border-style: solid; border-color:black;">
                    <td>Who Are the Applicable Companies</td>
                    <td><input size="40" type="text" id="applicableCompanies" /></td>
                </tr>
            </table>
        </div>
        <div style="background-color: #ffffff; width: 450px; float: right;">
            <span id="fib"></span>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="<%: Url.Content("~/Content/js/tree.jquery.js") %>"></script>

    <link type="text/css" rel="stylesheet" href="<%: Url.Content("~/Content/css/jqtree.css") %>" />

    <script type="text/javascript">
        var discountCodes;
        var discountCodeList = new Array();

        function populateDiscountCodeInfo(dcId) {
            
        }
        
        function populateSinglueUseDiscountCodes(discountCodeId, position) {
            jQuery("#SingleUseCodes").empty();
            
            var discountCode = discountCodeList[position];

            if (discountCode.SingleUseDiscountCodes.length > 0) {
                jQuery("#SingleUseCodes").show();
                
                for (var i = 0; i < discountCode.SingleUseDiscountCodes.length; ++i)
                {
                    var id = discountCode.SingleUseDiscountCodes[i].Id;
                    var codeName = discountCode.SingleUseDiscountCodes[i].DiscountCodeSuffix;

                    jQuery('#SingleUseCodes').append(jQuery("<li></li>")
                        .attr("value", i)
                        .attr("id", id)
                        .append(jQuery("<span class='folder'></span>"))
                        .text(codeName));
                }
            } else {
                jQuery("#SingleUseCodes").hide();
            }
        }
        
        function populateDiscountCodes() {

            var id = "";
            var codeName = "";
            
            for (var i = 0; i < discountCodes.length; ++i) {
                id = discountCodes[i].Id;
                codeName = discountCodes[i].CodeName;
                
                jQuery('#browser')
                    .append(jQuery("<li></li>")
                        .attr("value", i)
                        .attr("id", id)
                        .append(jQuery("<span class='folder'></span>"))
                        .text(codeName));

                jQuery('#' + id).click(function () {
                    
                    populateSinglueUseDiscountCodes(id, jQuery(this).val());

                    var startDate = new Date(parseInt(discountCodes[jQuery(this).val()].StartDate.substring(6)));
                    var startDateOutput = startDate.getMonth() + "/" + startDate.getDay() + "/" + startDate.getFullYear();

                    var endDate = new Date(parseInt(discountCodes[jQuery(this).val()].EndDate.substring(6)));
                    var endDateOutput = endDate.getMonth() + "/" + endDate.getDay() + "/" + endDate.getFullYear();

                    jQuery("#startDate").val(startDateOutput);
                    jQuery("#endDate").val(endDateOutput);
                    jQuery("#isAutoApply").val(discountCodes[jQuery(this).val()].IsAutoApply);
                    jQuery("#isFixedAmount").val(discountCodes[jQuery(this).val()].IsFixedAmount);
                    jQuery("#freeShipping").val(discountCodes[jQuery(this).val()].FreeShipping);
                    jQuery("#isSingleUsePrefix").val(discountCodes[jQuery(this).val()].IsSingleUsePrefix);
                    jQuery("#isTiedToProduct").val(discountCodes[jQuery(this).val()].IsTiedToProducts);
                    jQuery("#longDescription").val(discountCodes[jQuery(this).val()].LongDescription);
                    jQuery("#minAmountOff").val(discountCodes[jQuery(this).val()].MinPurchaseAmount);
                    jQuery("#maxAmountOff").val(discountCodes[jQuery(this).val()].MaxAmountOff);
                    jQuery("#amountOff").val(discountCodes[jQuery(this).val()].AmountOff);
                    jQuery("#applicableCompanies").val(discountCodes[jQuery(this).val()].ApplicableCompanies);
                });
            }
        }

        jQuery(document).ready(function () {
            var temp = <%= Model.JsonForTreeView.Replace("\\\"", "\"").Replace("\"[", "[").Replace("]\"", "]") %>;
            jQuery("#SingleUseCodes").hide();
            
            jQuery("#tree").tree({
                data: temp,
                onCreateLi: function(temp, list) {
                    
                }
            });
            
            jQuery.ajax({
                url: '<%: Url.Action("ReturnAllDiscountCodes", "DiscountCodeManagement")%>',
                type: 'POST',
                success: function(data) {
                    discountCodes = data;
                    /*for (var i = 0; i < discountCodes.length; ++i) {
                        discountCodeList[i] = discountCodes[i];
                    }*/

                    //populateDiscountCodes();                    
                }
            });
            

        });
    </script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SidebarFirstContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SidebarSecondContent" runat="server">
</asp:Content>
