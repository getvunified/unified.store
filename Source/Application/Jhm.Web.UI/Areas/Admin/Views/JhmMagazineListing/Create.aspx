﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Admin.Models.JhmMagazineItemViewModel>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Create
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="/Content/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Content/js/jquery.datepick.min.js"></script>
    <link href="/Content/css/humanity.datepick.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.date').datepick({ dateFormat: "mm/dd/yyyy", selectDefaultDate: true });

            jQuery('img.thumbnail').error(function () {
                this.style.width = "16px";
                this.style.height = "16px";
                this.src = '/Content/images/error_16.png';
            });
        });

        function refreshImage(fileInput, imgElementId) {
            var filename = fileInput.value.replace(/^.*[\\\/]/, '');
            var imgElement = document.getElementById(imgElementId);
            var oldImagePath = imgElement.src;
            jQuery('#' + imgElementId).load(function () {
                doExistingImageCheck(this, oldImagePath, fileInput);
            });
            imgElement.style.width = "180px";
            imgElement.style.height = "auto";
            imgElement.src = "http://media.jhm.org/Images/Web/JhmMagazines/" + filename;
        }

        function doExistingImageCheck(imgElement, oldImagePath, fileInput) {
            jQuery(imgElement).unbind("load");

            var filename = imgElement.src.replace(/^.*[\\\/]/, '');
            if (filename == "error_16.png") {
                return;
            }
            var res = confirm('There is already an image with that same name in the server, do you want to replace that image?\n\nNote: If you don\'t want to replace the image cancel this prompt and try renaming your image');
            if (!res) {
                fileInput.value = null;
                filename = oldImagePath.replace(/^.*[\\\/]/, '');
                if (filename == "error_16.png") {
                    oldImagePath = "/fakepath.png";
                }
                imgElement.src = oldImagePath;
            }
        }
    </script>
    <style type="text/css">
        .formTable tr {
            height: 40px;
        }
        .formTable td {
            vertical-align: top;
        }
        .formTable tr > td:first-child{
            text-align: right;
            padding-right: 10px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title"><%: Html.ActionLink("JHM Magazine Listing Management","Index") %> >> Create Issue</h2>
        <div class="content">
            <% using (Html.BeginForm("Create","JhmMagazineListing", FormMethod.Post, new { enctype = "multipart/form-data" }))
               { %>
                <table class="formTable">
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.Title) %>:
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.Title, new{ size = 60 }) %>
                            <span style="color: #900;">*
                                <%: Html.ValidationMessageFor(m => m.Title)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.SubTitle) %>:
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.SubTitle, new { size = 40 })%>
                            <span style="color: #900;">*
                                <%: Html.ValidationMessageFor(m => m.SubTitle)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.ImagePath) %>:
                        </td>
                        <td>
                            <%: Html.HiddenFor(m => m.ImagePath) %>
                            <div style="width: auto; float: left; margin-right: 10px;">
                                <img class="thumbnail" id="imgImageUrl" src="<%= Model.ImagePath %>" width="180"/>
                            </div>
                            <div style="width: 300px; float: left;">
                                <input type="file" name="ImageFile" id="ImageFile" onchange="refreshImage(this,'imgImageUrl','tdHDFilename')" />
                            </div>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.Description) %>:
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.Description, new { size = 80 })%>
                            <span style="color: #900;">
                                <%: Html.ValidationMessageFor(m => m.Description)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.MagazineContentId) %>:
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.MagazineContentId, new { size = 80 })%>
                            <span style="color: #900;">
                                <%: Html.ValidationMessageFor(m => m.MagazineContentId)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.IsActive) %>:
                        </td>
                        <td>
                            <%: Html.CheckBoxFor(m => m.IsActive)%>
                            <span style="color: #900;">
                                <%: Html.ValidationMessageFor(m => m.IsActive)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.IsMagazine) %>:
                        </td>
                        <td>
                            <%: Html.CheckBoxFor(m => m.IsMagazine, true)%>
                            <span style="color: #900;">
                                <%: Html.ValidationMessageFor(m => m.IsMagazine)%>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.LabelFor(m => m.Rank) %>:
                        </td>
                        <td>
                            <%: Html.TextBoxFor(m => m.Rank)%>
                            <span style="color: #900;">
                                <%: Html.ValidationMessageFor(m => m.Rank)%>
                            </span>
                        </td>
                    </tr>
                </table>
                <br/>
                <input type="submit" value="Submit"/>
            <% } %>
        </div>
    </div>
</asp:Content>