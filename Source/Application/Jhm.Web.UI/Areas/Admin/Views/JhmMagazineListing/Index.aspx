﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Admin.Models.JhmMagazineListingViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>


<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    JHM Magazine Listing Management
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
    <h2 class="title">JHM Magazine Issues List</h2>
    <div class="content">
        <% using (Html.BeginForm("Index")){ %>
            <%: Html.CheckBoxFor(m => m.ShowOnlyActive) %><%: Html.LabelFor(m => m.ShowOnlyActive) %>
            <input type="submit" value="Apply"/>
        <% } %>
        <table style="border-collapse:collapse" cellpadding="4" border="1">
            <thead>
                <tr>
                    <th style="width:70px">Date Created</th>
                    <th style="width:120px">Image</th>
                    <th>Title</th>
                    <th style="width:24px">Active?</th>
                    <th style="width:40px; text-align:center">Edit</th>
                    <th style="width:40px; text-align:center">Delete</th>
                </tr>
            </thead>
            <tbody>
                <% foreach (var issue in Model.Issues.OrderByDescending(x => x.Rank))
                   { %>
                    <tr>
                        <td>
                            <%= issue.DateCreated.ToShortDateString() %>
                        </td>
                        <td align="center">
                            <img src="<%= issue.ImagePath %>" width="90"/>
                        </td>
                        <td><%= issue.Title %></td>
                        <td align="center">
                            <img src="<%= Url.Content(issue.IsActive? "~/Content/Images/Icons/ok.gif": "~/Content/Images/Icons/error_16.png") %>"/>
                        </td>
                        <td align="center">
                            <a href="<%= Url.Action("Edit",new{id=issue.Id}) %>" title="Edit Issue">
                                <img src="<%= Url.Content("~/Content/Images/Icons/cog_edit.png") %>"/>
                            </a>
                        </td>
                        <td align="center">
                            <a href="<%= Url.Action("Delete",new{id=issue.Id}) %>" title="Delete Issue" onclick="return confirm('Are you sure you want to delete this issue?');">
                                <img src="<%= Url.Content("~/Content/Images/Icons/error_16.png") %>"/>
                            </a>
                        </td>
                    </tr>
                <% } %>
            </tbody>
        </table>
        <br/>
        <%: Html.ActionLink("Add New Issue","Create") %>
    </div>
    </div>
</asp:Content>
