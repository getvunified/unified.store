﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Admin.Models.HomeBannerViewModel>"
    ValidateRequest="false" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Edit Banner:
    <%= Model.Title %>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script src="/Content/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Content/js/jquery.datepick.min.js"></script>
    <link href="/Content/css/humanity.datepick.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.date').datepick({ dateFormat: "mm/dd/yyyy", selectDefaultDate: true });

            jQuery('img.thumbnail').error(function () {
                this.style.width = "16px";
                this.style.height = "16px";
                this.src = '/Content/images/error_16.png';
            });
        });

        function refreshImage(fileInput, imgElementId) {
            var filename = fileInput.value.replace(/^.*[\\\/]/, '');
            var imgElement = document.getElementById(imgElementId);
            var oldImagePath = imgElement.src;
            jQuery('#' + imgElementId).load(function () {
                doExistingImageCheck(this, oldImagePath, fileInput);
            });
            imgElement.style.width = "180px";
            imgElement.style.height = "auto";
            imgElement.src = "http://media.jhm.org/Images/Web/HomeBanners/" + filename;
        }

        function doExistingImageCheck(imgElement, oldImagePath, fileInput) {
            jQuery(imgElement).unbind("load");

            var filename = imgElement.src.replace(/^.*[\\\/]/, '');
            if (filename == "error_16.png") {
                return;
            }
            var res = confirm('There is already an image with that same name in the server, do you want to replace that image?\n\nNote: If you don\'t want to replace the image cancel this prompt and try renaming your image');
            if (!res) {
                fileInput.value = null;
                filename = oldImagePath.replace(/^.*[\\\/]/, '');
                if (filename == "error_16.png") {
                    oldImagePath = "/fakepath.png";
                }
                imgElement.src = oldImagePath;
            }
        }
    </script>
    <style type="text/css">
        .formTable tr
        {
            height: 40px;
        }
        .formTable td
        {
            vertical-align: top;
        }
        .formTable tr > td:first-child
        {
            text-align: right;
            padding-right: 10px;
            font-weight: bold;
            padding-top: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
        <h2 class="title">
            <%: Html.ActionLink("Home Banner Management","Index") %>
            > Edit Banner</h2>
        <div class="content">
            <% using (Html.BeginForm("Edit", "HomeBanner", FormMethod.Post, new { enctype = "multipart/form-data" }))
               { %>
            <%: Html.HiddenFor(m => m.Id) %>
            <%: Html.HiddenFor(m => m.Position) %>
            <table class="formTable">
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.Title) %>:
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.Title, new{ size = 40 }) %>
                        <span style="color: #900;">*
                            <%: Html.ValidationMessageFor(m => m.Title)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.SubTitle) %>:
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.SubTitle, new { size = 60 })%>
                        <span style="color: #900;">*
                            <%: Html.ValidationMessageFor(m => m.SubTitle)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.AvailableCompanies) %>:
                    </td>
                    <td>
                        <% foreach (var company in Model.AvailableCompanies)
                           {  %>
                        <input type="checkbox" name="SelectedCompanies" value="<%=company.Id%>" id="group<%=company.CompanyCode%>"
                            <%= Model.SelectedCompanies.Contains(company.Id) ? "checked=\"checked\"" : String.Empty %> />
                        <label for="group<%=company.CompanyCode%>">
                            <%=company.Company.DisplayName%></label>
                        <% } %>
                        <span style="color: #900;">
                            <%: Html.ValidationMessageFor(m => m.AvailableCompanies)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.ImageUrl) %>:
                    </td>
                    <td>
                        <%: Html.HiddenFor(m => m.ImageUrl) %>
                        <div style="width: auto; float: left; margin-right: 10px;">
                            <img class="thumbnail" id="imgImageUrl" src="<%= Model.ImageUrl %>" width="180" />
                        </div>
                        <div style="width: 300px; float: left;">
                            <input type="file" name="ImageFile" id="ImageFile" onchange="refreshImage(this,'imgImageUrl','tdHDFilename')" />
                        </div>
                        <span style="color: #900;">*
                            <%: Html.ValidationMessageFor(m => m.ImageUrl)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.ImageLinkUrl) %>:
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.ImageLinkUrl, new { size = 80 })%>
                        <span style="color: #900;">
                            <%: Html.ValidationMessageFor(m => m.ImageLinkUrl)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.OpenTargetInNewWindow) %>:
                    </td>
                    <td>
                        <%: Html.CheckBoxFor(m => m.OpenTargetInNewWindow)%>
                        <span style="color: #900;">
                            <%: Html.ValidationMessageFor(m => m.OpenTargetInNewWindow)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.CustomAttributes) %>:
                    </td>
                    <td>
                        <%: Html.TextBoxFor(m => m.CustomAttributes, new { size = 40 })%>
                        <span style="color: #900;">
                            <%: Html.ValidationMessageFor(m => m.CustomAttributes)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.ExternalHtml) %>:
                    </td>
                    <td>
                        <%: Html.TextAreaFor(m => m.ExternalHtml, new { rows = 6, style = "width:562px" })%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%: Html.LabelFor(m => m.IsActive) %>:
                    </td>
                    <td>
                        <%: Html.CheckBoxFor(m => m.IsActive)%>
                        <span style="color: #900;">*
                            <%: Html.ValidationMessageFor(m => m.IsActive)%>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        Available Date Range:
                    </td>
                    <td>
                        <%: Html.LabelFor(m => m.StartDate) %>:
                        <%: Html.TextBoxFor(m => m.StartDate, new { size = 14, @class="date" })%>
                        12:00 AM &nbsp;&nbsp;&nbsp;&nbsp;
                        <%: Html.LabelFor(m => m.EndDate) %>:
                        <%: Html.TextBoxFor(m => m.EndDate, new { size = 20, @class = "date" })%>
                        11:59 PM
                    </td>
                </tr>
            </table>
            <br />
            <input type="submit" value="Save Changes" />
            <% } %>
        </div>
    </div>
</asp:Content>
