﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<Jhm.Web.UI.Admin.Models.HomeBannerListViewModel>"
    MasterPageFile="~/Views/Shared/Site.Master" %>


<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Banner Management
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="block">
    <h2 class="title">Home Banner List</h2>
    <div class="content">
        <% using (Html.BeginForm("Index")){ %>
            Filter By Company: <%= Html.DropDownListFor(m => m.SelectCompanyCode, new SelectList(Model.AvailableCompanies, "Value", "DisplayName"), "All")%>
            <%: Html.CheckBoxFor(m => m.ShowOnlyVisible) %><%: Html.LabelFor(m => m.ShowOnlyVisible) %>
            <%: Html.CheckBoxFor(m => m.HideExpired) %><%: Html.LabelFor(m => m.HideExpired) %>
            <%: Html.CheckBoxFor(m => m.ShowOnlyActive) %><%: Html.LabelFor(m => m.ShowOnlyActive) %>
            <input type="submit" value="Apply"/>
        <% } %>
        <table style="border-collapse:collapse" cellpadding="4" border="1">
            <thead>
                <tr>
                    <th style="width:70px">Position</th>
                    <th style="width:120px">Image</th>
                    <th>Title</th>
                    <th>Companies</th>
                    <th style="width:24px">Visible?</th>
                    <th style="width:40px; text-align:center">Edit</th>
                </tr>
            </thead>
            <tbody>
                <% foreach (var bannerItem in Model.Banners)
                   { %>
                    <tr>
                        <td>
                            <%= bannerItem.Position %>
                            <a href="<%= Url.Action("MoveBannerUp",new{id=bannerItem.Id}) %>" title="Move Banner Up">
                                <img src="<%= Url.Content("~/Content/Images/Icons/arrow_up.png") %>"/>
                            </a>
                            <a href="<%= Url.Action("MoveBannerDown",new{id=bannerItem.Id}) %>" title="Move Banner Down">
                                <img src="<%= Url.Content("~/Content/Images/Icons/arrow_down_red.png") %>"/>
                            </a>
                        </td>
                        <td align="center">
                            <img src="<%= bannerItem.ImageUrl %>" width="90"/>
                        </td>
                        <td><%= bannerItem.Title %></td>
                        <td><%= bannerItem.Companies.Aggregate(bannerItem.Companies.Any() ? String.Empty : ", ", (text, t) => text + t.Company.DisplayName + ", ", res => res.Remove(res.Length - 2))%></td>
                        <td align="center">
                            <img src="<%= Url.Content(bannerItem.IsCurrentlyVisible()? "~/Content/Images/Icons/ok.gif": "~/Content/Images/Icons/error_16.png") %>"/>
                        </td>
                        <td align="center">
                            <a href="<%= Url.Action("Edit",new{id=bannerItem.Id}) %>" title="Edit Banner">
                                <img src="<%= Url.Content("~/Content/Images/Icons/cog_edit.png") %>"/>
                            </a>
                        </td>
                    </tr>
                <% } %>
            </tbody>
        </table>
        <br/>
        <%: Html.ActionLink("Add New Banner","Create") %>
    </div>
    </div>
</asp:Content>
