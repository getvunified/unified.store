﻿<%@ Page Title="Title" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>"
    MasterPageFile="~/Views/Shared/Site.Master" %>
<%@ Import Namespace="Jhm.Web.Core.Models" %>

<asp:Content ID="pageTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Admin Menu
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Context.User.IsInRole(Role.KnownRoles.SuperAdmin) || Context.User.IsInRole(Role.KnownRoles.Admin) )
        { %>
        <div id="Div2" class="block">
            <h2 class="title">Admin & Super Admin</h2>
            <div class="content">
                <ul class="links">
                    <li><a class="parentLinkMenu" href="javascript:void(0);">Restful Actions</a>
                        <ul style="display: none;">
                            <li class="first">
                                <%= Html.ActionLink("Reload Products", "ReloadProductsFromAllEnvironments", "RestfulActions",null, new{target="_blank"}) %>
                            </li>
                            <li>
                                <%= Html.ActionLink("Reload Subscriptions", "ReloadSubscriptionsFromAllEnvironments", "RestfulActions",null, new{target="_blank"}) %>
                            </li>
                            <li>
                                <%= Html.ActionLink("Refresh Careers Cache", "RefreshCareersCache", "Resources",new{Area=String.Empty}, new{target="_blank"}) %>
                            </li>
                            <li class="last">
                                <%= Html.ActionLink("Impersonate User", "ImpersonateUserForm", "Account",new{Area=String.Empty}, new{target="_blank"}) %>
                            </li>
                            <li class="last">
                                <%= Html.ActionLink("Test Bluefin", "GetCreditCardTestToken", "RestfulActions",null, new{target="_blank"}) %>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <% } %>
    <% if (Context.User.IsInRole(Role.KnownRoles.SuperAdmin) ||
                       Context.User.IsInRole(Role.KnownRoles.Admin) ||
                       Context.User.IsInRole(Role.KnownRoles.ContentEditor))
                   { %>
        <div id="accountOptionsSideMenu" class="block">
            <h2 class="title">Content Management</h2>
            <div class="content">
                <ul class="links">
                    <li><a class="parentLinkMenu" href="javascript:void(0);">Home Banner Management</a>
                        <ul style="display: none;">
                            <li class="first">
                                <%= Html.ActionLink("List", "Index", "HomeBanner") %>
                            </li>
                            <li class="last">
                                <%= Html.ActionLink("Add New Banner", "Create", "HomeBanner") %>
                            </li>
                        </ul>
                    </li>
                    <li><a class="parentLinkMenu" href="javascript:void(0);">Jhm Magazine Listing</a>
                        <ul style="display: none;">
                            <li class="first">
                                <%= Html.ActionLink("List", "Index", "JhmMagazineListing") %>
                            </li>
                            <li class="last">
                                <%= Html.ActionLink("Add New Issue", "Create", "JhmMagazineListing") %>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <% } %>
     <% if (Context.User.IsInRole(Role.KnownRoles.SuperAdmin) || 
            Context.User.IsInRole(Role.KnownRoles.Admin) || 
            Context.User.IsInRole(Role.KnownRoles.ContentEditor ) )
       { %>
        <div id="Div1" class="block">
            <h2 class="title">
                Product Management</h2>
            <div class="content">
                <ul class="links">
                    <li><a class="parentLinkMenu" href="javascript:void(0);">Discounts</a>
                        <ul style="display: none;">
                           <li class="last">
                                <%=Html.ActionLink("Discount Management", "Index", "DiscountCodeManagement")%>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <% } %>
    <% if (Context.User.IsInRole(Role.KnownRoles.SuperAdmin) || 
            Context.User.IsInRole(Role.KnownRoles.Admin) || 
            Context.User.IsInRole(Role.KnownRoles.CustomerService) )
       { %>
        <div id="Div1" class="block">
            <h2 class="title">
                Customer Service</h2>
            <div class="content">
                <ul class="links">
                    <li><a class="parentLinkMenu" href="javascript:void(0);">Miscellaneous</a>
                        <ul style="display: none;">
                            <%--<li class="first">
                                <%=Html.ActionLink("Send Prophetic Blessing's Treasure Chest Email", "SendPropheticBlessingEmail", "CustomerService")%>
                            </li>--%>
                            <li class="last">
                                <%=Html.ActionLink("MP3 Downloads Manager", "DigitalDownloadsManager", "CustomerService")%>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.block ul.links .parentLinkMenu').bind("click", function (e) {
                var spanElement = this.previousSibling;
                $(spanElement).toggleClass('closed').toggleClass('open');
                {
                    $(spanElement).parent('li').toggleClass('').toggleClass('current');
                }
                $(spanElement).parents('li:first').children('ul:first').slideToggle('slow');
            });
        });
    </script>
</asp:Content>
