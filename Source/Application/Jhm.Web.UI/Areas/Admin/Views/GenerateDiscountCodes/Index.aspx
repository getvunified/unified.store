﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="<%:Url.Content("~/Content/css/DownloadsGridSixColumns.css") %>" type="text/css" rel="stylesheet"/>
    <script src="<%:Url.Content("~/Content/js/jquery-ui-1.7.2.custom.min.js") %>"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_6">
    <div class="grid_3">
        <input type="radio" id="discountCode" /> Create a new discount code
    </div>
    <div class="grid_3">
        <input type="radio" id="promotionalItem" /> Create a new promotional item
    </div>
</div>

</asp:Content>