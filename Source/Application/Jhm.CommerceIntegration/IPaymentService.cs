namespace Jhm.CommerceIntegration
{
    public interface IPaymentService
    {
        IPaymentResponse Process(IPaymentRequest request);
    }
}