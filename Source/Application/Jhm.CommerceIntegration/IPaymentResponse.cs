namespace Jhm.CommerceIntegration
{
    public interface IPaymentResponse
    {
        bool IsAuthorized { get; }
        string ReasonText { get; }
        string ApprovalCode { get; }
        string TransactionID { get; }
        string AvsResponse { get; }
        string Cvv2Response { get; }
        string AuthMessage { get; }
    }
}