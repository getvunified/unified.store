namespace Jhm.CommerceIntegration
{
    public class PaymentResponse : IPaymentResponse
    {
        private bool isAuthorized;
        private string reasonText;
        private string approvalCode;
        private string transactionID;
        private string avsResponse;
        private string cvv2Response;
        private string authMessage;

        public PaymentResponse(bool isAuthorized, string reasonText, string approvalCode, string transactionId, string avsResponse, string cvv2Response, string authMessage)
        {
            this.isAuthorized = isAuthorized;
            this.reasonText = reasonText;
            this.approvalCode = approvalCode;
            transactionID = transactionId;
            this.avsResponse = avsResponse;
            this.cvv2Response = cvv2Response;
            this.authMessage = authMessage;
        }

        public PaymentResponse(bool isAuthorized, string reasonText, string approvalCode, string transactionID)
        {
            this.isAuthorized = isAuthorized;
            this.transactionID = transactionID;
            this.approvalCode = approvalCode;
            this.reasonText = reasonText;
        }

        public string Cvv2Response
        {
            get { return cvv2Response; }
        }

        public string AuthMessage
        {
            get { return authMessage; }
        }

        public string AvsResponse
        {
            get { return avsResponse; }
        }

        public bool IsAuthorized
        {
            get { return isAuthorized; }
        }

        public string ReasonText
        {
            get { return reasonText; }
        }

        public string ApprovalCode
        {
            get { return approvalCode; }
        }

        public string TransactionID
        {
            get { return transactionID; }
        }
    }
}