using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace Jhm.CommerceIntegration
{
    public class BlueFinPaymentService : IPaymentService
    {
        public static readonly string LiveBlueFinAuthUrl = "https://secure.bluefingateway.com:1402/gw/sas/direct3.1";

        public class BluefinTransactionType
        {
            public static string Sale = "S";
            public static string Auth = "A";
            public static string Capture = "D";
            public static string Quasi = "Q";
        }
        

        public IPaymentResponse Process(IPaymentRequest request)
        {
            try
            {
                byte[] responseBody = ExecuteWebRequest(LiveBlueFinAuthUrl,CreatePayload(request, request.IsTest));

                string[] response = Encoding.ASCII.GetString(responseBody).Split('&');

                NameValueCollection responseItems = new NameValueCollection();
                foreach (string[] items in response.Select(s => s.Split('=')).Where(items => items.Length == 2))
                {
                    responseItems.Add(items[0], items[1]);
                }
                var isAuthorized = (responseItems[ResponseValues.Status].Equals("1") ||
                                    responseItems[ResponseValues.Status].Equals("T"))
                                       ? true
                                       : false;
                var approvalCode = responseItems[ResponseValues.AuthorizationCode];
                var reasonText = (!isAuthorized) ? responseItems[ResponseValues.AuthorizationMessage] : string.Empty;
                var transactionId = responseItems[ResponseValues.TransactionId];
                var avsResponse = responseItems[ResponseValues.AvsResult];
                var cvv2Response = responseItems[ResponseValues.Cvv2Result];
                var authMessage = responseItems[ResponseValues.AuthorizationMessage];


                return new PaymentResponse(isAuthorized, reasonText, approvalCode, transactionId, avsResponse, cvv2Response, authMessage);
                
            }
            catch (Exception ex)
            {
                return new PaymentResponse(false,
                    string.Format("Processing ERROR: {0}", ex.Message), string.Empty, string.Empty);
            }
            
        }

        private static byte[] ExecuteWebRequest(string baseAddress, NameValueCollection data)
        {
            var webClient = new WebClient { BaseAddress = baseAddress };
            return webClient.UploadValues(webClient.BaseAddress, "POST", data);
        }

        private NameValueCollection CreatePayload(IPaymentRequest request, bool isTest)
        {
            var data = new NameValueCollection(30)
            {
                {"account_id", request.AccountId},
                {"site_tag", request.SiteTag},
                {"dynip_sec_code", request.TransactionKey},
                {"pay_type", "C"},
                {"tran_type", request.TransactionType},
                {"amount", request.Amount.ToString()},
                {"bill_name1", request.FirstName},
                {"bill_name2", request.LastName},
                {"bill_street", request.AddressLine1},
                {"bill_city", request.City},
                {"bill_state", request.State},
                {"bill_zip", request.Zip},
                {"bill_country", request.Country},
                {"cust_email", request.CustomerEmail},
                {"cust_phone", request.CustomerPhone},
                {"description", request.DescriptionOfCharge},
                {"user_data", request.ExtraUserData},
                {"misc_info", request.MiscInfo},
                {"card_number", request.CardNumber},
                {"card_expire", request.CardExpDate},
                {"card_cvv2", request.CardCCV}
            };


            //data.Add("trans_id", "");
            //data.Add("orig_id", "");

            //data.Add("tax_amount", "");
            //data.Add("ship_amount", "");
            //data.Add("purch_order", "");
            //data.Add("courier_tracking", "");  // FEDEX, UPS, USPS, TNT, DHL;  FEDEX:xxxxxxxx

            //data.Add("ship_name1", request.);
            //data.Add("ship_name2", request.LastName);
            //data.Add("ship_street", "");
            //data.Add("ship_city", "");
            //data.Add("ship_state", ""); //2: xx
            //data.Add("ship_zip", "");
            //data.Add("ship_country", "");  //ISO 2  xx


            //data.Add("cust_ip", request.CustomerIP);
            //data.Add("cust_host", request.CustomerHost);
            //data.Add("cust_browser", request.CustomerBrowser);

            //data.Add("disable_avs", ConvertToString(request.DisableAVS));
            //data.Add("disable_cvv2", ConvertToString(request.DisableCVV2));
            //data.Add("disable_fraud_checks", ConvertToString(request.DisableFraudChecks));
            //data.Add("disable_negative_db", ConvertToString(request.DisableNegativeDB));
            //data.Add("disable_email_receipts", ConvertToString(request.DisableEmailReceipts));
            //data.Add("cisp_storage", ConvertToString(request.CispStorageEnabled));  //  Set to 1 to store card securely

            //data.Add("card_track1", request.CardTrack1);
            //data.Add("card_track2", request.CardTrack2);
            
            
            //data.Add("force_code", request.ForceCode);
            //data.Add("3ds_cavv", request.3dsCavv);  //Verified With Visa 
            //data.Add("3ds_xid", request.3dsXid);  //Verified With Visa
            
            //ACH
            //data.Add("account_number", "");
            //data.Add("bill_photo_id_no", "");
            //data.Add("bill_photo_id_no", "");
            //data.Add("bill_photo_id_state", "");
            //data.Add("bill_tax_id_no", "");
            //data.Add("bill_birth_date", "");
            
            
            //data.Add("card_pin", request.PINCodeForStoredCardHolder);
            
            
            //data.Add("member_username", "");
            //data.Add("member_duration", "");
            //data.Add("member_password", "");
            //data.Add("member_memo", "");
            //data.Add("recurring_amount", "");
            //data.Add("recurring_period", "");
            //data.Add("recurring_count", "");
            //data.Add("recurring_prorate", "");
            
            
            ////objInf.Add("x_password", AuthNetPassword);
            //data.Add("x_version", AuthorizeNetVersion);
            //data.Add("x_login", request.accountID);
            //data.Add("x_tran_key", request.TransactionKey);
            //data.Add("x_test_request", isTest ? "True" : "False");

            //data.Add("x_delim_data", "True");
            //data.Add("x_relay_response", "False");
            //data.Add("x_delim_char", ",");
            //data.Add("x_encap_char", "|");

            //data.Add("x_first_name", request.FirstName);
            //data.Add("x_last_name", request.LastName);
            //data.Add("x_address", request.Address);
            //data.Add("x_city", request.City);
            //data.Add("x_state", request.State);
            //data.Add("x_zip", request.Zip);
            //data.Add("x_country", request.Country);
            //data.Add("x_description", request.DescriptionOfCharge);

            //// Card Details
            //data.Add("x_card_num", request.CardNumber);
            //data.Add("x_exp_date", request.CardExpDate);

            //if (!string.IsNullOrEmpty(request.CardCCV) &&
            //    !request.CardCCV.Trim().Equals(String.Empty))
            //    data.Add("x_card_code", request.CardCCV);

            //data.Add("x_method", "CC");
            //data.Add("x_type", "AUTH_CAPTURE");
            //data.Add("x_amount", request.Amount.ToString());

            //// Currency setting. Check the guide for other supported currencies
            //data.Add("x_currency_code", "USD");

            return data;
        }


        

        private string ConvertToString(bool propertyToTest)
        {
            return propertyToTest ? "1" : "0";
        }


        public static class ResponseValues
        {
            public const string Status = "status_code";
            public const string TransactionId = "trans_id";
            public const string AuthorizationCode = "auth_code";
            public const string AuthorizationDate = "auth_date";
            public const string AuthorizationMessage = "auth_msg";
            public const string AvsResult = "avs_code";
            public const string Cvv2Result = "cvv2_code";
            public const string TicketCode = "ticket_code";
            public const string ReasonCode2 = "reason_code2";
            public const string MemberId = "member_id";
            public const string RecurringId = "recurring_id";
        }
    }
}
