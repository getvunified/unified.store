namespace Jhm.CommerceIntegration
{
    public interface IPaymentRequest
    {
        string AccountId { get; }
        string TransactionKey { get; }
        bool IsTest { get; }

        string FirstName { get; }
        string LastName { get; }

        string AddressLine1 { get; }
        string AddressLine2 { get; }
        string City { get; }
        string State { get; }
        string Zip { get; }
        string Country { get; }

        string DescriptionOfCharge { get; }
        string CardNumber { get; }
        string CardExpDate { get; }
        string CardCCV { get; }
        decimal Amount { get; }
        string Currency { get; set; }
        string PINCodeForStoredCardHolder { get; }
        string CardTrack2 { get; }
        string CardTrack1 { get; }
        bool CispStorageEnabled { get; }
        bool DisableNegativeDB { get; }
        bool DisableEmailReceipts { get; }
        bool DisableFraudChecks { get; }
        bool DisableCVV2 { get; }
        bool DisableAVS { get; }
        string MiscInfo { get; }
        string ExtraUserData { get; }
        string CustomerBrowser { get; }
        string CustomerHost { get; }
        string CustomerIP { get; }
        string CustomerPhone { get; }
        string CustomerEmail { get; }
        string SiteTag { get; }

        string TransactionType { get; }
    }
}