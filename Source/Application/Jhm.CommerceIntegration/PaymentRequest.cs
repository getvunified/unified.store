using System;

namespace Jhm.CommerceIntegration
{
    public class PaymentRequest : IPaymentRequest
    {
       
        private readonly string accountID;  
        private readonly string dynipSecCode;

        

        private readonly string transactionKey;
        private readonly string firstName;
        private readonly string lastName;
        private readonly string address;
        private readonly string city;
        private readonly string state;
        private readonly string zip;
        private readonly string country;
        private readonly string descriptionOfCharge;
        private readonly string cardNumber;
        private readonly string cardExpDate;
        private readonly string cardCCV;
        private readonly bool isTest;

        private readonly string addressLine1;
        private readonly string addressLine2;
        private readonly string pinCodeForStoredCardHolder;
        private readonly string cardTrack2;
        private readonly string cardTrack1;
        private readonly bool cispStorageEnabled;
        private readonly bool disableNegativeDb;
        private readonly bool disableEmailReceipts;
        private readonly bool disableFraudChecks;
        private readonly bool disableCvv2;
        private readonly bool disableAvs;
        private readonly string miscInfo;
        private readonly string extraUserData;
        private readonly string customerBrowser;
        private readonly string customerHost;
        private readonly string customerIp;
        private readonly string customerPhone;
        private readonly string customerEmail;
        private readonly string siteTag;
        private readonly string transactionType;


        public PaymentRequest(string accountId, string transactionKey, string firstName, string lastName, string address, string city, string state, string zip, string country, string descriptionOfCharge, string cardNumber, string cardExpDate, string cardCcv, decimal amount, bool isTest, string addressLine1, string addressLine2, string pinCodeForStoredCardHolder, string cardTrack2, string cardTrack1, bool cispStorageEnabled, bool disableNegativeDb, bool disableEmailReceipts, bool disableFraudChecks, bool disableCvv2, bool disableAvs, string miscInfo, string extraUserData, string customerBrowser, string customerHost, string customerIp, string customerPhone, string customerEmail, string siteTag, string currency, string transactionType = "S")
        {
            accountID = accountId;
            this.transactionKey = transactionKey;
            
            this.firstName = firstName;
            this.lastName = lastName;
            this.address = address;
            this.city = city;
            this.state = state;
            this.zip = zip;
            this.country = country;
            this.descriptionOfCharge = descriptionOfCharge;
            this.cardNumber = cardNumber;
            this.cardExpDate = cardExpDate;
            cardCCV = cardCcv;
            this.Amount = amount;
            this.isTest = isTest;
            this.addressLine1 = addressLine1;
            this.addressLine2 = addressLine2;
            this.pinCodeForStoredCardHolder = pinCodeForStoredCardHolder;
            this.cardTrack2 = cardTrack2;
            this.cardTrack1 = cardTrack1;
            this.cispStorageEnabled = cispStorageEnabled;
            this.disableNegativeDb = disableNegativeDb;
            this.disableEmailReceipts = disableEmailReceipts;
            this.disableFraudChecks = disableFraudChecks;
            this.disableCvv2 = disableCvv2;
            this.disableAvs = disableAvs;
            this.miscInfo = miscInfo;
            this.extraUserData = extraUserData;
            this.customerBrowser = customerBrowser;
            this.customerHost = customerHost;
            this.customerIp = customerIp;
            this.customerPhone = customerPhone;
            this.customerEmail = customerEmail;
            this.siteTag = siteTag;
            Currency = currency;
            this.transactionType = transactionType;
        }

        
        public PaymentRequest(string accountId, string transactionKey, bool isTest, string firstName, string lastName, string address, string city, string state, string zip, string country, string descriptionOfCharge, string cardNumber, string cardExpDate, string cardCCV, decimal amount, string siteTag, string currency, string transactionType = "S")
        {
            this.accountID = accountId;
            this.isTest = isTest;
            this.Amount = amount;
            this.cardCCV = cardCCV;
            this.cardExpDate = cardExpDate;
            this.cardNumber = cardNumber;
            this.descriptionOfCharge = descriptionOfCharge;
            this.country = country;
            this.zip = zip;
            this.state = state;
            this.city = city;
            this.address = address;
            this.lastName = lastName;
            this.firstName = firstName;
            this.transactionKey = transactionKey;
            this.transactionType = transactionType;
            this.siteTag = siteTag;
            Currency = currency;
        }

        public string AccountId
        {
            get { return accountID; }
        }
       
        public string DynipSecCode
        {
            get { return dynipSecCode; }
        }
        public string TransactionKey
        {
            get { return transactionKey; }
        }

        public bool IsTest
        {
            get { return isTest; }
        }

        public string FirstName
        {
            get { return firstName; }
        }

        public string LastName
        {
            get { return lastName; }
        }

        public string AddressLine1
        {
            get { return addressLine1; }
        }

        public string AddressLine2
        {
            get { return addressLine2; }
        }

        public string Address
        {
            get { return address; }
        }

        public string City
        {
            get { return city; }
        }

        public string State
        {
            get { return state; }
        }

        public string Zip
        {
            get { return zip; }
        }

        public string Country
        {
            get { return country; }
        }

        public string DescriptionOfCharge
        {
            get { return descriptionOfCharge; }
        }

        public string CardNumber
        {
            get { return cardNumber; }
        }

        public string CardExpDate
        {
            get { return cardExpDate; }
        }

        public string CardCCV
        {
            get { return cardCCV; }
        }

        public decimal Amount { get; set; }

        public string Currency { get; set; }

        public string PINCodeForStoredCardHolder
        {
            get { return pinCodeForStoredCardHolder; }
        }

        public string CardTrack2
        {
            get { return cardTrack2; }
        }

        public string CardTrack1
        {
            get { return cardTrack1; }
        }

        public bool CispStorageEnabled
        {
            get { return cispStorageEnabled; }
        }

        public bool DisableNegativeDB
        {
            get { return disableNegativeDb; }
        }

        public bool DisableEmailReceipts
        {
            get { return disableEmailReceipts; }
        }

        public bool DisableFraudChecks
        {
            get { return disableFraudChecks; }
        }

        public bool DisableCVV2
        {
            get { return disableCvv2; }
        }

        public bool DisableAVS
        {
            get { return disableAvs; }
        }

        public string MiscInfo
        {
            get { return miscInfo; }
        }

        public string ExtraUserData
        {
            get { return extraUserData; }
        }

        public string CustomerBrowser
        {
            get { return customerBrowser; }
        }

        public string CustomerHost
        {
            get { return customerHost; }
        }

        public string CustomerIP
        {
            get { return customerIp; }
        }

        public string CustomerPhone
        {
            get { return customerPhone; }
        }

        public string CustomerEmail
        {
            get { return customerEmail; }
        }

        public string SiteTag
        {
            get { return siteTag; }
        }

        public string TransactionType
        {
            get { return transactionType; }
        }
    }
}