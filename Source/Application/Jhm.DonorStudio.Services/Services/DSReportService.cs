﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Jhm.Common;
using Jhm.DonorStudio.Services.DonorReportService;
using Jhm.DonorStudio.Services.Utility;

namespace Jhm.DonorStudio.Services.Services
{
    public class DSReportService : IDSReportService
    {
        public byte[] GetTaxReceiptReport(DateTime startDate, DateTime endDate, long accountNumber, out string extension,
                                          out string mimeType, out string encoding)
        {
            IList<ParameterValue> parameters = new List<ParameterValue>();

            parameters.Add(new ParameterValue(){Name = "DataSource", Value = "JS-SQL1"});
            parameters.Add(new ParameterValue(){Name = "Database", Value = "JHMDONORSTUDIOBUSINESS"});
            
            parameters.Add(new ParameterValue(){Name = "BegDate", Value = startDate.ToShortDateString()});
            parameters.Add(new ParameterValue(){Name = "EndDate", Value = endDate.ToShortDateString()});
            parameters.Add(new ParameterValue(){Name = "AccountNumber", Value = accountNumber.ToString()});
            parameters.Add(new ParameterValue(){Name = "ShowByProj", Value = "Y"});

            byte[] output;
            Warning[] warnings;
            string[] streamIds;

            const string reportName = @"/Custom Reports/Account Reports/CUSTOM - Don Receipt By Acct";

            ReportExporter.Export(
                "donorReportServerEndpoint",
                new NetworkCredential("JWebAdmin", "fe*aq2swTr?", "JHM"),
                reportName, 
                parameters.ToArray(),
                ExportFormat.PDF,
                out output, 
                out extension,
                out mimeType,
                out encoding,
                out warnings,
                out streamIds);


            return output;
        }
    }

    public interface IDSReportService : IService
    {
        byte[] GetTaxReceiptReport(DateTime startDate, DateTime endDate, long accountNumber, out string extension,
                                   out string mimeType, out string encoding);


    }
}
