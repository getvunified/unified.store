using System;
using System.Collections.Generic;
using System.Linq;
using D2DNext.AB_CMN;
using D2DNext.AB_CMN.SearchCriteria;
using D2DNext.AB_CSA;
using D2DNext.AB_DL.BusinessDb;
using D2DNext.AB_DL.DomainModel;
using Jhm.Common.Framework.Extensions;
using Jhm.DonorStudio.Services.Config;
using Jhm.DonorStudio.Services.DTO;

namespace Jhm.DonorStudio.Services
{
    public class DSAccountTransactionService : IDSAccountTransactionService
    {
        private readonly IDSInternalService internalService;

        public DSAccountTransactionService(IDSInternalService internalService)
        {
            this.internalService = internalService;
        }

        
        public JHMAccount GetAccount(string environment, long accountNumber)
        {
            var environmentService = internalService.ServiceAccess(environment);

            accountNumber = GetCurrentAccountNumber(environment, accountNumber);

            var ifi = environmentService.Account().AccountsIndividualAndFamilyInfo(accountNumber);

            if (ifi == null)
                return null;

            string accountStatus = null;
            long familyId = -1;
            string accountType = null;
            var accountMaster = environmentService.Account().AccountMasterSearchByAccountNumber(accountNumber);
            if (accountMaster != null && !string.IsNullOrEmpty(accountMaster.Status))
            {
                accountType = accountMaster.AccountType;
                accountStatus = accountMaster.Status;
                familyId = accountMaster.FamilyId.GetValueOrDefault();
            }
            var activity = environmentService.Account().OrderDetailsSearchByAccountNumber(accountNumber);//,new PagingContext(){EndingRow=19,MaxRows=20,StartingRow=0});

            JHMAccount jhmAccount = new JHMAccount
            {
                AccountNumber = ifi.PrimaryAccount.Account.AccountNumber,
                DsAddress = new DsAddress
                {
                    City = ifi.PrimaryAddress.City,
                    Country = ifi.PrimaryAddress.Country,
                    Line1 = ifi.PrimaryAddress.AddressLine1,
                    Line2 = ifi.PrimaryAddress.AddressLine2,
                    State = ifi.PrimaryAddress.State,
                    Type = ifi.PrimaryAddress.Type,
                    ZipCode = ifi.PrimaryAddress.ZipPostal,
                    IsPrimary = true,
                    AddressId = ifi.PrimaryAddress.AddressId
                },
                Birthday = ifi.PrimaryAccount.BirthDate.DateValue.GetValueOrDefault(),
                Email = ifi.PrimaryAccount.PrimaryEmail.EmailAddress,
                Title = ifi.PrimaryAccount.Account.Title,
                FirstName = ifi.PrimaryAccount.Account.FirstName,
                MiddleName = ifi.PrimaryAccount.Account.MiddleName,
                LastName = ifi.PrimaryAccount.Account.LastName,
                Suffix = ifi.PrimaryAccount.Account.Suffix,
                Gender = ifi.PrimaryAccount.Gender.CodeValue,
                Phone = new DsPhone
                {
                    Type = ifi.PrimaryPhone.PhoneType,
                    AreaCode = ifi.PrimaryPhone.AreaCode,
                    CountryCode = ifi.PrimaryPhone.CountryPrefix,
                    Number = ifi.PrimaryPhone.TelephoneNumber,
                    Extension = ifi.PrimaryPhone.TelephoneExtension
                },
                Salutation = ifi.PrimaryAccount.PrimarySalutation.Salutation,
                Source = ifi.SourceCode.CodeValue,
                Type = accountType,
                Status = accountStatus,
                FamilyId = familyId
            };
            return jhmAccount;

        }
        public long CreateAccount(string environment, JHMAccount jhmAccount)
        {
            var environmentService = internalService.ServiceAccess(environment);

            IndividualAndFamilyInfo ifi = new IndividualAndFamilyInfo();

            //Primary

            //AccountMaster
            ifi.PrimaryAccount.Account.AccountType = jhmAccount.Type;
            ifi.PrimaryAccount.Account.AllowTransactions = true;
            ifi.PrimaryAccount.Account.FamilyConsolidate = true;
            ifi.PrimaryAccount.Account.Status = DSConstants.AccountStatus.Duplicate;
            ifi.PrimaryAccount.Account.FirstName = jhmAccount.FirstName;
            ifi.PrimaryAccount.Account.MiddleName = jhmAccount.MiddleName;
            ifi.PrimaryAccount.Account.LastName = jhmAccount.LastName;
            ifi.PrimaryAccount.Account.Suffix = jhmAccount.Suffix;
            ifi.PrimaryAccount.Account.Title = jhmAccount.Title;

            //Email
            ifi.PrimaryAccount.PrimaryEmail.DataSource = DSConstants.DataSource;
            ifi.PrimaryAccount.PrimaryEmail.EmailType = DSConstants.EmailType.Web;
            ifi.PrimaryAccount.PrimaryEmail.EmailAddress = jhmAccount.Email;

            //Gender
            ifi.PrimaryAccount.Gender.Active = true;
            ifi.PrimaryAccount.Gender.CodeValue = jhmAccount.Gender;
            ifi.PrimaryAccount.Gender.DataSource = DSConstants.DataSource;

            //Birthdate
            ifi.PrimaryAccount.BirthDate.DateValue = jhmAccount.Birthday.Date;
            ifi.PrimaryAccount.BirthDate.DataSource = DSConstants.DataSource;

            //Salutation
            ifi.PrimaryAccount.PrimarySalutation.Salutation = jhmAccount.Salutation;

            //Source code
            ifi.SourceCode.DataSource = DSConstants.DataSource;
            ifi.SourceCode.CodeValue = DSConstants.AccountSourceCode;

            //DsAddress
            ifi.PrimaryAddress.AddressIsPOBox = false;
            ifi.PrimaryAddress.Type = jhmAccount.DsAddress.Type;
            ifi.PrimaryAddress.AddressLine1 = jhmAccount.DsAddress.Line1;
            ifi.PrimaryAddress.AddressLine2 = jhmAccount.DsAddress.Line2;
            ifi.PrimaryAddress.City = jhmAccount.DsAddress.City;
            ifi.PrimaryAddress.State = jhmAccount.DsAddress.State;
            ifi.PrimaryAddress.Country = jhmAccount.DsAddress.Country;
            ifi.PrimaryAddress.ZipPostal = jhmAccount.DsAddress.ZipCode;

            //Phone
            ifi.PrimaryPhone.CountryPrefix = jhmAccount.Phone.CountryCode;
            ifi.PrimaryPhone.AreaCode = jhmAccount.Phone.AreaCode;
            ifi.PrimaryPhone.TelephoneNumber = jhmAccount.Phone.Number;
            ifi.PrimaryPhone.TelephoneExtension = jhmAccount.Phone.Extension;
            ifi.PrimaryPhone.PhoneType = jhmAccount.Phone.Type;
            ifi.PrimaryPhone.DataSource = DSConstants.DataSource;

            long accountNumber = environmentService.Account().AccountsIndividualAndFamilyInfoAdd(ifi,
                                                                            DSConstants.AccountDefinitionType.Single,
                                                                            string.Empty);

            return accountNumber;
        }
        public void UpdateAccount(string environment, JHMAccount jhmAccount)
        {
            var environmentService = internalService.ServiceAccess(environment);

            
            IndividualAndFamilyInfo oldifi = environmentService.Account().AccountsIndividualAndFamilyInfo(jhmAccount.AccountNumber);


            IndividualAndFamilyInfo ifi = environmentService.Account().AccountsIndividualAndFamilyInfo(jhmAccount.AccountNumber);

            //Primary

            //AccountMaster
            ifi.PrimaryAccount.Account.AccountType = jhmAccount.Type;
            ifi.PrimaryAccount.Account.AllowTransactions = true;
            ifi.PrimaryAccount.Account.FamilyConsolidate = true;
            ifi.PrimaryAccount.Account.Status = DSConstants.AccountStatus.Duplicate;
            ifi.PrimaryAccount.Account.FirstName = jhmAccount.FirstName;
            ifi.PrimaryAccount.Account.MiddleName = jhmAccount.MiddleName;
            ifi.PrimaryAccount.Account.LastName = jhmAccount.LastName;
            ifi.PrimaryAccount.Account.Suffix = jhmAccount.Suffix;
            ifi.PrimaryAccount.Account.Title = jhmAccount.Title;
            ifi.PrimaryAccount.Account.AccountNumber = jhmAccount.AccountNumber;
            ifi.PrimaryAccount.Account.FamilyId = jhmAccount.FamilyId;
            ifi.PrimaryAccount.Account.Status = jhmAccount.Status;

            //Email
            ifi.PrimaryAccount.PrimaryEmail.DataSource = DSConstants.DataSource;
            ifi.PrimaryAccount.PrimaryEmail.EmailType = DSConstants.EmailType.Web;
            ifi.PrimaryAccount.PrimaryEmail.EmailAddress = jhmAccount.Email;

            //Gender
            ifi.PrimaryAccount.Gender.Active = true;
            ifi.PrimaryAccount.Gender.CodeValue = jhmAccount.Gender;
            ifi.PrimaryAccount.Gender.DataSource = DSConstants.DataSource;

            //Birthdate
            ifi.PrimaryAccount.BirthDate.DateValue = jhmAccount.Birthday.Date;
            ifi.PrimaryAccount.BirthDate.DataSource = DSConstants.DataSource;

            //Salutation
            ifi.PrimaryAccount.PrimarySalutation.Salutation = jhmAccount.Salutation;

            //Source code
            ifi.SourceCode.DataSource = DSConstants.DataSource;
            ifi.SourceCode.CodeValue = jhmAccount.Source;

            //DsAddress
            ifi.PrimaryAddress.AddressId = jhmAccount.DsAddress.AddressId;
            ifi.PrimaryAddress.AddressIsPOBox = false;
            ifi.PrimaryAddress.Type = jhmAccount.DsAddress.Type;
            ifi.PrimaryAddress.AddressLine1 = jhmAccount.DsAddress.Line1;
            ifi.PrimaryAddress.AddressLine2 = jhmAccount.DsAddress.Line2;
            ifi.PrimaryAddress.City = jhmAccount.DsAddress.City;
            ifi.PrimaryAddress.State = jhmAccount.DsAddress.State;
            ifi.PrimaryAddress.Country = jhmAccount.DsAddress.Country;
            ifi.PrimaryAddress.ZipPostal = jhmAccount.DsAddress.ZipCode;

            //Phone
            ifi.PrimaryPhone.CountryPrefix = jhmAccount.Phone.CountryCode;
            ifi.PrimaryPhone.AreaCode = jhmAccount.Phone.AreaCode;
            ifi.PrimaryPhone.TelephoneNumber = jhmAccount.Phone.Number;
            ifi.PrimaryPhone.TelephoneExtension = jhmAccount.Phone.Extension;
            ifi.PrimaryPhone.PhoneType = jhmAccount.Phone.Type;
            ifi.PrimaryPhone.DataSource = DSConstants.DataSource;

            environmentService.Account().AccountsIndividualAndFamilyInfoUpdate(oldifi, ifi,
                                                                            DSConstants.AccountDefinitionType.Single,
                                                                            string.Empty);
        }

        public long CreateAccountAddress(string environment, long accountNumber, DsAddress dsAddress)
        {
            var environmentService = internalService.ServiceAccess(environment);

            try
            {
                environmentService.Account().AccountAddressAdd(new A02_AccountAddressesSearchByAccountNumberResult
                {
                    AccountNumber = accountNumber,
                    Active = true,
                    AddressIsPOBox = false,
                    AddressLine1 = dsAddress.Line1,
                    AddressLine2 = dsAddress.Line2,
                    City = dsAddress.City,
                    State = dsAddress.State,
                    Country = dsAddress.Country,
                    ZipPostal = dsAddress.ZipCode,
                    UseAsPrimary = dsAddress.IsPrimary,
                    Type = dsAddress.Type,
                    StartDate = DateTime.Now.Date
                });


                var allAddresses =
                    environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                                                                                      {
                                                                                          AccountNumber = accountNumber,
                                                                                          ActiveOnly = 'Y',
                                                                                          PrimaryOnly = dsAddress.IsPrimary ? 'Y' : 'N'
                                                                                      },
                                                                                  PagingContext.AllDataPaging());
                long addressId = allAddresses.OrderByDescending(x => x.RecordId.GetValueOrDefault()).Select(x => x.AddressId).
                    FirstOrDefault().GetValueOrDefault();

                if (addressId > 0)
                    return addressId;

                return -2;
            }
            catch (Exception)
            {
                return -1;
            }
        }
        public long UpdateAccountAddress(string environment, long accountNumber, DsAddress dsAddress)
        {
            var environmentService = internalService.ServiceAccess(environment);
            var accountAddresses = GetAccountAddresses(environment, accountNumber);
            var address = accountAddresses.Find(x => x.AddressId == dsAddress.AddressId);
            if (address != null)
            {
                var newAddress = new A02_AccountAddressesSearchByAccountNumberResult
                                     {
                                         AccountNumber = accountNumber,
                                         Active = true,
                                         AddressIsPOBox = false,
                                         AddressId = dsAddress.AddressId,
                                         AddressLine1 = dsAddress.Line1,
                                         AddressLine2 = dsAddress.Line2,
                                         A02RecordId = dsAddress.A02RecordId,
                                         RecordId = dsAddress.RecordId,
                                         City = dsAddress.City,
                                         State = dsAddress.State,
                                         Country = dsAddress.Country,
                                         ZipPostal = dsAddress.ZipCode,
                                         UseAsPrimary = dsAddress.IsPrimary,
                                         Type = dsAddress.Type,
                                         
                                         StartDate = DateTime.Now.Date

                                     };
                
                try
                {
                    environmentService.Account().AccountAddressUpdate( newAddress);


                    var allAddresses =
                        environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                                                                                          {
                                                                                              AccountNumber =
                                                                                                  accountNumber,
                                                                                              ActiveOnly = 'Y',
                                                                                              PrimaryOnly =
                                                                                                  dsAddress.IsPrimary
                                                                                                      ? 'Y'
                                                                                                      : 'N'
                                                                                          },
                                                                                      PagingContext.AllDataPaging());
                    long addressId =
                        allAddresses.OrderByDescending(x => x.RecordId.GetValueOrDefault()).Select(x => x.AddressId).
                            FirstOrDefault().GetValueOrDefault();

                    if (addressId > 0)
                        return addressId;

                    return -2;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            return 1;
        }
        public long DeleteAccountAddress(string environment, long accountNumber, DsAddress dsAddress)
        {
            var environmentService = internalService.ServiceAccess(environment);
            var accountAddresses = GetAccountAddresses(environment, accountNumber);
            var address = accountAddresses.Find(x => x.RecordId == dsAddress.RecordId);
            if (address != null)
            {
                try
                {
                    environmentService.Account().AccountAddressDelete(dsAddress.A02RecordId);


                    var allAddresses =
                        environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                        {
                            AccountNumber =
                                accountNumber,
                            ActiveOnly = 'Y',
                            PrimaryOnly =
                                dsAddress.IsPrimary
                                    ? 'Y'
                                    : 'N'
                        },
                                                                                      PagingContext.AllDataPaging());
                    long addressId =
                        allAddresses.OrderByDescending(x => x.RecordId.GetValueOrDefault()).Select(x => x.AddressId).
                            FirstOrDefault().GetValueOrDefault();

                    if (addressId > 0)
                        return addressId;

                    return -2;
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            return 1;
        }


        public void AddAccountCodes(string environment, long accountNumber, List<DSAccountCode> accountCodes)
        {
            var environmentService = internalService.ServiceAccess(environment);

            foreach (var ac in accountCodes)
            {
                environmentService.Account().AccountCodesAdd(new A01cAccountCodes { AccountNumber = accountNumber, Active = ac.Active, CodeType = ac.Type, CodeValue = ac.Value, DataSource = ac.DataSource });
            }
        }
        public void UpdateAccountCodes(string environment, long accountNumber, List<DSAccountCode> accountCodes)
        {
            var environmentService = internalService.ServiceAccess(environment);

            foreach (var ac in accountCodes)
            {
                environmentService.Account().AccountCodesUpdate(new A01cAccountCodes { AccountNumber = accountNumber, Active = ac.Active, CodeType = ac.Type, CodeValue = ac.Value, DataSource = ac.DataSource, RecordId = ac.RecordId });
            }
        }
        public void DeleteAccountCodes(string environment, long accountNumber, List<DSAccountCode> accountCodes)
        {
            var environmentService = internalService.ServiceAccess(environment);

            foreach (var ac in accountCodes)
            {
                environmentService.Account().AccountCodesDelete(ac.RecordId);
            }
        }
        
        
        public List<JHMTransaction> GetTransactionHistory(string environment, int howManyMonthsBack, long accountNumber)
        {
            List<JHMTransaction> result = new List<JHMTransaction>();
            
            var environmentService = internalService.ServiceAccess(environment);

            var tMaster = environmentService.Account().TransactionMasterSearchByAccountNumber(accountNumber, string.Empty, PagingContext.AllDataPaging());
            var monthsBack = DateTime.Today.AddMonths(-howManyMonthsBack);
            var checkTransactionDate = tMaster.Where(x => x.Date >= monthsBack);

            foreach (var tm in checkTransactionDate)
            {
                //var tData = environmentService.Account().TransactionMasterContainsDataByCriteria(tm.DocumentNumber.GetValueOrDefault(), tm.ResponseId.GetValueOrDefault());
                var myJhmTransaction = GetTransactionByDocumentNumber(environment, tm.DocumentNumber);

                if (myJhmTransaction != null)
                {
                    result.Add(myJhmTransaction);
                }
            }

            return result;
        }
        public JHMTransaction GetTransactionByDocumentNumber(string enviroment, long accountNumber, long documentNumber)
        {
            var transactionByDocumentNumber = GetTransactionByDocumentNumber(enviroment, documentNumber);
            return transactionByDocumentNumber;
        }
        public long CreateTransaction(string environment, JHMTransaction transaction, bool useAutoBatch = false)
        {
            var environmentService = internalService.ServiceAccess(environment);

            long transactionId = 0;
            decimal orderAmount = 0;
            decimal donationAmount = 0;
   			decimal subscriptionsAmount = 0;
			 long orderId = 1;

            Transaction t = new Transaction();

            t.TransactionMaster = new T01_TransactionMaster();
            t.TransactionMaster.AccountNumber = transaction.AccountNumber;  //Purchaser

            t.TransactionMaster.BatchNumber = (useAutoBatch) ? GetAutoBatchNumber(environment, "JHM.Web", DSConstants.GetCompany(environment)) : transaction.BatchNumber;

            //t.TransactionMaster.BatchNumber = transaction.BatchNumber;
            t.TransactionMaster.CurrencyCode = transaction.CurrencyCode;
            t.TransactionMaster.MediaId = transaction.MediaId;
            t.TransactionMaster.Date = transaction.Date.Date;
            t.TransactionMaster.AmountDue = transaction.AmountDue;
            t.TransactionMaster.RemainingAmountDue = transaction.RemainingAmountDue;
            t.TransactionMaster.Status = transaction.Status;

            t.OrderHeaders = new List<T02_Orders>();
            t.OrderDetails = new List<T03_OrderDetails>();
            t.Donations = new List<T04_GiftDetails>();
            t.PaymentDetails = new List<PaymentContainer>();
            t.Subscriptions = new List<S01_SubscriptionHeaders>();
            t.SecondPartyOrderResponses = new List<T07_TransactionResponseMaster>();
            

            //  Primary Address of the Purchaser
            DsAddress dsPurchaserPrimaryAddress = new DsAddress();
            dsPurchaserPrimaryAddress = GetPrimaryAddress(environment, transaction.AccountNumber);

            foreach (var order in transaction.Orders)
            {
                DsAddress dsAddress = new DsAddress();
                
                //TODO:  Verify below
                #region Verify this address logic.. Affects the address for the shipment
                //Get DsAddress for AddressId
                if (order.AddressId > 0)
                {
                    dsAddress = GetAddress(environment, order.AddressId);

                    // Set to 0 if not found
                    if (dsAddress == null)
                        order.AddressId = 0;
                }

                //If DsAddress not specified, get main one for the account
                if (order.AddressId <= 0)
                {
                    dsAddress = dsPurchaserPrimaryAddress;
                    if (dsAddress != null)
                        order.AddressId = dsAddress.AddressId;
                }
                #endregion


                //New Order Header
                var oh = new T02_Orders();
                oh.OrderId = orderId;   // unique per address

                oh.AccountNumber = transaction.AccountNumber;  // Ship to Account Number
                oh.AddressId = order.AddressId;  // Ship to address
                oh.ShipMethod = order.ShippingMethod;
                oh.Warehouse = order.Warehouse;
                oh.ShippingAmount = order.ShippingAmount;
                

                // If the ship to address is different than the primary address Add the T07
                if((order.AddressId != dsPurchaserPrimaryAddress.AddressId) || (orderId > 1))
                {
                    oh.ResponseId = orderId;

                    var trp = new T07_TransactionResponseMaster();
                    
                    trp.ResponseId = orderId;
                    trp.AccountNumber = transaction.AccountNumber;
                    trp.AddressId = order.AddressId;  // Ship to Address
                    trp.LetterCodeAutoAssigned = false;
                    t.SecondPartyOrderResponses.Add(trp);
                }

                orderAmount += order.ShippingAmount;

                foreach (var detail in order.OrderDetails)
                {
                    orderAmount += (detail.Price * detail.Quantity) - detail.DiscountAmount;

                    var od = new T03_OrderDetails();
                    od.DiscountAmount = detail.DiscountAmount;
                    od.OrderId = orderId;  //  Which address to go to
                    od.ProductCode = detail.ProductCode;
                    od.Quantity = detail.Quantity;
                    od.ShortComment = detail.Comment;
                    od.SourceCode = detail.SourceCode;
                    od.PriceCode = detail.PriceCode;
                    od.Price = detail.Price;

                    t.OrderDetails.Add(od);


                }

                //Get Tax Rates
                if (dsAddress != null && order.Tax == null)
                    order.Tax = GetTaxRates(environment, null, dsAddress.ZipCode, dsAddress.Country, orderAmount, order.ShippingAmount);

                if (order.Tax != null)
                {
                    orderAmount += order.Tax.TaxAmount1 + order.Tax.TaxAmount2 + order.Tax.TaxAmount3 + order.Tax.TaxAmount4 + order.Tax.TaxAmount5;
                    oh.TaxArea = order.Tax.TaxArea;
                    oh.SalesTaxAmount1 = order.Tax.TaxAmount1;
                    oh.SalesTaxAmount2 = order.Tax.TaxAmount2;
                    oh.SalesTaxAmount3 = order.Tax.TaxAmount3;
                    oh.SalesTaxAmount4 = order.Tax.TaxAmount4;
                    oh.SalesTaxAmount5 = order.Tax.TaxAmount5;
                }

                t.OrderHeaders.Add(oh);

                orderId++;
            }

            foreach (var donation in transaction.Donations)
            {
                donationAmount += donation.Amount;

                var d = new T04_GiftDetails();
                d.ProjectCode = donation.ProjectCode;
                d.SourceCode = donation.SourceCode;
                d.TotalAmount = donation.Amount;
                d.Deductible = donation.IsDeductible;
                d.Anonymous = donation.Anonymous;

                t.Donations.Add(d);
            }

            var subscriptionCount = 1;
            if(transaction.Subscriptions.Count >0)
                t.SecondPartySubscriptionResponses = new List<T07_TransactionResponseMaster>();


            // Subscriptions
            foreach (var userSubscription in transaction.Subscriptions)
            {
                subscriptionsAmount += userSubscription.SubscriptionCost;
                
                var s = CreateSubscriptionHeader(userSubscription);
                
                if ((userSubscription.DeliveryAddressId != dsPurchaserPrimaryAddress.AddressId) || subscriptionCount > 1)
                {

                    s.ResponseId = subscriptionCount;

                    var trp = new T07_TransactionResponseMaster();

                    trp.ResponseId = subscriptionCount;
                    trp.AccountNumber = userSubscription.AccountNumber;
                    trp.AddressId = userSubscription.DeliveryAddressId;
                    trp.LetterCodeAutoAssigned = false;
                    t.SecondPartySubscriptionResponses.Add(trp);
                }


                t.Subscriptions.Add(s);

                subscriptionCount++;
            }

            transaction.TotalAmount = orderAmount + donationAmount + subscriptionsAmount;

            t.TransactionMaster.Amount = transaction.TotalAmount;

            var pc = new PaymentContainer {PaymentType = transaction.PaymentType};

            if (transaction.PaymentType == DSConstants.PaymentType.CreditCard)
            {
                pc.NameOnCard = transaction.CreditCard.Name;

                if (transaction.CreditCard.Token.IsNot().NullOrEmpty())
                {
                    pc.Token = transaction.CreditCard.Token;
                    pc.CreditCardNumber = transaction.CreditCard.Number;
                    pc.ExpirationMonth = transaction.CreditCard.ExpirationMonth;
                    pc.ExpirationYear = transaction.CreditCard.ExpirationYear;
                }
                else
                {
                    pc.CreditCardNumber = transaction.CreditCard.Number;
                    pc.ExpirationMonth = transaction.CreditCard.ExpirationMonth;
                    pc.ExpirationYear = transaction.CreditCard.ExpirationYear;
                    pc.CID = transaction.CreditCard.Cvv;
                }

                pc.MerchantId = transaction.CreditCard.MerchantId;
                pc.AlreadyProcessed = transaction.CreditCard.AlreadyProcessed;
            }
            else if (transaction.PaymentType == DSConstants.PaymentType.NoFunds)
            {
            }

            pc.Amount = transaction.TotalAmount;

            t.PaymentDetails.Add(pc);

            transactionId = environmentService.Account().AddTransaction(t);

            return transactionId;
        }
        public Tax GetTaxRates(string environment, string productCode, string zip, string country, decimal price, decimal shipping)
        {
            var environmentService = internalService.ServiceAccess(environment);

            Tax tax = null;

            var taxRates = environmentService.Account().CalculateSalesTax(productCode, zip, country, price, shipping);

            if (taxRates != null)
                tax = new Tax
                          {
                              TaxAmount1 = taxRates.TaxRate1.GetValueOrDefault(),
                              TaxAmount2 = taxRates.TaxRate2.GetValueOrDefault(),
                              TaxAmount3 = taxRates.TaxRate3.GetValueOrDefault(),
                              TaxAmount4 = taxRates.TaxRate4.GetValueOrDefault(),
                              TaxAmount5 = taxRates.TaxRate5.GetValueOrDefault(),
                              TaxArea = taxRates.TaxArea
                          };

            return tax;
        }
        public List<DsUserSubscription> GetActiveAccountSubscriptions(string environment, long accountNumber)
        {
            var environmentService = internalService.ServiceAccess(environment);

            var subscriptions = environmentService.Account().SubscriptionsSearchByAccountNumber(accountNumber, PagingContext.AllDataPaging());

            return subscriptions
                .Where(x => x.SubscriptionStatus == "A")
                .Select(subscription => new DsUserSubscription
                {
                    AccountNumber = subscription.AccountNumber.GetValueOrDefault(), 
                    CancelDate = subscription.CancelDate, 
                    CancelReasonCode = subscription.CancelReasonCode, 
                    DeliveryAddressId = subscription.DeliveryId.GetValueOrDefault(), 
                    ExpirationDate = subscription.ExpirationDate, 
                    NumberOfIssues = (int) subscription.NumberOfIssues.GetValueOrDefault(), 
                    NumberOfIssuesRemaining = (int) subscription.NumberRemaining.GetValueOrDefault(), 
                    PriceCode = subscription.PriceCode, 
                    StartDate = subscription.StartDate.GetValueOrDefault(), 
                    SubscriptionCode = subscription.SubscriptionCode, 
                    SubscriptionStatus = subscription.SubscriptionStatus, 
                    SubscriptionId = subscription.SubscriptionId.GetValueOrDefault(),
                    DsRecordId = subscription.RecordId.GetValueOrDefault()
                }).ToList();
        }
        public void CancelAccountSubscription(string environment, DsUserSubscription subscription)
        {
            var environmentService = internalService.ServiceAccess(environment);
            subscription.CancelDate = DateTime.Now;
            subscription.CancelReasonCode = "NIL";
            subscription.SubscriptionStatus = "X";

            environmentService.Account().AccountSubscriptionsUpdate(CreateSubscriptionHeader(subscription));
        }
        public void UpdateAccountSubscription(string environment, DsUserSubscription subscription)
        {
            var environmentService = internalService.ServiceAccess(environment);
            
            environmentService.Account().AccountSubscriptionsUpdate(CreateSubscriptionHeader(subscription));
            
        }
        public long AddSubscription(string environment, long accountNumber, S01_SubscriptionHeaders subscriptionHeader)
        {
            var environmentService = internalService.ServiceAccess(environment);

            long subscriptionId;
            subscriptionHeader.AccountNumber = accountNumber;
            environmentService.Account().AccountSubscriptionsAdd(out subscriptionId, subscriptionHeader);
            return subscriptionId;
        }
        public long AddSubscription(string environment, long accountNumber, DsUserSubscription userSubscription)
        {
            var environmentService = internalService.ServiceAccess(environment);

            long subscriptionId;
            userSubscription.AccountNumber = accountNumber;
            environmentService.Account().AccountSubscriptionsAdd(out subscriptionId, CreateSubscriptionHeader(userSubscription));
            return subscriptionId;
        }
        public List<DsAddress> GetAccountAddresses(string environment, long accountNumber)
        {
            var environmentService = internalService.ServiceAccess(environment);

            var addresses = environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                                                                                              {
                                                                                                  AccountNumber = accountNumber,
                                                                                                  ActiveOnly = 'Y'
                                                                                              }, PagingContext.AllDataPaging());
            var result = addresses.Select(a => new DsAddress
            {
                RecordId = a.RecordId.GetValueOrDefault(), 
                A02RecordId = a.A02RecordId.GetValueOrDefault(), 
                AddressId = a.AddressId.GetValueOrDefault(), 
                Type = a.Type, 
                Line1 = a.AddressLine1, 
                Line2 = a.AddressLine2, 
                City = a.City, 
                State = a.State, 
                ZipCode = a.ZipPostal, 
                Country = a.Country, 
                IsPrimary = a.UseAsPrimary.GetValueOrDefault()
            }).ToList();

            return result.Count > 0 ? result : null;
        }
        public DsAddress GetAddress(string environment, long addressId)
        {
            var environmentService = internalService.ServiceAccess(environment);

            var dsAddress = new DsAddress();

            var a = environmentService.Account().AddressMasterSearchByAddressId(addressId);

            if (a != null)
            {
                dsAddress.AddressId = a.AddressId.GetValueOrDefault();
                dsAddress.Type = a.Type;
                dsAddress.Line1 = a.AddressLine1;
                dsAddress.Line2 = a.AddressLine2;
                dsAddress.City = a.City;
                dsAddress.State = a.State;
                dsAddress.ZipCode = a.ZipPostal;
                dsAddress.Country = a.Country;

                return dsAddress;
            }

            return null;
        }
        public DsAddress GetPrimaryAddress(string environment, long accountNumber)
        {
            var environmentService = internalService.ServiceAccess(environment);

            DsAddress dsAddress = new DsAddress();

            var a =
                environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                                                                                  {
                                                                                      AccountNumber = accountNumber,
                                                                                      ActiveOnly = 'Y',
                                                                                      PrimaryOnly = 'Y'
                                                                                  },
                                                                              PagingContext.InitialPaging(1)).FirstOrDefault();


            if (a != null)
            {
                dsAddress.AddressId = a.AddressId.GetValueOrDefault();
                dsAddress.Type = a.Type;
                dsAddress.Line1 = a.AddressLine1;
                dsAddress.Line2 = a.AddressLine2;
                dsAddress.City = a.City;
                dsAddress.State = a.State;
                dsAddress.ZipCode = a.ZipPostal;
                dsAddress.Country = a.Country;
            }

            return dsAddress;
        }
        public long GetCurrentAccountNumber(string environment, long mergedAccountNumber)
        {
            var environmentService = internalService.ServiceAccess(environment);

            while (true)
            {
                var currentAccount = mergedAccountNumber;
                var mergeHeader = environmentService.Account().AccountMergeHeadersSearchByMergedAccountNumber(mergedAccountNumber);
                if (mergeHeader != null && mergeHeader.AccountNumber > 0)
                    mergedAccountNumber = (((mergeHeader.FamilyId > 0) && (mergeHeader.FamilyConsolidate.GetValueOrDefault())) ? mergeHeader.FamilyId : mergeHeader.AccountNumber).GetValueOrDefault();


                if ((currentAccount == mergedAccountNumber) || (mergedAccountNumber == 0))
                    break;
            }

            return mergedAccountNumber;
        }
        public long GetAutoBatchNumber(string environment, string assignedUser, string companyCode)
        {
            var environmentService = internalService.ServiceAccess(environment);

            long batchNumber = 0;

            var batch = environmentService.Account().GetAutomaticBatchByUserIdAndCompany(assignedUser, companyCode);

            if (batch != null)
                batchNumber = batch.BatchNumber.GetValueOrDefault();

            return batchNumber;
        }
        public void AddAccountCode(string environment, long accountNumber, string codeType, string codeValue)
        {
            var environmentService = internalService.ServiceAccess(environment);
            accountNumber = GetCurrentAccountNumber(environment, accountNumber);
            var result = environmentService.Account().AccountCodesSearchByAccountNumberAndType(accountNumber, codeType);
            if ((result.Length == 0) || (result.All(x => x.CodeValue != codeValue)))
            {
                environmentService.Account().AccountCodesAdd(new A01cAccountCodes
                {
                    AccountNumber = accountNumber,
                    Active = true,
                    CodeType = codeType,
                    CodeValue = codeValue,
                    DataSource = "INET"
                });
            }
        }
        public List<DSAccountCode> GetAccountCodes(string environment, long accountNumber, bool activeOnly)
        {
            var environmentService = internalService.ServiceAccess(environment);

            List<DSAccountCode> result = new List<DSAccountCode>();

            var accountCodes = environmentService.Account().AccountCodesSearchByAccountNumber(accountNumber, activeOnly ? "Y" : string.Empty, string.Empty, PagingContext.AllDataPaging()).ToList();

            if (accountCodes.Count > 0)
            {
                foreach (var ac in accountCodes)
                {
                    if (activeOnly && ac.Active.GetValueOrDefault())
                        result.Add(new DSAccountCode { Active = true, DataSource = ac.DataSource, RecordId = ac.RecordId.GetValueOrDefault(), Type = ac.CodeType, Value = ac.CodeValue });

                    if (!activeOnly)
                        result.Add(new DSAccountCode { Active = ac.Active.GetValueOrDefault(), RecordId = ac.RecordId.GetValueOrDefault(), DataSource = ac.DataSource, Type = ac.CodeType, Value = ac.CodeValue });
                }
            }

            return result;
        }




        private JHMTransaction GetTransactionByDocumentNumber(string enviroment, long? documentNumber)
        {
            var environmentService = internalService.ServiceAccess(enviroment);

            var tm = environmentService.Account().TransactionMasterSearchByDocumentNumber(documentNumber.GetValueOrDefault());
            var tData = environmentService.Account().TransactionMasterContainsDataByCriteria(documentNumber.GetValueOrDefault(), tm.ResponseId.GetValueOrDefault());
            decimal calculatedAmount = 0;
            var jhmTransaction = new JHMTransaction
            {
                AccountNumber = tm.AccountNumber.GetValueOrDefault(),
                DocumentNumber = tm.DocumentNumber.GetValueOrDefault(),
                Orders = new List<Order>(),
                Donations = new List<Donation>(),
                TotalAmount = tm.Amount.GetValueOrDefault(),
                Date = tm.Date.Value
            };

            if (tData.HasOrders.GetValueOrDefault())
            {
                var tOrders = environmentService.Account().OrdersSearchByDocumentNumber(tm.DocumentNumber.GetValueOrDefault()).ToList();

                foreach (var to in tOrders)
                {
                    var tOrderDetails = environmentService.Account().OrderDetailsSearchByOrderId(to.OrderId.GetValueOrDefault()).ToList();

                    Order order = new Order
                    {
                        OrderId = to.OrderId.GetValueOrDefault(),
                        ShippingAmount = to.ShippingAmount.GetValueOrDefault(),
                        ShippingMethod = to.ShipMethod,
                        OrderDetails = new List<OrderDetail>()
                    };

                    foreach (var tod in tOrderDetails)
                    {
                        var productDescription = environmentService.Inventory().ProductDescription(tod.ProductCode, string.Empty);
                        var statusDate = tod.OrderStatus == DSConstants.OrderStatus.Backordered
                                             ? tod.BackorderDate
                                             : tod.OrderStatus == DSConstants.OrderStatus.Cancelled
                                                   ? tod.CancelDate
                                                   : tod.CommitDate;

                        var orderDetail = new OrderDetail
                        {
                            ProductCode = tod.ProductCode,
                            ProductDescription = productDescription,
                            Price = tod.Price.GetValueOrDefault(),
                            Quantity = tod.Quantity.GetValueOrDefault(),
                            ExtendedAmount = tod.Price.GetValueOrDefault() * tod.Quantity.GetValueOrDefault(),
                            Status = tod.OrderStatus,
                            StatusDate = statusDate
                        };

                        calculatedAmount += orderDetail.ExtendedAmount;

                        order.OrderDetails.Add(orderDetail);
                    }
                    calculatedAmount += order.ShippingAmount;

                    jhmTransaction.Orders.Add(order);
                }
            }

            if (tData.HasDonations.GetValueOrDefault())
            {
                var tDonations = environmentService.Account().GiftDetailsSearchByDocumentNumber(tm.DocumentNumber.GetValueOrDefault(), PagingContext.AllDataPaging());
                foreach (var td in tDonations)
                {
                    var donation = new Donation
                    {
                        Amount = td.TotalAmount.GetValueOrDefault(),
                        ProjectCode = td.ProjectCode,
                        ProjectDescription = td.ProjectDescription
                    };

                    calculatedAmount += donation.Amount;

                    jhmTransaction.Donations.Add(donation);
                }

            }

            if (tData.HasSubscriptions.GetValueOrDefault())
            {
                var tSubscriptions = environmentService.Account().SubscriptionHeadersSearchByDocumentNumber(tm.DocumentNumber.GetValueOrDefault(), PagingContext.AllDataPaging());
                foreach (var td in tSubscriptions)
                {
                    var sub = new DsUserSubscription
                    {
                        SubscriptionCost = td.TotalAmount.GetValueOrDefault(),
                        SubscriptionCode = td.SubscriptionCode,
                        SubscriptionStatus = td.SubscriptionStatus
                    };

                    calculatedAmount += sub.SubscriptionCost;

                    jhmTransaction.Subscriptions.Add(sub);
                }

            }

            jhmTransaction.CalculatedAmount = calculatedAmount;

            if (tData.HasDonations.GetValueOrDefault() || tData.HasOrders.GetValueOrDefault() || tData.HasSubscriptions.GetValueOrDefault())
                return jhmTransaction;

            return null;
        }
        private S01_SubscriptionHeaders CreateSubscriptionHeader(DsUserSubscription userSubscription)
        {
            return new S01_SubscriptionHeaders
            {
                AccountNumber = userSubscription.AccountNumber,
                DeliveryId = userSubscription.DeliveryAddressId,
                NumberOfIssues = userSubscription.NumberOfIssues,
                NumberRemaining = userSubscription.NumberOfIssuesRemaining,

                PriceCode = userSubscription.PriceCode,
                StartDate = (userSubscription.StartDate ?? DateTime.Now).Date,
                SubscriptionCode = userSubscription.SubscriptionCode,
                SubscriptionStatus = userSubscription.SubscriptionStatus,
                SourceCode = "WEB",
                FulfillmentMethod = "MAIL",
                RecordId = userSubscription.DsRecordId,
                SubscriptionId = userSubscription.SubscriptionId,
                DeliveryTableId = "A03",
                TotalAmount = userSubscription.SubscriptionCost,
                RemainingDeferredAmount = 0,
                CopyCount = 1,
                NeverExpires = false
            };
        }
        

        /*public long CreateAccountSubscription(string environment, long accountNumber)
    {
        var environmentService = internalService.ServiceAccess(environment);
        try
        {
            long subscriptionId;
            environmentService.Account().AccountSubscriptionsAdd(out subscriptionId, new S01_SubscriptionHeaders()
                {
                    AccountNumber = accountNumber,

                });


            var allAddresses =
                environmentService.Account().AccountAddressesSearchByCriteria(new AccountAddressSearchCriteria
                {
                    AccountNumber = accountNumber,
                    ActiveOnly = 'Y',
                    PrimaryOnly = dsAddress.IsPrimary ? 'Y' : 'N'
                },
                                                                              PagingContext.AllDataPaging());
            long addressId = allAddresses.OrderByDescending(x => x.RecordId.GetValueOrDefault()).Select(x => x.AddressId).
                FirstOrDefault().GetValueOrDefault();

            if (addressId > 0)
                return addressId;

            return -2;
        }
        catch (Exception)
        {
            return -1;
        }
    }*/

    }
}