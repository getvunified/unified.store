﻿using System.Collections.Generic;
using D2DNext.AB_CSA;

namespace Jhm.DonorStudio.Services
{
    public class DSInternalService : IDSInternalService
    {
       
        private Dictionary<string, ServiceAccessBase> activeEnvironments = new Dictionary<string, ServiceAccessBase>();

        public DSInternalService(InternalServiceAccessConfig config)
        {
            Init(config);
        }

        private void Init(InternalServiceAccessConfig config)
        {
            foreach (string environment in config.Environments)
            {
                activeEnvironments.Remove(environment);

                var serviceAccess = InternalServiceAccess.CreateInstance();
                serviceAccess.Login(environment);
                activeEnvironments.Add(environment, serviceAccess);
            }
        }


        public ServiceAccessBase ServiceAccess(string environment)
        {
            return activeEnvironments[environment];
        }
    }
}