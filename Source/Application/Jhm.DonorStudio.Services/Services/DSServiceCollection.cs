﻿using Jhm.DonorStudio.Services.Interfaces;
using StructureMap;
using getv.donorstudio;

namespace Jhm.DonorStudio.Services.Services
{
    public class DSServiceCollection : IDSServiceCollection
    {
        private readonly IContainer container;

        public DSServiceCollection(IContainer container)
        {
            this.container = container;
        }

        public IDsInventoryService DsInventoryService
        {
            get { return container.GetInstance<DsInventoryService>(); }
        }

        public IDsAccountTransactionService DsAccountTransactionService
        {
            get { return container.GetInstance<DsAccountTransactionService>(); }
        }

        //IDsSystemAdministrationService
        public IDsSystemAdministrationService DsSystemAdministrationService
        {
            get { return container.GetInstance<DsSystemAdministrationService>(); }
        }


    }
}
