using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using D2DNext.AB_CMN;
using D2DNext.AB_CMN.SearchCriteria;
using D2DNext.AB_DL.BusinessDb;
using Jhm.Common.Framework.Extensions;
using Jhm.DonorStudio.Services.Config;

namespace Jhm.DonorStudio.Services
{

    public class DSProductService : IDSProductService
    {
        private readonly IDSInternalService internalService;

        public DSProductService(IDSInternalService internalService)
        {
            this.internalService = internalService;
        }

        /// <summary>
        /// Gets products
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="author"></param>
        /// <param name="description"></param>
        /// <param name="productCode"></param>
        /// <param name="title"></param>
        /// <param name="priceMin"></param>
        /// <param name="priceMax"></param>
        /// <param name="minimumReleaseDate"></param>
        /// <param name="category"></param>
        /// <param name="subject"></param>
        /// <param name="mediaType"></param>
        /// <param name="availableOnly">Products that have at least one item available</param>
        /// <param name="onSaleOnly">Products with web sale price of at least one item less than the standard price</param>
        /// <param name="pageNumber"></param>
        /// <param name="maxRowsOnPage"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public List<JHMProduct> GetProducts(string environment, string author, string description, string productCode, string title, decimal priceMin, decimal priceMax, DateTime? minimumReleaseDate,
            string category, string subject, string mediaType, bool availableOnly, bool onSaleOnly, int pageNumber, int maxRowsOnPage, ref int totalRows)
        {
            List<JHMProduct> results = new List<JHMProduct>();

            var environmentService = internalService.ServiceAccess(environment);

            var products = environmentService.Inventory().ProductMasterSearchByCriteria(new ProductMasterLookupCriteria
                                                                             {
                                                                                 //Author = author,
                                                                                 Description = description,
                                                                                 ProductCode = productCode,
                                                                                 Status = "A"
                                                                             },
                                                                         PagingContext.AllDataPaging()).Where(x => x.UseInShoppingCart.GetValueOrDefault());

            if (products.Count() == 0)
                return results;
            
            foreach (var p in products)
            {
                var pNotes = environmentService.Inventory().ProductNotesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                string rootProductCode = string.Empty;
                string pTitle = string.Empty;

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).Count() > 0)
                    rootProductCode = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).FirstOrDefault().ShortComment;

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).Count() > 0)
                    pTitle = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).FirstOrDefault().ShortComment;


                if (string.IsNullOrEmpty(rootProductCode))
                    continue;

                var pCodes = environmentService.Inventory().ProductCodesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());
                var pDates = environmentService.Inventory().ProductDatesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());


                var pWarehouses = environmentService.Inventory().ProductWarehousesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());
                var pWarehouseCode = DSConstants.GetWarehouseFromList(environment, pWarehouses);
                var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
                        new ProductWarehousesLookupCriteria
                        {
                            ProductCode = p.ProductCode,
                            WarehouseCode = pWarehouseCode
                        });


                //var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
                //    new ProductWarehousesLookupCriteria
                //        {
                //            ProductCode = p.ProductCode,
                //            WarehouseCode = DSConstants.GetWarehouse(environment)
                //            });


                //NOTE:  Added this method call (Has Const Company and Warehouse) To get Quantity Because Warehouse show 0 for kits
                var quantityOnHand = environmentService.Inventory().ProductQuantity(p.ProductCode, pWarehouseCode, DSConstants.GetCompany(environment));
                //var quantityOnHand = environmentService.Inventory().ProductQuantity(p.ProductCode, DSConstants.GetWarehouse(environment), DSConstants.GetCompany(environment));

                var pPrices = environmentService.Inventory().ProductPricesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                var pAttachments = environmentService.Inventory().ProductAttachmentsSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                Item jhmProductItem = new Item()
                                            {
                                                Description = p.Description,
                                                ProductCode = p.ProductCode,
                                                BasePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.Base).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                                                SalePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.WebSale).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                                                IsInHouse = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.InHouse).Select(x => x.CodeValue == "Y").FirstOrDefault(),
                                                MediaType = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.MediaType).Select(x => x.CodeValue).FirstOrDefault(),
                                                ReleaseDate = pDates.Where(x => x.DateType == DSConstants.DateTypes.ReleaseDate).Select(x => x.DateValue).FirstOrDefault(),
                                                AllowBackorder = (pWarehouse != null) ? pWarehouse.AllowBackorder.GetValueOrDefault() : false,
                                                QuantityInStock = quantityOnHand,
                                                Link = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.ProductLink).Select(x => x.ExternalDocumentAddress).FirstOrDefault()
                                            };

                var rootProduct = results.Where(x => x.RootProductCode == rootProductCode).FirstOrDefault();

                if (rootProduct != null)
                {
                    rootProduct.Items.Add(jhmProductItem);

                    rootProduct.FeaturedOnTelevision = (rootProduct.FeaturedOnTelevision ||
                        pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                            x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnTelevision));

                    rootProduct.FeaturedOnGetv = (rootProduct.FeaturedOnGetv ||
                        pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                            x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnGetv));



                    if (string.IsNullOrEmpty(rootProduct.Author))
                    {
                        rootProduct.Author = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValue).FirstOrDefault();
                        rootProduct.AuthorDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValueDescription).FirstOrDefault();
                    }
                    
                    if (string.IsNullOrEmpty(rootProduct.Publisher))
                    {
                        rootProduct.Publisher = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValue).FirstOrDefault();
                        rootProduct.PublisherDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValueDescription).FirstOrDefault();
                    }


                    if (string.IsNullOrEmpty(rootProduct.Description))
                        rootProduct.Description = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault();

                    if (string.IsNullOrEmpty(rootProduct.Title))
                        rootProduct.Title = pTitle;
                        //rootProduct.Title = p.Description;

                    if (string.IsNullOrEmpty(rootProduct.Trailer))
                        rootProduct.Trailer = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Trailer).Select(x => x.ExternalDocumentAddress).FirstOrDefault();

                    var newCategories = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Category && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                    if (newCategories.Count > 0)
                        rootProduct.Categories.AddRange(newCategories);

                    var newSubjects = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Subject && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                    if (newSubjects.Count > 0)
                        rootProduct.Subjects.AddRange(newSubjects);

                    var newRelatedProductCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProduct && !rootProduct.RelatedProducts.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                    if (newRelatedProductCodes.Count > 0)
                    {
                        foreach (var rpc in newRelatedProductCodes)
                        {
                            var relatedProduct = GetRelatedProduct(environment, rpc);
                            if (relatedProduct != null)
                                rootProduct.RelatedProducts.Add(relatedProduct);
                        }
                    }

                    var newRelatedProjectCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProject && !rootProduct.RelatedProjects.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                    if (newRelatedProjectCodes.Count > 0)
                    {
                        foreach (var rpc in newRelatedProjectCodes)
                        {
                            RelatedProject relatedProject = new RelatedProject { Code = rpc };
                            var rp = environmentService.FinancialManagement().ProjectMasterSearchByProjectCode(rpc);
                            relatedProject.Description = rp != null ? rp.Description : string.Empty;
                            rootProduct.RelatedProjects.Add(relatedProject);
                        }
                    }
                }
                // Create a new root product, add the current analyzed DS product as the first stock item
                else
                {
                    var jhmProduct = new JHMProduct
                                                {
                                                    RootProductCode = rootProductCode,
                                                    
                                                    FeaturedOnTelevision = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb)
                                                                                .Any(x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnTelevision),
                                                    FeaturedOnGetv = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb)
                                                                                .Any(x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnGetv),

                                                    Author = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValue).FirstOrDefault(),
                                                    AuthorDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValueDescription).FirstOrDefault(),
                                                    
                                                    Description = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault(),
                                                    Title = pTitle,
                                                    //Title = p.Description,
                                                    Trailer = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Trailer).Select(x => x.ExternalDocumentAddress).FirstOrDefault(),

                                                    Publisher = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValue).FirstOrDefault(),
                                                    PublisherDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValueDescription).FirstOrDefault(),
                                                    
                                                    Images = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Image).Select(x => x.ExternalDocumentAddress).ToList(),
                                                    Categories = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Category).Select(x => x.CodeValue).ToList(),
                                                    Subjects = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Subject).Select(x => x.CodeValue).ToList(),
                                                    Items = new List<Item>(),
                                                    RelatedProjects = new List<RelatedProject>(),
                                                    RelatedProducts = new List<RelatedProduct>()
                                                };

                    jhmProduct.Items.Add(jhmProductItem);

                    var relatedProductCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProduct).Select(x => x.ShortComment).ToList();
                    if (relatedProductCodes.Count > 0)
                    {
                        foreach (var rpc in relatedProductCodes)
                        {
                            var relatedProduct = GetRelatedProduct(environment, rpc);
                            if (relatedProduct != null)
                                jhmProduct.RelatedProducts.Add(relatedProduct);
                        }
                    }

                    var relatedProjectCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProject).Select(x => x.ShortComment).ToList();
                    if (relatedProjectCodes.Count > 0)
                    {
                        foreach (var rpc in relatedProjectCodes)
                        {
                            RelatedProject relatedProject = new RelatedProject { Code = rpc };
                            var rp = environmentService.FinancialManagement().ProjectMasterSearchByProjectCode(rpc);

                            if (rp != null)
                            {
                                relatedProject.Description = rp.Description;
                                jhmProduct.RelatedProjects.Add(relatedProject);
                            }
                        }
                    }


                    results.Add(jhmProduct);
                }
            }

            var filteredList = new List<JHMProduct>();
            filteredList.AddRange(results);


            if (!string.IsNullOrEmpty(title))
            {
                filteredList = filteredList.Where(x => x.Title.Contains(title)).ToList();
            }

            if (priceMin > 0)
            {
                //Keep products that have at least one item greater than the minimum price
                filteredList = filteredList.Where(x => x.Items.Where(y => y.SalePrice > priceMin).Count() > 0).ToList();
            }

            if (priceMax > 0)
            {
                //Keep products that have at least one item priced lower than the maximum price;
                filteredList = filteredList.Where(x => x.Items.Where(y => y.SalePrice < priceMax).Count() > 0).ToList();
            }

            // Remove products that are to be released in the future
            filteredList = filteredList.Where(x => x.Items.Where(y => y.ReleaseDate.GetValueOrDefault().CompareTo(DateTime.Today) <= 0).Count() > 0).ToList();

            if (minimumReleaseDate != null && minimumReleaseDate > new DateTime())
            {
                filteredList = filteredList.Where(x => x.Items.Where(y => y.ReleaseDate.GetValueOrDefault().CompareTo(minimumReleaseDate) >= 0).Count() > 0).ToList();
            }

            if (!string.IsNullOrEmpty(category))
            {
                //Keep products that contain the category requested
                filteredList = filteredList.Where(x => x.Categories.Contains(category)).ToList();
            }

            if (!string.IsNullOrEmpty(mediaType))
            {
                //Keep products that contain at least one item of the media type requested
                filteredList = filteredList.Where(x => x.Items.Where(y => y.MediaType == mediaType).Count() > 0).ToList();
            }

            if (availableOnly)
            {
                //Keep only available products (at least one item is available)43
                filteredList = filteredList.Where(x => x.Items.Where(y => y.QuantityInStock > 0 || y.IsInHouse).Count() > 0).ToList();
            }

            if (onSaleOnly)
            {
                //Keep only products that have the web sale price of at least one item less than the standard price
                filteredList = filteredList.Where(x => x.Items.Where(y => y.SalePrice < y.BasePrice).Count() > 0).ToList();
            }

            results = filteredList.Page(pageNumber, maxRowsOnPage).ToList();

            totalRows = filteredList.Count;

            return results;
        }

        public List<JHMProduct> GetAllProducts(string environment)
        {
            List<JHMProduct> results = new List<JHMProduct>();

            var environmentService = internalService.ServiceAccess(environment);

            var products = environmentService.Inventory().ProductMasterSearchByCriteria(new ProductMasterLookupCriteria
            {
                Status = "A"
            },
            PagingContext.AllDataPaging()).Where(x => x.UseInShoppingCart.GetValueOrDefault());

            if (products.Count() == 0)
                return results;

            

            foreach (var p in products)
            {
                var pNotes = environmentService.Inventory().ProductNotesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                string rootProductCode = string.Empty;
                string pTitle = string.Empty;

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).Count() > 0)
                    rootProductCode = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).FirstOrDefault().ShortComment;

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).Count() > 0)
                    pTitle = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).FirstOrDefault().ShortComment;


                if (string.IsNullOrEmpty(rootProductCode))
                    continue;

                var pCodes = environmentService.Inventory().ProductCodesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());
                var pDates = environmentService.Inventory().ProductDatesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());







                var pWarehouses = environmentService.Inventory().ProductWarehousesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());
                var pWarehouseCode = DSConstants.GetWarehouseFromList(environment, pWarehouses);
                var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
                        new ProductWarehousesLookupCriteria
                        {
                            ProductCode = p.ProductCode,
                            WarehouseCode = pWarehouseCode
                        });



  //var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
  //                      new ProductWarehousesLookupCriteria
  //                      {
  //                          ProductCode = p.ProductCode,
  //                          WarehouseCode = DSConstants.GetWarehouse(environment)
  //                      });

                

                var quantityOnHand = environmentService.Inventory().ProductQuantity(p.ProductCode, pWarehouseCode, DSConstants.GetCompany(environment));
                
                var pPrices = environmentService.Inventory().ProductPricesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                var pAttachments = environmentService.Inventory().ProductAttachmentsSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                Item jhmProductItem = new Item()
                {
                    Description = p.Description,
                    ProductCode = p.ProductCode,
                    BasePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.Base).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                    SalePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.WebSale).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                    IsInHouse = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.InHouse).Select(x => x.CodeValue == "Y").FirstOrDefault(),
                    MediaType = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.MediaType).Select(x => x.CodeValue).FirstOrDefault(),
                    ReleaseDate = pDates.Where(x => x.DateType == DSConstants.DateTypes.ReleaseDate).Select(x => x.DateValue).FirstOrDefault(),
                    AllowBackorder = (pWarehouse != null) ? pWarehouse.AllowBackorder.GetValueOrDefault() : false,
                    QuantityInStock = quantityOnHand,
                    Link = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.ProductLink).Select(x => x.ExternalDocumentAddress).FirstOrDefault(),
                    WarehouseCode = pWarehouseCode
                };

                var rootProduct = results.Where(x => x.RootProductCode == rootProductCode).FirstOrDefault();

                if (rootProduct != null)
                {
                    rootProduct.Items.Add(jhmProductItem);

                    rootProduct.FeaturedOnTelevision = (rootProduct.FeaturedOnTelevision ||
                        pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                            x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnTelevision));

                    rootProduct.FeaturedOnGetv = (rootProduct.FeaturedOnGetv ||
                        pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                            x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnGetv));

                    if (string.IsNullOrEmpty(rootProduct.Author))
                    {
                        rootProduct.Author = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValue).FirstOrDefault();
                        rootProduct.AuthorDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValueDescription).FirstOrDefault();
                    }

                    if (string.IsNullOrEmpty(rootProduct.Description))
                        rootProduct.Description = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault();

                    if (string.IsNullOrEmpty(rootProduct.Title))
                        //rootProduct.Title = p.Description;
                        rootProduct.Title = pTitle;


                    if (string.IsNullOrEmpty(rootProduct.Publisher))
                    {
                        rootProduct.Publisher = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValue).FirstOrDefault();
                        rootProduct.PublisherDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValueDescription).FirstOrDefault();

                    }


                    if (string.IsNullOrEmpty(rootProduct.Trailer))
                        rootProduct.Trailer = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Trailer).Select(x => x.ExternalDocumentAddress).FirstOrDefault();

                    var newCategories = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Category && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                    if (newCategories.Count > 0)
                        rootProduct.Categories.AddRange(newCategories);

                    var newSubjects = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Subject && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                    if (newSubjects.Count > 0)
                        rootProduct.Subjects.AddRange(newSubjects);

                    var newRelatedProductCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProduct && rootProduct.RelatedProducts.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                    if (newRelatedProductCodes.Count > 0)
                    {
                        foreach (var rpc in newRelatedProductCodes)
                        {
                            var relatedProduct = GetRelatedProduct(environment, rpc);
                            if (relatedProduct != null)
                                rootProduct.RelatedProducts.Add(relatedProduct);
                        }
                    }

                    var newRelatedProjectCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProject && !rootProduct.RelatedProjects.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                    if (newRelatedProjectCodes.Count > 0)
                    {
                        foreach (var rpc in newRelatedProjectCodes)
                        {
                            RelatedProject relatedProject = new RelatedProject { Code = rpc };
                            var rp = environmentService.FinancialManagement().ProjectMasterSearchByProjectCode(rpc);
                            relatedProject.Description = rp != null ? rp.Description : string.Empty;
                            rootProduct.RelatedProjects.Add(relatedProject);
                        }
                    }
                }
                // Create a new root product, add the current analyzed DS product as the first stock item
                else
                {
                    var fileLength = "0.0";
                    if(pNotes.FirstOrDefault(x => x.NoteType == DSConstants.NoteTypes.FileLength) != null)
                    {
                        fileLength =
                            pNotes.FirstOrDefault(x => x.NoteType == DSConstants.NoteTypes.FileLength).ShortComment.ToString();
                    }
                    var jhmProduct = new JHMProduct
                    {
                        RootProductCode = rootProductCode,

                        FeaturedOnTelevision = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb)
                            .Any(x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnTelevision),
                        FeaturedOnGetv = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb)
                                                    .Any(x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnGetv),



                        Author = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValue).FirstOrDefault(),
                        AuthorDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValueDescription).FirstOrDefault(),
                        Description = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault(),
                        //Title = p.Description,
                        Title = pTitle,
                        
                        
                        Trailer = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Trailer).Select(x => x.ExternalDocumentAddress).FirstOrDefault(),
                        Publisher = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValue).FirstOrDefault(),
                        PublisherDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValueDescription).FirstOrDefault(),
                        Images = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Image).Select(x => x.ExternalDocumentAddress).ToList(),
                        Categories = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Category).Select(x => x.CodeValue).ToList(),
                        Subjects = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Subject).Select(x => x.CodeValue).ToList(),
                        Items = new List<Item>(),
                        RelatedProjects = new List<RelatedProject>(),
                        RelatedProducts = new List<RelatedProduct>(),
                        FileLength = decimal.Parse(fileLength),
                        RecordId = p.RecordId.GetValueOrDefault(),
                        Isbn = p.ISBN,
                        DsType = p.Type,
                        Status = p.Status,
                        Packaging = p.Packaging,
                        UseInShoppingCart = p.UseInShoppingCart.GetValueOrDefault(),
                        DiscountAllowed = p.DiscountAllowed.GetValueOrDefault(),
                        Code = p.ProductCode,
                        

                        

                    };

                    jhmProduct.Items.Add(jhmProductItem);

                    var relatedProductCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProduct).Select(x => x.ShortComment).ToList();
                    if (relatedProductCodes.Count > 0)
                    {
                        foreach (var rpc in relatedProductCodes)
                        {
                            var relatedProduct = GetRelatedProduct(environment, rpc);
                            if (relatedProduct != null)
                                jhmProduct.RelatedProducts.Add(relatedProduct);
                        }
                    }

                    var relatedProjectCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProject).Select(x => x.ShortComment).ToList();
                    if (relatedProjectCodes.Count > 0)
                    {
                        foreach (var rpc in relatedProjectCodes)
                        {
                            RelatedProject relatedProject = new RelatedProject { Code = rpc };
                            var rp = environmentService.FinancialManagement().ProjectMasterSearchByProjectCode(rpc);

                            if (rp != null)
                            {
                                relatedProject.Description = rp.Description;
                                jhmProduct.RelatedProjects.Add(relatedProject);
                            }
                        }
                    }


                    results.Add(jhmProduct);
                }
            }

            var filteredList = new List<JHMProduct>();
            filteredList.AddRange(results);

            // Remove products that are to be released in the future
            filteredList = filteredList.Where(x => x.Items.Where(y => y.ReleaseDate.GetValueOrDefault().CompareTo(DateTime.Today) <= 0).Count() > 0).ToList();

            //return results;
            return filteredList;
        }

        public I01aProductNotes GetMp3ProductNote(string environment, string rootProductCode)
        {
            if (string.IsNullOrEmpty(rootProductCode))
                return null;

            var environmentService = internalService.ServiceAccess(environment);

            var pagingContext = PagingContext.AllDataPaging();
 
            var product = environmentService.Inventory().ProductNotesSearchByProductCode(rootProductCode, pagingContext);

            var selectNote = product.Where(note=>note.NoteType == "WEBFILELEN");

            var webFileLen = selectNote.FirstOrDefault();
            I01aProductNotes masterNote;

            if (webFileLen != null)
            {
                masterNote = new I01aProductNotes
                {
                    LongComment = webFileLen.LongComment,
                    NoteType = webFileLen.NoteType,
                    ProductCode = webFileLen.ProductCode,
                    RecordId = long.Parse(webFileLen.RecordId.ToString()),
                    ShortComment = webFileLen.ShortComment,
                };
            }
            else
            {
                masterNote = new I01aProductNotes
                {
                    LongComment = "",
                    NoteType = "",
                    ProductCode = "",
                    RecordId = 0,
                    ShortComment = "",
                };
            }
                
                return masterNote;
        }
        
        public JHMProduct GetProduct(string environment, string rootProductCode)
        {
            if (string.IsNullOrEmpty(rootProductCode))
                return null;

            var environmentService = internalService.ServiceAccess(environment);

            var products = environmentService.Inventory().ProductMasterSearchByCriteria(new ProductMasterLookupCriteria
                                                                                                {
                                                                                                    ProductCode = rootProductCode,
                                                                                                    Status = "A"
                                                                                                },
                                                                                                PagingContext.AllDataPaging()).Where(x => x.UseInShoppingCart.GetValueOrDefault());

            if (products.Count() == 0)
                return null;

            var rootProduct = new JHMProduct
            {
                RootProductCode = rootProductCode,
                Images = new List<string>(),
                Categories = new List<string>(),
                Subjects = new List<string>(),
                Items = new List<Item>(),
                RelatedProjects = new List<RelatedProject>(),
                RelatedProducts = new List<RelatedProduct>()
            };

            foreach (var p in products)
            {
                var pNotes = environmentService.Inventory().ProductNotesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                var pRoot = string.Empty;
                var pTitle = string.Empty;

              

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).Count() > 0)
                    pRoot = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).FirstOrDefault().ShortComment;

                if (pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).Count() > 0)
                    pTitle = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).FirstOrDefault().ShortComment;


                if (pRoot != rootProductCode)
                    continue;

                var pCodes = environmentService.Inventory().ProductCodesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());
                var pDates = environmentService.Inventory().ProductDatesSearchByProductCode(p.ProductCode, "Y", "M", PagingContext.AllDataPaging());



                var pWarehouses = environmentService.Inventory().ProductWarehousesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());
                var pWarehouseCode = DSConstants.GetWarehouseFromList(environment, pWarehouses);
                var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
                        new ProductWarehousesLookupCriteria
                        {
                            ProductCode = p.ProductCode,
                            WarehouseCode = pWarehouseCode
                        });


                
                //var pWarehouse = environmentService.Inventory().ProductWarehousesSearchByProductCodeAndWarehouse(
                //        new ProductWarehousesLookupCriteria
                //        {
                //            ProductCode = p.ProductCode,
                //            WarehouseCode = DSConstants.GetWarehouse(environment)
                //        });

                var quantityOnHand = environmentService.Inventory().ProductQuantity(p.ProductCode, pWarehouseCode, DSConstants.GetCompany(environment));
                //var quantityOnHand = environmentService.Inventory().ProductQuantity(p.ProductCode, DSConstants.GetWarehouse(environment), DSConstants.GetCompany(environment));
                
                var pPrices = environmentService.Inventory().ProductPricesSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                var pAttachments = environmentService.Inventory().ProductAttachmentsSearchByProductCode(p.ProductCode, PagingContext.AllDataPaging());

                Item jhmProductItem = new Item()
                {
                    Description = p.Description,
                    ProductCode = p.ProductCode,
                    BasePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.Base).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                    SalePrice = (pPrices.Length > 0) ? pPrices.Where(x => x.PriceCode == DSConstants.PriceCodes.WebSale).Select(x => x.Price).FirstOrDefault().GetValueOrDefault() : -1,
                    IsInHouse = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.InHouse).Select(x => x.CodeValue == "Y" ? true : false).FirstOrDefault(),
                    MediaType = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.MediaType).Select(x => x.CodeValue).FirstOrDefault(),
                    ReleaseDate = pDates.Where(x => x.DateType == DSConstants.DateTypes.ReleaseDate).Select(x => x.DateValue).FirstOrDefault(),
                    AllowBackorder = (pWarehouse != null) ? pWarehouse.AllowBackorder.GetValueOrDefault() : false,
                    QuantityInStock = quantityOnHand,
                    Link = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.ProductLink).Select(x => x.ExternalDocumentAddress).FirstOrDefault()
                };

                rootProduct.Items.Add(jhmProductItem);

                rootProduct.FeaturedOnTelevision = (rootProduct.FeaturedOnTelevision || 
                        pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                            x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnTelevision));

                rootProduct.FeaturedOnGetv = (rootProduct.FeaturedOnGetv || 
                    pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.FeaturedOnWeb).Any(
                        x => x.CodeValue == DSConstants.CodeTypes.FeaturedOnGetv));


                if (string.IsNullOrEmpty(rootProduct.Author))
                {
                    rootProduct.Author = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValue).FirstOrDefault();
                    rootProduct.AuthorDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Author).Select(x => x.CodeValueDescription).FirstOrDefault();
                }

                if (string.IsNullOrEmpty(rootProduct.Description))
                {
                    rootProduct.Description = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault();
                }
                if (string.IsNullOrEmpty(rootProduct.Title))
                {
                    rootProduct.Title = pTitle;
                    //rootProduct.Title = p.Description;
                }

                if (string.IsNullOrEmpty(rootProduct.Publisher))
                {
                    rootProduct.Publisher = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValue).FirstOrDefault();
                    rootProduct.PublisherDescription = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Publisher).Select(x => x.CodeValueDescription).FirstOrDefault();
                }

                if (string.IsNullOrEmpty(rootProduct.Trailer))
                    rootProduct.Trailer = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Trailer).Select(x => x.ExternalDocumentAddress).FirstOrDefault();

                var newImages = pAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Image).Select(x => x.ExternalDocumentAddress).ToList();
                if(newImages.Count > 0)
                    rootProduct.Images.AddRange(newImages);

                var newCategories = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Category && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                if (newCategories.Count > 0)
                    rootProduct.Categories.AddRange(newCategories);

                var newSubjects = pCodes.Where(x => x.CodeType == DSConstants.CodeTypes.Subject && !rootProduct.Categories.Contains(x.CodeValue)).Select(x => x.CodeValue).ToList();
                if (newSubjects.Count > 0)
                    rootProduct.Subjects.AddRange(newSubjects);

                var newRelatedProductCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProduct && !rootProduct.RelatedProducts.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                if (newRelatedProductCodes.Count > 0)
                {
                    foreach (var rpc in newRelatedProductCodes)
                    {
                        var relatedProduct = GetRelatedProduct(environment, rpc);
                        rootProduct.RelatedProducts.Add(relatedProduct);
                    }
                }

                var newRelatedProjectCodes = pNotes.Where(x => x.NoteType == DSConstants.NoteTypes.RelatedProject && !rootProduct.RelatedProjects.Select(y => y.Code).Contains(x.ShortComment)).Select(x => x.ShortComment).ToList();
                if (newRelatedProjectCodes.Count > 0)
                {
                    foreach (var rpc in newRelatedProjectCodes)
                    {
                        RelatedProject relatedProject = new RelatedProject { Code = rpc };
                        var rp = environmentService.FinancialManagement().ProjectMasterSearchByProjectCode(rpc);
                        relatedProject.Description = rp != null ? rp.Description : string.Empty;
                        rootProduct.RelatedProjects.Add(relatedProject);
                    }
                }

            }

            return rootProduct;
        }

        public void UpdateProductNoteFileLength(string environment, string productCode, decimal weight)
        {
            var environmentService = internalService.ServiceAccess(environment);

            var product = GetMp3ProductNote(environment, productCode);

            product.ShortComment = ( Math.Round((weight / 1024) / 1024,2) ).ToString(CultureInfo.InvariantCulture);

            environmentService.Inventory().ProductNotesUpdate(product);

        }

        #region Helpers

        private RelatedProduct GetRelatedProduct(string environment, string rpc)
        {
            var environmentService = internalService.ServiceAccess(environment);

            RelatedProduct relatedProduct = new RelatedProduct { Code = rpc };

            var rps = environmentService.Inventory().ProductMasterSearchByCriteria(
                                                        new ProductMasterLookupCriteria
                                                        {
                                                            ProductCode = rpc,
                                                            Status = "A"
                                                        }, PagingContext.AllDataPaging());

            //If no I01 entries were found for this, then it's not findable
            if (rps.Count() == 0)
                return null;

            foreach (var rp in rps)
            {
                if (!rp.UseInShoppingCart.GetValueOrDefault())
                    continue;

                var rpNotes = environmentService.Inventory().ProductNotesSearchByProductCode(rp.ProductCode, PagingContext.AllDataPaging());

                var pTitle = string.Empty;
                if (rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).Count() > 0)
                    pTitle = rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Title).FirstOrDefault().ShortComment;

                //If no root elements were found for this product
                if (rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).Count() == 0)
                    continue;

                //If root is not equal to the entry parameter
                if (rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Root).Select(x => x.ShortComment).FirstOrDefault() != rpc)
                    continue;

                if (!string.IsNullOrEmpty(relatedProduct.Description) && !string.IsNullOrEmpty(relatedProduct.Image) && !string.IsNullOrEmpty(relatedProduct.Title))
                    break;

                if (string.IsNullOrEmpty(relatedProduct.Description))
                {
                    if (rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Count() > 0)
                        relatedProduct.Description = rpNotes.Where(x => x.NoteType == DSConstants.NoteTypes.Description).Select(x => x.LongComment).FirstOrDefault();
                }

                if (string.IsNullOrEmpty(relatedProduct.Image))
                {
                    var rpAttachments = environmentService.Inventory().ProductAttachmentsSearchByProductCode(rp.ProductCode, PagingContext.AllDataPaging());

                    if (rpAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Image).Count() > 0)
                        relatedProduct.Image = rpAttachments.Where(x => x.AttachmentType == DSConstants.AttachmentTypes.Image).Select(x => x.ExternalDocumentAddress).FirstOrDefault();
                }

                if (string.IsNullOrEmpty(relatedProduct.Title) && !string.IsNullOrEmpty(rp.Description))
                    relatedProduct.Title = pTitle;
                    //relatedProduct.Title = rp.Description;

            }
            return relatedProduct;
        }

        #endregion
    }
}