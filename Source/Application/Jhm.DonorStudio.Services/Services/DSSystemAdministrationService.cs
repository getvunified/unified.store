using System.Collections.Generic;
using System.Linq;
using D2DNext.AB_CMN;
using D2DNext.AB_CMN.SearchCriteria;
using Jhm.DonorStudio.Services.DTO;

namespace Jhm.DonorStudio.Services
{
    public class DSSystemAdministrationService : IDSSystemAdministrationService
    {
        private readonly IDSInternalService internalService;

        public DSSystemAdministrationService(IDSInternalService internalService)
        {
            this.internalService = internalService;
        }

        public List<DSCodeValue> GetCodeValues(string environment, string codeType)
        {
            var environmentService = internalService.ServiceAccess(environment);

            List<DSCodeValue> results = null;

            var codeValues = environmentService.SystemAdministration().GetValidationList(codeType,
                                                                                         new CodeValueLookupCriteria
                                                                                             {
                                                                                                 CodeDescription = string.Empty,
                                                                                                 CodeValue = string.Empty
                                                                                             },
                                                                                         string.Empty, string.Empty,
                                                                                         PagingContext.AllDataPaging());

            if (codeValues.Length > 0)
            {
                results = codeValues.Select(cv => new DSCodeValue
                                                      {
                                                          Description = cv.Description, 
                                                          Value = cv.Value
                                                      }).ToList();
            }

            return results;
        }

        public DSControlRecord GetControlRecord(string environment, string controlRecord)
        {
            var environmentService = internalService.ServiceAccess(environment);

            DSControlRecord result = null;

            var cr = environmentService.SystemAdministration().ControlRecordsSearchByType(controlRecord);

            if (cr != null)
                result = new DSControlRecord
                             {
                                 Comment = cr.Comment,
                                 Description = cr.Description,
                                 Type = cr.Type,
                                 Value = cr.Value
                             };


            return result;
        }

        public List<DSShippingMethod> GetShippingMethods(string environment)
        {
            var environmentService = internalService.ServiceAccess(environment);

            List<DSShippingMethod> result = new List<DSShippingMethod>();

            var shippingMethods = environmentService.SystemAdministration().ShippingMethodsSearchByCriteria(new ShippingMethodsLookupCriteria(), PagingContext.AllDataPaging());

            if (shippingMethods != null)
                foreach (var sm in shippingMethods)
                {
                    var newSm = new DSShippingMethod()
                    {
                        Method = sm.ShipMethod,
                        Description = sm.Description,
                        DeliverInternational = sm.DeliverInternational.GetValueOrDefault(),
                        DeliverOnHolidays = sm.DeliverOnHolidays.GetValueOrDefault(),
                        DeliverOnSaturday = sm.DeliverOnSaturday.GetValueOrDefault(),
                        DeliverOnSunday = sm.DeliverOnSunday.GetValueOrDefault(),
                        NumberOfDays = sm.NumberOfDays.GetValueOrDefault(),
                        ShippingControlCode = sm.ShippingControlCode,
                        ShippingPreference = sm.ShippingPreference.GetValueOrDefault(),
                        ZoneBasedShipping = sm.ZoneBasedShipping.GetValueOrDefault()
                    };

                    result.Add(newSm);
                }

            if (result.Count > 0)
                return result;

            return null;
        }

        public List<DSMasterSubscription> GetMagazineSubscriptions(string environment)
        {
            var environmentService = internalService.ServiceAccess(environment);

            var searchResults = environmentService.MarketingAnalysis().SubscriptionMasterSearchByCriteria(
                new SubscriptionMasterLookupCriteria { SubscriptionType = "M", Active=true }, PagingContext.AllDataPaging());

            var priceCodes = environmentService.SystemAdministration().CodeValuesGetMultiple("DDCSUBPRCD", "True",PagingContext.AllDataPaging());
            var result = new List<DSMasterSubscription>();
            foreach (var sr in searchResults)
            {
                var prices = environmentService.MarketingAnalysis().SubscriptionPricesSearchBySubscriptionCode(sr.SubscriptionCode, PagingContext.AllDataPaging());
                var subscription = new DSMasterSubscription
                    {
                        SubscriptionCode = sr.SubscriptionCode,
                        Description = sr.Description,
                        Fullfilment = sr.FulfillmentMethod,
                        Type = sr.SubscriptionType,
                        Prices = prices.Select(x =>
                            {
                                var priceCode = priceCodes.SingleOrDefault(p => p.Value == x.PriceCode);
                                return new DSSubscriptionPrice
                                {
                                    Description = priceCode != null ? priceCode.Description : "(No price description)",
                                    EndDate = x.EndDate.GetValueOrDefault(),
                                    NumberOfIssues = x.NumberOfIssues.GetValueOrDefault(),
                                    Price = x.Price.GetValueOrDefault(),
                                    PriceCode = x.PriceCode,
                                    StartDate = x.StartDate.GetValueOrDefault(),
                                    SubscriptionCode = x.SubscriptionCode
                                };
                            }).ToList()
                    };
                result.Add(subscription);
            }
            return result;
        } 

    }
}