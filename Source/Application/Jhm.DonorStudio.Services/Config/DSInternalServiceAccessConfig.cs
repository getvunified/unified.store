﻿using System.Collections.Generic;

public class InternalServiceAccessConfig
{
    private IEnumerable<string> environments = new List<string>();
       

    public InternalServiceAccessConfig(IEnumerable<string> environments)
    {
        this.environments = environments;
            
    }


    public IEnumerable<string> Environments
    {
        get {
            return environments;
        }
    }

       

      
}