﻿using System.Linq;
using D2DNext.AB_DL.BusinessDb;

namespace Jhm.DonorStudio.Services.Config
{
    public static class DSConstants
    {
        //Constant Settings
        public static string DataSource = "WEB";
        public static string AccountSourceCode = "WEB";
        public static string OrderSourceCode = "WEB";
        public static string DonationSourceCode = "WEB";
        //public static string Warehouse = "10";
        public static string WarehouseLocation = "DEFAULT";
        
        //public static string GetWarehouse(string  environment)
        //{
        //    if( environment == Jhm.Web.Core.Models.Company.JHM_Canada.DonorStudioEnvironment)
        //    {
        //        return "20"; // Canada
        //    }
        //    if (environment == Jhm.Web.Core.Models.Company.JHM_UnitedKingdom.DonorStudioEnvironment)
        //    {
        //        return "50"; // UK
        //    }
            
        //    return "10"; // Default

        //}

        public static string GetWarehouseFromList(string environment, I03_ProductWarehousesSearchByProductCodeResult[] pWarehouses)
        {
            if (environment == Jhm.Web.Core.Models.Company.JHM_Canada.DonorStudioEnvironment)
            {
                return "20"; // Canada
            }
            if (environment == Jhm.Web.Core.Models.Company.JHM_UnitedKingdom.DonorStudioEnvironment)
            {
                return "50"; // UK
            }
            if (environment == Jhm.Web.Core.Models.Company.JHM_UnitedStates.DonorStudioEnvironment)
            {
                return pWarehouses.Any(i03ProductWarehousesSearchByProductCodeResult => i03ProductWarehousesSearchByProductCodeResult.WarehouseCode.Equals("5")) ? "5" : "10";
            }
            return "10";
        }


        public static string GetCompany(string environment)
        {
            if (environment == Jhm.Web.Core.Models.Company.JHM_UnitedKingdom.DonorStudioEnvironment)
            {
                return "JHMUK";
            }

            return "00001";

        }

        public static class CodeTypes
        {
            public static string MediaType = "WEBMEDIA";
            public static string Publisher = "WEBPUBLISH";
            public static string Trailer = "WEBTRAILER";
            public static string Image = "WEBIMAGE";
            public static string InHouse = "WEBINHOUSE"; //"INVS02" similar
            public static string RelatedProject = "WEBRELPROJ";
            public static string RelatedProduct = "WEBRELPROD";
            public static string Category = "WEBCAT";
            public static string Author = "WEBAUTHOR";
            public static string Subject = "WEBSUBJECT";

            public static string Gender = "DDCGENDER";


            public static string FeaturedOnWeb = "WEBFEATURE";
            public static string FeaturedOnTelevision = "TELEVISION";
            public static string FeaturedOnGetv = "GETV";
            
        }

        public static class NoteTypes
        {
            public static string Root = "WEBROOT";
            public static string Link = "WEBLINK";
            public static string Publisher = "WEBPUBLISH";
            public static string Title = "WEBTITLE";
            public static string Description = "WEBDESC";
            public static string FileLength = "WEBFILELEN";
            public static string RelatedProduct = "WEBRELPROD";
            public static string RelatedProject = "WEBRELPROJ";
        }

        public static class DateTypes
        {
            public static string ReleaseDate = "WEBRELDATE";
        }

        public static class AttachmentTypes
        {
            public static string ProductLink = "WEBLINK";
            public static string Trailer = "WEBTRAILER";
            public static string Image = "WEBIMAGE";
        }

        public static class PriceCodes
        {
            public static string Base = "ST";
            public static string WebSale = "W";
        }

        public static class AccountStatus
        {
            public static string Duplicate = "D";
        }

        public static class AccountType
        {
            public static string Individual = "I";
        }

        public static class EmailType
        {
            public static string Web = "WEB";
        }

        public static class AccountDefinitionType
        {
            public static string Single = "S";
            public static string Married = "M";
        }

        public static class OrderStatus
        {
            public static string Backordered = "B";
            public static string Cancelled = "C";
            public static string Committed = "A";
        }

        public class PaymentType
        {
            public static string CreditCard = "CC";
            public static string Check = "CK";
            public static string ElectronicFundsTransfer = "EFT";
            public static string GiftCard = "GC";
            public static string Adjustment = "ADJ";
            public static string Refund = "REF";
            public static string NoFunds = "NF";
            public static string AccountsReceivable = "AR";
            public static string Cash = "CA";
            public static string InterCompany = "IC";
        }

       
    }
}
