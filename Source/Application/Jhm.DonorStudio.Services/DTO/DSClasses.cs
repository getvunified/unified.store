﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.DonorStudio.Services.DTO
{
    public class DSAccountCode
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public bool Active { get; set; }
        public string DataSource { get; set; }
        public long RecordId { get; set; }
    }

    public class DSCodeValue
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }

    public class DSControlRecord
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Comment { get; set; }
    }

    public class DSShippingMethod
    {
        public string Method { get; set; }
        public string Description { get; set; }
        public long NumberOfDays { get; set; }
        public long ShippingPreference { get; set; }
        public bool ZoneBasedShipping { get; set; }
        public bool DeliverOnSaturday { get; set; }
        public bool DeliverOnSunday { get; set; }
        public bool DeliverInternational { get; set; }
        public bool DeliverOnHolidays { get; set; }
        public string ShippingControlCode { get; set; }
    }

    public class DSMasterSubscription
    {
        public string SubscriptionCode { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Fullfilment { get; set; }
        public List<DSSubscriptionPrice> Prices { get; set; }
    }

    public class DSSubscriptionPrice
    {
        public string SubscriptionCode { get; set; }
        public string PriceCode { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public long NumberOfIssues { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
