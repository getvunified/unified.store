﻿using System;
namespace Jhm.DonorStudio.Services
{
    public class JHMAccount
    {
        public long AccountNumber { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string Source { get; set; }
        public DsAddress DsAddress { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public string Salutation { get; set; }
        public DsPhone Phone { get; set; }
        public string Status { get; set; }
        public long FamilyId { get; set; }
    }

    public class DsAddress
    {
        public long AddressId { get; set; }
        public string Type { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public bool IsPrimary { get; set; }
        public long RecordId { get; set; }
        public long A02RecordId { get; set; }
    }

    public class DsPhone
    {
        public string Type { get; set; }
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string Number { get; set; }
        public string Extension { get; set; }
    }

    public class DsUserSubscription
    {
        public long AccountNumber { get; set; }
        public long SubscriptionId { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CancelReasonCode { get; set; }
        public long DeliveryAddressId { get; set; }
        public int NumberOfIssues { get; set; }
        public int NumberOfIssuesRemaining { get; set; }
        public string PriceCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string SubscriptionCode { get; set; }
        public string SubscriptionStatus { get; set; }
        public decimal SubscriptionCost { get; set; }
        public long DsRecordId { get; set; }

        public bool IsRenewal { get; set; }
    }
}
