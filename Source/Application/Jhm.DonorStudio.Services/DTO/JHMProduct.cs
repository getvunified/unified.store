using System;
using System.Collections.Generic;

namespace Jhm.DonorStudio.Services
{
    public class JHMProduct
    {
        public string Author { get; set; }
        public string AuthorDescription { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string DsType { get; set; }
        public string Isbn { get; set; }
        public string Title { get; set; }
        public string Trailer { get; set; }
        public string Packaging { get; set; }
        public string Publisher { get; set; }
        public string PublisherDescription { get; set; }
        public string RootProductCode { get; set; }
        public string Status { get; set; }

        public List<string> Categories { get; set; }
        public List<string> Images { get; set; }
        public List<string> Subjects { get; set; }

        public List<RelatedProduct> RelatedProducts { get; set; }
        public List<RelatedProject> RelatedProjects { get; set; }

        public List<Item> Items { get; set; }

        public bool DiscountAllowed { get; set; }
        public bool FeaturedOnTelevision { get; set; }
        public bool FeaturedOnGetv { get; set; }
        public bool UseInShoppingCart { get; set; }

        public decimal FileLength { get; set; }

        public long RecordId { get; set; }
    }

    public class RelatedProject
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class RelatedProduct
    {
        //public decimal SalePrice { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
    }

    public class Item
    {
        public string MediaType { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public bool AllowBackorder { get; set; }
        public bool IsInHouse { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long QuantityInStock { get; set; }
        public decimal BasePrice { get; set; }
        public decimal SalePrice { get; set; }
        public string Link { get; set; }
        public string WarehouseCode { get; set; }
    }
}