﻿using System;
using System.Collections.Generic;
using Jhm.DonorStudio.Services.Config;

namespace Jhm.DonorStudio.Services
{
    public class JHMTransaction
    {
        public JHMTransaction()
        {
            Date = new DateTime();
            Orders = new List<Order>();
            Donations = new List<Donation>();
            Subscriptions = new List<DsUserSubscription>();
            CreditCard = new CreditCard();
        }

        public long AccountNumber { get; set; }
        public long DocumentNumber { get; set; }
        
        // Original Amount on the transaction
        public decimal TotalAmount { get; set; }
        
        // Used just when retrieving transactions, Orders + Donations only
        public decimal CalculatedAmount { get; set; }
        
        public string CurrencyCode { get; set; }
        public string MediaId { get; set; }
        public long BatchNumber { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }

        // May not be necessary
        public decimal AmountDue { get; set; }
        public decimal RemainingAmountDue { get; set; }

        public List<Order> Orders { get; set; }
        public List<Donation> Donations { get; set; }
        public List<DsUserSubscription> Subscriptions { get; set; }

        public CreditCard CreditCard { get; set; }

        public string PaymentType { get; set; }
    }

    public class CreditCard
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Cvv { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public bool AlreadyProcessed { get; set; }

        public string Token { get; set; }

        public string MerchantId { get; set; }
    }

    public class Donation
    {
        public string ProjectCode { get; set; }
        public string ProjectDescription { get; set; }
        public decimal Amount { get; set; }
        public bool IsDeductible { get; set; }
        public string SourceCode { get; set; }
        public bool Anonymous { get; set; }
    }

    public class Order
    {
        public Order()
        {
            OrderDetails = new List<OrderDetail>();
            Tax = new Tax();
        }

        public long AddressId { get; set; }
        public long OrderId { get; set; }
        public string ShippingMethod { get; set; }
        public decimal ShippingAmount { get; set; }
        public List<OrderDetail> OrderDetails { get; set; }
        public string Warehouse { get; set; }

        public Tax Tax { get; set; }
    }

    public class Tax
    {
        public string TaxArea { get; set; }
        public decimal TaxAmount1 { get; set; }
        public decimal TaxAmount2 { get; set; }
        public decimal TaxAmount3 { get; set; }
        public decimal TaxAmount4 { get; set; }
        public decimal TaxAmount5 { get; set; }
    }
    
    public class OrderDetail
    {
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public decimal Price { get; set; }
        public string PriceCode { get; set; }
        public int Quantity { get; set; }
        public decimal ExtendedAmount { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public string SourceCode { get; set; }
        public string Comment { get; set; }
        public decimal DiscountAmount { get; set; }
    }
}