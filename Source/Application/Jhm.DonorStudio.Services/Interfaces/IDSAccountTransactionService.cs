﻿using System.Collections.Generic;
using D2DNext.AB_CSA;
using Jhm.Common;
using Jhm.DonorStudio.Services.DTO;

namespace Jhm.DonorStudio.Services
{
    public interface IDSAccountTransactionService : IService
    {
        long CreateAccount(string environment, JHMAccount jhmAccount);
        long CreateAccountAddress(string environment, long accountNumber, DsAddress dsAddress);
        long UpdateAccountAddress(string environment, long accountNumber, DsAddress dsAddress);
        long DeleteAccountAddress(string environment, long accountNumber, DsAddress dsAddress);
        long CreateTransaction(string environment, JHMTransaction transaction, bool useAutoBatch = false);

        void UpdateAccount(string environment, JHMAccount jhmAccount);

        JHMAccount GetAccount(string environment, long accountNumber);
        List<JHMTransaction> GetTransactionHistory(string environment, int howManyMonthsBack, long accountNumber);
        JHMTransaction GetTransactionByDocumentNumber(string enviroment, long documentNumber, long accountNumber);
        List<DSAccountCode> GetAccountCodes(string enviroment, long accountNumber, bool activeOnly);
        DsAddress GetAddress(string environment, long addressId);
        DsAddress GetPrimaryAddress(string environment, long accountNumber);
        List<DsAddress> GetAccountAddresses(string environment, long accountNumber);
        Tax GetTaxRates(string environment, string productCode, string zip, string country, decimal price, decimal shipping);
        long GetCurrentAccountNumber(string environment, long mergedAccountNumber);
        long GetAutoBatchNumber(string environment, string assignedUser, string companyCode);
        List<DsUserSubscription> GetActiveAccountSubscriptions(string environment, long accountNumber);
        long AddSubscription(string environment, long accountNumber, DsUserSubscription userSubscription);
        void UpdateAccountSubscription(string environment, DsUserSubscription subscription);
        void CancelAccountSubscription(string environment, DsUserSubscription subscription);

        void AddAccountCode(string dsEnvironment, long accountNumber, string codeType, string codeValue);
    }
}