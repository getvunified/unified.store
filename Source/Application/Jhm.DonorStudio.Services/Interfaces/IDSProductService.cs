﻿using System;
using System.Collections.Generic;
using Jhm.Common;

namespace Jhm.DonorStudio.Services
{
    public interface IDSProductService : IService
    {
        List<JHMProduct> GetProducts(string environment, string author, string description, string productCode, string title, decimal priceMin, decimal priceMax, DateTime? minimumReleaseDate,
            string category, string subject, string mediaType, bool isAvailable, bool onSaleOnly, int pageNumber, int maxRowsOnPage, ref int totalRows);

        List<JHMProduct> GetAllProducts(string environment);

        JHMProduct GetProduct(string environment, string rootProductCode);

        void UpdateProductNoteFileLength(string environment, string productCode, decimal weight);
    }
}