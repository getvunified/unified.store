﻿using System.Collections.Generic;
using Jhm.Common;
using Jhm.DonorStudio.Services.DTO;

namespace Jhm.DonorStudio.Services
{
    public interface IDSSystemAdministrationService : IService
    {
        List<DSCodeValue> GetCodeValues(string environment, string codeType);
        DSControlRecord GetControlRecord(string environment, string controlRecord);
        List<DSShippingMethod> GetShippingMethods(string environment);
        List<DSMasterSubscription> GetMagazineSubscriptions(string environment);
    }
}