﻿using Jhm.Common;
using getv.donorstudio;

namespace Jhm.DonorStudio.Services.Interfaces
{
    public interface IDSServiceCollection : IService
    {
        IDsInventoryService DsInventoryService { get; }
        IDsAccountTransactionService DsAccountTransactionService { get; }
        
    }
}
