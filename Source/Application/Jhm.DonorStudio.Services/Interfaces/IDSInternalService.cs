﻿using D2DNext.AB_CSA;

namespace Jhm.DonorStudio.Services
{
    public interface IDSInternalService
    {
        ServiceAccessBase ServiceAccess(string environment);

    }
}