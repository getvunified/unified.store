using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.Web.Core.Models.Modules.ECommerce;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Utilities;
using getv.donorstudio.core.eCommerce;
using CreditCard = getv.donorstudio.core.eCommerce.CreditCard;
using Donation = getv.donorstudio.core.eCommerce.Donation;

namespace Jhm.DonorStudio.Services.Mappers
{
    public class DsTransactionMapper
    {
        // Cart to Transaction

        public static DsTransaction Map(CheckoutOrderReviewViewModel from)
        {
            var to = new DsTransaction();
            var orders = new List<Order>();
            // Order order = null;

            to.AccountNumber = from.Profile.User.AccountNumber;
            to.CurrencyCode = from.CurrencyCode;
            to.Date = from.OrderDate;
            //to.MediaId = string.Empty;
            to.MediaId = "UNKN";
            to.Status = "A"; //active

            to.Orders = new List<Order>();
            to.Donations = new List<Donation>();

            to.TotalAmount = from.OrderTotal;


            // Temporary and quick solution to reflect discount applied on each individual products ordered
            var stockItemsCount = from.CartItems.ToLIST().FindAll(i => i.Item is StockItem).Count;

            foreach (var cartLineItem in from.CartItems)
            {
                #region CartItem is an StockItem

                if (cartLineItem.Item is StockItem)
                {
                    var item = cartLineItem.Item as StockItem;

                    var deliveryAddressId = from.ShippingAddress != null ? from.ShippingAddress.DSAddressId : 0;
                    var orderAccountNumber = to.AccountNumber;

                    var order = orders.FirstOrDefault(x => x.Warehouse.Equals(item.Warehouse) && x.AddressId == deliveryAddressId && x.AccountNumber == orderAccountNumber);
                    // Creating an order per each combination of Warehouse, Account and  DeliveryAddress
                    // if does not exist
                    if (order == null)
                    {
                        order = new Order() {Warehouse = item.Warehouse, AddressId = deliveryAddressId, AccountNumber = orderAccountNumber};
                        orders.Add(order);
                    }

                    //if (!orders.Any(x => x.Warehouse.Equals(item.Warehouse)))
                    //    orders.Add(new Order() { Warehouse = item.Warehouse });

                    //if (order.IsNull())
                    //    order = new Order();

                    var discountAmount = 0.0m;
                    // Temporary and quick solution to reflect discount applied on each individual products ordered
                    if (from.DiscountApplied != null)
                    {
                        discountAmount = from.DiscountApplied.ActiveRule.IsFixedAmount ?
                            // Split the fix amount off between the number of items in the order since DS requires the discount amount to be applied individually
                            from.DiscountApplied.ActiveRule.AmountOff / stockItemsCount / cartLineItem.Quantity :
                            from.DiscountApplied.ActiveRule.CalculatePercentageDiscount(item.Price);
                    }

                    order.OrderDetails.Add(new OrderDetail()
                    {
                        Price = item.Price,
                        Quantity = cartLineItem.Quantity,
                        ProductCode = item.Code,
                        PriceCode = (item.IsOnSale()) ? "W" : "ST",
                        SourceCode = from.DsSourceCode,
                        DiscountAmount = discountAmount * cartLineItem.Quantity
                    });
                }

                #endregion

                #region CartItem is an GiftCard

                if (cartLineItem.Item is GiftCardCartItem)
                {
                    var item = cartLineItem.Item as GiftCardCartItem;

                    var deliveryAddressId = item.GiftCardFormInfo.IsForOwnAddress
                                                ? item.GiftCardFormInfo.DeliveryAddress.DSAddressId
                                                : item.GiftCardFormInfo.User.Profile.PrimaryAddress.DSAddressId;

                    var orderAccountNumber = item.GiftCardFormInfo.IsForOwnAddress
                                                 ? to.AccountNumber
                                                 : item.GiftCardFormInfo.User.AccountNumber;

                    var order = orders.FirstOrDefault(x => x.Warehouse.Equals(item.Warehouse) && x.AddressId == deliveryAddressId && x.AccountNumber == orderAccountNumber);
                    // Creating an order per each combination of Warehouse, Account and  DeliveryAddress
                    // if does not exist
                    if (order == null)
                    {
                        order = new Order() { Warehouse = item.Warehouse, AddressId = deliveryAddressId, AccountNumber = orderAccountNumber };
                        orders.Add(order);
                    }

                    item.SetCode(item.GiftCardFormInfo.GiftCard.ProductCode);
                    order.OrderDetails.Add(new OrderDetail()
                    {
                        Price = item.Price,
                        Quantity = cartLineItem.Quantity,
                        ProductCode = item.Code,
                        PriceCode = "ST",
                        SourceCode = from.DsSourceCode,
                        DiscountAmount = 0
                    });
                }

                #endregion

                #region CartItem is a Donation

                else if (cartLineItem.Item is DonationCartItem)
                {
                    var item = cartLineItem.Item as DonationCartItem;

                    to.Donations.Add(new Donation()
                                         {
                                             Amount = item.Price,
                                             SourceCode = from.DsSourceCode,
                                             Anonymous = false,
                                             IsDeductible = true,
                                             ProjectCode = item.ProjectCode
                                         });

                    foreach (var includedStockItem in item.IncludedStockItems)
                    {

                        var deliveryAddressId = from.ShippingAddress != null ? from.ShippingAddress.DSAddressId : 0;

                        var orderAccountNumber = to.AccountNumber;

                        var order = orders.FirstOrDefault(x => x.Warehouse.Equals(includedStockItem.Warehouse) && x.AddressId == deliveryAddressId && x.AccountNumber == orderAccountNumber);
                        // Creating an order per each combination of Warehouse, Account and  DeliveryAddress
                        // if does not exist
                        if (order == null)
                        {
                            order = new Order() { Warehouse = includedStockItem.Warehouse, AddressId = deliveryAddressId, AccountNumber = orderAccountNumber };
                            orders.Add(order);
                        }

                        order.OrderDetails.Add(new OrderDetail()
                         {
                             Comment =  item.CertificateName ?? "Auto Added By A Donation",
                             Price = 0,
                             Quantity = 1,
                             //  Assuming donation only adds 1 all the time
                             ProductCode = includedStockItem.Code,
                             PriceCode = "P",
                             //  Promotional
                             SourceCode = from.DsSourceCode,
                         });
                    }
                    
                    foreach (var promoItem in from.Cart.PromotionalItems)
                    {
                        /*
                        var deliveryAddressId = from.ShippingAddress != null ? from.ShippingAddress.DSAddressId : 0;

                        var orderAccountNumber = to.AccountNumber;

                        var order = orders.FirstOrDefault(x => x.Warehouse.Equals(item.Warehouse) && x.AddressId == deliveryAddressId && x.AccountNumber == orderAccountNumber);
                        // Creating an order per each combination of Warehouse, Account and  DeliveryAddress
                        // if does not exist
                        if (order == null)
                        {
                            order = new Order() { Warehouse = item.Warehouse, AddressId = deliveryAddressId, AccountNumber = orderAccountNumber };
                            orders.Add(order);
                        }
                        */

                        //TODO:   Need to get warehouseCode from the promo code that is added
                        /*if (!orders.Any(x => x.Warehouse.Equals(promoItem.ProductWarehouse)))
                            orders.Add(new Order() { Warehouse = promoItem.ProductWarehouse });

                        orders.First(x => x.Warehouse.Equals(promoItem.ProductWarehouse));*/
                        
                        //BUG:  Needs to add STOCKITEM not Product and get warehouse code from stockitem
                        if (!orders.Any(x => x.Warehouse.Equals("10")))
                            orders.Add(new Order() { Warehouse = "10" });

                        orders.First(x => x.Warehouse.Equals("10"))
                         .OrderDetails.Add(new OrderDetail()
                         {
                             Comment = "Auto Added By Promotional Item",
                             Price = 0,
                             Quantity = promoItem.Quantity,
                             //  Assuming donation only adds 1 all the time
                             ProductCode = promoItem.Product.Code,
                             
                             PriceCode = "P",
                             //  Promotional
                             SourceCode = from.DsSourceCode
                         });
                    }

                }
                #endregion

                #region CartItem is a SubscriptionCartItem

                else if (cartLineItem.Item is SubscriptionCartItem)
                {
                    var item = cartLineItem.Item as SubscriptionCartItem;

                    to.Subscriptions.Add(new DsUserSubscription
                    {
                        AccountNumber = item.UserSubscription.User.AccountNumber,
                        DeliveryAddressId = item.UserSubscription.IsGiftSubcription ? 
                                                item.UserSubscription.User.Profile.PrimaryAddress.DSAddressId : 
                                                from.ShippingAddress != null ? from.ShippingAddress.DSAddressId : 0,
                        NumberOfIssues = item.UserSubscription.NumberOfIssues,
                        NumberOfIssuesRemaining = item.UserSubscription.NumberOfIssuesRemaining,
                        PriceCode = item.UserSubscription.PriceCode,
                        StartDate = item.UserSubscription.StartDate,
                        SubscriptionCode = item.UserSubscription.SubscriptionCode,
                        SubscriptionCost = item.UserSubscription.SubscriptionPrice,
                        SubscriptionStatus = "A", // Active
                        IsRenewal = item.UserSubscription.IsRenewal,
                        SubscriptionId = item.UserSubscription.IsRenewal ? item.UserSubscription.Id : 0,
                        DsRecordId = item.UserSubscription.IsRenewal ? item.UserSubscription.DsRecordId : 0
                    });
                }
                #endregion


            }

            foreach (var discountItem in from.Cart.DiscountItems)
            {

                if (!orders.Any(x => x.Warehouse.Equals(discountItem.ProductWarehouse)))
                    orders.Add(new Order() { Warehouse = discountItem.ProductWarehouse });

                orders.First(x => x.Warehouse.Equals(discountItem.ProductWarehouse)).OrderDetails.Add(new OrderDetail()
                 {
                     Comment = "Auto Added By Promotional Item",
                     Price = 0,
                     Quantity = discountItem.Quantity,
                     //  Assuming donation only adds 1 all the time
                     ProductCode = discountItem.Product.Code,

                     PriceCode = "P",
                     //  Promotional
                     SourceCode = from.DsSourceCode
                 });
            }

            //TODO:  Need to put STANDARD on the first or warehouse 10 but FREE on fulfillment center

            if (orders.Count == 1)
            {
                var order = orders.First();

                if (from.ShippingAddress != null && order.AddressId == 0) order.AddressId = from.ShippingAddress.DSAddressId;
                if (from.ShippingMethod == null)
                {
                    from.ShippingMethod = ShippingMethod.NotApplicable;
                }
                order.ShippingMethod = from.ShippingMethod.DSValue;
                order.ShippingAmount = from.ShippingMethod.Price;
            }
            if (orders.Count > 1)
            {
                foreach (var order in orders)
                {
                    if (from.ShippingAddress != null && order.AddressId == 0) order.AddressId = from.ShippingAddress.DSAddressId;
                    if (from.ShippingMethod == null)
                    {
                        from.ShippingMethod = ShippingMethod.NotApplicable;
                    }

                    //TODO:  Checking for warehouse 10.  Need to lookup "default" for company
                    order.ShippingMethod = from.ShippingMethod.DSValue;  // Set to value of method even if we give it for free

                    //order.ShippingMethod = order.Warehouse.Equals("10") ? @from.ShippingMethod.DSValue : ShippingMethod.NotApplicable.DSValue;
                    order.ShippingAmount = order.Warehouse.Equals("10") ? from.ShippingMethod.Price : ShippingMethod.NotApplicable.Price;
                }
            }

            to.Orders = orders;

            foreach (var payment in from.PaymentList.Payments)
            {
                var cc = payment.CreditCard;
                var gc = payment.GiftCard;
                to.Payments.Add( new Payment
                    {
                        AlreadyProcessed = payment.IsAlreadyProcessed,
                        Amount = payment.Amount,
                        PaymentType = payment.PaymentType.Value,
                        CreditCard = cc == null ? null : new CreditCard
                            {
                                    Token = cc.Token,
                                    MerchantId = "BLUE",
                                    Name = cc.NameOnCard,
                                    Number = cc.CreditCardNumber,
                                    Cvv = cc.CID,
                                    ExpirationMonth = cc.CCExpirationMonth,
                                    ExpirationYear = cc.CCExpirationYear
                            },
                        GiftCard = gc == null? null : new GiftCardPayment
                            {
                                Number = gc.Number,
                                Pin = gc.Pin
                            }
                    });
            }

            //to.PaymentType = DsConstants.PaymentType.CreditCard; //NOTE:  Defaulting for now..  This will be sent in from CheckoutData

            //to.CreditCard = new CreditCard()
            //                    {
                                    //AlreadyProcessed = true,
                                    //Token = from.Token,
                                    //MerchantId = "BLUE",
                                    //Name = from.NameOnCard,
                                    //Number = from.CreditCardNumber,
                                    //Cvv = from.CID,
                                    //ExpirationMonth = from.CCExpirationMonth,
                                    //ExpirationYear = from.CCExpirationYear
            //                    };
            return to;
        }

        public static TransactionViewModel Map(DsTransaction from)
        {
            var to = new TransactionViewModel();
            to.AccountNumber = from.AccountNumber;
            to.AmountDue = from.AmountDue;
            to.CurrencyCode = from.CurrencyCode;
            to.Date = from.Date;
            to.DocumentNumber = from.DocumentNumber;
            //to.CreditCard = Map(from.CreditCard);
            to.Donations = from.Donations.Select(Map).ToLIST();
            to.Orders = from.Orders.Select(Map).ToLIST();
            to.TotalAmount = from.TotalAmount;
            to.Subscriptions = from.Subscriptions.Select(JHMSubscriptionMapper.Map).ToLIST();
            return to;
        }

        public static CreditCardViewModel Map(CreditCard from)
        {
            var to = new CreditCardViewModel();
            to.CreditCardNumber = from.Number;
            return to;
        }

        public static OrderViewModel Map(Order from)
        {
            var to = new OrderViewModel();
            to.AddressId = from.AddressId;
            to.OrderId = from.OrderId;
            to.ShippingMethod = from.ShippingMethod;
            to.ShippingAmount = from.ShippingAmount;
            to.OrderDetails = from.OrderDetails.Select(Map).ToLIST();
            return to;
        }

        public static DonationViewModel Map(Donation from)
        {
            var to = new DonationViewModel();
            to.Amount = from.Amount;
            to.Anonymous = from.Anonymous;
            to.IsDeductible = from.IsDeductible;
            to.ProjectCode = from.ProjectCode;
            to.ProjectDescription = from.ProjectDescription;
            to.SourceCode = from.SourceCode;
            return to;
        }

        public static OrderDetailViewModel Map(OrderDetail from)
        {
            var to = new OrderDetailViewModel();
            to.DiscountAmount = from.DiscountAmount;
            to.ExtendedAmount = from.ExtendedAmount;
            to.Price = from.Price;
            to.ProductCode = from.ProductCode;
            to.ProductDescription = from.ProductDescription;
            to.Quantity = from.Quantity;
            to.Status = from.Status;

            //var isItemInStock = from.ProductCode.ToLIST().FindAll(i => i.Item is StockItem).Count;

            return to;
        }


    }


}