using getv.donorstudio.core.Account;
using Address = Jhm.em3.Core.Address;
using AddressType = Jhm.Web.Core.AddressType;

namespace Jhm.DonorStudio.Services.Mappers
{
    public class JHMAddressMapper
    {
        public static Address Map(DsAddress from)
        {
            var to = new Address();
            to.A02RecordId = from.A02RecordId;
            to.RecordId = from.RecordId;
            to.DSAddressId = from.AddressId;
            to.Address1 = from.Line1 ?? string.Empty;
            to.Address2 = from.Line2 ?? string.Empty;
            to.City = from.City ?? string.Empty;
            to.StateCode = from.State ?? string.Empty;
            to.PostalCode = from.ZipCode ?? string.Empty;
            to.Country = from.Country ?? string.Empty;
            to.SetDSAddressType(AddressType.TryGetFromValue<AddressType>(from.Type.Trim()));
            to.DsIsPrimary = from.IsPrimary;

            //to.CongressionalDistrict = "";


            return to;
        }


        public static DsAddress Map(Address from)
        {
            var to = new DsAddress();
            to.A02RecordId = from.A02RecordId;
            to.RecordId = from.RecordId;
            to.AddressId = from.DSAddressId;
            to.Line1 = from.Address1;
            to.Line2 = from.Address2;
            to.City = from.City;
            to.State = from.StateCode; //?
            to.ZipCode = from.PostalCode;
            to.Country = from.Country; //?
            to.Type = from.DSAddressType.Value;
            to.IsPrimary = from.DsIsPrimary;
            

            return to;
        }




     


    }
}