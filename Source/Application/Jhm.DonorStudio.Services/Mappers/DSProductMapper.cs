﻿using D2DNext.AB_DL.BusinessDb;
using getv.donorstudio.core.Inventory;

namespace Jhm.DonorStudio.Services
{
    public class DSProductMapper
    {
        public DsProduct Map(I01_ProductMasterSearchByCriteriaResult from)
        {
            var to = new DsProduct();

            to.RootProductCode = from.ProductCode;
            to.Description = from.Description;
            to.Author = from.Author;
            

            return to;
        }
    }
}
