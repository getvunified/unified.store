﻿using System;
using System.Linq;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Inventory;

namespace Jhm.DonorStudio.Services.Mappers
{
    public class JHMSubscriptionMapper
    {
        public static UserSubscriptionViewModel Map(DsUserSubscription from)
        {
            var to = new UserSubscriptionViewModel();
            return Map(from, to);
        }

        public static UserSubscriptionViewModel Map(DsUserSubscription from, UserSubscriptionViewModel to)
        {
            if (from == null) throw new ArgumentNullException("from");
            if (to == null) throw new ArgumentNullException("to");
            to.Id = from.SubscriptionId;
            to.CancelDate = from.CancelDate;
            to.CancelReasonCode = from.CancelReasonCode;
            to.ExpirationDate = from.ExpirationDate;
            to.NumberOfIssues = from.NumberOfIssues;
            to.NumberOfIssuesRemaining = from.NumberOfIssuesRemaining;
            to.PriceCode = from.PriceCode;
            to.StartDate = from.StartDate;
            to.SubscriptionCode = from.SubscriptionCode;
            to.SubscriptionStatus = from.SubscriptionStatus;
            to.DeliveryAddress.DSAddressId = from.DeliveryAddressId;
            to.DsRecordId = from.DsRecordId;
            to.SubscriptionPrice = from.SubscriptionCost;
            return to;
        }

        public static DsUserSubscription Map(UserSubscriptionViewModel from)
        {
            var to = new DsUserSubscription();
            return Map(from, to);
        }

        public static DsUserSubscription Map(UserSubscriptionViewModel from, DsUserSubscription to)
        {
            if (from == null) throw new ArgumentNullException("from");
            if (to == null) throw new ArgumentNullException("to");

            to.CancelDate = from.CancelDate;
            to.CancelReasonCode = from.CancelReasonCode;
            to.ExpirationDate = from.ExpirationDate;
            to.NumberOfIssues = from.NumberOfIssues;
            to.NumberOfIssuesRemaining = from.NumberOfIssuesRemaining;
            to.PriceCode = from.PriceCode;
            to.StartDate = from.StartDate;
            to.SubscriptionCode = from.SubscriptionCode;
            to.SubscriptionStatus = from.SubscriptionStatus;
            to.SubscriptionCost = from.SubscriptionPrice;
            to.DsRecordId = from.DsRecordId;
            to.SubscriptionId = from.Id;
            if (from.User != null) to.AccountNumber = from.User.AccountNumber;
            if (from.DeliveryAddress != null) to.DeliveryAddressId = from.DeliveryAddress.DSAddressId;
            return to;
        }

        public static Subscription Map(DsMasterSubscription from)
        {
            if (from == null) throw new ArgumentNullException("from");
            var to = new Subscription
            {
                Description = @from.Description,
                Fullfilment = @from.Fullfilment,
                Prices = @from.Prices.Select(Map).ToList(),
                SubscriptionCode = @from.SubscriptionCode,
                Type = @from.Type
            };

            return to;
        }

        private static SubscriptionPrice Map(DsSubscriptionPrice from)
        {
            if (from == null) throw new ArgumentNullException("from");
            var to = new SubscriptionPrice
            {
                Description = @from.Description,
                EndDate = @from.EndDate,
                NumberOfIssues = @from.NumberOfIssues,
                Price = @from.Price,
                PriceCode = @from.PriceCode,
                StartDate = @from.StartDate,
                SubscriptionCode = @from.SubscriptionCode
            };

            return to;
        }
    }
}
