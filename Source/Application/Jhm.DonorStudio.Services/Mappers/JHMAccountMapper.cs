using System;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.Utilities;

namespace Jhm.DonorStudio.Services.Mappers
{


    public class DsAccountMapper
    {

        public static User Map(DsAccount from, User to)
        {
            if (from == null) throw new ArgumentNullException("from");
            if (to == null) throw new ArgumentNullException("to");

            to.AccountNumber = from.AccountNumber;
            to.Profile.Title = from.Title;
            to.Profile.FirstName = from.FirstName;
            to.Profile.LastName = from.LastName;
            to.Profile.Gender = from.Gender;
            to.Profile.PrimaryAddress = JHMAddressMapper.Map(from.DsAddress);
            to.Profile.PrimaryPhone = JHMPhoneMapper.Map(from.Phone);

            return to;
        }
        public static DsAccount Map(User from, DsAccount to)
        {
            if (from == null) throw new ArgumentNullException("from");
            if (to == null) throw new ArgumentNullException("to");

            to.AccountNumber = from.AccountNumber;
            to.Title = from.Profile.Title;
            to.FirstName = from.Profile.FirstName;
            //to.MiddleName = from.Profile.MiddleName;
            to.LastName = from.Profile.LastName;
            to.Gender = (from.Profile.Gender == "M" || from.Profile.Gender == "Male") ? "M" : (from.Profile.Gender == "F" || from.Profile.Gender == "Female") ? "F" : "Udefined?";
            to.Birthday = from.Profile.BirthDate;
            to.Email = from.Email;
            to.Salutation = from.Profile.FirstName;
            //NOTE: Set salutation to FirstName
            to.Source = "WEB";
            //to.Suffix = from.Profile.Suffix;
            //to.Title = from.Profile.Title;
            to.Type = DsConstants.AccountType.Individual;
            
            to.DsAddress = new DsAddress()
                               {
                                   A02RecordId = from.Profile.PrimaryAddress.A02RecordId,
                                   RecordId = from.Profile.PrimaryAddress.RecordId,
                                   AddressId = from.Profile.PrimaryAddress.DSAddressId,
                                   Type = from.Profile.PrimaryAddress.DSAddressType.Value,
                                   Line1 = from.Profile.PrimaryAddress.Address1,
                                   Line2 = from.Profile.PrimaryAddress.Address2,
                                   City = from.Profile.PrimaryAddress.City,
                                   State = from.Profile.PrimaryAddress.StateCode,
                                   Country = from.Profile.PrimaryAddress.Country,
                                   ZipCode = from.Profile.PrimaryAddress.PostalCode,
                                   IsPrimary = true
                               };


            to.Phone = new DsPhone()
            {
                CountryCode = from.Profile.PrimaryPhone.CountryCode,
                AreaCode = from.Profile.PrimaryPhone.AreaCode,
                Number = from.Profile.PrimaryPhone.Number,
                Extension = from.Profile.PrimaryPhone.Extension,
                Type = from.Profile.PrimaryPhone.PhoneType.Value
            };
            return to;
        }

        public static User Map(DsAccount from)
        {
            return Map(from, new User());
        }
        public static DsAccount Map(User from)
        {
            return Map(from, new DsAccount());
        }

    }
}