using getv.donorstudio.core.Account;
using Phone = Jhm.Web.Core.Phone;

namespace Jhm.DonorStudio.Services.Mappers
{
    public class JHMPhoneMapper
    {
        public static Phone Map(DsPhone from)
        {
            var to = new Phone
                         {
                             AreaCode = from.AreaCode,
                             CountryCode = from.CountryCode,
                             Extension = from.Extension,
                             Number = from.Number
                         };

            to.SetPhoneType(from.Type);

            return to;
        }


        public static DsPhone Map(Phone from)
        {
            return new DsPhone
            {
                AreaCode = from.AreaCode,
                CountryCode = from.CountryCode,
                Extension = from.Extension,
                Number = from.Number,
                Type = from.PhoneType.Value
            };
        }
    }
}