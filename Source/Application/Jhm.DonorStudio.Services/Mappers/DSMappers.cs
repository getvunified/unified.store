﻿namespace Jhm.DonorStudio.Services
{
    public static class DSMappers
    {
        public static readonly DSTransactionMapper DSTransactionMapper = new DSTransactionMapper();
        public static readonly DSProductMapper DSProductMapper = new DSProductMapper();
    }
}