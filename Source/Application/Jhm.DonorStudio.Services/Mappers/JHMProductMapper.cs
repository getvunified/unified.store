﻿using System;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Inventory;
using MediaType = Jhm.Web.Core.Models.MediaType;

namespace Jhm.DonorStudio.Services.Mappers
{
    public class DsProductMapper
    {
        public static Product Map(DsProduct from)
        {
            var to = new Product(from.RootProductCode);
            
            to.SetAuthor(from.Author);
            to.SetAuthorDescription(from.AuthorDescription);
            to.SetDescription(from.Description);
            to.SetPublisher(from.Publisher);
            to.SetPublisherDescription(from.PublisherDescription);
            to.SetTitle(from.Title);
            to.SetTrailer(from.Trailer);
            to.SetFileLength(from.FileLength);
            to.SetRecordId(from.RecordId);
            to.SetIsbn(from.Isbn);
            to.SetDsType(from.DsType);
            to.SetStatus(from.Status);
            to.SetPackaging(from.Packaging);
            to.SetUseInShoppingCart(from.UseInShoppingCart);
            to.SetDiscountAllowed(from.DiscountAllowed);

            to.AddRelatedItems(null,from.RelatedProducts.Select(x => new RelatedItem { Code = x.Code, Description = x.Description, Image = x.Image, Title = x.Title }).ToArray());
            
            to.SetIsFeaturedOnGetv(from.FeaturedOnGetv);
            to.SetIsFeaturedOnTelevision(from.FeaturedOnTelevision);

            foreach (Category cat in from.Categories.SelectMany(category => Category.AllCategories.Where(cat => cat.Value.Equals(category))))
                to.AddCategory(cat);

            foreach (var subject in from.Subjects)
                to.AddSubject(subject);

            foreach (var image in from.Images)
                to.AddImages(image);

            if(from.Images.Count == 0)
                to.AddImages(@"W:\IMAGES\JHM.jpg"); //Default Logo



            //foreach (var relatedProduct in from.RelatedProducts)
            //    to.AddRelatedItems(new CartItem() {Code = relatedProduct});

            //foreach (var relatedProjects in from.relatedProjects)
            //    to.AddRelatedItems(new CartItem() {Code = relatedProjects});


            foreach (var item in from.Items)
            {
                var stockItem = new StockItem(MediaType.TryGetFromValue<MediaType>(item.MediaType), (int) item.QuantityInStock, item.AllowBackorder, item.BasePrice, item.SalePrice, item.IsInHouse, item.Link,item.WarehouseCode, from.FileLength, item.MediaSize);
                stockItem.SetReleaseDate(item.ReleaseDate ?? new DateTime().TruncateToSeconds());
                //var stockItem = new StockItem(MediaType.GetMediaTypeByCodeSuffix(item.MediaType), (int) item.QuantityInStock, item.AllowBackorder, item.BasePrice, item.SalePrice, item.IsInHouse);
                to.AddStockItems(stockItem);
            }

            return to;
        }
    }


}
