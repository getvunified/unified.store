﻿using Jhm.Common;
using Jhm.Common.Repositories;

using StructureMap;

namespace Jhm.Web.Repository
{
    public class RepositoryDecorator<T> : IRepository<T> where T : class, IEntity
    {
        private readonly IContainer container;
        private IRepository<T> internalRepository;

        public RepositoryDecorator()
        {
            container = ObjectFactory.GetInstance<IContainer>();
            InternalRepository = ObjectFactory.GetInstance<IRepository<T>>();
        }

        public RepositoryDecorator(IContainer container, IRepository<T> inner)
        {
            this.container = container;
            InternalRepository = inner;
        }

        private IRepository<T> InternalRepository
        {
            get { return internalRepository ?? (internalRepository = container.GetInstance<IRepository<T>>()); }
            set { internalRepository = value; }
        }

        public virtual T Get(object id)
        {
            return InternalRepository.Get(id);
        }

        public virtual void Save(T entity)
        {
            InternalRepository.Save(entity);
        }

        public virtual void Delete(T entity)
        {
            InternalRepository.Delete(entity);
        }

        //public void DeleteAll(IQuery<T> query)
        //{
        //    InternalRepository.DeleteAll(query);
        //}

        //public virtual T FindOne(IQuery<T> query)
        //{
        //    return InternalRepository.FindOne(query);
        //}

        //public virtual IList<T> FindAll(IQuery<T> query)
        //{
        //    return InternalRepository.FindAll(query);
        //}
    }
}
