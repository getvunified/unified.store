﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
    public interface IHomeBannerRepository
    {
        IList<HomeBannerItem> GetAll();

        IList<HomeBannerItem> GetAllActive();

        IList<HomeBannerItem> GetAllActiveByCompany(Company company);

        HomeBannerItem GetById(Guid id);

        void SaveChanges();

        IList<HomeBannerItem> GetAllActiveByCompanyCode(int companyCode);

        IEnumerable<HomeBannerCompany> GetAllBannerCompanies();

        void SaveChanges(object entity);
    }
}
