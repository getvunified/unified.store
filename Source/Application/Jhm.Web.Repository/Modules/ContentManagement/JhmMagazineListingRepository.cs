﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
    public class JhmMagazineListingRepository : RepositoryDecorator<JhmMagazineListingItem>, IJhmMagazineListingRepository
    {
        #region Implementation of IJhmMagazineListingRepository

        public IList<JhmMagazineListingItem> GetAll()
        {
            return FluentNHibernateHelper.CurrentSession.Query<JhmMagazineListingItem>()
                .OrderByDescending(x => x.DateCreated)
                .ToList();
        }

        public IList<JhmMagazineListingItem> GetAllActive()
        {
            return FluentNHibernateHelper.CurrentSession.Query<JhmMagazineListingItem>()
                .Where(x => x.IsActive)
                .OrderByDescending(x => x.DateCreated)
                .ToList();
        }

        public JhmMagazineListingItem GetById(Guid id)
        {
            return Get(id);
        }

        public void SaveChanges(JhmMagazineListingItem entity)
        {
            Save(entity);
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        #endregion
    }
}
