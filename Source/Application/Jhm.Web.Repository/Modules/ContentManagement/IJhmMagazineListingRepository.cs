﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
    public interface IJhmMagazineListingRepository
    {
        IList<JhmMagazineListingItem> GetAll();

        IList<JhmMagazineListingItem> GetAllActive();

        JhmMagazineListingItem GetById(Guid id);

        void SaveChanges(JhmMagazineListingItem entity);

        void Delete(Guid id);
    }
}
