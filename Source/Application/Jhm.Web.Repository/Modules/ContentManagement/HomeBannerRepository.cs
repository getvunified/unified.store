﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Web.Core.Models;
using Jhm.FluentNHibernateProvider;
using NHibernate.Linq;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
    public class HomeBannerRepository : RepositoryDecorator<HomeBannerItem>, IHomeBannerRepository
    {
        public IList<HomeBannerItem> GetAll()
        {
            return FluentNHibernateHelper.CurrentSession.Query<HomeBannerItem>()
                .OrderBy(x => x.Position)
                .ToList();
        }

        public IList<HomeBannerItem> GetAllActive()
        {
            var now = DateTime.Now;
            return FluentNHibernateHelper.CurrentSession.Query<HomeBannerItem>()
                .Where(x => x.IsActive)
                .Where(x => x.StartDate == null || x.StartDate <= now)
                .Where(x => x.EndDate == null || x.EndDate >= now)
                .OrderBy(x => x.Position)
                .ToList();
        }

        public IList<HomeBannerItem> GetAllActiveByCompany(Company company)
        {
            var now = DateTime.Now;
            HomeBannerCompany c = null;
            var companyCode = company.Value;
            return FluentNHibernateHelper.CurrentSession.QueryOver<HomeBannerItem>()
                .Right.JoinAlias(x => x.Companies, () => c)
                .Where(x => x.IsActive)
                .Where(x => x.StartDate == null || x.StartDate <= now)
                .Where(x => x.EndDate == null || x.EndDate >= now)
                .Where(x => c.CompanyCode == companyCode)
                .OrderBy(x => x.Position).Asc
                .List<HomeBannerItem>();
            
        }

        public HomeBannerItem GetById(Guid id)
        {
            return FluentNHibernateHelper.CurrentSession.Get<HomeBannerItem>(id);
        }

        public IList<HomeBannerItem> GetAllActiveByCompanyCode(int companyCode)
        {
            HomeBannerCompany c = null;
            return FluentNHibernateHelper.CurrentSession.QueryOver<HomeBannerItem>()
                .Right.JoinAlias(x => x.Companies, () => c)
                .Where(x => c.CompanyCode == companyCode)
                .OrderBy(x => x.Position).Asc
                .List<HomeBannerItem>();
        }

        public IEnumerable<HomeBannerCompany> GetAllBannerCompanies()
        {
            return FluentNHibernateHelper.CurrentSession.Query<HomeBannerCompany>()
                .OrderBy(x => x.CompanyCode)
                .ToList();
        }

        public void SaveChanges()
        {
            FluentNHibernateHelper.CurrentSession.Flush();
        }

        public void SaveChanges(object entity)
        {
            FluentNHibernateHelper.CurrentSession.Save(entity);
            //FluentNHibernateHelper.CurrentSession.Flush();
        }
    }
}
