﻿using Jhm.Web.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
	public interface IEmailSubscriptionListRepository
	{
		IList<EmailSubscriptionList> GetAll();

		ENewsLetterSignUpViewModel GetSubscribedLists(string emailAddress);

		void UpdateSubscription(ENewsLetterSignUpViewModel data);
	}
}
