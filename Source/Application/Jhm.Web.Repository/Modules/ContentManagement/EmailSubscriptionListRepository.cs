﻿using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace Jhm.Web.Repository.Modules.ContentManagement
{
	public class EmailSubscriptionListRepository : RepositoryDecorator<EmailSubscriptionList>, IEmailSubscriptionListRepository
	{
		const string JHMDSBConnectionString = "Data Source=JS-SQL1;Initial Catalog=JhmDonorStudioBusiness;User Id=EmailCampaign;Password=MG2877;";
 
		public IList<EmailSubscriptionList> GetAll()
		{
			return FluentNHibernateHelper.CurrentSession.Query<EmailSubscriptionList>()
				.OrderBy(x => x.Title)
				.ToList();

		}


		public ENewsLetterSignUpViewModel GetSubscribedLists(string emailAddress)
		{
            using (var conn = new SqlConnection(JHMDSBConnectionString))
			{
				conn.Open();

				const string sql = @"SELECT List,FirstName,LastName FROM dbo.[JHM_AccountEmailLists] WHERE EmailAddress = @email";

				using (var cmd = new SqlCommand(sql, conn))
				{
					cmd.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = emailAddress;

					var reader = cmd.ExecuteReader();
					var result = new ENewsLetterSignUpViewModel();
					var lists = new List<string>(10);
					while (reader.Read())
					{
						lists.Add(reader.GetString(0));

                        if   ( (reader.GetValue(1)!=System.DBNull.Value) && (reader.GetValue(2)!=System.DBNull.Value))      // JFE Updated to formal null check 9/2
						//if (!string.IsNullOrEmpty(reader.GetString(1)) && !string.IsNullOrEmpty(reader.GetString(2)))    /// Not the proper way to find DBNull values
						{
							result.FirstName = reader.GetString(1);
							result.LastName = reader.GetString(2);
						}
					}
					result.SelectedEmailLists = lists;
					return result;
				}
			}
		}

		protected bool EMailSignUp(ENewsLetterSignUpViewModel data)
		{
			bool ReturnValue = true;

            using (var conn = new SqlConnection(JHMDSBConnectionString))
            {
                conn.Open();
                const string sql = @"INSERT INTO dbo.[JHM_AccountEmailLists] (List,EmailAddress,FirstName,LastName,AccountNumber,OptOut) 
                            VALUES (@listCode,@email,@firstName,@lastName,@accountNumber,0)";

                using (var comm = new SqlCommand(sql, conn))
                {
                    comm.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = data.Email;
                    comm.Parameters.Add("@firstName", SqlDbType.NVarChar, 300).Value =
                        String.IsNullOrEmpty(data.FirstName) ? (object)DBNull.Value : data.FirstName;
                    comm.Parameters.Add("@lastName", SqlDbType.NVarChar, 300).Value =
                        String.IsNullOrEmpty(data.LastName) ? (object)DBNull.Value : data.LastName;
                    comm.Parameters.Add("@listCode", SqlDbType.VarChar, 10);

                    comm.Parameters.Add("@accountNumber", SqlDbType.Int).Value = data.AccountNumber;
                    foreach (var emailList in data.SelectedEmailLists)
                    {
                        comm.Parameters["@listCode"].Value = emailList;
                        try
                        {
                            comm.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("IX_JHM_AccountEmailLists_Uniqueness"))
                            {
                                // Primary Key constraint error ignored
                            }
                            else
                            {
                                // other exceptions are not ignored
                                ReturnValue = false;
                            }
                        }
                    }
                }
            }
				//return Redirect("/");


			//ViewData["StateList"] = new SelectList(State.UnitedStates, "ID", "Name");
			return ReturnValue;
			//return View("ENewsLetterSignUpForm", data);
		}


		public void UpdateSubscription(ENewsLetterSignUpViewModel data)
		{
			bool ReturnValue = true;

			using (var conn = new SqlConnection(JHMDSBConnectionString))
			{
				
				
				conn.Open();

				#region SQL
				const string deleteSql = @"DELETE FROM dbo.[JHM_AccountEmailLists]
											WHERE EmailAddress = @email AND List = @listCode";
				const string addSql = @"INSERT INTO dbo.[JHM_AccountEmailLists] (List,EmailAddress,FirstName,LastName,AccountNumber,OptOut)
                            VALUES (@listCode,@email,@firstName,@lastName,@accountNumber,0)";
				
				#endregion

				if (data.EmailLists != null && data.EmailLists.Any())
				{
					var removeLists = data.SelectedEmailLists ==null? data.EmailLists.Select(x => x.Code).ToList() : data.EmailLists.Select(x => x.Code).Except(data.SelectedEmailLists).ToList();
					using (var cmd = new SqlCommand(deleteSql, conn))
					{
						cmd.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = data.Email;
						cmd.Parameters.Add("@listCode", SqlDbType.VarChar, 10);

						foreach (var item in removeLists)
						{
							cmd.Parameters["@listCode"].Value = item;
							cmd.ExecuteNonQuery();
						}
					}
				}

				if (data.SelectedEmailLists == null || !data.SelectedEmailLists.Any())
				{
					return;
				}

				using (var comm = new SqlCommand(addSql, conn))
				{
					comm.Parameters.Add("@email", SqlDbType.NVarChar, 200).Value = data.Email;
					comm.Parameters.Add("@listCode", SqlDbType.VarChar, 10);
					comm.Parameters.Add("@firstName", SqlDbType.NVarChar, 300).Value =
						String.IsNullOrEmpty(data.FirstName) ? (object)DBNull.Value : data.FirstName;
					comm.Parameters.Add("@lastName", SqlDbType.NVarChar, 300).Value =
						String.IsNullOrEmpty(data.LastName) ? (object)DBNull.Value : data.LastName;
					
					comm.Parameters.Add("@accountNumber", SqlDbType.Int).Value = data.AccountNumber;
					foreach (var emailList in data.SelectedEmailLists)
					{
						comm.Parameters["@listCode"].Value = emailList;
						try
						{
							comm.ExecuteNonQuery();
						}
						catch (Exception ex)
						{
							if (ex.Message.Contains("IX_JHM_AccountEmailLists_Uniqueness"))
							{
								// Primary Key constraint error ignored
							}
							else
							{
								throw ex;
							}
						}
					}
				}
			}
		}
	}
}
