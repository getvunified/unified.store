using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Repository.Modules.Account
{
    public partial class MockDonationRepository : IDonationRepository
    {
        private readonly List<Donation> allDonations = new List<Donation>();
        private readonly List<Donation> allUSDonations = new List<Donation>();
        private readonly List<Donation> allCanadaDonations = new List<Donation>();
        private readonly List<Donation> allUKDonations = new List<Donation>();

        public MockDonationRepository()
        {
            allUSDonations = GetRefreshedAllUSDonations();
            allCanadaDonations = GetRefreshedAllCanadaDonations();
            allDonations = allUSDonations.Concat(allCanadaDonations).Concat(allUKDonations).ToLIST();
        }



        private List<Donation> GetRefreshedAllDonations()
        {
            return GetRefreshedAllUSDonations().Concat(GetRefreshedAllCanadaDonations()).ToLIST();
        }



        private List<Donation> GetRefreshedAllUSDonations()
        {
            return new List<Donation>
                       {
                           
                           CreateUSSalt_Covenant(),
                           CreateUSSalt_Donation(),
                           CreateUSSanctuaryOfHope(),
                           CreateUSThreeHeavens(),
                           CreateUSFirstFruits(),                           
                           CreateUSFounders_Wall(),
                           CreateUSTree_of_Life(), 
                           CreateUSWalk_of_Honor(),
                           CreateUSExodus_II(), 
                           CreateUSOne_Time(), 
                           CreateUSPlant_Your_Roots(),                          
                           //CreateUSMasterYourMoneyDonation()
                       };
        }



        #region Unused Code - please factor out    JFE 12/22/2014


        //private List<Donation> GetRefreshedAllUSDonations()
        //{
        //    return new List<Donation>
        //               {
        //                   //CreateUSBornToBeBlessed(),
        //                   CreateUSSalt_Covenant(),
        //                   CreateUSFirstFruits(),
        //                   //CreateUSThe_Unity_Cross(),
        //                   //CreateUSMatthew_1820(),
        //                   //CreateUSTheComingKing(),
        //                   //CreateUSFruit_of_the_Righteous_Platter(),
        //                   //CreateUSDigitalArchiveSolution(),
        //                   CreateUSFounders_Wall(),
        //                   CreateUSTree_of_Life(), 
        //                   CreateUSWalk_of_Honor(),
        //                   //CreateUSMan_of_Vision(),
        //                   CreateUSExodus_II(), 
        //                   //CreateUSDivine_Servant(),
        //                   CreateUSOne_Time(), 
        //                   CreateUSPlant_Your_Roots(),
        //                   //CreateUSTen_Commandments_Artwork(),
        //                   //CreateUSMilitary_Warriors(),
        //                   //CreateUSPropheticBlessingsDonation()
        //                   //CreateUSFourBloodMoonsDonation(),
        //                   CreateUSMasterYourMoneyDonation()
        //               };
        //}

        private Donation CreateUSBornToBeBlessed()
        {
            const decimal amountValue = 30m;
            #region Body
            string body = @"<p>
                God�s desire is to powerfully bless and multiply his people � prophecy is that word of hope, deliverance, bounty and richness for 
                each one of us. In Born to Be Blessed, Pastor Hagee combines Scripture and the original teaching from his best-selling book The Power 
                of the Prophetic Blessing to make prophetic blessing profoundly personal for readers. An excellent resource for every parent and grandparent, 
                Born to Be Blessed includes over 75 prophetic blessings to pray over your family in all kinds of circumstances. Topics include: birth and adoption, finances, 
                marriage and dating, healing, career, purpose, encouragement, parenting, temptation, protection and many others. 
                <br/><br/>
                You can receive your signed copy of Born to Be Blessed with any gift of [C:{0}] or more. Orders will be shipped out on the release date, May 7th 2013.

                </p>".FormatWith(amountValue);
            #endregion
            var description = "Born To Be Blessed Book Donation";
            var rDonation = new Donation("Born_To_Be_Blessed_Book", "Born To Be Blessed Book", description, body, listViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Book_Small.jpg", detailViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Book_Large.jpg", formViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Book_Large.jpg");

            // Setup Included StockItems
            var product = new Product("B192");
            product.SetTitle("Born To Be Blessed Book");
            var productStockItem = new StockItem(new Guid("369F8EB0-6711-4D30-BC08-6C281AB92EB1"), MediaType.BookHardBackSigned, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(product);


            //One Time Donation (Min Requirements)
            var minAmountDonation = new MinimumAmountDonationOption(new Guid("42394C65-3C0A-4DAB-B781-614EE7E4DDED"), amountValue);
            minAmountDonation.EnableAmountOutsideOfRange();
            minAmountDonation.SetTitle("One-Time Deduction");
            minAmountDonation.SetDescription("Please Accept my gift of [C:{0}] or more to receive a signed copy of Born to Be Blessed Book.".FormatWith(amountValue));
            minAmountDonation.AddIncludedStockItem(productStockItem);

            rDonation.AddDonationOption(minAmountDonation);

            rDonation.SetProjectCode("JHM");

            return rDonation;
        }

        private Donation CreateUSThe_Unity_Cross()
        {
            const decimal amountValue = 500m;
            #region Body
            string body = @"<p>
                The unique symbolism behind the Unity Cross will be a constant reminder of your support for John Hagee Ministries, 
                helping to spread All Of The Gospel To All Of The World and Every Generation.  This timeless cross starts with a solid foundation, 
                symbolizing the Word of God that helps us through all of life�s challenges.
                <br/><br/>
                The external cross is comprised of straight lines, representing the straight-forward and boldly-proclaimed Word of God delivered 
                from the platform at Cornerstone Church.  This outer cross would not be complete without the intricate interior design, 
                which represents those who have heard the Gospel and have been changed by the power of its truth.
                <br/><br/>
                Both crosses are attached by three nails, representing God our Father, Christ our Savior and the Holy Spirit.  
                When these two crosses come together, it is a beautiful representation of our strength through the unifying power of God�s Word.
                </p>".FormatWith(amountValue);
            #endregion

            const string description = "Unity Cross Donation";
            var rDonation = new Donation("The_Unity_Cross", "The Unity Cross", description, body, listViewImageUrl: "~/Content/images/UnityCross-small.jpg", detailViewImageUrl: "~/Content/images/UnityCross-large.jpg", formViewImageUrl: "~/Content/images/UnityCross-large.jpg");

            // Setup Included StockItems
            var product = new Product("K521");
            product.SetTitle("The Unity Cross");
            var productStockItem = new StockItem(new Guid("E8EACE5D-F92E-41C8-BD7E-182122C1F31C"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(product);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("B19C98AA-A47A-45CE-9117-63B09FB5B58E"), amountValue);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(productStockItem);

            rDonation.SetProjectCode("OUT");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation CreateUSMatthew_1820()
        {
            const decimal amountValue = 2500m;
            #region Body
            string body = @"<p>
                A Bas Relief design, created exclusively for John Hagee Ministries, can be yours for a gift of [C:{0}].  
                This beautifully framed rendition of Matthew 18:20 is sure to proclaim that Jesus Christ is the head of your household.  
                Made using an ancient process on handmade papyrus, this 27� x 31� contemporary rendition is a great conversation piece, 
                and a reminder that Christ is always with you.</p>".FormatWith(amountValue);
            #endregion

            const string description = "Matthew 18:20 Donation";
            var rDonation = new Donation("Matthew_1820", "Matthew 18:20", description, body, listViewImageUrl: "~/Content/images/Matt1820-small.jpg", detailViewImageUrl: "~/Content/images/Matt1820-large.jpg", formViewImageUrl: "~/Content/images/Matt1820-large.jpg");

            // Setup Included StockItems
            var product = new Product("MATT18");
            product.SetTitle("Matthew 18:20 Framed Art");
            var productStockItem = new StockItem(new Guid("609D7B9D-FBAC-491D-BB73-8ABC2CE66AD0"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(product);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("798F0746-93B4-4B66-80AC-8CE09C61EADF"), amountValue);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(productStockItem);

            rDonation.SetProjectCode("OUT");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation CreateUSFruit_of_the_Righteous_Platter()
        {
            const decimal amountValue = 5000m;
            #region Body
            string body = @"<p>
                This exquisite 18� glass platter is inlaid with 24k gold, and boasts the Scripture found in Proverbs 11:30.  
                This beautiful, custom-designed work of art, made exclusively for John Hagee Ministries by Schlanser Design Studios, 
                can be yours for a gift of [C:{0}]. 
                <br/><br/>
                Let this stunning platter be a constant reminder of your dedication to winning lost souls for Christ by feeding them from the Word of God.</p>".FormatWith(amountValue);
            #endregion

            const string description = "Fruit of Righteousness Donation";
            var rDonation = new Donation("Fruit_of_the_Righteous", "Fruit of the Righteous Platter", description, body, listViewImageUrl: "~/Content/images/FruitRightPlatter-small.jpg", detailViewImageUrl: "~/Content/images/FruitRightPlatter-large.jpg", formViewImageUrl: "~/Content/images/FruitRightPlatter-large.jpg");

            // Setup Included StockItems
            var product = new Product("PLTR18");
            product.SetTitle("Fruit of the Righteous Platter");
            var productStockItem = new StockItem(new Guid("FBDA753D-BDD3-4C9C-8CEF-8C6869F6C71F"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(product);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("D8791023-BBF9-44F6-A098-FD7F20491EC9"), amountValue);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(productStockItem);

            rDonation.SetProjectCode("OUT");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation CreateUSTheComingKing()
        {
            const decimal amountValue = 5000m;
            #region Body
            string body = @"<p>As a ministry we are constantly striving to maintain the highest quality of technology available in the marketplace.  Your [C:{0}] pledge will allow us to reach the high-tec generation by giving us the funds necessary to broadcast in high-def in venues we have never seen before.
            ""The Coming King"" clock was exclusively designed by Max Greiner for John Hagee Ministries and our Operation Outreach.  Mark 13:32 says, ""No one knows about that day or hour, not even the angels in heaven, nor the Son, but only the Father.""  
            <br/>
            <br/>
            This beautiful clock encased in walnut, holds the artistic representation of this Scripture.  According to the artist, there is no other dramatic creation like this one in existence.
            Let this beautifully sculpted clock be a reminder that Christ�s triumphant return is just around the corner!  Let�s make a difference with the time we have left here on Earth!
            </p>".FormatWith(amountValue);
            #endregion

            const string description = "The Coming King Donation";
            var rDonation = new Donation("The_Coming_King", "The Coming King", description, body, listViewImageUrl: "~/Content/images/TheComingKing-small.jpg", detailViewImageUrl: "~/Content/images/TheComingKing-large.jpg", formViewImageUrl: "~/Content/images/TheComingKing-large.jpg");

            // Setup Included StockItems
            var product = new Product("TCK");
            product.SetTitle("The Coming King Clock");
            var productStockItem = new StockItem(new Guid("5F12B00B-01B3-4FE5-96E1-CF18AB970D3D"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(product);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("8AD047E0-1DA0-419D-B356-4351B39F328A"), amountValue);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(productStockItem);

            rDonation.SetProjectCode("OUT");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation CreateUSBornToBeBlessedBracelets()
        {
            #region Body

            const string body = @"<p>For your gift of $25 we will send you two �Born to Be Blessed� bracelets that are a daily 
                                    reminder of what God intended you to be. You were born for His purpose and His glory!</p>";

            #endregion

            const string description = "Born To Be Blessed Offering Donation";
            var rDonation = new Donation("Born_To_Be_Blessed", "Born To Be Blessed Offering", description, body, listViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Small.jpg", detailViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Large.jpg", formViewImageUrl: "~/Content/images/Born_To_Be_Blessed_Large.jpg");

            var bornToBeBlessedOffering = new AnyAmountDonationOption(new Guid("bad7007f-f58b-4feb-a3ba-9d84fdb97ce1"));
            bornToBeBlessedOffering.SetTitle("Born To Be Blessed Offering");
            bornToBeBlessedOffering.SetDescription("I would like to give an offering in the amount of:");

            rDonation.SetProjectCode("JHM");

            rDonation.AddDonationOption(bornToBeBlessedOffering);

            return rDonation;
        }

        #endregion 



        private Donation CreateUSFirstFruits()
        {
            #region Body

            const string body = @"<img src='~\Content\shop\images\shop\firstfruits-bkgnd.jpg' /><br/><br/>
                            <h3>First Fruits Offering</h3>
                            <p>Put God first in everything you give.</p>
                            <p><span style='color:yellow'>Thank you for supporting the Gospel of Jesus Christ.</span></p>
                            <p>By sowing your seed into the work at John Hagee Ministries, you are winning souls to the saving 
                            knowledge of Jesus Christ and changing lives with the powerful Word of the living God! Your firstfruits
                            are making a profound difference, as daily we receive life changing testimonies of healed bodies, 
                            restored marriages and new souls touched by the miracle power of the Gospel!  Your seed will produce
                            a harvest for all eternity. </p>";

            #endregion

            const string description = "Firstfruits Offering Donation";
            var rDonation = new Donation("First_Fruits", "Firstfruits Offering", description, body, listViewImageUrl: "~/Content/images/firstfruits-sm.png", detailViewImageUrl: "~/Content/images/firstfruits-lg.png", formViewImageUrl: "~/Content/images/firstfruits-lg.png");

            var firstFruitsOffering = new AnyAmountDonationOption(new Guid("F71C6905-5801-45F1-9B19-3CAF8B8EACA0"));
            firstFruitsOffering.SetTitle("Firstfruits Offering");
            firstFruitsOffering.SetDescription("The Lord has directed me to give my firstfruits offering in the amount below.");

            rDonation.SetProjectCode("FIRST");

            rDonation.AddDonationOption(firstFruitsOffering);

            return rDonation;
        }

        public Donation Get(object id)
        {
            return allDonations.Find(x => x.Id == id.ToString().TryConvertTo<Guid>());
        }
        private Donation CreateNewDonation(string donationCode)
        {
            return GetStubbedDonations(1, donationCode).ToLIST()[0];
        }
        public void Save(Donation entity)
        {

        }
        public void Delete(Donation entity)
        {

        }
        public IEnumerable<Donation> GetActiveDonationOpportunities()
        {
            //NOTE:  Returning refreshed list each time
            //return allDonations;
            return GetRefreshedAllUSDonations();
        }
        public IDonation GetDonation(string code)
        {
            //NOTE:  Returning refreshed list each time
            //return allDonations.Find(x => x.DonationCode == code);
            return GetRefreshedAllUSDonations().Find(x => x.DonationCode.Equals(code, StringComparison.InvariantCultureIgnoreCase));
        }

        public IDonation GetUSDonation(string donationCode)
        {
            return GetRefreshedAllUSDonations().Find(x => x.DonationCode.Equals(donationCode, StringComparison.InvariantCultureIgnoreCase));
        }

        #region All Active Donation Opportunites (Setup Area)


        private Donation CreateUSSalt_Covenant()
        {
            #region Body

            const string body =
                @"
                <p>
                    &quot;Season all your grain offerings with salt. Do not leave the salt
                    <br />
                    of the covenant of your God out of your grain offerings;
                    <br />
                    add salt to all your offerings.&quot; Leviticus 2:13<br />
                </p>
                <p>
                    &quot;Don�t you know that the Lord, the God of Israel, has given
                    <br />
                    the kingship of Israel to David and his descendants forever<br />
                    by a covenant of salt?&quot; 2 Chronicles 13:5<br />
                </p>
                <p>
                    Over the years the Lord has led many precious friends of this ministry to become
                    <b>SALT COVENANT PARTNERS</b> through both prayer and financial support. If you
                    are currently a partner with us you �go the extra mile� by making a monthly financial
                    commitment for the costs of our television outreach and we thank you for your sacrifice!
                </p>
                <p>
                    If you are not a partner with John Hagee Ministries you may be asking the question,
                    <b>�WHAT IS A SALT COVENANT?�</b>
                    <br />
                    In biblical times men carried a pouch of salt on their belts to prevent dehydration
                    as well as to trade it as a form of currency. When two men desired to finalize a
                    business transaction or enter into a covenant of loyalty with each other, they would
                    recite the conditions of their covenant and then exchange salt from each other�s
                    pouch. Once they exchanged salt, they shook the salt grains within their pouches
                    symbolizing the sealing of the covenant which became binding for it was impossible
                    to retrieve their original salt grains from one another.
                </p>
                <p>
                    <b>Who is a Salt Covenant Partner?</b>
                </p>
                <p>
                    <b>Your Commitment To Us.</b><br />
                    A SALT COVENANT PARTNER of John Hagee Ministries is someone who pledges to send
                    an offering of any amount to John Hagee Ministries each month or on a continual
                    basis each year to help us proclaim the Gospel of Jesus Christ to America and the
                    nations of the world. Our partners also pledge to pray daily for our ministry as
                    together we take the Good News to the lost.
                </p>
                <p>
                    <b>Our Commitment To You.</b><br />
                    Our Salt Covenant with our partners is to be absolutely committed to change America
                    and the world by being obedient to the Great Commission, to win the lost to Christ,
                    to take America and the world back to the God of Abraham, Isaac and Jacob. Pastor
                    Hagee promises to preach an uncompromised message of love, healing and deliverance
                    to the lost, hurting and oppressed. In addition our ministry also pledges to pray
                    for our partners and their loved ones on a daily basis.
                </p>
                <p>
                    <b>How Does One Become A Salt Covenant Partner?</b><br />
                    You may become a SALT COVENANT PARTNER today by simply completing the enclosed form.
                    With your initial gift you will receive two salt pouches symbolizing your covenant
                    with Pastor and Diana Hagee, a CD titled What Is a Salt Covenant and a Salt Covenant
                    Partner Card.
                </p>
                <p>
                    <b>The Salt Covenant Partner benefits include:</b><br />
                    <ul>
                        <li>10% discount on ministry material</li>
                        <li>Our staff prays for you daily</li>
                        <li>Toll-free number to our Partner Relations Department for special prayer needs</li>
                        <li>Advance seating at special John Hagee Ministry sponsored events</li>
                        <li>Early notice on ministry partner trips</li>
                        <li>We will inform you when Pastor Hagee is planning to be in your area</li>
                        <li>Bi-monthly ministry magazine</li>
                        <li>Teleministry prayer partners will stay in touch with you</li>
                        <li>Periodic updates from Pastor and Diana including weekly e-mails from Pastor Hagee�s
                            desk</li>
                    </ul>
                </p>";

            #endregion

            #region Description

            const string description =
                @"<p>Will you join us in a covenant of loyalty to Christ?</p>
<p>Over the years, the Lord has led many precious friends to become partners with this ministry through their financial support. Our Salt Covenant partners are those who ""go the extra mile"" by making a commitment to send a monthly gift for the ongoing costs of our television outreach.</p>
<p>What is a Salt Covenant?";

            #endregion


            var rDonation        = new Donation("Salt_Covenant", "Salt Covenant Partnership", description, body, listViewImageUrl: "~/Content/images/salt-sm.jpg", detailViewImageUrl: "~/Content/images/salt-lg.jpg", formViewImageUrl: "~/Content/images/salt-lg.jpg");
            var oneTimeOptionNEW = new AnyAmountDonationOption(NewSaltPartnerOptionId);

            oneTimeOptionNEW.SetTitle("I want to become a Salt Covenant Partner");
            oneTimeOptionNEW.SetDescription("Pastor Hagee, I want to become a Salt Covenant Partner. I pledge a monthly gift to help you continue to share the Gospel with all the world. Here is my gift toward my Salt Covenant Pledge.");

            rDonation.AddDonationOption(oneTimeOptionNEW);

            rDonation.SetProjectCode("SALT");
            rDonation.DonationId = SaltCovenantId;
            return rDonation;
        }

        private Donation CreateUSSalt_Donation() {
            #region Body

            const string body =
                @"
                <p>
                    &quot;Season all your grain offerings with salt. Do not leave the salt
                    <br />
                    of the covenant of your God out of your grain offerings;
                    <br />
                    add salt to all your offerings.&quot; Leviticus 2:13<br />
                </p>
                <p>
                    &quot;Don�t you know that the Lord, the God of Israel, has given
                    <br />
                    the kingship of Israel to David and his descendants forever<br />
                    by a covenant of salt?&quot; 2 Chronicles 13:5<br />
                </p>
                <p>
                    Over the years the Lord has led many precious friends of this ministry to become
                    <b>SALT COVENANT PARTNERS</b> through both prayer and financial support. If you
                    are currently a partner with us you �go the extra mile� by making a monthly financial
                    commitment for the costs of our television outreach and we thank you for your sacrifice!
                </p>
                <p>
                    If you are not a partner with John Hagee Ministries you may be asking the question,
                    <b>�WHAT IS A SALT COVENANT?�</b>
                    <br />
                    In biblical times men carried a pouch of salt on their belts to prevent dehydration
                    as well as to trade it as a form of currency. When two men desired to finalize a
                    business transaction or enter into a covenant of loyalty with each other, they would
                    recite the conditions of their covenant and then exchange salt from each other�s
                    pouch. Once they exchanged salt, they shook the salt grains within their pouches
                    symbolizing the sealing of the covenant which became binding for it was impossible
                    to retrieve their original salt grains from one another.
                </p>
                <p>
                    <b>Who is a Salt Covenant Partner?</b>
                </p>
                <p>
                    <b>Your Commitment To Us.</b><br />
                    A SALT COVENANT PARTNER of John Hagee Ministries is someone who pledges to send
                    an offering of any amount to John Hagee Ministries each month or on a continual
                    basis each year to help us proclaim the Gospel of Jesus Christ to America and the
                    nations of the world. Our partners also pledge to pray daily for our ministry as
                    together we take the Good News to the lost.
                </p>
                <p>
                    <b>Our Commitment To You.</b><br />
                    Our Salt Covenant with our partners is to be absolutely committed to change America
                    and the world by being obedient to the Great Commission, to win the lost to Christ,
                    to take America and the world back to the God of Abraham, Isaac and Jacob. Pastor
                    Hagee promises to preach an uncompromised message of love, healing and deliverance
                    to the lost, hurting and oppressed. In addition our ministry also pledges to pray
                    for our partners and their loved ones on a daily basis.
                </p>
                <p>
                    <b>How Does One Become A Salt Covenant Partner?</b><br />
                    You may become a SALT COVENANT PARTNER today by simply completing the enclosed form.
                    With your initial gift you will receive two salt pouches symbolizing your covenant
                    with Pastor and Diana Hagee, a CD titled What Is a Salt Covenant and a Salt Covenant
                    Partner Card.
                </p>
                <p>
                    <b>The Salt Covenant Partner benefits include:</b><br />
                    <ul>
                        <li>10% discount on ministry material</li>
                        <li>Our staff prays for you daily</li>
                        <li>Toll-free number to our Partner Relations Department for special prayer needs</li>
                        <li>Advance seating at special John Hagee Ministry sponsored events</li>
                        <li>Early notice on ministry partner trips</li>
                        <li>We will inform you when Pastor Hagee is planning to be in your area</li>
                        <li>Bi-monthly ministry magazine</li>
                        <li>Teleministry prayer partners will stay in touch with you</li>
                        <li>Periodic updates from Pastor and Diana including weekly e-mails from Pastor Hagee�s
                            desk</li>
                    </ul>
                </p>";

            #endregion

            #region Description

            const string description =
                @"<p>Will you join us in a covenant of loyalty to Christ?</p>
<p>Over the years, the Lord has led many precious friends to become partners with this ministry through their financial support. Our Salt Covenant partners are those who ""go the extra mile"" by making a commitment to send a monthly gift for the ongoing costs of our television outreach.</p>
<p>What is a Salt Covenant?";

            #endregion


            var rDonation             = new Donation("Salt_Donation", "Salt Covenant Partnership Donation", description, body, listViewImageUrl: "~/Content/images/salt-sm.jpg", detailViewImageUrl: "~/Content/images/salt-lg.jpg", formViewImageUrl: "~/Content/images/salt-lg.jpg");
            var oneTimeOptionExisting = new AnyAmountDonationOption(ExistingSaltPartnerOptionId);

            oneTimeOptionExisting.SetTitle("I am already a Salt Covenant Partner");
            oneTimeOptionExisting.SetDescription("Pastor Hagee, I am already a Salt Covenant Partner, here is my gift toward my Salt Covenant Pledge.");

            rDonation.AddDonationOption(oneTimeOptionExisting);

            rDonation.SetProjectCode("SALT");
            rDonation.DonationId = SaltCovenantId;
            return rDonation;
        }


        private Donation CreateUSThreeHeavens()
        {
            #region Body

            const string body = @"
                <p>BATTLES ARE RAGING BETWEEN ANGELS AND DEMONS OVER YOU</p>
                <p>John Hagee�s breathtaking biblical tour of the three heavens takes you inside the timeless clash between the Kingdom of Light and the Kingdom of Darkness and explains why that battle makes all the difference in this world and the world to come.</p>
                <p>In The Three Heavens, Hagee uses the Word of God, science, and incredible true stories of the supernatural to explore the First Heaven. He then exposes Satan�s diabolical tactics in the Second Heaven and how they affect each one of us. Finally, he looks at the deep riches of the Third Heaven and how our hope of that eternal home changes our life on earth.</p>
                <p>Personally signed by Pastor Hagee, this commemorative collector�s edition is a blue leather, burnished hardback book with silver gilded pages. Exclusive to John Hagee Ministries and not available in stores, this is a beautiful edition to any library or private collection.</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("B206HS");
            item.SetTitle("The Three Heavens Collector�s Edition");
            var stockItem = new StockItem(new Guid("F9EAEADA-455C-4578-A8FE-C9E68A4D180F"), MediaType.Book, 3000, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "The Three Heavens Collector�s Edition";
            var rDonation = new Donation("TheThreeHeavens", "The Three Heavens", description, body, listViewImageUrl: "~/Content/images/ThreeHeavensDonationOpp/3HBook.jpg", detailViewImageUrl: "~/Content/images/ThreeHeavensDonationOpp/3HBanner.jpg", formViewImageUrl: "~/Content/images/ThreeHeavensDonationOpp/3HBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(new Guid("3ad44c48-9955-4f37-a645-d272801c8bf1"), 50m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("BOOK");

            rDonation.AddDonationOption(oneTimeOption);


            return rDonation;
        }

        private Donation CreateUSSanctuaryOfHope()
        {
            #region Body

            //const string body = @"<p>With a one-time gift toward the John Hagee Ministries Sanctuary of Hope, your donation will go towards building a boarding house for students located on the Cornerstone Christian Schools campus.</p><p>Donated funds will pay for their housing needs and tuition along with food and clothing, giving these children the opportunity to have a world-class Christian education and build godly relationships with those around them, truly teaching them about the Love of Christ in action.</p><i>&quot;Be diligent to present yourself approved to God, a worker who does not need to be ashamed, rightly dividing the word of truth.&quot;2 Timothy 2:15</i>";

            const string body = @"<p>Sanctuary of Hope is an oasis of hope for children that are seeking spiritual guidance and opportunity. It is a comprehensive, long-term concept to save the youth of our city and America.</p><p>
When Pastor John Hagee was a teenager, he worked many different jobs. One that has left an indelible impression upon him was the time he spent working in an orphanage. &quotI was privileged in the time that I was there to see life through the eyes of an orphan.&quot He recalls the young boys who would chat amongst themselves saying, &quotThis week, my mom�s going to come get me. My dad is coming finally�and he�s going to be driving a big, new car.&quot The day would come, and no one came. Not even for a visit. Week after week this happened and then it dawned on them. &quotNo one�s coming for me; I�m by myself. I�m completely alone.&quot </p><p>
This is where the idea for Sanctuary of Hope was born. Pastor says, &quotI know that it is an ocean of need. But it is wrong to see a need and not try to meet that need. We can help some of them. And because we can, we should. And because this is the Commission of God, we are.&quot </p><p>
First, saving children begins with saving their physical life. America is losing 1000 babies a day to abortion, hindering our future and hope as nation. Secondly, children need a loving home where they can be nurtured in a Christ-centered environment. Finally, children should be educated in a righteous environment, free from the secular indoctrination that is prevalent in the school systems today. </p><p>
With these three foundational statements, donated funds for this outreach will go towards the Sanctuary of Hope with three divisions: </p>
<p>
<b>Division One:</b> An unwed mother's facility where young women who are pregnant and do not want to terminate their pregnancy would be invited to stay until the baby is born. The newborn baby could be adopted by a godly family of proven character or raised in the Sanctuary of Hope orphanage. </p>
<p>
<b>Division Two:</b> The building and development of a state-of-the-art orphanage in the San Antonio area.  </p>
<p>
<b>Division Three: </b>The building of a dormitory for a world-class boarding school where students can attend one of the finest Christian schools in the country, Cornerstone Christian Schools. </p>
<p>
This cannot be accomplished without you. Love is not what you say. Love is what you do. </p>
<p>
Won�t you please join us at John Hagee Ministries in this endeavor to meet the need of these priceless treasures, our children? As Pastor Hagee says, &quotWhen I get to heaven, God isn�t going to pat me on the back for erecting buildings to help children. He is going to reward those who gave and made it possible.&quot </p>";

            #endregion

            var description = "Sanctuary of Hope Donation";
            var rDonation = new Donation("SanctuaryOfHope", "Sanctuary of Hope", description, body, listViewImageUrl: "~/Content/images/Sanctuary_Hope-sm-V2.jpg", detailViewImageUrl: "~/Content/images/Sanctuary_Hope-banner.jpg", formViewImageUrl: "~/Content/images/Sanctuary_Hope-banner.jpg");


            // Setup Included StockItems
            //var FWPaver = new Product("FWP");
            //FWPaver.SetTitle("Founder's Wall Plaque");
            //var FWPaverStockItem = new StockItem(new Guid("E2F416D2-6F8D-4667-86F4-4755F9A8029D"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(FWPaver);

            var oneTimeOption = new AnyAmountDonationOption(new Guid("e320dc47-49a3-4693-944b-722e2d141bc7"));
            //var oneTimeOption = new MinimumAmountDonationOption(new Guid("e320dc46-49a2-4692-944a-722e2d141bc6"), 1000m);
            //var oneTimeOption = new AmountRangeDonationOption(new Guid("e320dc46-49a2-4692-944a-722e2d141bc6"), 1000m, 5000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");
            //oneTimeOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:", maxLength: 40);
            //oneTimeOption.AddIncludedStockItem(FWPaverStockItem);

            //Quarterly Option
            //var quarterlyOption = new AmountRangeDonationOption(new Guid("7db0b677-a470-4919-91c0-62575cde64df"), 250.00m, 1250.00m);
            //quarterlyOption.SetTitle("Quarterly - Automatic Deduction");
            //quarterlyOption.SetDescription("Beginning this month, I pledge a quarterly gift in the amount below.");
            //quarterlyOption.AddDonationFrequency(null, new[] { DonationFrequency.QuarterlyResidual });
            //quarterlyOption.DisableOneTimeDonationFrequency();
            //quarterlyOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:");

            //Monthly Option
            //var monthlyOption = new AmountRangeDonationOption(new Guid("48e914b6-9114-4bd2-9c7d-3a1bed53d068"), 83.33m, 416.66m);
            //monthlyOption.SetTitle("Monthly - Automatic Deduction");
            //monthlyOption.SetDescription("Beginning this month, I pledge a monthly gift in the amount below.");
            //monthlyOption.AddDonationFrequency(null, new[] { DonationFrequency.MonthlyResidual });
            //monthlyOption.DisableOneTimeDonationFrequency();
            //monthlyOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:");

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(quarterlyOption);
            //rDonation.AddDonationOption(monthlyOption);


            return rDonation;
        }



        private Donation CreateUSFounders_Wall()
        {
            #region Body

            const string body = @"<p>With a one-time minimum gift of $1,000 toward the John Hagee Ministries
International Media Center, your name or the name of a loved one can be commemorated with a
beautiful plaque adorning the Founder�s Wall in our prayer room.</p>

<i>&quot;Be diligent to present yourself approved to God, a worker who does not need to be ashamed, rightly dividing the word of truth.&quot;
2 Timothy 2:15</i>";

            #endregion

            var description = "Founders Wall Donation";
            var rDonation = new Donation("Founders_Wall", "Founders Wall", description, body, listViewImageUrl: "~/Content/images/founderswall-sm.jpg", detailViewImageUrl: "~/Content/images/founderswall-lg.jpg", formViewImageUrl: "~/Content/images/founderswall-lg.jpg");


            // Setup Included StockItems
            var FWPaver = new Product("FWP");
            FWPaver.SetTitle("Founder's Wall Plaque");
            var FWPaverStockItem = new StockItem(new Guid("E2F416D2-6F8D-4667-86F4-4755F9A8029D"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(FWPaver);

            var oneTimeOption = new MinimumAmountDonationOption(new Guid("e320dc46-49a2-4692-944a-722e2d141bc6"), 1000m);
            //var oneTimeOption = new AmountRangeDonationOption(new Guid("e320dc46-49a2-4692-944a-722e2d141bc6"), 1000m, 5000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below. Please add my name to the Founders Wall.");
            oneTimeOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:", maxLength: 40);
            oneTimeOption.AddIncludedStockItem(FWPaverStockItem);

            //Quarterly Option
            //var quarterlyOption = new AmountRangeDonationOption(new Guid("7db0b677-a470-4919-91c0-62575cde64df"), 250.00m, 1250.00m);
            //quarterlyOption.SetTitle("Quarterly - Automatic Deduction");
            //quarterlyOption.SetDescription("Beginning this month, I pledge a quarterly gift in the amount below.");
            //quarterlyOption.AddDonationFrequency(null, new[] { DonationFrequency.QuarterlyResidual });
            //quarterlyOption.DisableOneTimeDonationFrequency();
            //quarterlyOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:");

            //Monthly Option
            //var monthlyOption = new AmountRangeDonationOption(new Guid("48e914b6-9114-4bd2-9c7d-3a1bed53d068"), 83.33m, 416.66m);
            //monthlyOption.SetTitle("Monthly - Automatic Deduction");
            //monthlyOption.SetDescription("Beginning this month, I pledge a monthly gift in the amount below.");
            //monthlyOption.AddDonationFrequency(null, new[] { DonationFrequency.MonthlyResidual });
            //monthlyOption.DisableOneTimeDonationFrequency();
            //monthlyOption.SetRequiredOptionField("Wall Plaque", "Please enter name for Wall Plaque:");

            rDonation.SetProjectCode("HONOR");

            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(quarterlyOption);
            //rDonation.AddDonationOption(monthlyOption);


            return rDonation;
        }

        private Donation CreateUSTree_of_Life()
        {
            #region Body

            const string body = @"
            <p>
                The Tree of Life is a beautiful sculpture cast in bronze bearing leaves of polished
                brass permanently located in the lobby of John Hagee Ministries. The leaves that
                hang from the many branches of this magnificent tree are inscribed with the name
                of each person who has given a gift of $1000 or more to John Hagee Ministries for
                the purpose of preaching the Gospel. Each contribution is a memorial of the giver�s
                devotion to the Great Commission for years to come.
            </p>
            <p>
                If you would like your name or the name of a loved one commemorated on this magnificent
                symbol of life please complete the form below.
            </p>
            <p>
                <i>�He who has an ear, let him hear what the Spirit says to the churches. To him who
                    overcomes, I will give the right to eat from the tree of life, which is in the Paradise
                    of God.� Revelation 2:7 </i>
            </p>";

            #endregion

            var description = "Tree of Life Donation";
            var rDonation = new Donation("Tree_of_Life", "Tree of Life", description, body, listViewImageUrl: "~/Content/images/treeoflife-sm.jpg", detailViewImageUrl: "~/Content/images/treeoflife-lg.jpg", formViewImageUrl: "~/Content/images/treeoflife-lg.jpg");


            // Setup Included StockItems
            var brassLeaf = new Product("TOLP");
            brassLeaf.SetTitle("Brass Leaf");
            var brassLeafStockItem = new StockItem(new Guid("1E3DE338-0E42-4C06-9351-1423E074CF6E"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(brassLeaf);


            var oneTimeOption = new MinimumAmountDonationOption(new Guid("f3eb894b-3e22-4a1d-bc5d-bb881666591c"), 1000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.MinAmount));
            oneTimeOption.SetRequiredOptionField("Leaf", "Please enter name for Leaf:", maxLength: 40);
            oneTimeOption.AddIncludedStockItem(brassLeafStockItem);

            //Monthly Option
            //var monthlyOption = new FixedAmountDonationOption(new Guid("b697659c-c3c8-4fd5-93c5-777453360879"), 83.33m);
            //monthlyOption.SetTitle("Monthly - Automatic Deduction");
            //monthlyOption.SetDescription("Beginning this month, I pledge a monthly gift in the amount of {0}. <blockquote>(*Charge will be deducted each month on the date of initial transaction and the Leaf will be added to the Tree of Life upon completion of pledge) </blockquote>".FormatWith(monthlyOption.Amount.ToString("C", Thread.CurrentThread.CurrentCulture)));
            //monthlyOption.AddDonationFrequency(null, new[] { DonationFrequency.MonthlyResidual });
            //monthlyOption.DisableOneTimeDonationFrequency();
            //monthlyOption.SetRequiredOptionField("Leaf", "Please enter name for Leaf:");

            //Quarterly Option
            //var quarterlyOption = new FixedAmountDonationOption(new Guid("a801890c-5319-4133-be42-752436f74337"), 250m);
            //quarterlyOption.SetTitle("Quarterly - Automatic Deduction");
            //quarterlyOption.SetDescription("Beginning this month, I pledge a quarterly gift in the amount of {0}. <blockquote>(*Charge will be deducted each three months on the date of initial transaction and the Leaf will be added to the Tree of Life upon completion of pledge) </blockquote>".FormatWith(monthlyOption.Amount.ToString("C", Thread.CurrentThread.CurrentCulture)));
            //quarterlyOption.AddDonationFrequency(null, new[] { DonationFrequency.QuarterlyResidual });
            //quarterlyOption.DisableOneTimeDonationFrequency();
            //quarterlyOption.SetRequiredOptionField("Leaf", "Please enter name for Leaf:");

            rDonation.SetProjectCode("HONOR");

            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(quarterlyOption);
            //rDonation.AddDonationOption(monthlyOption);


            return rDonation;
        }
        private Donation CreateUSWalk_of_Honor()
        {
            #region Body

            const string body = @"
            <p>
                How beautiful on the mountains are the feet of those who bring good news, who proclaim
                peace, who bring good tidings, who proclaim salvation, who say to Zion, �Your God
                reigns!� Isaiah 52:7
            </p>
            <p>
                Commemorate the name of a loved one or simply take a stand for the Gospel by placing
                your name on one or more pavers. These personalized pavers are located on the walkway
                of our John Hagee Ministries International Media Center. If you would like to join
                the thousands that have already contributed to the Walk of Honor, please complete
                the form below. 
            </p>
            <p>
                <i>(If you are already a Salt Covenant Partner, please know that this program is intended
                    to raise funds over and above the amount already pledged to Salt.) </i>
            </p>
            <p>
                <i>Dedication Opportunities �The kingdom of heaven is like treasure hidden in a field.
                    When a man found it, he hid it again, and then in his joy went and sold all he had
                    and bought that field.� Matthew 13:44 </i>
            </p>";

            #endregion

            var description = "Walk of Honor Donation";
            var rDonation = new Donation("Walk_of_Honor", "Walk Of Honor", description, body, listViewImageUrl: "~/Content/images/walkhonor-sm.jpg", detailViewImageUrl: "~/Content/images/walkhonor-lg.jpg", formViewImageUrl: "~/Content/images/walkhonor-lg.jpg");

            // Setup Included StockItems
            var brickPaver = new Product("K220");
            brickPaver.SetTitle("Brick Paver");
            var brickPaverStockItem = new StockItem(new Guid("016316BA-8AA5-49F2-A000-5E7170FD8DF1"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(brickPaver);

            var option = new MultipleOfdonationOption(new Guid("60798d66-b677-47f7-89e1-6fe75df59299"), 100.00m);
            option.SetTitle("One-Time Deduction");
            option.SetDescription("Please enter the name for each paver you would like. The cost is $100 per paver.");
            option.SetRequiredOptionField("Paver", "Please enter name for Paver:", maxLength: 40);
            option.EnableCertificateRelatedMultiple();
            option.AddIncludedStockItem(brickPaverStockItem);


            rDonation.SetProjectCode("HONOR");

            rDonation.AddDonationOption(option);

            return rDonation;
        }
        private Donation CreateUSMan_of_Vision()
        {
            #region Body

            const string body = @"
                <p>
                    <b>A 328 Page, Full Color, Commemorative Limited Edition Book</b>
                    <br />
                    Featuring photos, articles and memories capturing the life and ministry of Pastor
                    Hagee. Also included are letters of Tribute from Heads of State, dignitaries, colleagues,
                    and friends.
                </p>
                <p>
                    <i>The Lord instructed Moses on Mount Sinai, �Consecrate the fiftieth year and proclaim
                        liberty throughout the land to all its inhabitants... For it is a jubilee and is
                        to be holy for you.� Leviticus 25:10, 12 (NIV) </i>
                </p>
            ";

            #endregion

            var rDonation = new Donation("Man_of_Vision", "John Hagee: Man of Vision - 50th Book", string.Empty, body, listViewImageUrl: "~/Content/images/50thbook-sm.jpg", detailViewImageUrl: "~/Content/images/50thbook-lg.jpg", formViewImageUrl: "~/Content/images/50thbook-lg.jpg");

            var b144Option = new MinimumAmountDonationOption(new Guid("e607ff67-18e7-424d-85e9-56a9648be166"), 70.00m);
            b144Option.SetTitle("Gift of [C:{0}] or more".FormatWith(b144Option.MinAmount));
            b144Option.SetDescription("- <b>B144</b><br>&nbsp; - Gold hardback with red foil lettering");
            var p1 = new Product("B144");
            b144Option.AddStockItem(new StockItem(new Guid("109571e7-4619-46cc-9797-293c6dde0b33"), MediaType.Book, 100, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(p1));

            var b144LOption = new MinimumAmountDonationOption(new Guid("f678ee3a-0ddc-425c-8520-a974a99cc5fe"), 1000.00m);
            b144LOption.SetTitle("Gift of [C:{0}] or more".FormatWith(b144LOption.MinAmount));
            b144LOption.SetDescription(@"- <b>B144L</b><br>&nbsp; - Hand numbered<br>&nbsp; - Red bonded leather with gold foil lettering.<br>&nbsp; - <b><span style=""text-decoration: underline;"">Signed by Pastor Hagee<br></span></b>");
            var p2 = new Product("B144L");
            b144LOption.AddStockItem(new StockItem(new Guid("67370446-8230-46ce-83d4-b11a11b6f692"), MediaType.Book, 100, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(p2));


            rDonation.SetProjectCode("JHM");

            rDonation.AddDonationOption(b144Option);
            rDonation.AddDonationOption(b144LOption);

            return rDonation;
        }
        private Donation CreateUSExodus_II()
        {
            #region Body

            const string body =
                @"<p>
                    World history is literally forming before our very eyes. It is clear that Israel
                    is in the gravest danger that she has been in since the six Arab armies tried to
                    strangle her at birth in 1948. We want the world and the Jewish people to know that
                    millions of evangelical Christians in America and around the world have a biblical
                    belief to love Israel, to speak up and support her not only with prayer, but financially
                    as well. At time when Israel feels more alone than ever, I want us to let them know
                    �YOU ARE NOT ALONE!� You are God�s Chosen people and have an eternal covenant with
                    God that will stand forever.
                </p>
                <p>
                    Friends and partners, I am awed at what you have done the past and what I know you
                    will continue to do in the future with your generous donations through our Exodus
                    II programs with John Hagee Ministries. To date, you have given $48,127,240.60 to
                    various causes that support the Jewish people. Now that is an Awesome God! Your
                    gifts go for many causes, such as education, helping orphans, the end gathering
                    of exiles from around the world who wish to relocate to Israel and many other humanitarian
                    causes. ALL of our efforts directly benefit Israel and her people. You are the reason
                    we have made such a difference in the lives of so many of God�s people.
                </p>
                <p>
                    As Christians, it is our duty to minister to the Jewish people in material things.
                    �It pleased them indeed, and they are their debtors. For if the Gentiles have been
                    partakers of their spiritual things, their duty is also to minister to them in material
                    things,� Romans 15:27. If you want God�s favor then I challenge you to make the
                    finest contribution you can to Exodus II. I ask that you join me in praying daily
                    for the peace of Jerusalem, and ask that you let your voice heard, �For Zion�s sake
                    I will not keep silent, for Jerusalem�s sake I will not remain quiet, till her righteousness
                    shines out like the dawn, her salvation like a blazing torch.� - Isaiah 62:1. Together
                    � let�s bless Israel and the Jewish people!
                </p>";

            #endregion

            var description = "Exodus II Donation";
            var rDonation = new Donation("Exodus_II", "Exodus II", description, body, listViewImageUrl: "~/Content/images/exodusII-sm.jpg", detailViewImageUrl: "~/Content/images/exodusII-lg.jpg", formViewImageUrl: "~/Content/images/exodusII-lg.jpg");


            var prod_procCert = new Product("K221");
            prod_procCert.SetTitle("Proclamation Certificate");
            var prod_NTHIDVD = new Product("1041E");
            prod_NTHIDVD.SetTitle(@"""Night to Honor Israel""");
            var prod_Pendant = new Product("K511");
            prod_Pendant.SetTitle("Jerusalem Temple Coin Pendant");
            var prod_CoinSet = new Product("K472");
            prod_CoinSet.SetTitle("3 Coin Set with Frame");

            // Menorah REMOVED 3/6/2013
            //var prod_Menorah = new Product("K440");
            //prod_Menorah.SetTitle("Menorah");
            // END: Menorah REMOVED 3/6/2013

            var prod_toli = new Product("TOLI");
            prod_toli.SetTitle("Tree Of Life Plaque at an Israel location");

            StockItem procCert = new StockItem(new Guid("72b6875b-2907-4728-902c-da53ab369971"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_procCert);
            StockItem nthiDVD = new StockItem(new Guid("72e248ec-f559-40cb-b194-e23d7704725f"), MediaType.DVD, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_NTHIDVD);
            StockItem pendant = new StockItem(new Guid("f49d801e-f4c4-4d3d-b4d7-40a3a426f9b0"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_Pendant);
            StockItem coinSet = new StockItem(new Guid("62e007e3-6933-4e74-8ca3-1361ef7f38d0"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_CoinSet);

            // ***Menorah REMOVED 3/6/2013
            //StockItem menorah = new StockItem(new Guid("a2df7cba-816f-4583-8651-882c14fdc18d"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_Menorah);
            // ***END: Menorah REMOVED 3/6/2013

            StockItem plaque = new StockItem(new Guid("77558f0f-c85f-4ec1-bb35-5491b3a2b4b4"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(prod_toli);

            //coinSet.SetTitle(coinSet.Title.Replace("- CD", String.Empty));
            //menorah.SetTitle(menorah.Title.Replace("- CD", String.Empty));

            //var o1 = new FixedAmountDonationOption(new Guid("cdb34457-af7b-4358-8797-99899ca43b8f"), 50.00m);
            //o1.SetTitle("Proclamation Certificate");
            //o1.AddStockItem(procCert);
            //rDonation.AddDonationOption(o1);

            //var o2 = new FixedAmountDonationOption(new Guid("96d32fc9-efb1-4fcb-9035-20805e08914a"), 150.00m);
            //o2.SetTitle("Certificate &amp; Jerusalem Temple Coin Pendant");
            //o2.AddIncludedStockItem(procCert);
            //o2.AddIncludedStockItem(pendant);
            //rDonation.AddDonationOption(o2);

            //var o3 = new FixedAmountDonationOption(new Guid("80c6c3b5-db8c-4d59-ae59-e63468f0eacd"), 400.00m);
            //o3.SetTitle("Certificate, Pendant, NTHI DVD");
            //o3.AddIncludedStockItem(procCert);
            //o3.AddIncludedStockItem(nthiDVD);
            //o3.AddIncludedStockItem(pendant);
            //rDonation.AddDonationOption(o3);

            // ***Menorah REMOVED 3/6/2013
            //var o4 = new FixedAmountDonationOption(new Guid("ba1a9d88-2b53-49df-bf79-7652a15d6339"), 1000.00m);
            //o4.SetTitle("Certificate, NTHI DVD, Pendant, Coins set with Frame, Menorah, Tree Of Life Plaque");
            //o4.AddIncludedStockItem(procCert);
            //o4.AddIncludedStockItem(nthiDVD);
            //o4.AddIncludedStockItem(pendant);
            //o4.AddIncludedStockItem(coinSet);
            //o4.AddIncludedStockItem(menorah);
            //o4.AddIncludedStockItem(plaque);
            //rDonation.AddDonationOption(o4);
            // ***END: Menorah REMOVED 3/6/2013


            var option5 = new AnyAmountDonationOption(new Guid("16F799AB-9F71-4DA4-A46E-DB7891F88E7D"));
            option5.SetTitle("Exodus II Donation");
            option5.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");
            rDonation.AddDonationOption(option5);




            rDonation.SetProjectCode("ISRAEL");

            return rDonation;
        }
        private Donation CreateUSDivine_Servant()
        {
            #region Body

            const string body = @"
                <p>
                    Christian Artist Max Greiner has designed an exclusive for John Hagee Ministries
                    Operation Outreach using his first monumental bronze sculpture The Divine Servant.
                </p>
                <p>
                    This table top commemorative sculpture is a 1/6th life size resin replica of his
                    first monumental bronze sculpture (based on John 13) that is located in the Prayer
                    Garden of Cornerstone Church. This beautiful statue is mounted on a solid limestone
                    base inscribed with the John Hagee Ministries mission to preach �All the Gospel
                    to All the World and to All Generations.�
                </p>                
            ";

            #endregion

            // Setup Included StockItems
            var sculpture = new Product("DSS");
            sculpture.SetTitle("Divine Servant Sculpture");
            var sculptureStockItem = new StockItem(new Guid("F9EAEADA-473C-4577-A8FD-C9E68A4D180E"), MediaType.Book, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(sculpture);

            var description = "Divine Servant Donation";
            var rDonation = new Donation("Divine_Servant", "Divine Servant", description, body, listViewImageUrl: "~/Content/images/devine-sm.jpg", detailViewImageUrl: "~/Content/images/devine-lg.jpg", formViewImageUrl: "~/Content/images/devine-lg.jpg");

            var oneTimeOption = new FixedAmountDonationOption(new Guid("3ad44c48-993c-4f36-a644-d272802c8bf1"), 1200m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddStockItem(sculptureStockItem);

            //Monthly Option
            //var monthlyOption = new FixedAmountDonationOption(new Guid("b476dea7-4b1b-497b-ae5a-26c5fe74fe45"), 100m);
            //monthlyOption.SetTitle("Monthly - Automatic Deduction");
            //monthlyOption.SetDescription("Beginning this month, I pledge a monthly gift in the amount of {0}. <blockquote>(*Charge will be deducted each month on the date of initial transaction and sculpture will be shipped upon completion of pledge) </blockquote>".FormatWith(monthlyOption.Amount.ToString("C", Thread.CurrentThread.CurrentCulture)));
            //monthlyOption.AddDonationFrequency(null, new[] { DonationFrequency.MonthlyResidual });
            //monthlyOption.DisableOneTimeDonationFrequency();

            rDonation.SetProjectCode("OUT");

            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(monthlyOption);


            return rDonation;
        }
        private Donation CreateUSOne_Time()
        {
            #region Body

            const string body = @"<p>John Hagee Ministries is now being seen or heard in over 245 nations
                    around the world and being broadcasted into&nbsp;255&nbsp;million&nbsp;homes worldwide. Many of
                    the countries currently receiving our program are ravaged by civil war, famine, natural disasters.</p>

                    <p>Together we are truly helping to fulfill the Great Commission!</p>

                    <p>Our annual television air time budget presently exceeds $14,000,000, most of
                    which goes to purchasing time on Christian Networks. Every telecast you see on TV is paid for...
                    there are NO FREE TELECASTS on any network!</p>
                    <p>We want to continue to take one heart at a time, one soul at a time, one home at a time with the
                    Gospel of Jesus Christ!</p>

                    <p>Thank you once again for your one time donation
                    to help see this vision accomplished.We have faith and confidence
                    that together, we can &quot;do all things through Christ who strengthens us!&quot;</p>";

            #endregion

            var description = "John Hagee Ministries One Time Donation";
            var rDonation = new Donation("One_Time", "One Time Donation", description, body, listViewImageUrl: "~/Content/images/onetime-sm.jpg", detailViewImageUrl: "~/Content/images/onetime-lg.jpg", formViewImageUrl: "~/Content/images/onetime-lg.jpg");

            var oneTimeOption = new AnyAmountDonationOption(new Guid("ddba15d8-5ff4-4f3f-a564-b6e4f7132514"));
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");

            rDonation.SetProjectCode("JHM");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }
        private Donation CreateUSPlant_Your_Roots()
        {
            #region Body

            const string body = @"<p>This campaign has the goal of planting trees in Israel to help revitalize the land! Planting a tree is more than mere aesthetics. It is an opportunity to help: Transform barren regions of Israel into fruitful, productive land, create jobs, provide needed irrigation and sustain the local economy.</p>
                <p>Each donation of $35 plants a tree in Israel and contributes to a unique water conservation project. With your donation you will receive a certificate to commemorate your participation. You can also plant a tree in memory of a loved one.</p>

                <p>You can plant as many trees as you would like! JHM partners have planted tens of thousands of trees with the Land of Promise oversight. We thank you for your support of the land of Israel.</p>";
            #endregion

            var description = "Israel Tree Planting Donation";
            var rDonation = new Donation("Plant_Your_Roots", "Plant Your Roots in Israel", description, body, listViewImageUrl: "~/Content/images/israeltrees-sm.jpg", detailViewImageUrl: "~/Content/images/israeltrees-lg.jpg", formViewImageUrl: "~/Content/images/israeltrees-lg.jpg");

            // Setup Included StockItems
            var certificate = new Product("K463");
            certificate.SetTitle("Certificate");
            var certificateStockItem = new StockItem(new Guid("1FF1A9DB-D1A0-4736-BCFF-0F0213A9728F"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(certificate);


            var o1 = new MultipleOfdonationOption(new Guid("31c6d773-1424-4870-ae79-cbe3db8d67b4"), 35.00m);
            o1.SetTitle("Please enter the number of trees you would like planted in Israel:");
            o1.SetDescription(" The cost is [C:{0}] per tree.".FormatWith(o1.PricePerMultiple));
            // TODO Test JFE
            o1.SetRequiredOptionField("Certificate", "Please enter name for Certificate:", maxLength: 40);
         //   o1.EnableCertificateRelatedMultiple();
            //o1.AddDonationFrequency(DonationFrequency.MonthlyResidual);
            o1.AddIncludedStockItem(certificateStockItem);

            rDonation.SetProjectCode("TREES");

            rDonation.AddDonationOption(o1);

            return rDonation;
        }
        private Donation CreateUSTen_Commandments_Artwork()
        {

            #region Body

            const string body = @"
            <p>
                This work of art by Spooner Galleries is one of our most memorable Operation Outreach
                offerings declaring the mission to �Take All the Gospel to All the World and to
                All Generations.� Made exclusively for John Hagee Ministries, this piece handsomely
                displays the Ten Commandments in a 30� by 37� decorative frame with an exquisite
                mat. The precious keepsake will be an attractive addition to your home or workplace
                and will minister to your family for generations to come.
            </p>
            ";

            #endregion

            var description = "Ten Commandments Donation";
            var rDonation = new Donation("Ten_Commandments_Artwork", "Ten Commandments Artwork", description, body, listViewImageUrl: "~/Content/images/10commendments-sm.jpg", detailViewImageUrl: "~/Content/images/10commendments-lg.jpg", formViewImageUrl: "~/Content/images/10commendments-lg.jpg");

            // Setup Included StockItems
            var wallArt = new Product("TENC");
            wallArt.SetTitle("Ten Commandments Wall Art");
            var wallArtStockItem = new StockItem(new Guid("6E33EF48-68F7-4809-931D-290A220AF9B7"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(wallArt);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("79193b32-bd08-42ec-87d0-7b8264c09252"), 2500m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(wallArtStockItem);

            //Monthly Option
            //var monthlyOption = new FixedAmountDonationOption(new Guid("dcc89384-7fa8-47f5-bde0-1af30d19e7e9"), 208.33m);
            //monthlyOption.SetTitle("Monthly - Automatic Deduction");
            //monthlyOption.SetDescription("Beginning this month, I pledge a monthly gift in the amount of {0}. <blockquote>(*Charge will be deducted each month on the date of initial transaction and the Artwork will be shipped upon completion of pledge) </blockquote>".FormatWith(monthlyOption.Amount.ToString("C", Thread.CurrentThread.CurrentCulture)));
            //monthlyOption.AddDonationFrequency(null, new[] { DonationFrequency.MonthlyResidual });
            //monthlyOption.DisableOneTimeDonationFrequency();

            //Quaterly Option
            //var quarterlyOption = new FixedAmountDonationOption(new Guid("a801890c-5319-4133-be42-752436f74337"), 625m);
            //quarterlyOption.SetTitle("Quarterly - Automatic Deduction");
            //quarterlyOption.SetDescription("Beginning this month, I pledge a quarterly gift in the amount of {0}. <blockquote>(*Charge will be deducted each three months on the date of initial transaction and the Artwork will be shipped upon completion of pledge) </blockquote>".FormatWith(monthlyOption.Amount.ToString("C", Thread.CurrentThread.CurrentCulture)));
            //quarterlyOption.AddDonationFrequency(null, new[] { DonationFrequency.QuarterlyResidual });
            //quarterlyOption.DisableOneTimeDonationFrequency();


            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(quarterlyOption);
            //rDonation.AddDonationOption(monthlyOption);

            rDonation.SetProjectCode("OUT");

            return rDonation;
        }
        private Donation CreateUSMilitary_Warriors()
        {
            #region Body

            const string body = @"
            <p>
                The Military Warrior Support Foundation was established to honor and assist returning
                disabled soldiers with the everyday challenges that they and their loved ones now
                face.
            </p>
            <p>
                Although their lives will never be the same, it�s our desire that they return to
                normalcy as soon as possible. The Military Warrior Support Foundation is working
                hard to make it happen.
            </p>
            <p>
                The Foundation was created to provide: Educational Scholarships, Job Re-training,
                Job Placement, Recreation Opportunities, and Financial Aid to warriors in need.
                You can help by financially supporting The Military Warriors Support Foundation
                through John Hagee Ministries.
            </p>
            ";

            #endregion

            var description = "Military Warrior Support Fund Donation";
            var rDonation = new Donation("Military_Warriors", "Military Warriors Support Foundation", description, body, listViewImageUrl: "~/Content/images/military-sm.jpg", detailViewImageUrl: "~/Content/images/military-lg.jpg", formViewImageUrl: "~/Content/images/military-lg.jpg");

            var oneTimeOption = new AnyAmountDonationOption(new Guid("ddba15d8-5ff4-4f3f-a564-b6e4f7132514"));
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");

            rDonation.AddDonationOption(oneTimeOption);

            rDonation.SetProjectCode("MWSF");

            return rDonation;
        }
        private Donation CreateUSDigitalArchiveSolution()
        {
            #region Body

            const string body = @"Please prayerfully consider helping JHM in our efforts to digitize our library and give 
            new vitality and a new audience a chance to hear the pure Word of God.  Your gift of $250.00 will help convert thousands of
            yards of taped sermons into a digital format allowing anyone, anywhere to hear the Word of God as preached on John Hagee Ministries
            each and every day. This process will allow your phone, computer, Ipad or other equipment to view Pastor John Hagee and 
            Pastor Matthew Hagee from every location imaginable.  To commemorate this grand achievement we are offering a beautiful 
            table top Ten Commandments piece.";

            #endregion

            var rDonation = new Donation("Digital_Archive_Solution", "Digital Archive Solution", string.Empty, body, listViewImageUrl: "~/Content/images/10sculpture-sm.jpg", detailViewImageUrl: "~/Content/images/10sculpture-lg.jpg", formViewImageUrl: "~/Content/images/10sculpture-lg.jpg");

            // Setup Included StockItems
            var tenCommandmentArtPiece = new Product("K503");
            tenCommandmentArtPiece.SetTitle("Ten Commandments Table Top Art Piece");
            var tenCommandmentsArtPieceStockItem = new StockItem(new Guid("8E942208-D19C-40B2-8712-FAD12E5CC78C"), MediaType.Misc, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(tenCommandmentArtPiece);


            //One Time Donation (Min Requirements)
            var minAmountDonation = new MinimumAmountDonationOption(new Guid("42394C65-3C0A-4DAB-B781-614EE7E4DDED"), 250.00m);
            minAmountDonation.EnableAmountOutsideOfRange();
            minAmountDonation.SetTitle("One-Time Deduction");
            minAmountDonation.SetDescription("Please Accept my gift of $250.00 or more to support the digital upgrade and to receive a beautiful table top Ten Commandments piece.");
            minAmountDonation.AddIncludedStockItem(tenCommandmentsArtPieceStockItem);

            rDonation.AddDonationOption(minAmountDonation);

            rDonation.SetProjectCode("DAS");

            return rDonation;
        }


        private Donation CreateUSPropheticBlessingsDonation()
        {
            #region Body

            const string body = @"I would like to give a one time gift and receive a copy of <strong>�The Power of the Prophetic Blessing�</strong><br/>In order to receive the contents of the virtual treasure chest you must provide a valid email address.";

            const string customEmailText = @"Thank you for your donation. We will be sending you the book, �The Power of the Prophetic Blessing� in Fall 2012.";

            #endregion

            var rDonation = new Donation(ProphecticBlessingCode, "Prophetic Blessing Donation", string.Empty, body, listViewImageUrl: "~/Content/images/10sculpture-sm.jpg", detailViewImageUrl: "~/EmailTemplates/PropheticBlessing/Prophetic_Donation_Banner.jpg", formViewImageUrl: "~/EmailTemplates/PropheticBlessing/Prophetic_Donation_Banner.jpg");
            rDonation.IsHidden = true;
            rDonation.CustomEmailBody = customEmailText;
            rDonation.CustomEmailTemplateFileName = "PropheticBlessing/PropheticBlessing.html";

            // Setup Included StockItems
            var book = new Product("B180G");
            book.SetTitle("The Power of the Prophetic Blessing");
            var bookStockItem = new StockItem(new Guid("8E942208-D19C-40B2-8712-FAD12E5CC78C"), MediaType.Book, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(book);


            //One Time Donation (Min Requirements)
            var oneTimeOption = new AnyAmountDonationOption(new Guid("344F99EE-7068-4D24-8A73-8A2ADECB7971"));
            oneTimeOption.SetTitle("One-Time Deduction");
            //oneTimeOption.SetDescription("");
            oneTimeOption.AddIncludedStockItem(bookStockItem);

            rDonation.AddDonationOption(oneTimeOption);

            rDonation.SetProjectCode("BOOK");

            return rDonation;
        }

        private Donation CreateUSFourBloodMoonsDonation()
        {

            #region Body

            const string body = @"
            <p>
Pastor Hagee says that God uses the sun, moon and stars as His own personal, high-definition billboard to communicate with us. Are we listening? This latest release, Four Blood Moons, is a compilation of scientific data recorded by NASA with each fact verified by Scripture. Four Blood Moons illustrates topics never discussed before in a format that is easy for the reader to follow. The full lunar eclipses (or �blood moons�) are caused by an infrared band of light encircling the Earth, making the moon appear red. Learn the significance behind these blood moons and how it has affected us in the past and will continue to affect us in the future. This in-depth manuscript includes data from NASA combined with rabbinical studies alongside decades of personal research conducted by Pastor Hagee. This book will take you on a journey through the Jewish holidays and explain the significance of how each affects us on both a personal and spiritual level. Pastor Hagee says, �There are no solar or lunar accidents; every heavenly body is controlled by the unseen hand of God to send signals to humanity of coming events.� Find out today why SOMETHING IS ABOUT TO CHANGE!

This JHM exclusive, commemorative edition is bound in red leather and personally signed by Pastor Hagee. It is a beautiful addition to any library or private collection. It makes a great gift for birthdays, anniversaries or holidays, and is the perfect tool to witness to your unsaved friend or loved one. Buy your copy today as this is only available for a limited time!

            </p>
            ";

            #endregion

            var description = "Four Blood Moons Donation";
            var rDonation = new Donation("FourBloodMoonsDonation", "Four Blood Moons Donation", description, body, listViewImageUrl: "~/Content/images/FourBloodMoons-sm.jpg", detailViewImageUrl: "~/Content/images/FourBloodMoons-lg.jpg", formViewImageUrl: "~/Content/images/FourBloodMoons-lg.jpg");

            // Setup Included StockItems
            var fourBloodMoonsProduct = new Product("B195");
            fourBloodMoonsProduct.SetTitle("Four Blood Moons: Something Is About To Change Collector�s Edition");
            var fourBloodMoonStockItem = new StockItem(new Guid("3FA8A5BC-41C2-4D5A-9D3E-72B1FA6D0B6C"), MediaType.BookHardBackSigned, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(fourBloodMoonsProduct);

            var oneTimeOption = new FixedAmountDonationOption(new Guid("8F436C7A-2A32-4841-B7A9-8FA8310C9664"), 50m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(fourBloodMoonStockItem);

            rDonation.AddDonationOption(oneTimeOption);
            rDonation.SetProjectCode("OUT");

            return rDonation;
        }

        private Donation CreateUSMasterYourMoneyDonation()
        {

            #region Body

            const string body = @"<p>How is today's unstable economy going to impact you financially? What are the Bible principles that protect your finances? Everyone can use supernatural help with their finances. Now you can receive the Biblical knowledge for financial prosperity from Pastor Hagee's book entitled ""Mastering Your Money"" for your gift of any amount.</p>";

            #endregion

            var description = "Master Your Money Donation";
            var rDonation = new Donation("MasterYourMoneyDonation", "Master Your Money Donation", description, body, listViewImageUrl: "~/Content/images/MasterYourMoney-sm.jpg", detailViewImageUrl: "~/Content/images/MasterYourMoney-lg.jpg", formViewImageUrl: "~/Content/images/MasterYourMoney-lg.jpg");
         
            // Setup Included StockItems
            var masterYourMoneyProduct = new Product("B104");
            masterYourMoneyProduct.SetTitle("Master Your Money Booklet");
            var masterYourMoneyStockItem = new StockItem(new Guid("5A3FA26A-24AC-4A9F-9BBF-702A4C57312B"), MediaType.Book, 1000, true, 0.00m, 0.00m, true, USWarehouse).AddProduct(masterYourMoneyProduct);

            var oneTimeOption = new AnyAmountDonationOption(new Guid("E0586ACB-CFF6-4CE9-9F42-F517638D8CD9"));
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift.");
            oneTimeOption.AddIncludedStockItem(masterYourMoneyStockItem);
            
            rDonation.AddDonationOption(oneTimeOption);
            rDonation.SetProjectCode("BOOK");

            return rDonation;
        }

        #endregion



        #region Private Methods
        private IEnumerable<Donation> GetStubbedDonations(int quantity, string donationCode)
        {
            var rList = new List<Donation>();

            for (int i = 0; i < quantity; i++)
            {
                if (donationCode.Is().NullOrEmpty())
                    donationCode = "MockedDonationCode";

                var d1 = new Donation("{0}-{1}-{2}".FormatWith(donationCode, i, 1), "MockTitle-{0}-{1}".FormatWith(i, 1), GetDescription(), GetBody(), pdfVersion: "http://www.jhm.org/Media/DocumentLibrary/check_debit.pdf");
                var d2 = new Donation("{0}-{1}-{2}".FormatWith(donationCode, i, 2), "MockTitle-{0}-{1}".FormatWith(i, 2), GetDescription(), GetBody());

                AddDonationOptions(d1);
                AddDonationOptions(d2);
                rList.Add(d1);
                rList.Add(d2);
            }

            return rList;
        }
        private string ContentImagesIconFacebookPng()
        {
            return "~/Content/images/icon-facebook.png";
        }
        private string GetDescription()
        {
            return @"<div id=""lipsum"">
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nisl urna, tincidunt a lobortis vitae, varius vel magna. Donec a risus eget purus placerat dapibus eget rutrum magna. Donec eu magna non ligula placerat porta. Duis lorem lacus, placerat at sagittis vel, imperdiet non mauris. Integer quis elit eget lacus hendrerit tincidunt convallis sit amet elit. Aenean varius semper dui a molestie. Maecenas a massa nulla, sit amet consectetur nisi. Nunc sed eros libero, vel vehicula nulla. Quisque facilisis ante eu neque porttitor laoreet. Nunc tincidunt leo id metus dictum vulputate. Pellentesque dictum placerat leo, scelerisque dictum magna gravida ut. Suspendisse potenti.
</p></div>";
        }
        private string GetBody()
        {
            return
                @"<div id=""lipsum"">
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur non massa elit, vitae aliquam arcu. Sed id magna quam. Sed non condimentum velit. Pellentesque porta enim ut mauris congue sit amet bibendum est aliquam. Praesent porttitor sem quis erat dignissim suscipit. Pellentesque cursus porta nisi. Aenean facilisis arcu non diam pellentesque eget bibendum dolor volutpat. Quisque luctus volutpat odio vitae rutrum. Duis sollicitudin iaculis enim eu sollicitudin. Duis metus ipsum, pellentesque bibendum egestas vel, semper quis dolor. Mauris eget faucibus odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
</p>
<p>
Nam tincidunt enim sed ante facilisis euismod. Integer condimentum pretium quam, ut scelerisque nulla suscipit eget. Nunc porttitor sodales quam ut porttitor. Aliquam in ultrices turpis. Proin sed mauris id lorem molestie commodo vel eget sem. Mauris laoreet condimentum purus, id porta odio rhoncus ac. Fusce viverra elit a sem porttitor ut condimentum tellus venenatis. Nam euismod malesuada ultrices. Suspendisse posuere fermentum purus, vitae hendrerit risus iaculis nec. Quisque a faucibus erat. Suspendisse potenti. Donec pellentesque tortor ornare nisi venenatis vitae sodales tortor suscipit. Mauris tincidunt, massa in malesuada luctus, dui turpis sagittis nibh, eu dictum augue massa nec urna. Aenean et dolor non erat gravida volutpat nec eu velit. Nulla porta metus eget tortor tempor rutrum. Cras quis odio eget nisi pharetra vulputate eget eu nisi. Nam eget odio non nisl blandit scelerisque vehicula non enim.
</p>
<p>
Aenean ut felis mauris, et lobortis velit. Curabitur imperdiet porta ante non varius. Integer tincidunt urna vestibulum sem ultrices sed consectetur nibh luctus. Donec diam mauris, sagittis non ornare et, euismod ut nisi. Pellentesque sed elit metus, non vestibulum ligula. In dapibus tellus a ligula rhoncus sed sollicitudin nunc convallis. Nulla mattis, magna non feugiat egestas, nisi velit congue purus, non vulputate nibh neque ac leo. Duis non luctus velit. Praesent interdum leo et felis lacinia accumsan. Proin non ligula purus, ut hendrerit mauris.
</p>
</div>";
        }
        private void AddDonationOptions(Donation donation)
        {
            donation.AddDonationOption(GetFixedDonationOption());
            donation.AddDonationOption(GetAnyAmountDonationOption());
            donation.AddDonationOption(GetMultipleOfdonationOption());
            donation.AddDonationOption(GetAmountRangeDonationOption());
            donation.AddDonationOption(GetMinimumAmountDonationOption());
        }
        private DonationOption GetAnyAmountDonationOption()
        {
            var option = new AnyAmountDonationOption(new Guid("37c42aa4-229d-45d0-b046-5cc210e2e2d9"));
            option.EnableRequireCertificateField();
            DecorateDonationOption(option, "Any Amount Donation - Required Certificate");
            return option;
        }
        private DonationOption GetFixedDonationOption()
        {
            var option = new FixedAmountDonationOption(new Guid("537cd6d7-3207-4e60-800b-ccf51a28e88f"), 50.00m);
            DecorateDonationOption(option, "Fixed Donation - Required Certificate");
            AddStockItemChoices(option);
            option.EnableRequireCertificateField();
            return option;
        }
        private void AddStockItemChoices(DonationOption option)
        {
            var product = new Product("StockItemChoices-Code", new[] { Category.Interviews, Category.MoviesAndDocumentaries });
            product.SetTitle("StockItemChoices - Product Title");
            product.AddStockItems(null, new List<StockItem>
                                            {
                                                new StockItem(new Guid("ed0c6ffc-7b54-4d28-87d1-4dc6af7d7475"),  MediaType.Book, 10, true, 10.00m, 10.00m, true, USWarehouse),
                                                new StockItem(new Guid("55520c68-f5cb-44da-a88b-960e06c3f1fd"), MediaType.DVD, 15, true, 15.00m, 15.00m, true, USWarehouse),
                                                new StockItem(new Guid("190f02ad-a8f2-458f-a5e6-2498fce78d4d"),MediaType.AudioFileDownload, 3, true, 3.00m, 3.00m, true, USWarehouse),
                                                new StockItem(new Guid("9a750d5b-cda7-4fbc-9379-51154a60ce4d"),MediaType.OnDemand, 5, true, 5.00m, 5.00m, true, USWarehouse),
                                            }.ToArray());

            option.AddStockItemChoice(null, product.StockItems.ToLIST().ToArray());
        }
        private DonationOption GetMultipleOfdonationOption()
        {
            var option = new MultipleOfdonationOption(new Guid("4fae432d-58ad-491f-bc48-bb661bb6de81"), 25.00m);
            DecorateDonationOption(option, "Multiple Of Donation");
            return option;
        }
        private DonationOption GetAmountRangeDonationOption()
        {
            var option = new AmountRangeDonationOption(new Guid("e0fe74d7-b0f3-4efd-8ce9-f187da06cda5"), 10.00m, 100.00m);
            DecorateDonationOption(option, "Amount Range Donation");
            return option;
        }
        private DonationOption GetMinimumAmountDonationOption()
        {
            var option = new MinimumAmountDonationOption(new Guid("fac30bc6-e950-4396-a9a9-f40c9adaf8c4"), 25.00m);
            DecorateDonationOption(option, "Minimum Amount Donation");
            return option;
        }
        private void DecorateDonationOption(DonationOption option, string title)
        {
            option.SetTitle(title);
            option.SetDescription(GetOptionDescription());
            option.SetImageUrl(ContentImagesIconFacebookPng());
            option.EnableOptionField();

            //Add StockItems
            var product = new Product("Productcode", new[] { Category.Interviews, Category.MoviesAndDocumentaries });
            product.SetTitle("Product Title");
            product.AddStockItems(null, new List<StockItem>
                                            {
                                                new StockItem(MediaType.Book, 10, true, 10.00m, 10.00m, true, "10"),
                                                new StockItem(MediaType.DVD, 15, true, 15.00m, 15.00m, true, "5"),
                                                new StockItem(MediaType.AudioFileDownload, 3, true, 3.00m, 3.00m, true, "10"),
                                                new StockItem(MediaType.OnDemand, 5, true, 5.00m, 5.00m, true, "10"),
                                            }.ToArray());

            option.AddStockItem(null, product.StockItems.ToLIST().ToArray());
        }
        private string GetOptionDescription()
        {
            return
                @"<div id=""lipsum"">
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Nullam rutrum, libero dictum sodales feugiat, quam libero hendrerit tortor, eget iaculis erat mauris at felis. Sed dignissim eros eu risus pellentesque faucibus. Vivamus fringilla dapibus diam, id molestie leo scelerisque ut. Etiam mattis lorem in ligula dignissim in cursus velit rutrum. Aenean vehicula risus imperdiet velit consequat ornare. Donec purus lacus, vehicula ut sodales eu, dapibus vitae dui.
</p></div>";
        }
        #endregion



        #region Not Used At This Time
        //NOTE:  NEED PHOTOS FOR BELOW
        private Donation CreateUSFreedom_Isnt_Free()
        {
            var rDonation = new Donation("Freedom_Isnt_Free", "Freedom Isn't Free", string.Empty, "Suggested gift of $5.00", pdfVersion: "http://www.jhm.org/Media/DocumentLibrary/check_debit.pdf");
            var oneTimeOption = new AnyAmountDonationOption(new Guid("8f6612d5-fcfd-47bf-ba2e-5e1577697090"));
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");
            var product = new Product("M61");
            product.SetTitle("Freedom Isn't Free");
            oneTimeOption.AddStockItem(new StockItem(new Guid("affcc1a7-e4be-4392-81e3-ff2710421a13"), MediaType.CD, 100, true, 5.00m, 5.00m, true, USWarehouse).AddProduct(product));

            rDonation.AddDonationOption(oneTimeOption);
            return rDonation;
        }
        private Donation CreateUSEli_Wiesel()
        {
            var body = @"<p>In the mid-1990's, soon after thousands of Ethiopian Jews were rescued from violence and persecution in Africa, The Elie Wiesel Foundation for Humanity created two educational centers in the State of Israel. These Beit Tzipora Centers, in Ashkelon and Kiryat Malachi, focus on educating the Ethiopian-Jewish community and giving Ethiopian-Israeli young people the opportunity to grow and participate fully in Israeli society. The centers currently enroll more than 1,000 boys and girls in after-school programs and serve as a model for other schools. </p>
<p>Named in memory of Elie Wiesel's younger sister, who died in Auschwitz, these centers have become a major part of the Foundation's work and remain a passion of the Wiesels. Elie and Marion Wiesel have been actively involved in the Beit Tzipora Centers since the beginning and continue to remain active in the management of the Centers, regularly visiting and encouraging the students to succeed.</p>
<p>School and public officials have recognized that the Beit Tzipora study and enrichment programs significantly improve the quality of life for these young people. The Foundation's dream is to give all Ethiopian children in Israel the chance to excel. This work is constantly expanding and growing and will remain at the heart of the mission of The Elie Wiesel Foundation for Humanity.</p>
<p>With your gift of $50 or more you will receive Elie Wiesels prize winning book <i>Night</i>. Proceeds from the funds from Night will go toward the Elie Wiesel foundation, specifically the Beit Tzipora Center. </p>
";
            var rDonation = new Donation("Eli_Wiesel", "Eli Wiesel", string.Empty, body, pdfVersion: "http://www.jhm.org/Media/DocumentLibrary/check_debit.pdf");
            var oneTimeOption = new MinimumAmountDonationOption(new Guid("6a2e4c7d-5376-46f3-844a-84b170a8e725"), 50.00m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");
            var product = new Product("M61");
            product.SetTitle("Night - Elie Wiesel");
            oneTimeOption.AddStockItem(new StockItem(new Guid("1bc70aba-30f6-4f4a-b82f-20f0d33d7d8e"), MediaType.Book, 100, true, 5.00m, 5.00m, true, USWarehouse).AddProduct(product));

            rDonation.AddDonationOption(oneTimeOption);
            return rDonation;
        }
        private Donation CreateUSWarrior_Chef_CookOff()
        {
            #region Body

            const string body =
                @"<p>
            A few Sundays ago an idea was born when Pastor Matthew Hagee stood on the stage
            of Cornerstone Church and issued a challenge from the pulpit to his mother. He challenged
            Mrs. Diana Hagee to a cook off.
        </p>
        <p>
            Pastor Matthew Hagee will be paired with Chef Eric Rocha. Diana Hagee will be paired
            with her sister Sandy Farhart. Each team will be required to cook and present a
            four course meal and each course will have the added pressure of a <strong><i>secret</i></strong> ingredient.
            The winner will be determined by a panel of restaurant owners.
        </p>
        <p>
            Proceeds from this event will benefit the children of single parents who would like
            to attend Cornerstone Christian Schools in San Antonio, Texas. You can watch this
            competition LIVE by registering on jhm.org and clicking the �media services� button.
            Help support the free airing of this event by clicking �DONATE�. Team Pastor Matthew
            Hagee or Team Diana Hagee � Who will be the WARRIOR CHEF?
        </p>";

            #endregion

            #region Description

            const string description =
                @"<p>Description</p>";

            #endregion


            var rDonation = new Donation("Warrior_Chef_CookOff", "Warror Chef Cook Off", description, body, listViewImageUrl: "~/Content/images/warriorchef-sm.jpg", detailViewImageUrl: "~/Content/images/warriorchef-lg.jpg", formViewImageUrl: "~/Content/images/warriorchef-lg.jpg");


            var oneTimeOption = new AnyAmountDonationOption(new Guid("0F96F09D-DA8A-4A0D-9E91-13E2D6A16832"));
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("Warrior Chef Cook Off");

            rDonation.AddDonationOption(oneTimeOption);
            //rDonation.AddDonationOption(monthlyOption);

            rDonation.SetProjectCode("CCS");

            return rDonation;
        }

        #endregion

    }
}