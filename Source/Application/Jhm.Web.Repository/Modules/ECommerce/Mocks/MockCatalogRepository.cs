using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Repository.Modules.Account
{
    public class MockCatalogRepository : ICatalogRepository
    {
        private List<Product> mockedProducts = new List<Product>();
        private Random ratingRandom = new Random();
        private Random randomNumberOfReviews = new Random();
        public MockCatalogRepository()
        {
            //LoadOxcyonProducts();
        }

        private void LoadOxcyonProducts()
        {

            try
            {
                string path = (Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + @"\DsProducts.xml").Replace(@"file:\", "");
                Stream xmlStream = new FileStream(path, FileMode.Open);
                XmlTextReader xmlReader = new XmlTextReader(xmlStream);

                XDocument xmlDoc = XDocument.Load(xmlReader);

                var xmlProducts = from record in xmlDoc.Descendants("Record")
                                  select new
                                             {
                                                 MediaType = GetMediaTypeFromXmlNode(record),
                                                 Categories = GetCategoriesFor(record),
                                                 Code = GetCleanCode(record),
                                                 IdentificationNumber2 = GetValueFor(record, "IdentificationNumber2"),
                                                 ItemImage1 = GetImageLocationWithUrlPrefix(record),
                                                 Title = GetCleanTitle(record),
                                                 BasePrice = GetDecimalFor(record, "Price1"),
                                                 SalePrice = GetDecimalFor(record, "Price2").Is().GreaterThan(0) ? GetDecimalFor(record, "Price2") : GetDecimalFor(record, "Price1"),
                                             };

                //Insert All That DONOT already exist
                foreach (var xmlProduct in xmlProducts.Where(x => mockedProducts.Where(y => y.Title == x.Title).Count() == 0))
                {
                    var product = CreateStubbedProduct(new
                                                           {
                                                               xmlProduct.Title,
                                                               Author_Producer = "MOCK AUTHOR / PRODUCER",
                                                               ProductCode = xmlProduct.Code,
                                                               StockItemDetails = new dynamic[] {
                                                                                                    new {
                                                                                                            MediaType=  xmlProduct.MediaType, 
                                                                                                            BasePrice = xmlProduct.BasePrice, 
                                                                                                            SalePrice = xmlProduct.SalePrice
                                                                                                        }
                                                                                                },
                                                               Categories = xmlProduct.Categories,
                                                               Images = new[] { xmlProduct.ItemImage1 },
                                                               Description = "FROM OXCYON - {0}".FormatWith(GetMockText())
                                                           });

                    mockedProducts.Add(product);
                }

                //Add stockitem, categories and images - for all products that already exist
                foreach (var xmlProduct in xmlProducts.Where(x => mockedProducts.Where(y => y.Title == x.Title).Count() > 0))
                {
                    foreach (var mockedProduct in mockedProducts.Where(x => x.Title == xmlProduct.Title))
                    {
                        if (mockedProduct.StockItems.Where(x => x.MediaType != xmlProduct.MediaType).Count() > 0)
                            mockedProduct.AddStockItems(new StockItem(xmlProduct.MediaType, 100, true, xmlProduct.BasePrice, xmlProduct.SalePrice, true, string.Empty));

                        mockedProduct.AddImages(xmlProduct.ItemImage1);
                        mockedProduct.AddCategory(null, xmlProduct.Categories);
                    }
                }
            }
            
            
            catch 
            {
            }


        }

        private string GetCleanCode(XElement record)
        {
            var xmlValue = GetValueFor(record, "IdentificationNumber1");
            xmlValue = xmlValue.Trim().ToUpper().TrimStart('F');

            foreach (var mediaType in MediaType.GetAllSortedByDisplayName<MediaType>().Where(x => x.CodeSuffix == xmlValue.ToCharArray().Last().ToString()))
            {
                string codeSuffix = "{0}".FormatWith(mediaType.CodeSuffix.ToUpper());
                if (xmlValue.EndsWith(codeSuffix))
                    xmlValue = xmlValue.TrimEnd(codeSuffix.ToCharArray());
            }

            return xmlValue;
        }

        private string GetImageLocationWithUrlPrefix(XElement record)
        {
            return "http://www.jhm.org{0}".FormatWith(GetValueFor(record, "ItemImage1"));
        }

        private decimal GetDecimalFor(XElement record, string labelId)
        {
            return GetValueFor(record, labelId).TryConvertTo<decimal>();
        }

        private string GetCleanTitle(XElement record)
        {

            var xmlValue = GetValueFor(record, "ItemName");

            foreach (var mediaType in MediaType.GetAllSortedByDisplayName<MediaType>().Where(x => xmlValue.ToUpper().Contains(x.DisplayName.ToUpper())))
            {
                var value = xmlValue.ToUpper().Trim().Replace("  ", " ").Replace("&", "and").Replace(":", "-");

                string mediaTypeSuffixNoDash = mediaType.DisplayName.ToUpper();
                string mediaTypeSuffix = "- {0}".FormatWith(mediaTypeSuffixNoDash);
                string mediaTypeSuffixWithSpace = " {0}".FormatWith(mediaTypeSuffix);
                string seriesSuffix = "{0} series".FormatWith(mediaTypeSuffix).ToUpper();
                string seriesSuffixNoDash = " {0} series".FormatWith(mediaTypeSuffixNoDash).ToUpper();

                if (value.EndsWith(mediaTypeSuffixWithSpace))
                    xmlValue = value.Replace(mediaTypeSuffixWithSpace, "").ToTitleCase();
                else if (value.EndsWith(mediaTypeSuffix))
                    xmlValue = value.Replace(mediaTypeSuffix, "").ToTitleCase();
                else if (value.EndsWith(seriesSuffix))
                    xmlValue = value.Replace(seriesSuffix, "").ToTitleCase();
                else if (value.EndsWith(seriesSuffixNoDash))
                    xmlValue = value.Replace(seriesSuffixNoDash, "").ToTitleCase();
                else if (value.EndsWith(mediaTypeSuffixNoDash))
                    xmlValue = value.Replace(mediaTypeSuffixNoDash, "").ToTitleCase();
            }
            xmlValue = xmlValue.Trim();

            return xmlValue;
        }

        private Category[] GetCategoriesFor(XElement record)
        {
            var tier1Cat = GetValueFor(record, "1stTierType");
            var tier3Cat = GetValueFor(record, "3rdTierType");
            tier3Cat = tier3Cat == "Evang" ? "Evangelism and Christianity" : tier3Cat;
            tier3Cat = tier3Cat == "Health" ? "Health and Healing" : tier3Cat;
            tier3Cat = tier3Cat.Replace("&", "and");
            return tier1Cat != "0" ? new[] { Category.TryGetFromDisplayName<Category>(tier1Cat), Category.TryGetFromDisplayName<Category>(tier3Cat) } : tier3Cat != "0" ? new[] { Category.TryGetFromDisplayName<Category>(tier3Cat) } : new Category[0];
        }

        private MediaType GetMediaTypeFromXmlNode(XElement record)
        {
            var xmlValue = GetValueFor(record, "2ndTierType");
            var mediaType = xmlValue.IsNot().NullOrEmpty() && xmlValue.Length.Is().GreaterThan(3) ? xmlValue.ToTitleCase() : xmlValue;
            mediaType = mediaType == "Mp3 - Downloadable Content" ? "MP3" : mediaType;

            return MediaType.FromDisplayName<MediaType>(mediaType);
        }


        private static string GetValueFor(XElement record, string labelId)
        {
            return record.Elements("Field").ToLIST().Find(x => x.Attribute("Label").Value == labelId).Value;
        }



        private void AddFeaturedProducts()
        {
            var pilgrimsProgress = CreateStubbedProduct(new
                                                            {
                                                                Title = "Pilgrim's Progress (feature film) - Journey to Heaven",
                                                                Author_Producer = "DRC Productions",
                                                                ProductCode = "K435",
                                                                StockItemDetails = new dynamic[] {
                                                                                                     new {MediaType=  MediaType.DVD, BasePrice = 22.00m, SalePrice =  22.00m}
                                                                                                 },
                                                                Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/GenComProductCatalog/DRC_DVD_Boxes_Pilgrims_Progress_smallb.jpg&width=0&height=225&quality=90" },
                                                                Description = @"
                                                                                Pilgrim�s Progress: Journey to Heaven is a modern adaptation of John Bunyan�s beloved
                                                                                classic. It is the #1 Christian fantasy of all time! The novel has been heralded
                                                                                as a literary masterpiece around the world, as it has been published in over 100
                                                                                languages and is the most read book other than the Bible.
                                                                                <br>
                                                                                <br>
                                                                                Amazing visual effects, beautiful locations and a wonderful cast bring to life the
                                                                                story that has inspired each generation for hundreds of years. Follow Christian
                                                                                and his companions on a great Journey from the City of Destruction to the gates
                                                                                of Heaven as they face obstacles large and small, man-made and demon-spawned.<br>
                                                                                &nbsp;<br>
                                                                                Beyond the gripping drama, Bunyan�s powerful allegory teaches us all the hazards
                                                                                and hopes of the Christian life, and it features the triumphant glory that awaits
                                                                                all who faithfully follow the King of kings!
                                                                               "
                                                            });

            var escapeFromHell = CreateStubbedProduct(new
                                                          {
                                                              Title = "Escape From Hell",
                                                              Author_Producer = string.Empty,
                                                              ProductCode = "K263",
                                                              StockItemDetails = new dynamic[] {
                                                                                                   new {MediaType=  MediaType.DVD, BasePrice = 22.00m, SalePrice =  22.00m}
                                                                                               },
                                                              Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                              Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/E-CommerceProductCatalog/K263.jpg&width=0&height=225&quality=90" },
                                                              Description = @"    
                                                                                Do You believe in life after death? Dr. Eric Robinson wants to believe and experience
                                                                                that infinite love and warmth that near death testimonies claim is on the other
                                                                                side of life. His colleague, Dr. Marissa Holloway, is on a crusade to alleviate
                                                                                the fear of death and suffering by proving to the world that heaven awaits everyone.
                                                                                In a moment of desperation, Dr. Robinson faces death and discovers the reality of
                                                                                hell - a place the Bible portrays - a hell from which we must all escape.
                                                                                <br />
                                                                                This new evangelistic movie challenges viewers to confront the inevitable�death
                                                                                and judgment. In action-packed drama we get a glimpse of what heaven and hell may
                                                                                be like. It is a wonderful tool to share Christ with unsaved family and friends.
                                                                              "
                                                          });

            var irvingRothInterview = CreateStubbedProduct(new
                                                               {
                                                                   Title = "Irving Roth Interview",
                                                                   Author_Producer = string.Empty,
                                                                   ProductCode = "K482",
                                                                   StockItemDetails = new dynamic[] {
                                                                                                        new {MediaType=  MediaType.DVD, BasePrice = 20.00m, SalePrice =  20.00m}
                                                                                                    },
                                                                   Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                   Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/GenComProductCatalog/IrvingRoth.png&width=0&height=225&quality=90" },
                                                                   Description = @"
                                                                                    In this riveting 30 minute program you are taken on a personal tour of Auschwitz,
                                                                                    Treblinka and Majdanek by Irving Roth, a Holocaust Survivor. Mr. Roth re-calls
                                                                                    in great detail what life was like in three of the horrific death camps responsible
                                                                                    for the genocide of the Jewish people during World War II.
                                                                                   "
                                                               });

            var hageeFamilyTreasures = CreateStubbedProduct(new
                                                                {
                                                                    Title = "Hagee Family Treasures",
                                                                    Author_Producer = "Pastor John Hagee, Pastor Matthew Hagee & Diana Hagee",
                                                                    ProductCode = "K192",
                                                                    StockItemDetails = new dynamic[] {
                                                                                                         new {MediaType=  MediaType.DVD, BasePrice = 45.00m, SalePrice =  45.00m},
                                                                                                         new {MediaType=  MediaType.CD, BasePrice = 21.00m, SalePrice =  21.00m}
                                                                                                     },
                                                                    Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                    Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/GenComProductCatalog/Family_Treasures-lg.jpg&width=0&height=225&quality=90" },
                                                                    Description = @"
                                                                                    This is an exciting new series, that for the first time combines&nbsp;Pastor John
                                                                                    Hagee, Diana Hagee and Pastor Matthew Hagee's teachings on God�s Plan for our families.
                                                                                    <br />

                                                                                    In Pastor Hagee�s teaching, ""God's Plan for children,"" he explains that every
                                                                                    parent has a choice to either raise their children by the&nbsp; law of God or the
                                                                                    law of the jungle. There are many opinions about how to train&nbsp; up a child,
                                                                                    but Pastor Hagee shows you God's plan for rearing your children.
                                                                                    <br />
                                                                                    Next is first lady of Cornerstone, Diana Hagee.&nbsp; Diana covers ""The Supplications
                                                                                    of a Mother.� Supplication means a petition, a request or prayer. Learn 5 specific
                                                                                    ways you should pray for your children, as&nbsp; well as be entertained with personal
                                                                                    Hagee family stories.
                                                                                    <br />

                                                                                    Finally, Pastor Matthew Hagee teaches ""God's plan for the Man."" It&nbsp; all begins
                                                                                    when the husband submits to Christ and gives sacrificial love to his wife. God has
                                                                                    called every man to be his wife's savior, sanctifier and satisfier. Fulfilling these
                                                                                    roles will guarantee your wife's every need, want and desire.
                                                                                    <br />
                                                                                    <br />
                                                                                    This is a treasure and will be enjoyed by all.

                                                                                   "
                                                                });

            var theKingdom_PowerAndGlory = CreateStubbedProduct(new
                                                                    {
                                                                        Title = "The Kingdom, The Power and The Glory",
                                                                        Author_Producer = "Pastor Matthew Hagee",
                                                                        ProductCode = "SM1016E",
                                                                        StockItemDetails = new dynamic[] {
                                                                                                             new {MediaType=  MediaType.DVD, BasePrice = 45.00m, SalePrice =  45.00m},
                                                                                                             new {MediaType=  MediaType.CD, BasePrice = 21.00m, SalePrice =  21.00m}
                                                                                                         },
                                                                        Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                        Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/GenComProductCatalog/kingdom_power_Glory_lg.jpg&width=0&height=225&quality=90" },
                                                                        Description = @"
                                                                                        History paints fantastic portraits of some of the greatest kingdoms ever established.
                                                                                        As far back as King Nebuchadnezzar, men have sought to establish one kingdom that
                                                                                        would rule the world. Modern society has tried to build their own kingdoms yet none
                                                                                        succeed. The United States has strived to build a kingdom built on democracy and
                                                                                        capitalism, and that also seems to be in shambles.<br>
                                                                                        <br>
                                                                                        There is not a system of government on the face of the earth that has the power
                                                                                        to rule the world.<br>
                                                                                        The only true Kingdom is ruled by the King of Kings and Lord of Lords, Jesus Christ!
                                                                                        His Kingdom shall see no end!
                                                                                        <br>
                                                                                        Pastor Matthew Hagee shows you how to become a part of His Kingdom as well as the
                                                                                        eternal benefits that await those who dedicate themselves to the Kingdom of God
                                                                                        and are not deceived by the kingdoms of this world.
                                                                                        <br>
                                                                                       "
                                                                    });

            var the_Secrets_of_Jonathan_Sperry = CreateStubbedProduct(new
                                                                          {
                                                                              Title = "The Secrets of Jonathan Sperry",
                                                                              Author_Producer = string.Empty,
                                                                              ProductCode = "K475",
                                                                              StockItemDetails = new dynamic[] {
                                                                                                                   new {MediaType=  MediaType.DVD, BasePrice = 22.00m, SalePrice =  22.00m},
                                                                                                               },
                                                                              Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                              Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/GenComProductCatalog/JonathanSperry-lg.jpg&width=0&height=225&quality=90" },
                                                                              Description = @"
                                                                                            Gavin MacLeod (The Love Boat) plays the title character in this faith-based family film. 
                                                                                            It is 1970 and best buddies Dustin, Albert and Mark are three 12-year-olds looking forward to a summer of fun. 
                                                                                            Dustin has a crush on Tanya and wants to ask her out on a first date, but his friends aren't much help, 
                                                                                            offering some terrible (and hilarious) advice. Complicating matters is Nick, the town bully, who also likes Tanya. 
                                                                                            Dustin meets 74-year-old Jonathan Sperry and his reclusive neighbor Mr. Barnes. What happens that summer will 
                                                                                            change each of them forever in this touching multigenerational tale of first love and life lessons. A truly inspired Christian film!
                                                                                             "
                                                                          });

            var against_All_Odds = CreateStubbedProduct(new
                                                            {
                                                                Title = "Against All Odds - Jewish Miracles",
                                                                Author_Producer = string.Empty,
                                                                ProductCode = "K372",
                                                                StockItemDetails = new dynamic[] {
                                                                                                     new {MediaType=  MediaType.DVD, BasePrice = 22.00m, SalePrice =  22.00m},
                                                                                                 },
                                                                Categories = new[] { Category.Interviews, Category.MoviesAndDocumentaries },
                                                                Images = new[] { "http://www.jhm.org/Console/Common/Image.asp?image=/Media/E-CommerceProductCatalog/K372D.jpg&width=0&height=225&quality=90" },
                                                                Description = @"
                                                                                Out of every modern-day war or conflict in Israel, come incredible stories that
                                                                                go against all odds and can only be classified as miracles.
                                                                                <br />
                                                                                In a remarkable TV special, Against All Odds: In Search of a Miracle the personal
                                                                                stories of generals, commanders, soldiers, war heroes and common citizens, have
                                                                                been recorded to show the most powerful, most transforming moments of their lives.
                                                                                Experiences that changed them forever�events that helped shape the course of their
                                                                                nation's history.
                                                                                <br />
                                                                                Key moments in their stories are reenacted. Historical events are illustrated with
                                                                                rare film footage from Israel's National Archive. The narratives are in their own
                                                                                voices, telling these stories as they happened to them.
                                                                                <br />
                                                                                You will want to make this special 90 minute video part of your library.
                                                                                <br />
                                                                                This incredible story has never been told until now.
                                                                                <br />
                                                                                Against All Odds is a dramatic, history-making journey into the soul of modern day
                                                                                Israel.
                                                                                <br />
                                                                               "
                                                            });

            mockedProducts.AddUnique(pilgrimsProgress);
            mockedProducts.AddUnique(escapeFromHell);
            mockedProducts.AddUnique(irvingRothInterview);
            mockedProducts.AddUnique(hageeFamilyTreasures);
            mockedProducts.AddUnique(theKingdom_PowerAndGlory);
            mockedProducts.AddUnique(the_Secrets_of_Jonathan_Sperry);
            mockedProducts.AddUnique(against_All_Odds);
        }

        private Product CreateStubbedProduct(dynamic prodDetail)
        {
            var rProduct = new Product(prodDetail.ProductCode, prodDetail.Categories);
            rProduct.SetTitle(prodDetail.Title);
            rProduct.SetAuthor(prodDetail.Author_Producer);
            rProduct.SetDescription(prodDetail.Description);
            rProduct.AddImages(null, prodDetail.Images);

            foreach (var stockItemDetail in prodDetail.StockItemDetails)
            {
                rProduct.AddStockItems(new StockItem(stockItemDetail.MediaType, 100, true, stockItemDetail.BasePrice, stockItemDetail.SalePrice, true, stockItemDetail.WarehouseCode));
            }

            User user = new User { Username = "MockUserName" };


            for (int i = 0; i < randomNumberOfReviews.Next(4, 20); i++)
            {
                rProduct.AddReview(new Review(prodDetail.ProductCode, DateTime.Now, (decimal)(ratingRandom.NextDouble() * 100), "ReviewTitle", GetMockHtml(), user, ReviewStatus.Active));
            }

            return rProduct;
        }

        public IEnumerable<IProduct> FindProducts(string searchString)
        {
            return mockedProducts.Where(x => x.Code.ToLower().Contains(searchString.ToLower())
                                             || x.Title.ToLower().Contains(searchString.ToLower())
                                             || x.Description.ToLower().Contains(searchString.ToLower())
                                             || x.Author.ToLower().Contains(searchString.ToLower()));
        }

        public IEnumerable<IProduct> GetProducts(string categories, string mediaType)
        {
            var cats = categories.Split(',');
            var mType = MediaType.TryGetFromDisplayName<MediaType>(mediaType);

            if (!cats.Contains("All") && cats.IsNot().Empty() && mType.IsNot().Null())
            {
                return mockedProducts.Where(x => (x.Categories.ToLIST().Find(cat => cats.Contains(cat.DisplayName)).IsNot().Null() && x.StockItems.ToLIST().Find(stockItem => stockItem.MediaType == mType).IsNot().Null()));
            }
            else if (!cats.Contains("All") && cats.IsNot().Empty())
            {
                /*List<Product> foundProducts = new List<Product>();
                foreach (var p in mockedProducts)
                {
                    if (p.Categories != null)
                    {
                        List<Category> productCategories = p.Categories.ToLIST();
                        foreach (var cat in productCategories)
                        {
                            if (cat == null)
                            {
                                continue;
                            }
                            if ( cats.Contains(cat.DisplayName) )
                            {
                                foundProducts.Add(p);
                            }
                        }
                    }
                }
                return foundProducts;*/
                return mockedProducts.Where(x => 
                    (
                        x.Categories.ToLIST().Find(
                            cat => cat.IsNot().Null() && cats.Contains(cat.DisplayName)
                        ).IsNot().Null()
                    )
                );
            }
            else if (mType.IsNot().Null())
            {
                return mockedProducts.Where(x => (x.StockItems.ToLIST().Find(stockItem => stockItem.MediaType == mType).IsNot().Null()));
            }
            else if (cats.Contains("All"))
            {
                return mockedProducts;
            }
            else
            {
                return new List<Product>();
            }

        }

        public IEnumerable<IProduct> GetProducts(string authors)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> FindProducts(string searchString, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> FindProducts(string environment, string searchString, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProducts(string categories, string subjects, string mediaType, string author, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProducts(string authors, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetFeaturedItems(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetAllProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetAllProducts(string environment)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetLatestReleasedProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProduct(string environment, string sku)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetFeaturedItems()
        {
            return mockedProducts.Where(x => x.Categories.Contains(Category.Interviews));
        }

        public IEnumerable<IProduct> GetLatestReleasedProducts()
        {
            return mockedProducts.Take(10);
        }

        public IProduct GetProduct(string sku)
        {
            return mockedProducts.Find(x => x.Code == sku);
        }

        public StockItem GetStockItemByProductId(string environment, Guid productId, MediaType mediaType)
        {
            throw new NotImplementedException();
        }

        public Company GetCompany()
        {
            return Company.JHM_UnitedStates;
        }

        public IEnumerable<IProduct> GetProductsByAuthor(string environment, string author, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsByAuthorOrderedBy(string environment, string author, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsByCategory(string environment, string categories, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsByCategoryOrderedBy(string environment, string categories, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsBySubject(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsBySubjectOrderedBy(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsOnSale(string environment, string onSale, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsByMediaTypes(string environment, string mediaTypes, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetFeaturedOnTelevisionProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetFeaturedOnGetvProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            throw new NotImplementedException();
        }

        #region Private Methods

        private IEnumerable<IProduct> GetStubbedProducts(int quantity, string categories)
        {
            var random = new Random();
            var rList = new List<Product>();
            var categoryList = GetCategoryList(categories);

            for (int i = 0; i < quantity; i++)
            {
                var product = new Product("MockCode{0}".FormatWith(i), categoryList);
                product.SetDescription(GetMockText());
                product.SetTitle("MockTitle");
                product.SetAuthor("MockAuthor");
                product.SetTrailer("MockTrailerUrl");
                product.AddStockItems(null, new List<StockItem>()
                                                {
                                                    GetMockStockItem(MediaType.Book, 100, true, 10.00m, 5.00m, "10"),
                                                    GetMockStockItem(MediaType.DVD, 1, true, 15.00m, 15.00m, "10"),
                                                    GetMockStockItem(MediaType.AudioFileDownload, Int32.MaxValue, true, 2.00m, 2.00m, "10"),
                                                    GetMockStockItem(MediaType.OnDemand, Int32.MaxValue, true, 5.00m, 5.00m, "10"),
                                                }.ToArray());

                User user = new User { Username = "MockUserName" };

                product.AddReview(null, new List<Review>
                                            {
                                                new Review(product.Code, DateTime.Now, (decimal) (random.NextDouble() * 100), "ReviewTitle", GetMockHtml(), user, ReviewStatus.Active),
                                                new Review(product.Code, DateTime.Now, (decimal) (random.NextDouble() * 100), "ReviewTitle", GetMockHtml(), user, ReviewStatus.Active),
                                                new Review(product.Code, DateTime.Now, (decimal) (random.NextDouble() * 100), "ReviewTitle", GetMockHtml(), user, ReviewStatus.Active),
                                                new Review(product.Code, DateTime.Now, (decimal) (random.NextDouble() * 100), "ReviewTitle", GetMockHtml(), user, ReviewStatus.Active),
                                            }.ToArray());

                product.AddRelatedItems(null, product.StockItems.Select(x => new RelatedItem{Code = x.ProductCode, Description = x.Description, Image = x.ImageLocation, Title = x.Title}).ToArray());
                rList.Add(product);
            }

            return rList;
        }

        private StockItem GetMockStockItem(MediaType mediaType, int quantityInStock, bool allowBackorder, decimal basePrice, decimal salePrice, string warehouseCode)
        {
            var rStockItem = new StockItem(mediaType, quantityInStock, allowBackorder, basePrice, salePrice, true, warehouseCode);
            rStockItem.SetDescription(GetMockHtml());
            return rStockItem;
        }

        private string GetMockText()
        {
            return
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        }

        private string GetMockHtml()
        {
            return @"<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>";
        }

        private IEnumerable<Category> GetCategoryList(string categories)
        {
            IEnumerable<Category> categoryList;
            if (categories.IsNot().NullOrEmpty())
                categoryList = GetCategoriesFromCommaString(categories);
            else
                categoryList = new List<Category>()
                                   {
                                       Category.Interviews,
                                       Category.MoviesAndDocumentaries
                                   };
            return categoryList;
        }

        private IEnumerable<Category> GetCategoriesFromCommaString(string categories)
        {
            var cats = categories.Split(",".ToCharArray());
            var rCategories = new List<Category>();
            foreach (var cat in cats)
            {
                var c = Category.FromDisplayName<Category>(cat);
                rCategories.AddUnique(c);
            }

            return rCategories;
        }

        #endregion



        public StockItem GetStockItem(string environment, string productCode, MediaType mediaType)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<IProduct> UpdateProductJson(string environment)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProductsByPublisher(string environment, string publisher, PagingInfo pagingInfo, ref int totalResults)
        {
            throw new NotImplementedException();
        }

        public void UpdateProductNoteFileLength(string environment, string productCode, decimal fileLength)
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductFromItemCode(string environment, string itemProductCode, bool includeOtherStockItems)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetAllProducts(string environment, bool force)
        {
            throw new NotImplementedException();
        }
    }
}