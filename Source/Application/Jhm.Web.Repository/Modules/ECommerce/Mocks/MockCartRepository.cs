using System;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.DonorStudio.Services;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.Repository.Modules.Account
{
    public class MockCartRepository : ICartRepository
    {
        private Cart cart;

        public Cart Get(object id)
        {
            if (cart.Is().Null())
                CreateNewCart();

            return cart;
        }

        public void Save(Cart entity)
        {
            foreach (var cartLineItem in entity.Items.Where(x => x.Item.Id == Guid.Empty))
            {
                CartItem item = null;
                int quantity = cartLineItem.Quantity;
                if(cartLineItem.Item is DonationCartItem)
                {
                    var itemAsDonationCartItem = (cartLineItem.Item as DonationCartItem);
                    var donationCartItem = new DonationCartItem(Guid.NewGuid());
                    donationCartItem.SetCode(cartLineItem.Item.Code);
                    donationCartItem.SetDescription(cartLineItem.Item.Description);
                    donationCartItem.SetPrice(cartLineItem.Item.Price);
                    donationCartItem.SetTitle(cartLineItem.Item.Title);
                    donationCartItem.SetCertificateName(itemAsDonationCartItem.CertificateName);
                    donationCartItem.SetDonationFrequency(itemAsDonationCartItem.DonationFrequency);
                    donationCartItem.SetStockItemChosen(itemAsDonationCartItem.StockItemChosen);
                    donationCartItem.SetIncludedStockItems(itemAsDonationCartItem.IncludedStockItems);
                    donationCartItem.SetProjectCode(itemAsDonationCartItem.ProjectCode);
                    item = donationCartItem;
                }
                if (cartLineItem.Item is StockItem)
                {
                    var itemAsStockItem = cartLineItem.Item as StockItem;
                    var stockItem = new StockItem(Guid.NewGuid(), itemAsStockItem.MediaType, itemAsStockItem.QuantityInStock, itemAsStockItem.AllowBackorder, itemAsStockItem.BasePrice, itemAsStockItem.SalePrice, true, itemAsStockItem.MediaPath);
                    stockItem.SetCode(itemAsStockItem.Code);
                    stockItem.SetTitle(itemAsStockItem.Title);
                    stockItem.SetDescription(itemAsStockItem.Description);
                    stockItem.SetPrice(itemAsStockItem.Price);
                    stockItem.SetWarehouseCode(itemAsStockItem.Warehouse);
                    item = stockItem;
                }
                entity.RemoveItem(cartLineItem.Item);
                entity.AddItem(item, quantity);
            }

            cart = entity;
        }

        public void Delete(Cart entity)
        {
            cart = null;
        }

        public Cart GetCartByUserName(string userName)
        {
            if (cart.Is().Null())
                CreateNewCart();

            return cart;
        }

        public long SaveTransaction(string dsEnvironment, CheckoutOrderReviewViewModel checkoutData)
        {
            throw new NotImplementedException();
        }

        public DiscountCode GetDiscountByCodeName(string codeName)
        {
            throw new NotImplementedException();
        }

        public long SaveTransaction(string dsEnvironment, DsTransaction DsTransaction)
        {
            throw new NotImplementedException();
        }


        public void CreateNewCart()
        {
            cart = new Cart();

            //for (int i = 0; i < 25; i++)
            //{
            //    var cartItem = new CartItem();
            //    cartItem.SetCode("Code{0}".FormatWith(i));
            //    cart.AddItem(cartItem, i + 1);
            //}
        }
    }
}