﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Repository.Modules.Account
{
    public partial class MockDonationRepository
    {
        public static readonly Guid NewSaltPartnerOptionId = new Guid("1CB395F6-2BC0-45D8-BB77-00D69A54CE52");
        public static readonly Guid ExistingSaltPartnerOptionId = new Guid("FAEB50F2-1487-4A7E-A63C-5D3D3C4CEF85");
        public static readonly Guid SaltCovenantId = new Guid("5272C19A-90A8-4F01-B9C7-03F55BB5B43E");
        public static readonly Guid FirstFruitsId = new Guid("4B2B5F6E-81D4-49DB-A6E2-FC003160929B");
        public static readonly Guid TheUnityCrossId = new Guid("474DA4A8-6890-479A-887A-50F8842F6379");
        public static readonly Guid Matthew1820Id = new Guid("FEB96B58-9EF7-4025-A199-2DFD978D4CD6");
        public static readonly Guid TheComingKingId = new Guid("268AB372-83E3-4E6B-A2AF-EC0C20FBB9E5");
        public static readonly Guid FruitoftheRighteousPlatterId = new Guid("5CA81E0C-4452-4131-96B9-D6BBF80C5541");
        public static readonly Guid DigitalArchiveSolutionId = new Guid("CD959248-BFB4-4620-A91F-474DBCE9E1C6");
        public static readonly Guid FoundersWallId = new Guid("46170805-C544-4EDB-9574-DA4C41C67655");
        public static readonly Guid TreeofLifeId = new Guid("EA9DC6B3-FE9D-48D1-A142-9A5D84CA22F4");
        public static readonly Guid WalkofHonorId = new Guid("D34E4F80-081C-401E-8849-49382B8DF7F2");
        public static readonly Guid ManofVisionId = new Guid("BFE01540-151C-43C4-8D9D-3C9931C9D94E");
        public static readonly Guid ExodusIIId = new Guid("05A5D6C9-C7EC-4B88-8228-096F1C722C12");
        public static readonly Guid DivineServantId = new Guid("A7282EAF-27A3-4A3C-A15D-6012E4A6D661");
        public static readonly Guid OneTimeId = new Guid("47AD28D3-F6D4-4AEE-B104-58F28E627A4A");
        public static readonly Guid PlantYourRootsId = new Guid("72F194C4-C975-4B82-AC3B-1A4031E5392B");
        public static readonly Guid TenCommandmentsArtworkId = new Guid("8D46B905-402A-41D9-8675-8D36A746DEA1");
        public static readonly Guid MilitaryWarriorsId = new Guid("C589D300-4572-4D93-8116-4AE12295E1B0");
        public static readonly Guid FreedomIsntFree = new Guid("E73E15F7-1179-4F91-B102-95749B3A132A");
        public static readonly Guid EliWieselId = new Guid("493F5452-BC9A-4322-BDC2-B95F128CAF94");
        public static readonly Guid WarriorChefCookOffId = new Guid("0040D8BF-609E-4252-B4C9-601ED35E9C1D");

        public const String ProphecticBlessingCode = "PropheticBlessing";

        private const string USWarehouse = "10";
        private const string UKWarehouse = "50";
        private const string CanadaWarehouse = "20";
    }
}
