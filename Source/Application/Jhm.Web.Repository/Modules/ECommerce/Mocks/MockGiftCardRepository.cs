﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Web.Core.Models.ECommerce;

namespace Jhm.Web.Repository.Modules.ECommerce.Mocks
{
    public class MockGiftCardRepository : IGiftCardRepository
    {
        private readonly List<GiftCard> giftCards = new List<GiftCard>(); 

        public MockGiftCardRepository()
        {
            giftCards.Add(new GiftCard(new Guid("D2B8893E-CC4C-4C22-8FFA-9F1C912F2C31"))
            {
                ImagePath = "https://media.jhm.org/Images/Web/GiftCards/D2B8893E-CC4C-4C22-8FFA-9F1C912F2C31_small.jpg",
                ProductCode = "GCJHM",
                Title = "Gift Card - All the Gospel to All the World",
                Warehouse = "10"
            });

            //giftCards.Add(new GiftCard(new Guid("C6BBE043-79AB-42AD-B5CE-407A714E8537"))
            //{
            //    ImagePath = "https://media.jhm.org/Images/Web/GiftCards/C6BBE043-79AB-42AD-B5CE-407A714E8537_small.jpg",
            //    ProductCode = "GCJHM",
            //    Title = "For Mom Gift Card",
            //    Warehouse = "10"
            //});

            //giftCards.Add(new GiftCard(new Guid("47A807C2-5313-4D54-B03C-B3FBC0C338FE"))
            //{
            //    ImagePath = "https://media.jhm.org/Images/Web/GiftCards/47A807C2-5313-4D54-B03C-B3FBC0C338FE_small.jpg",
            //    ProductCode = "GCJHM",
            //    Title = "For Dad Gift Card",
            //    Warehouse = "10"
            //});

            //giftCards.Add(new GiftCard(new Guid("7F691BC8-922C-4C31-91EA-8C1B276BBE49"))
            //{
            //    ImagePath = "https://media.jhm.org/Images/Web/GiftCards/7F691BC8-922C-4C31-91EA-8C1B276BBE49_small.jpg",
            //    ProductCode = "GCJHM",
            //    Title = "Congratulations Gift Card",
            //    Warehouse = "10"
            //});

        }

        public IEnumerable<GiftCard> GetAvailableGiftCards()
        {
            return giftCards;
        }

        public GiftCard GetGiftCard(Guid giftCardId)
        {
            return giftCards.FirstOrDefault(x => x.Id == giftCardId);
        }
    }
}
