﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public interface ISubscriptionsRepository
    {
        IEnumerable<ISubscription> LoadAllSubscriptions(string environment, bool forceUpdateToCacheFile);
        IEnumerable<ISubscription> UpdateSubscriptionsJson(string environment);
        IEnumerable<ISubscription> GetAvailableMagazineSubscriptions(string environment);

        ISubscription GetSubscription(string environment, string subscriptionCode);
    }
}
