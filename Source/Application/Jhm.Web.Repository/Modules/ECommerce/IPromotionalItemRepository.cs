﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public interface IPromotionalItemRepository
    {
        List<PromotionalItem> GetAll();
        List<PromotionalItem> GetAllActive();
    }
}
