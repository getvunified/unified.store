﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.DonorStudio.Services.Services;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.Account
{
    public class DiscountCodeRepository : RepositoryDecorator<DiscountCode>, IDiscountCodeRepository
    {
        private DSServiceCollection _dsServiceCollection;

        public DiscountCodeRepository(DSServiceCollection dsServiceCollection)
        {
            _dsServiceCollection = dsServiceCollection;
        }

        public List<DiscountCodeItems> GetAllAssociatedProducts(DiscountCode discountCode)
        {
            var now = DateTime.Now;
            var discountCodeItems = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCodeItems>()
                                        .Where(x => x.CodeId == discountCode.Id)
                                        .Where(x => x.StartDate <= now)
                                        .Where(x => x.EndDate >= now).List<DiscountCodeItems>().ToList();

            return discountCodeItems;
        }

        public DiscountCode GetDiscountCodeByName(string codeName)
        {
            var now = DateTime.Now;
            
            SingleUseDiscountCodes sudc = null;
            DiscountCodeOptionalCriteria dcoc = null;
            DiscountCode d = null;

            var allDiscountCodes = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCode>(() => d)
                //.Left.JoinQueryOver(() => d.SingleUseDiscountCodes, () => sudc)
                //.Left.JoinQueryOver(() => d.OptionalCriteria, () => dcoc)
                //.Where(() => d.Id != null || sudc.DiscountCode.Id == d.Id)
                //.Where(() => dcoc.Id == null || d.Id == dcoc.DiscountCodeId)
                .List<DiscountCode>();


            var discountCodes = allDiscountCodes
                .Where(x => x.CodeName.Equals(codeName, StringComparison.InvariantCultureIgnoreCase)).ToList();

            //if (discountCode != null)
            //{
            //    var code = discountCode;
            //    discountCode.Rules = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCodeRequirements>()
            //        .Where(x => x.DiscountCodeId == code.Id).SingleOrDefault<DiscountCodeRequirements>() 
            //        ?? new DiscountCodeRequirements();

            //}

            if (!discountCodes.Any())
            {
                //var groupCodes = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCode>()
                //                                                      .Where(x => x.Rules.IsSingleUsePrefix && (x.Rules.StartDate <= now) && (x.Rules.EndDate >= now))
                //                                                      .Where(y => y.Rules.IsActive)
                //                                                      .List<DiscountCode>().ToList();

                var r = new DiscountCodeRequirements();

                var groupCodes = FluentNHibernateHelper.CurrentSession.QueryOver(() => d)
                    .JoinQueryOver(() => d.Rules, () => r)
                    .Where(() => d.Rules != null)
                    .Where(() => r.IsSingleUsePrefix && (r.StartDate <= now) && (r.EndDate >= now))
                    .Where(() => r.IsActive)
                    .List<DiscountCode>().ToList();

                var discountCode = groupCodes.SingleOrDefault(x => codeName.StartsWith(x.CodeName));
                if (discountCode != null)
                {
                    DiscountCode code = discountCode;
                    discountCode.Rules = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCodeRequirements>()
                                                .Where(x => x.DiscountCodeId == code.Id).List();

                }
            }



            return discountCodes.FirstOrDefault();
        }

        public IList<DiscountCode> GetAllAutoApplyCodes()
        {
            SingleUseDiscountCodes sudc = null;
            DiscountCodeOptionalCriteria dcoc = null;
            DiscountCode d = null;

            var allDiscountCodes = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCode>(() => d)
                .Left.JoinQueryOver(() => d.SingleUseDiscountCodes, () => sudc)
                .Left.JoinQueryOver(() => d.OptionalCriteria, () => dcoc)
                .Where(() => d.Id != null || sudc.DiscountCode.Id == d.Id)
                .Where(() => d.Rules != null)
                //.Where(() => d.Id == dcoc.DiscountCodeId)
                .List<DiscountCode>();

            //foreach (var discountCode in allDiscountCodes.Where(x => x.Rules == null))
            //{
            //    discountCode.Rules = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCodeRequirements>()
            //                                .Where(x => x.DiscountCodeId == discountCode.Id).SingleOrDefault<DiscountCodeRequirements>();
            //}

            allDiscountCodes = allDiscountCodes.Where(x => x.Rules != null && x.Rules.Any(y => y.IsAutoApply))
                                               .OrderBy(y => y.Rules.First().EndDate)
                                               .ToList();

            return allDiscountCodes;

        }

        public IList<DiscountCode> GetAllDiscountCodes()
        {
            SingleUseDiscountCodes sudc = null;
            DiscountCode d = null;

            var allDiscountCodes = FluentNHibernateHelper.CurrentSession.CreateCriteria<DiscountCode>()
                .Add(Restrictions.IsNotNull("Id"))
                .AddOrder(Order.Asc("StartDate")).List<DiscountCode>();

            /*return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(DiscountCode), "dc")
                                                     .Add(Restrictions.Eq("CodeName", codeName))
                                                     .Add(Restrictions.Le("StartDate", DateTime.Now))
                                                     .Add(Restrictions.Ge("EndDate", DateTime.Now))
                                                     .UniqueResult<DiscountCode>();*/

            /*var allDiscountCodes = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCode>(() => d)
                            .Where(() => (d.Id != null))
                            .OrderBy(() => d.StartDate).Asc.List<DiscountCode>();*/


            /*var singleUse =
                FluentNHibernateHelper.CurrentSession.QueryOver<SingleUseDiscountCodes>()
                                      .Where(su => su.IsActive)
                                      .List<SingleUseDiscountCodes>();

            foreach (var allDiscountCode in allDiscountCodes)
            {
                foreach (var singleUseDiscountCode in singleUse)
                {
                    if (allDiscountCode.Id == singleUseDiscountCode.DiscountCodeId)
                    {
                        allDiscountCode.SingleUseDiscountCodes.Add(singleUseDiscountCode);
                    }
                }
            }*/

            return allDiscountCodes;
        }

        public bool ValidateSingleUseDiscountCodes(DiscountCode discountCode, string suffix)
        {
            var returnValue = false;
            SingleUseDiscountCodes sudc = null;
            DiscountCode d = null;

            var allDiscountCodes = FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCode>(() => d)
                .Left.JoinQueryOver(() => d.SingleUseDiscountCodes, () => sudc)
                .Where(() => sudc.IsActive)
                .Where(() => sudc.DiscountCode.Id == d.Id)
                .Where(() => sudc.DiscountCodeSuffix == suffix)
                .List<DiscountCode>();

            if (allDiscountCodes.Count > 0)
            {
                returnValue = true;
            }

            return returnValue;
        }

        


        public void IncreaseUseCount(Guid discountCodeId)
        {
            var code = FluentNHibernateHelper.CurrentSession.Query<DiscountCode>().SingleOrDefault(x => x.Id == discountCodeId);
            if (code == null)
            {
                throw new Exception("DiscountCode not found");
            }

            var currentCount = code.UseCount;
            code.UseCount = currentCount + 1;
            FluentNHibernateHelper.CurrentSession.Update(code);
        }
    }
}
