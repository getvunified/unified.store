﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IDiscountCodeOptionalCriteriaRepository
    {
        List<DiscountCodeOptionalCriteria> GetItemsRelatedToDiscountCode(DiscountCode discountCode);
    }
}
