﻿using System.Collections.Generic;
using System.Linq;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public class DiscountCodeOptionalCriteriaRepository
    {
        public List<DiscountCodeOptionalCriteria> GetItemsRelatedToDiscountCode(DiscountCode discountCode)
        {
            return FluentNHibernateHelper.CurrentSession.Query<DiscountCodeOptionalCriteria>()
                .Where(x => x.DiscountCodeId == discountCode.Id).ToList(); 
        }
    }
}
