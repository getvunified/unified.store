using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public class DonationRepository : RepositoryDecorator<Donation>, IDonationRepository
    {
        public IEnumerable<Donation> GetActiveDonationOpportunities()
        {
            
            throw new NotImplementedException();
        }

        public IDonation GetDonation(string code)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Donation> GetActiveCanadaDonationOpportunities()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Donation> GetActiveUKDonationOpportunities()
        {
            throw new NotImplementedException();
        }

        public IDonation GetUSDonation(string donationCode)
        {
            throw new NotImplementedException();
        }

        public IDonation GetCanadaDonation(string donationCode)
        {
            throw new NotImplementedException();
        }

        public IDonation GetUKDonation(string donationCode)
        {
            throw new NotImplementedException();
        }
    }
}