﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public class DiscountItemRepository : RepositoryDecorator<DiscountCodeItems>, IDiscountItemRepository
    {
        public List<DiscountCodeItems> GetItemsRelatedToDiscountCode(DiscountCode discountCode)
        {
            var now = DateTime.Now;
            return FluentNHibernateHelper.CurrentSession.Query<DiscountCodeItems>()
                .Where(x => x.CodeId == discountCode.Id)
                .Where(x => x.StartDate <= now)
                .Where(x => x.EndDate >= now)
                .ToList(); 

        }
    }
}
