﻿using System.Collections.Generic;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IDonationRepository : IRepository<Donation>
    {
        IEnumerable<Donation> GetActiveDonationOpportunities();
        IDonation GetDonation(string code);
        IEnumerable<Donation> GetActiveCanadaDonationOpportunities();
        IEnumerable<Donation> GetActiveUKDonationOpportunities();
        IDonation GetUSDonation(string donationCode);

        IDonation GetCanadaDonation(string donationCode);

        IDonation GetUKDonation(string donationCode);
    }
}