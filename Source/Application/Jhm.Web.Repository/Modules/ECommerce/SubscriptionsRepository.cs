﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using log4net;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public class SubscriptionsRepository : ISubscriptionsRepository
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SubscriptionsRepository));
        private DSServiceCollection dsServiceCollection;

        public SubscriptionsRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }

        private static IEnumerable<ISubscription> GetSubscriptionsFromMemory(string environment)
        {
            var cache  = MemoryCache.Default;
            var key    = environment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix;
            var result = cache[key] as IEnumerable<ISubscription>;
             
            //var result = (IEnumerable<ISubscription>)HttpContext.Current.Application[environment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix];
            return result;
        }


        public IEnumerable<ISubscription> GetAvailableMagazineSubscriptions(string environment)
        {
            var allSubs = GetSubscriptionsFromMemory(environment);

            return allSubs ?? new List<ISubscription>();
        }

        public ISubscription GetSubscription(string environment, string subscriptionCode)
        {
            var allSubs = GetAvailableMagazineSubscriptions(environment);
            return allSubs.SingleOrDefault(x => x.SubscriptionCode == subscriptionCode) ?? new Subscription();
        }


        public IEnumerable<ISubscription> LoadAllSubscriptions(string environment, bool forceUpdateToCacheFile)
        {
            var filepath = HttpContext.Current.Server.MapPath(@"\") + environment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix + ".bin";
            if (!File.Exists(filepath) || forceUpdateToCacheFile)
            {
                return UpdateSubscriptionsJson(environment);
            }
            IEnumerable<ISubscription> result = ReadBinaryFromFile<List<Subscription>>(filepath);
            result = result.OrderByDescending(s => s.SubscriptionCode);
            return result;
        }

        public IEnumerable<ISubscription> UpdateSubscriptionsJson(string environment)
        {
            var filepath = HttpContext.Current.Server.MapPath(@"\") + environment + MagicStringEliminator.ApplicationKeys.SubscriptionsPrefix + ".bin";
            try
            {
                var result =
                    dsServiceCollection.DsSystemAdministrationService.GetMagazineSubscriptions(environment)
                        .Select(JHMSubscriptionMapper.Map)
                        .ToList();
                WriteBinaryToFile(filepath, result);
                return result;
            }
            catch (Exception e)
            {
                log.Error(String.Format("Update Subscription File: {0}", e.Message));
                return null;
            }
        }

        public static void WriteBinaryToFile<T>(string filename, T entity) where T : class
        {
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(stream, entity);
            }
        }

        public static T ReadBinaryFromFile<T>(string filename) where T : class
        {
            T result;
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(filename, FileMode.Open))
            {
                result = formatter.Deserialize(stream) as T;
            }

            return result;
        }
    }
}
