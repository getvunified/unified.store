﻿using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public class DiscountCodeRequirementsRepository
    {
        public DiscountCodeRequirements GetAllDiscountCodeRequirements(DiscountCode discountCode)
        {
            return FluentNHibernateHelper.CurrentSession.QueryOver<DiscountCodeRequirements>()
                                         .Where(x => x.DiscountCodeId == discountCode.Id).As<DiscountCodeRequirements>();
        }

    }
}
