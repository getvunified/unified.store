﻿using System;
using System.Collections.Generic;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IDiscountCodeRepository : IRepository<DiscountCode>
    {
        IList<DiscountCode> GetAllDiscountCodes();
        bool ValidateSingleUseDiscountCodes(DiscountCode discountCode, string suffix);

        /*bool ValidateDiscountCode(DiscountCode discountCode);*/

        DiscountCode GetDiscountCodeByName(string codeName);
        List<DiscountCodeItems> GetAllAssociatedProducts(DiscountCode discountCode);
        IList<DiscountCode> GetAllAutoApplyCodes();
        void IncreaseUseCount(Guid discountCodeId);
    }
}
