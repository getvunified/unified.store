﻿#region

using Jhm.Web.Core.Models;

#endregion

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public interface IDiscountCodeRequirementsRepository
    {
        DiscountCodeRequirements GetAllDiscountCodeRequirements(DiscountCode discountCode);
    }
}