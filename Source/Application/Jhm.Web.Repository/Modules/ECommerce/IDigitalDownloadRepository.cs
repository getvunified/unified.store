﻿using System.Collections.Generic;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IDigitalDownloadRepository : IRepository<DigitalDownload>
    {

        IEnumerable<DigitalDownload> GetAllBy(IDigitalDownloadSearchCriteria searchCriteria);
    }
}