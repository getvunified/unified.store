﻿using Jhm.Common.Repositories;
using Jhm.DonorStudio.Services;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface ICartRepository : IRepository<Cart>
    {
        Cart GetCartByUserName(string userName);
        long SaveTransaction(string dsEnvironment, CheckoutOrderReviewViewModel checkoutData);

        DiscountCode GetDiscountByCodeName(string codeName);
    }
}