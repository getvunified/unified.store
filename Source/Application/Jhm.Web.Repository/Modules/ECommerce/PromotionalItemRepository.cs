﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public class PromotionalItemRepository : RepositoryDecorator<PromotionalItem>, IPromotionalItemRepository
    {
        public List<PromotionalItem> GetAll()
        {
            return FluentNHibernateHelper.CurrentSession.Query<PromotionalItem>().ToList();
        }

        public List<PromotionalItem> GetAllActive()
        {
            var now = DateTime.Now;
            return FluentNHibernateHelper.CurrentSession.Query<PromotionalItem>()
                .Where(x => x.IsActive)
                .Where(x => x.StartDate == null || x.StartDate <= now)
                .Where(x => x.EndDate == null || x.EndDate >= now)
                .ToList();
        }
    }
}
