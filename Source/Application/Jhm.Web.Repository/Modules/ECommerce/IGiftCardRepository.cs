﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.ECommerce;

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public interface IGiftCardRepository
    {
        IEnumerable<GiftCard> GetAvailableGiftCards();

        GiftCard GetGiftCard(Guid giftCardId);
    }
}
