using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate;
using NHibernate.Linq;

namespace Jhm.Web.Repository.Modules.Account
{
    public class DigitalDownloadRepository : RepositoryDecorator<DigitalDownload>, IDigitalDownloadRepository
    {
        public IEnumerable<DigitalDownload> GetAllBy(IDigitalDownloadSearchCriteria searchCriteria)
        {
            var now = DateTime.Now;
            var username = searchCriteria.Username;
            var firstName = searchCriteria.FirstName;
            var lastName = searchCriteria.LastName;
            var email = searchCriteria.Email;
            var status = searchCriteria.SelectedStatus;
            
            var results = FluentNHibernateHelper.CurrentSession.Query<DigitalDownload>()
                .Where(x => username == null    || username == String.Empty     || x.User.Username == username)
                .Where(x => email == null       || email == String.Empty        || x.User.Email == email)
                .Where(x => firstName == null   || firstName == String.Empty    || x.User.Profile.FirstName == firstName)
                .Where(x => lastName == null    || lastName == String.Empty     || x.User.Profile.LastName == lastName)
                .Where(x => status == null      || status == String.Empty       ||
                    (status == "Active" && x.ExpirationDate > now && x.DownloadAttemptsRemaining > 0) ||
                    (status == "Inactive" && !(x.ExpirationDate > now && x.DownloadAttemptsRemaining > 0))
                )
                .OrderByDescending(x => x.CreatedDate)
                .ToList();
            
            return results;
        }
    }
}