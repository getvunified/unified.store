using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Repository.Modules.Account
{
    public partial class SoHDonationRepository : IDonationRepository
    {
        private readonly List<Donation> SoHUSDonations = new List<Donation>();

        public SoHDonationRepository()
        {
            SoHUSDonations = GetSoHUSDonations();
        }

        private const string USWarehouse = "10";
      

#region Unused Parts of Implimentation
        
        // Required for the IDonationRepository interface but not used

        public IEnumerable<Donation> GetActiveCanadaDonationOpportunities()
        {
            return null;
            //throw new NotImplementedException();
        }

        public IEnumerable<Donation> GetActiveUKDonationOpportunities()
        {
            return null;
        }

        public IDonation GetCanadaDonation(string donation)
        {
            return null;
        }

        public IDonation GetUKDonation(string donation)
        {
            return null;
        }

#endregion

  #region Methods

        public Donation Get(object id)
        {
            return SoHUSDonations.Find(x => x.Id == id.ToString().TryConvertTo<Guid>());
        }

        public void Save(Donation entity)
        {

        }

        public void Delete(Donation entity)
        {

        }

        public IEnumerable<Donation> GetActiveDonationOpportunities()
        {
            //NOTE:  Returning refreshed list each time
            //return allDonations;
            return GetSoHUSDonations();
        }

        private List<Donation> GetSoHUSDonations()
        {
            return new List<Donation>
                       {   
                           AnyAmountUSSanctuaryOfHope(),
                           DonateLibraryBook(),
                           DonateTeacherOrAdminDesk(),
                           DonateStudentDesk(),
                           DonateBench(),
                           DonatePicnicTable(),                           
                           DonateTree(),
                           DonateSchoolOffice(), 
                           DonateDormBedroom(),
                           DonateElementaryClassroom(), 
                           DonateSecondaryClassroom(), 
                           DonateConcessionStandFootball(),  
                           DonateSchoolCornerstone(),
                           DonateSpecialityClassroomElementary(),
                           DonateSpecialityClassroomSecondary(),
                           DonateGuardHouse(),
                           DonateElementaryPlayground()                         
                       };
        }

        public IDonation GetDonation(string code)
        {
            //NOTE:  Returning refreshed list each time
            //return allDonations.Find(x => x.DonationCode == code);
            return GetSoHUSDonations().Find(x => x.DonationCode.Equals(code, StringComparison.InvariantCultureIgnoreCase));
        }

        public IDonation GetUSDonation(string donationCode)
        {
            return GetSoHUSDonations().Find(x => x.DonationCode.Equals(donationCode, StringComparison.InvariantCultureIgnoreCase));
        }

        public int GetCurrentQtyAvailable(Product item)
        {
            return 100;
        }

        #endregion


  #region All Active Donation Opportunites (Setup Area)

        public static readonly Guid LibraryBookOptionId = new Guid("7316d330-dfeb-4329-b989-9ab47dac9571");         
        public static readonly Guid TeacherAdminDeskOptionId = new Guid("2c5d8c90-5acd-475c-af67-ed26a09eee05");
        public static readonly Guid StudentDeskOptionId = new Guid("fae29652-323f-4f62-b639-d7c3adad5487");
        public static readonly Guid BenchOptionId = new Guid("317bab32-d5f9-4b1e-ac42-595d1bb32e84");
        public static readonly Guid PicnicTableOptionId = new Guid("ff603dcf-8cf8-415e-b679-10a4fddcb880");
        public static readonly Guid TreeOptionId = new Guid("a9d4e696-7772-43a8-b0c0-c91f23932849");
        public static readonly Guid SchoolOfficeOptionId = new Guid("c477fe2a-9c43-4fb6-b514-e72efba9a323");
        public static readonly Guid DormBedroomOptionId = new Guid("085a949b-b976-4702-818f-275b4b38a47f");
        public static readonly Guid ElementaryClassroomOptionId = new Guid("30f920f3-16f4-4f62-829e-04e457b7f019");
        public static readonly Guid SecondaryClassroomOptionId = new Guid("4d4189de-71d2-4269-b92c-0c1e0633995a");
        public static readonly Guid ConcessionStandFootballOptionId = new Guid("dfe83658-84c0-41e7-bab9-5ea34827910d");
        public static readonly Guid ConerstoneOfElementarySecondaryOptionId = new Guid("93cb287d-30a9-4300-bb60-acd9ef9eaa25");
        public static readonly Guid ElemenatrySpecialityClassroomOptionId = new Guid("19f820d8-0360-48a3-9e0f-60ea59bce328");
        public static readonly Guid SecondarySpecialityClassroomOptionId = new Guid("5c471bb6-6bfa-41b7-8092-6c9c065a3476");
        public static readonly Guid GuardHouseOptionId = new Guid("80efdefe-e76e-4561-8ed4-52e924f45c50");
        public static readonly Guid ElementaryPlaygroundOptionId = new Guid("fc2a2326-534d-46bb-a929-a4c511489aeb");

        public static readonly Guid LibraryBookCertId = new Guid("15E95AA8-09FA-4C0F-A8F0-4C308F818C93");
        public static readonly Guid TeacherAdminDeskCertId = new Guid("2BDB5C8D-3511-4371-A4D5-6140F1F06488");
        public static readonly Guid StudentDeskCertId = new Guid("6BCE075D-5D26-4664-B6DA-29B603014DCA");
        public static readonly Guid BenchCertId = new Guid("B9CD968A-9C82-4F4C-A38A-BFFF4C84A171");
        public static readonly Guid PicnicTableCertId = new Guid("C1FE776B-AB87-47AB-8B02-BBB0A5961EB6");
        public static readonly Guid TreeCertId = new Guid("779B518D-B199-42D8-953A-6F7763BF1758");
        public static readonly Guid SchoolOfficeCertId = new Guid("2FE44159-39AA-4D61-AE7B-D293F046857B");
        public static readonly Guid DormBedroomCertId = new Guid("A0E2F7D1-7CA9-41A0-94DF-2760E1181585");
        public static readonly Guid ElementaryClassroomCertId = new Guid("ADB0C5FC-6DA2-487C-AFF4-4F84D150265F");
        public static readonly Guid SecondaryClassroomCertId = new Guid("4240B91B-9DEE-4105-9203-00A6DEB7F21B");
        public static readonly Guid ConcessionStandFootballCertId = new Guid("F4FC301F-ABA7-4ED1-BFA0-1112E12C3EAC");
        public static readonly Guid ConerstoneOfElementarySecondaryCertId = new Guid("10D4D4F7-4591-4E63-86C9-7B7619E150B0");
        public static readonly Guid ElemenatrySpecialityClassroomCertId = new Guid("967C8B00-B3C8-43E3-9B2A-27BA63D22C7C");
        public static readonly Guid SecondarySpecialityClassroomCertId = new Guid("5A37D587-889B-41E4-A22F-2B59AC8AA0B4");
        public static readonly Guid GuardHouseCertId = new Guid("D50CBF06-E707-4A37-AB2C-33CCBF87129C");
        public static readonly Guid ElementaryPlaygroundCertId = new Guid("3E5AC611-8B51-4BAD-BDFC-247E26FD159E");


        private Donation AnyAmountUSSanctuaryOfHope()
        {
            // Copied from USDonation

            #region Body

            const string body = @"<p>Sanctuary of Hope is an oasis of hope for children that are seeking spiritual guidance and opportunity. It is a comprehensive, long-term concept to save the youth of our city and America.</p><p>
When Pastor John Hagee was a teenager, he worked many different jobs. One that has left an indelible impression upon him was the time he spent working in an orphanage. &quotI was privileged in the time that I was there to see life through the eyes of an orphan.&quot He recalls the young boys who would chat amongst themselves saying, &quotThis week, my mom�s going to come get me. My dad is coming finally�and he�s going to be driving a big, new car.&quot The day would come, and no one came. Not even for a visit. Week after week this happened and then it dawned on them. &quotNo one�s coming for me; I�m by myself. I�m completely alone.&quot </p><p>
This is where the idea for Sanctuary of Hope was born. Pastor says, &quotI know that it is an ocean of need. But it is wrong to see a need and not try to meet that need. We can help some of them. And because we can, we should. And because this is the Commission of God, we are.&quot </p><p>
First, saving children begins with saving their physical life. America is losing 1000 babies a day to abortion, hindering our future and hope as nation. Secondly, children need a loving home where they can be nurtured in a Christ-centered environment. Finally, children should be educated in a righteous environment, free from the secular indoctrination that is prevalent in the school systems today. </p><p>
With these three foundational statements, donated funds for this outreach will go towards the Sanctuary of Hope with three divisions: </p>
<p>
<b>Division One:</b> An unwed mother's facility where young women who are pregnant and do not want to terminate their pregnancy would be invited to stay until the baby is born. The newborn baby could be adopted by a godly family of proven character or raised in the Sanctuary of Hope orphanage. </p>
<p>
<b>Division Two:</b> The building and development of a state-of-the-art orphanage in the San Antonio area.  </p>
<p>
<b>Division Three: </b>The building of a dormitory for a world-class boarding school where students can attend one of the finest Christian schools in the country, Cornerstone Christian Schools. </p>
<p>
This cannot be accomplished without you. Love is not what you say. Love is what you do. </p>
<p>
Won�t you please join us at John Hagee Ministries in this endeavor to meet the need of these priceless treasures, our children? As Pastor Hagee says, &quotWhen I get to heaven, God isn�t going to pat me on the back for erecting buildings to help children. He is going to reward those who gave and made it possible.&quot </p>";

            #endregion

            var description = "Sanctuary of Hope Donation";
            var rDonation = new Donation("SanctuaryOfHope", "Sanctuary of Hope", description, body, listViewImageUrl: "~/Content/images/Sanctuary_Hope-sm-V2.jpg", detailViewImageUrl: "~/Content/images/Sanctuary_Hope-banner.jpg", formViewImageUrl: "~/Content/images/Sanctuary_Hope-banner.jpg");

            var oneTimeOption = new AnyAmountDonationOption(new Guid("e320dc47-49a3-4693-944b-722e2d141bc7")); 
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift in the amount below.");

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateLibraryBook()
        {
            #region Body

            const string body = @"
                <p>Donate Library Book</p>
            ";

            #endregion

            var item = new Product("SO25");

            var qtyAvailable = GetCurrentQtyAvailable(item);

            // Setup Included StockItems
            item.SetTitle("SOH -Library Book Donation Certificate");
            var stockItem = new StockItem(LibraryBookCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Library Book";
            var rDonation = new Donation("SOHLibraryBook", "Sanctuary of Hope Library Book", description, body, listViewImageUrl: "~/Content/images/SOH/LibraryBook.jpg", detailViewImageUrl: "~/Content/images/SOH/LibraryBookBanner.jpg", formViewImageUrl: "~/Content/images/SOH/LibraryBookBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(LibraryBookOptionId, 25m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateTeacherOrAdminDesk()
        {
            #region Body

            const string body = @"
                <p>Donate Teacher Or Admin Desk</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH250A");

            var qtyAvailable = GetCurrentQtyAvailable(item);

            item.SetTitle("SOH - Teacher or Admin Desk Donation Certificate");
            var stockItem = new StockItem(TeacherAdminDeskCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Teacher or Admin Desk";
            var rDonation = new Donation("SOHLibraryBook", "Sanctuary of Hope Teacher or Admin Desk", description, body, listViewImageUrl: "~/Content/images/SOH/TeacherorAdminDesk.jpg", detailViewImageUrl: "~/Content/images/SOH/TeacherorAdminDeskkBanner.jpg", formViewImageUrl: "~/Content/images/SOH/TeacherorAdminDeskBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(TeacherAdminDeskOptionId, 250m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateStudentDesk()
        {
            #region Body

            const string body = @"
                <p>Student Desk</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH250B");

            var qtyAvailable = GetCurrentQtyAvailable(item);

            item.SetTitle("SOH - Student Desk Donation Certificate");
            var stockItem = new StockItem(StudentDeskCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Student Desk";
            var rDonation = new Donation("SOHLibraryBook", "Sanctuary of Hope Teacher or Admin Desk", description, body, listViewImageUrl: "~/Content/images/SOH/TeacherorAdminDesk.jpg", detailViewImageUrl: "~/Content/images/SOH/TeacherorAdminDeskkBanner.jpg", formViewImageUrl: "~/Content/images/SOH/TeacherorAdminDeskBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(StudentDeskOptionId, 250m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateBench()
        {
            #region Body

            const string body = @"
                <p>Donate Bench</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH500");
            var qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Bench Donation Certificate");
            var stockItem = new StockItem(BenchCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Bench";
            var rDonation = new Donation("SOHBench", "Sanctuary of Hope Bench", description, body, listViewImageUrl: "~/Content/images/SOH/Bench.jpg", detailViewImageUrl: "~/Content/images/SOH/benchBanner.jpg", formViewImageUrl: "~/Content/images/SOH/BenchBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(BenchOptionId, 500m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonatePicnicTable()
        {
            #region Body

            const string body = @"
                <p>Donate Picnic Table</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH750");
            var qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Picnic Table Donation Certificate");
            var stockItem = new StockItem(PicnicTableCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Picnic Table";
            var rDonation = new Donation("SOHPicnicTable", "Sanctuary of Hope Picnic Table", description, body, listViewImageUrl: "~/Content/images/SOH/PicnicTable.jpg", detailViewImageUrl: "~/Content/images/SOH/PicnicTableBanner.jpg", formViewImageUrl: "~/Content/images/SOH/PicnicTableBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(PicnicTableOptionId, 750m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateTree()
        {
            #region Body

            const string body = @"
                <p>Donate Tree</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH1000");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Tree Donation Certificate");
            var stockItem = new StockItem(TreeCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Tree";
            var rDonation = new Donation("SOHTree", "Sanctuary of Hope Tree", description, body, listViewImageUrl: "~/Content/images/SOH/Tree.jpg", detailViewImageUrl: "~/Content/images/SOH/TreeBanner.jpg", formViewImageUrl: "~/Content/images/SOH/TreeBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(TreeOptionId, 1000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateSchoolOffice()
        {
            #region Body

            const string body = @"
                <p>Donate School Office</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH3500");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("School Office Donation Certificate");
            var stockItem = new StockItem(SchoolOfficeCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope School Office";
            var rDonation = new Donation("SOHSchoolOffice", "Sanctuary of Hope School Office", description, body, listViewImageUrl: "~/Content/images/SOH/SchoolOffice.jpg", detailViewImageUrl: "~/Content/images/SOH/SchoolOfficeBanner.jpg", formViewImageUrl: "~/Content/images/SOH/SchoolOfficeBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(SchoolOfficeOptionId, 3500m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateDormBedroom()
        {
            #region Body

            const string body = @"
                <p>Donate DormBedroom</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH7500");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Dorm Bedroom Donation Certificate");
            var stockItem = new StockItem(DormBedroomCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Dorm Bedroom";
            var rDonation = new Donation("SOHDormBedroom", "Sanctuary of Hope Dorm Bedroom", description, body, listViewImageUrl: "~/Content/images/SOH/DormBedroom.jpg", detailViewImageUrl: "~/Content/images/SOH/DormBedroomBanner.jpg", formViewImageUrl: "~/Content/images/SOH/DormBedroomBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(DormBedroomOptionId, 7500m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateElementaryClassroom()
        {
            #region Body

            const string body = @"
                <p>Donate Elementary Classroom</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH10KA");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Elementary Classroom Donation Certificate");
            var stockItem = new StockItem(ElementaryClassroomCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Elementary Classroom";
            var rDonation = new Donation("SOHElementaryClassroom", "Sanctuary of Hope Elementary Classroom", description, body, listViewImageUrl: "~/Content/images/SOH/ElementaryClassroom.jpg", detailViewImageUrl: "~/Content/images/SOH/ElementaryClassroomBanner.jpg", formViewImageUrl: "~/Content/images/SOH/ElementaryClassroomBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(ElementaryClassroomOptionId, 10000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateSecondaryClassroom()
        {
            #region Body

            const string body = @"
                <p>Donate Secondary Classroom</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH10KB");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Secondary Classroom Donation Certificate");
            var stockItem = new StockItem(SecondaryClassroomCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Secondary Classroom";
            var rDonation = new Donation("SOHSecondaryClassroom", "Sanctuary of Hope Secondary Classroom", description, body, listViewImageUrl: "~/Content/images/SOH/SecondaryClassroom.jpg", detailViewImageUrl: "~/Content/images/SOH/SecondaryClassroomBanner.jpg", formViewImageUrl: "~/Content/images/SOH/SecondaryClassroomBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(SecondaryClassroomOptionId, 10000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateConcessionStandFootball()
        {
            #region Body

            const string body = @"
                <p>Donate Football Concession Stand</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH20K");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Football Concession Stand Donation Certificate");
            var stockItem = new StockItem(ConcessionStandFootballCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Football Concession Stand";
            var rDonation = new Donation("SOHConcessionStandFootball", "Sanctuary of Hope Football Concession Stand", description, body, listViewImageUrl: "~/Content/images/SOH/ConcessionStandFootball.jpg", detailViewImageUrl: "~/Content/images/SOH/ConcessionStandFootballBanner.jpg", formViewImageUrl: "~/Content/images/SOH/ConcessionStandFootballBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(ConcessionStandFootballOptionId, 20000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateSchoolCornerstone()
        {
            #region Body

            const string body = @"
                <p>Donate School Cornerstone</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH25KA");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("School Cornerstone Donation Certificate");
            var stockItem = new StockItem(ConerstoneOfElementarySecondaryCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope School Cornerstone";
            var rDonation = new Donation("SOHSchoolCornerstone", "Sanctuary of Hope School Cornerstone", description, body, listViewImageUrl: "~/Content/images/SOH/SchoolCornerstone.jpg", detailViewImageUrl: "~/Content/images/SOH/SchoolCornerstoneBanner.jpg", formViewImageUrl: "~/Content/images/SOH/SchoolCornerstoneBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(ConerstoneOfElementarySecondaryOptionId, 25000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateSpecialityClassroomElementary()
        {
            #region Body

            const string body = @"
                <p>Donate Speciality Classroom -Elementary</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH25KB");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Library Book Speciality Classroom - Elementary");
            var stockItem = new StockItem(ElemenatrySpecialityClassroomCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Speciality Classroom -Elementary";
            var rDonation = new Donation("SOHSpecialityClassroomElementary", "Sanctuary of Hope Speciality Classroom-Elementary", description, body, listViewImageUrl: "~/Content/images/SOH/SpecialityClassroomElementary.jpg", detailViewImageUrl: "~/Content/images/SOH/SpecialityClassroomElementaryBanner.jpg", formViewImageUrl: "~/Content/images/SOH/SpecialityClassroomElementaryBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(ElemenatrySpecialityClassroomOptionId, 25000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateSpecialityClassroomSecondary()
        {
            #region Body

            const string body = @"
                <p>Donate Speciality Classroom - Secondary</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH25KC");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Speciality Classroom - Secondary  Donation Certificate");
            var stockItem = new StockItem(SecondarySpecialityClassroomCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Speciality Classroom-Secondary";
            var rDonation = new Donation("SOHSpecialityClassroomSecondary", "Sanctuary of Hope Speciality Classroom - Secondary", description, body, listViewImageUrl: "~/Content/images/SOH/SpecialityClassroomSecondary.jpg", detailViewImageUrl: "~/Content/images/SOH/SpecialityClassroomSecondaryBanner.jpg", formViewImageUrl: "~/Content/images/SOH/SpecialityClassroomSecondaryBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(SecondarySpecialityClassroomOptionId, 25000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateGuardHouse()
        {
            #region Body

            const string body = @"
                <p>Donate GuardHouse</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("SOH50K");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Library Book Guard House");
            var stockItem = new StockItem(GuardHouseCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Guard House";
            var rDonation = new Donation("SOHGuardHouse", "Sanctuary of Hope Guard House", description, body, listViewImageUrl: "~/Content/images/SOH/GuardHouse.jpg", detailViewImageUrl: "~/Content/images/SOH/GuardHouseBanner.jpg", formViewImageUrl: "~/Content/images/SOH/GuardHouseBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(GuardHouseOptionId, 50000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

        private Donation DonateElementaryPlayground()
        {
            #region Body

            const string body = @"
                <p>Donate Elementary Playground</p>
            ";

            #endregion

            // Setup Included StockItems
            var item = new Product("Elementary Playground");
            int qtyAvailable = GetCurrentQtyAvailable(item);
            item.SetTitle("Elementary Playground Donation Certificate");
            var stockItem = new StockItem(ElementaryPlaygroundCertId, MediaType.Misc, qtyAvailable, false, 0.00m, 0.00m, true, USWarehouse).AddProduct(item);

            var description = "Donate Sanctuary of Hope Elementary Playground";
            var rDonation = new Donation("SOHElementaryPlayground", "Sanctuary of Hope Elementary Playground", description, body, listViewImageUrl: "~/Content/images/SOH/ElementaryPlayground.jpg", detailViewImageUrl: "~/Content/images/SOH/ElementaryPlaygroundBanner.jpg", formViewImageUrl: "~/Content/images/SOH/ElementaryPlaygroundBanner.jpg");

            var oneTimeOption = new FixedAmountDonationOption(ElementaryPlaygroundOptionId, 75000m);
            oneTimeOption.SetTitle("One-Time Deduction");
            oneTimeOption.SetDescription("The Lord has directed me to give a special one time gift of [C:{0}].".FormatWith(oneTimeOption.Amount));
            oneTimeOption.AddIncludedStockItem(stockItem);

            rDonation.SetProjectCode("HOPE");

            rDonation.AddDonationOption(oneTimeOption);

            return rDonation;
        }

  #endregion

    }
}