using System;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.DonorStudio.Services.Services;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;
using NHibernate.Criterion;

namespace Jhm.Web.Repository.Modules.Account
{
    public class CartRepository : RepositoryDecorator<Cart>, ICartRepository
    {

        private DSServiceCollection dsServiceCollection;

        public CartRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }


        public Cart GetCartByUserName(string userName)
        {
            throw new NotImplementedException();
        }

        public long SaveTransaction(string dsEnvironment, CheckoutOrderReviewViewModel checkoutData)
        {
            var dsTransaction = DsTransactionMapper.Map(checkoutData);
            return dsServiceCollection.DsAccountTransactionService.CreateTransaction(dsEnvironment, dsTransaction, true);  //Setting true to use auto batch
        }

        public DiscountCode GetDiscountByCodeName(string codeName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(DiscountCode), "dc")
                                                     .Add(Restrictions.Eq("CodeName", codeName))
                                                     .Add(Restrictions.Le("StartDate", DateTime.Now))
                                                     .Add(Restrictions.Ge("EndDate", DateTime.Now))
                                                     .UniqueResult<DiscountCode>();
        }
    }
}