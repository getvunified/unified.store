using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using Jhm.Common.Framework.Extensions;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Common;
using Jhm.Common.Utilities;
using System.Text.RegularExpressions;
using getv.donorstudio.core.Global;
using log4net;

namespace Jhm.Web.Repository.Modules.Account
{
    public class CatalogRepository : ICatalogRepository
    {
        private DSServiceCollection dsServiceCollection;
        private static readonly ILog log = LogManager.GetLogger(typeof(CatalogRepository));

        public CatalogRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }


        public static string ReadStringFromFile(string filename)
        {
            var result = String.Empty;
            if (!File.Exists(filename))
            {
                return result;
            }
            using (var filestream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (var streamreader = new StreamReader(filestream))
                {
                    result = streamreader.ReadToEnd();
                }
            }
            return result;
        }


        public static void WriteStringToFile(string filename, string contents, bool overwrite)
        {
            if (!overwrite)
            {
                if (File.Exists(filename))
                {
                    // We don't want to overwrite the file, so just return
                    return;
                }
            }
            using (var filestream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                using (var streamwriter = new StreamWriter(filestream))
                {
                    streamwriter.Write(contents);
                }
            }
        }

        public static void WriteBinaryToFile<T>(string filename, T entity) where T : class
        {
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(stream, entity);
            }
        }

        public static T ReadBinaryFromFile<T>(string filename) where T : class
        {
            T result;
            var formatter = new BinaryFormatter();
            using (var stream = File.Open(filename, FileMode.Open))
            {
                result = formatter.Deserialize(stream) as T;
            }

            return result;
        }

        public IEnumerable<IProduct> GetAllProducts(string environment, bool force)
        {
            var filepath = HttpContext.Current.Server.MapPath(@"\") + environment + ".bin";
            IEnumerable<IProduct> result;
            if (!File.Exists(filepath) || force)
            {
                return UpdateProductJson(environment);
            }
            try
            {
                result = ReadBinaryFromFile<List<Product>>(filepath);
                result = result.OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate);
                return result;
            }
            catch (Exception e)
            {
                throw;
                //log.Error(String.Format("[TryWithTransaction]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}", e.Message, e.StackTrace, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url));
                log.Error(String.Format("[TryWithTransaction]\nException: {0}\n StackTrace: {1}\n HostName: {2}\n Url: {3}", e.Message, e.StackTrace, String.Empty, String.Empty));
                return new List<IProduct>();
            }


        }

        public IEnumerable<IProduct> UpdateProductJson(string environment)
        {
            var filepath = HttpContext.Current.Server.MapPath(@"\") + environment + ".bin";
            //var serializer = new JavaScriptSerializer();
            //var result = dsServiceCollection.DsInventoryService.GetAllProducts(environment).Select(DsProductMapper.Map).Cast<IProduct>().ToList().OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate);

            var products = dsServiceCollection.DsInventoryService.GetAllProducts(environment);
            var result   = products.Select(DsProductMapper.Map).OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).ToList();

            //var json = serializer.Serialize(result);
            //WriteStringToFile(filepath, json, true);

            WriteBinaryToFile<List<Product>>(filepath, result);
            return result;
        }



        public IEnumerable<IProduct> GetFeaturedItems(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            var featuredProducts = new List<IProduct>
            {
                /*** To Keep as of 2/6/2012 ***/
                new Product("S1135"), 
                new Product("B178"), 
                new Product("KT205"), 
                new Product("SM1132E"),
                new Product("KT192"),
                new Product("S1138"), 
                new Product("KT516"), 
                new Product("K263"), 
                new Product("KT200"),
                new Product("KT207"),
                new Product("KT208"),
                new Product("K458"),
                new Product("B131"),
                new Product("B117"),
                new Product("K515"),
                new Product("K510"),
                new Product("M64"),
                new Product("B43B"), 
                                          
                /***** Added as of 2/6/2012 *****/
                new Product("S0822"),
                new Product("1107E"),
                new Product("1147E"),
                new Product("1108E"),
                new Product("B02")
            };


            var allProducts = GetProductsFromMemory(environment);


            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = (from product in allProducts
                                from featuredProduct in featuredProducts
                                where featuredProduct.Code == product.Code
                                select product).ToList();

            totalRows = filteredList.Count();

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        private static IEnumerable<IProduct> GetProductsFromMemory(string environment)
        {
            var cache  = MemoryCache.Default;
            var result = cache[environment] as IEnumerable<IProduct>;

            //var result = (IEnumerable<IProduct>)HttpContext.Current.Application[environment];
            return result;
        }

        public IEnumerable<IProduct> GetLatestReleasedProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            var allProducts = GetProductsFromMemory(environment);

            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = allProducts.Where(x => x.StockItems.Any(p => p.ReleaseDate < 1.Days().InTheFuture()))
                .OrderByDescending(p => p.StockItems.FirstOrDefault().ReleaseDate).Take(15);

            totalRows = filteredList.Count();


            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);

        }


        public IEnumerable<IProduct> GetFeaturedOnTelevisionProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            var allProducts = GetProductsFromMemory(environment);

            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = allProducts.Where(x => x.IsFeaturedOnTelevision);

            totalRows = filteredList.Count();

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetFeaturedOnGetvProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            var allProducts = GetProductsFromMemory(environment);

            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = allProducts.Where(x => x.IsFeaturedOnGetv);

            totalRows = filteredList.Count();

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }


        public IEnumerable<IProduct> GetProductsByAuthor(string environment, string author, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = author.Equals("All") ? allProducts : allProducts.Where(x => x.Author.IsNot().NullOrEmpty() && x.Author.Equals(author));

            totalResults = filteredList.Count();

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetProductsByAuthorOrderedBy(string environment, string author, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = author.Equals("All") ? allProducts : allProducts.Where(x => x.Author.IsNot().NullOrEmpty() && x.Author.Equals(author));

            totalResults = filteredList.Count();

            //SORTING
            filteredList = orderDirection.ToLower() == "desc" ? filteredList = filteredList.OrderByDescending(x => x.Title) : filteredList = filteredList.OrderBy(x => x.Title);

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }
        public IEnumerable<IProduct> GetProductsByPublisher(string environment, string publisher, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();

            var filteredList = publisher.Equals("All") ? allProducts : allProducts.Where((x => x.Publisher.IsNot().NullOrEmpty() && x.Publisher.Equals(publisher)));

            totalResults = filteredList.Count();

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public void UpdateProductNoteFileLength(string environment, string productCode, decimal fileLength)
        {
            dsServiceCollection.DsInventoryService.UpdateProductNoteFileLength(environment, productCode, fileLength);
        }


        public IEnumerable<IProduct> GetProductsByCategory(string environment, string categories, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = categories.Equals("All") ? allProducts : allProducts.Where(x => x.Categories.Count() > 0 && x.Categories.Any(category => category.Value.Equals(categories)));
            totalResults = filteredList.Count();
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetProductsByCategoryOrderedBy(string environment, string categories, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = categories.Equals("All") ? allProducts : allProducts.Where(x => x.Categories.Count() > 0 && x.Categories.Any(category => category.Value.Equals(categories)));
            totalResults = filteredList.Count();

            //SORTING
            filteredList = orderDirection.ToLower() == "desc" ? filteredList = filteredList.OrderByDescending(x => x.Title) : filteredList = filteredList.OrderBy(x => x.Title);

            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }
        public IEnumerable<IProduct> GetProductsBySubject(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = subjects.Equals("All") ? allProducts : allProducts.Where(x => x.Subjects.Count() > 0 && x.Subjects.Any(subject => subject.Equals(subjects)));
            totalResults = filteredList.Count();
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetProductsBySubjectOrderedBy(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = subjects.Equals("All") ? allProducts : allProducts.Where(x => x.Subjects.Count() > 0 && x.Subjects.Any(subject => subject.Equals(subjects)));
            totalResults = filteredList.Count();

            //SORTING
            filteredList = orderDirection.ToLower() == "desc" ? filteredList = filteredList.OrderByDescending(x => x.Title) : filteredList = filteredList.OrderBy(x => x.Title);
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetProductsOnSale(string environment, string onSale, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = onSale.Equals("All") ? allProducts : allProducts.Where(x => x.StockItems.Any(y => y.IsOnSale()));
            totalResults = filteredList.Count();
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IEnumerable<IProduct> GetProductsByMediaTypes(string environment, string mediaTypes, PagingInfo pagingInfo, ref int totalResults)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = mediaTypes.Equals("All") ? allProducts : allProducts.Where(x => x.StockItems.Any(p => p.MediaType.CodeSuffix.Equals(mediaTypes)));
            totalResults = filteredList.Count();
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);
        }

        public IProduct GetProductByProductCode(string environment, string productCode)
        {
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return null;
            var result = allProducts.Where(x => x.StockItems.Any(p => p.ProductCode == productCode)).FirstOrDefault();
            return result;
        }

        private StringIgnoreCaseComparer<String> ignoreCaseComparer = new StringIgnoreCaseComparer<string>();
        public IEnumerable<IProduct> FindProducts(string environment, string searchString, PagingInfo pagingInfo, ref int totalRows)
        {
            if (searchString.IsNullOrEmpty())
            {
                return new List<IProduct>();
            }
            const string splitWordsPattern = @"\W";
            var noSpecialCharsRegex = new Regex("(?:[^a-z0-9 ]|(?<=['\"]))", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            searchString = noSpecialCharsRegex.Replace(searchString, String.Empty);
            var noHTMLTags = new Regex(@"<[^>]*>", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            var searchStrings = Regex.Split(searchString, splitWordsPattern);
            var allProducts = GetProductsFromMemory(environment);
            if (allProducts == null)
                return new List<IProduct>();
            var filteredList = allProducts.Where(x =>
                {
                    var contentArray = new[]
                                           {
                                               x.Code, 
                                               noSpecialCharsRegex.Replace(x.Title ?? String.Empty, String.Empty), 
                                               //noSpecialCharsRegex.Replace(noHTMLTags.Replace(x.Description ?? String.Empty,String.Empty), String.Empty),
                                               noSpecialCharsRegex.Replace(x.Author ?? String.Empty, String.Empty), 
                                               noSpecialCharsRegex.Replace(x.Publisher ?? String.Empty, String.Empty)
                                           };  //Code is root.. StockItems Prod Code?
                    foreach (var content in contentArray)
                    {
                        if (content.IsNullOrEmpty()) continue;
                        var contentWords = Regex.Split(content, splitWordsPattern);
                        if (contentWords.ContainsAllOf(searchStrings, ignoreCaseComparer))
                            return true;
                    }
                    // StockItems lookup
                    var stockItemsMatch = x.GetAvailableStockItems().Any(s =>
                    {
                        // Note: not sure if it should look in Title and Description of stockItems
                        var stockItemFieldsArray = new[] { s.Code/*, s.Title, s.Description*/ };
                        foreach (var field in stockItemFieldsArray)
                        {
                            if (field.IsNullOrEmpty()) continue;
                            var fieldWords = Regex.Split(field, splitWordsPattern);
                            if (fieldWords.ContainsAllOf(searchStrings, ignoreCaseComparer))
                                return true;
                        }
                        return false;
                    });
                    return stockItemsMatch;
                });

            totalRows = filteredList.Count();
            return filteredList.Skip((pagingInfo.CurrentPage - 1) * pagingInfo.PageSize).Take(pagingInfo.PageSize);

        }
        public IEnumerable<IProduct> GetProducts(string categories, string subjects, string mediaType, string author, PagingInfo pagingInfo, ref int totalRows)
        {
            return new List<Product>();
        }
        public IEnumerable<IProduct> GetProducts(string authors, PagingInfo pagingInfo, ref int totalRows)
        {
            return new List<Product>();
        }
        public IProduct GetProduct(string environment, string sku)
        {
            var allProducts = GetProductsFromMemory(environment);
            var product = allProducts == null ? new Product() : allProducts.FirstOrDefault(x => x.Code.Equals(sku));

            return product;
            //return DsProductMapper.Map(dsServiceCollection.DsInventoryService.GetProduct(environment, sku));
        }


        public IProduct GetProductFromItemCode(string environment, string itemProductCode, bool includeOtherStockItems)
        {
            var allProducts = GetProductsFromMemory(environment);
            var product = allProducts == null ? new Product() : allProducts.FirstOrDefault(x => x.Code.Equals(itemProductCode) || x.StockItems.Any(s => s.Code == itemProductCode)) ?? new Product();

            if (includeOtherStockItems) return product;

            var copy = (IProduct)product.Clone();
            // Removing stock items from copy
            copy.SetStockItems(product.StockItems.Where(s => s.Code == itemProductCode));

            return copy;
        }

        public StockItem GetStockItem(string environment, string productCode, MediaType mediaType)
        {
            var allProducts = GetProductsFromMemory(environment);
            var product = allProducts.FirstOrDefault(x => x.Code.Equals(productCode));
            if (product != null)
            {
                if (product.StockItems == null)
                {
                    throw new ApplicationException("There are no StockItems for this product code:" + productCode);
                }
                var stockItem = product.StockItems.FirstOrDefault(x => x.MediaType == mediaType);

                return stockItem;
            }

            return null;
            //return DsProductMapper.Map(dsServiceCollection.DsInventoryService.GetProduct(environment, sku));
        }

        public StockItem GetStockItemByProductId(string environment, Guid productId, MediaType mediaType)
        {
            var allProducts = GetProductsFromMemory(environment);
            var product = allProducts.FirstOrDefault();
            if (product != null)
            {
                if (product.StockItems == null)
                {
                    throw new ApplicationException("There are no StockItems for this product Id:" + productId);
                }
                var stockItem = product.StockItems.FirstOrDefault();

                return stockItem;
            }

            return null;
            //return DsProductMapper.Map(dsServiceCollection.DsInventoryService.GetProduct(environment, sku));
        }

        public Company GetCompany()
        {
            return new Company();
        }


    }
}