﻿#region

using System.Collections.Generic;
using Jhm.Web.Core.Models;

#endregion

namespace Jhm.Web.Repository.Modules.ECommerce
{
    public interface IDiscountItemRepository
    {
        List<DiscountCodeItems> GetItemsRelatedToDiscountCode(DiscountCode discountCode);
    }
}