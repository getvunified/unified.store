﻿using System;
using System.Collections.Generic;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.eCommerce;
using Address = Jhm.em3.Core.Address;
using Phone = Jhm.Web.Core.Phone;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUser(string username, string applicationName, string dsEnvironment);
        User GetUser(string username, string applicationName);
        User GetUser(Guid Id, string applicationName);
        IList<User> GetUsers(string applicationName);
        IList<User> GetUsers(string emailToMatch, string usernameToMatch, string applicationName);
        int GetAllUserCount(string applicationName);
        int GetNumberOfUsersOnline(DateTime compareTime, string applicationName);
        User GetUserByEmail(string email, string applicationName);
        IList<User> GetUsersByEmail(string email, string applicationName);
        IList<Profile> GetProfiles(DateTime userInactiveSinceDate, bool isAnonymous, string applicationName);
        IList<Profile> GetProfiles(string applicationName);
        
        List<Address> GetUserAddresses(string username, string applicationName, string dsEnvironment);
        long AddAddressToUser(long dsAccountNumber, Address address, string dsEnvironment);
        long GetCurrentDsAccount(long dsCurrentAccountNumber, string dsEnvironment);
        Address GetUserAddressById(long shippingAddresId, string dsEnvironment);
        Address GetUserPrimaryAddress(string username, string applicationName, string dsEnvironment);
        Phone GetUserPhone(string username, string applicationName, string dsEnvironment);
        List<DsTransaction> GetUserTransactions(long accountNumber,int howManyMonthsBack, string dsEnviroment);
        DsTransaction GetTransactionByDocumentNumber(long accountNumber, string dsEnviroment, long documentNumber);
        void SaveDSAccount(DsAccount DsAccount, string dsEnvironment);
        bool IsUserASaltPartner(long accountNumber, User user, string dsEnvironment);
        bool AddCreditCardToUser(string username, Core.Models.CreditCard creditCard, string applicationName);

        Core.Models.CreditCard GetSavedCreditCard(string username, string token, string applicationName);
        long DeleteAccountAddress(long dsAccountNumber, Address address, string dsEnvironment);
        long UpdateAccountAddress(long dsAccountNumber, Address address, string dsEnvironment);
        List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(string username, string applicationName, string dsEnvironment);
        long AddSubscription(string environment, User user, UserSubscriptionViewModel userSubscription);

        List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(long accountNumber, string dsEnvironment);
        void UpdateAccountSubscription(string environment, UserSubscriptionViewModel subscription);
        void CancelAccountSubscription(string environment, UserSubscriptionViewModel subscription);

        User GetUserByGlobalId(string globalId, string applicationName);

        User GetUserByGlobalId(string globalId, string applicationName, string dsEnvironment);

        void AddGoGreenCode(string dsEnvironment, long accountNumber);

        void DeleteCreditCard(Guid cardId, Guid userId, string applicationName);
    }
}