﻿using System.Collections.Generic;
using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IRoleRepository : IRepository<Role>
    {
        Role GetRole(string roleName, string applicationName);
        IList<Role> GetAllRoles(string applicationName);
    }
}