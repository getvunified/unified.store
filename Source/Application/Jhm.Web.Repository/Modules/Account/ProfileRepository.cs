using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public class ProfileRepository : RepositoryDecorator<Profile>, IProfileRepository 
    {
        private readonly DSServiceCollection dsServiceCollection;

        public ProfileRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }
        
    }
}