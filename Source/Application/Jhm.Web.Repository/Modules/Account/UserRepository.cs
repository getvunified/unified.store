﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.DonorStudio.Services.Services;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;
using NHibernate.Criterion;
using getv.donorstudio.core.Account;
using getv.donorstudio.core.eCommerce;
using Address = Jhm.em3.Core.Address;
using CreditCard = Jhm.Web.Core.Models.CreditCard;
using Phone = Jhm.Web.Core.Phone;

namespace Jhm.Web.Repository.Modules.Account
{
    public class UserRepository : RepositoryDecorator<User>, IUserRepository
    {
        private DSServiceCollection dsServiceCollection;


        public UserRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }


        public User GetUser(string username, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User), "u")
                       .Add(Restrictions.Eq("Username", username))
                       .Add(Restrictions.Eq("IsApproved", true))

                       //.Add(Restrictions.Eq("ApplicationName", applicationName))
                //.SetFirstResult(51)  //Paging
                //.SetMaxResults(25)
                //.AddOrder(Order.Asc("UserName"))
                       .UniqueResult<User>() ?? new User();
            
            //TODO:  Send in an example user and return user(s) that satisfy: .Add(Example.Create(new User())
        }

        public User GetUser(Guid id, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                       .Add(Restrictions.Eq("Id", id))
                       .Add(Restrictions.Eq("IsApproved", true))
                       //.Add(Restrictions.Eq("ApplicationName", applicationName))
                       .UniqueResult<User>() ?? new User();
        }

        public User GetUserByEmail(string email, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                       .Add(Restrictions.Eq("Email", email))
                       //.Add(Restrictions.Eq("ApplicationName", applicationName))
                       .UniqueResult<User>() ?? new User();
        }

        public IList<User> GetUsersByEmail(string email, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                .Add(Restrictions.Eq("Email", email))
                //.Add(Restrictions.Eq("ApplicationName", applicationName))
                .List<User>();
        }

        public IList<User> GetUsers(string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                //.Add(Restrictions.Eq("ApplicationName", applicationName))
                .List<User>();
        }

        public IList<User> GetUsers(string emailToMatch, string usernameToMatch, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                .Add(Restrictions.Like("Email", string.Format("%{0}%", emailToMatch)))
                .Add(Restrictions.Like("Username", string.Format("%{0}%", usernameToMatch)))
                //.Add(Restrictions.Eq("ApplicationName", applicationName))
                .List<User>();
        }

        public int GetAllUserCount(string applicationName)
        {
            return Convert.ToInt32(FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                                       //.Add(Restrictions.Eq("ApplicationName", applicationName))
                                       .SetProjection(Projections.Count("Id")).UniqueResult());
        }

        public int GetNumberOfUsersOnline(DateTime compareTime, string applicationName)
        {
            return Convert.ToInt32(FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(User))
                                       //.Add(Restrictions.Eq("ApplicationName", applicationName))
                                       .Add(Restrictions.Gt("LastActivityDate", compareTime))
                                       .SetProjection(Projections.Count("Id")).UniqueResult());
        }


        public IList<Profile> GetProfiles(DateTime userInactiveSinceDate, bool isAnonymous, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(Profile))
                //.Add(Restrictions.Eq("ApplicationName", applicationName))
                .Add(Restrictions.Le("LastActivityDate", userInactiveSinceDate))
                .Add(Restrictions.Eq("IsAnonymous", isAnonymous))
                .List<Profile>();
        }

        public IList<Profile> GetProfiles(string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(Profile))
                //.Add(Restrictions.Eq("ApplicationName", applicationName))
                .List<Profile>();
        }

        #region Donor Studio Address Lookups

        public List<Address> GetUserAddresses(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName);
            if (user.IsTransient())
                return new List<Address>();

            var addresses = dsServiceCollection.DsAccountTransactionService.GetAccountAddresses(dsEnvironment,
                                                                                                user.AccountNumber);

            return addresses == null ? new List<Address>() : addresses.Select(JHMAddressMapper.Map).ToList();
        }

        public long AddAddressToUser(long dsAccountNumber, Address address, string dsEnvironment)
        {
            var dsAddress = JHMAddressMapper.Map(address);
            return dsServiceCollection.DsAccountTransactionService.CreateAccountAddress(dsEnvironment, dsAccountNumber,
                                                                                        dsAddress);
        }

        public long GetCurrentDsAccount(long dsCurrentAccountNumber, string dsEnvironment)
        {
            return dsServiceCollection.DsAccountTransactionService.GetCurrentAccountNumber(dsEnvironment,dsCurrentAccountNumber);
        }

        public long DeleteAccountAddress(long dsAccountNumber, Address address, string dsEnvinment)
        {
            var dsAddress = JHMAddressMapper.Map(address);
            return dsServiceCollection.DsAccountTransactionService.DeleteAccountAddress(dsEnvinment, dsAccountNumber, dsAddress);
        }
        public long UpdateAccountAddress(long dsAccountNumber, Address address, string dsEnvinment)
        {
            var dsAddress = JHMAddressMapper.Map(address);
            return dsServiceCollection.DsAccountTransactionService.UpdateAccountAddress(dsEnvinment, dsAccountNumber, dsAddress);
        }

        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName, dsEnvironment);
            return GetActiveAccountSubscriptions(user.AccountNumber, dsEnvironment);
        }

        public long AddSubscription(string environment, User user, UserSubscriptionViewModel userSubscription)
        {
            var dsSubscription = new DsUserSubscription();
            userSubscription.User = user;
            var subscriptionId = dsServiceCollection.DsAccountTransactionService.AddSubscription(environment, user.AccountNumber, JHMSubscriptionMapper.Map(userSubscription, dsSubscription));
            return subscriptionId;
        }

        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(long accountNumber, string dsEnvironment)
        {
            var subscriptions = dsServiceCollection.DsAccountTransactionService.GetActiveAccountSubscriptions(dsEnvironment, accountNumber);
            var result = new List<UserSubscriptionViewModel>();
            foreach (var dsSubscription in subscriptions)
            {
                var user = new User{AccountNumber = dsSubscription.AccountNumber};
                var sub = JHMSubscriptionMapper.Map(dsSubscription, new UserSubscriptionViewModel());
                sub.DeliveryAddress = GetUserAddressById(dsSubscription.DeliveryAddressId, dsEnvironment);
                sub.User = DsAccountMapper.Map(dsServiceCollection.DsAccountTransactionService.GetAccount(dsEnvironment, user.AccountNumber), user);
                result.Add(sub);
            }
            return result;
        }

        public void UpdateAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            var dsSubscription = new DsUserSubscription();
            //dsSubscription = dsServiceCollection.DsAccountTransactionService.GetActiveAccountSubscriptions(environment,subscription.User.AccountNumber).Single(s =>s.SubscriptionId ==subscription.Id);

            //dsSubscription.DeliveryAddressId = subscription.DeliveryAddress.DSAddressId;
            //dsServiceCollection.DsAccountTransactionService.UpdateAccountSubscription(environment, dsSubscription);

            dsServiceCollection.DsAccountTransactionService.UpdateAccountSubscription(environment, JHMSubscriptionMapper.Map(subscription, dsSubscription));
        }

        public void CancelAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            var dsSubscription = new DsUserSubscription();
            dsServiceCollection.DsAccountTransactionService.CancelAccountSubscription(environment, JHMSubscriptionMapper.Map(subscription, dsSubscription));
        }

        public User GetUserByGlobalId(string globalId, string applicationName)
        {
            Profile p = null;
            return FluentNHibernateHelper.CurrentSession.QueryOver<User>()
                .Left.JoinAlias(x => x.Profile, () => p)
                .Where(x => x.GlobalId == globalId)
                .Where(x => x.IsApproved)
                .List<User>()
                .SingleOrDefault() ?? new User();
        }

        public User GetUserByGlobalId(string globalId, string applicationName, string dsEnvironment)
        {
            var user = GetUserByGlobalId(globalId, applicationName) ?? new User();
            if (!user.IsTransient())
            {
                var dsAccount = dsServiceCollection.DsAccountTransactionService.GetAccount(dsEnvironment, user.AccountNumber);
                if (dsAccount == null)
                    return user;

                user = DsAccountMapper.Map(dsAccount, user);
            }
            return user;
        }

        public void AddGoGreenCode(string dsEnvironment, long accountNumber)
        {
            dsServiceCollection.DsAccountTransactionService.AddAccountCode(dsEnvironment, accountNumber, "NOPAPER","NOTAXPAPER");
        }

        public void DeleteCreditCard(Guid cardId, Guid userId, string applicationName)
        {
            var user = GetUser(userId, applicationName);
            user.DeleteCreditCard(cardId);
        }

        public Address GetUserAddressById(long dsAddressId, string dsEnvironment)
        {
            var dsAddress = dsServiceCollection.DsAccountTransactionService.GetAddress(dsEnvironment, dsAddressId);
            return JHMAddressMapper.Map(dsAddress);
        }

        public Address GetUserPrimaryAddress(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName);
            return
                JHMAddressMapper.Map(dsServiceCollection.DsAccountTransactionService.GetPrimaryAddress(dsEnvironment,user.AccountNumber));
        }

        public Phone GetUserPhone(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName);
            return
                JHMPhoneMapper.Map(dsServiceCollection.DsAccountTransactionService.GetAccount(dsEnvironment, user.AccountNumber).Phone);
        }

        public List<DsTransaction> GetUserTransactions(long accountNumber, int howManyMonthsBack, string dsEnviroment)
        {
            return dsServiceCollection.DsAccountTransactionService.GetTransactionHistory(dsEnviroment, howManyMonthsBack,
                                                                                         accountNumber);
        }

        public DsTransaction GetTransactionByDocumentNumber(long documentNumber, string dsEnviroment,
                                                             long accountNumber)
        {
            return dsServiceCollection.DsAccountTransactionService.GetTransactionByDocumentNumber(dsEnviroment,
                                                                                                  documentNumber,
                                                                                                  accountNumber);
        }

        public void SaveDSAccount(DsAccount DsAccount, string dsEnvironment)
        {
            dsServiceCollection.DsAccountTransactionService.UpdateAccount(dsEnvironment, DsAccount);
        }

        public bool IsUserASaltPartner(long accountNumber, User user, string dsEnvironment)
        {
            //get the account code from donor studio
            accountNumber = GetCurrentDsAccount(accountNumber, dsEnvironment);

            var accountCodes = dsServiceCollection.DsAccountTransactionService.GetAccountCodes(dsEnvironment,accountNumber,true);
            var userIsSaltPartner = false;

            //gets the type and value from the account code list
            foreach (var accountCode in accountCodes)
            {
                var codeType = accountCode.Type;
                var codeValue = accountCode.Value;

                //check to see if the code type is 007 and its value is SALT
                if (codeType != "007" || codeValue != "SALT") continue;

                userIsSaltPartner = true;
                break;
            }
            //returns true if user is salt or false if user is not
            return userIsSaltPartner;
        }

        public bool AddCreditCardToUser(string username, CreditCard creditCard, string applicationName)
        {
            var user = GetUser(username, applicationName);
            return user.AddCreditCard(creditCard);
        }

        public CreditCard GetSavedCreditCard(string username, string token, string applicationName)
        {
            var user = GetUser(username, applicationName);
            return user.GetUnexpiredCreditCards().Find(x => x.Token.Equals(token));
        }

        public User GetUser(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName) ?? new User();
            if (!user.IsTransient())
            {

                var dsAccount = dsServiceCollection.DsAccountTransactionService.GetAccount(dsEnvironment, user.AccountNumber);

                //Note: I'm not sure if we should be doing this here
                if (dsAccount == null && user.Profile.PrimaryAddress != null)
                {
                    user.AccountNumber = dsServiceCollection.DsAccountTransactionService.CreateAccount(dsEnvironment, DsAccountMapper.Map(user));
                    Save(user);
                    dsAccount = dsServiceCollection.DsAccountTransactionService.GetAccount(dsEnvironment, user.AccountNumber);
                    //throw new Exception(String.Format("Could not find DS Account for User: {0} in Environment: {1}",user.Username, dsEnvironment));
                }


                user = DsAccountMapper.Map(dsAccount, user);
            }
            return user;
        }

        #endregion
    }
}