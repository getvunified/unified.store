using Jhm.Common.Repositories;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public interface IProfileRepository : IRepository<Profile>
    {
        
    }
}