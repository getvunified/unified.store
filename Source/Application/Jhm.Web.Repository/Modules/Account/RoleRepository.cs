﻿using System.Collections.Generic;
using Jhm.DonorStudio.Services.Services;
using Jhm.FluentNHibernateProvider;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Repository.Modules.Account
{
    public class RoleRepository : RepositoryDecorator<Role>, IRoleRepository
    {
        private readonly DSServiceCollection dsServiceCollection;

        
        public RoleRepository(DSServiceCollection dsServiceCollection)
        {
            this.dsServiceCollection = dsServiceCollection;
        }
        public Role GetRole(string roleName, string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(Role))
                .Add(NHibernate.Criterion.Restrictions.Eq("RoleName", roleName))
                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", applicationName))
                .UniqueResult<Role>();
        }

        public IList<Role> GetAllRoles(string applicationName)
        {
            return FluentNHibernateHelper.CurrentSession.CreateCriteria(typeof(Role))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", applicationName))
                                                .List<Role>();
        }
    }
}