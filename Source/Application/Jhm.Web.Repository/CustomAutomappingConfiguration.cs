﻿using System;
using FluentNHibernate.Automapping;
using Foundation;
using Jhm.Common.Framework.Extensions;
using Jhm.em3.Core;
using Jhm.Web.Core;

namespace Jhm.Web.Repository
{
    public class CustomAutomappingConfiguration : DefaultAutomappingConfiguration
    {
        public override bool IsComponent(Type type)
        {
            return (
                        type == typeof(Address)
                        || type == typeof(Phone)
                        || type == typeof(DateRange)
                        
                   );
        }



        public override bool ShouldMap(Type type)
        {
            return (
                        type.Namespace.StartsWith("Jhm.Web.Core.")


                        && (MiscExtensions.IsNull(type.BaseType) || !type.BaseType.Name.EndsWith("Exception"))

                        && !type.Name.Equals("enum")

                        //NOTE: Ensure all the 'lookups' are not mapped
                        && type.BaseType != typeof(Enumeration<int>)
                        && type.BaseType != typeof(Enumeration<string>)


                        && !(type.Name.Contains("Client"))

                        && !(type.Name.Contains("ViewModel"))


                        //&& !(type.Name.Equals("Product"))

                        //  Rating Related Items
                //&& !(type.Name.Equals("Review"))
                //&& !(type.Name.Equals("Category"))
                //&& !(type.Name.Equals("ReviewStatus"))



                        // Donation Related Items
                        && !(type.Name.Equals("Donation"))
                        && !(type.Name.Equals("DonationOption"))
                        && !(type.Name.Equals("DonationCartItemFactory"))
                        && !(type.Name.Equals("DonationCriteria"))
                        && !(type.Name.Equals("AmountDonationCriteria"))
                        && !(type.Name.Equals("MultipleOfDonationCriteria"))
                        && !(type.Name.Equals("BaseDonationOption"))
                        && !(type.Name.Equals("AnyAmountDonationOption"))
                        && !(type.Name.Equals("FixedAmountDonationOption"))
                        && !(type.Name.Equals("MultipleOfdonationOption"))
                        && !(type.Name.Equals("AmountRangeDonationOption"))
                        && !(type.Name.Equals("MinimumAmountDonationOption"))
                        && !(type.Name.Equals("DonationOptionField"))

                   );
        }
    }
}
