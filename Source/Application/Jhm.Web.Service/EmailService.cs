using System.Net.Mail;
using Jhm.Common;

namespace Jhm.Web.Service
{
    #region Config Setup
        //http://weblogs.asp.net/scottgu/archive/2005/12/10/432854.aspx
        //     <system.net>
        //    <mailSettings>
        //      <smtp from="test@foo.com">
        //        <network host="smtpserver1" port="25" userName="username" password="secret" defaultCredentials="true" />
        //      </smtp>
        //    </mailSettings>
        //  </system.net>
    #endregion

    public class EmailService : IEmailService
    {
        private readonly IEmailer emailer;

        public EmailService(IEmailer emailer)
        {
            this.emailer = emailer;
        }

        public void SendEmail(MailMessage message)
        {
            emailer.SendEmail(message);
        }


        public void SendDMEmail(MailMessage message)
        {
            message.From = new MailAddress("noreply@differencemedia.org");
            emailer.SendEmail(message);
        }
    }
}
