﻿using System.Net.Mail;

namespace Jhm.Web.Service
{
    public interface IEmailService
    {
        void SendEmail(MailMessage message);
        void SendDMEmail(MailMessage message);
    }
}