using System;
using Getv.Security;

using Jhm.DonorStudio.Services.Interfaces;
using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Service.Modules.Account;
using Jhm.Web.Service.Modules.ContentManagement;
using Jhm.Web.Service.Modules.ECommerce;
using StructureMap;
using getv.donorstudio;

namespace Jhm.Web.Service
{
    public class ServiceCollection : IServiceCollection
    {
        private readonly IContainer container;

        #region MembershipServices

        public ServiceCollection(IContainer container)
        {
            this.container = container;
        }

        public Modules.Account.IAccountService AccountService
        {
            get { return container.GetInstance<CustomAccountService>(); }
        }

        public IHomeBannerService HomeBannerService
        {
            get { return container.GetInstance<HomeBannerService>(); }
        }

        public IFormsAuthenticationService FormsService
        {
            get { return container.GetInstance<FormsAuthenticationService>(); }
        }

        public IMembershipService MembershipService
        {
            get { return container.GetInstance<CustomAccountMembershipService>(); }
        }

        public ISecurityService SecurityService
        {
            get { return container.GetInstance<ISecurityService>(); }
        }

        #endregion

        public ICatalogService CatalogService
        {
            get { return container.GetInstance<CatalogService>(); }
        }

        public IDonationService DonationService
        {
            get { return container.GetInstance<DonationService>(); }
        }

        public ICartService CartService
        {
            get { return container.GetInstance<CartService>(); }
        }

        public IEmailService EmailService
        {
            get { return container.GetInstance<EmailService>(); }
        }

        public IDSServiceCollection DsServiceCollection
        {
            get { return container.GetInstance<DSServiceCollection>(); }
        }

        public ICommerceService CommerceService
        {
            get { return container.GetInstance<CommerceService>(); }
        }

        public IDigitalDownloadService DigitalDownloadService
        {
            get { return container.GetInstance<DigitalDownloadService>(); }
        }


        public IPromotionalItemService PromotionalItemService
        {
            get { return container.GetInstance<PromotionalItemService>(); }
        }

        public IDiscountItemService DiscountItemService
        {
            get { return container.GetInstance<DiscountItemService>(); }
        }


        public ISubscriptionService SubscriptionService
        {
            get { return container.GetInstance<SubscriptionService>(); }
        }



        public IJhmMagazineListingService JhmMagazineListingService
        {
            get { return container.GetInstance<JhmMagazineListingService>(); }
        }

        public IDiscountCodeService DiscountCodeService { get { return container.GetInstance<DiscountCodeService>(); } }

        public IDsReportService DSReportService { get { return container.GetInstance<DsReportService>(); } }


        public IGiftCardService GiftCardService
        {
            get { return container.GetInstance<GiftCardService>(); }
        }


		public IEmailSubscriptionListService EmailSubscriptionListService
		{
			get { return container.GetInstance<EmailSubscriptionListService>(); }
		}
	}
}
