﻿using System;
using System.Collections.Generic;
using Jhm.Common;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.em3.Core;

namespace Jhm.Web.Service.Modules.Account
{
    public interface IAccountService : IService
    {
        Role GetRole(string roleName, string applicationName);
        void AddUsersToRoles(string[] usernames, string[] rolenames, string applicationName);
        void CreateRole(Role role);
        void DeleteRole(Role role);
        IList<Role> GetAllRoles(string applicationName);
        User GetUser(string username, string applicationName, string dsEnvironment);
        User GetUser(string globalIdOrUsername);
        User GetUser(string globalIdOrUsername, string applicationName);
        User GetUser(Guid id);
        User GetUser(Guid Id, string applicationName);
        void RemoveUsersFromRoles(string[] usernames, string[] rolenames, string applicationName);
        Profile GetProfile(string username, bool isAnonymous, string applicationName);
        Profile GetProfile(string username, string applicationName);
        Profile GetProfile(Guid id, string applicationName);
        IList<Profile> GetProfiles(DateTime userInactiveSinceDate, bool isAnonymous, string applicationName);
        IList<Profile> GetProfiles(string applicationName);
        void SaveUser(User user, bool saveGlobalUser = true, string dsEnvironment = null);
        IList<User> GetUsers(string applicationName);
        IList<User> GetUsers(string emailToMatch, string usernameToMatch, string applicationName);
        void DeleteUser(User user);
        int GetAllUserCount(string applicationName);
        int GetNumberOfUsersOnline(DateTime compareTime, string applicationName);
        User GetUserByEmail(string email, string applicationName);
        User GetUserByEmail(string email);
        IList<User> GetUsersByEmail(string email, string applicationName);


        IList<Address> GetUserAddresses(string username, string applicationName, string dsEnvironment);
        long AddAddressToUser(long dsAccountNumber, Address address, string dsEnvironment);
        long UpdateAccountAddress(long dsAccountNumber, Address address, string dsEnvironment);
        long DeleteAccountAddress(long dsAccountNumber, Address address, string dsEnvironment);
        Address GetUserAddressById(long shippingAddresId, string dsEnvironment);
        Address GetUserPrimaryAddress(string username, string applicationName, string dsEnvironment);
        Phone GetUserPhone(string username, string applicationName, string dsEnvironment);



        bool AddCreditCardToUser(string username, CreditCard creditCard, string applicationName);

        CreditCard GetSavedCreditCard(string username, string token, string applicationName);
        List<TransactionViewModel> GetTransactionsList(User user, int howManyMonthsBack, string dsEnviroment = null);
        TransactionViewModel GetTransactionByDocumentNumber(string dsEnviroment, User user, long documentNumber);
        bool IsUserASaltPartner(Client client, User user, string dsEnviroment);

        List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(string username, string applicationName, string dsEnvironment);
        long AddSubscription(string environment, User user, UserSubscriptionViewModel userSubscription);


        List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(long accountNumber, string dsEnvironment);
        void UpdateAccountSubscription(string environment, UserSubscriptionViewModel subscription);
        void CancelAccountSubscription(string environment, UserSubscriptionViewModel subscription);

        User GetUserByGlobalId(string globalId);
        User GetUserByGlobalId(string globalId, string applicationName);

        void AddGoGreenCode(string dsEnvironment, long accountNumber);

        void DeleteCreditCard(Guid cardId, Guid userId, string applicationName);
    }
}