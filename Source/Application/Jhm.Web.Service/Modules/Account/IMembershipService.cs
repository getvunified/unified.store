﻿using System;
using System.Web.Security;
using Foundation;
using Getv.Security;

namespace Jhm.Web.Service.Modules.Account
{
    public interface IMembershipService
    {
        int MinPasswordLength { get; }

        bool ValidateUser(string userName, string password);
        //MembershipCreateStatus CreateUser(string userName, string password, string email, string passwordQuestion, string passwordAnswer);
        MembershipCreateStatus CreateUserWithGlobalId(string globalId, string userName, string password, string email, string passwordQuestion, string passwordAnswer);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
        string ResetPassword(string userName, string passwordAnswer);

        bool ChangePasswordQuestion(string userName, string password, string newPasswordQuestion, string newPasswordAnswer);

        bool ValidateUser(string userName, string password, out string reason);
        ApiMessage ValidateSecurityUser(string userName, string password);
    }
}