﻿using System;
using System.Web.Security;
using Foundation;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;


namespace Jhm.Web.Service.Modules.Account
{
    public class CustomAccountMembershipService : IMembershipService
    {
        private readonly MembershipProvider provider;

        //public AccountMembershipService(MembershipProvider provider)
        //{
        //    this.provider = provider;
        //}
        private readonly IServiceCollection service;


        public CustomAccountMembershipService(IServiceCollection serviceCollection)
        {
            provider = Membership.Provider;
            service = serviceCollection;
        }

        public int MinPasswordLength
        {
            get
            {
                return provider.MinRequiredPasswordLength;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

            return provider.ValidateUser(userName, password);
        }

        //NOTE: This method is obsolete in the new GETV-JHM Authentication integration
        //      Use ValidateSecurityUser instead.
        [Obsolete]
        public bool ValidateUser(string userName, string password, out string reason)
        {
            reason = "The user name or password provided is incorrect.";
            var authenticationResult = service.SecurityService.AuthenticateUser(userName, password);
            if (!authenticationResult.IsSuccessful)
            {
                reason = authenticationResult.Error.UserMessage.SeparateCapitalizedWords();
            }
            return authenticationResult.IsSuccessful;
        }

        public ApiMessage ValidateSecurityUser(string userName, string password)
        {
            var authenticationResult = service.SecurityService.AuthenticateUser(userName, password);
            return authenticationResult;
        }

        //public MembershipCreateStatus CreateUser(string userName, string password, string email, string passwordQuestion, string passwordAnswer)
        //{
        //    if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
        //    if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
        //    if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");
        //    if (String.IsNullOrEmpty(passwordQuestion)) throw new ArgumentException("Must Select A Password Question.", "passwordQuestion");
        //    if (String.IsNullOrEmpty(passwordAnswer)) throw new ArgumentException("Password Answer cannot be null or empty.", "passwordAnswer");

        //    MembershipCreateStatus status;
        //    provider.CreateUser(userName, password, email, passwordQuestion, passwordAnswer, true, null, out status);
        //    return status;
        //}

        public MembershipCreateStatus CreateUserWithGlobalId(string globalId, string userName, string password, string email, string passwordQuestion, string passwordAnswer)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");
            if (String.IsNullOrEmpty(passwordQuestion)) throw new ArgumentException("Must Select A Password Question.", "passwordQuestion");
            if (String.IsNullOrEmpty(passwordAnswer)) throw new ArgumentException("Password Answer cannot be null or empty.", "passwordAnswer");

            MembershipCreateStatus status;
            provider.CreateUser(userName, password, email, passwordQuestion, passwordAnswer, true, globalId, out status);
            return status;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                return provider.ChangePassword(userName, oldPassword, newPassword); ;
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

        public string ResetPassword(string userName, string passwordAnswer)
        {
            if (String.IsNullOrEmpty(passwordAnswer)) throw new ArgumentException("Value cannot be null or empty.", "passwordAnswer");
            MembershipUser currentUser = provider.GetUser(userName, true /* userIsOnline */);
            if (MiscExtensions.IsNull(currentUser))
                throw new MembershipPasswordException("User Could Not Be found.");
            return currentUser.ResetPassword(passwordAnswer);
        }

        public bool ChangePasswordQuestion(string userName, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            if (String.IsNullOrEmpty(newPasswordQuestion)) throw new ArgumentException("Value cannot be null or empty.", "newPasswordQuestion");
            if (String.IsNullOrEmpty(newPasswordAnswer)) throw new ArgumentException("Value cannot be null or empty.", "newPasswordAnswer");
            var currentUser = provider.GetUser(userName, true /* userIsOnline */);
            if (MiscExtensions.IsNull(currentUser))
                throw new MembershipPasswordException("User Could Not Be found.");
            return currentUser.ChangePasswordQuestionAndAnswer(password, newPasswordQuestion, newPasswordAnswer);
        }

        
    }
}