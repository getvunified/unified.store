﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Getv.Security;
using Jhm.Common.Framework.Extensions;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Repository.Modules.ECommerce;
using Jhm.em3.Core;
using User = Jhm.Web.Core.Models.User;
using Role = Jhm.Web.Core.Models.Role;

namespace Jhm.Web.Service.Modules.Account
{
    public class CustomAccountService : IAccountService
    {
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly ISecurityService securityService;
        private readonly ISubscriptionsRepository subsRepository;

        public CustomAccountService(IUserRepository userRepository, IRoleRepository roleRepository, ISecurityService securityService, ISubscriptionsRepository subsRepository)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.subsRepository = subsRepository;
            this.securityService = securityService;
        }

        public Role GetRole(string roleName, string applicationName)
        {
            return roleRepository.GetRole(roleName, applicationName) ?? new Role();
        }
        public void AddUsersToRoles(string[] usernames, string[] rolenames, string applicationName)
        {
            foreach (string username in usernames)
            {
                foreach (string rolename in rolenames)
                {
                    //get the user
                    User user = userRepository.GetUser(username, applicationName);

                    if (!user.IsTransient())
                    {
                        //get the role first from db
                        Role role = roleRepository.GetRole(rolename, applicationName);
                        user.AddRole(role);
                    }
                    userRepository.Save(user);
                }
            }

        }
        public void CreateRole(Role role)
        {
            roleRepository.Save(role);
        }
        public void DeleteRole(Role role)
        {
            roleRepository.Delete(role);
        }
        public IList<Role> GetAllRoles(string applicationName)
        {
            return roleRepository.GetAllRoles(applicationName);
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] rolenames, string applicationName)
        {
            foreach (var username in usernames)
            {
                var user = userRepository.GetUser(username, applicationName) ?? new User();
                if (user.IsTransient()) continue;
                var rolesToDelete = (from rolename in rolenames
                                     let roles = user.Roles.ToLIST()
                                     from r in (IList<Role>)roles
                                     where r.RoleName.Equals(rolename)
                                     select r).ToList();
                foreach (var roleToDelete in rolesToDelete)
                    user.RemoveRole(roleToDelete);
                userRepository.Save(user);
            }
        }

        public Core.Models.User GetUser(string globalIdOrUsername, string applicationName, string dsEnvironment)
        {
            var user = GetUserByGlobalId(globalIdOrUsername, applicationName, dsEnvironment);
            if (user.IsTransient())
            {
                user = userRepository.GetUser(globalIdOrUsername, applicationName, dsEnvironment);
            }
            return user;
        }

        private Core.Models.User GetUserByGlobalId(string gloabalId, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserByGlobalId(gloabalId, applicationName, dsEnvironment) ?? new Core.Models.User();
        }

        public Core.Models.User GetUser(string globalIdOrUsername)
        {
            const string applicationName = "Jhm.Web";
            return GetUser(globalIdOrUsername, applicationName);
        }

        public Core.Models.User GetUser(string globalIdOrUsername, string applicationName)
        {
            var user = GetUserByGlobalId(globalIdOrUsername, applicationName);
            if (user.IsTransient())
            {
                user = userRepository.GetUser(globalIdOrUsername, applicationName) ?? new Core.Models.User();
                if (!String.IsNullOrEmpty(user.GlobalId))
                {
                    user = GetUserByGlobalId(user.GlobalId, applicationName);
                }
            }
            return user;
        }

        public Core.Models.User GetUser(Guid id)
        {
            const string applicationName = "Jhm.Web";
            return GetUser(id, applicationName);
        }

        public Core.Models.User GetUser(Guid Id, string applicationName)
        {
            return userRepository.GetUser(Id, applicationName) ?? new Core.Models.User();
        }


        public Profile GetProfile(string username, bool isAnonymous, string applicationName)
        {
            var user = userRepository.GetUser(username, applicationName) ?? new User();
            if (!user.IsTransient())
                return user.Profile ?? new Profile();
            return new Profile();
        }

        public Profile GetProfile(string username, string applicationName)
        {
            return GetProfile(username, false, applicationName);
        }

        public Profile GetProfile(Guid userId, string applicationName)
        {
            var user = userRepository.GetUser(userId, applicationName) ?? new User();
            return user.Profile;
        }

        public IList<Profile> GetProfiles(DateTime userInactiveSinceDate, bool isAnonymous, string applicationName)
        {
            throw new NotImplementedException();
        }

        public IList<Profile> GetProfiles(string applicationName)
        {
            throw new NotImplementedException();
        }

        public void SaveUser(User user, bool saveGlobalUser = true, string dsEnvironment = null)
        {
            dsEnvironment = dsEnvironment ?? user.DsEnvironment;
            var initDSAccount = user.AccountNumber;
            if (dsEnvironment.IsNot().NullOrEmpty() && user.Profile.PrimaryAddress != null && user.AccountNumber > 0)
            {
                user.AccountNumber = userRepository.GetCurrentDsAccount(user.AccountNumber, dsEnvironment);
                SaveDsAccount(user, dsEnvironment);
            }
            userRepository.Save(user);
            if ((saveGlobalUser) || (initDSAccount != user.AccountNumber))
            {
                Guid parsedGuid = Guid.Parse(user.GlobalId);


                var result = securityService.SaveUser(parsedGuid, user.Username, user.Email,
                                                    user.Profile.FirstName, user.Profile.LastName, user.NewsLetterOptIn,
                                                    user.AccountNumber, dsEnvironment);
                if (!result.IsSuccessful)
                {
                    throw new Exception(result.Error.UserMessage);
                }
            }

        }

        private void SaveDsAccount(Core.Models.User user, string dsEnvironment)
        {
            var DsAccount = DsAccountMapper.Map(user);

            userRepository.SaveDSAccount(DsAccount, dsEnvironment);
        }

        public IList<Core.Models.User> GetUsers(string applicationName)
        {
            throw new NotImplementedException();
        }

        public IList<Core.Models.User> GetUsers(string emailToMatch, string usernameToMatch, string applicationName)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(User user)
        {
            userRepository.Delete(user);
        }

        public int GetAllUserCount(string applicationName)
        {
            throw new NotImplementedException();
        }

        public int GetNumberOfUsersOnline(DateTime compareTime, string applicationName)
        {
            throw new NotImplementedException();
        }

        public Core.Models.User GetUserByEmail(string email, string applicationName)
        {
            return userRepository.GetUserByEmail(email, applicationName) ?? new Core.Models.User();
        }

        public Core.Models.User GetUserByEmail(string email)
        {
            const string applicationName = "Jhm.Web";
            return GetUserByEmail(email, applicationName);
        }

        public IList<Core.Models.User> GetUsersByEmail(string email, string applicationName)
        {
            return userRepository.GetUsersByEmail(email, applicationName) ?? new List<Core.Models.User>();
        }

        public IList<Address> GetUserAddresses(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserAddresses(username, applicationName, dsEnvironment);
        }

        public long AddAddressToUser(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.AddAddressToUser(dsAccountNumber, address, dsEnvironment);
        }

        public long UpdateAccountAddress(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.UpdateAccountAddress(dsAccountNumber, address, dsEnvironment);
        }

        public long DeleteAccountAddress(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.DeleteAccountAddress(dsAccountNumber, address, dsEnvironment);
        }

        public Address GetUserAddressById(long shippingAddresId, string dsEnvironment)
        {
            return userRepository.GetUserAddressById(shippingAddresId, dsEnvironment);
        }

        public Address GetUserPrimaryAddress(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserPrimaryAddress(username, applicationName, dsEnvironment);
        }

        public Phone GetUserPhone(string username, string applicationName, string dsEnvironment)
        {
            throw new NotImplementedException();
        }

        public bool AddCreditCardToUser(string username, CreditCard creditCard, string applicationName)
        {
            return userRepository.AddCreditCardToUser(username, creditCard, applicationName);
        }

        public CreditCard GetSavedCreditCard(string username, string token, string applicationName)
        {
            return userRepository.GetSavedCreditCard(username, token, applicationName);
        }

        public List<TransactionViewModel> GetTransactionsList(Core.Models.User user, int howManyMonthsBack, string dsEnviroment = null)
        {
            if (dsEnviroment.IsNullOrEmpty())
            {
                throw new Exception("No transaction Found");
            }

            return userRepository.GetUserTransactions(user.AccountNumber, howManyMonthsBack, dsEnviroment).Select(DsTransactionMapper.Map).ToList();
        }

        public TransactionViewModel GetTransactionByDocumentNumber(string dsEnviroment, Core.Models.User user, long documentNumber)
        {
            var transaction = userRepository.GetTransactionByDocumentNumber(user.AccountNumber, dsEnviroment, documentNumber);
            return DsTransactionMapper.Map(transaction);
        }

        public bool IsUserASaltPartner(Client client, Core.Models.User user, string dsEnviroment)
        {
            return userRepository.IsUserASaltPartner(user.AccountNumber, user, dsEnviroment);
        }

        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName, dsEnvironment);
            return GetActiveAccountSubscriptions(user.AccountNumber, dsEnvironment);
        }

        public long AddSubscription(string environment, User user, UserSubscriptionViewModel userSubscription)
        {
            return userRepository.AddSubscription(environment, user, userSubscription);
        }

        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(long accountNumber, string dsEnvironment)
        {
            var subs = userRepository.GetActiveAccountSubscriptions(accountNumber, dsEnvironment);
            foreach (var sub in subs)
            {
                var masterSub = subsRepository.GetSubscription(dsEnvironment, sub.SubscriptionCode);
                sub.SubscriptionTitle = masterSub.Description;
                var price = masterSub.Prices.SingleOrDefault(x => x.PriceCode == sub.PriceCode);
                if (price != null)
                {
                    sub.PriceCodeDescription = price.Description;
                    // Calculate Expiration Date if not set in Donor based on number of issues bi-montly from the start date
                    if (sub.ExpirationDate == null)
                    {
                        sub.ExpirationDate = sub.StartDate.GetValueOrDefault().AddMonths((int)price.NumberOfIssues * 2);
                    }
                }
                // Setting default expiration date to one year from the start date
                if (sub.ExpirationDate == null)
                {
                    sub.ExpirationDate = sub.StartDate.GetValueOrDefault().AddYears(1);
                }
            }
            return subs;
        }

        public void UpdateAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            userRepository.UpdateAccountSubscription(environment, subscription);
        }

        public void CancelAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            userRepository.CancelAccountSubscription(environment, subscription);
        }

        public Core.Models.User GetUserByGlobalId(string securityId)
        {
            const string applicationName = "Jhm.Web";
            return GetUserByGlobalId(securityId, applicationName);
        }

        public Core.Models.User GetUserByGlobalId(string globalId, string applicationName)
        {
            var localUser = userRepository.GetUserByGlobalId(globalId, applicationName);
            if (!localUser.IsTransient())
            {
                var securityUser = securityService.LoadUserById(Guid.Parse(localUser.GlobalId));
                if (securityUser.IsSuccessful && securityUser.Data != null )
                {
                    localUser.Email = securityUser.Data.EmailAddress;
                    localUser.Password = securityUser.Data.Password;
                    localUser.Username = securityUser.Data.Username;
                    localUser.DsEnvironment = securityUser.Data.DonorEnvironment;
                    localUser.AccountNumber = securityUser.Data.DonorAccountNumber;
                }
            }
            return localUser;
        }


        public void AddGoGreenCode(string dsEnvironment, long accountNumber)
        {
            userRepository.AddGoGreenCode(dsEnvironment, accountNumber);
        }

        public void DeleteCreditCard(Guid cardId, Guid userId, string applicationName)
        {
            userRepository.DeleteCreditCard(cardId, userId, applicationName);
        }
    }
}
