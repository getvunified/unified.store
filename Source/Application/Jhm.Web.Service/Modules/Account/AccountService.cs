﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using Jhm.DonorStudio.Services.Mappers;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Repository.Modules.ECommerce;
using Jhm.em3.Core;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;


namespace Jhm.Web.Service.Modules.Account
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly ISubscriptionsRepository subsRepository;

        public AccountService(IUserRepository userRepository, IRoleRepository roleRepository, ISubscriptionsRepository subsRepository)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.subsRepository = subsRepository;
        }



        #region Role Methods

        public Role GetRole(string roleName, string applicationName)
        {
            return roleRepository.GetRole(roleName, applicationName) ?? new Role();
        }
        public void AddUsersToRoles(string[] usernames, string[] rolenames, string applicationName)
        {
            foreach (string username in usernames)
            {
                foreach (string rolename in rolenames)
                {
                    //get the user
                    User user = userRepository.GetUser(username, applicationName);

                    if (!user.IsTransient())
                    {
                        //get the role first from db
                        Role role = roleRepository.GetRole(rolename, applicationName);
                        user.AddRole(role);
                    }
                    userRepository.Save(user);
                }
            }

        }
        public void CreateRole(Role role)
        {
            roleRepository.Save(role);
        }
        public void DeleteRole(Role role)
        {
            roleRepository.Delete(role);
        }
        public IList<Role> GetAllRoles(string applicationName)
        {
            return roleRepository.GetAllRoles(applicationName);
        }

        public User GetUser(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUser(username, applicationName, dsEnvironment);

        }


        public void RemoveUsersFromRoles(string[] usernames, string[] rolenames, string applicationName)
        {
            foreach (var username in usernames)
            {
                var user = userRepository.GetUser(username, applicationName) ?? new User();
                if (user.IsTransient()) continue;
                var rolesToDelete = (from rolename in rolenames
                                     let roles = user.Roles
                                     from r in (IList<Role>)roles
                                     where r.RoleName.Equals(rolename)
                                     select r).ToList();
                foreach (var roleToDelete in rolesToDelete)
                    user.RemoveRole(roleToDelete);
                userRepository.Save(user);
            }
        }

        #endregion

        #region Profile Methods

        public Profile GetProfile(string username, bool isAnonymous, string applicationName)
        {
            User user = userRepository.GetUser(username, applicationName) ?? new User();
            if (!user.IsTransient())
                return user.Profile ?? new Profile();
            return new Profile();
        }
        public Profile GetProfile(string username, string applicationName)
        {
            return GetProfile(username, false, applicationName);
        }
        public Profile GetProfile(Guid userId, string applicationName)
        {
            var user = userRepository.GetUser(userId, applicationName) ?? new User();
            return user.Profile;
        }

        public IList<Profile> GetProfiles(DateTime userInactiveSinceDate, bool isAnonymous, string applicationName)
        {
            return userRepository.GetProfiles(userInactiveSinceDate, isAnonymous, applicationName) ?? new List<Profile>();
        }
        public IList<Profile> GetProfiles(string applicationName)
        {
            return userRepository.GetProfiles(applicationName) ?? new List<Profile>();
        }


        #endregion

        #region User Methods

        //TODO:  Come look at me..
        public User GetUser(string username)
        {
            const string applicationName = "Jhm.Web";
            return GetUser(username, applicationName);
        }

        public User GetUser(string username, string applicationName)
        {
            return userRepository.GetUser(username, applicationName) ?? new User();
        }

        //TODO:  Come look at me..
        public User GetUser(Guid id)
        {
            const string applicationName = "Jhm.Web";
            return GetUser(id, applicationName);
        }

        public User GetUser(Guid id, string applicationName)
        {
            return userRepository.GetUser(id, applicationName) ?? new User();
        }
        public IList<User> GetUsers(string applicationName)
        {
            return userRepository.GetUsers(applicationName) ?? new List<User>();
        }
        public IList<User> GetUsers(string emailToMatch, string usernameToMatch, string applicationName)
        {
            return userRepository.GetUsers(emailToMatch, usernameToMatch, applicationName) ?? new List<User>();
        }

        //TODO:  Come look at me too..
        public User GetUserByEmail(string email)
        {
            const string applicationName = "Jhm.Web";
            return GetUserByEmail(email, applicationName);
        }
        public User GetUserByEmail(string email, string applicationName)
        {
            return userRepository.GetUserByEmail(email, applicationName) ?? new User();
        }
        public IList<User> GetUsersByEmail(string email, string applicationName)
        {
            return userRepository.GetUsersByEmail(email, applicationName) ?? new List<User>();
        }
        public int GetAllUserCount(string applicationName)
        {
            return userRepository.GetAllUserCount(applicationName);
        }
        public int GetNumberOfUsersOnline(DateTime compareTime, string applicationName)
        {
            return userRepository.GetNumberOfUsersOnline(compareTime, applicationName);
        }
        public void SaveUser(User user, bool environment, string dsEnvironment = null)
        {
            userRepository.Save(user);
            if(dsEnvironment.IsNotNullOrEmpty())
                SaveDsAccount(user, dsEnvironment);
        }

        private void SaveDsAccount(User user, string dsEnvironment)
        {
            var DsAccount = DsAccountMapper.Map(user);
            userRepository.SaveDSAccount(DsAccount, dsEnvironment);
        }

        public void DeleteUser(User user)
        {
            userRepository.Delete(user);
        }


        public List<TransactionViewModel> GetTransactionsList(User user, int howManyMonthsBack, string dsEnviroment = null)
        {
            if (String.IsNullOrEmpty(dsEnviroment))
            {
                throw new Exception("No transaction Found");
            }

            return userRepository.GetUserTransactions(user.AccountNumber, howManyMonthsBack, dsEnviroment).Select(DsTransactionMapper.Map).ToList();
        }

        public TransactionViewModel GetTransactionByDocumentNumber(string dsEnviroment, User user, long documentNumber)
        {
            var transaction = userRepository.GetTransactionByDocumentNumber(user.AccountNumber, dsEnviroment, documentNumber);
            var mappedTransaction = DsTransactionMapper.Map(transaction);
            if ( mappedTransaction.Subscriptions.Any())
            {
                foreach (var sub in mappedTransaction.Subscriptions)
                {
                    var masterSub = subsRepository.GetSubscription(dsEnviroment, sub.SubscriptionCode);
                    sub.SubscriptionTitle = masterSub.Description;
                    var price = masterSub.Prices.SingleOrDefault(x => x.PriceCode == sub.PriceCode);
                    if (price != null)
                    {
                        sub.PriceCodeDescription = price.Description;
                    }
                }
            }
            return mappedTransaction;
        }



        #region Donor Studio Address Lookups

        public IList<Address> GetUserAddresses(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserAddresses(username, applicationName, dsEnvironment);
        }

        public long AddAddressToUser(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.AddAddressToUser(dsAccountNumber,address, dsEnvironment);
        }
        public long UpdateAccountAddress(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.UpdateAccountAddress(dsAccountNumber, address, dsEnvironment);
        }
        public long DeleteAccountAddress(long dsAccountNumber, Address address, string dsEnvironment)
        {
            return userRepository.DeleteAccountAddress(dsAccountNumber, address, dsEnvironment);
        }
        public Address GetUserAddressById(long shippingAddresId, string dsEnvironment)
        {
            return userRepository.GetUserAddressById(shippingAddresId, dsEnvironment);
        }

        public Address GetUserPrimaryAddress(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserPrimaryAddress(username, applicationName, dsEnvironment);
        }

        public Phone GetUserPhone(string username, string applicationName, string dsEnvironment)
        {
            return userRepository.GetUserPhone(username, applicationName, dsEnvironment);
        }

        public bool AddCreditCardToUser(string username, CreditCard creditCard, string applicationName)
        {
            return userRepository.AddCreditCardToUser(username, creditCard, applicationName);
        }

        public CreditCard GetSavedCreditCard(string username, string token, string applicationName)
        {
            return userRepository.GetSavedCreditCard(username, token, applicationName);
        }

        public bool IsUserASaltPartner(Client client, User user, string dsEnviroment)
        {
            return userRepository.IsUserASaltPartner(user.AccountNumber, user, dsEnviroment);
        }
        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(string username, string applicationName, string dsEnvironment)
        {
            var user = GetUser(username, applicationName, dsEnvironment);
            return GetActiveAccountSubscriptions(user.AccountNumber, dsEnvironment);
        }

        public long AddSubscription(string environment, User user, UserSubscriptionViewModel userSubscription)
        {
            return userRepository.AddSubscription(environment, user, userSubscription);
        }

        public List<UserSubscriptionViewModel> GetActiveAccountSubscriptions(long accountNumber, string dsEnvironment)
        {
            var subs = userRepository.GetActiveAccountSubscriptions(accountNumber, dsEnvironment);
            foreach (var sub in subs)
            {
                var masterSub = subsRepository.GetSubscription(dsEnvironment, sub.SubscriptionCode);
                sub.SubscriptionTitle = masterSub.Description;
                var price = masterSub.Prices.SingleOrDefault(x => x.PriceCode == sub.PriceCode);
                if (price != null)
                {
                    sub.PriceCodeDescription = price.Description;
                    // Calculate Expiration Date if not set in Donor based on number of issues bi-montly from the start date
                    if ( sub.ExpirationDate == null )
                    {
                        sub.ExpirationDate = sub.StartDate.GetValueOrDefault().AddMonths((int) price.NumberOfIssues*2);
                    }
                }
                // Setting default expiration date to one year from the start date
                if ( sub.ExpirationDate == null )
                {
                    sub.ExpirationDate = sub.StartDate.GetValueOrDefault().AddYears(1);
                }
            }
            return subs;
        }

        public void UpdateAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            userRepository.UpdateAccountSubscription(environment,subscription);
        }

        public void CancelAccountSubscription(string environment, UserSubscriptionViewModel subscription)
        {
            userRepository.CancelAccountSubscription(environment,subscription);
        }

        public void AddGoGreenCode(string dsEnvironment, long accountNumber)
        {
            userRepository.AddGoGreenCode(dsEnvironment, accountNumber);
        }

        public void DeleteCreditCard(Guid cardId, Guid userId, string applicationName)
        {
            throw new NotImplementedException();
        }

        #endregion


        public User GetUserByGlobalId(string globalId)
        {
            throw new NotImplementedException();
        }

        public User GetUserByGlobalId(string globalId, string applicationName)
        {
            throw new NotImplementedException();
        }

        #endregion

        }
}
