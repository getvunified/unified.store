﻿using System;
using System.Web.Security;
using Foundation;
using Getv.Security;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Service.Modules.Account
{
    public class AccountMembershipService : IMembershipService
    {
        private readonly MembershipProvider provider;

        //public AccountMembershipService(MembershipProvider provider)
        //{
        //    this.provider = provider;
        //}

        public AccountMembershipService()
        {
            provider = Membership.Provider;
        }

        public int MinPasswordLength
        {
            get
            {
                return provider.MinRequiredPasswordLength;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

            return provider.ValidateUser(userName, password);
        }

        public bool ValidateUser(string userName, string password, out string reason)
        {
            reason = "The user name or password provided is incorrect.";
            if (String.IsNullOrEmpty(userName))
            {
                reason = "The user name cannot be null or empty.";
                return false;
            }
            if (String.IsNullOrEmpty(password))
            {
                reason = "The password cannot be null or empty.";
                return false;
            }
            var currentUser = provider.GetUser(userName, true /* userIsOnline */);
            if ( currentUser == null)
            {
                return false;
            }

            if ( currentUser.IsLockedOut)
            {
                reason = "Your account has been locked out because the number of allowed login failed attempts has been exceeded. Consider using the forgot password page.";
                return false; 
            }
            return provider.ValidateUser(userName, password);
        }

        public ApiMessage ValidateSecurityUser(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email, string passwordQuestion, string passwordAnswer)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");
            if (String.IsNullOrEmpty(passwordQuestion)) throw new ArgumentException("Must Select A Password Question.", "passwordQuestion");
            if (String.IsNullOrEmpty(passwordAnswer)) throw new ArgumentException("Password Answer cannot be null or empty.", "passwordAnswer");

            MembershipCreateStatus status;
            provider.CreateUser(userName, password, email, passwordQuestion, passwordAnswer, true, null, out status);
            return status;
        }

        public MembershipCreateStatus CreateUserWithGlobalId(string globalId, string userName, string password, string email, string passwordQuestion, string passwordAnswer)
        {
            throw new NotImplementedException();
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                return provider.ChangePassword(userName, oldPassword, newPassword); ;
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

        public string ResetPassword(string userName, string passwordAnswer)
        {
            if (String.IsNullOrEmpty(passwordAnswer)) throw new ArgumentException("Value cannot be null or empty.", "passwordAnswer");
            MembershipUser currentUser = provider.GetUser(userName, true /* userIsOnline */);
            if (MiscExtensions.IsNull(currentUser))
                throw new MembershipPasswordException("User Could Not Be found.");
            return currentUser.ResetPassword(passwordAnswer);
        }

        public bool ChangePasswordQuestion(string userName, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            if (String.IsNullOrEmpty(newPasswordQuestion)) throw new ArgumentException("Value cannot be null or empty.", "newPasswordQuestion");
            if (String.IsNullOrEmpty(newPasswordAnswer)) throw new ArgumentException("Value cannot be null or empty.", "newPasswordAnswer");
            var currentUser = provider.GetUser(userName, true /* userIsOnline */);
            if (MiscExtensions.IsNull(currentUser))
                throw new MembershipPasswordException("User Could Not Be found.");
            return currentUser.ChangePasswordQuestionAndAnswer(password, newPasswordQuestion, newPasswordAnswer);
        }

        
    }
}