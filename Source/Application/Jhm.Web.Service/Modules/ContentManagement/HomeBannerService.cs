﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ContentManagement;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ContentManagement
{
    public class HomeBannerService: IHomeBannerService
    {
        private readonly IHomeBannerRepository bannerRepository;

        public HomeBannerService(IHomeBannerRepository bannerRepository)
        {
            this.bannerRepository = bannerRepository;
        }

        public IList<HomeBannerItem> GetAll()
        {
            return bannerRepository.GetAll();
        }

        public IList<HomeBannerItem> GetAllActive()
        {
            return bannerRepository.GetAllActive();
        }

        public IList<HomeBannerItem> GetAllActiveByCompany(Company company)
        {
            return bannerRepository.GetAllActiveByCompany(company);
        }

        public HomeBannerItem GetById(Guid id)
        {
            return bannerRepository.GetById(id);
        }

        public void SaveChanges()
        {
            bannerRepository.SaveChanges();
        }

        public IList<HomeBannerItem> GetAllByCompanyCode(int companyCode)
        {
            return bannerRepository.GetAllActiveByCompanyCode(companyCode);
        }

        public IEnumerable<HomeBannerCompany> GetAllBannerCompanies()
        {
            return bannerRepository.GetAllBannerCompanies();
        }

        public void SaveChanges(object entity)
        {
            bannerRepository.SaveChanges(entity);
        }
    }
}
