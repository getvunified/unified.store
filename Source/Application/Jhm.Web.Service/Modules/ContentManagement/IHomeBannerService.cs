﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ContentManagement
{
    public interface IHomeBannerService
    {
        IList<HomeBannerItem> GetAll();

        IList<HomeBannerItem> GetAllActive();

        IList<HomeBannerItem> GetAllActiveByCompany(Company company);

        HomeBannerItem GetById(Guid id);

        void SaveChanges();

        IList<HomeBannerItem> GetAllByCompanyCode(int companyCode);

        IEnumerable<HomeBannerCompany> GetAllBannerCompanies();

        void SaveChanges(object entity);
    }
}
