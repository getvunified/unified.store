﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Service.Modules.ContentManagement
{
    public interface IJhmMagazineListingService
    {
        IList<JhmMagazineListingItem> GetAll();

        IList<JhmMagazineListingItem> GetAllActive();

        JhmMagazineListingItem GetById(Guid id);

        void SaveChanges(JhmMagazineListingItem entity);

        void Delete(Guid id);
    }
}
