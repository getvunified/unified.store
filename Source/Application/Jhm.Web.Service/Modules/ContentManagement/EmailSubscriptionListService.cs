﻿using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ContentManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Service.Modules.ContentManagement
{
	public class EmailSubscriptionListService : IEmailSubscriptionListService
	{
		private readonly IEmailSubscriptionListRepository repository;

		public EmailSubscriptionListService(IEmailSubscriptionListRepository repository)
        {
            this.repository = repository;
        }

		public IList<EmailSubscriptionList> GetAll(){
			return repository.GetAll();
		}


		public ENewsLetterSignUpViewModel GetSubscribedLists(string emailAddress)
		{
			return repository.GetSubscribedLists(emailAddress);
		}


		public void UpdateSubscription(ENewsLetterSignUpViewModel data)
		{
			repository.UpdateSubscription(data);
		}
	}
}
