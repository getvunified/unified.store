﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ContentManagement;

namespace Jhm.Web.Service.Modules.ContentManagement
{
    public class JhmMagazineListingService : IJhmMagazineListingService
    {
        private readonly IJhmMagazineListingRepository _repository;

        public JhmMagazineListingService(IJhmMagazineListingRepository repository)
        {
            _repository = repository;
        }

        #region Implementation of IJhmMagazineListingService

        public IList<JhmMagazineListingItem> GetAll()
        {
            return _repository.GetAll();
        }

        public IList<JhmMagazineListingItem> GetAllActive()
        {
            return _repository.GetAllActive();
        }

        public JhmMagazineListingItem GetById(Guid id)
        {
            return _repository.GetById(id);
        }

        public void SaveChanges(JhmMagazineListingItem entity)
        {
            _repository.SaveChanges(entity);
        }

        public void Delete(Guid id)
        {
            _repository.Delete(id);
        }

        #endregion
    }
}
