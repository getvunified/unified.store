﻿using Jhm.CommerceIntegration;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface ICommerceService
    {
        IPaymentService BlueFinPaymentService { get; }
    }
}