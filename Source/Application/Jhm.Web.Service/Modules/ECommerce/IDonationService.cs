﻿using System.Collections.Generic;
using Jhm.Common;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IDonationService : IService
    {
        IEnumerable<Donation> GetActiveDonationOpportunities();
        IDonation GetDonation(string code);
        IEnumerable<Donation> GetActiveDonationOpportunities(Company company);
        IDonation GetDonation(Company company, string donationCode);
    }
}