using System.Security.Principal;
using Jhm.Common;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface ICartService : IService
    {
        Cart GetCart(IPrincipal userName);
        void SaveCart(Cart cart);
        long SaveTransaction(string dsEnvironment, CheckoutOrderReviewViewModel checkoutData);

        DiscountCode GetDiscountByCodeName(string codeName);
    }
}