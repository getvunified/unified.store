﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Repository.Modules.Account;
using Jhm.Web.Repository.Modules.ECommerce;
using StructureMap;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class DiscountCodeService : IDiscountCodeService
    {
        private readonly IDiscountCodeRepository _repository;
        private readonly IDiscountCodeRequirementsRepository _requirementsRepository;
        private readonly IDiscountCodeOptionalCriteriaRepository _optionalCriteraRepository;
        private readonly ICatalogService CatalogService;

        //internal ICatalogService CatalogService
        //{
        //    get { return DependencyResolver.GetInstance<CatalogService>(); }
        //}
        
        /**
         * Adds itmes related to the discount code to the cart.
         * 
         * This function adds the items that are tied to a discount
         * code and that are marked as a free item, to the cart.
         * 
         * @param Client $client, DiscountCode $discountCode, List<DiscountCodeItemsLogic> $groups
         *   client is the client information from the checkout process.
         *   discountCode is the discountCode this is being attempted to be applied.
         *   groups is a list of DiscountCodeItemLogic that is used to determine if
         *   there are any free items associated with the discount code.
         */

        public DiscountCodeService()
        {
            CatalogService = ObjectFactory.GetInstance<ICatalogService>();
            _repository = ObjectFactory.GetInstance<IDiscountCodeRepository>();
        }

        

        private void AddDiscountCodeItemsToCart(Client client, DiscountCode discountCode, List<DiscountCodeItemLogic> groups )
        {
            var groupItems = groups.SelectMany(x => x.ItemLogic.GroupItems);
            var discountCodeItemses = groupItems as DiscountCodeItems[] ?? groupItems.ToArray();
            var passDiscountItemViewModel = new List<DiscountItemViewModel>();
            
            var products = GetFreeProductsTiedToDiscountCode(discountCode, client, groups);

            if (products != null)
            {
                var discountItemViewModel = new DiscountItemViewModel();

                foreach (var product in products)
                {
                    var codeItem = discountCodeItemses.SingleOrDefault(x => x.ProductCode == product.Code);

                    discountItemViewModel.MinimumPurchaseAmount = discountCode.ActiveRule.MinPurchaseAmount;
                    discountItemViewModel.Product = product;
                    if (codeItem != null)
                    {
                        discountItemViewModel.Quantity = codeItem.Quantity;
                        discountItemViewModel.ProductWarehouse = codeItem.ProductWarehouse;
                    }

                    passDiscountItemViewModel.Add(discountItemViewModel);

                }

                client.Cart.DiscountItems = passDiscountItemViewModel;
            }
        }

        public bool ApplyDiscountCode(DiscountCode discountCode, Client client, string faultCode)
        {
            var returnType = false;

            if (discountCode != null)
            {
                bool nextStep;
                if (discountCode.ActiveRule.IsSingleUsePrefix)
                {
                    var singleUseCodes = discountCode.SingleUseDiscountCodes.ToList();
                    var code = singleUseCodes.Find(x => x.DiscountCodeId == discountCode.Id);
                    nextStep = ValidateSingleUseDiscountCodes(code);
                }
                else
                {
                    nextStep = true;
                }

                if (nextStep)
                {
                    var discountTotal = CalculateDiscountTotal(discountCode, client);
                    if (client.Cart.DiscountApplied != null && client.Cart.DiscountApplied.Id != discountCode.Id && client.Cart.DiscountTotal > discountTotal)
                    {
                        client.Cart.SetDiscountErrorMessage(
                            "The discount (" + discountCode.ShortDescription + ") was not applied because you get a greater discount with the existing discount in your cart.");
                        discountCode.ActiveRule.IsDirty = false;
                    }
                    else
                    {
                        client.Cart.SetDiscountTotal(discountTotal);
                        client.Cart.SetDiscountApplied(discountCode);

                        discountCode.ActiveRule.IsDirty = false;
                        returnType = true;
                    }
                }
            }
            else
            {
                client.Cart.SetDiscountErrorMessage(faultCode);
                client.Cart.SetDiscountApplied(null);
            }

            return returnType;
        }

        private decimal CalculateDiscountTotal(DiscountCode discountCode, Client client)
        {
            decimal discountTotal;
            // Gets the subtotal of items in the cart to which this discount can be applied (Stock Items Only)
            var productsSubTotal = GetApplicableItemsSubtotalFromCart(discountCode, client);
            if (discountCode.ActiveRule.IsFixedAmount)
            {
                // If the amount of this discount is greater than the stock items subtotal. Cuts the discount amount off to be the stock items subtotal
                discountTotal = productsSubTotal >= discountCode.ActiveRule.AmountOff? discountCode.ActiveRule.AmountOff : productsSubTotal;
            }
            else
            {
                discountTotal = (productsSubTotal / 100) * discountCode.ActiveRule.AmountOff;
            }

            if (discountCode.ActiveRule.MaxAmountOff > 0 && discountTotal > discountCode.ActiveRule.MaxAmountOff)
            {
                discountTotal = discountCode.ActiveRule.MaxAmountOff;
            }

            return discountTotal;
        }

        private decimal GetApplicableItemsSubtotalFromCart(DiscountCode discountCode, Client client)
        {
            var subtotal = 0m;

            foreach (var cartLineItem in client.Cart.Items)
            {
                var item = cartLineItem.Item;
                if (!(item is StockItem) || (discountCode.ActiveRule.AppliesOnlyToItemsMatchingCriteria && !cartLineItem.MatchesDiscountCriteria)) continue;
                var product = CatalogService.GetProductFromItemCode(client.Company.DonorStudioEnvironment, item.Code, true);

                var canApplyToStockItem = true;
                if (!discountCode.AllowOnSaleItems)
                {
                    var stockItem = product.StockItems.FirstOrDefault(x => x.Code == item.Code);
                    if (stockItem != null && stockItem.IsOnSale())
                    {
                        canApplyToStockItem = false;
                    }
                }

                if (product.DiscountAllowed && canApplyToStockItem)
                {
                    subtotal += (item.Price*cartLineItem.Quantity);
                }
            }

            return subtotal;
        }

        

        public IList<DiscountCode> GetAllAutoApplyCodes()
        {
            return _repository.GetAllAutoApplyCodes();
        }

        public IList<DiscountCode> GetAllDiscountCodes()
        {
            return _repository.GetAllDiscountCodes();
        }

        public DiscountCodeRequirements GetAllDiscountCodeRequirements(DiscountCode discountCode)
        {
            return _requirementsRepository.GetAllDiscountCodeRequirements(discountCode);
        }

        public List<DiscountCodeOptionalCriteria> GetAllOptionalCriteria(DiscountCode discountCode)
        {
            return _optionalCriteraRepository.GetItemsRelatedToDiscountCode(discountCode);
        }


        /**
         * Returns a list of products.
         * 
         * This function returns a list of free products, if any,
         * that are tied to a discount code.
         * 
         * @param DiscountCode $discountCode, Client $client, List<DiscountCodeItemsLogic> $groups
         *   client is the client information from the checkout process.
         *   discountCode is the discountCode this is being attempted to be applied.
         *   groups is a list of DiscountCodeItemLogic that is used to determine if
         *   there are any free items associated with the discount code.
         */
        private List<IProduct> GetFreeProductsTiedToDiscountCode(DiscountCode discountCode, Client client, List<DiscountCodeItemLogic> groups)
        {
            var productList = new List<IProduct>();

            var discountCodeProductList = _repository.GetAllAssociatedProducts(discountCode);

            if (discountCodeProductList.Any(x => x.IsFreeItem))
            {
                var satisfiedList = groups.Where(p => p.GroupIsSatisfied);
                var discountCodeItemLogics = satisfiedList as IList<DiscountCodeItemLogic> ?? satisfiedList.ToList();
                var catalogService = CatalogService;

                var selectFreeProductList = discountCodeProductList.Where(p => p.IsFreeItem).Select(q => q.ProductCode.ToString(CultureInfo.InvariantCulture));
                List<string> freeProductList = null;


                if (discountCodeItemLogics.Any(k => k.ItemLogic.ProductAndItemsAreSatisfied))
                {
                    var andProductList = discountCodeItemLogics.SelectMany(r => r.ItemLogic.ProductAndItems);
                    freeProductList = selectFreeProductList.Intersect(andProductList).ToList();
                }
                else if (discountCodeItemLogics.Any(k => k.ItemLogic.ProductOrItemsAreSatisfied))
                {
                    var orProductList = discountCodeItemLogics.SelectMany(r => r.ItemLogic.ProductOrItems);
                    freeProductList = selectFreeProductList.Intersect(orProductList).ToList();
                }
                else
                {
                    var andProductList = discountCodeItemLogics.SelectMany(r => r.ItemLogic.ProductAndItems);
                    var orProductList = discountCodeItemLogics.SelectMany(r => r.ItemLogic.ProductOrItems);

                    if (andProductList.FirstOrDefault() != null)
                    {
                        freeProductList = selectFreeProductList.Intersect(andProductList).ToList();
                    }
                    else if (orProductList.FirstOrDefault() != null)
                    {
                        freeProductList = selectFreeProductList.Intersect(orProductList).ToList();
                    }
                }

                if (freeProductList != null)
                {
                    productList.AddRange(
                        freeProductList.Select(retrieveItem =>
                                               catalogService.GetProduct(client.Company.DonorStudioEnvironment,
                                                                         retrieveItem)));
                }
            }
            else
            {
                productList = null;
            }

            return productList;
        }

        public void MarkSingleUseDiscountCodeAsUsed(SingleUseDiscountCodes code, DiscountCode discountCode, string userName)
        {
            code.DiscountCode = discountCode;
            code.IsActive = false;
            code.UserName = userName;
            code.DateUsed = DateTime.Now;

            var discountCodeIndex = discountCode.SingleUseDiscountCodes.IndexOf(code);
            discountCode.SingleUseDiscountCodes[discountCodeIndex] = code;

            _repository.Save(discountCode);
        }

        private List<DiscountCodeItemLogic> PopulateItemsForAnalysis(DiscountCode discountCode)
        {
            var discountCodeProductList = _repository.GetAllAssociatedProducts(discountCode);
            var groups = new List<DiscountCodeItemLogic>();

            foreach (var discountCodeItems in discountCodeProductList.Select(y => y.GroupId).Distinct())
            {

                Guid discountCodeItemsGuid = discountCodeItems;
                groups.Add(
                    new DiscountCodeItemLogic
                    {
                        GroupId = discountCodeItems,
                        ItemLogic = new ProductLogic
                        {
                            GroupItems = discountCodeProductList.Where(x => x.GroupId == discountCodeItemsGuid).ToList(),
                            ProductAndItems = discountCodeProductList.Where(x => x.GroupId == discountCodeItemsGuid && x.ProductAnd).Select(items => items.ProductCode).ToList(),
                            ProductOrItems = discountCodeProductList.Where(x => x.GroupId == discountCodeItemsGuid && !x.ProductAnd).Select(items => items.ProductCode).ToList(),
                            ProductAndItemsAreSatisfied = false,
                            ProductOrItemsAreSatisfied = false,
                        },
                        IsAndGroup = discountCodeProductList.Where(x => x.GroupId == discountCodeItems).Select(g => g.GroupAnd).Distinct().SingleOrDefault(),
                        GroupIsSatisfied = false,
                    });
            }

            return groups;
        }

        private bool RunLogic(ICollection<string> cartItems, IEnumerable<string> check, bool andOr)
        {
            var returnValue = false;

            if (andOr)
            {
                //And
                foreach (var discountItem in check)
                {
                    returnValue = cartItems.Contains(discountItem);
                    if (!returnValue)
                    {
                        break;
                    }
                }
            }
            else
            {
                //Or
                foreach (var discountItem in check)
                {
                    returnValue = cartItems.Contains(discountItem);
                    if (returnValue)
                    {
                        break;
                    }
                }
            }

            return returnValue;
        }

        // Added by Alex 2/26/2013
        public void RecalculateAppliedDiscount(Client client)
        {
            foreach (var cartLineItem in client.Cart.Items)
            {
                cartLineItem.MatchesDiscountCriteria = false;
            }
            if (client.Cart.DiscountApplied == null) return;
            string faultCode;
            var discountCode = ValidateDiscountCode(client.Cart.DiscountApplied.CodeName, client, out faultCode);
            if (discountCode == null)
            {
                faultCode = "Sorry, The discount was removed because the items in your cart do not satisfy the discount code requirements.";
            }
            ApplyDiscountCode(discountCode, client, faultCode);
        }

        public DiscountCode GetDiscountCode(Guid id)
        {
            return _repository.Get(id);
        }

        public void IncreaseUseCount(Guid discountCodeId)
        {
            _repository.IncreaseUseCount(discountCodeId);
        }

        private bool ValidateDiscountAsViable(DiscountCode discountCode, Client client, out string faultCode)
        {
            faultCode = "";
            var returnValue = false;
            var companyIsApplicable = false;
            var discountCodeExists = false;
            var discountCodeMeetsTimeContraints = false;
            var minimumPurchaseRequirement = false;

            if (discountCode != null)
            {
                discountCodeExists = true;
                if (discountCode.ActiveRule.CompanyIsApplicable(client.Company))
                {
                    companyIsApplicable = true;
                    discountCodeMeetsTimeContraints = ValidateDiscountCodeDate(discountCode, out faultCode);
                    minimumPurchaseRequirement = ValidateMinimumPurchaseRequirement(discountCode, client, out faultCode);
                }
            }

            if (discountCodeExists && companyIsApplicable && discountCodeMeetsTimeContraints && minimumPurchaseRequirement)
            {
                returnValue = true;
            }

            return returnValue;
        }

        public void CartUpdated(bool productsSucceeded, DiscountCode discount, Client client)
        {
            var faultCode = "";
            client.Cart.SetDiscountErrorMessage("");
            if (!productsSucceeded)
            {
                ValidateDiscountCode(discount, client, out faultCode);
            }
        }

        public DiscountCode ValidateDiscountCode(string codeName, Client client, out string faultCode)
        {
            client.Cart.SetDiscountErrorMessage("");
            bool successful = false;
            bool productsSatisfied = false;

            var discountCode = _repository.GetDiscountCodeByName(codeName);

            if (ValidateDiscountAsViable(discountCode, client, out faultCode))
            {
                bool nextStep = false;
                if (discountCode.ActiveRule.IsSingleUsePrefix)
                {
                    var singleUseCodes = discountCode.SingleUseDiscountCodes.ToList();
                    var code = singleUseCodes.Find(x => x.DiscountCodeId == discountCode.Id);
                    
                    if (code.DateUsed < DateTime.Parse("01/01/1753"))
                    {
                        code.DateUsed = DateTime.Parse("01/01/1753");
                    }
                    
                        nextStep = ValidateSingleUseDiscountCodes(code);
                }
                else
                {
                    nextStep = true;
                }

                if (discountCode.ActiveRule.IsTiedToProducts && nextStep)
                {
                    var groups = PopulateItemsForAnalysis(discountCode);
                    
                    if (VerifyProductsTiedToDiscountCode(client.Cart, groups))
                    {
                        AddDiscountCodeItemsToCart(client, discountCode, groups);
                        productsSatisfied = true;
                    }
                    else
                    {
                        faultCode = "Sorry, but the items in your cart do not satisfy the discount code requirements.";
                    }

                    if (!client.Cart.IsCartUpdatedSet())
                    {
                        DiscountCode code = discountCode;
                        client.Cart.CartUpdated +=
                            (sender, args) =>
                                {
                                    if (args.CommandName == "RemoveItem")
                                    {
                                        CartUpdated(VerifyProductsTiedToDiscountCode(client.Cart, groups),
                                                    code, client);
                                    }
                                };
                    }

                    successful = productsSatisfied;
                    nextStep = productsSatisfied;
                }
                
                if (nextStep)
                {
					successful = ValidateOptionalCritera(discountCode, client);
                    if (successful)
                    {
                        if (discountCode.ActiveRule.Quantity > 1)
                        {
                            var quantitySumOfMatchingItems =
                                client.Cart.Items.Where(x => x.MatchesDiscountCriteria).Sum(x => x.Quantity);
                            if (quantitySumOfMatchingItems < discountCode.ActiveRule.Quantity)
                            {
                                foreach (var cartLineItem in client.Cart.Items)
                                {
                                    cartLineItem.MatchesDiscountCriteria = false;
                                }
                                faultCode = "Sorry, but the quantity of items in your cart do not satisfy the discount code requirements.";
								successful = false;
                            }
                        }

                        if (successful)
                        {
                            successful = ValidateItemsOnSaleConstraint(discountCode, client);
                        }
                    }
                    else
                    {
                        faultCode = "Sorry, but the items in your cart do not satisfy the discount code requirements.";
                    }
                }
                else
                {
                    faultCode = "001: No discount was found with the provided discount code";
                }
            }
            else if (String.IsNullOrWhiteSpace(faultCode))
            {
                faultCode = "002: No discount was found with the provided discount code";
            }

            client.Cart.SetDiscountErrorMessage(faultCode);

            if (!successful)
            {
                discountCode = null;
                client.Cart.SetDiscountApplied(discountCode);
            }

            return discountCode;
        }

        public DiscountCode ValidateDiscountCode(DiscountCode discountCode, Client client, out string faultCode)
        {
            faultCode = "";
            client.Cart.SetDiscountErrorMessage("");
            bool successful = false;
            bool productsSatisfied = false;

            if (ValidateDiscountAsViable(discountCode, client, out faultCode))
            {
                bool nextStep = true;
                if (discountCode.ActiveRule.IsSingleUsePrefix)
                {
                    var singleUseCodes = discountCode.SingleUseDiscountCodes.ToList();
                    var code = singleUseCodes.Find(x => x.DiscountCodeId == discountCode.Id);
                    nextStep = ValidateSingleUseDiscountCodes(code);
                }
                
                if (discountCode.ActiveRule.IsTiedToProducts && nextStep)
                {
                    var groups = PopulateItemsForAnalysis(discountCode);

                    if (VerifyProductsTiedToDiscountCode(client.Cart, groups))
                    {
                        AddDiscountCodeItemsToCart(client, discountCode, groups);
                        productsSatisfied = true;
                    }
                    else
                    {
                        faultCode = "Sorry, but the items in your cart do not satisfy the discount code requirements.";
                    }

                    successful = productsSatisfied;
                    nextStep = productsSatisfied;
                }

                if (nextStep)
                {
                    successful = nextStep = ValidateOptionalCritera(discountCode, client);                    
                }

                if (nextStep)
                {
                    successful = ValidateItemsOnSaleConstraint(discountCode, client);
                }
            }
            else
            {
                faultCode = "No discount was found with the provided discount code";
            }

            if (!discountCode.ActiveRule.IsAutoApply)
            {
                client.Cart.SetDiscountErrorMessage(faultCode);
            }

            if (!successful)
            {
                if (!discountCode.ActiveRule.IsAutoApply)
                {
                    client.Cart.SetDiscountApplied(discountCode);
                }
                return null;                
            }

            return discountCode;
        }

        private bool ValidateItemsOnSaleConstraint(DiscountCode discountCode, Client client)
        {
            if (discountCode.AllowOnSaleItems)
            {
                return true;
            }

            var atLeastOneItemQualifies = false;
            foreach (var cartLineItem in client.Cart.Items)
            {
                var item = cartLineItem.Item;
                if (!(item is StockItem)) continue;
                var product = CatalogService.GetProductFromItemCode(client.Company.DonorStudioEnvironment, item.Code, true);

                var stockItem = product.StockItems.FirstOrDefault(x => x.Code == item.Code);
                var canApplyToStockItem = stockItem != null && !stockItem.IsOnSale();

                if (product.DiscountAllowed && canApplyToStockItem)
                {
                    cartLineItem.MatchesDiscountCriteria = true;
                    atLeastOneItemQualifies = true;
                }
            }

            return atLeastOneItemQualifies;
        }

        private bool ValidateDiscountCodeDate(DiscountCode discountCode, out string faultCode)
        {
            faultCode = "";
            bool returnValue = false;
            var now = DateTime.Now;

            if (discountCode.ActiveRule.StartDate <= now && discountCode.ActiveRule.EndDate >= now)
            {
                returnValue = true;
            }
            else
            {
                faultCode = "No discount was found with the provided discount code";
            }

            return returnValue;
        }
        
        private bool ValidateMinimumPurchaseRequirement(DiscountCode discountCode, Client client, out string faultCode)
        {
            faultCode = "";

            //if (!discountCode.AllowOnSaleItems && GetApplicableItemsSubtotalFromCart(discountCode, client) >= discountCode.ActiveRule.MinPurchaseAmount)
            //{
            //    return true; 
            //}
            if (client.Cart.GetSubTotalByItemType(typeof(StockItem)) >= discountCode.ActiveRule.MinPurchaseAmount)
            {
                return true; 
            }
            else
            {
                faultCode = String.Format("Sorry, this discount can only be applied to purchases of [C:{0}] or more", discountCode.ActiveRule.MinPurchaseAmount);
            }
            
            return false;
        }

        private bool ValidateDiscountCodeItemRequirements(DiscountCodeItems discountCodeItems, Cart cart, out string faultCode)
        {
            faultCode = "";

            bool returnValue = false;

            if (cart.GetSubTotalByItemType(typeof(StockItem)) >= discountCodeItems.MinPurchaseAmount)
            {
                returnValue = true;
            }
            else
            {
                faultCode = String.Format("Sorry, this discount can only be applied to purchases of [C:{0}] or more", discountCodeItems.MinPurchaseAmount);
            }

            return returnValue;
        }
        
        private bool ValidateSingleUseDiscountCodes(ISingleUseDiscountCodes discountCode)
        {
            bool returnType = (discountCode.IsActive && discountCode.UserName == null);

            return returnType;
        }

        private bool ValidateOptionalCritera(IDiscountCode discountCode, Client client)
        {
            var optionalCriteria = discountCode.OptionalCriteria.ToList();
            // If there are no optional criteria specified
            if (!optionalCriteria.Any()) return true;

            var cart = client.Cart;
            
            var productCriteria = optionalCriteria.Where(x => x.ApplicableClass == (short) DiscountCodeOptionalCriteria.ProductOrStock.Product).ToList();

            var stockItemCriteria = optionalCriteria.Where(x => x.ApplicableClass == (short)DiscountCodeOptionalCriteria.ProductOrStock.StockItem).ToList();

            List<IProduct> products;
            List<StockItem> stockLoop;
            if (discountCode.ActiveRule.AppliesOnlyToItemsMatchingCriteria)
            {
                products = cart.Items.Select(cartItem =>
                    {
                        var product = CatalogService.GetProductFromItemCode(client.Company.DonorStudioEnvironment,
                                                                            cartItem.Item.Code, false);
                        product.CartLineItem = cartItem;
                        return product;
                    }).ToList();

                stockLoop = products.SelectMany(product => product.StockItems.Select(stock =>
                    {
                        stock.CartLineItem = product.CartLineItem;
                        return stock;
                    })).ToList();
            }
            else
            {
                products = cart.Items.Select(cartItem => CatalogService.GetProductFromItemCode(client.Company.DonorStudioEnvironment, cartItem.Item.Code, false)).ToList();

                stockLoop = products.SelectMany(x => x.StockItems).ToList();
            }

            #region Mandatory
            if (optionalCriteria.Any(x => x.IsMandatory))
            {
                var mandatoryCriteria = productCriteria.Where(c => c.IsMandatory).ToList();
                var mandatoryProductCriteriaIsValid = true;
                if (mandatoryCriteria.Any()) mandatoryProductCriteriaIsValid = ValidateAllCriteriaListFromAnyOfItems(products, typeof(Product), mandatoryCriteria);
                mandatoryCriteria = stockItemCriteria.Where(c => c.IsMandatory).ToList();
                var mandatoryStockItemCriteriaIsValid = true;
                if (mandatoryCriteria.Any()) mandatoryStockItemCriteriaIsValid = ValidateAllCriteriaListFromAnyOfItems(stockLoop, typeof(StockItem), mandatoryCriteria);
            
                return mandatoryProductCriteriaIsValid && mandatoryStockItemCriteriaIsValid;
            }
            #endregion


            #region Optional
            var optionalProductCriteriaIsValid = ValidateAnyCriteriaFromAnyOfItems(products, typeof(Product), productCriteria.Where(c => !c.IsMandatory).ToList());
            var optionalStockItemCriteriaIsValid = ValidateAnyCriteriaFromAnyOfItems(stockLoop, typeof(StockItem), stockItemCriteria.Where(c => !c.IsMandatory).ToList());
            #endregion

            return optionalProductCriteriaIsValid || optionalStockItemCriteriaIsValid;
        }

        /// <summary>
        /// Evaluates if any of the items in the cart satisfy all the criteria list
        /// By Alex Payares
        /// </summary>
        /// <param name="items"></param>
        /// <param name="type"></param>
        /// <param name="criteriaList"></param>
        /// <returns>true if one of the items satisfies all the criteria in the list</returns>
        private bool ValidateAllCriteriaListFromAnyOfItems(IEnumerable<object> items, Type type, List<DiscountCodeOptionalCriteria> criteriaList)
        {
            var itemProperties = type.GetProperties();

            var oneOfTheItemsSatifiesAllCriteria = false;

            //for each item in the cart
            foreach (var item in items)
            {
                // We stablished that this item matches all the criteria
                var matchesAllCriteria = true;

                //for each criteria in this discount code
                foreach (var criteria in criteriaList)
                {
                    // Get the property indicated by the criteria
                    var propInfo = itemProperties.SingleOrDefault(x => x.Name == criteria.Criteria);

                    if (propInfo == null)
                    {
                        throw new Exception(String.Format("The property '{0}' indicated by the criteria '{1}' was not found in the type {2}",criteria.Criteria, criteria.Id, type.FullName));
                    }

                    //If the property indicated by the criteria is a collection
                    if (criteria.ValueDataType == DiscountCodeOptionalCriteria.SupportedDataTypes.List.ToString() && propInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        // We stablished that no items in the collection match the criteria value
                        var matchesAnyValue = false;
                        //loop through the items in the collection property to find a match for the indicated criteria value
                        foreach (var propValue in (IEnumerable)propInfo.GetValue(item, null))
                        {
                            if (propValue.ToString().Equals(criteria.Value, StringComparison.InvariantCultureIgnoreCase))
                            {
                                //An item in the collection matches the criteria value... we are good so far
                                matchesAnyValue = true;
                                // no need to keep looping through the collection property
                                break;
                            }
                        }
                        // If none of the items in the collection matched the criteria value then we can be sure
                        // that this cart item does not match all the criteria
                        if (!matchesAnyValue)
                        {
                            // No need to keep looping through the list of criterias
                            matchesAllCriteria = false;
                            break;
                        }
                    }
                    else
                    {
                        //Gets the value of the property indicated by the criteria
                        var propValue = (propInfo.GetValue(item) ?? new object()).ToString();

                        if (!criteria.Value.Equals(propValue, StringComparison.InvariantCultureIgnoreCase))
                        {
                            // If the value does not match then there is no need to keep looping through the list of mandatory criterias
                            matchesAllCriteria = false;
                            break;
                        }
                    }
                }

                if (matchesAllCriteria)
                {
                    //We found an item that matches all the criteria
                    oneOfTheItemsSatifiesAllCriteria = true;
                    var cartItemLink = item as ILinkableToCartItem;
                    if (cartItemLink != null && cartItemLink.CartLineItem != null)
                    {
                        cartItemLink.CartLineItem.MatchesDiscountCriteria = true;
                    }
                }
                else
                {
                    var cartItemLink = item as ILinkableToCartItem;
                    if (cartItemLink != null && cartItemLink.CartLineItem != null)
                    {
                        cartItemLink.CartLineItem.MatchesDiscountCriteria = false;
                    }
                }
            }
            return oneOfTheItemsSatifiesAllCriteria;
        }


        /// <summary>
        /// Evaluates if any of the items in the cart satisfy any of the criteria list
        /// By Alex Payares
        /// </summary>
        /// <param name="items"></param>
        /// <param name="type"></param>
        /// <param name="criteriaList"></param>
        /// <returns>true if any of the items satisfies any of the criteria in the list</returns>
        private bool ValidateAnyCriteriaFromAnyOfItems(IEnumerable<object> items, Type type, List<DiscountCodeOptionalCriteria> criteriaList)
        {
            var itemProperties = type.GetProperties();

            var optionalCriteria = criteriaList;

            var oneItemMatchesAnyCriteria = false;

            //for each item in the cart
            foreach (var item in items)
            {
                var itemMatchesCriteria = false;
                //for each criteria in the criteria list
                foreach (var criteria in optionalCriteria)
                {
                    // Get the property indicated by the criteria
                    var propInfo = itemProperties.SingleOrDefault(x => x.Name == criteria.Criteria);

                    if (propInfo == null)
                    {
                        throw new Exception(String.Format("The property '{0}' indicated by the criteria '{1}' was not found in the type {2}", criteria.Criteria, criteria.Id, type.FullName));
                    }

                    //If the property indicated by the criteria is a collection
                    if (criteria.ValueDataType == DiscountCodeOptionalCriteria.SupportedDataTypes.List.ToString() && propInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    {
                        //loop through the items in the collection property to find a match for the indicated criteria value
                        foreach (var propValue in (IEnumerable)propInfo.GetValue(item, null))
                        {
                            if (propValue.ToString().Equals(criteria.Value, StringComparison.InvariantCultureIgnoreCase))
                            {
                                //An item in the collection matches the criteria value... we are good to go
                                itemMatchesCriteria = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        //Gets the value of the property indicated by the criteria
                        var propValue = (propInfo.GetValue(item) ?? new object()).ToString();

                        if (criteria.Value.Equals(propValue, StringComparison.InvariantCultureIgnoreCase))
                        {
                            //This cart item matches any of the criteria in the list
                            itemMatchesCriteria = true;
                        }
                    }

                    if (itemMatchesCriteria)
                    {
                        break;
                    }
                }
                if (!itemMatchesCriteria) continue;

                oneItemMatchesAnyCriteria = true;
                var cartItemLink = item as ILinkableToCartItem;
                if (cartItemLink != null && cartItemLink.CartLineItem != null)
                {
                    cartItemLink.CartLineItem.MatchesDiscountCriteria = true;
                }
            }
            return oneItemMatchesAnyCriteria;
        }


        

        private bool VerifyProductsTiedToDiscountCode(Cart cart, List<DiscountCodeItemLogic> groups)
        {
            bool allSatisfied = false;

            var cartItemList = cart.Items.Select(x => x.Item.Code).ToList();

            foreach (var discountCodeItemLogicGroup in groups)
            {
                bool andSatisfied;
                var orSatisfied = discountCodeItemLogicGroup.ItemLogic.ProductOrItemsAreSatisfied;
                
                if (discountCodeItemLogicGroup.ItemLogic.ProductAndItems.Any())
                {
                    andSatisfied = RunLogic(cartItemList, discountCodeItemLogicGroup.ItemLogic.ProductAndItems, true);

                    if (!andSatisfied && discountCodeItemLogicGroup.ItemLogic.ProductOrItems.Any())
                    {
                        orSatisfied = RunLogic(cartItemList, discountCodeItemLogicGroup.ItemLogic.ProductOrItems, false);
                    }

                    if (andSatisfied || orSatisfied)
                    {
                        discountCodeItemLogicGroup.GroupIsSatisfied = true;
                    }
                    else
                    {
                        discountCodeItemLogicGroup.GroupIsSatisfied = false;
                    }

                    discountCodeItemLogicGroup.ItemLogic.ProductAndItemsAreSatisfied = andSatisfied;
                    discountCodeItemLogicGroup.ItemLogic.ProductOrItemsAreSatisfied = orSatisfied;
                }
                else
                {
                    orSatisfied = RunLogic(cartItemList, discountCodeItemLogicGroup.ItemLogic.ProductOrItems, false);

                    discountCodeItemLogicGroup.GroupIsSatisfied = orSatisfied;

                    discountCodeItemLogicGroup.ItemLogic.ProductOrItemsAreSatisfied = orSatisfied;
                }

				if (groups.Any(y => y.IsAndGroup) && groups.Where(y => y.IsAndGroup).All(x => x.GroupIsSatisfied))
                {
                    if (discountCodeItemLogicGroup.ItemLogic.GroupItems.Select(x => x.IsFreeItem).FirstOrDefault())
                    {
                        allSatisfied = true;
                        discountCodeItemLogicGroup.GroupIsSatisfied = true;

                    }
                    break;
                }
            }

			bool isFreeItem = groups.SingleOrDefault().ItemLogic.GroupItems.Any(x => x.IsFreeItem);

            if (groups.Any(y => y.IsAndGroup)) 
            {
                if (groups.TrueForAll(x => x.GroupIsSatisfied))
                {
                    allSatisfied = true;
                }
            }
            else if (groups.Where(y=> !y.IsAndGroup).Any(x => x.GroupIsSatisfied))
            {
                allSatisfied = true;
            }
            else if (isFreeItem)
            {
                allSatisfied = true;
            }
            
            return allSatisfied;
        }

        public void ApplyAutoApplyCodes(Client client)
        {
            var autoApplyDiscountCodes = GetAllAutoApplyCodes().Where(x => x.ActiveRule.EndDate >= DateTime.Now && x.ActiveRule.StartDate <= DateTime.Now).ToList();
            
            if (!autoApplyDiscountCodes.Any())
            {
                return;
            }

            var faultCode = "";

            var winnersFromGroup = autoApplyDiscountCodes.Where(code => ValidateDiscountCode(code, client, out faultCode) != null).ToList();

            DiscountCode winner = client.Cart.DiscountApplied;
            var bestDeal = 0m;

            foreach (var candidate in winnersFromGroup)
            {
                var discountTotal = CalculateDiscountTotal(candidate, client);
                if (discountTotal >= bestDeal)
                {
                    winner = candidate;
                    bestDeal = discountTotal;
                }
            }

            if (winner != null)
            {
                ApplyDiscountCode(winner, client, faultCode);
            }
            else
            {
                client.Cart.SetDiscountErrorMessage(client.Cart.DiscountApplied != null ? "Sorry, The discount was removed because the items in your cart do not satisfy the discount code requirements." : String.Empty);
                client.Cart.SetDiscountApplied(null);
            }            
        }
    }


    internal class DiscountCodeItemLogic
    {
        public virtual Guid GroupId { get; set; }
        public virtual ProductLogic ItemLogic { get; set; }
        public virtual bool IsAndGroup { get; set; }
        public virtual bool GroupIsSatisfied { get; set; }
    }

    internal class ProductLogic
    {
        public virtual List<DiscountCodeItems> GroupItems { get; set; }
        public virtual List<String> ProductAndItems { get; set; }
        public virtual List<String> ProductOrItems { get; set; }
        public virtual bool ProductAndItemsAreSatisfied { get; set; }
        public virtual bool ProductOrItemsAreSatisfied { get; set; }

    }

}
