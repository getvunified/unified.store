﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IPromotionalItemService
    {
        IEnumerable<PromotionalItem> GetAll();
        IEnumerable<PromotionalItem> GetAllActive();
    }
}
