using System.Security.Principal;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Repository.Modules.Account;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _repository;

        public CartService(ICartRepository repository)
        {
            _repository = repository;
        }

        public Cart GetCart(IPrincipal userName)
        {
            
            return _repository.GetCartByUserName(userName.Identity.Name);
        }

        public void SaveCart(Cart cart)
        {
            _repository.Save(cart);
        }

        public long SaveTransaction(string dsEnvironment, CheckoutOrderReviewViewModel checkoutData)
        {
            return _repository.SaveTransaction(dsEnvironment, checkoutData);
        }

        public DiscountCode GetDiscountByCodeName(string codeName)
        {
            return _repository.GetDiscountByCodeName(codeName);
        }
    }
}