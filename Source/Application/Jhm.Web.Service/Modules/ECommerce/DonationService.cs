using System.Collections.Generic;
using System.ComponentModel;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class DonationService : IDonationService
    {
        private readonly IDonationRepository _repository;

        public DonationService(IDonationRepository repository)
        {
            _repository = repository;
            //_repository = new SoHDonationRepository();
        }

        public IEnumerable<Donation> GetActiveDonationOpportunities()
        {
            return GetActiveDonationOpportunities(Company.JHM_UnitedStates);
        }

        public IDonation GetDonation(string code)
        {
            return _repository.GetDonation(code);
        }

        public IEnumerable<Donation> GetActiveDonationOpportunities(Company company)
        {
            IEnumerable<Donation> donations = new BindingList<Donation>();
      //      donations = new SoHDonationRepository().GetActiveDonationOpportunities();

            if (company == null || company == Company.JHM_UnitedStates)
            {
                donations = _repository.GetActiveDonationOpportunities();
            }
            else if (company == Company.JHM_Canada)
            {
                donations = _repository.GetActiveCanadaDonationOpportunities();
            }
            else if (company == Company.JHM_UnitedKingdom)
            {
                donations = _repository.GetActiveUKDonationOpportunities();
            }

            return new List<Donation>(donations).FindAll(d => !d.IsHidden);
        }

        public IDonation GetDonation(Company company, string donationCode)
        {
            if (company == null || company == Company.JHM_UnitedStates)
            {
                return _repository.GetUSDonation(donationCode);
            }

            if (company == Company.JHM_Canada)
            {
                return _repository.GetCanadaDonation(donationCode);
            }

            if (company == Company.JHM_UnitedKingdom)
            {
                return _repository.GetUKDonation(donationCode);
            }

            var donation = _repository.GetUSDonation(donationCode);

            return donation;
        }
    }
}