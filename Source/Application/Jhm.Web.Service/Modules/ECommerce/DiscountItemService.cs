﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class DiscountItemService : IDiscountItemService
    {
        private readonly IDiscountItemRepository repository;

        public DiscountItemService(IDiscountItemRepository repository)
        {
            this.repository = repository;
        }

        public List<DiscountCodeItems> GetItemsRelatedToDiscountCode(DiscountCode discountCode)
        {
            return repository.GetItemsRelatedToDiscountCode(discountCode);
        }
    }
}
