﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class SubscriptionService: ISubscriptionService
    {
        private readonly ISubscriptionsRepository repository;

        public SubscriptionService(ISubscriptionsRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<ISubscription> LoadAllSubscriptions(string environment, bool forceUpdateToCacheFile)
        {
            return repository.LoadAllSubscriptions(environment, forceUpdateToCacheFile);
        }

        public IEnumerable<ISubscription> UpdateSubscriptionsJson(string environment)
        {
            return repository.UpdateSubscriptionsJson(environment);
        }

        public IEnumerable<ISubscription> GetAvailableMagazineSubscriptions(string environment)
        {
            return repository.GetAvailableMagazineSubscriptions(environment);
        }

        public ISubscription GetSubscription(string environment, string subscriptionCode)
        {
            return repository.GetSubscription(environment, subscriptionCode);
        }
    }
}
