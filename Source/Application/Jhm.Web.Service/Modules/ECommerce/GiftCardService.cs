﻿using System;
using System.Collections.Generic;
using Foundation.WebApi;
using Getv.Foundation.Infrastructure;
using Getv.GiftCard.Core;
using Getv.GiftCard.Reporting;
using Getv.GiftCard.Services;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Repository.Modules.ECommerce;
using GiftCard = Jhm.Web.Core.Models.ECommerce.GiftCard;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class GiftCardService : IGiftCardService
    {
        private readonly IGiftCardRepository repository;
        private readonly IMessageDispatcher _messageDispatcher;

        public GiftCardService(IGiftCardRepository repository, IMessageDispatcher messageDispatcher)
        {
            this.repository = repository;
            _messageDispatcher = messageDispatcher;
        }

        public IEnumerable<GiftCard> GetAvailableGiftCards()
        {
            return repository.GetAvailableGiftCards();
        }

        public GiftCard GetGiftCard(Guid giftCardId)
        {
            return repository.GetGiftCard(giftCardId);
        }

        public decimal GetBalance(string number, string pin)
        {
            return _messageDispatcher.Dispatch(new GetBalance2 { Number = number }).Balance;
        }

        public void Redeem(string number, string pin, decimal amount, string globalId)
        {
            _messageDispatcher.Dispatch(new Redeem { Number = number, Pin = pin , Amount = amount, UserId = globalId});
        }

        public void Refund(string number, string pin, decimal amount, string reason, string globalId)
        {
            _messageDispatcher.Dispatch(new Refund { Number = number, Pin = pin, Amount = amount, Reason = reason, UserId = globalId });
        }

        public void LoadGiftCard(string number, string pin, decimal amount)
        {
            _messageDispatcher.Dispatch(new Load { Number = number, Amount = amount });
        }
        
    }

    public class GetBalance2 : IQuery<GiftCardBalance2>
    {
        public string Number { get; set; }
    }

    public class GiftCardBalance2
    {
        public decimal Balance { get; set; }
    }
}
