using System;
using System.Collections.Generic;
using System.Security.Principal;
using Jhm.Common;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IDigitalDownloadService : IService
    {
        DigitalDownload GetDigitalDownload(Guid id);
        void SaveDigitalDownload(DigitalDownload download);

        IEnumerable<DigitalDownload> GetAllBy(IDigitalDownloadSearchCriteria searchCriteria);
    }
}