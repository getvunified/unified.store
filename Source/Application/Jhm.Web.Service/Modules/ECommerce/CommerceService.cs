using System;
using Jhm.CommerceIntegration;

using Jhm.Web.Core.Models;
using StructureMap;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class CommerceService : ICommerceService
    {
        private readonly IContainer container;

        public CommerceService(IContainer container)
        {
            this.container = container;
        }

        public IPaymentService BlueFinPaymentService
        {
            get { return container.GetInstance<BlueFinPaymentService>(); }
        }

        
    }
}