﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class PromotionalItemService : IPromotionalItemService
    {
        private readonly IPromotionalItemRepository repository;

        public PromotionalItemService(IPromotionalItemRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<PromotionalItem> GetAll()
        {
            return repository.GetAll();
        }

        public IEnumerable<PromotionalItem> GetAllActive()
        {
            return repository.GetAllActive();
        }
    }
}
