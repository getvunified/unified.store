﻿using System;
using System.Collections.Generic;
using Jhm.Common;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface ICatalogService : IService
    {
        IEnumerable<IProduct> FindProducts(string environment, string searchString, PagingInfo pagingInfo, ref int totalRows);
        IEnumerable<IProduct> GetProducts(string categories, string subjects, string mediaType, string author, PagingInfo pagingInfo, ref int totalRows);
        IEnumerable<IProduct> GetProducts(string authors, PagingInfo pagingInfo, ref int totalRows);
        IEnumerable<IProduct> GetFeaturedItems(string environment, PagingInfo pagingInfo, ref int totalRows);
        IEnumerable<IProduct> GetAllProducts(string environment);
        IEnumerable<IProduct> GetLatestReleasedProducts(string environment, PagingInfo pagingInfo, ref int totalRows);
        IProduct GetProduct(string environment, string sku);
        StockItem GetStockItem(string environment, string productCode, MediaType mediaType);
        StockItem GetStockItemByProductId(string environment, Guid productId, MediaType mediaType);
        Company GetCompany();
        IEnumerable<IProduct> GetProductsByAuthor(string environment, string author, PagingInfo pagingInfo, ref int totalResults);
        IEnumerable<IProduct> GetProductsByAuthorOrderedBy(string environment, string author, PagingInfo pagingInfo, ref int totalResults, string orderDirection);
        IEnumerable<IProduct> GetProductsByCategory(string environment, string categories, PagingInfo pagingInfo, ref int totalResults);
        IEnumerable<IProduct> GetProductsByCategoryOrderedBy(string environment, string categories, PagingInfo pagingInfo, ref int totalResults, string orderDirection);
        IEnumerable<IProduct> GetProductsBySubject(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults);
        IEnumerable<IProduct> GetProductsBySubjectOrderedBy(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults, string orderDirection);
        IEnumerable<IProduct> GetProductsByMediaTypes(string environment, string mediaTypes, PagingInfo pagingInfo, ref int totalResults);
        IEnumerable<IProduct> GetProductsOnSale(string environment, string onSale, PagingInfo pagingInfo, ref int totalResults);

        IEnumerable<IProduct> GetFeaturedOnTelevisionProducts(string environment, PagingInfo pagingInfo, ref int totalRows);
        IEnumerable<IProduct> GetFeaturedOnGetvProducts(string environment, PagingInfo pagingInfo, ref int totalRows);

        IEnumerable<IProduct> ReloadAllProducts(string environment);

        IEnumerable<IProduct> GetProductsByPublisher(string environment, string publisher, PagingInfo pagingInfo, ref int totalResults);

        void UpdateProductWeight(string environment, string productCode, decimal weight);

        IProduct GetProductFromItemCode(string environment, string itemProductCode, bool includeOtherStockItems);
    }
}