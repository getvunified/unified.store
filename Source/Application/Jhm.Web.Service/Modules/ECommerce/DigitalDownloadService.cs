using System;
using System.Collections.Generic;
using System.Security.Principal;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class DigitalDownloadService : IDigitalDownloadService
    {
        private readonly IDigitalDownloadRepository repository;

        public DigitalDownloadService(IDigitalDownloadRepository repository)
        {
            this.repository = repository;
        }

        public DigitalDownload GetDigitalDownload(Guid id)
        {
            return repository.Get(id);
        }

        public void SaveDigitalDownload(DigitalDownload download)
        {
            repository.Save(download);
        }

        public IEnumerable<DigitalDownload> GetAllBy(IDigitalDownloadSearchCriteria searchCriteria)
        {
            return repository.GetAllBy(searchCriteria);
        }
    }
}