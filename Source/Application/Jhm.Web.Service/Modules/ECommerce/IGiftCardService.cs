﻿using System;
using System.Collections.Generic;
using Jhm.Common;
using Jhm.Web.Core.Models.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IGiftCardService : IService
    {
        IEnumerable<GiftCard> GetAvailableGiftCards();

        GiftCard GetGiftCard(Guid id);
        decimal GetBalance(string number, string pin);
        void Redeem(string number, string pin, decimal amount, string globalId);
        void Refund(string number, string pin, decimal amount, string reason, string globalId);
        void LoadGiftCard(string number, string pin, decimal amount);

    }
}
