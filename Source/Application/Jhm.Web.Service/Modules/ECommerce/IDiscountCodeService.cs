﻿using System;
using System.Collections.Generic;
using Jhm.Common;
using Jhm.Web.Core.Models;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IDiscountCodeService : IService
    {
        IList<DiscountCode> GetAllDiscountCodes();
        DiscountCode ValidateDiscountCode(string codeName, Client client, out string faultCode);
        DiscountCode ValidateDiscountCode(DiscountCode discountCode, Client client, out string faultCode);
        bool ApplyDiscountCode(DiscountCode discountCode, Client client, string faultCode);
        IList<DiscountCode> GetAllAutoApplyCodes();
        void MarkSingleUseDiscountCodeAsUsed(SingleUseDiscountCodes code, DiscountCode discountCode, string user);

        // Added by Alex 2/26/2013
        void RecalculateAppliedDiscount(Client client);

        DiscountCode GetDiscountCode(System.Guid id);

        void IncreaseUseCount(Guid discountCodeId);

        void ApplyAutoApplyCodes(Client client);
    }
}
