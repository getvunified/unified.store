﻿using System.Collections.Generic;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public interface IDiscountItemService
    {
        List<DiscountCodeItems> GetItemsRelatedToDiscountCode(DiscountCode discountCode);
    }
}
