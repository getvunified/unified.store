using System;
using System.Collections.Generic;
using Jhm.Web.Core;
using Jhm.Web.Core.Models;
using Jhm.Web.Repository.Modules.Account;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Service.Modules.ECommerce
{
    public class CatalogService : ICatalogService
    {
        private readonly ICatalogRepository repository;

        public CatalogService(ICatalogRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<IProduct> FindProducts(string environment, string searchString, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.FindProducts(environment, searchString, pagingInfo, ref totalRows);
        }

        public IEnumerable<IProduct> GetProducts(string categories, string subjects, string mediaType, string author, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetProducts(categories, subjects, mediaType, author, pagingInfo, ref totalRows);
        }

        public IEnumerable<IProduct> GetProducts(string authors, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetProducts(authors, pagingInfo, ref  totalRows);
        }

        public IEnumerable<IProduct> GetFeaturedItems(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetFeaturedItems(environment, pagingInfo, ref totalRows);
        }

        public IEnumerable<IProduct> GetAllProducts(string environment)
        {
            return repository.GetAllProducts(environment,false);
        }

        public IEnumerable<IProduct> GetLatestReleasedProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetLatestReleasedProducts(environment, pagingInfo, ref totalRows);
        }

        public IProduct GetProduct(string environment, string sku)
        {
            return repository.GetProduct(environment, sku);
        }

        public StockItem GetStockItem(string environment, string productCode, MediaType mediaType)
        {
            return repository.GetStockItem(environment, productCode, mediaType);
        }

        public StockItem GetStockItemByProductId(string environment, Guid productId, MediaType mediaType)
        {
            return repository.GetStockItemByProductId(environment, productId, mediaType);
        }

        public Company GetCompany()
        {
            return repository.GetCompany();
        }

        public IEnumerable<IProduct> GetProductsByAuthor(string environment, string author, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsByAuthor(environment, author, pagingInfo, ref totalResults);
        }

        public IEnumerable<IProduct> GetProductsByAuthorOrderedBy(string environment, string author, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            return repository.GetProductsByAuthorOrderedBy(environment, author, pagingInfo, ref totalResults, orderDirection);
        }

        public IEnumerable<IProduct> GetProductsByCategory(string environment, string categories, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsByCategory(environment, categories, pagingInfo, ref totalResults);
        }

        public IEnumerable<IProduct> GetProductsByCategoryOrderedBy(string environment, string categories, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            return repository.GetProductsByCategoryOrderedBy(environment, categories, pagingInfo, ref totalResults, orderDirection);
        }

        public IEnumerable<IProduct> GetProductsBySubject(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsBySubject(environment, subjects, pagingInfo, ref totalResults);
        }

        public IEnumerable<IProduct> GetProductsBySubjectOrderedBy(string environment, string subjects, PagingInfo pagingInfo, ref int totalResults, string orderDirection)
        {
            return repository.GetProductsBySubjectOrderedBy(environment, subjects, pagingInfo, ref totalResults, orderDirection);
        }

        public IEnumerable<IProduct> GetProductsOnSale(string environment, string onSale, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsOnSale(environment, onSale, pagingInfo, ref totalResults);
        }

        public IEnumerable<IProduct> GetProductsByMediaTypes(string environment, string mediaTypes, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsByMediaTypes(environment, mediaTypes, pagingInfo, ref totalResults);
        }

        public IEnumerable<IProduct> GetFeaturedOnTelevisionProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetFeaturedOnTelevisionProducts(environment, pagingInfo, ref totalRows);
        }

        public IEnumerable<IProduct> GetFeaturedOnGetvProducts(string environment, PagingInfo pagingInfo, ref int totalRows)
        {
            return repository.GetFeaturedOnGetvProducts(environment, pagingInfo, ref totalRows);
        }

        public IEnumerable<IProduct> ReloadAllProducts(string environment)
        {
            return repository.GetAllProducts(environment, true);
        }

        public IEnumerable<IProduct> GetProductsByPublisher(string environment, string publisher, PagingInfo pagingInfo, ref int totalResults)
        {
            return repository.GetProductsByPublisher(environment, publisher, pagingInfo, ref totalResults);
        }

        public void UpdateProductWeight(string environment, string productCode, decimal weight)
        {
            repository.UpdateProductNoteFileLength(environment, productCode, weight);
        }

        public IProduct GetProductFromItemCode(string environment, string itemProductCode, bool includeOtherStockItems = true)
        {
            return repository.GetProductFromItemCode(environment, itemProductCode, includeOtherStockItems);
        }
    }
}