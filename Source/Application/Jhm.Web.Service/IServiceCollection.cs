using Getv.Security;
using Jhm.Common;
using Jhm.DonorStudio.Services.Interfaces;
using Jhm.DonorStudio.Services.Services;
using Jhm.Web.Service.Modules.Account;
using Jhm.Web.Service.Modules.ContentManagement;
using Jhm.Web.Service.Modules.ECommerce;
using getv.donorstudio;


namespace Jhm.Web.Service
{
    public interface IServiceCollection : IService
    {
        ICatalogService CatalogService { get; }
        Modules.Account.IAccountService AccountService { get; }
        IFormsAuthenticationService FormsService { get; }
        IMembershipService MembershipService { get; }
        IDonationService DonationService { get; }
        ICartService CartService { get; }
        IEmailService EmailService { get; }
        IDigitalDownloadService DigitalDownloadService { get; }
        ISecurityService SecurityService { get; }

        IDSServiceCollection DsServiceCollection { get; }
        ICommerceService CommerceService { get; }
        IHomeBannerService HomeBannerService { get; }
        IPromotionalItemService PromotionalItemService { get; }
        IDiscountItemService DiscountItemService { get; }
        ISubscriptionService SubscriptionService { get; }
        IJhmMagazineListingService JhmMagazineListingService { get; }



        IDiscountCodeService DiscountCodeService { get; }

        IDsReportService DSReportService { get; }
        IGiftCardService GiftCardService { get; }

		IEmailSubscriptionListService EmailSubscriptionListService { get; }
    }
}