﻿namespace Jhm.Web.Core
{
    public static class AccountManagementTabs
    {
        public enum MainTab
        {
            LogOn,
            MyAccount
        }

        public enum SecondaryTab
        {
            // Anonymous
            SignIn,
            Register,
            ForgotPassword,

            // Authenticated
            MyAccount,
            OrderHistory,
            ChangePassword,
            ChangeQuestion,
            Preferences,
            Downloads,
            MyTransactions,
            ForgotUsername,
            ChangeEmail,
            MySubscriptions
        }
    }
}
