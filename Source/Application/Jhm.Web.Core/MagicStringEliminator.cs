namespace Jhm.Web.Core
{
    public static class MagicStringEliminator
    {
        public static class ApplicationKeys
        {
            public const string SubscriptionsPrefix = "_SUBS";
        }


        public static class ViewDataKeys
        {
            public const string Client = "Catalog_Client";
            public const string PopupMessage = "GenericPopupMessage";
        }

        public static class Captcha
        {
            public const string ValidationErrorFieldKey = "Captcha";
        }

        public static class Messages
        {
            public const string ErrorMessage = "Error_Message";
            public const string Status = "Status_Message";
            public const string Warning = "Warning_Message";
        }
        public static class CheckoutActions
        {
            public const string AddOneTimeDonation = "ShoppingCart_AddOneTimeDonation";
            public const string UpdateCart = "ShoppingCart_UpdateCart";
            public const string Payment = "Payment";
            public const string Review = "Review";
            public const string Receipt = "Receipt";
            public const string AddAddress = "AddAddress";
            public const string Cart = "Cart";
            public const string ApplyDiscountCode = "ShoppingCart_ApplyDiscountCode";
            public const string ClearDiscount = "ShoppingCart_ClearDiscount";
            public const string Shipping = "Shipping";

            public const string Checkout = "Checkout";
        }

        public static class EasyButtonIcon
        {
            public const string Buy = "buy";
            public const string Watch = "watch";
            public const string Listen = "listen";

        }

        public static class SessionKeys
        {
            public const string RedirectTo = "RedirectTo";

            public const string LoggedUserFullName = "LoggedUserFullName";

            public const string SiteMenu = "SiteMenuLoeadedFromJson";
        }
        public static class ServerVariables
        {
            public const string RemoteIP = "REMOTE_ADDR";
        }

        public static class EasyButtonLabel
        {
            public const string AddToCart = "Add to Cart";
            public const string DonateNow = "Donate Now";
            public const string Update = "Update";
            public const string Checkout = "Checkout";
        }
        public static class CatalogActions
        {
            public const string Author = "Author";
            public const string Index = "Index";
            public const string Category = "Category";
            public const string Subject = "Subject";
            public const string MediaTypeLookup = "MediaTypeLookup";
            public const string Search = "Search";
            public const string SpecialCaseType = "SpecialCaseType";
            public const string Product = "Product";
            public const string OnSale = "OnSale";
        }
        public static class Controller
        {
            public const string Catalog = "Catalog";
        }

        public static class Routes
        {
            public static readonly RouteData Catalog = new RouteData("Catalog", "Catalog", "Catalog", "Index");
            public static readonly RouteData Catalog_AddProduct = new RouteData("Catalog_AddProduct", "Catalog/AddToCart/Product/{sku}/{mediaType}", "Catalog", "AddProduct");
            public static readonly RouteData Catalog_Product = new RouteData("Catalog_Product", "Catalog/Product/{sku}/{productTitle}", "Catalog", "Product");
            public static readonly RouteData Catalog_Search = new RouteData("Catalog_Search", "Catalog/Search/{queryString}", "Catalog", "Search");
            public static readonly RouteData Catalog_Category = new RouteData("Catalog_Category", "Catalog/Category/{categories}/{mediaType}", "Catalog", "Category");
            public static readonly RouteData Catalog_Author = new RouteData("Catalog_Author", "Catalog/Author/{author}", "Catalog", "Author");
            public static readonly RouteData Catalog_Subject = new RouteData("Catalog_Subject", "Catalog/Subject/{subjects}", "Catalog", "Subject");
            public static readonly RouteData Catalog_MediaType = new RouteData("Catalog_MediaType", "Catalog/MediaType/{mediaTypes}", "Catalog", "MediaTypeLookup");
            public static readonly RouteData Catalog_OnSale = new RouteData("Catalog_OnSale", "Catalog/OnSale/{OnSale}", "Catalog", "OnSale");
            public static readonly RouteData Checkout_ShoppingCart = new RouteData("Checkout_ShoppingCart", "Checkout/Cart", "Checkout", "Cart");
            public static readonly RouteData Checkout_Payment = new RouteData("OrderPayment", "Checkout/Payment", "Checkout", "Payment");
            public static readonly RouteData Checkout_Review = new RouteData("OrderReview", "Checkout/Review", "Checkout", "Review");
            public static readonly RouteData Checkout_Receipt = new RouteData("OrderReceipt", "Checkout/Receipt", "Checkout", "Receipt");
            public static readonly RouteData Donation_Opportunities = new RouteData("Donation_Opportunities", "Donation/Opportunities", "Donation", "Index");
            public static readonly RouteData Donation_Detail = new RouteData("Donation_Detail", "Donation/{donationCode}", "Donation", "DonationDetail");
            public static readonly RouteData Donation_Form = new RouteData("Donation_Form", "Donation/{donationCode}/Form", "Donation", "DonationForm");
            public static readonly RouteData MyAccount = new RouteData("Profile", "Account/MyAccount", "Account", "MyAccount");
            public static readonly RouteData Catalog_SpecialCaseType = new RouteData("Catalog_SpecialCaseType", "Catalog/SpecialCaseType/{type}", "Catalog", "SpecialCaseType");
            public static readonly RouteData Account_Logon = new RouteData("Logon", "Account/LogOn/{ReturnUrl}", "Account", "LogOn");
            public static readonly RouteData Account_PromotionSettings = new RouteData("Account_Preferences", "Account/Preferences", "Account", "Preferences");
            public static readonly RouteData Checkout_AddAddress = new RouteData("Checkout_AddAddress", "Checkout/AddAddress", "Checkout", "Checkout_AddAddress");
            public static readonly RouteData DigitalDownload = new RouteData("DigitalMedia", "DigitalMedia/Download/{mediaId}", "DigitalMedia", "Download");
            public static readonly RouteData FileDownloadManager = new RouteData("FileDownloadManager", "Account/FileDownloadManager/{downloadId}", "Account", "FileDownloadManager");
            public static readonly RouteData ReloadDsProducts = new RouteData("ReloadProducts", "ReloadProducts/Reload", "ReloadDsProducts", "ReloadAllEnvironments");
            public static readonly RouteData GoPaperless = new RouteData("GoPaperless", "Account/GoPaperlessSignup/{environment}/{accountNumber}", "Account", "GoPaperlessSignup");


            public class RouteData
            {
                public string RouteName { get; private set; }
                public string Route { get; private set; }
                public string Controller { get; private set; }
                public string Action { get; private set; }

                public RouteData(string routeName, string route, string controller, string action)
                {
                    RouteName = routeName;
                    Route = route;
                    Controller = controller;
                    Action = action;
                }
            }
        }



    }
}