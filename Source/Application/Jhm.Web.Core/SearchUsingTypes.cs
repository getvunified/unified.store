﻿using System.ComponentModel.DataAnnotations;

namespace Jhm.Web.Core
{
    public enum SearchUsingTypes
    {
        [Display(Name = "All Words")]
        AllWords,

        [Display(Name = "Any Word")]
        AnyWord,

        [Display(Name = "Exact Phrase")]
        ExactPhrase
    }
}
