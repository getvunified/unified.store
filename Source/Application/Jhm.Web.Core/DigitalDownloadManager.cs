﻿using System.Web;

namespace Jhm.Web.Core
{
    public static class DigitalDownloadManager
    {

        public static void DownloadFile(string name, HttpContextBase context)
        {
           var file = new System.IO.FileInfo(name);
            if (file.Exists)
            {
                context.Response.Clear();
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                context.Response.AddHeader("Content-Length", file.Length.ToString());
                context.Response.ContentType = "application/octet-stream";
                context.Response.WriteFile(file.FullName);
                context.Response.End();
            }
           
        }
    }
}
