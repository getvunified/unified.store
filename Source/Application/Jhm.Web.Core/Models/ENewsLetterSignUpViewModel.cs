﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jhm.Common.Utilities;

namespace Jhm.Web.Core.Models
{
    public class ENewsLetterSignUpViewModel
    {
        /*public RadioButtonListViewModel<EmailTypes> WhatEmailTypeRadioButtonList { get; set; }

        public ENewsLetterSignUpViewModel()
        {
            WhatEmailTypeRadioButtonList = new RadioButtonListViewModel<EmailTypes>
            {
                Id = "WhatEmailType",
                SelectedValue = EmailTypes.HTML,
                ListItems = new List<RadioButtonListItem<EmailTypes>>
                {
                    new RadioButtonListItem<EmailTypes>{Text = "Html", Value = EmailTypes.HTML},
                    new RadioButtonListItem<EmailTypes>{Text = "Plain Text", Value = EmailTypes.PlainText}
                }
            };

        }*/

        [DisplayName("First Name")]
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        public string Address1 { get; set; }
        
        public string Address2 { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [DisplayName("Postal Code")]
        public string Zip { get; set; }
        

        public string Phone { get; set; }
        
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        public string EmailListCode { get; set; }

        public int AccountNumber { get; set; }

        [DisplayName("What type of email can you receive?")]
        public string WhatEmailType { get; set; }

        [DisplayName("Would you like to be included in Pastor Hagee's weekly Rapid Response?")]
        //[Comparer]
        public bool IncludeInWeeklyRapidResponse { get; set; }

		public IEnumerable<EmailSubscriptionList> EmailLists { get; set; }

		public IEnumerable<string> SelectedEmailLists { get; set; }

		public bool IsFullSignupList { get; set; }
	}
}
