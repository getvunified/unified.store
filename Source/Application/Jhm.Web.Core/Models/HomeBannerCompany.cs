﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Core.Models
{
    public interface IHomeBannerCompany
    {
        int CompanyCode { get; set; }
        IList<HomeBannerItem> BannerItems { get; }
    }

    public class HomeBannerCompany: DefaultEntity, IHomeBannerCompany
    {
        
        public HomeBannerCompany()
        {
            bannerItems = new List<HomeBannerItem>();
        }

        public HomeBannerCompany(Guid id) : base(id)
        {
            bannerItems = new List<HomeBannerItem>();
        }

        private int _companyCode;
        public virtual int CompanyCode
        {
            get { return _companyCode; }
            set { _companyCode = value; }
        }

        private Company _company;
        public virtual Company Company
        {
            get { return _company ?? (_company = Company.TryGetFromValue<Company>(CompanyCode)); }
            set
            {
                _company = value;
                CompanyCode = value.Value;
            }
        }

        private IList<HomeBannerItem> bannerItems;
        private Guid x;
        public virtual IList<HomeBannerItem> BannerItems
        {
            get { return bannerItems; }
        }

        public virtual bool Equals(HomeBannerCompany other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && other._companyCode == _companyCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as HomeBannerCompany);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode()*397) ^ _companyCode;
            }
        }
    }
}
