﻿using Jhm.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Core.Models
{
	public class EmailSubscriptionList : DefaultEntity
	{
		public virtual string Code { get; set; }
		public virtual string Title { get; set; }
		public virtual string Description { get; set; }

		
	}
}
