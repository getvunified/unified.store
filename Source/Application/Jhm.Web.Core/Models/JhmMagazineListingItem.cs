﻿using System;

namespace Jhm.Web.Core.Models
{
    public interface IJhmMagazineListingItem
    {
        DateTime DateCreated { get; set; }
        string Title { get; set; }
        string SubTitle { get; set; }
        string Description { get; set; }
        string ImagePath { get; set; }
        string MagazineContentId { get; set; }
        bool IsActive { get; set; }
        bool IsMagazine { get; set; }
        short Rank { get; set; }
    }


    public class JhmMagazineListingItem : DefaultEntity, IJhmMagazineListingItem
    {
        #region Implementation of IJhmMagazineListingItem

        public virtual DateTime DateCreated { get; set; }
        public virtual string Title { get; set; }
        public virtual string SubTitle { get; set; }
        public virtual string Description { get; set; }
        public virtual string ImagePath { get; set; }
        public virtual string MagazineContentId { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsMagazine { get; set; }
        public virtual short Rank { get; set; }

        #endregion
    }
}
