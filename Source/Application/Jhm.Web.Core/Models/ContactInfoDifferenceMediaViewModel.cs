﻿using System.ComponentModel.DataAnnotations;
using Jhm.Web.Core.Models.Modules.ECommerce;
using System.ComponentModel;

namespace Jhm.Web.Core.Models
{
    public class ContactInfoDifferenceMediaViewModel : CartViewModel
    {
        [DisplayName("Name")]
        [Required(ErrorMessage = "You Name is required")]
        public string Name { get; set; }

        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }

        [DisplayName("Email")]
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("Message")]
        [Required(ErrorMessage = "A message is required")]
        public string Message { get; set; }
    }
}
