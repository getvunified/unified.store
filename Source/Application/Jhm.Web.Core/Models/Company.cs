using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Jhm.Common.Lookups;
using Jhm.Common.Utilities;

namespace Jhm.Web.Core.Models
{
    public partial class Company : Enumeration<int>
    {
        public enum DataKeys
        {
            PhoneNumber,
            FullAddress,
            AddressLine1,
            AddressLine2,
            LegalName,
            TaxId,
            TaxIdLabel,
            TaxMessage
        }


        public static readonly int USCompanyCode = 1;
        public static readonly int CanadaCompanyCode = 2;
        public static readonly int UKCompanyCode = 3;

        public static readonly short USCompanyBinaryCode = 1;
        public static readonly short CanadaCompanyBinaryCode = 2;
        public static readonly short UKCompanyBinaryCode = 4;


        public static readonly Company JHM_UnitedStates = InitializeUSCompany();
        public static readonly Company JHM_Canada = InitializeCanadaCompany();
        public static readonly Company JHM_UnitedKingdom = InitializeUKCompany();

        // TEST INFO
        //public static readonly Company JHM_UnitedStates = new Company(0, "JHM US", Country.All, State.UnitedStates, "JHMPROD", "120442522524", "BfxH8amYJmBCoX7k", "WEB", true, "USD"); 
        //http://www.csharp-examples.net/culture-names/
        //var culture = new CultureInfo("en-US");
        //var culture = new CultureInfo("en-CA");
        //var culture = new CultureInfo("en-GB");
        //culture.NumberFormat.CurrencyDecimalDigits = 3;
        //Trace.WriteLine(Cost.ToString("c", culture));

        public Company()
        {
        }

        public Company(int value, short binary, string displayName, IEnumerable<Country> countries, 
            Country defaultCountry, IEnumerable<State> statesProvinces, 
            string donorStudioEnvironment, string commerceAccountId, string transactionKey, 
            string siteTag, bool cispStorageEnabled, string currency, string currencyPrefix, 
            string currencyFullName, CultureInfo culture)
            : base(value, binary, displayName)
        {
            Countries = countries;
            StateProvinces = statesProvinces;
            DonorStudioEnvironment = donorStudioEnvironment;
            CommerceAccountId = commerceAccountId;
            SiteTag = siteTag;
            CispStorageEnabled = cispStorageEnabled;
            CurrencyPrefix = currencyPrefix;
            TransactionKey = transactionKey;
            Culture = culture;
            Currency = currency;
            CurrencyFullName = currencyFullName;
            DefaultCountry = defaultCountry;
        }

        public CultureInfo Culture { get; set; }


        public virtual IEnumerable<State> StateProvinces { get; private set; }
        public virtual IEnumerable<Country> Countries { get; private set; }
        public virtual string CommerceAccountId { get; private set; }
        public virtual string DonorStudioEnvironment { get; private set; }
        public virtual string SiteTag { get; private set; }
        public virtual string TransactionKey { get; private set; }
        public virtual bool CispStorageEnabled { get; private set; }
        public virtual string CurrencyPrefix { get; private set; }
        public virtual string Currency { get; set; }
        public virtual string CurrencyFullName { get; set; }
        public virtual ShippingMethod InternationalShippingMethod { get; set; }
        /// <summary>
        /// Dictionary that contains miscellaneous data specific to this company
        /// </summary>
        public virtual ReadOnlyDictionary<DataKeys, string> AdditionalData { get; private set; }
        /// <summary>
        /// Country used as default selection in dropdowns with countries
        /// </summary>
        public virtual Country DefaultCountry { get; private set; }


        public static IEnumerable<Company> All
        {
            get
            {
                return new List<Company>()
                           {
                               JHM_UnitedStates,
                               JHM_Canada,
                               JHM_UnitedKingdom
                           };
            }
        }


        public static Company GetCompanyByCountry(Country country)
        {
            return country.Company ?? JHM_UnitedStates;
        }


        public string GetCurrencySymbol()
        {
            return CurrencyPrefix + new RegionInfo(Culture.LCID).CurrencySymbol;
        }

        internal string FormatWithCulture(string format, object value)
        {
            if (value is decimal) return CurrencyPrefix + ((decimal)value).ToString(format, Culture);
            if (value is IFormattable) return ((IFormattable)value).ToString(format, Culture);
            return value.ToString();
        }

        public static Company GetCompanyByDSEnvironment(string dsEnvironment)
        {
            var companies = new List<Company> { JHM_UnitedStates, JHM_Canada, JHM_UnitedKingdom };
            return companies.Find(x => x.DonorStudioEnvironment == dsEnvironment);
        }

        internal IEnumerable<ShippingMethod> GetShippingMethods(bool shouldIncludeFreeMethod)
        {
            List<ShippingMethod> shipMethods;
            if( this == JHM_UnitedStates )
            {
                shipMethods = ShippingMethod.GetUnitedStatesShipMethods(shouldIncludeFreeMethod);
            }
            else if ( this == JHM_Canada )
            {
                shipMethods = ShippingMethod.GetCanadaShipMethods(shouldIncludeFreeMethod);
            }
            else
            {
                shipMethods = ShippingMethod.GetUKShipMethods(shouldIncludeFreeMethod);
            }

            return shipMethods.Select(x =>
            {
                x.UpdateDisplayName(this);
                return x;
            });
        }

        
    }
}