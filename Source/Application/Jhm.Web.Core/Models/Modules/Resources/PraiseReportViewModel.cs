﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public sealed class PraiseReportViewModel: CartViewModel
    {
        [DisplayName("First Name")]
        [Required(ErrorMessage="First Name is required")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [DisplayName("Address 1")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [DisplayName("Zip/Postal")]
        [Required(ErrorMessage = "Zip/Postal Code is required")]
        public string Zip { get; set; }

        public string Country { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }
        public string Fax { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
        
        [DisplayName("Your Title")]
        public string Title { get; set; }

        [DisplayName("Praise Report")]
        public string PraiseReport { get; set; }

        [DisplayName("Would you like to be included in our email newsletter?")]
        public bool IncludeInNewsletter { get; set; }
    }
}
