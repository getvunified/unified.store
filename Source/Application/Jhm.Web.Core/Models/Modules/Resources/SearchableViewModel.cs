﻿using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public abstract class SearchableViewModel : CartViewModel
    {
        public virtual string SearchFor { get; set; }
        public virtual SearchUsingTypes SearchUsing { get; set; }
        public virtual int SearchUsingAsInt { get; set; }
        public abstract int ResultsCount { get; }
    }
}
