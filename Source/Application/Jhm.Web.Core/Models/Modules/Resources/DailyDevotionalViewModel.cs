﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Xml.Linq;

namespace Jhm.Web.Core.Models
{
    public class DailyDevotionalViewModel : SearchableViewModel
    {
        private List<XmlSyndicationRecordViewModel> items = new List<XmlSyndicationRecordViewModel>();

        public DailyDevotionalViewModel()
        {
            items = new List<XmlSyndicationRecordViewModel>();
            LoadDevs();
        }

        public DailyDevotionalViewModel(IEnumerable<XmlSyndicationRecordViewModel> list)
        {
            items = new List<XmlSyndicationRecordViewModel>(list);
            //LoadDevs();
        }

        private void LoadDevs()
        {
            string path = "http://feeds.jhm.org/ME2/Console/XmlSyndication/Display/XML.asp?xsid=B49FBA8EE01C40F885291453601E41F0";

            XDocument xmlDoc = XDocument.Load(path);

            var xmlDevs = from record in xmlDoc.Descendants("Record")
                          select new
                          {
                              Title = GetValueFor(record, "Title"),
                              Content = GetValueFor(record, "Tip Text"),
                              Id = record.Attribute("id").Value,
                              PublicationDate = GetValueFor(record, "StartDate")
                          };
            items = new List<XmlSyndicationRecordViewModel>(xmlDevs.Count());
            foreach (var xmlDev in xmlDevs)
            {
                XmlSyndicationRecordViewModel dev = new XmlSyndicationRecordViewModel();
                dev.Id = xmlDev.Id;
                dev.Title = xmlDev.Title;
                dev.Content = xmlDev.Content;
                dev.PublicationDate = xmlDev.PublicationDate;
                items.Add(dev);
            }
        }

        private static string GetValueFor(XElement record, string name)
        {
            if (record == null)
            {
                return null;
            }
            foreach (XElement xElement in record.Elements("Field"))
            {
                string labelValue = xElement.Attribute("Label").Value;
                if (labelValue == name)
                {
                    return xElement.Value;
                }
            }
            return null;
        }

      

       
        
        public override int ResultsCount
        {
            get { return items.Count; }
        }

        public List<XmlSyndicationRecordViewModel> GetAll()
        {
            return items;
        }

        public void SetDevsResults(List<XmlSyndicationRecordViewModel> filteredDevs)
        {
            items = filteredDevs ?? new List<XmlSyndicationRecordViewModel>();
        }

        public XmlSyndicationRecordViewModel GetById(string id)
        {
            XmlSyndicationRecordViewModel dev = items.Find(x => x.Id == id);
            return dev;
        }
    }
}
