﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Jhm.Web.Core.Models
{
    public class LatestNewsViewModel : SearchableViewModel
    {
        private List<XmlSyndicationRecordViewModel> items = new List<XmlSyndicationRecordViewModel>();

        public LatestNewsViewModel()
        {
            items = new List<XmlSyndicationRecordViewModel>();
            LoadNews();
        }

        public LatestNewsViewModel(IEnumerable<XmlSyndicationRecordViewModel> list)
        {
            items = new List<XmlSyndicationRecordViewModel>(list);
            //LoadNews();
        }

        private void LoadNews()
        {
            string path = "http://feeds.jhm.org/ME2/Console/XmlSyndication/Display/XML.asp?xsid=B77F0CE74C704C0DA579947D01531FF9";

            XDocument xmlDoc = XDocument.Load(path);

            var xmlNews = from record in xmlDoc.Descendants("Record")
                          select new
                          {
                              Title = GetValueFor(record, "Title"),
                              Content = GetValueFor(record, "Body Text"),
                              Id = record.Attribute("id").Value,
                              PublicationDate = GetValueFor(record, "StartDate")
                          };
            items = new List<XmlSyndicationRecordViewModel>(xmlNews.Count());
            foreach (var xmlNew in xmlNews)
            {
                XmlSyndicationRecordViewModel news = new XmlSyndicationRecordViewModel();
                news.Id = xmlNew.Id;
                news.Title = xmlNew.Title;
                news.Content = xmlNew.Content;
                news.PublicationDate = xmlNew.PublicationDate;
                items.Add(news);
            }
        }

        private static string GetValueFor(XElement record, string name)
        {
            if (record == null)
            {
                return null;
            }
            foreach (XElement xElement in record.Elements("Field"))
            {
                string labelValue = xElement.Attribute("Label").Value;
                if (labelValue == name)
                {
                    return xElement.Value;
                }
            }
            return null;
        }

        

        public override int ResultsCount
        {
            get { return items.Count; }
        }

        public List<XmlSyndicationRecordViewModel> GetAll()
        {
            return items;
        }

        public void SetNewsResults(List<XmlSyndicationRecordViewModel> filteredNews)
        {
            items = filteredNews ?? new List<XmlSyndicationRecordViewModel>();
        }

        public XmlSyndicationRecordViewModel GetById(string id)
        {
            XmlSyndicationRecordViewModel news = items.Find(x => x.Id == id);
            return news;
        }
    }
}
