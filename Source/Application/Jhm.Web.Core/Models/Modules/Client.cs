﻿using getv.donorstudio.core.Global;

namespace Jhm.Web.Core.Models.Modules
{
    public class Client
    {
        public Client()
        {
            
        }
        public Company Company { get; set; }
        public Country Country { get; set; }
        public virtual PagingInfo PagingInfo { get; set; }
        public Cart Cart { get; set; }
    }
}
