﻿using System;
using System.Collections.Generic;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DonationCartItem : CartItem
    {

        private List<StockItem> includedStockItems = new List<StockItem>();
        private string donationFrequency;


        public DonationCartItem()
        {
            CertificateLabelName = "Certificate";
        }

        public DonationCartItem(Guid id)
            : base(id)
        {
            CertificateLabelName = "Certificate";
        }

        public virtual string CertificateName { get; private set; }
        public virtual void SetCertificateName(string certificateName)
        {
            CertificateName = certificateName;
        }

        public virtual string CertificateLabelName { get; private set; }
        public virtual void SetCertificateLabelName(string certificateLabelName)
        {
            CertificateLabelName = certificateLabelName;
        }

        public virtual DonationFrequency DonationFrequency
        {
            get { return !string.IsNullOrEmpty(donationFrequency) ? DonationFrequency.TryGetFromValue<DonationFrequency>(donationFrequency) : DonationFrequency.OneTime; }
        }
        public virtual void SetDonationFrequency(DonationFrequency donationFreq)
        {
            donationFrequency = (donationFreq.IsNot().Null()) ? donationFreq.Value : DonationFrequency.OneTime.Value;
        }
        public virtual StockItem StockItemChosen { get; private set; }
        public virtual void SetStockItemChosen(StockItem stockItem)
        {
            StockItemChosen = stockItem;
        }
        public virtual List<StockItem> IncludedStockItems
        {
            get
            {
                return includedStockItems;
            }
            private set { includedStockItems = value; }
        }

        public virtual string ProjectCode { get; private set; }
        public virtual void SetProjectCode(string projectCode)
        {
            ProjectCode = projectCode;
        }


        public virtual void SetIncludedStockItems(IEnumerable<StockItem> stockItems)
        {
            IncludedStockItems = new List<StockItem>(stockItems);
        }

        public override bool HasShippingCost
        {
            get
            {
                //return IncludedStockItems != null && IncludedStockItems.Count(item => item.HasShippingCost) > 0;
                return false;
            }
        }
    }
}