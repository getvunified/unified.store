﻿using System;

namespace Jhm.Web.Core.Models
{
    public interface IDiscountCodeOptionalCriteria
    {
        Guid Id { get; set; }
        Guid DiscountCodeId { get; set; }
        string Criteria { get; set; }
        string Value { get; set; }
        string ValueDataType { get; set; }
        string AssociatedDiscountCode { get; set; }
        DiscountCode DiscountCode { get; set; }
        short ApplicableClass { get; set; }
        bool Satisfied { get; set; }
        bool IsMandatory { get; set; }
    }
}
