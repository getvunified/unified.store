﻿using System;

namespace Jhm.Web.Core.Models
{
    public class DiscountCodeItems : DefaultEntity, IDiscountCodeItems
    {
        public virtual Guid CodeId { get; set; }
        public virtual Guid GroupId { get; set; }
        public virtual bool GroupAnd { get; set; }
        public virtual bool ProductAnd { get; set; }
        public virtual string ProductCode { get; set; }
        public virtual string DiscountCode { get; set; }
        public virtual bool IsFreeItem { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual bool IsFixedAmount { get; set; }
        public virtual bool FreeShipping { get; set; }
        public virtual decimal AmountOff { get; set; }
        public virtual decimal MinPurchaseAmount { get; set; }
        public virtual decimal MaxAmountOff { get; set; }
        public virtual short Quantity { get; set; }
        public virtual string ProductWarehouse { get; set; }

        /*public virtual string Category { get; set; }
        public virtual string MediaType { get; set; }
        public virtual string Author { get; set; }
        public virtual string Language { get; set; }
        public virtual bool IsKit { get; set; }*/

        public DiscountCodeItems() { }

        public DiscountCodeItems(Guid codeId, Guid groupId, bool groupAnd, bool productAnd, string productCode,
                                 string discountCode, bool isFreeItem, DateTime startDate, DateTime endDate,
                                 bool isFixedAmount, bool freeShipping, decimal amountOff, decimal minPurchaseAmount,
                                 decimal maxAmountOff, short quantity, string productWarehouse)
        {
            CodeId = codeId;
            GroupId = groupId;
            GroupAnd = groupAnd;
            ProductAnd = productAnd;
            ProductCode = productCode;
            DiscountCode = discountCode;
            IsFreeItem = isFreeItem;
            StartDate = startDate;
            EndDate = endDate;
            IsFixedAmount = isFixedAmount;
            FreeShipping = freeShipping;
            AmountOff = amountOff;
            MinPurchaseAmount = minPurchaseAmount;
            MaxAmountOff = maxAmountOff;
            Quantity = quantity;
            ProductWarehouse = productWarehouse;
        }
    }
}
