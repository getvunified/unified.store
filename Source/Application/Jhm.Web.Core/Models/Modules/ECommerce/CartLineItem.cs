﻿using System;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class CartLineItem : DefaultEntity
    {

        public CartLineItem() { }
        public CartLineItem(CartItem item, int quantity)
        {
            Item = item;
            Quantity = quantity;
        }


        public virtual void SetQuantity(int quantity)
        {
            Quantity = quantity;
        }
        public virtual CartItem Item { get; private set; }
        public virtual int Quantity { get; private set; }

        public virtual bool MatchesDiscountCriteria { get; set; }
    }
}