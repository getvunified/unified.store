﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class RelatedItem
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
    }
}
