﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public interface IDiscountCode
    {
        string CodeName { get; set; }
        string ShortDescription { get; set; }
        string LongDescription { get; set; }
        IList<SingleUseDiscountCodes> SingleUseDiscountCodes { get; set; }
        DateTime? CreationDate { get; set; }
        DateTime? SendOutDate { get; set; }
        DateTime? UsedDate { get; set; }
        Guid? BatchId { get; set; }
        IEnumerable<DiscountCodeRequirements> Rules { get; set; }
        IList<DiscountCodeOptionalCriteria> OptionalCriteria { get; set; }
        DiscountCodeRequirements ActiveRule { get; set; }
    }
}
