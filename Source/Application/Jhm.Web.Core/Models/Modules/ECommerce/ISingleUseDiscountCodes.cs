﻿using System;

namespace Jhm.Web.Core.Models
{
    public interface ISingleUseDiscountCodes
    {
        DiscountCode DiscountCode { get; set; }
        string DiscountCodeSuffix { get; set; }
        bool IsActive { get; set; }
        Guid DiscountCodeId { get; set; }
        string UserName { get; set; }
        DateTime DateUsed { get; set; }
        /*Guid CodeGroupId { get; set; }*/
    }
}
