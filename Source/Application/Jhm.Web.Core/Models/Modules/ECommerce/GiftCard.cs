﻿using System;

namespace Jhm.Web.Core.Models.ECommerce
{
    public class GiftCard : DefaultEntity, IGiftCard
    {
        public GiftCard(){}
        public GiftCard(Guid id) : base(id) {}

        public string ProductCode { get; set; }
        public string ImagePath { get; set; }
        public string SmallImagePath { get { return "https://media.jhm.org/Images/Web/GiftCards/"+Id+"_small.jpg"; } }
        public string LargeImagePath { get { return "https://media.jhm.org/Images/Web/GiftCards/"+Id+"_large.jpg"; } }
        public string Title { get; set; }
        public string Warehouse { get; set; }

    }
}
