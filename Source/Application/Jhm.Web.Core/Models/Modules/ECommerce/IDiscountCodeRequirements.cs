﻿using System;

namespace Jhm.Web.Core.Models
{
    public interface IDiscountCodeRequirements
    {
        Guid Id { get; set; }
        Guid DiscountCodeId { get; set; }
        bool IsDirty { get; set; }
        string FreeShippingType { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        bool IsFixedAmount { get; set; }
        decimal AmountOff { get; set; }
        bool FreeShipping { get; set; }
        decimal MinPurchaseAmount { get; set; }
        decimal MaxAmountOff { get; set; }
        short ApplicableCompanies { get; set; }
        bool IsAutoApply { get; set; }
        bool IsSingleUsePrefix { get; set; }
        bool IsTiedToProducts { get; set; }
        bool IsActive { get; set; }
        short Quantity { get; set; }
        DiscountCode DiscountCode { get; set; }
        bool AppliesOnlyToItemsMatchingCriteria { get; set; }
    }
}
