﻿using System;
using System.Web.Script.Serialization;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DiscountCodeRequirements : IDiscountCodeRequirements
    {
        public DiscountCodeRequirements(){}        
        
        public virtual Guid Id { get; set; }
        public virtual Guid DiscountCodeId { get; set; }
        public virtual bool IsDirty { get; set; }
        public virtual string FreeShippingType { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual bool IsFixedAmount { get; set; }
        public virtual decimal AmountOff { get; set; }
        public virtual bool FreeShipping { get; set; }
        public virtual decimal MinPurchaseAmount { get; set; }
        public virtual decimal MaxAmountOff { get; set; }
        public virtual short ApplicableCompanies { get; set; }
        public virtual bool IsAutoApply { get; set; }
        public virtual bool IsSingleUsePrefix { get; set; }
        public virtual bool IsTiedToProducts { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual short Quantity { get; set; }
        //Since now a discount can have multiple rules (DiscountCodeRequirements) 
        //A new description field was added to rules to allow displaying different
        //descriptions in the shopping cart for the same code based on which rule is active.
        public virtual string Description { get; set; }
        

        [ScriptIgnore]
        public virtual DiscountCode DiscountCode { get; set; }
        
        public virtual bool CompanyIsApplicable(Company company)
        {
            return (company.Binary & ApplicableCompanies) == company.Binary;
        }

        public virtual decimal CalculatePercentageDiscount(decimal price)
        {
            if (IsFixedAmount)
            {
                throw new InvalidOperationException("Cannot calculate percentage discount on FixedAmount discount type");
            }

            AmountOff = AmountOff < 0 ? 0 : AmountOff;
            AmountOff = AmountOff > 100 ? 100.0m : AmountOff;
            return Math.Round((price / 100.0m) * AmountOff, 2, MidpointRounding.AwayFromZero);
        }

        public virtual bool AppliesOnlyToItemsMatchingCriteria { get; set; }
    }
}
