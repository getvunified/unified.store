﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Core.Models
{
    public interface ILinkableToCartItem
    {
        CartLineItem CartLineItem { get; set; }
    }
}
