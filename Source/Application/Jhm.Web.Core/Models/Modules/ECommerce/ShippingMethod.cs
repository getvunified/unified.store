using System;
using System.Linq;
using Jhm.Common;
using Jhm.Common.Lookups;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class ShippingMethod : Enumeration<string>
    {
        public static readonly ShippingMethod NotApplicable = new ShippingMethod("AEE4BB93-9A2D-4D76-993C-909BCD89269A", "FREE", "Not Applicable", 0);


        public static readonly ShippingMethod USPSStandard = new ShippingMethod("95E7F1F8-4F35-4B3C-8A0D-7464EC4D3511", "STANDARD", "USPS - Standard", 5.00m);
        public static readonly ShippingMethod USPSStandardFree = new ShippingMethod("26B850D2-D0A8-4F56-899E-15409BDFBEF2", "STANDARD", "USPS - Standard (Free)", 0);
        public static readonly ShippingMethod USPSGround = new ShippingMethod("F02D9C33-953E-48FD-B9F0-984B7AD812E1", "GROUND", "USPS - Ground", 10.00m, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod UPSRush = new ShippingMethod("5596C857-D410-47A1-A38B-030891C380DE", "RUSH", "UPS - Rush", 15.00m, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod USPSInternational = new ShippingMethod("980BD762-A2E7-4859-882A-07B4A184A2A1", "INTER", "USPS - International", 20.00m);


        public static readonly ShippingMethod CanadaPost = new ShippingMethod("921DB6B3-5E80-4EFE-8745-02759E4B08F4", "CND POST", "Canada Post Expedited � (5-9 Business days)", 8.00m);
        public static readonly ShippingMethod CanadaPostFree = new ShippingMethod("F4D26EAF-94AE-4892-BD39-C8C4D5B84801", "CND POST", "Canada Post Expedited � (5-9 Business days) - FREE", 0);
        public static readonly ShippingMethod FedExGroundCanada = new ShippingMethod("A389E045-085C-4A03-83AC-F03DC2BF4765", "FEDEX", "FedEx Ground � (2-4 Business days)", 20.00m, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod InternationalCanada = new ShippingMethod("A7BA56C8-905B-4CB3-9635-0BF3F338F02F", "INTER", "CanadaPost International (7-14 Business days)", 20.00m);


        public static readonly ShippingMethod UKStandard = new ShippingMethod("ADE58F14-D5F0-4B0C-BCFD-B78AC75C4862", "STANDARD", "UK - Standard", 3.00m, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod UKStandardFree = new ShippingMethod("68D61CA2-F6FA-45BA-9508-CA3CA383D7C6", "STANDARD", "UK - Standard (Free)", 0, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod UKRush = new ShippingMethod("73014EFD-86AC-4BD6-8360-B0303691B712", "RUSH", "UK - Rush", 10.00m, shouldAllowPOBoxes: false);
        public static readonly ShippingMethod UKInternational = new ShippingMethod("FF03EE1E-0679-48A5-ADD3-A5D94DC306AC", "INTER", "UK - International", 9.00m);


        public ShippingMethod()
        {
        }

        public ShippingMethod(string id, string dsValue, string displayName, decimal price, bool shouldAllowPOBoxes = true)
            : base(id, displayName)
        {
            Price = price;
            Description = displayName;
            UpdateDisplayName();
            DSValue = dsValue;
            ShouldAllowPOBoxes = shouldAllowPOBoxes;
        }

        private void UpdateDisplayName()
        {
            SetDisplayName(String.Format("{1:C} - {0} - Shipping and Handling", base.DisplayName, Price));
        }

        public void UpdateDisplayName(Company company)
        {
            SetDisplayName(FormatHelper.FormatWithCompanyCulture(company, "{1:C} - {0} - Shipping and Handling", Description, Price));
        }

        [ScriptIgnore]
        public virtual decimal Price { get; set; }
        [ScriptIgnore]
        public virtual string Description { get; set; }
        [ScriptIgnore]
        public virtual string DSValue { get; set; }
        public virtual bool ShouldAllowPOBoxes { get; set; }

        public static IEnumerable<T> GetAllSortedByPrice<T>() where T : ShippingMethod, new()
        {
            var rlist = GetAll<T>().ToList();
            rlist.Sort((x, y) => x.Price.CompareTo(y.Price));

            return rlist;
        }

        public static IEnumerable<T> GetAllNonFree<T>() where T : ShippingMethod, new()
        {
            return GetAllSortedByPrice<T>().Where(x => x.Price > 0);
        }

        public static IEnumerable<ShippingMethod> GetAllNonFree(Company company)
        {
            return GetAllSortedByPrice<ShippingMethod>().Where(x => x.Price > 0).Select(x =>
                                                                                            {
                                                                                                x.UpdateDisplayName(company);
                                                                                                return x;
                                                                                            });
        }

        public static List<ShippingMethod> GetUnitedStatesShipMethods(bool shouldIncludeFreeMethod)
        {
            return new List<ShippingMethod>
                                {
                                    shouldIncludeFreeMethod? USPSStandardFree : USPSStandard,
                                    USPSGround,
                                    UPSRush,
                                    USPSInternational
                                };
        }

        public static List<ShippingMethod> GetCanadaShipMethods(bool shouldIncludeFreeMethod)
        {
            return new List<ShippingMethod>
                                       {
                                           shouldIncludeFreeMethod ? CanadaPostFree : CanadaPost,
                                           FedExGroundCanada,
                                           InternationalCanada
                                       };
        }

        public static readonly List<ShippingMethod> UnitedKingdomShipMethods =
                   Collection.Sort(new List<ShippingMethod>
                                {
                                    UKStandard,
                                    UKRush,
                                    UKInternational,
                                    NotApplicable
                                }, (x, y) => x.Price.CompareTo(y.Price));



        internal static List<ShippingMethod> GetUKShipMethods(bool shouldIncludeFreeMethod)
        {
            return new List<ShippingMethod>
                                {
                                    shouldIncludeFreeMethod? UKStandardFree : UKStandard,
                                    UKRush,
                                    UKInternational
                                };
        }
    }
}