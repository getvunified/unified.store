﻿using System;

namespace Jhm.Web.Core.Models
{
    public interface IDiscountCodeItems
    {
        //DiscountCode ParentCode { get; set; }
        Guid CodeId { get; set; }
        Guid GroupId { get; set; }
        bool GroupAnd { get; set; }
        bool ProductAnd { get; set; }
        string ProductCode { get; set; }
        string DiscountCode { get; set; }
        bool IsFreeItem { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        bool IsFixedAmount { get; set; }
        bool FreeShipping { get; set; }
        decimal AmountOff { get; set; }
        decimal MinPurchaseAmount { get; set; }
        decimal MaxAmountOff { get; set; }
        short Quantity { get; set; }
        string ProductWarehouse { get; set; }
    }
}
