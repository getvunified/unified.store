﻿using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class Review : DefaultEntity
    {
        private string reviewStatus;

        public Review(string productCode, DateTime reviewDate, decimal rating, string title, string body, User reviewedBy, ReviewStatus status)
        {
            ProductCode = productCode;
            Date = reviewDate;
            Rating = rating;
            Title = title;
            Body = body;
            ReviewedBy = reviewedBy;
            reviewStatus = (status.IsNot().Null()) ? status.Value : ReviewStatus.InActive.Value;
        }

        public Review() { }

        public virtual string ProductCode { get; private set; }
        public virtual DateTime Date { get; private set; }
        public virtual string Body { get; private set; }
        public virtual User ReviewedBy { get; private set; }
        public virtual ReviewStatus ReviewStatus { get { return ReviewStatus.TryGetFromValue<ReviewStatus>(reviewStatus); } }
        public virtual void SetStatus(ReviewStatus status)
        {
            reviewStatus = (status.IsNot().Null()) ? status.Value : ReviewStatus.InActive.Value;

        }
        public virtual string Title { get; private set; }
        public virtual decimal Rating { get; private set; }
       
    }
}