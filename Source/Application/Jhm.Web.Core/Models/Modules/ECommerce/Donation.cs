﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class Donation : DefaultEntity, IDonation
    {
        private List<DonationOption> donationOptions = new List<DonationOption>();

        public Donation(Guid donationId, string donationCode, string title, string description, string body, string listViewImageUrl = null, string detailViewImageUrl = null, string formViewImageUrl = null, string pdfVersion = null)
        {
            DonationId = donationId;
            DonationCode = donationCode;
            Title = title;
            Description = description;
            Body = body;
            PdfVersion = pdfVersion;
            ListViewImageUrl = listViewImageUrl;
            DetailViewImageUrl = detailViewImageUrl;
            FormViewImageUrl = formViewImageUrl;
        }


        public Donation(string donationCode, string title, string description, string body, string listViewImageUrl = null, string detailViewImageUrl = null, string formViewImageUrl = null, string pdfVersion = null)
            : this(Guid.Empty, donationCode, title, description, body, listViewImageUrl, detailViewImageUrl, formViewImageUrl, pdfVersion)
        {

        }
        public Donation(string projectCode, string title)
        {
            ProjectCode = projectCode;
            Title = title;
        }

        public virtual string PdfVersion { get; private set; }
        public virtual string ListViewImageUrl { get; private set; }
        public virtual void SetListViewImageUrl(string imageUrl) { ListViewImageUrl = imageUrl; }
        public virtual string DetailViewImageUrl { get; private set; }
        public virtual void SetDetailViewImageUrl(string imageUrl) { DetailViewImageUrl = imageUrl; }
        public virtual string FormViewImageUrl { get; private set; }
        public void SetFormViewImageUrl(string imageUrl) { FormViewImageUrl = imageUrl; }
        public virtual string Body { get; private set; }
        public virtual string Title { get; private set; }
        public virtual string Description { get; private set; }
        public virtual string DonationCode { get; private set; }

        public virtual bool IsHidden { get; set; }
        public virtual string CustomEmailBody { get; set; }

        public virtual string CustomEmailTemplateFileName { get; set; }

        public virtual IEnumerable<DonationOption> DonationOptions
        {
            get
            {
                var rList = donationOptions.ToLIST();
                rList.Sort((x, y) => x.SortOrder.CompareTo(y.SortOrder));
                return rList;
            }
        }

        public virtual void AddDonationOption(DonationOption donationOption)
        {
            if (donationOption.Is().Null())
                throw new ArgumentNullException("donationOption");
            SetDonationOptionSortOrder(donationOption);
            donationOptions.AddUnique(donationOption);
        }
        private void SetDonationOptionSortOrder(DonationOption donationOption)
        {
            var reverseOrderedList = donationOptions.ToLIST();
            reverseOrderedList.Sort((x, y) => y.SortOrder.CompareTo(x.SortOrder)); //reverse sort

            var largestSortOrder = 0;
            if (reverseOrderedList.Count.Is().GreaterThan(0))
                largestSortOrder = reverseOrderedList.First().SortOrder;

            donationOption.SetSortOrder(largestSortOrder + 1);
        }
        public virtual DonationCartItem MapSelectedOptionToNewDonationCartItem(DonationCriteria donationCriteria)
        {
            if (donationCriteria.Is().Null())
                throw new ArgumentNullException("donationCriteria");

            var selectedOption = donationOptions.Find(x => x.Id == donationCriteria.DonationOptionId);

            if (selectedOption.Is().Null())
                throw new ArgumentException(
                    "Unable to find 'Donation Option' with ID: {0}".FormatWith(donationCriteria.DonationOptionId));

            var donationCartItem = new DonationCartItem();
            donationCartItem.SetCode(DonationCode);
            donationCartItem.SetTitle(Title);
            donationCartItem.SetDescription(Description);
            donationCartItem.SetProjectCode(ProjectCode);
            if (selectedOption.OptionFieldIsEnabled())
            {
                donationCartItem.SetCertificateLabelName(selectedOption.OptionField.FieldName);
            }
            selectedOption.ApplyDonationCriteria(donationCartItem, donationCriteria);



            return donationCartItem;
        }
        public virtual IEnumerable<RelatedItem> RelatedItems
        {
            get { throw new NotImplementedException(); }
        }

        public void SetProjectCode(string projectCode)
        {
            ProjectCode = projectCode;
        }

        public virtual string ProjectCode { get; private set; }

        public Guid DonationId { get; set; }
    }
}