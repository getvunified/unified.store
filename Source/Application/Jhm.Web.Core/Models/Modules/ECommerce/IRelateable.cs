﻿using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public interface IRelateable
    {
        IEnumerable<RelatedItem> RelatedItems { get; }
    }
}