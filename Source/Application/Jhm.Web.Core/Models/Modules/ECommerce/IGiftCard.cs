﻿using Jhm.Common;

namespace Jhm.Web.Core.Models.ECommerce
{
    public interface IGiftCard : IEntity
    {
        string ProductCode { get; set; }
        string ImagePath { get; set; }
        string Title { get; set; }
        string Warehouse { get; set; }
    }
}
