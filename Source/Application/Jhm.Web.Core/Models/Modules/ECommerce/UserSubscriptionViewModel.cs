﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{
    public class UserSubscriptionViewModel
    {
        public long Id { get; set; }
        public User User { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CancelReasonCode { get; set; }

        private Address _deliveryAddress;
        public Address DeliveryAddress
        {
            get { return (_deliveryAddress = _deliveryAddress ?? new Address()); }
            set { _deliveryAddress = value; }
        }

        public int NumberOfIssues { get; set; }
        public int NumberOfIssuesRemaining { get; set; }
        public string PriceCode { get; set; }
        public string PriceCodeDescription { get; set; }
        public string StartDateString { get { return StartDate.GetValueOrDefault().ToShortDateString(); } }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string ExpirationDateString { get { return ExpirationDate.GetValueOrDefault().ToShortDateString(); } }
        public string SubscriptionCode { get; set; }
        public string SubscriptionStatus { get; set; }
        public bool IsGiftSubcription { get; set; }

        public string ShoppingCartDescription { get; set; }

        public decimal SubscriptionPrice { get; set; }

        public string SubscriptionTitle { get; set; }

        public long DsRecordId { get; set; }

        public bool IsRenewal { get; set; }
    }
}
