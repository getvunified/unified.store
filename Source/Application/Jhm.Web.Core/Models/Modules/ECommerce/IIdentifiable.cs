﻿namespace Jhm.Web.Core.Models
{
    public interface IIdentifiable
    {
        string Code { get; }
        string Title { get; }
        string Description { get; }
    }
}