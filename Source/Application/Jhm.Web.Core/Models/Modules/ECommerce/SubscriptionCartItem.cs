﻿namespace Jhm.Web.Core.Models
{
    public class SubscriptionCartItem: CartItem
    {
        public SubscriptionCartItem(){}

        public SubscriptionCartItem(System.Guid id)
            : base(id)
        {
            // TODO: Complete member initialization
        }

        public override bool HasShippingCost
        {
            get
            {
                //Shipping cost is included in the subscription cost
                return false;
            }
        }

        public UserSubscriptionViewModel UserSubscription { get; set; }

        public void MapCartItem(UserSubscriptionViewModel from)
        {
            SetCode(from.SubscriptionCode + from.User.Username + from.User.Profile.FirstName + from.User.Profile.LastName + from.DeliveryAddress.Address1 + from.DeliveryAddress.City);
            SetDescription(from.ShoppingCartDescription);
            SetPrice(from.SubscriptionPrice);
            SetTitle(from.IsGiftSubcription? from.SubscriptionTitle + " as Gift" : from.SubscriptionTitle);
        }
    }
}
