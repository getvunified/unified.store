﻿#region

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DiscountCode : DefaultEntity, IDiscountCode
    {
        public DiscountCode()
        {
        }

        public DiscountCode(string codeName, string shortDescription, string longDescription,
                            DateTime creationDate, DateTime sendOutDate,
                            DateTime usedDate, Guid batchId, string freeShippingType)
        {
            CodeName = codeName;
            ShortDescription = shortDescription;
            LongDescription = longDescription;
            CreationDate = creationDate;
            SendOutDate = sendOutDate;
            UsedDate = usedDate;
            BatchId = batchId;
            FreeShippingType = freeShippingType;

        }
        public virtual string CodeName { get; set; }

        private string shortDescription;
        public virtual string ShortDescription 
        { 
            get 
            {
                //Since now a discount can have multiple rules (DiscountCodeRequirements) 
                //A new description field was added to rules to allow displaying different
                //descriptions in the shopping cart for the same code based on which rule is
                //active. If no description is provided by the active rule, the one in the 
                //DiscountCodes table is used
                if (!string.IsNullOrEmpty(ActiveRule.Description))
                {
                    return ActiveRule.Description;
                }
                return shortDescription;
            }
            set
            {
                shortDescription = value;
            }
        }

        public virtual string LongDescription { get; set; }
        public virtual string FreeShippingType { get; set; }
        public virtual DateTime? CreationDate { get; set; }
        public virtual DateTime? SendOutDate { get; set; }
        public virtual DateTime? UsedDate { get; set; }
        public virtual Guid? BatchId { get; set; }
        public virtual int UseCount { get; set; }

        private IList<DiscountCodeOptionalCriteria> optionalCriteria;
        public virtual IList<DiscountCodeOptionalCriteria> OptionalCriteria
        {
            get { return optionalCriteria; }
            set
            {
                optionalCriteria = new List<DiscountCodeOptionalCriteria>(value);
            }
        }

        private IEnumerable<DiscountCodeRequirements> rules;
        public virtual IEnumerable<DiscountCodeRequirements> Rules
        {
            get { return rules; }
            set { rules = new List<DiscountCodeRequirements>(value); }
        }

        private IList<SingleUseDiscountCodes> singleUseDiscountCodes;
        public virtual IList<SingleUseDiscountCodes> SingleUseDiscountCodes
        {
            get { return singleUseDiscountCodes; }
            set { singleUseDiscountCodes = new List<SingleUseDiscountCodes>(value); }
        }

        public bool AllowOnSaleItems { get; set; }

        private DiscountCodeRequirements activeRule;
        public DiscountCodeRequirements ActiveRule
        {
            get {
                var now = DateTime.Now;

				
				//var isDummyRuleActive = activeRule.Id.Equals(Guid.Empty);
                //since the active rule is time sensitive we have to check if the cached rule has not expired
				if (activeRule == null || (activeRule != null && !activeRule.Id.Equals(Guid.Empty) && activeRule.EndDate < now))
                {
                    activeRule = Rules.FirstOrDefault(x => x.StartDate <= now && x.EndDate >= now);
                    if (activeRule == null)
                    {
                        //returns a dummy rule to prevent null reference exceptions if no rule was found in the given time
                        activeRule = new DiscountCodeRequirements();
                    }
                }
				if (activeRule == null)
				{
					//returns a dummy rule to prevent null reference exceptions if no rule was found in the given time
					activeRule = new DiscountCodeRequirements();
				}
                return activeRule; 
            }
            set { activeRule = value; }
        }
    }
}