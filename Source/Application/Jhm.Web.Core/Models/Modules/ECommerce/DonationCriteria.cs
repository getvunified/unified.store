﻿using System;
using System.Collections.Generic;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    public static class DonationCriteriaFactory
    {
        public static DonationCriteria DonationCriteria(Guid id, string certificateName = null, DonationFrequency donationFrequency = null, Guid stockItemChoiceId = new Guid())
        {
            return new DonationCriteria() { DonationOptionId = id, CertificateName = certificateName, DonationFrequency = donationFrequency, StockItemChoiceId = stockItemChoiceId };
        }

        public static AmountDonationCriteria AmountDonationCriteria(Guid id, decimal amount, string certificateName = null, DonationFrequency donationFrequency = null, Guid stockItemChoiceId = new Guid())
        {
            return new AmountDonationCriteria() { DonationOptionId = id, DonationAmount = amount, CertificateName = certificateName, DonationFrequency = donationFrequency, StockItemChoiceId = stockItemChoiceId };
        }

        public static MultipleOfDonationCriteria MultipleOfDonationCriteria(Guid id, int multiple, string certificateName = null, DonationFrequency donationFrequency = null, Guid stockItemChoiceId = new Guid())
        {
            return new MultipleOfDonationCriteria() { DonationOptionId = id, Multiple = multiple, CertificateName = certificateName, DonationFrequency = donationFrequency, StockItemChoiceId = stockItemChoiceId };
        }
    }

    [Serializable]
    public class DonationCriteria
    {
        public virtual Guid DonationOptionId { get; set; }
        public virtual string CertificateName { get; set; }
        public virtual DonationFrequency DonationFrequency { get; set; }
        public virtual Guid StockItemChoiceId { get; set; }


    }

    public class AmountDonationCriteria : DonationCriteria
    {
        public decimal DonationAmount { get; set; }
    }

    public class MultipleOfDonationCriteria : DonationCriteria
    {
        private int multiple;

        /// <summary>
        /// Value must be greater than Zero.
        /// </summary>
        public int Multiple
        {
            get { return multiple; }
            set
            {
                if (value.IsNot().GreaterThan(0))
                    throw new ArgumentException("Value must be greater than Zero.", "Multiple");

                multiple = value;
            }
        }
    }
}