﻿using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class CartItem : DefaultEntity, IIdentifiable
    {
        public CartItem()
        {
        }

        public CartItem(Guid id) : base(id)
        {
        }

        public virtual decimal Price { get; private set; }
        public virtual string Code { get; private set; }
        public virtual string Title { get; private set; }
        public virtual string Description { get; private set; }
        public virtual bool HasShippingCost { get; private set; }
        public virtual string Warehouse { get; private set; }

        public virtual string GetDescription(int truncateAtLength)
        {
            if (Description.IsNot().NullOrEmpty() && Description.Length.Is().GreaterThan(truncateAtLength))
                return Description.Substring(0, truncateAtLength) + "...";
            return Description;
        }
        public virtual string ImageLocation { get; private set; }


        public virtual void SetPrice(decimal value) { Price = value; }
        public virtual void SetCode(string value) { Code = value; }
        public virtual void SetTitle(string value) { Title = value; }
        public virtual void SetDescription(string value) { Description = value; }
        
        public virtual void SetImageLocation(string value) { ImageLocation = value; }
        public virtual void SetHasShippingCost(bool value) { HasShippingCost = value; }
        public virtual void SetWarehouseCode(string value) { Warehouse = value; }
    }
}