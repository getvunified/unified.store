﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.UI.WebControls;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.Web.Core.Models.Modules.ECommerce;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class Cart : DefaultEntity, ICloneable
    {
        public virtual event CommandEventHandler CartUpdated;
        
        private string shippingMethod;

        public Cart()
        {
            items = new List<CartLineItem>();
            _promotionalItems = new List<PromotionalItemViewModel>();
            _discountItems = new List<DiscountItemViewModel>();
        }

        public virtual ShippingMethod ShippingMethod
        {
            get { return !string.IsNullOrEmpty(shippingMethod) ? ShippingMethod.TryGetFromValue<ShippingMethod>(shippingMethod) : ShippingMethod.USPSStandard; }
        }
        public virtual void SetShippingMethod(ShippingMethod shipMethod) { shippingMethod = shipMethod.IsNot().Null() ? shipMethod.Value : ShippingMethod.USPSStandard.Value; }

        private List<CartLineItem> items;
        public virtual IEnumerable<CartLineItem> Items { get { return items.ToLIST(); } }

        public virtual decimal DiscountTotal
        {
            get { return Math.Round(_discountTotal, 2, MidpointRounding.AwayFromZero); }
            private set { _discountTotal = value; }
        }

        public virtual DiscountCode DiscountApplied { get; private set; }
        public virtual string DiscountErrorMessage { get; private set; }
        public virtual decimal SubTotalWithoutDiscount
        {
            get { return (subTotalWithoutDiscount < 0) ? 0 : subTotalWithoutDiscount; }
            set { subTotalWithoutDiscount = value; }
        }

        public virtual decimal SubTotalWithDiscount
        {
            get { return (subTotalWithDiscount < 0) ? 0 : subTotalWithDiscount; }
            set { subTotalWithDiscount = value; }
        }

        public virtual bool IsCartUpdatedSet()
        {
            return CartUpdated != null && (CartUpdated.GetInvocationList().Count() == 2);
        } 

        private List<PromotionalItemViewModel> _promotionalItems;
        public virtual List<PromotionalItemViewModel> PromotionalItems
        {
            get { return _promotionalItems; }
            set { _promotionalItems = value; }
        }
        
        private List<PromotionalItemViewModel> _availablePromotionalItems;
        public virtual List<PromotionalItemViewModel> AvailablePromotionalItems
        {
            get { return (_availablePromotionalItems = _availablePromotionalItems ?? new List<PromotionalItemViewModel>()); }
            set { _availablePromotionalItems = value; }
        }

        private List<DiscountItemViewModel> _discountItems;
        public virtual List<DiscountItemViewModel> DiscountItems
        {
            get { return _discountItems; }
            set { _discountItems = value; }
        }

        private List<DiscountItemViewModel> _availableDiscountItems;
        private decimal subTotalWithoutDiscount;
        private decimal subTotalWithDiscount;
        private decimal _discountTotal;

        public virtual List<DiscountItemViewModel> AvailableDiscountItems
        {
            get { return (_availableDiscountItems = _availableDiscountItems ?? new List<DiscountItemViewModel>()); }
            set { _availableDiscountItems = value; }
        }

        #region Public Methods

        /// <summary>
        /// Add/Update an item to/in the cart
        /// </summary>
        /// <param name="item"></param>
        /// <param name="quantity"></param>
        /// <returns>Returns the quantity of the Item in the cart after Add/Update</returns>
        public virtual int AddItem(CartItem item, int quantity)
        {
            SetDiscountErrorMessage("");
            //Note: If CartItem Exists In Cart
            foreach (CartLineItem cartLineItem in Items.Where(x => x.Item.Code == item.Code))
            {
                var newQuantity = cartLineItem.Quantity + quantity;

                if (IsStockItem(item))
                    newQuantity = ResetQuantityIfQuantityInStockIsLess(item as StockItem, newQuantity);

                cartLineItem.SetQuantity(newQuantity);
                PerformCartChangedActions();
                return newQuantity;
            }


            if (IsStockItem(item))
                quantity = ResetQuantityIfQuantityInStockIsLess(item as StockItem, quantity);

            items.Add(new CartLineItem(item, quantity));
            //Hack: Until we persist this with nhibernate or the like
            CreateIDsForEmptyGuidCartItems();
            PerformCartChangedActions();
            return quantity;

        }
        private void CreateIDsForEmptyGuidCartItems()
        {
            foreach (var cartLineItem in Items.Where(x => x.Item.Id == Guid.Empty))
            {
                CartItem item = null;
                int quantity = cartLineItem.Quantity;
                if (cartLineItem.Item is DonationCartItem)
                {
                    var itemAsDonationCartItem = (cartLineItem.Item as DonationCartItem);
                    var donationCartItem = new DonationCartItem(Guid.NewGuid());
                    donationCartItem.SetCode(cartLineItem.Item.Code);
                    donationCartItem.SetDescription(cartLineItem.Item.Description);

                    donationCartItem.SetPrice(cartLineItem.Item.Price);
                    donationCartItem.SetTitle(cartLineItem.Item.Title);
                    donationCartItem.SetCertificateName(itemAsDonationCartItem.CertificateName);
                    donationCartItem.SetDonationFrequency(itemAsDonationCartItem.DonationFrequency);
                    donationCartItem.SetProjectCode(itemAsDonationCartItem.ProjectCode);
                    donationCartItem.SetStockItemChosen(itemAsDonationCartItem.StockItemChosen);
                    donationCartItem.SetIncludedStockItems(itemAsDonationCartItem.IncludedStockItems);
                    donationCartItem.SetCertificateLabelName(itemAsDonationCartItem.CertificateLabelName);
                    item = donationCartItem;
                }
                else if (cartLineItem.Item is StockItem)
                {
                    var itemAsStockItem = cartLineItem.Item as StockItem;
                    var stockItem = new StockItem(Guid.NewGuid(), itemAsStockItem.MediaType, itemAsStockItem.QuantityInStock, itemAsStockItem.AllowBackorder, itemAsStockItem.BasePrice, itemAsStockItem.SalePrice, true, itemAsStockItem.MediaPath);
                    stockItem.SetCode(itemAsStockItem.Code);
                    stockItem.SetTitle(itemAsStockItem.Title);
                    stockItem.SetDescription(itemAsStockItem.Description);
                    stockItem.SetAuthorDescription(itemAsStockItem.AuthorDescription);
                    stockItem.SetPrice(itemAsStockItem.Price);
                    stockItem.SetProductTitle(itemAsStockItem.ProductTitle);
                    stockItem.SetProductCode(itemAsStockItem.ProductCode);
                    stockItem.SetWarehouseCode(itemAsStockItem.Warehouse);
                    stockItem.SetImageLocation(itemAsStockItem.ImageLocation);
                    item = stockItem;
                }
                else if ( cartLineItem.Item is SubscriptionCartItem )
                {
                    var itemAsSubsItem = cartLineItem.Item as SubscriptionCartItem;
                    var subscriptionCartItem = new SubscriptionCartItem(Guid.NewGuid());
                    subscriptionCartItem.SetCode(itemAsSubsItem.Code);
                    subscriptionCartItem.SetDescription(itemAsSubsItem.Description);
                    subscriptionCartItem.SetPrice(itemAsSubsItem.Price);
                    subscriptionCartItem.SetTitle(itemAsSubsItem.Title);
                    subscriptionCartItem.UserSubscription = itemAsSubsItem.UserSubscription;
                    item = subscriptionCartItem;
                }
                else if (cartLineItem.Item is GiftCardCartItem)
                {
                    var itemAsGCItem = cartLineItem.Item as GiftCardCartItem;
                    var newCartItem = new GiftCardCartItem(Guid.NewGuid());
                    newCartItem.SetCode(itemAsGCItem.Code);
                    newCartItem.SetDescription(itemAsGCItem.Description);
                    newCartItem.SetPrice(itemAsGCItem.Price);
                    newCartItem.SetTitle(itemAsGCItem.Title);
                    newCartItem.SetWarehouseCode(itemAsGCItem.Warehouse);
                    newCartItem.SetImageLocation(itemAsGCItem.ImageLocation);
                    newCartItem.GiftCardFormInfo = itemAsGCItem.GiftCardFormInfo;
                    item = newCartItem;
                }
                RemoveItem(cartLineItem.Item);
                AddItem(item, quantity);
            }

        }
        /// <summary>
        /// Adds a DonationCartItem to the cart
        /// </summary>
        /// <param name="donationCartItem">DonationCartItem</param>
        /// <exception cref="DonationCartItemExistsException">Throws a DonationCartItemExistsException if Donation exists in cart.</exception>
        public virtual void AddItem(DonationCartItem donationCartItem)
        {
            SetDiscountErrorMessage("");
            if (items.Exists(x => x.Item.Code == donationCartItem.Code))
                throw new DonationCartItemExistsException(donationCartItem);

            items.Add(new CartLineItem(donationCartItem, 1));
            //Hack: need to update this when we persist the cart to the DB via nHibernate
            CreateIDsForEmptyGuidCartItems();
            PerformCartChangedActions();
        }

        /// <summary>
        /// Adds a SubscriptionCartItem to the cart
        /// </summary>
        /// <param name="subscriptionCartItemItem">SubscriptionCartItem</param>
        /// <exception cref="DonationCartItemExistsException">Throws a SubscriptionCartItemExistsException if Subscription exists in cart.</exception>
        public virtual void AddItem(SubscriptionCartItem subscriptionCartItemItem)
        {
            SetDiscountErrorMessage("");
            if (items.Exists(x => x.Item.Code == subscriptionCartItemItem.Code))
                throw new SubscriptionCartItemExistsException(subscriptionCartItemItem);

            items.Add(new CartLineItem(subscriptionCartItemItem, 1));
            //Hack: need to update this when we persist the cart to the DB via nHibernate
            CreateIDsForEmptyGuidCartItems();
            PerformCartChangedActions();
        }

        public virtual int AddItem(GiftCardCartItem cartItem, int quantity)
        {
            foreach (CartLineItem cartLineItem in Items.Where(x => x.Item.Code == cartItem.Code))
            {
                var newQuantity = cartLineItem.Quantity + quantity;

                //if (IsStockItem(item))
                //    newQuantity = ResetQuantityIfQuantityInStockIsLess(item as StockItem, newQuantity);

                cartLineItem.SetQuantity(newQuantity);
                PerformCartChangedActions();
                return newQuantity;
            }


            //if (IsStockItem(item))
            //    quantity = ResetQuantityIfQuantityInStockIsLess(item as StockItem, quantity);

            items.Add(new CartLineItem(cartItem, quantity));
            //Hack: Until we persist this with nhibernate or the like
            CreateIDsForEmptyGuidCartItems();
            PerformCartChangedActions();
            return quantity;
        }

        private void CheckDiscountCodeItems(Cart cart)
        {
            cart.DiscountItems.Clear();
            if (cart.DiscountApplied.IsNull()) return;

            var availableDiscountItems = cart.AvailableDiscountItems;
            if (!availableDiscountItems.Any()) return;

            var purchasedItemsSubtotal = cart.GetSubTotalByItemType(typeof(StockItem));
            var applicableItems = availableDiscountItems.Where(x => purchasedItemsSubtotal >= x.MinimumPurchaseAmount).ToLIST();
            if (!applicableItems.Any()) return;

            foreach (var discountItem in applicableItems)
            {
                cart.DiscountItems.AddUnique(discountItem);
            }
        }

        private void PerformCartChangedActions()
        {
            if (DiscountApplied != null)
                DiscountApplied.ActiveRule.IsDirty = true;
            CheckPromotionalItems();
            
        }

        

        private void CheckPromotionalItems()
        {
            PromotionalItems.Clear();
            var activePromotionalItems = AvailablePromotionalItems;
            if (!activePromotionalItems.Any()) return;
            
            foreach (var cartItem in items.Where(x => x.Item is DonationCartItem))
            {
                var donation = cartItem.Item as DonationCartItem;
                var applicableDonationItems = activePromotionalItems.Where(x => x.DonationCode == donation.Code).ToLIST();
                if (!applicableDonationItems.Any()) continue;

                foreach (var promotionalItem in applicableDonationItems.Where(promotionalItem => donation.Price >= promotionalItem.MinimumDonationAmount))
                {
                    PromotionalItems.AddUnique(promotionalItem);
                }
            }
            var purchasedItemsSubtotal = GetSubTotalByItemType(typeof(StockItem));
            var applicableItems = activePromotionalItems.Where(x => purchasedItemsSubtotal >= x.MinimumPurchaseAmount).ToLIST();
            if (!applicableItems.Any()) return;

            foreach (var promotionalItem in applicableItems)
            {
                PromotionalItems.AddUnique(promotionalItem);
            }
        }

        public virtual void RemoveItem(CartItem cartItem)
        {
            items.RemoveAll(Containing(cartItem));
            if (CartUpdated != null)
            {
                
                CartUpdated(this, new CommandEventArgs("RemoveItem", null));
            }
            PerformCartChangedActions();
        }
        public virtual int NumberOfItemsInCart()
        {
            return items.Count;
        }
        public virtual void Empty()
        {
            Clear();
        }
        public virtual void ChangeQuantity(CartItem cartItem, int i)
        {
            foreach (CartLineItem cartLineItem in Items.Where(cartLineItem => cartLineItem.Item.Code.Equals(cartItem.Code)))
            {
                if (i <= 0)
                    items.Remove(cartLineItem);
                else
                    cartLineItem.SetQuantity(i);
            }
            PerformCartChangedActions();
        }

        /// <summary>
        /// Gets subtotal of all items in the cart (Donations + StockItems) less the discount if specified
        /// </summary>
        /// <param name="subtractDiscount">flag to indicate whether the subtotal should be calculated including the discount or not</param>
        /// <returns></returns>
        public virtual decimal SubTotal(bool subtractDiscount)
        {
            if (subtractDiscount)
            {
                //RecalculateAppliedDiscount();

            }
            return items.Sum(cartLineItem => cartLineItem.Quantity * cartLineItem.Item.Price) - (subtractDiscount ? DiscountTotal : 0);
        }

        public virtual decimal GetSubTotalByItemType(Type cartItemType)
        {
            /*if (!cartItemType.IsSubclassOf(typeof(CartItem)))
            {
                return 0;
            }*/
            return items.Where(x => x.Item.GetType() == cartItemType).Sum(cartLineItem => cartLineItem.Quantity * cartLineItem.Item.Price);
        }

        public virtual decimal Total()
        {
            if (ShippingMethod.Is().Null())
                throw new Exception("Shipping Method is not defined");

            var total = SubTotal(subtractDiscount: true) + ShippingMethod.Price;
            return (total < 0) ? 0 : total;
        }

        //public virtual void AddBillingAddress(Address billingAddress) { BillingAddress = billingAddress; }
        //public virtual void AddShippingAddress(Address shippingAddress) { ShippingAddress = shippingAddress; }
        public virtual CartLineItem GetLineItem(CartItem cartItem)
        {
            return Enumerable.FirstOrDefault(Items, cartLineItem => cartLineItem.Item.Code.Equals(cartItem.Code));
        }

        public virtual void SetShippingType(ShippingMethod shippingMethod)
        {
            SetShippingMethod(shippingMethod);
        }

        public virtual bool HasItemsWithShippingCost()
        {
            return items.Any(x => x.Item.HasShippingCost);
        }

        public virtual bool HasItemsWithCost()
        {
            return items.Any(x => x.Item.Price > 0 || x.Item.HasShippingCost);
        }

        public virtual bool HasShippableItems()
        {
            return items.Any(x => x.Item is StockItem) 
                || items.Any(x => x.Item is DonationCartItem && ((DonationCartItem)x.Item).IncludedStockItems.Any());
        }

        #endregion

        #region Private Methods

        private Predicate<CartLineItem> Containing(CartItem cartItem)
        {
            return x => x.Item.Code.Equals(cartItem.Code);
        }
        public static Cart NewEmpty()
        {
            var cart = new Cart { items = new List<CartLineItem>() };
            return cart;
        }
        private bool IsStockItem(CartItem item)
        {
            return item is StockItem;
        }
        private int ResetQuantityIfQuantityInStockIsLess(StockItem stockItem, int quantity)
        {
            //NOTE:  This is a good check, but needs to be after the point they could change it (or both) with a message
            //return stockItem.QuantityInStock.Is().LessThan(quantity) ? stockItem.QuantityInStock : quantity;
            return quantity;
        }

        #endregion

        public virtual void Clear()
        {
            items.Clear();
            DiscountApplied = null;
            DiscountTotal = 0;
            DiscountErrorMessage = null;
			DiscountItems = new List<DiscountItemViewModel>();
            PerformCartChangedActions();
        }


        public virtual void Refresh()
        {
            PerformCartChangedActions();
        }


        public virtual object Clone()
        {
            var cart = new Cart();
            cart.items = new List<CartLineItem>(items);
            cart.shippingMethod = shippingMethod;
            cart.DiscountApplied = DiscountApplied;
            cart.DiscountTotal = DiscountTotal;
            cart.PromotionalItems = new List<PromotionalItemViewModel>(PromotionalItems);
            cart.DiscountItems = new List<DiscountItemViewModel>(DiscountItems);
            return cart;
        }

  
        public virtual void SetDiscountTotal(decimal total)
        {
            DiscountTotal = total;
        }

        public virtual void SetDiscountApplied(DiscountCode discountCode)
        {
            DiscountApplied = discountCode;
            if (discountCode.IsNull())
            {
                ClearDiscount();
            }
        }

        public virtual void SetDiscountErrorMessage(string errorMessage)
        {
            DiscountErrorMessage = errorMessage;
        }

        [Obsolete]
        public virtual bool ApplyDiscount(DiscountCode discount, out string reason)
        {
            reason = null;
            DiscountTotal = 0;
            DiscountApplied = null;
            
            /*if (discount.StartDate > DateTime.Now)
            {
                reason = "Sorry, this promotional code cannot be used at this time";
                return false;
            }

            if (discount.EndDate < DateTime.Now)
            {
                reason = "Sorry, this promotional code is expired";
                return false;
            }

            var discountTotal = CalculateDiscountTotal(discount);

            if (discount.MaxAmountOff > 0 && discountTotal > discount.MaxAmountOff)
            {
                discountTotal = discount.MaxAmountOff;
            }

            var productsSubTotal = GetSubTotalByItemType(typeof(StockItem));
            if (productsSubTotal < discount.MinPurchaseAmount)
            {
                reason = String.Format("Sorry, this discount can only be applied to purchases of [C:{0}] or more", discount.MinPurchaseAmount);
                return false;
            }

            discount.IsDirty = false;
            DiscountTotal = discountTotal;
            DiscountApplied = discount;*/
            Refresh();
            return true;
        }

        
        [Obsolete]
        private void RecalculateAppliedDiscount()
        {
            if (DiscountApplied == null || !DiscountApplied.ActiveRule.IsDirty) return;

            string reason;
            if (!ApplyDiscount(DiscountApplied, out reason))
            {
                DiscountErrorMessage = reason + ". The discount was removed from your cart.";
            }
        }

        public virtual void ClearDiscount()
        {
            DiscountApplied = null;
            DiscountTotal = 0;
            DiscountItems.Clear();
            foreach (var cartLineItem in Items)
            {
                cartLineItem.MatchesDiscountCriteria = false;
            }

            if (CartUpdated != null)
            {
                CartUpdated(this, new CommandEventArgs("ClearDiscount", null));
            }

        }

        public virtual void ClearDiscountErrorMessage()
        {
            DiscountErrorMessage = null;
        }

        public virtual bool HasFreeShippingDiscount()
        {
            bool returnType = DiscountApplied.IsNot().Null() && DiscountApplied.ActiveRule.FreeShipping;

            return returnType;
        }

        
    }

    public class DonationCartItemExistsException : Exception
    {
        public const string DonationAlreadyExists = "Donation Already Exists";

        public DonationCartItemExistsException(DonationCartItem donationCartItem)
            : base(DonationAlreadyExists)
        {

        }

        public DonationCartItemExistsException()
        {
        }

        public DonationCartItemExistsException(string message)
            : base(message)
        {
        }

        public DonationCartItemExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DonationCartItemExistsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    public class SubscriptionCartItemExistsException: Exception
    {
        public const string AlreadyExists = "Subscription Already Exists";

        public SubscriptionCartItemExistsException(SubscriptionCartItem cartItem)
            : base(AlreadyExists)
        {
        }
    }
}