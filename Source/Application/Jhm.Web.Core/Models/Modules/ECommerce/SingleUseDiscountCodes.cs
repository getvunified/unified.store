﻿using System;
using System.Web.Script.Serialization;

namespace Jhm.Web.Core.Models
{
    public class SingleUseDiscountCodes : DefaultEntity, ISingleUseDiscountCodes
    {
        [ScriptIgnore]
        public virtual DiscountCode DiscountCode { get; set; }
        public virtual string DiscountCodeSuffix { get; set; }
        public virtual Guid DiscountCodeId { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string UserName { get; set; }
        public virtual DateTime DateUsed { get; set; }

        public SingleUseDiscountCodes(){}

        public SingleUseDiscountCodes(DiscountCode discountCode, string discountCodeSuffix, bool isActive)
        {
            DiscountCodeSuffix = discountCodeSuffix;
            DiscountCode = discountCode;
            IsActive = isActive;
        }
    }
}
