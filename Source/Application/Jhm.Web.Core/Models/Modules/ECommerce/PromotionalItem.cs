﻿using System;

namespace Jhm.Web.Core.Models
{
    public class PromotionalItem : DefaultEntity
    {
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string ProductCode { get; set; }
        public virtual string ProductTitle { get; set; }
        public virtual string ProductDescription { get; set; }
        public virtual int Quantity { get; set; }
        public virtual decimal MinimumPurchaseAmount { get; set; }
        public virtual decimal MinimumDonationAmount { get; set; }
        public virtual string DonationCode { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
