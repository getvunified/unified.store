﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public interface ISubscription
    {
        string SubscriptionCode { get; set; }
        string Description { get; set; }
        string Type { get; set; }
        string Fullfilment { get; set; }
        IEnumerable<SubscriptionPrice> Prices { get; set; }
    }

    public interface ISubscriptionPrice
    {
        string SubscriptionCode { get; set; }
        string PriceCode { get; set; }
        string Description { get; set; }
        decimal Price { get; set; }
        long NumberOfIssues { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
    }

    [Serializable]
    public class Subscription: ISubscription
    {
        public string SubscriptionCode { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Fullfilment { get; set; }
        public IEnumerable<SubscriptionPrice> Prices { get; set; }
    }

    [Serializable]
    public class SubscriptionPrice: ISubscriptionPrice
    {
        public string SubscriptionCode { get; set; }
        public string PriceCode { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public long NumberOfIssues { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
