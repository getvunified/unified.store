﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public interface IDonation : IRelateable
    {
        string PdfVersion { get; }
        string ListViewImageUrl { get; }
        string DetailViewImageUrl { get; }
        string FormViewImageUrl { get; }
        string Body { get; }
        string Title { get; }
        Guid DonationId { get; }
        string Description { get; }
        string DonationCode { get; }
        IEnumerable<DonationOption> DonationOptions { get; }
        void AddDonationOption(DonationOption donationOption);
        DonationCartItem MapSelectedOptionToNewDonationCartItem(DonationCriteria donationCriteria);

        bool IsHidden { get; set; }
        string CustomEmailBody { get; set; }

        string CustomEmailTemplateFileName { get; set; }
    }
}