﻿using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class StockItem : CartItem, ILinkableToCartItem
    {

        private string mediaType;
        

        public StockItem()
        {
        }


        public StockItem(MediaType mediaType, int quantityInStock, bool allowBackorder, decimal basePrice, decimal salePrice, bool isInHouse, string link, string warehouseCode, decimal fileLength, string mediaSize)
        {
            SetValues(mediaType, quantityInStock, allowBackorder, basePrice, salePrice, isInHouse, link, warehouseCode, fileLength, mediaSize);
        }

        public StockItem(Guid id, MediaType mediaType, int quantityInStock, bool allowBackorder, decimal basePrice, decimal salePrice, bool isInHouse, string warehouseCode)
            : base(id)
        {
            SetValues(mediaType, quantityInStock, allowBackorder, basePrice, salePrice, isInHouse, string.Empty, warehouseCode);
        }

        public StockItem(MediaType mediaType, int quantityInStock, bool allowBackorder, decimal basePrice, decimal salePrice, bool isInHouse, string warehouseCode)
        {
            SetValues(mediaType, quantityInStock, allowBackorder, basePrice, salePrice, isInHouse, String.Empty, warehouseCode);
        }

        
        private void SetValues(MediaType mediaType, int quantityInStock, bool allowBackorder, decimal basePrice, decimal salePrice, bool isInHouse, string link, string warehouseCode, decimal fileLength = 0, string mediaSize = null)
        {
            SetMediaType(mediaType);
            QuantityInStock = quantityInStock;
            AllowBackorder = allowBackorder;
            BasePrice = basePrice;
            SalePrice = salePrice;
            IsInHouse = isInHouse;
            MediaPath = link;
            SetWarehouseCode(warehouseCode);
            FileLength = fileLength;
            MediaSize = mediaSize;
        }

        public virtual bool IsInHouse { get; private set; }
        public virtual MediaType MediaType
        {
            get
            {
                return !string.IsNullOrEmpty(mediaType) ? MediaType.TryGetFromValue<MediaType>(mediaType) : MediaType.Misc;
            }
        }

        public virtual void SetMediaType(MediaType mType)
        {
            mediaType = (mType.IsNot().Null()) ? mType.Value : MediaType.Misc.Value;
        }

        public virtual int QuantityInStock { get; private set; }
        public virtual bool AllowBackorder { get; private set; }
        public virtual decimal BasePrice { get; private set; }
        public virtual decimal SalePrice { get; private set; }
        public virtual string ProductCode { get; private set; }
        public virtual string MediaPath { get; private set; }
        public virtual string AuthorDescription { get; private set; }
        public virtual void SetAuthorDescription(string value) { AuthorDescription = value; }
        public virtual decimal FileLength { get; private set; }

        public virtual DateTime ReleaseDate { get; private set; }
        public virtual void SetReleaseDate(DateTime releaseDate) { ReleaseDate = releaseDate; }

        public virtual bool AllowPreorder { get; private set; }
        public virtual void EnableAllowPreorder() { AllowPreorder = true; }
        public virtual void DisableAllowPreorder() { AllowPreorder = false; }

        public virtual bool IsOnSale() { return (SalePrice.IsNot().Null() && SalePrice != 0 && SalePrice.Is().LessThan(BasePrice)); }
        public virtual string PercentOffBasePrice() { return "{0}%".FormatWith((100 - ((SalePrice / BasePrice) * 100)).ToString("0")); }
        public virtual string GetAvailability()
        {
            //Note: this should probably be a Enumeration Object
            return QuantityInStock.Is().GreaterThan(0) || IsInHouse ? "In Stock" : AllowBackorder ? "Backordered" : "Out of Stock";
        }

        public virtual void SetProductCode(string productCode)
        {
            ProductCode = productCode;
        }

        public virtual StockItem AddProduct(Product product)
        {
            MapCartItem(product);
            return this;
        }

        private void MapCartItem(Product product)
        {
            SetProductCode(product.Code);
            //HACK:  For old product codes handle the anomalies
            if ((MediaType.CodeSuffix == MediaType.Book.CodeSuffix) || (MediaType.CodeSuffix == MediaType.Misc.CodeSuffix))
            {
                SetCode(product.Code);
            }
            else
            {
                SetCode(product.Code + MediaType.CodeSuffix);
            }
            SetProductTitle(product.Title);
            SetTitle("{0}- {1}".FormatWith(product.Title, MediaType));
            SetDescription(product.Description);
            SetAuthorDescription(product.AuthorDescription);
            SetPrice(IsOnSale() ? SalePrice : BasePrice);
            
        }

        public override bool HasShippingCost
        {
            get
            {
                return ! IsDownloadableProduct();
            }
        }

        public virtual void SetProductTitle(string productTitle)
        {
            ProductTitle = productTitle;
        }

        public virtual string ProductTitle { get; private set; }

        public virtual bool IsDownloadableProduct()
        {
                return
                    MediaType == MediaType.VideoDownloadStandardDefinition ||
                    MediaType == MediaType.VideoDownloadHighDefinition ||
                    MediaType == MediaType.VideoDownloadStandardDefinitionSpanish ||
                    MediaType == MediaType.VideoDownloadHighDefinitionSpanish ||
                    MediaType == MediaType.AudioFileDownload ||
                    MediaType == MediaType.DigitalFileDownload || 
                    MediaType == MediaType.AudioFileDownloadSpanish;

        }



        public virtual string MediaSize { get; private set; }
        public virtual CartLineItem CartLineItem { get; set; }
    }
}