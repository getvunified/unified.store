﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public enum AvailabilityTypes
    {
        In_Stock,
        Backordered,
        Out_of_Stock
    }

    public interface IProduct : IIdentifiable, IRelateable, ICloneable, ILinkableToCartItem
    {
        IEnumerable<string> AlternativeImages { get; }
        IEnumerable<string> Subjects { get; }

        IEnumerable<Category> Categories { get; }

        IEnumerable<StockItem> StockItems { get; }

        IEnumerable<StockItem> GetAvailableStockItems();

        IEnumerable<Review> Reviews { get; }

        IEnumerable<RelatedItem> RelatedItems { get; }

        bool HasTrailer();
        bool InStock();

        decimal Rating();
        decimal FileLength { get; }

        long RecordId { get; }

        string Author { get; }
        string AuthorDescription { get; }
        string Code { get; }
        string Description { get; }
        string DsType { get; }
        string Isbn { get; }
        string Packaging { get; }
        string Publisher { get; }
        string PublisherDescription { get; }
        string Status { get; }
        string Trailer { get; }
        
        bool DiscountAllowed { get; }
        bool IsFeaturedOnTelevision { get; }
        bool IsFeaturedOnGetv { get; }
        bool UseInShoppingCart { get; }

        AvailabilityTypes GetAvailability();
        string GetAvailabilityAsString();

        // Added to ease DiscountCodes's logic
        void SetStockItems(IEnumerable<StockItem> items);
    }
}