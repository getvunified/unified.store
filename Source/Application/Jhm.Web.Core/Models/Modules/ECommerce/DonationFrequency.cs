﻿using System;
using Foundation;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DonationFrequency : Enumeration<string>
    {
        public static readonly DonationFrequency OneTime = new DonationFrequency("OneTime", "Onetime Donation");
        public static readonly DonationFrequency MonthlyResidual = new DonationFrequency("Monthly", "Monthly Residual");
        public static readonly DonationFrequency QuarterlyResidual = new DonationFrequency("Quarterly", "Quarterly Residual");
        public static readonly DonationFrequency YearlyResidual = new DonationFrequency("Yearly", "Yearly Residual");

        public DonationFrequency()
        {
        }

        public DonationFrequency(string value, string displayName)
            : base(value, displayName)
        {
        }
    }


}