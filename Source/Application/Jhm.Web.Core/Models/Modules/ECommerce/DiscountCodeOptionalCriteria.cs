﻿using System;
using System.Web.Script.Serialization;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DiscountCodeOptionalCriteria : IDiscountCodeOptionalCriteria
    {
        public enum ProductOrStock
        {
            StockItem = 1,
            Product = 2
        }

        public enum SupportedDataTypes
        {
            String,
            List
        }

        public virtual Guid Id { get; set; }
        public virtual Guid DiscountCodeId { get; set; }
        public virtual string Criteria { get; set; }
        public virtual string Value { get; set; }
        public virtual string ValueDataType { get; set; }
        public virtual string AssociatedDiscountCode { get; set; }
        public virtual short ApplicableClass { get; set; }
        public virtual bool IsMandatory { get; set; }


        public virtual ProductOrStock AppliesToClass
        { 
            get { return (ProductOrStock) ApplicableClass; }
            set { ApplicableClass = (short) value; } 
        }
        
        [ScriptIgnore]
        public virtual DiscountCode DiscountCode { get; set; }

        public virtual bool Satisfied { get; set; }

        public DiscountCodeOptionalCriteria(){}
        public DiscountCodeOptionalCriteria(string criteria, string value, string valueDataType,
                                            string associatedDiscountCode, short applicableClass)
        {
            Criteria = criteria;
            Value = value;
            ValueDataType = valueDataType;
            AssociatedDiscountCode = associatedDiscountCode;
            ApplicableClass = applicableClass;
        }

    }
}
