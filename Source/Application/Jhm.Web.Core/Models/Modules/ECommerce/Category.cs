﻿using System;
using System.Collections.Generic;
using Foundation;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class Category : Enumeration<string>
    {
        public static readonly Category All = new Category("ALL", "All");
        public static readonly Category BooksBiblesDevotionals = new Category("BBD", "Books, Bibles And Devotionals");
        public static readonly Category ChildrenAndTeens = new Category("CHILD", "Children and Teens");
        public static readonly Category HealthAndHealing = new Category("HEALTH", "Health and Healing");
        public static readonly Category Interviews = new Category("INTERVIEW", "Interviews");
        public static readonly Category MoviesAndDocumentaries = new Category("MD", "Movie and Documentaries");
        public static readonly Category MiscellaneousProductOrResources = new Category("MISC", "Miscellaneous Product Or Resources");
        public static readonly Category Music = new Category("MUSIC", "Music");
        public static readonly Category SeriesOrTeaching = new Category("SERMON", "Sermon Series");
        public static readonly Category Single = new Category("SINGLE", "Sermon Singles");
        public static readonly Category Spanish = new Category("SPANISH", "En Espanol");
        public static readonly Category Women = new Category("WOMEN", "Women");

        public Category()
        {

        }

        public static readonly List<Category> AllCategories = GetAll<Category>().ToLIST();

        private Category(string value, string displayName)
            : base(value, displayName)
        {
        }


    }


}