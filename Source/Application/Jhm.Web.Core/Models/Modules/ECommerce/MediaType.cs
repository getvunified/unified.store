﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class MediaType : Enumeration<string>
    {
        public static readonly MediaType Book = new MediaType("BOOK", "Book", "BOOK");
        public static readonly MediaType Misc = new MediaType("M", "Misc", "M");
        public static readonly MediaType Signed = new MediaType("S", "Book - Signed", "S");
        public static readonly MediaType SpanishBook = new MediaType("SP", "Book - Spanish", "SP");


        public static readonly MediaType OnDemand = new MediaType("OD", "OnDemand", "OD");


        public static readonly MediaType CD = new MediaType("C", "CD", "C");
        public static readonly MediaType DVD = new MediaType("D", "DVD", "D");
        public static readonly MediaType BlueRayDisc = new MediaType("BD", "Blu-ray Disc", "BD");
        public static readonly MediaType AudioFileDownload = new MediaType("AF", "MP3 Download", "AF");
        public static readonly MediaType DigitalFileDownload = new MediaType("DF", "Digital File Download", "DF");
        public static readonly MediaType VideoDownloadStandardDefinition = new MediaType("SDVF", "MP4 Download", "SDVF");
        public static readonly MediaType VideoDownloadHighDefinition = new MediaType("HDVF", "MP4 Download - High Definition", "HDVF");
        public static readonly MediaType VideoDownloadStandardDefinitionSpanish = new MediaType("SDVFSP", "Standard Definition Video File (Spanish)", "SDVF");
        public static readonly MediaType VideoDownloadHighDefinitionSpanish = new MediaType("HDVFSP", "High Definition Video File (Spanish)", "HDVF");

        public static readonly MediaType CDSpanish = new MediaType("CSP", "CD Spanish", "CSP");
        public static readonly MediaType DVDSpanish = new MediaType("DSP", "DVD Spanish", "DSP");
        public static readonly MediaType AudioFileDownloadSpanish = new MediaType("AFSP", "Audio File Download Spanish", "AFSP");

        public static readonly MediaType BookHardBack = new MediaType("H", "Hardback", "H");
        public static readonly MediaType BookPaperback = new MediaType("P", "Paperback", "P");
        public static readonly MediaType BookLeather = new MediaType("L", "Leather", "L");
        public static readonly MediaType BookBondedLeather = new MediaType("B", "Bonded Leather", "B");

        public static readonly MediaType BookHardBackSigned = new MediaType("HS", "Hardback Signed", "HS");
        public static readonly MediaType BookPaperbackSigned = new MediaType("PS", "Paperback Signed", "PS");
        public static readonly MediaType BookLeatherSigned = new MediaType("LS", "Leather Signed", "LS");
        public static readonly MediaType BookBondedLeatherSigned = new MediaType("BS", "Bonded Leather Signed", "BS");

        public static readonly MediaType BookHardBackSpanish = new MediaType("HSP", "Hardback Spanish", "HSP");
        public static readonly MediaType BookPaperbackSpanish = new MediaType("PSP", "Paperback Spanish", "PSP");
        public static readonly MediaType BookLeatherSpanish = new MediaType("LSP", "Leather Spanish", "LSP");
        public static readonly MediaType BookBondedLeatherSpanish = new MediaType("BSP", "Bonded Leather Spanish", "BSP");

        public static readonly MediaType BookHardBackSignedSpanish = new MediaType("HSSP", "Hardback Signed Spanish", "HSSP");
        public static readonly MediaType BookPaperbackSignedSpanish = new MediaType("PSSP", "Paperback Signed Spanish", "PSSP");
        public static readonly MediaType BookLeatherSignedSpanish = new MediaType("LSSP", "Leather Signed Spanish", "LSSP");
        public static readonly MediaType BookBondedLeatherSignedSpanish = new MediaType("BSSP", "Bonded Leather Signed Spanish", "BSSP");


        public static readonly MediaType TShirtOnsie6Months = new MediaType("TB6", "06 mos", "TB6");
        public static readonly MediaType TShirtOnsie12Months = new MediaType("TB12", "12 mos", "TB12");
        public static readonly MediaType TShirtOnsie18Months = new MediaType("TB18", "18 mos", "TB18");
        public static readonly MediaType TShirtOnsie24Months = new MediaType("TB24", "24 mos", "TB24");
        
        public static readonly MediaType TShirtToddler2T = new MediaType("TT2", "2T", "TT2");
        public static readonly MediaType TShirtToddler3T = new MediaType("TT3", "3T", "TT3");
        public static readonly MediaType TShirtToddler4T = new MediaType("TT4", "4T", "TT4");
        public static readonly MediaType TShirtToddlerXS = new MediaType("TTXS", "XS (2-4)", "TTXS");

        public static readonly MediaType TShirtYouthS = new MediaType("TYS", "S (6-8)", "TYS");
        public static readonly MediaType TShirtYouthM = new MediaType("TYM", "M (10-12)", "TYM");
        public static readonly MediaType TShirtYouthL = new MediaType("TYL", "L (14-16)", "TYL");

        public static readonly MediaType TShirtAdultS = new MediaType("TAS", "S", "TAS");
        public static readonly MediaType TShirtAdultM = new MediaType("TAM", "M", "TAM");
        public static readonly MediaType TShirtAdultL = new MediaType("TAL", "L", "TAL");
        public static readonly MediaType TShirtAdultXL = new MediaType("TAXL", "XL", "TAXL");
        public static readonly MediaType TShirtAdult2XL = new MediaType("TA2X", "2XL", "TA2X");
        public static readonly MediaType TShirtAdult3XL = new MediaType("TA3X", "3XL", "TA3X");


        public virtual string CodeSuffix { get; private set; }


        public MediaType()
        {
        }

        public MediaType(string value, string displayName, string codeSuffix)
            : base(value, displayName)
        {
            CodeSuffix = codeSuffix;
        }


        public static readonly List<MediaType> All = GetAll<MediaType>().ToLIST();


        public static MediaType GetMediaTypeByCodeSuffix(string codeSuffix)
        {
            foreach (var mediaType in All.Where(mediaType => mediaType.CodeSuffix == codeSuffix))
                return mediaType;
            return Misc; //Return Misc as the "Default" MediaType
        }
    }


}