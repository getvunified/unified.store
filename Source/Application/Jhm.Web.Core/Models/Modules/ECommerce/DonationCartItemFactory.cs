﻿using System;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public static class DonationCartItemFactory
    {
       

        public static DonationCartItem CreateFrom(IDonation donation, DonationCriteria donationCriteria)
        {
            return donation.MapSelectedOptionToNewDonationCartItem(donationCriteria);
        }

        public static DonationCartItem CreateFrom(IDonation donation, FormCollection formCollection)
        {
            if(formCollection[DonationFormViewModel.SelectedChoiceName].Is().Null())
                throw new ArgumentException("SelectedChoice ID is required.");

            var selectedChoiceId = new Guid(formCollection[DonationFormViewModel.SelectedChoiceName]);
            
                

            var option = donation.DonationOptions.ToLIST().Find(x => x.Id == selectedChoiceId);

            if (option.Is().Null())
                throw new ArgumentException("Unable to find donation option choice by 'ID:{0}'.".FormatWith(selectedChoiceId));

            var criteria = CreateDonationCritieriaOf(option, formCollection);

            return CreateFrom(donation, criteria);
        }

        private static DonationCriteria CreateDonationCritieriaOf(DonationOption option, FormCollection formCollection)
        {
            if (option is AnyAmountDonationOption)
                return GetAnyAmountDonationCriteria(option, formCollection);
            if (option is FixedAmountDonationOption)
                return GetFixedAmountDonationCriteria(option, formCollection);
            if (option is MultipleOfdonationOption)
                return GetMultipleOfdonationCriteria(option, formCollection);
            if (option is AmountRangeDonationOption)
                return GetAmountRangeDonationCriteria(option, formCollection);
            if (option is MinimumAmountDonationOption)
                return GetMinimumAmountDonationCriteria(option, formCollection);




            throw new ArgumentException("DonationOption is of unknown type.");
        }
        private static DonationCriteria GetBaseDonationCriteriaInfo(DonationOption option, FormCollection formCollection)
        {
            return DonationCriteriaFactory.DonationCriteria(option.Id,
                                                            formCollection[DonationFormViewModel.CertificateTextBoxName(option.Id)],
                                                            TryGetDonationFrequency(option, formCollection),
                                                            formCollection[DonationFormViewModel.SelectedStockItemChoiceListName(option.Id)].TryConvertTo<Guid>()
                                                                  );
        }

        public static DonationFrequency TryGetDonationFrequency(DonationOption option, FormCollection formCollection)
        {
            DonationFrequency donationFrequency = null;
            try
            {
                donationFrequency = DonationFrequency.FromDisplayName<DonationFrequency>(formCollection[DonationFormViewModel.DonationFrequencySelectListName(option.Id)]);
            }
            catch { }

            return donationFrequency;
        }


        private static DonationCriteria GetAnyAmountDonationCriteria(DonationOption option, FormCollection formCollection)
        {
            var baseCriteria = GetBaseDonationCriteriaInfo(option, formCollection);
            return DonationCriteriaFactory.AmountDonationCriteria(baseCriteria.DonationOptionId,
                                                                  formCollection[DonationFormViewModel.AmountTextBoxName(option.Id)].TryConvertTo<decimal>(),
                                                                  baseCriteria.CertificateName,
                                                                  baseCriteria.DonationFrequency
                                                                  );
        }



        private static DonationCriteria GetFixedAmountDonationCriteria(DonationOption option, FormCollection formCollection)
        {
            return GetBaseDonationCriteriaInfo(option, formCollection);
        }

        private static DonationCriteria GetMultipleOfdonationCriteria(DonationOption option, FormCollection formCollection)
        {
            var baseCriteria = GetBaseDonationCriteriaInfo(option, formCollection);
            return DonationCriteriaFactory.MultipleOfDonationCriteria(baseCriteria.DonationOptionId,
                                                                  formCollection[DonationFormViewModel.QuantityTextBoxName(option.Id)].TryConvertTo<int>(),
                                                                  baseCriteria.CertificateName,
                                                                  baseCriteria.DonationFrequency
                                                                  );
        }

        private static DonationCriteria GetAmountRangeDonationCriteria(DonationOption option, FormCollection formCollection)
        {
            return GetAnyAmountDonationCriteria(option, formCollection);
        }

        private static DonationCriteria GetMinimumAmountDonationCriteria(DonationOption option, FormCollection formCollection)
        {
            return GetAnyAmountDonationCriteria(option, formCollection);
        }

    }
}