﻿using System;
using Foundation;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class ReviewStatus : Enumeration<string>
    {
        public static readonly ReviewStatus Active = new ReviewStatus("ACTIVE", "Active");
        public static readonly ReviewStatus InActive = new ReviewStatus("INACTIVE", "InActive");

        public ReviewStatus()
        {
        }

        public ReviewStatus(string value, string displayName)
            : base(value, displayName)
        {
        }
    }
}