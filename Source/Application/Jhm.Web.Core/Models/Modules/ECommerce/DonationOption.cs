﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Jhm.Common.Framework.Extensions;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class DonationOptionException : ArgumentException
    {
        public DonationOptionException()
        {
        }

        public DonationOptionException(string message)
            : base(message)
        {
        }

        public DonationOptionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public DonationOptionException(string message, string paramName, Exception innerException)
            : base(message, paramName, innerException)
        {
        }

        public DonationOptionException(string message, string paramName)
            : base(message, paramName)
        {
        }

        protected DonationOptionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    public class DonationOption : DefaultEntity
    {
        private List<StockItem> stockItems = new List<StockItem>();
        private List<StockItem> includedStockItems = new List<StockItem>();
        private decimal amount;
        private List<string> donationFrequencyOptions = new List<string>() { DonationFrequency.OneTime.Value };
        private List<StockItem> stockItemChoices = new List<StockItem>();
        private string minimumShippingMethod;



        public DonationOption(Guid id)
            : base(id)
        {

        }

        public DonationOption()
        {
        }

        public virtual decimal Amount
        {
            get { return amount; }
            set
            {
                if (value.IsNot().GreaterThan(0))
                    throw new DonationOptionException("Value must be greater than ZERO.", "Amount");

                amount = value;
            }
        }

        public virtual bool AllowAmountOutsideOfRange { get; private set; }
        public virtual IEnumerable<StockItem> StockItems { get { return stockItems; } }
        public virtual IEnumerable<StockItem> IncludedStockItems { get { return includedStockItems; } }
        public virtual IEnumerable<StockItem> StockItemChoices { get { return stockItemChoices; } }
        public virtual bool ResidualEnabled { get; private set; }
        public virtual IEnumerable<DonationFrequency> DonationFrequencyOptions { get { return donationFrequencyOptions.Select(donationFrequencyOption => !string.IsNullOrEmpty(donationFrequencyOption) ? DonationFrequency.TryGetFromValue<DonationFrequency>(donationFrequencyOption) : DonationFrequency.OneTime).ToList(); } }
        public virtual int SortOrder { get; private set; }
        public virtual string Title { get; private set; }
        public virtual string Description { get; private set; }
        public virtual string ImageUrl { get; private set; }



        #region Donation Option Fields (Certificates)

        public DonationOptionField OptionField { get; private set; }

        public virtual void EnableOptionField()
        {
            if (OptionField != null) OptionField.SetIsEnabled(true);
        }

        public virtual void DisableOptionField()
        {
            UnRequireOptionField();
            if (OptionField != null) OptionField.SetIsEnabled(false);
        }

        public virtual bool OptionFieldIsEnabled()
        {
            return OptionField != null && OptionField.IsEnabled;
        }

        public virtual bool OptionFieldIsRequired()
        {
            return OptionField != null && OptionField.IsRequired;
        }
        public virtual void SetCertificateName(DonationCartItem donationCartItem, string certificateName)
        {
            if (CanSetCertificateName(certificateName))
                donationCartItem.SetCertificateName(certificateName);
        }
        public virtual bool CanSetCertificateName(string certificateName)
        {
            if (!OptionFieldIsEnabled() && certificateName.IsNot().NullOrEmpty())
                throw new DonationOptionException((OptionField != null && !String.IsNullOrEmpty(OptionField.FieldName) ? OptionField.FieldName : "Certificate") + " IS NOT enabled.");

            if (OptionFieldIsEnabled() && OptionFieldIsRequired() && certificateName.Is().NullOrEmpty())
                throw new DonationOptionException((String.IsNullOrEmpty(OptionField.FieldName) ? "Certificate" : OptionField.FieldName) + " NAME is required.");

            var certValues = certificateName.IsNot().NullOrEmpty() ? certificateName.TrimEnd(',').Split(',') : new string[] { };
            if (certValues.Any(x => x.Is().NullOrEmpty()))
            {
                throw new DonationOptionException((String.IsNullOrEmpty(OptionField.FieldName) ? "Certificate" : OptionField.FieldName) + " NAME is required.");
            }

            return certificateName.IsNot().NullOrEmpty();
        }
        private void UnRequireOptionField()
        {
            if (OptionField != null) OptionField.SetIsRequired(false);
        }

        public virtual void MarkOptionFieldAsRequired()
        {
            EnableOptionField();
            if (OptionField != null) OptionField.SetIsRequired(true);
        }

        public void EnableRequireCertificateField(int maxLength = -1)
        {
            SetOptionField("Please enter name for certificate:", "Certificate", maxLength);
        }

        public void SetRequiredOptionField(string labelText, int maxLength = -1)
        {
            SetOptionField(labelText, "Field", maxLength);
        }

        public void SetRequiredOptionField(string name, string labelText, int maxLength = -1)
        {
            SetOptionField(labelText, name, maxLength);
        }

        public void SetOptionField(string labelText, string name = null, int maxLength = -1, bool isRequired = true, bool isEnabled = true)
        {
            var field = new DonationOptionField(name, labelText, isRequired, isEnabled, maxLength);
            SetOptionField(field);
        }
        public void SetOptionField(DonationOptionField field)
        {
            OptionField = field;
        }
        #endregion

        public virtual void EnableAmountOutsideOfRange()
        {
            AllowAmountOutsideOfRange = true;
        }
        public virtual void DisableAmountOutsideOfRange()
        {
            AllowAmountOutsideOfRange = false;
        }

        public virtual void AddIncludedStockItem(StockItem stockItem)
        {
            includedStockItems.AddUnique(stockItem);
        }
        public virtual void ClearIncludedStockItems()
        {
            includedStockItems.Clear();
        }
        public virtual void AddStockItem(StockItem stockItem, params StockItem[] stockItems)
        {
            if (stockItem.IsNot().Null())
                this.stockItems.AddUnique(stockItem);

            foreach (var item in stockItems)
            {
                this.stockItems.AddUnique(item);
            }
        }
        public virtual void RemoveStockItem(StockItem stockItem, params StockItem[] stockItems)
        {
            if (stockItem.IsNot().Null())
                this.stockItems.Remove(stockItem);

            foreach (var item in stockItems)
            {
                this.stockItems.Remove(item);
            }
        }
        public virtual void AddStockItemChoice(StockItem stockItem, params StockItem[] stockItems)
        {
            if (stockItem.IsNot().Null())
                this.stockItemChoices.AddUnique(stockItem);

            foreach (var item in stockItems)
            {
                this.stockItemChoices.AddUnique(item);
            }
        }
        public virtual void RemoveStockItemChoice(StockItem stockItem, params StockItem[] stockItems)
        {
            if (stockItem.IsNot().Null())
                this.stockItemChoices.Remove(stockItem);

            foreach (var item in stockItems)
            {
                this.stockItemChoices.Remove(item);
            }
        }
        private void EnableResidual() { ResidualEnabled = true; }
        public virtual void DisableResidual()
        {
            ResidualEnabled = false;
            donationFrequencyOptions = new List<string>() { DonationFrequency.OneTime.Value };
        }
        public virtual void DisableOneTimeDonationFrequency()
        {
            RemoveDonationFrequency(DonationFrequency.OneTime);
        }
        public virtual void SetDonationFrequency(DonationCartItem donationCartItem, DonationFrequency donationFrequency)
        {
            if (donationFrequency.Is().Null())
                donationFrequency = DonationFrequency.OneTime;

            if (CanSetDonationFrequency(donationFrequency))
                donationCartItem.SetDonationFrequency(donationFrequency);
        }
        public virtual bool CanSetDonationFrequency(DonationFrequency donationFrequency)
        {
            if (donationFrequency.Is().Null())
                donationFrequency = DonationFrequency.OneTime;

            if (!ResidualEnabled && donationFrequency != DonationFrequency.OneTime)
                throw new DonationOptionException("Residual IS NOT enabled.");

            if (!donationFrequencyOptions.Contains(donationFrequency.Value))
                throw new DonationOptionException("Donation Frequency [{0}] is unavailable.".FormatWith(donationFrequency.DisplayName));

            return true;
        }
        public virtual void SetChosenStockItem(DonationCartItem donationCartItem, Guid chosenStockItemId)
        {
            if (!CanSetChosenStockItem(chosenStockItemId)) return;

            var stockItem = stockItemChoices.Find(x => x.Id == chosenStockItemId);
            donationCartItem.SetStockItemChosen(stockItem);
        }
        public virtual bool CanSetChosenStockItem(Guid chosenStockItemId)
        {
            var stockItem = stockItemChoices.Find(x => x.Id == chosenStockItemId);

            if (stockItem.IsNot().Null()) return true;

            if (stockItemChoices.Count.Is().GreaterThan(0))
                if (chosenStockItemId != Guid.Empty)
                    throw new DonationOptionException("Unable to find your StockItem choice with ID: {0}.".FormatWith(chosenStockItemId));
                else
                    throw new DonationOptionException("Please choose a StockItem.");

            return false;
        }
        public virtual void SetSortOrder(int sortOrder)
        {
            SortOrder = sortOrder;
        }
        public virtual void SetTitle(string title)
        {
            Title = title;
        }
        public virtual void SetDescription(string description)
        {
            Description = description;
        }
        public virtual void SetImageUrl(string imageUrl)
        {
            ImageUrl = imageUrl;
        }
        public virtual void AddDonationFrequency(DonationFrequency donationFrequency, params DonationFrequency[] donationFrequencyOptions)
        {
            if (donationFrequency.IsNot().Null() || donationFrequencyOptions.ToLIST().Count > 0)
                EnableResidual();

            if (donationFrequency.IsNot().Null())
                this.donationFrequencyOptions.AddUnique(donationFrequency.Value);

            foreach (var item in donationFrequencyOptions)
            {
                this.donationFrequencyOptions.AddUnique(item.Value);
            }
        }
        public virtual void RemoveDonationFrequency(DonationFrequency donationFrequency, params DonationFrequency[] donationFrequencyOptions)
        {
            if (donationFrequency.IsNot().Null())
                this.donationFrequencyOptions.Remove(donationFrequency.Value);

            foreach (var item in donationFrequencyOptions)
            {
                this.donationFrequencyOptions.Remove(item.Value);
            }
        }
        public virtual void SetMinimumShippingMethod(ShippingMethod shippingMethod)
        {
            minimumShippingMethod = shippingMethod.Value;
        }
        public virtual IEnumerable<ShippingMethod> GetAllowedShippingMethods()
        {
            if (minimumShippingMethod.Is().Null())
                //return ShippingMethod.GetAll<ShippingMethod>();
                return ShippingMethod.GetUnitedStatesShipMethods(false); // TODO: should return based on Company

            //return ShippingMethod.GetAll<ShippingMethod>().Where(x => x.Price >= ShippingMethod.TryGetFromValue<ShippingMethod>(minimumShippingMethod).Price);
            // TODO: should return based on Company
            return ShippingMethod.GetUnitedStatesShipMethods(false).Where(x => x.Price >= ShippingMethod.TryGetFromValue<ShippingMethod>(minimumShippingMethod).Price);
        }
        public virtual void ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria)
        {
            SetCertificateName(donationCartItem, donationCriteria.CertificateName);
            SetDonationFrequency(donationCartItem, donationCriteria.DonationFrequency);
            SetChosenStockItem(donationCartItem, donationCriteria.StockItemChoiceId);
            SetIncludedDonationStockItems(donationCartItem);
            OverrideThis_ApplyDonationCriteria(donationCartItem, donationCriteria);
            donationCartItem.SetPrice(Amount);
            
        }
        private void SetIncludedDonationStockItems(DonationCartItem donationCartItem)
        {

            if (includedStockItems.Count.Is().GreaterThan(0))
                donationCartItem.SetIncludedStockItems(includedStockItems);
        }
        protected virtual void OverrideThis_ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria) { }
        public T ConvertTo<T>(DonationCriteria donationCriteria) where T : class
        {
            var critiera = (donationCriteria as T);

            if (critiera.Is().Null())
                throw new DonationOptionException("donationCriteria must be of type {0}.".FormatWith(typeof(T)));
            return critiera;
        }


        public static string ReplaceDonationFormat(string str, Modules.Client client)
        {
            return FormatHelper.ReplaceCurrencyFormat(str, client);
        }
    }

    public class AnyAmountDonationOption : DonationOption
    {

        public AnyAmountDonationOption(Guid id)
            : base(id)
        {
        }

        public AnyAmountDonationOption()
        {
        }

        /// <summary>
        /// Value must defined using the ApplyDonationCriteria method.
        /// </summary>
        public decimal Amount
        {
            get { return base.Amount; }

            set
            {
                throw new DonationOptionException("Value must defined using the ApplyDonationCriteria method.", "Amount");
            }
        }

        public void SetAmount(decimal amount)
        {
            base.Amount = amount;
        }

        protected override void OverrideThis_ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria)
        {
            var criteria = ConvertTo<AmountDonationCriteria>(donationCriteria);
            SetAmount(criteria.DonationAmount);
        }
    }

    public class FixedAmountDonationOption : DonationOption
    {

        public FixedAmountDonationOption(Guid id, decimal amount)
            : base(id)
        {
            Amount = amount;
        }

        public FixedAmountDonationOption()
        {
        }

    }

    public class MultipleOfdonationOption : DonationOption
    {
        public MultipleOfdonationOption()
        {
        }
        public MultipleOfdonationOption(Guid id, decimal pricePerMultiple)
            : base(id)
        {
            PricePerMultiple = pricePerMultiple;
        }

        /// <summary>
        /// Value must defined using the ApplyDonationCriteria method.
        /// </summary>
        public virtual decimal Amount
        {
            get { return base.Amount; }

            private set { base.Amount = value; }
        }

        public virtual decimal PricePerMultiple { get; private set; }
        public virtual bool CertificateIsRelatedToMultiple { get; private set; }
        public virtual void EnableCertificateRelatedMultiple()
        {
            CertificateIsRelatedToMultiple = true;
            EnableOptionField();
            MarkOptionFieldAsRequired();
        }


        protected override void OverrideThis_ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria)
        {
            var criteria = ConvertTo<MultipleOfDonationCriteria>(donationCriteria);

            SetQuantity(criteria.Multiple);
            UpdateAmount();
        }

        private void UpdateAmount()
        {
            Amount = PricePerMultiple * Quantity;
        }

        protected int Quantity { get; private set; }
        public virtual void SetQuantity(int quantity)
        {
            if (quantity.IsNot().GreaterThan(0))
                throw new DonationOptionException("Quantity must be greater than ZERO.");

            Quantity = quantity;
        }


    }

    public class AmountRangeDonationOption : DonationOption
    {
        public AmountRangeDonationOption(Guid id, decimal minAmount, decimal maxAmount = Int32.MaxValue)
            : base(id)
        {
            MinAmount = minAmount;
            MaxAmount = maxAmount;
        }


        public virtual decimal MinAmount { get; private set; }
        public virtual decimal MaxAmount { get; private set; }

        /// <summary>
        /// Value must set using the ApplyDonationCriteria method.
        /// </summary>
        public decimal Amount
        {
            get { return base.Amount; }

            private set
            {
                if (value.IsNot().Between(MinAmount, MaxAmount, allowEqualTo: true))
                {
                    if (!AllowAmountOutsideOfRange)
                        throw new DonationOptionException("Value must be between [C:{0}] and [C:{1}]".FormatWith(MinAmount, MaxAmount));

                    ClearIncludedStockItems();
                }

                base.Amount = value;
            }
        }

        protected override void OverrideThis_ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria)
        {
            var criteria = ConvertTo<AmountDonationCriteria>(donationCriteria);

            SetAmount(criteria.DonationAmount);
        }

        public void SetAmount(decimal amount)
        {
            Amount = amount;
        }
    }

    public class MinimumAmountDonationOption : DonationOption
    {
        public MinimumAmountDonationOption(Guid id, decimal minAmount)
            : base(id)
        {
            MinAmount = minAmount;
        }


        public virtual decimal MinAmount { get; private set; }


        /// <summary>
        /// Value must set using the ApplyDonationCriteria method.
        /// </summary>
        public decimal Amount
        {
            get { return base.Amount; }

            private set
            {
                if (value.IsNot().GreaterThan(MinAmount, allowEqualTo: true))
                {
                    if (!AllowAmountOutsideOfRange)
                        throw new DonationOptionException("Value must be greater than or equal to [C:{0}].".FormatWith(MinAmount));


                    ClearIncludedStockItems();
                }


                base.Amount = value;
            }
        }

        protected override void OverrideThis_ApplyDonationCriteria(DonationCartItem donationCartItem, DonationCriteria donationCriteria)
        {
            var criteria = ConvertTo<AmountDonationCriteria>(donationCriteria);

            SetAmount(criteria.DonationAmount);
        }

        public void SetAmount(decimal amount)
        {
            Amount = amount;
        }


    }

    public class DonationOptionField
    {
        public virtual string FieldName { get; private set; }
        public virtual string LabelText { get; private set; }
        public virtual bool IsEnabled { get; private set; }
        public virtual bool IsRequired { get; private set; }
        public virtual int MaxLength { get; private set; }

        public DonationOptionField(string fieldName, string labelText, bool isEnabled, bool isRequired, int maxLength)
        {
            LabelText = labelText;
            IsEnabled = isEnabled;
            IsRequired = isRequired;
            FieldName = fieldName;
            MaxLength = maxLength;
        }

        public void SetIsEnabled(bool value)
        {
            IsEnabled = value;
        }

        public void SetIsRequired(bool value)
        {
            IsRequired = value;
        }

        public void SetLabelText(string value)
        {
            LabelText = value;
        }

        public void SetFieldName(string value)
        {
            FieldName = value;
        }

        public void SetMaxLength(int value)
        {
            MaxLength = value;
        }
    }
}