﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jhm.Web.Core.Models.ECommerce
{
    public class GiftCardCartItem : CartItem
    {
        public GiftCardCartItem(){}

        public GiftCardCartItem(Guid id)
            : base(id)
        {
            // TODO: Complete member initialization
        }

        public override bool HasShippingCost
        {
            get
            {
                //Shipping cost is included
                return false;
            }
        }

        public OrderGiftCardViewModel GiftCardFormInfo { get; set; }

        public void MapCartItem(OrderGiftCardViewModel from)
        {
            SetCode(from.GiftCard.Id.ToString() + from.CardValue + from.DeliveryAddress.Address1 + from.DeliveryAddress.City + from.DeliveryAddress.StateCode);
            SetDescription(from.ShoppingCartDescription);
            SetPrice(from.CardValue);
            SetTitle(from.GiftCard.Title);
            SetImageLocation(from.GiftCard.SmallImagePath);
            SetWarehouseCode(from.GiftCard.Warehouse);
        }
    }
}
