﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    [Serializable]
    public class Product : DefaultEntity, IProduct
    {
        private List<string> alternativeImages = new List<string>();
        private List<StockItem> stockItems = new List<StockItem>();
        private List<Review> reviews = new List<Review>();
        private List<RelatedItem> relatedItems = new List<RelatedItem>();
        private List<string> categories = new List<string>();
        private List<string> subjects = new List<string>();

        #region Constructors

        public Product(string code)
        {
            Code = code;
        }
        public Product() { }
        public Product(string code, IEnumerable<Category> categories)
        {
            Code = code;
            if (categories.Is().Null())
                categories = new List<Category>();
            foreach (var category in categories)
            {
                this.categories.AddUnique(category.Value);
            }
        }

        #endregion

        #region Public Methods
        public virtual IEnumerable<Category> Categories
        {
            get
            {
                return categories.Select(Category.TryGetFromValue<Category>).ToList();
            }
        }
        public virtual void AddCategory(Category category, params Category[] categories)
        {
            if (category.IsNot().Null())
            {
                this.categories.AddUnique(category.Value);
            }

            foreach (var cat in categories)
            {
                this.categories.AddUnique(cat.Value);
            }
        }
        public virtual IEnumerable<string> Subjects { get { return subjects; } }
        public virtual void AddSubject(string subject, params string[] subjects)
        {
            if (subject.IsNot().Null())
            {
                this.subjects.AddUnique(subject);
            }

            foreach (var sub in subjects)
            {
                this.subjects.AddUnique(sub);
            }
        }
        public virtual IEnumerable<string> AlternativeImages { get { return alternativeImages; } }
        public virtual string Trailer { get; private set; }
        public virtual bool HasTrailer() { return Trailer.IsNot().NullOrEmpty(); }
        public virtual IEnumerable<StockItem> StockItems { get { return stockItems; } }
        public virtual IEnumerable<StockItem> GetAvailableStockItems()
        {
            return stockItems.FindAll(x => x.QuantityInStock.Is().GreaterThan(0) || x.AllowBackorder || x.IsInHouse);
        }
        public virtual IEnumerable<Review> Reviews { get { return reviews; } }
        public virtual decimal Rating()
        {
            if (Reviews.Count() == 0)
                return 0;
            return Reviews.Sum(x => x.Rating) / Reviews.Count();
            //Reviews.Sum(review => review.Rating / Reviews.Count());
        }


        public virtual string Author { get; private set; }
        public virtual string AuthorDescription { get; private set; }
        public virtual decimal FileLength { get; private set; }
        public virtual long RecordId { get; private set; }
        public virtual string Isbn { get; private set; }
        public virtual string DsType { get; private set; }
        public virtual string Status { get; private set; }
        public virtual string Packaging { get; private set; }
        public virtual bool UseInShoppingCart { get; private set; }
        public virtual bool DiscountAllowed { get; private set; }

        public virtual AvailabilityTypes GetAvailability()
        {
            return StockItems.Where( x => ((x.QuantityInStock > 0) || (x.IsInHouse)) ).Count().Is().GreaterThan(0) ? 
                AvailabilityTypes.In_Stock : 
                (StockItems.Where(x => x.AllowBackorder).Count().Is().GreaterThan(0) ? 
                    AvailabilityTypes.Backordered : 
                    AvailabilityTypes.Out_of_Stock
                );
        }

        public virtual string GetAvailabilityAsString()
        {
            return GetAvailability().ToString().Replace('_', ' ');
        }

        public virtual IEnumerable<RelatedItem> RelatedItems { get { return relatedItems; } }
        public virtual string Code { get; private set; }
        public virtual string Title { get; private set; }
        public virtual string Description { get; private set; }
        public virtual string Publisher { get; private set; }
        public virtual string PublisherDescription { get; private set; }

        public virtual DateRange PublishDateRange { get; private set; }
        public virtual void SetPublishDateRange(DateRange dateRange) { PublishDateRange = dateRange; }
        public virtual void SetPublisher(string publisher) { Publisher = publisher; }
        public virtual void SetPublisherDescription(string publisherDescription) { PublisherDescription = publisherDescription; }
        public virtual void SetTrailer(string trailerUrl)
        {
            if (trailerUrl.IsNot().NullOrEmpty())
                Trailer = trailerUrl.Replace(@"W:", @"https://media.jhm.org/Jhm.Web").Replace(@"\", @"/");
        }

        public virtual bool IsFeaturedOnTelevision { get; private set; }
        public virtual void SetIsFeaturedOnTelevision(bool value)
        {
            IsFeaturedOnTelevision = value;
        }

        public virtual bool IsFeaturedOnGetv { get; private set; }
        public virtual void SetIsFeaturedOnGetv(bool value)
        {
            IsFeaturedOnGetv = value;
        }

        public virtual void AddStockItems(StockItem stockItem, params StockItem[] stockItems)
        {
            if (stockItem.IsNot().Null())
            {
                this.stockItems.AddUnique(stockItem);
                stockItem.AddProduct(this);
            }

            foreach (var item in stockItems.Where(item => item.IsNot().Null()))
            {
                this.stockItems.AddUnique(item);
                item.AddProduct(this);
            }
        }
        public virtual bool InStock()
        {
            return stockItems.FindAll(x => x.QuantityInStock.Is().GreaterThan(0) || x.IsInHouse).Count.Is().GreaterThan(0);
        }
        public virtual void AddReview(Review review, params Review[] reviews)
        {
            if (review.IsNot().Null())
                this.reviews.AddUnique(review);

            foreach (var r in Enumerable.Where(reviews, r => r.IsNot().Null()))
            {
                this.reviews.AddUnique(r);
            }
        }
        public virtual void SetAuthor(string author) { Author = author; }
        public virtual void SetAuthorDescription(string authorDescription) { AuthorDescription = authorDescription; }
        public virtual void AddRelatedItems(RelatedItem relatedItem, params RelatedItem[] relatedItems)
        {
            if (relatedItem.IsNot().Null())
            {
                if ( relatedItem.Image.IsNot().NullOrEmpty() )
                    relatedItem.Image = relatedItem.Image.Replace(@"W:", @"https://media.jhm.org/Jhm.Web").Replace(@"\", @"/");
                this.relatedItems.AddUnique(relatedItem);
            }

            foreach (var r in relatedItems.Where(r => r != null))
            {
                if (r.Image.IsNot().NullOrEmpty())
                    r.Image = r.Image.Replace(@"W:", @"https://media.jhm.org/Jhm.Web").Replace(@"\", @"/");
                if (this.relatedItems.Find(x => x.Code == r.Code) == null)
                    this.relatedItems.Add(r);
            }
        }
        public virtual void SetTitle(string title)
        {
            Title = title;
        }
        public virtual void SetDescription(string description)
        {
            Description = description;
        }
        public virtual void AddImages(string imageUrl, params string[] imageUrls)
        {
            if (imageUrl.IsNot().NullOrEmpty())
                alternativeImages.AddUnique(imageUrl.Replace(@"W:", @"https://media.jhm.org/Jhm.Web").Replace(@"\", @"/"));

            foreach (var url in Enumerable.Where(imageUrls, url => url.IsNot().NullOrEmpty()))
            {
                alternativeImages.AddUnique(url);
            }
        }

        #endregion


        public virtual void SetFileLength(decimal fileLength)
        {
            FileLength = fileLength;
        }
        public virtual void SetRecordId(long recordId)
        {
            RecordId = recordId;
        }
        public virtual void SetIsbn(string isbn)
        {
            Isbn = isbn;
        }
        public virtual void SetDsType(string dsType)
        {
            DsType = dsType;
        }
        public virtual void SetStatus(string status)
        {
            Status = status;
        }
        public virtual void SetPackaging(string packaging)
        {
            Packaging = packaging;
        }
        public virtual void SetUseInShoppingCart(bool useInShoppingCart)
        {
            UseInShoppingCart = useInShoppingCart;
        }
        public virtual void SetDiscountAllowed(bool discountAllowed)
        {
            DiscountAllowed = discountAllowed;
        }


        public virtual void SetStockItems(IEnumerable<StockItem> items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            stockItems = items.ToList();
        }

        public virtual object Clone()
        {
            return new Product
                {
                    alternativeImages = alternativeImages,
                    Author = Author,
                    AuthorDescription = AuthorDescription,
                    categories = categories,
                    Code = Code,
                    Description = Description,
                    DiscountAllowed = DiscountAllowed,
                    DsType = DsType,
                    FileLength = FileLength,
                    IsFeaturedOnGetv = IsFeaturedOnGetv,
                    Isbn = Isbn,
                    IsFeaturedOnTelevision = IsFeaturedOnTelevision,
                    Packaging = Packaging,
                    PublishDateRange = PublishDateRange,
                    Publisher = Publisher,
                    PublisherDescription = PublisherDescription,
                    RecordId = RecordId,
                    relatedItems = relatedItems,
                    reviews = reviews,
                    Status = Status,
                    stockItems = stockItems,
                    subjects = subjects,
                    Title = Title,
                    Trailer = Trailer,
                    UseInShoppingCart = UseInShoppingCart
                };
        }

        public virtual CartLineItem CartLineItem { get; set; }
    }
}