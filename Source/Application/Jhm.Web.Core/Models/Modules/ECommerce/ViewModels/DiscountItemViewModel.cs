﻿using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class DiscountItemViewModel
    {
        public IProduct Product { get; set; }
        public short Quantity { get; set; }

        public decimal MinimumPurchaseAmount { get; set; }

        public virtual string GetDescription(int truncateAtLength)
        {
            if (Product.Description.IsNot().NullOrEmpty() && Product.Description.Length.Is().GreaterThan(truncateAtLength))
                return Product.Description.Substring(0, truncateAtLength) + "...";
            return Product.Description;
        }

        public string ProductWarehouse { get; set; }
    }
}