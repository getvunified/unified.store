﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Jhm.Web.Core.Models.ECommerce;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models.ECommerce
{
    public enum GiftCardDeliveryTypes
    {
        NotSet = 0,
        ToOwnAddress = 1,
        ToSomeoneElse = 2
    }


    public class OrderGiftCardViewModel
    {
        public Guid GiftCardId { get; set; }
        public GiftCard GiftCard { get; set; }
        [DisplayName("Card Value")]
        public decimal CardValue { get; set; }
        [DisplayName("Number Of Cards")]
        public short NumberOfCards { get; set; }

        public GiftCardDeliveryTypes CardDeliveryType { get; set; }

        public bool IsForOwnAddress
        {
            get { return CardDeliveryType == GiftCardDeliveryTypes.ToOwnAddress; }
            set { CardDeliveryType = value ? GiftCardDeliveryTypes.ToOwnAddress : CardDeliveryType; }
        }
        public bool IsForSomeoneElse
        {
            get { return CardDeliveryType == GiftCardDeliveryTypes.ToSomeoneElse; }
            set { CardDeliveryType = value ? GiftCardDeliveryTypes.ToSomeoneElse : CardDeliveryType; }
        }

        [DisplayName("Recipient's First Name")]
        public string RecipientFirstName { get; set; }
        [DisplayName("Recipient's Last Name")]
        public string RecipientLastName { get; set; }


        [DisplayName("Gift Message")]
        public string GiftMessage { get; set; }
        [DisplayName("Gift Message From")]
        public string GiftMessageFrom { get; set; }
        [DisplayName("Gift Message To")]
        public string GiftMessageTo { get; set; }

        private Address _deliveryAddress;
        private IList<Address> _addressList;

        public Address DeliveryAddress
        {
            get { return (_deliveryAddress = _deliveryAddress ?? new Address()); }
            set { _deliveryAddress = value; }
        }

        public string ShoppingCartDescription { get; set; }

        public long? DeliveryAddressId { get; set; }

        public IList<Address> AddressList
        {
            get { return (_addressList = _addressList?? new List<Address>()); }
            set { _addressList = value; }
        }

        public User User { get; set; }
    }
}