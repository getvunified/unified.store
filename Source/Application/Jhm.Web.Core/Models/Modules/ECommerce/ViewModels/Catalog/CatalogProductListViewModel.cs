﻿using System.Collections.Generic;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CatalogProductListViewModel : CartViewModel
    {
        private IEnumerable<IProduct> products;


        protected CatalogProductListViewModel(IEnumerable<IProduct> products)
        {
            this.products = products.ToLIST();
        }

        public IEnumerable<IProduct> Products { get { return products; } }


    }
}