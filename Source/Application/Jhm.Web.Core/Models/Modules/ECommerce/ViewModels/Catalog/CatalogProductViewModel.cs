using System;
using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CatalogProductViewModel : CartViewModel
    {

        protected CatalogProductViewModel(IProduct product)
        {
            MapToView(product);
        }

        public static CatalogProductViewModel With(IProduct product)
        {
            return new CatalogProductViewModel(product);
        }

        public IProduct OriginalProduct { get; set; }
        public string Code { get; private set; }
        public string Title { get; private set; }
        public IEnumerable<string> AlternativeImages { get; private set; }
        public bool HasTrailer { get; private set; }
        public string Trailer { get; private set; }
        public bool InStock { get; private set; }
        public string Description { get; private set; }
        public IEnumerable<StockItem> StockItems { get; private set; }
        public IEnumerable<Review> Reviews { get; private set; }
        public decimal Price { get; private set; }
        public decimal SalePrice { get; private set; }
        public bool OnSale { get; private set; }
        public decimal Rating { get; private set; }
        public string Author { get; private set; }
        public string AuthorDescription { get; private set; }
        public string Publisher { get; set; }
        public string PublisherDescription { get; set; }
        public IEnumerable<Category> Categories { get; private set; }
        public IEnumerable<RelatedItem> RelatedItems { get; private set; }
        public AvailabilityTypes Availability { get; private set; }
        public string AvailabilityAsString { get; private set; }


        void MapToView(IProduct product)
        {
            OriginalProduct = product;
            Code = product.Code;
            Title = product.Title;
            AlternativeImages = product.AlternativeImages;
            HasTrailer = product.HasTrailer();
            Trailer = product.Trailer;
            InStock = product.InStock();
            Description = product.Description;
            StockItems = product.GetAvailableStockItems();
            Reviews = product.Reviews;
            Rating = product.Rating();
            Author = product.Author;
            AuthorDescription = product.AuthorDescription;
            Publisher = product.Publisher;
            PublisherDescription = product.PublisherDescription;
            Categories = product.Categories;
            RelatedItems = product.RelatedItems;
            Availability = product.GetAvailability();
            AvailabilityAsString = product.GetAvailabilityAsString();
            OnSale = product.StockItems.Any(x => x.IsOnSale());
        }

        
    }
}