﻿using System.Collections.Generic;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class ProductListingViewModel : CatalogProductListViewModel
    {

        private ProductListingViewModel(string categories, string mediaType, IEnumerable<IProduct> products)
            : base(products)
        {
            Categories = categories;
            MediaType = mediaType;
        }

        private ProductListingViewModel(string authors, IEnumerable<IProduct> products, PagingInfo pagingInfo)
            : base(products)
        {
            Authors = authors;
            PagingInfo = pagingInfo;
        }
        private ProductListingViewModel(string authors, string categories, string subjects, string mediaTypes, string searchString, IEnumerable<IProduct> products, PagingInfo pagingInfo)
            : base(products)
        {
            Authors = authors;
            Categories = categories;
            Subjects = subjects;
            MediaTypes = mediaTypes;
            PagingInfo = pagingInfo;
            SearchString = searchString;
        }

        public string Categories { get; private set; }
        public string MediaType { get; private set; }
        public string Authors { get; private set; }
        public string Subjects { get; private set; }
        public string MediaTypes { get; private set; }
        public string SearchString { get; private set; }
        public PagingInfo PagingInfo { get; set; }
        public string ProductListingHeader { get; set; }



        public static ProductListingViewModel With(string authors, string categories, string subjects, string mediaTypes, string searchString, IEnumerable<IProduct> products, PagingInfo pagingInfo)
        {
            return new ProductListingViewModel(authors, categories, subjects, mediaTypes, searchString, products, pagingInfo);
        }



    }
}