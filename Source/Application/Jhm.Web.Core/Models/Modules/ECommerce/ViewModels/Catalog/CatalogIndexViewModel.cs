﻿using System.Collections.Generic;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CatalogIndexViewModel : CartViewModel
    {
        private List<IProduct> featuredItems;
        private List<IProduct> latestReleasedProducts;
        private List<IProduct> featuredOnGetv;
        private List<IProduct> featuredOnTelevision;

        private CatalogIndexViewModel(IEnumerable<IProduct> featuredItems, IEnumerable<IProduct> latestReleasedProducts)
        {
            this.featuredItems = new List<IProduct>(featuredItems);
            this.latestReleasedProducts = new List<IProduct>(latestReleasedProducts);
        }

        public CatalogIndexViewModel(IEnumerable<IProduct> featuredProducts, IEnumerable<IProduct> featuredOnGetv, IEnumerable<IProduct> featuredOnTelevision)
        {
            this.featuredItems = new List<IProduct>(featuredProducts);
            this.featuredOnGetv = new List<IProduct>(featuredOnGetv);
            this.featuredOnTelevision = new List<IProduct>(featuredOnTelevision);
        }



        public IEnumerable<IProduct> FeaturedItems { get { return new List<IProduct>(featuredItems).AsReadOnly(); } }
        public IEnumerable<IProduct> FeaturedOnTelevisionItems { get { return new List<IProduct>(featuredOnTelevision).AsReadOnly(); } }
        public IEnumerable<IProduct> FeaturedOnGetvItems { get { return new List<IProduct>(featuredOnGetv).AsReadOnly(); } }
        public IEnumerable<IProduct> LatestReleaseItems() { return new List<IProduct>(latestReleasedProducts).AsReadOnly(); }
        public IEnumerable<IProduct> LatestReleaseItems(MediaType mediaType) { return new List<IProduct>(latestReleasedProducts.FindAll(
                                                                                                            x => x.StockItems.IsNot().Null() && x.StockItems.ToLIST().Find(
                                                                                                                 y => y.MediaType.Equals(mediaType)).IsNot().Null())).AsReadOnly(); }


        //public static CatalogIndexViewModel With(IEnumerable<IProduct> featuredItems, IEnumerable<IProduct> latestReleasedProducts)
        //{
        //    return new CatalogIndexViewModel(featuredItems, latestReleasedProducts);
        //}

        public static CatalogIndexViewModel With(IEnumerable<IProduct> featuredProducts, IEnumerable<IProduct> featuredOnGetv, IEnumerable<IProduct> featuredOnTelevision)
        {
            return new CatalogIndexViewModel(featuredProducts, featuredOnGetv, featuredOnTelevision);
        }
    }
}