﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class DonationFormViewModel : CartViewModel
    {
        public static string SelectedChoiceName = "SelectedChoice";
        public static string SelectedStockItemChoiceListName(Guid optionId) { return "{0}-SelectedStockItemChoiceListName".FormatWith(optionId); }

        public static string CertificateTextBoxName(Guid optionId) { return "{0}-Certificate".FormatWith(optionId); }
        public static string DonationFrequencySelectListName(Guid optionId) { return "{0}-DonationFrequency".FormatWith(optionId); }
        public static string AmountTextBoxName(Guid optionId) { return "{0}-Amount".FormatWith(optionId); }
        public static string QuantityTextBoxName(Guid optionId) { return "{0}-Quantity".FormatWith(optionId); }

        private List<DonationOption> donationOptions = new List<DonationOption>();       

        public DonationFormViewModel()
        {
        }

        private DonationFormViewModel(IDonation donation)
        {
            MapToView(donation);
        }

        public IEnumerable<DonationOption> DonationOptions
        {
            get { return donationOptions; }
            set { donationOptions = value.ToLIST(); }
        }
        

        public string DonationCode { get; private set;}
        public string Description { get; private set;}
        public string Title { get; private set;}
        public string Body { get; private set;}
        public string ListViewImageUrl { get; private set;}
        public string DetailViewImageUrl { get; private set;}
        public string FormViewImageUrl { get; private set;}
        public string PdfVersion { get; private set;}
        public string ChoiceId { get; private set;}


        private void MapToView(IDonation donation)
        {
            donationOptions = donation.DonationOptions.ToLIST();
            PdfVersion = donation.PdfVersion;
            ListViewImageUrl = donation.ListViewImageUrl;
            DetailViewImageUrl = donation.DetailViewImageUrl;
            FormViewImageUrl = donation.FormViewImageUrl;
            Body = donation.Body;
            Title = donation.Title;
            Description = donation.Description;
            DonationCode = donation.DonationCode;
            ChoiceId = string.Empty;
        }



        public static DonationFormViewModel With(IDonation donation)
        {
            return new DonationFormViewModel(donation);
        }

        private static bool AddModelErrorReturnFalse(ViewDataDictionary viewData, string key, string errorMessage)
        {
            AddModelError(viewData, key, errorMessage);
            return false;
        }

        private static void AddModelError(ViewDataDictionary viewData, string key, string errorMessage)
        {
            viewData.ModelState.AddModelError(key, errorMessage);
        }

        public bool IsValid(ViewDataDictionary viewData, FormCollection formCollection)
        {
            if (InvalidChoiceId(viewData, formCollection)) return false;

            var option = GetSelectedDonationOption(formCollection);

            if (option.Is().Null())
                return AddModelErrorReturnFalse(viewData, SelectedChoiceName, "Unable to find donation option choice by 'ID:{0}'.".FormatWith(formCollection[SelectedChoiceName]));

            return CanSetOptionValues(viewData, formCollection, option);
        }

        private DonationOption GetSelectedDonationOption(FormCollection formCollection)
        {
            var selectedChoiceId = new Guid(formCollection[SelectedChoiceName]);
            return donationOptions.Find(x => x.Id == selectedChoiceId);
        }

        private static bool InvalidChoiceId(ViewDataDictionary viewData, FormCollection formCollection)
        {
            if (formCollection[SelectedChoiceName].Is().Null())
            {
                return !AddModelErrorReturnFalse(viewData, SelectedChoiceName, "Must make a selection.");
            }
            else
            {
                viewData.Add(SelectedChoiceName, formCollection[SelectedChoiceName]);
                return false;
            }
        }

        private static bool CanSetOptionValues(ViewDataDictionary viewData, FormCollection formCollection, DonationOption option)
        {
            if (option is AnyAmountDonationOption)
                return CanSetAnyAmountDonationOptionValues(viewData, option, formCollection);
            if (option is FixedAmountDonationOption)
                return CanSetFixedAmountDonationOptionValues(viewData, option, formCollection);
            if (option is MultipleOfdonationOption)
                return CanSetMultipleOfdonationOptionValues(viewData, option, formCollection);
            if (option is AmountRangeDonationOption)
                return CanSetAmountRangeDonationOptionValues(viewData, option, formCollection);
            if (option is MinimumAmountDonationOption)
                return CanSetMinimumAmountDonationOptionValues(viewData, option, formCollection);

            return AddModelErrorReturnFalse(viewData, "", "DonationOption is of unknown type.");
        }

        private static bool TryAddValue(ViewDataDictionary viewData, string key, Action method)
        {
            try
            {
                method();
                return true;
            }
            catch (DonationOptionException e)
            {
                var message = DonationOption.ReplaceDonationFormat(e.Message,(Client)viewData[MagicStringEliminator.ViewDataKeys.Client]);
                return AddModelErrorReturnFalse(viewData, key, message);
            }
        }

        private static bool DoesNotContainFalseBool(params bool[] bools)
        {
            var rValue = new List<bool>();
            rValue.AddRange(bools);
            return !rValue.Contains(false);
        }

        private static bool CanSetOptionBaseValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            string certificate = formCollection[CertificateTextBoxName(option.Id)] ?? string.Empty;
            viewData.Add(CertificateTextBoxName(option.Id), certificate);
            certificate = certificate.EndsWith(",") ? string.Empty : certificate;

            
            viewData.Add(DonationFrequencySelectListName(option.Id), formCollection[DonationFrequencySelectListName(option.Id)]);
            viewData.Add(SelectedStockItemChoiceListName(option.Id), formCollection[SelectedStockItemChoiceListName(option.Id)]);


            return DoesNotContainFalseBool(new List<bool>{
                                            TryAddValue(viewData, 
                                                        SelectedStockItemChoiceListName(option.Id), 
                                                        () => option.CanSetChosenStockItem(formCollection[SelectedStockItemChoiceListName(option.Id)].TryConvertTo<Guid>())),
                                            TryAddValue(viewData, 
                                                        CertificateTextBoxName(option.Id), 
                                                        () => option.CanSetCertificateName(certificate)),
                                            TryAddValue(viewData,
                                                        formCollection[DonationFrequencySelectListName(option.Id)],
                                                        () => option.CanSetDonationFrequency(DonationCartItemFactory.TryGetDonationFrequency(option, formCollection))),
                                       }.ToArray());
        }

        private static bool CanSetAnyAmountDonationOptionValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            viewData.Add(AmountTextBoxName(option.Id), formCollection[AmountTextBoxName(option.Id)]);
            return DoesNotContainFalseBool(new List<bool>{
                                                        CanSetOptionBaseValues(viewData, option, formCollection),
                                                        TryAddValue(viewData, 
                                                                    AmountTextBoxName(option.Id), 
                                                                    () => (option as AnyAmountDonationOption).SetAmount(formCollection[AmountTextBoxName(option.Id)].TryConvertTo<decimal>())),
                                       }.ToArray());

        }

        private static bool CanSetFixedAmountDonationOptionValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            return CanSetOptionBaseValues(viewData, option, formCollection);
        }

        private static bool CanSetMultipleOfdonationOptionValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            viewData.Add(QuantityTextBoxName(option.Id), formCollection[QuantityTextBoxName(option.Id)]);
            return DoesNotContainFalseBool(new List<bool>{
                                                        CanSetOptionBaseValues(viewData, option, formCollection),
                                                        TryAddValue(viewData, 
                                                                    QuantityTextBoxName(option.Id), 
                                                                    () => (option as MultipleOfdonationOption).SetQuantity(formCollection[QuantityTextBoxName(option.Id)].TryConvertTo<int>())),
                                       }.ToArray());
        }

        private static bool CanSetAmountRangeDonationOptionValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            viewData.Add(AmountTextBoxName(option.Id), formCollection[AmountTextBoxName(option.Id)]);
            return DoesNotContainFalseBool(new List<bool>{
                                                        CanSetOptionBaseValues(viewData, option, formCollection),
                                                        TryAddValue(viewData, 
                                                                    AmountTextBoxName(option.Id), 
                                                                    () => (option as AmountRangeDonationOption).SetAmount(formCollection[AmountTextBoxName(option.Id)].TryConvertTo<decimal>())),
                                       }.ToArray());
        }

        private static bool CanSetMinimumAmountDonationOptionValues(ViewDataDictionary viewData, DonationOption option, FormCollection formCollection)
        {
            viewData.Add(AmountTextBoxName(option.Id), formCollection[AmountTextBoxName(option.Id)]);
            return DoesNotContainFalseBool(new List<bool>{
                                                        CanSetOptionBaseValues(viewData, option, formCollection),
                                                        TryAddValue(viewData, 
                                                                    AmountTextBoxName(option.Id), 
                                                                    () => (option as MinimumAmountDonationOption).SetAmount(formCollection[AmountTextBoxName(option.Id)].TryConvertTo<decimal>())),
                                       }.ToArray());
        }
    }
}