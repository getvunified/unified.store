﻿using System.Collections.Generic;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class DonationIndexViewModel : CartViewModel
    {
        private List<IDonation> donationOpportunities = new List<IDonation>();

        private DonationIndexViewModel(IEnumerable<IDonation> donationOpportunities)
        {
            this.donationOpportunities = new List<IDonation>(donationOpportunities);
        }

        public IEnumerable<IDonation> DonationOpportunities { get { return donationOpportunities; } }

        public static DonationIndexViewModel With(IEnumerable<IDonation> donationOpportunities)
        {
            return new DonationIndexViewModel(donationOpportunities);
        }
    }
}