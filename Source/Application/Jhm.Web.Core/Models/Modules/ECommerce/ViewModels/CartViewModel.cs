﻿using System.Collections.Generic;
using System.Linq;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.ECommerce;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CartViewModel : BaseViewModel
    {
        public virtual void MapCart(Cart cart, bool doLightMapping)
        {
            MapCartToBaseView(cart, doLightMapping);
        }

        private void MapCartToBaseView(Cart cart, bool doLightMapping)
        {
            if (cart.IsNot().Null())
            {
                NumberOfItemsInCart = cart.NumberOfItemsInCart();
                if (doLightMapping) return;

                CartItems = cart.Items;
                CartSubTotal = cart.SubTotal(false);
                CartSubTotalWithDiscount = cart.SubTotal(true);
                DiscountApplied = cart.DiscountApplied;
                DiscountTotal = cart.DiscountTotal;
                DiscountErrorMessage = cart.DiscountErrorMessage;
                StockItemsCartSubTotal = cart.GetSubTotalByItemType(typeof(StockItem));
                DonationstemsCartSubTotal = cart.GetSubTotalByItemType(typeof(DonationCartItem));
                PromotionalItems = cart.PromotionalItems;
                DiscountItems = cart.DiscountItems;
                SubscriptionsCartSubTotal = cart.GetSubTotalByItemType(typeof (SubscriptionCartItem));
                GiftCardsCartSubTotal = cart.GetSubTotalByItemType(typeof (GiftCardCartItem));
            }
            
        }

        public Cart Cart
        {
            set { MapCartToBaseView(value, false); }
        }

        public Cart LightCart
        {
            set { MapCartToBaseView(value, true); }
        }

        public int NumberOfItemsInCart { get; private set; }
        public IEnumerable<CartLineItem> CartItems { get; private set; }
        public decimal CartSubTotal { get; private set; }

        //TODO: Need to write spec to test this
        public bool ContainsDonations()
        {
            return CartItems.ToLIST().Where(x => x.Item is DonationCartItem).Count().Is().GreaterThan(0);
        }

        //TODO: Need to write spec to test this
        public bool ContainsStockItem()
        {
            return CartItems.ToLIST().Where(x => x.Item is StockItem).Count().Is().GreaterThan(0);
        }

        //TODO: Need to write spec to test this
        public bool ContainsDownloadableItem()
        {
            return CartItems.ToLIST().Where(x => x.Item is StockItem && ((StockItem)x.Item).IsDownloadableProduct()).Count().Is().GreaterThan(0);
        }

        //TODO: Need to write spec to test this
        public bool ContainsSubscriptionItems()
        {
            return CartItems.ToLIST().Count(x => x.Item is SubscriptionCartItem).Is().GreaterThan(0);
        }

        public bool ContainsGifCards()
        {
            return CartItems.Any(x => x.Item is GiftCardCartItem);
        }

        public decimal StockItemsCartSubTotal { get; private set; }
        public decimal DonationstemsCartSubTotal { get; private set; }
        public decimal CartSubTotalWithDiscount { get; private set; }
        public decimal DiscountTotal { get; private set; }
        public DiscountCode DiscountApplied { get; private set; }
        public bool IsSaltPartner { get; set; }
        public string DiscountErrorMessage { get; private set; }
        public List<PromotionalItemViewModel> PromotionalItems { get; set; }
        public List<DiscountItemViewModel> DiscountItems { get; set; }
        public bool IsDifferenceMediaPage { get; set; }
        public decimal SubscriptionsCartSubTotal { get; set; }
        public decimal GiftCardsCartSubTotal { get; set; }
    }
}