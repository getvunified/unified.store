﻿using System;

namespace Jhm.Web.Core.Models.Modules.ECommerce.ViewModels
{
    public class ProductImageToStockItemViewModel
    {
        public StockItem StockItem { get; set; }
        public string ImagePath { get; set; }
        public Guid Id { get; set; }
    }
}
