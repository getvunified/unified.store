﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Jhm.em3.Core;
using getv.donorstudio.core.eCommerce;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CheckoutOrderReviewViewModel : CartViewModel
    {
        private IEnumerable<Address> _addressList;
        private PaymentListViewModel _paymentList;
        private bool _shouldReloadAddressList = true;
        private IEnumerable<CreditCard> _creditCardsList;
        public string ReturnToUrl { get; private set; }

        public new Cart Cart { get; set; }

        public CheckoutOrderReviewViewModel() { }

        public CheckoutOrderReviewViewModel(string returnToUrl = null)
        {
            ReturnToUrl = returnToUrl;
        }

        public static CheckoutOrderReviewViewModel InitializeWith(User user, Client client, string returnToUrl = null)
        {
            var model = new CheckoutOrderReviewViewModel(returnToUrl);
            model.InitializeUsing(user, client);

            return model;
        }

        public void InitializeUsing(User user, Client client)
        {
            FirstName = user.Profile.FirstName;
            LastName = user.Profile.LastName;
            Email = user.Email;
            ShippingFirstName = String.IsNullOrEmpty(ShippingFirstName) ? FirstName : ShippingFirstName;
            ShippingLastName = String.IsNullOrEmpty(ShippingLastName) ? LastName : ShippingLastName;
            ShippingCompany = user.Profile.Company;
            Profile = user.Profile;
            if (client.Cart.HasItemsWithShippingCost())
            {
                ShippingMethods = client.Company.GetShippingMethods(client.Cart.HasFreeShippingDiscount());
                ShouldDisplayShippingMethods = true;
            }
            else
            {
                var list = new List<ShippingMethod>();
                list.Add(ShippingMethod.NotApplicable);
                ShippingMethods = list;
                ShouldDisplayShippingMethods = false;
            }

            ShouldDisplayPaymentMethods = client.Cart.HasItemsWithCost();

            
            //NOTE: Commented out 3/29/2013 until we figure out Tokens in Donor
            //CreditCardsList = user.GetUnexpiredCreditCards(DateTime.Now);
            CreditCardsList = new CreditCard[] { };


            //IsUsingNewCreditCard = !CreditCardsList.Any();
            RegisterClient(client);
            MapCart(client.Cart, false);
        }

        public override void MapCart(Cart cart, bool doLightMapping)
        {
            MapToView(cart);
            base.MapCart(cart, doLightMapping);
        }


        private void MapToView(Cart cart)
        {
            Cart = cart;
        }


        public Address BillingAddress { get; set; }
        public Address ShippingAddress { get; set; }
        public string BillingPhone { get; set; }


        [Required]
        [DisplayName("shipping first name")]
        public string ShippingFirstName { get; set; }
        [Required]
        [DisplayName("shipping last name")]
        public string ShippingLastName { get; set; }
        public string ShippingCompany { get; set; }
        public string ShippingPhoneCountryCode { get; set; }
        public string ShippingPhoneAreaCode { get; set; }
        public string ShippingPhoneNumber { get; set; }

        [Required]
        [DisplayName("shipping method")]
        public ShippingMethod ShippingMethod { get; set; }

        

        [Required]
        [DisplayName("shipping address")]
        public long ShippingAddressId { get; set; }

        [Required]
        [DisplayName("billing address")]
        public long BillingAddressId { get; set; }

        public IEnumerable<Address> AddressList
        {
            get { return (_addressList = _addressList?? new List<Address>()); }
            set { _addressList = value; }
        }

        public Profile Profile { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ShippingAddressCountry { get; set; }

        public string BillingAddressCountry { get; set; }

        public decimal ShippingCost { get; set; }

        public decimal TaxCost { get; set; }

        public decimal OrderTotal { get; set; }

        public string ShippingMethodValue { get; set; }

        public string TransactionNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public string ApprovalCode { get; set; }

        public string CurrencyCode { get; set; }

        public string Token { get { return TransactionNumber; } }

        public IEnumerable<ShippingMethod> ShippingMethods { get; set; }

        public bool ShouldDisplayShippingMethods { get; private set; }

        public bool ShouldDisplayPaymentMethods { get; private set; }

        public IEnumerable<CreditCard> CreditCardsList
        {
            get { return _creditCardsList = (_creditCardsList ?? new CreditCard[]{}); }
            set { _creditCardsList = value; }
        }

        
        public string DsSourceCode { get; set; }

        public PaymentListViewModel PaymentList
        {
            get
            {
                var result = _paymentList ?? (_paymentList = new PaymentListViewModel
                    {
                        AvailableCreditCards = CreditCardsList,
                        AddressList = AddressList,
                        OrderTotal = OrderTotal,
                        IsDifferenceMediaPage = IsDifferenceMediaPage
                    });
                result.RegisterClient(Client);
                return result;
            }
            set { _paymentList = value; }
        }

        public bool ShouldReloadAddressList
        {
            get { return _shouldReloadAddressList; }
            set { _shouldReloadAddressList = value; }
        }

		public bool IsFirstTimeSaltDonation { get; set; }
	}
}