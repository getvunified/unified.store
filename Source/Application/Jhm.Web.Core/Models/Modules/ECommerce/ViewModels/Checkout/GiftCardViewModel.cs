﻿namespace Jhm.Web.Core.Models
{
    public class GiftCardViewModel: BaseViewModel
    {
        public decimal Amount { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
        public bool AlreadyProcessed { get; set; }

        public decimal Balance { get; set; }
    }
}