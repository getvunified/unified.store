using System.Collections.Generic;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CheckoutIndexViewModel : CartViewModel
    {
        protected CheckoutIndexViewModel(Company company)
        {
            MapToView(company);
        }

        public IEnumerable<Country> Countries { get; private set; }

        public IEnumerable<State> StatesProvinces { get; private set; }

        public static CheckoutIndexViewModel With(Company company)
        {
            return new CheckoutIndexViewModel(company);
        }

        private void MapToView(Company company)
        {
            Countries = company.Countries;
            StatesProvinces = company.StateProvinces;
        }
    }
}