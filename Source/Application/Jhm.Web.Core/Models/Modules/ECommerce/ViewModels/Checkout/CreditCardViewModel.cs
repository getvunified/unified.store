﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{
    public class CreditCardViewModel : BaseViewModel
    {
        private IEnumerable<CreditCard> _creditCardsList;
        private bool _isUsingNewCreditCard;
        public decimal Amount { get; set; }
        [Required]
        [DisplayName("credit card type")]
        public string CreditCardType { get; set; }
        [Required]
        [DisplayName("credit card number")]
        public string CreditCardNumber { get; set; }
        [Required]
        [DisplayName("credit card expiration month")]
        public string CCExpirationMonth { get; set; }
        [Required]
        [DisplayName("credit card expiration year")]
        public string CCExpirationYear { get; set; }
        [Required]
        [DisplayName("name on card")]
        public string NameOnCard { get; set; }
        [Required]
        [DisplayName("CV2 or CID")]
        public string CID { get; set; }

        public IEnumerable<SelectListItem> GetNext10YearsList()
        {
            var years = new List<SelectListItem>(15);
            DateTime now = DateTime.Now;
            for (int i = 0; i < 15; i++)
            {
                years.Add(new SelectListItem() { Selected = false, Text = now.Year.ToString(), Value = now.Year.ToString() });
                now = now.AddYears(1);
            }
            return years;
        }

        public bool AlreadyProcessed { get; set; }


        [DisplayName("Save this credit card for future use")]
        public bool ShouldSaveCreditCard { get; set; }

        public IEnumerable<CreditCard> CreditCardsList
        {
            get { return _creditCardsList = (_creditCardsList ?? new CreditCard[] {}); }
            set { _creditCardsList = value; }
        }


        public string SelectedCreditCardId { get; set; }

        public CreditCard SelectedCreditCard { get; set; }

        public bool IsUsingNewCreditCard
        {
            get { return _isUsingNewCreditCard; }
            set { _isUsingNewCreditCard = value; }

        }

        public decimal Balance { get; set; }

        public IEnumerable<Address> AddressList { get; set; }

        public long BillingAddressId { get; set; }

        public Address SelectedAddress { get; set; }

        public string TwoDigitsExpirationYear
        {
            get
            {
                return CCExpirationYear = (!String.IsNullOrEmpty(CCExpirationYear) && CCExpirationYear.Length >= 4)
                        ? CCExpirationYear.Substring(2, 2)
                        : CCExpirationYear;
            }
        }

        public string Token { get; set; }

        public string MaskedCreditCardNumber { get { return FirstFour(CreditCardNumber) + "-****-****-" + LastFour(CreditCardNumber); } }


        public static string LastFour(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < 4)
            {
                return "0000";
            }
            return value.Substring(value.Length - 4, 4);
        }

        public static string FirstFour(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < 4)
            {
                return "0000";
            }
            return value.Substring(0,4);
        }
    }
}
