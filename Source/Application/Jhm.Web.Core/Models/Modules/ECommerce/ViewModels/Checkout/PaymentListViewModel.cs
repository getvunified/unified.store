﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using getv.donorstudio.core.Utilities;

namespace Jhm.Web.Core.Models
{
    public class PaymentListViewModel : BaseViewModel
    {
        private List<PaymentViewModel> _payments;
        private IEnumerable<SelectListItem> _paymentTypes;

        private const string CreditCardDisplayName = "Credit Card";
        private const string GiftCardDisplayName = "Gift Card";

        public List<PaymentViewModel> Payments
        {
            get { return _payments ?? (_payments = new List<PaymentViewModel>()); }
            set { _payments = value; }
        }

        public decimal PaymentTotal
        {
            get
            {
                return Payments.Sum(p => p.Amount);
            }
        }

        public SelectListItem SelectedPaymentType { get; set; }

        public IEnumerable<SelectListItem> PaymentTypes
        {
            get
            {
                return _paymentTypes ?? (_paymentTypes = new[]
                    {
                        new SelectListItem {Text = CreditCardDisplayName, Value = DsConstants.PaymentType.CreditCard},
                        new SelectListItem {Text = GiftCardDisplayName, Value = DsConstants.PaymentType.GiftCard},
                    });
            }
        }

        public IEnumerable<CreditCard> AvailableCreditCards { get; set; }

        public decimal OrderTotal { get; set; }

        public decimal Balance
        {
            get { return OrderTotal - PaymentTotal; }
        }

        public void AddGiftCardPayment(GiftCardViewModel giftCard)
        {
            Payments.Add(new PaymentViewModel
                {
                    Amount = giftCard.Amount, 
                    GiftCard = giftCard, 
                    PaymentType = new PaymentTypeViewModel{Value = DsConstants.PaymentType.GiftCard, Name = GiftCardDisplayName}
                });
        }

        public void AddCreditCard(CreditCardViewModel creditCard)
        {
            Payments.Add(new PaymentViewModel
                {
                    Amount = creditCard.Amount, 
                    CreditCard = creditCard,
                    PaymentType = new PaymentTypeViewModel { Value = DsConstants.PaymentType.CreditCard, Name = CreditCardDisplayName + " (" + creditCard.CreditCardType + ")" }
                });
        }

        public void RemovePayment(Guid id)
        {
            Payments.Remove(Payments.Find(x => x.Id == id));
        }

        public IEnumerable<em3.Core.Address> AddressList { get; set; }

        public bool IsDifferenceMediaPage { get; set; }

        public void Clear()
        {
            Payments.Clear();
        }
    }

    public class PaymentViewModel
    {
        public Guid Id { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }
        public CreditCardViewModel CreditCard { get; set; }
        public GiftCardViewModel GiftCard { get; set; }
        public bool IsAlreadyProcessed { get; set; }

        public PaymentViewModel()
        {
            Id = Guid.NewGuid();
        }

        public string PaymentNumber
        {
            get
            {
                if (PaymentType.Value == DsConstants.PaymentType.CreditCard && !String.IsNullOrEmpty(CreditCard.CreditCardNumber))
                {
                    return CreditCard.CreditCardNumber;
                }
                if (PaymentType.Value == DsConstants.PaymentType.GiftCard && !String.IsNullOrEmpty(GiftCard.Number))
                {
                    return GiftCard.Number;
                }
                return "NONE";
            }
        }
        public decimal Amount { get; set; }

        public bool IsGifCard()
        {
            return PaymentType.Value == DsConstants.PaymentType.GiftCard && GiftCard != null;
        }

        public bool IsCreditCard()
        {
            return PaymentType.Value == DsConstants.PaymentType.CreditCard && CreditCard != null;
        }
    }

    public class PaymentTypeViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}