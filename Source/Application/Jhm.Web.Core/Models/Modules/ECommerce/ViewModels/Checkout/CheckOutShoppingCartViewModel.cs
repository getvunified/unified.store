﻿using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models.Modules.ECommerce
{
    public class CheckoutShoppingCartViewModel : CartViewModel, ILogOnModel
    {
        public string CatalogReturnToUrl { get; private set; }
        public string DonationsReturnToUrl { get; private set; }
        public static string SelectedCheckBoxName(Guid cartItemId) { return "{0}-SelectedCheckBoxName".FormatWith(cartItemId); }
        public static string QuantityTextBoxName(Guid cartItemId) { return "{0}-Quantity".FormatWith(cartItemId); }

        public CheckoutShoppingCartViewModel()
        {
            CatalogReturnToUrl = "/" + MagicStringEliminator.Routes.Catalog.Route;
            DonationsReturnToUrl = "/" + MagicStringEliminator.Routes.Donation_Opportunities.Route;
        }

        public CheckoutShoppingCartViewModel(string catalogReturnToUrl = null, string donationsReturnToUrl = null)
        {
            CatalogReturnToUrl = catalogReturnToUrl ?? "/"+MagicStringEliminator.Routes.Catalog.RouteName;
            DonationsReturnToUrl = donationsReturnToUrl ?? "/" + MagicStringEliminator.Routes.Donation_Opportunities.Route;
        }

        public override void MapCart(Cart cart, bool doLightMapping)
        {
            MapToView(cart);
            base.MapCart(cart, doLightMapping);
        }

        private void MapToView(Cart cart)
        {
            
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        public string GlobalUserId { get; set; }



        
    }
}