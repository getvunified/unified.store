﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Jhm.Web.Core.Models
{
    public class FAQViewModel : FAQ
    {
        private List<FAQ> faqs;

        public FAQViewModel()
        {
            LoadFAQs();
        }

        public FAQViewModel(IEnumerable<FAQ> faqList)
        {
            if (faqList == null)
            {
                LoadFAQs();
            }
            else
            {
                faqs = new List<FAQ>(faqList);
            }
        }

        public static FAQViewModel With(IEnumerable<FAQ> faqList)
        {
            return new FAQViewModel(faqList);
        }

        private void LoadFAQs()
        {
            string path = (Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + @"\FAQs.xml").Replace(@"file:\", "");
            path = path.Replace(@"\bin", String.Empty);
            Stream xmlStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            XmlTextReader xmlReader = new XmlTextReader(xmlStream);

            try
            {
                XDocument xmlDoc = XDocument.Load(xmlReader);

                var xmlFaqs = from record in xmlDoc.Descendants("Record")
                              select new
                                {
                                    Question = GetValueFor(record, "Question"),
                                    Answer = GetValueFor(record, "Answer"),
                                    Id = record.Attribute("id").Value,
                                    ForIndexQuestion = GetValueFor(record, "ForIndexQuestion")
                                };
                faqs = new List<FAQ>(xmlFaqs.Count());
                foreach (var xmlFaq in xmlFaqs)
                {
                    FAQ faq = new FAQ(xmlFaq.Question, xmlFaq.Answer);
                    faq.Id = xmlFaq.Id;
                    faq.ForIndexQuestion = xmlFaq.ForIndexQuestion;
                    faqs.Add(faq);
                }

            }
            finally
            {
                xmlReader.Close();
                xmlStream.Close();
            }
        }

        private static string GetValueFor(XElement record, string name)
        {
            if (record == null || record.Element(name) == null)
            {
                return null;
            }
            return record.Element(name).Value;
        }

        public ReadOnlyCollection<FAQ> GetAll()
        {
            return faqs.AsReadOnly();
        }

        public FAQ GetById(string id)
        {
            FAQ faq = faqs.Find(x => x.Id == id);
            return faq;
        }

      

        public string SearchFor { get; set; }
        public SearchUsingTypes SearchUsing { get; set; }
        public int SearchUsingAsInt { get; set; }

        public int ResultsCount { get { return faqs.Count; } }
    }
}
