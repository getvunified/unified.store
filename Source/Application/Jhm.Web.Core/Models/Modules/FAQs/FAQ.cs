﻿using System;
using Foundation;

namespace Jhm.Web.Core.Models
{
    public class FAQ : Enumeration<String>
    {
        public FAQ()
            : base(String.Empty, String.Empty)
        {
        }

        public FAQ(string question, string answer)
            : base(question, question)
        {
            Question = question;
            Answer = answer;
            Id = question;
        }

        public virtual string Question { get; set; }
        public virtual string Answer { get; set; }
        public virtual string Id { get; set; }
        public virtual string ForIndexQuestion { get; set; }
    }
}
