﻿namespace Jhm.Web.Core
{
    public interface IPhoneType
    {
        string Value { get; set; }
        string DisplayName { get; set; }
    }
}
