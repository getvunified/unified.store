﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core
{
    public interface IAddressType
    {
        string Value { get; set; }
        string DisplayName { get; set; }
    }
}
