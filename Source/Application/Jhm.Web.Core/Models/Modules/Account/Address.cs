using System;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core;


namespace Jhm.em3.Core
{
    public class Address : IAddress, ICloneable
    {
        public Address(){}


        private string dSAddressType;

       
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string City { get; set; }
        public virtual string StateCode { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }
        public virtual int Region { get; private set; }
        public virtual string CongressionalDistrict { get; set; }

        /// <summary>
        /// Id used by DonorStudio
        /// </summary>
        public virtual long DSAddressId { get; set; }
       
      
        /// <summary>
        /// Type used in DonorStudio
        /// </summary>
        public virtual AddressType DSAddressType
        {
            get
            {
                //NOTE: Defaulting to Mailing
                return !string.IsNullOrEmpty(dSAddressType) ? AddressType.TryGetFromValue<AddressType>(dSAddressType) : AddressType.Mailing;
            }
        }

        public bool DsIsPrimary { get; set; }
        public virtual void SetDSAddressType(AddressType addressType)
        {
            //NOTE: Defaulting to Mailing
            dSAddressType = (addressType.IsNot().Null()) ? addressType.Value : AddressType.Mailing.Value;
        }


        public Address(string address1, string address2, string city, string stateCode, string postalCode, string country, long dsAddressId) :
            this(address1, address2, city, stateCode, postalCode, country, null, dsAddressId) { }

        public Address(string address1,string address2,string city,string stateCode,string postalCode,string country,string congressionalDistrict = null, long dsAddressId = -1) :
            this(address1, address2, city, stateCode, postalCode, country, null, dsAddressId, AddressType.Mailing) { }


        public Address(string address1,string address2,string city,string stateCode,string postalCode,string country,string congressionalDistrict, long dsAddressId, AddressType dsAddressType)
        {
            Address1 = address1;
            Address2 = address2;
            City = city;
            StateCode = stateCode;
            PostalCode = postalCode;
            Country = country;
            CongressionalDistrict = congressionalDistrict;
            DSAddressId = dsAddressId;
            SetDSAddressType(dsAddressType);
        }



        public virtual void CopyTo(Address address)
        {
            address.Address1 = Address1;
            address.Address2 = Address2;
            address.City = City;
            address.StateCode = StateCode;
            address.PostalCode = PostalCode;
            address.Country = Country;
            address.CongressionalDistrict = CongressionalDistrict;
            address.DSAddressId = DSAddressId;
            SetDSAddressType(DSAddressType);
        }

        public virtual Address Clone()
        {
            return new Address(Address1, Address2, City, StateCode, PostalCode, Country, CongressionalDistrict, DSAddressId, DSAddressType);
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public virtual string ToString(string separator)
        {
            return string.Format("Address1:{1}{0}Address2:{2}{0}City:{3}{0}State:{4}{0}PostalCode:{5}{0}Country:{6}{0}CongressionalDistrict:{7}",
                separator,
                Address1,
                Address2,
                City,
                StateCode,
                PostalCode,
                Country,
                CongressionalDistrict);
        }

        public class Rules
        {

            //public static readonly IBusinessRule<Address> Address1 =
            //    new BusinessRule<Address>("Address1", "Address 1 must be provided",
            //                              address => address != null &&
            //                                         Specifications.NonEmptyStringSpecification.IsSatisfiedBy(
            //                                             address.Address1));

            //public static readonly IBusinessRule<Address> City =
            //    new BusinessRule<Address>("City", "City must be provided",
            //                              address => address != null &&
            //                                         Specifications.NonEmptyStringSpecification.IsSatisfiedBy(
            //                                             address.City));

            //public static readonly IBusinessRule<Address> State =
            //    new BusinessRule<Address>("State", "State must be provided",
            //                              address => address != null &&
            //                                        (address.Country != Core.Country.UnitedStates ||
            //                                        Specifications.NonEmptyStringSpecification.IsSatisfiedBy(
            //                                             address.StateCode)));

            //public static readonly IBusinessRule<Address> PostalCode =
            //    new BusinessRule<Address>("PostalCode", "Postal code must be provided and valid",
            //                              address => address != null &&
            //                                        (address.Country != Core.Country.UnitedStates ||
            //                                         Specifications.NonEmptyStringSpecification.IsSatisfiedBy(
            //                                             address.PostalCode) &&
            //                                         Core.Country.IsPostalCodeValid(address.Country, address.PostalCode)));

            //public static readonly IBusinessRule<Address> Country =
            //    new BusinessRule<Address>("Country", "Country must be provided",
            //                              address => address != null &&
            //                                         Specifications.NonEmptyStringSpecification.IsSatisfiedBy(
            //                                             address.Country));

            //public static readonly IBusinessRule<Address> NoPostOfficeBox =
            //    new BusinessRule<Address>("NoPostOfficeBox", "Post office box not allowed",
            //                              address => address != null &&
            //                                         !Specifications.PostOfficeBoxSpecification.IsSatisfiedBy(
            //                                              address.address1));

            //public static IBusinessRuleSet<Address> DefaultRuleSet
            //{
            //   get
            //   {
            //       return new BusinessRuleSet<Address>(Address1, City, State, PostalCode, Country);  
            //   } 
            //}

        }

        public virtual long RecordId { get; set; }
        public virtual long A02RecordId { get; set; }
    }
}
