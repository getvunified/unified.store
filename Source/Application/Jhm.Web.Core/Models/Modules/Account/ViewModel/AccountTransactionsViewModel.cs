﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core.Models
{
    public class OrderDetailViewModel
    {
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public decimal Price { get; set; }
        public string PriceCode { get; set; }
        public int Quantity { get; set; }
        public decimal ExtendedAmount { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public decimal DiscountAmount { get; set; }
    }

    public class OrderViewModel
    {
        public OrderViewModel()
        {
            OrderDetails = new List<OrderDetailViewModel>();
        }

        public long AddressId { get; set; }
        public long OrderId { get; set; }
        public string ShippingMethod { get; set; }
        public decimal ShippingAmount { get; set; }
        public List<OrderDetailViewModel> OrderDetails { get; set; }
    }
    public class DonationViewModel
    {
        public string ProjectCode { get; set; }
        public string ProjectDescription { get; set; }
        public decimal Amount { get; set; }
        public bool IsDeductible { get; set; }
        public string SourceCode { get; set; }
        public bool Anonymous { get; set; }
    }


    public class TransactionViewModel
    {
        public TransactionViewModel()
        {
            Date = new DateTime();
            Orders = new List<OrderViewModel>();
            Donations = new List<DonationViewModel>();
        }

        public long AccountNumber { get; set; }
        public long DocumentNumber { get; set; }

        // Original Amount on the transaction
        public decimal TotalAmount { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }

        // May not be necessary
        public decimal AmountDue { get; set; }
        public decimal RemainingAmountDue { get; set; }

        public List<OrderViewModel> Orders { get; set; }
        public List<DonationViewModel> Donations { get; set; }
        

        
        public string PaymentType { get; set; }


        public List<UserSubscriptionViewModel> Subscriptions { get; set; }
    }
}
