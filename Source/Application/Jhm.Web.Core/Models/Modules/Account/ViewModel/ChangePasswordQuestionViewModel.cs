﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public class ChangePasswordQuestionViewModel : CartViewModel
    {
        [Required]
        public string Password { get; set; }

        [Required]
        [DisplayName("Password Question")]
        public string PasswordQuestion { get; set; }

        [Required]
        [DisplayName("Password Answer")]
        public string PasswordAnswer { get; set; }
    }
}
