﻿using System.Web.Mvc;

namespace Jhm.Web.Core.Models.Modules.AccountViewModel
{
    public class AccountViewModel<T> where T : class
    {
        protected AccountViewModel(Account account)
        {
            MapToView(account);
        }

        private void MapToView(Account account)
        {
        }


        public static T From(ViewDataDictionary viewData)
        {
            return viewData.Model as T;
        }
    }
}
