﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{

    public class AddressViewModel : CartViewModel
    {
        public string UserName { get; set; }
        [DisplayName("Street Address")]
        [Required]
        public string Address1 { get; set; }

        [DisplayName("Address Line 2")]
        public string Address2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string Country { get; set; }

        [DisplayName("State/Province")]
        [Required(ErrorMessage = "State/Province is required")]
        public string State { get; set; }
        [DisplayName("Postal Code")]
        public string Zip { get; set; }

        [DisplayName("This is my Primary Address")]
        public bool Primary { get; set; }

        public long AddressId { get; set; }
        public long RecordId { get; set; }
        public long A02RecordId { get; set; }
        public Cart Cart { get; set; }

        public static AddressViewModel With(User user, Client client)
        {
            var model = new AddressViewModel { Cart = client.Cart };
            if (user.Profile.PrimaryAddress != null)
            {
                model.Address1 = user.Profile.PrimaryAddress.Address1;
                model.Address2 = user.Profile.PrimaryAddress.Address2 ?? String.Empty;
                model.City = user.Profile.PrimaryAddress.City;
                model.Country = user.Profile.PrimaryAddress.Country;
                model.State = user.Profile.PrimaryAddress.StateCode;
                model.Zip = user.Profile.PrimaryAddress.PostalCode;
                model.RecordId = user.Profile.PrimaryAddress.RecordId;
                model.A02RecordId = user.Profile.PrimaryAddress.A02RecordId;
            }
            return model;
        }

        public void MapToAddress(Address address)
        {
            address.DsIsPrimary = Primary;
            address.DSAddressId = AddressId;
            address.Address1 = Address1;
            address.Address2 = Address2;
            address.City = City;
            address.StateCode = State;
            address.Country = Country;
            address.PostalCode = Zip;
            address.DsIsPrimary = Primary;
            address.RecordId = RecordId;
            address.A02RecordId = A02RecordId;
        }

        public void MapToUser(User user)
        {
            user.Profile.PrimaryAddress.Address1 = Address1;
            user.Profile.PrimaryAddress.Address2 = Address2;
            user.Profile.PrimaryAddress.City = City;
            user.Profile.PrimaryAddress.StateCode = State;
            user.Profile.PrimaryAddress.Country = Country;
            user.Profile.PrimaryAddress.PostalCode = Zip;
            user.Profile.PrimaryAddress.RecordId = RecordId;
            user.Profile.PrimaryAddress.A02RecordId = A02RecordId;
        }
        public static AddressViewModel CreateFrom(Address from)
        {
            var to = new AddressViewModel();
            to.Primary = from.DsIsPrimary;
            to.AddressId = from.DSAddressId;
            to.Address1 = from.Address1;
            to.Address2 = from.Address2;
            to.City = from.City;
            to.Country = from.Country;
            to.State = from.StateCode;
            to.Zip = from.PostalCode;
            to.RecordId = from.RecordId;
            to.A02RecordId = from.A02RecordId;
            return to;
        }

    }


}
