﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core.Models
{
    public interface ILogOnModel
    {
        string UserName { get; set; }
        string Password { get; set; }
        bool RememberMe { get; set; }
        string GlobalUserId { get; set; }
    }
}
