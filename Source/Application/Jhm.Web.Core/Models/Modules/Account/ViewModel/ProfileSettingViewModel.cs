﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core.Models
{
    public class ProfileSettingViewModel : ICloneable
    {
        //public static readonly ProfileSettingViewModel GoldenTicketSetting = new ProfileSettingViewModel(new Guid("2FAB5F49-3405-415C-8A12-998A11A0F6D2"), "Golden Ticket", "Subscribe to win a free ticket for the next JHM event", PreferenceTypes.Promotion);
        public static readonly ProfileSettingViewModel JHMEmailSetting = new ProfileSettingViewModel(new Guid("20748D78-585A-4C50-85FC-3EC095C9C23F"), "JHM Email List", "Subscribe to receive daily devocionals", PreferenceTypes.EmailList);

        public virtual Guid Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual PreferenceTypes PreferenceType { get; set; }

        public ProfileSettingViewModel(Guid id, string title, string description, PreferenceTypes preferenceType)
        {
            Id = id;
            Title = title;
            Description = description;
            PreferenceType = preferenceType;
        }

        public ProfileSettingViewModel()
        {
            // TODO: Complete member initialization
        }

        public ProfileSettingViewModel(Guid id)
        {
            Id = id;
        }


        

        

        public object Clone()
        {
            return new ProfileSettingViewModel()
                       {
                           Id = Id,
                           Description = Description,
                           Title = Title,
                           PreferenceType = PreferenceType,
                           IsActive = IsActive
                       };
        }
    }
}
