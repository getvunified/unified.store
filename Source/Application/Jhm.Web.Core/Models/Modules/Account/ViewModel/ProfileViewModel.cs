﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jhm.Common;
using Jhm.Common.Utilities;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{
    public class ProfileViewModel : CartViewModel
    {

       

        public virtual RadioButtonListViewModel<Gender> GenderRadioButtonList { get; set; }
        public virtual Gender Gender { get; set; }

        public ProfileViewModel()
        {
            GenderRadioButtonList = new RadioButtonListViewModel<Gender>
            {
                Id = "Gender",
                ListItems = new System.Collections.Generic.List<RadioButtonListItem<Gender>>
                {
                    new RadioButtonListItem<Gender>{Text = "Male", Value = Gender.Male},
                    new RadioButtonListItem<Gender>{Text = "Female", Value = Gender.Female}
                }
            };
        }


        [DisplayName("User name")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Your Email")]
        [EmailAddress]
        public string Email { get; set; }

        public virtual string Title { get; set; }

        [Required]
        [DisplayName("First Name")]
        public virtual string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public virtual string LastName { get; set; }

        [DisplayName("Company")]
        public string Company { get; set; }

        public string Country { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        
        /*[DisplayName("Title or Ocuppation")]
        public string Occupation { get; set; }*/

        private List<Address> _addresses;
        private IEnumerable<CreditCard> _creditCards;

        public List<Address> Addresses
        {
            get { return (_addresses = _addresses??new List<Address>()); }
            set { _addresses = value; }
        }

        /*[Required(ErrorMessage = "Phone is required")]
        [DisplayName("Phone Number")]
        public string Phone { get; set; }


        [DisplayName("First Name")]
        public virtual string ShippingFirstName { get; set; }

        [DisplayName("Last Name")]
        public virtual string ShippingLastName { get; set; }

        [DisplayName("Company")]
        public string ShippingCompany { get; set; }

        [DisplayName("Street Address")]
        public string ShippingAddress1 { get; set; }

        [DisplayName("")]
        public string ShippingAddress2 { get; set; }

        public string ShippingCity { get; set; }

        public string ShippingCountry { get; set; }

        [DisplayName("State/Province")]
        public string ShippingState { get; set; }

        [DisplayName("Postal Code")]
        public string ShippingZip { get; set; }


        [DisplayName("Phone Number")]
        public string ShippingPhone { get; set; }*/
        [Required]
        [DisplayName("Phone Country Code")]
        public string PhoneCountryCode { get; set; }

        [Required]
        [DisplayName("Phone Area Code")]
        public string PhoneAreaCode { get; set; }

        [Required]
        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        public Cart Cart { get; set; }

        


        public static ProfileViewModel With(User user, Client client)
        {
            var model = new ProfileViewModel { Cart = client.Cart };
            model.Company = user.Profile.Company;
            model.Email = user.Email;
            model.FirstName = user.Profile.FirstName;
            model.LastName = user.Profile.LastName;
            model.UserName = user.Username;
            model.Title = user.Profile.Title;

            model.Gender = (Gender)Enum.Parse(typeof(Gender), ((user.Profile.Gender == "M") || user.Profile.Gender == "Male") ? "Male" : "Female", true);

            model.GenderRadioButtonList.SelectedValue = model.Gender;

            if (user.Profile.PrimaryPhone != null)
            {
                model.PhoneAreaCode = user.Profile.PrimaryPhone.AreaCode;
                model.PhoneCountryCode = user.Profile.PrimaryPhone.CountryCode;
                model.PhoneNumber = user.Profile.PrimaryPhone.Number;
            }

            model.CreditCards = user.CreditCards.ToList();

            return model;
        }

        public void MapToUser(User user)
        {
            user.Profile.Company = Company;
            //user.Profile.Occupation = Occupation;
            //user.Email = Email;
            user.Profile.Title = Title;
            user.Profile.FirstName = FirstName;
            user.Profile.LastName = LastName;
            Gender = GenderRadioButtonList.SelectedValue;
            user.Profile.Gender = Gender.ToString();

            user.Profile.PrimaryPhone.CountryCode = PhoneCountryCode;
            user.Profile.PrimaryPhone.AreaCode = PhoneAreaCode;
            user.Profile.PrimaryPhone.Number = PhoneNumber;
            
        }

        public IEnumerable<CreditCard> CreditCards
        {
            get { return _creditCards = (_creditCards?? new CreditCard[]{}); }
            set { _creditCards = value; }
        }
    }
}
