﻿using System;
using System.Collections.Generic;
using Jhm.Web.Core.Models.Modules;
using Jhm.Web.Core.Models.Modules.ECommerce;
using Jhm.Web.Core.Models.Modules.ECommerce.ViewModels;

namespace Jhm.Web.Core.Models
{
    public class DownloadsViewModel : CartViewModel
    {
        public virtual User User { get; set; }
        public virtual string Name { get; set; }
        public virtual Cart Cart { get; set; }
        public virtual DigitalDownload Download { get; set; }

        public Dictionary<Guid, ProductImageToStockItemViewModel> StockItems
        {
            get; set;
        }


        public DownloadsViewModel(User user,  Dictionary<Guid,ProductImageToStockItemViewModel> stockItems, DigitalDownload download, Client client)
        {
            User = user;
            StockItems = stockItems;
            Name = user.Profile.FirstName;
            Cart = client.Cart;
            Download = download;
        }

        public DownloadsViewModel()
        {
            StockItems = new Dictionary<Guid, ProductImageToStockItemViewModel>();
        }

        public DownloadsViewModel(User user, Dictionary<Guid, ProductImageToStockItemViewModel> stockItems, Client client)
        {
            // TODO: Complete member initialization
            User = user;
            StockItems = stockItems;
            Name = user.Profile.FirstName;
            Cart = client.Cart;
        }
    }
}
