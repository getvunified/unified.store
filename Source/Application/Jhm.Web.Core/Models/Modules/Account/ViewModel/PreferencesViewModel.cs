﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.Common.Framework.Extensions;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public class PreferencesViewModel : CartViewModel
    {
        public virtual IProfile Profile { get; set; }
        public List<ProfileSettingViewModel> ProfileSettings { get; set; }

        public PreferencesViewModel()
        {
            ProfileSettings = new List<ProfileSettingViewModel>();
        }

        public void AddSetting(ProfileSettingViewModel settingViewModel)
        {
            if ( settingViewModel == null )
            {
                throw new ArgumentNullException("settingViewModel");
            }

            if ( settingViewModel.Id.IsNull() || settingViewModel.Id.Is().Equals(Guid.Empty))
            {
                throw new InvalidOperationException("Only profile settings with a valid GUID can be added to model");
            }
            ProfileSettings.Add(settingViewModel);
        }

        public IEnumerable<ProfileSettingViewModel> GetAllByType(PreferenceTypes preferenceType)
        {
            return ProfileSettings.FindAll(x => x.PreferenceType == preferenceType);
        }

        public void Map(Models.Profile profile)
        {
            Profile = profile;

        }
    }
}
