﻿using Foundation;

namespace Jhm.Web.Core.Models
{
    public class CreditCardType : Enumeration<string>, ICreditCardType
    {
        public static readonly CreditCardType Visa = new CreditCardType("Visa", "visa", 1, "^4");
        public static readonly CreditCardType MasterCard = new CreditCardType("Master Card", "master", 2, "^5[1-5]");
        public static readonly CreditCardType Discover = new CreditCardType("Discover", "discover", 3, "^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        public static readonly CreditCardType Amex = new CreditCardType("American Express", "amex", 4, "^3[47]");

        public CreditCardType() { }

        public CreditCardType(string displayName, string code, short index, string pattern) :
            base(code, displayName)
        {
            DisplayName = displayName;
            Code = code;
            Index = index;
            RegexPattern = pattern;
        }

        public virtual string DisplayName { get; set; }
        public virtual string Code { get; set; }
        public virtual short Index { get; set; }
        public virtual string RegexPattern { get; set; }
    }
}
