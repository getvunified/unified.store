﻿using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core
{
    public sealed class Phone : DefaultEntity, IPhone
    {
        private string phoneType;
        public PhoneType PhoneType
        {
            get
            {
                return !string.IsNullOrEmpty(phoneType)
                           ? PhoneType.TryGetFromValue<PhoneType>(phoneType)
                           : PhoneType.Home;
            }
        }

        public void SetPhoneType(PhoneType phType)
        {
            phoneType = (phType.IsNot().Null()) ? phType.Value : PhoneType.Home.Value;
        }

        public void SetPhoneType(string phType)
        {
            phoneType = phType;
        }


        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string Number { get; set; }
        public string Extension { get; set; }

        public Phone(){}

        public Phone(string countryCode, string areaCode, string number) :
            this(PhoneType.Home,countryCode, areaCode,number){}


        private Phone(PhoneType type, string countryCode, string areaCode, string number, string extension = null)
        {
            SetPhoneType(type);
            CountryCode = countryCode;
            AreaCode = areaCode;
            Number = number;
            Extension = extension ?? String.Empty;
        }
    }
}
