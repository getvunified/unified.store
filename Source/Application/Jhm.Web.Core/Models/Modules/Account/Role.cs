﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public class Role : DefaultEntity, IRole
    {
        public static class KnownRoles
        {
            public const string SuperAdmin = "SuperAdmin";
            public const string Admin = "Admin";
            public const string ContentEditor = "ContentEditor";
            public const string CustomerService = "CustomerService";
        }


        private IList<User> usersInRole = new List<User>();

        public Role()
        {
            usersInRole = new List<User>();
        }


        public Role(Guid id) : base(id)
        {
            usersInRole = new List<User>();
        }

        public virtual string RoleName { get; set; }
        public virtual string ApplicationName { get; set; }
        public virtual IEnumerable<User> UsersInRole { get { return usersInRole; } }


        protected internal virtual void AddUserInRole(User user)
        {
            usersInRole.Add(user);
        }

        protected internal virtual void RemoveUserInRole(User user)
        {
            usersInRole.Remove(user);
        }
    }
}
