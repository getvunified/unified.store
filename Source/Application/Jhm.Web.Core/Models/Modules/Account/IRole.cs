﻿using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public interface IRole
    {
        string RoleName { get; set; }
        string ApplicationName { get; set; }
        IEnumerable<User> UsersInRole { get; }
    }
}