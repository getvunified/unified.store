﻿using System;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{
    public interface IProfile
    {
        User User { get; set; }
        string ApplicationName { get; set; }
        bool IsAnonymous { get; set; }
        DateTime LastActivityDate { get; set; }
        DateTime LastUpdatedDate { get; set; }
        string Subscription { get; set; }
        string Language { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Gender { get; set; }
        DateTime BirthDate { get; set; }
        //string Occupation { get; set; }
        string Website { get; set; }
        string Company { get; set; }
        Address PrimaryAddress { get; set; }
        Phone PrimaryPhone { get; set; }
        string Title { get; set; }
        string GoldenTicketCode { get; set; }
    }
}