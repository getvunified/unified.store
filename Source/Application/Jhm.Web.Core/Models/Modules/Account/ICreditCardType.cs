﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core
{
    public interface ICreditCardType
    {
        string DisplayName { get; set; }
        string Code { get; set; }
    }
}
