using System;
using System.Linq;
using Foundation;
using Jhm.Common;

namespace Jhm.Web.Core.Models
{
    public class CreditCard : DefaultEntity, ISelectListItem<string>, ICreditCard
    {

        public CreditCard(){}

        public CreditCard(string endingWith, string token, CreditCardType cardType, int expirationMonth, int expirationYear)
        {
            PopulateCreditCard(this, endingWith,token,cardType,expirationMonth,expirationYear);
        }

        private static void PopulateCreditCard(CreditCard cc, string endingWith, string token, CreditCardType cardType, int expirationMonth, int expirationYear)
        {
            cc.EndingWith = endingWith;
            cc.Token = token;
            cc.CardType = cardType;
            cc.ExpirationMonth = expirationMonth;
            cc.ExpirationYear = expirationYear;
        }

        public CreditCard(User user,string endingWith, string token, CreditCardType cardType, int expirationMonth, int expirationYear)
            : this(endingWith, token, cardType, expirationMonth, expirationYear)
        {
            User = user;
        }

        public CreditCard(Guid id,string endingWith, string token, CreditCardType cardType, int expirationMonth, int expirationYear)
            : base(id)
        {
            PopulateCreditCard(this, endingWith, token, cardType, expirationMonth, expirationYear);
        }

        public CreditCard(Guid id, User user, string endingWith, string token, CreditCardType cardType, int expirationMonth, int expirationYear)
            : base(id)
        {
            User = user;
            PopulateCreditCard(this, endingWith, token, cardType, expirationMonth, expirationYear);
        }


        public virtual string EndingWith { get; set; }
        public virtual string Token { get; set; }

        private string cardType;
        public virtual CreditCardType CardType {
            get { return String.IsNullOrEmpty(cardType) ? null : CreditCardType.TryGetFromValue<CreditCardType>(cardType); }
            set { cardType = value == null ? null : value.Code; }
        }
        public virtual int ExpirationMonth { get; set; }
        public virtual int ExpirationYear { get; set; }

        public virtual User User { get; set; }


        public virtual bool IsExpired(DateTime currentDate)
        {
            return currentDate.Year > ExpirationYear ||
                   (currentDate.Year == ExpirationYear && currentDate.Month > ExpirationMonth);
        }

        public static CreditCard Map(CreditCardViewModel from, User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            var cc = new CreditCard()
                                {
                                    User = user,
                                    CardType = CreditCardType.TryGetFromValue<CreditCardType>(from.CreditCardType),
                                    EndingWith = from.CreditCardNumber.Right(4),
                                    ExpirationMonth = Int32.Parse(from.CCExpirationMonth),
                                    ExpirationYear = Int32.Parse(from.CCExpirationYear),
                                    Token = from.Token
                                };
            return cc;
        }

        //public static bool IsValid(Modules.ECommerce.CheckoutOrderReviewViewModel from, out string reason)
        //{
        //    reason = String.Empty;
        //    if (!NumberIsValid(from.CreditCardNumber))
        //    {
        //        reason = "Credit card number is invalid";
        //        return false;
        //    }
        //    if (!CardTypeIsValid(from.CreditCardType))
        //    {
        //        reason = "Card type is not supported " + from.CreditCardType;
        //        return false;
        //    }

        //    int num;
        //    if (!Int32.TryParse(from.CCExpirationMonth, out num))
        //    {
        //        reason = "Expiration month is in a invalid format";
        //        return false;
        //    }

        //    if (!Int32.TryParse(from.CCExpirationYear, out num))
        //    {
        //        reason = "Expiration year is in a invalid format";
        //        return false;
        //    }

        //    if (IsExpired(DateTime.Now, Int32.Parse(from.CCExpirationMonth), Int32.Parse(from.CCExpirationYear)))
        //    {
        //        reason = "Credit Card is expired";
        //        return false;
        //    }
        //    return true;
        //}

        private static bool IsExpired(DateTime currentDate, int expirationMonth, int expirationYear)
        {
            return currentDate.Year > expirationYear ||
                   (currentDate.Year == expirationYear && currentDate.Month > expirationMonth);
        }

        private static bool CardTypeIsValid(string cardType)
        {
            return CreditCardType.TryGetFromValue<CreditCardType>(cardType) != null;
        }

        private static bool NumberIsValid(string cardNumber)
        {
            if ( String.IsNullOrEmpty(cardNumber)) return false;

            var sum = cardNumber.Where(c => Char.IsDigit(c))
                        .Reverse()
                        .SelectMany((c, i) => ((c - '0') << (i & 1)).ToString())
                        .Sum(c => c - '0') % 10;
            return sum == 0;
        }

        public new virtual bool Equals(object obj)
        {
            if ( obj == null)
            {
                return false;
            }
            if ( obj is CreditCard)
            {
                var cc = obj as CreditCard;
                return cc.Token == Token || (cc.CardType == CardType && cc.EndingWith == EndingWith && cc.ExpirationMonth == ExpirationMonth && cc.ExpirationYear == ExpirationYear );
            }
            if ( obj is String )
            {
                return Token == obj.ToString();
            }
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Token != null ? Token.GetHashCode() : 0);
            }
        }

        #region Implementation of ISelectListItem<CreditCard>

        public virtual string Value
        {
            get { return Token; }
            set { Token = value; }
        }
        public virtual string DisplayName
        {
            get { return String.Format("{0} - ***********{1}", CardType.DisplayName, EndingWith); }
            set {}
        }

        #endregion
    }
}