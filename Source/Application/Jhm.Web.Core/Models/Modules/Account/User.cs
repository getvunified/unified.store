﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Web.Script.Serialization;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core.Models
{
    public class User : DefaultEntity, IUser
    {
        private IList<Role> roles = new List<Role>();
        private IList<DigitalDownload> downloads = new List<DigitalDownload>();
        private List<TransactionViewModel> transactionHistory; 

        private DateTime lastActivityDate;
        private DateTime lastLoginDate;
        private DateTime lastPasswordChangedDate;
        private DateTime creationDate;
        private DateTime lastLockedOutDate;
        private DateTime failedPasswordAttemptWindowStart;
        private DateTime failedPasswordAnswerAttemptWindowStart;

        public User(Guid id) : base(id)
        {
            creationDate = (DateTime)SqlDateTime.MinValue;
            lastPasswordChangedDate = (DateTime)SqlDateTime.MinValue;
            lastActivityDate = (DateTime)SqlDateTime.MinValue;
            lastLockedOutDate = (DateTime)SqlDateTime.MinValue;
            failedPasswordAnswerAttemptWindowStart = (DateTime)SqlDateTime.MinValue;
            failedPasswordAttemptWindowStart = (DateTime)SqlDateTime.MinValue;
            lastLoginDate = (DateTime)SqlDateTime.MinValue;
            AddProfile(new Profile());
        }

        public virtual string Username { get; set; }
        [ScriptIgnore]
        public virtual string ApplicationName { get; set; }
        public virtual string Email { get; set; }
        [ScriptIgnore]
        public virtual string Comment { get; set; }
        [ScriptIgnore]
        public virtual string Password { get; set; }
        [ScriptIgnore]
        public virtual string PasswordQuestion { get; set; }
        [ScriptIgnore]
        public virtual string PasswordAnswer { get; set; }
        [ScriptIgnore]
        public virtual bool IsApproved { get; set; }
        [ScriptIgnore]
        public virtual DateTime LastActivityDate 
        { get { return lastActivityDate.TruncateToSeconds(); } set { lastActivityDate = value; } }
        [ScriptIgnore]
        public virtual DateTime LastLoginDate
        {
            get { return lastLoginDate.TruncateToSeconds(); }
            set { lastLoginDate = value; }
        }
        [ScriptIgnore]
        public virtual DateTime LastPasswordChangedDate
        {
            get { return lastPasswordChangedDate.TruncateToSeconds(); }
            set { lastPasswordChangedDate = value; }
        }
        [ScriptIgnore]
        public virtual DateTime CreationDate
        {
            get { return creationDate.TruncateToSeconds(); }
            set { creationDate = value; }
        }
        [ScriptIgnore]
        public virtual bool IsOnLine { get; set; }
        [ScriptIgnore]
        public virtual bool IsLockedOut { get; set; }
        [ScriptIgnore]
        public virtual DateTime LastLockedOutDate
        {
            get { return lastLockedOutDate.TruncateToSeconds(); }
            set { lastLockedOutDate = value; }
        }
        [ScriptIgnore]
        public virtual int FailedPasswordAttemptCount { get; set; }
        [ScriptIgnore]
        public virtual int FailedPasswordAnswerAttemptCount { get; set; }

        [ScriptIgnore]
        public virtual bool MustChangePasswordOnLogin { get; set; }

        [ScriptIgnore]
        public virtual DateTime FailedPasswordAttemptWindowStart
        {
            get { return failedPasswordAttemptWindowStart.TruncateToSeconds(); }
            set { failedPasswordAttemptWindowStart = value; }
        }
        [ScriptIgnore]
        public virtual DateTime FailedPasswordAnswerAttemptWindowStart
        {
            get { return failedPasswordAnswerAttemptWindowStart.TruncateToSeconds(); }
            set { failedPasswordAnswerAttemptWindowStart = value; }
        }
        [ScriptIgnore]
        public virtual IEnumerable<Role> Roles { get { return roles; } }
        [ScriptIgnore]
        public virtual IEnumerable<DigitalDownload> Downloads { get { return downloads; } }

        [ScriptIgnore]
        public virtual List<TransactionViewModel> TransactionHistory
        {
            get
            {
                return transactionHistory?? (transactionHistory = new List<TransactionViewModel>());
            }
            set { transactionHistory = value; }
        } 


        public virtual Profile Profile { get; private set; }
        [ScriptIgnore]
        public virtual Cart Cart { get; set; }


        public User()
        {
            creationDate = (DateTime) SqlDateTime.MinValue;
            lastPasswordChangedDate = (DateTime) SqlDateTime.MinValue;
            lastActivityDate = (DateTime) SqlDateTime.MinValue;
            lastLockedOutDate = (DateTime) SqlDateTime.MinValue;
            failedPasswordAnswerAttemptWindowStart = (DateTime) SqlDateTime.MinValue;
            failedPasswordAttemptWindowStart = (DateTime) SqlDateTime.MinValue;
            lastLoginDate = (DateTime) SqlDateTime.MinValue;
            AddProfile(new Profile());
        }

        public virtual void AddProfile(Profile profile)
        {
            profile.AddUserInProfile(this);
            Profile = profile;
        }

        public virtual void AddRole(Role role)
        {
            role.AddUserInRole(this);
            roles.Add(role);
        }

        public virtual void RemoveRole(Role role)
        {
            role.RemoveUserInRole(this);
            roles.Remove(role);
        }

        /// <summary>
        /// The AccountNumber from DonorStudio
        /// </summary>
        [ScriptIgnore]
        public virtual long AccountNumber { get; set; }

        private IList<CreditCard> creditCards = new List<CreditCard>();
        [ScriptIgnore]
        public virtual IEnumerable<CreditCard> CreditCards 
        { 
            get { return creditCards; }
        }

        /// <summary>
        /// The initial DonorStudio environment assigned based on 
        /// the client IP address when the account was created or
        /// based on their Profile's PrimaryAddress.
        /// </summary>
        [ScriptIgnore]
        public virtual string DsEnvironment { get; set; }

        public virtual bool? IsDeleted { get; set; }

        public virtual string GlobalId { get; set; }

        #region CreditCard stuff

        public virtual bool AddCreditCard(CreditCard creditCard)
        {
            if (creditCard == null)
            {
                throw new ArgumentNullException("creditCard");
            }

            if (creditCards.ToLIST().Exists(cc => cc.Token == creditCard.Token))
            {
                // Won't add duplicated token
                return false;
            }

            var existingCreditCard = creditCards.ToLIST().Find(cc => cc.Equals(creditCard));
            if (existingCreditCard.IsNot().Null())
            {
                // Update token of existing card
                existingCreditCard.Token = creditCard.Token;
                return true;
            }

            creditCards.Add(creditCard);
            return true;
        }

        public virtual void DeleteCreditCard(Guid cardId)
        {
            var creditCard = creditCards.ToLIST().Find(cc => cc.Id == cardId);
            if (creditCard == null)
            {
                throw new Exception("Credit Card Not Found");
            }
            creditCards.Remove(creditCard);
        }

        public virtual CreditCard GetCreditCard(string token)
        {
            var creditCard = creditCards.ToLIST().Find(cc => cc.Equals(token));
            return creditCard;
        }

        /// <summary>
        /// Return a list  
        /// </summary>
        /// <returns></returns>
        public virtual List<CreditCard> GetUnexpiredCreditCards()
        {
            return GetUnexpiredCreditCards(DateTime.Now);
        }

        public virtual List<CreditCard> GetUnexpiredCreditCards(int month, int year)
        {
            var fromExpirationDate = new DateTime(year, month, 1);
            return GetUnexpiredCreditCards(fromExpirationDate);
        }

        public virtual List<CreditCard> GetUnexpiredCreditCards(DateTime fromExpirationDate)
        {
            return creditCards.ToLIST().FindAll(x => !x.IsExpired(fromExpirationDate));
        } 
        #endregion


        public virtual bool NewsLetterOptIn { get; set; }

        
    }
}
