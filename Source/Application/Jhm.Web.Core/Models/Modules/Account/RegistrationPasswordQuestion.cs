﻿using System;
using System.Collections.Generic;
using Foundation;

namespace Jhm.Web.Core.Models
{
    public class RegistrationPasswordQuestion : Enumeration<Guid>
    {
        public static readonly RegistrationPasswordQuestion Month_of_your_Salvation = new RegistrationPasswordQuestion(new Guid("E2F313E7-850C-4381-9D9A-4FC813B5069F"), "What was the month of your Salvation?");
        public static readonly RegistrationPasswordQuestion Favorite_book_of_the_Bible = new RegistrationPasswordQuestion(new Guid("222F37BD-97F5-490B-99F4-A8E58C485D6B"), "What is your favorite book of the Bible?");
        public static readonly RegistrationPasswordQuestion Name_of_high_school_you_attended = new RegistrationPasswordQuestion(new Guid("6E55ADD0-06C6-4874-8397-8C486155A841"), "What is the name of the high school you attended?");
        public static readonly RegistrationPasswordQuestion Mothers_maiden_name = new RegistrationPasswordQuestion(new Guid("F2BA038C-357E-4650-B261-4A07A59988D7"), "What is your Mother's maiden name?");
        public static readonly RegistrationPasswordQuestion Favorite_author = new RegistrationPasswordQuestion(new Guid("69F234B1-962A-4C2E-9BE9-651667B32135"), "What is your favorite author?");
        public static readonly RegistrationPasswordQuestion Name_of_first_pet = new RegistrationPasswordQuestion(new Guid("CFC4C4BA-E964-45B3-99F5-01C75BD1B82C"), "What is the	name of your first pet?");
        public static readonly RegistrationPasswordQuestion Name_of_favorite_singer = new RegistrationPasswordQuestion(new Guid("34FFEB4F-74D0-4844-B685-37C1E8E262C6"), "What is the name of your favorite singer?");
        public static readonly RegistrationPasswordQuestion Street_you_were_raised_on = new RegistrationPasswordQuestion(new Guid("D782280F-279A-447B-8250-952295EBCE5B"), "What is the street you were raised on?");
        public static readonly RegistrationPasswordQuestion Favorite_teacher = new RegistrationPasswordQuestion(new Guid("C18D2EEB-3B5E-4434-B3D3-F64315DC9F7E"), "What is your favorite teacher?");
        public static readonly RegistrationPasswordQuestion Fathers_middle_name = new RegistrationPasswordQuestion(new Guid("BCB5094C-FE82-4371-89DA-A32412B41DA2"), "What is your father's middle name?");
        
        public RegistrationPasswordQuestion()
        {
        }

        public RegistrationPasswordQuestion(Guid value, string displayName)
            : base(value, displayName)
        {
        }

        public static IEnumerable<RegistrationPasswordQuestion> AllSortedByDisplayName()
        {
            return GetAllSortedByDisplayName<RegistrationPasswordQuestion>();
        }
    }
}