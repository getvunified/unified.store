﻿namespace Jhm.Web.Core
{
    public interface IPhone 
    {
        PhoneType PhoneType { get; }
        string CountryCode { get; }
        string AreaCode { get; }
        string Number { get; }
        string Extension { get; }
    }
}