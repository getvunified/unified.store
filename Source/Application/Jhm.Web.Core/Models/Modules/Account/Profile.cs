﻿using System;
using System.Data.SqlTypes;
using System.Web.Script.Serialization;
using Jhm.Common.Framework.Extensions;
using Jhm.em3.Core;

namespace Jhm.Web.Core.Models
{
    public class Profile : DefaultEntity, IProfile
    {
        private DateTime lastActivityDate;
        private DateTime lastUpdatedDate;
        private DateTime birthDate;

        [ScriptIgnore]
        public virtual User User { get; set; }
        [ScriptIgnore]
        public virtual string ApplicationName { get; set; }
        [ScriptIgnore]
        public virtual bool IsAnonymous { get; set; }
        [ScriptIgnore]
        public virtual DateTime LastActivityDate
        {
            get { return lastActivityDate.TruncateToSeconds(); }
            set { lastActivityDate = value; }
        }
        [ScriptIgnore]
        public virtual DateTime LastUpdatedDate
        {
            get { return lastUpdatedDate.TruncateToSeconds(); }
            set { lastUpdatedDate = value; }
        }
        [ScriptIgnore]
        public virtual string Subscription { get; set; }
        [ScriptIgnore]
        public virtual string Language { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        [ScriptIgnore]
        public virtual string Gender { get; set; }
        [ScriptIgnore]
        public virtual DateTime BirthDate
        {
            get { return birthDate.TruncateToSeconds(); }
            set { birthDate = value; }
        }
        //public virtual string Occupation { get; set; }
        [ScriptIgnore]
        public virtual string Website { get; set; }
        [ScriptIgnore]
        public virtual string Company { get; set; }
        [ScriptIgnore]
        public virtual Address PrimaryAddress { get; set; }

        private Phone primaryPhone;
        [ScriptIgnore]
        public virtual Phone PrimaryPhone
        {
            get { return (primaryPhone = primaryPhone?? new Phone()); }
            set { primaryPhone = value; }
        }
        public virtual string Title { get; set; }
        [ScriptIgnore]
        public virtual string GoldenTicketCode { get; set; }


        public Profile()
        {
            lastActivityDate = (DateTime) SqlDateTime.MinValue;
            lastUpdatedDate = (DateTime) SqlDateTime.MinValue;
            birthDate = (DateTime) SqlDateTime.MinValue;
            primaryPhone = new Phone();
        }
        public virtual void AddUserInProfile(User user)
        {
            User = user;
        }





        
    }
}
