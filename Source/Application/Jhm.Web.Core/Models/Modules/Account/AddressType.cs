﻿using Foundation;

namespace Jhm.Web.Core
{
    public class AddressType : Enumeration<string>, IAddressType
    {
        //        Value	Description
        //BUS	Business Address
        //TEMP	Temporary Address
        //ALT	Alternative Address
        //BILL	Billing Address
        //MAIL	Mailing Address
        //SHIP	Shipping Address
        //DACTA	Date Activated Annual Address
        //CKADD	Check Address
        //DACT	Date Activated Address - One Time
        //KMSEMAIL	email address from conversion
        //ONR	OCC Name Record
        //PNR	Proper Name Record
        //REMIT	Remittance Address

        public static readonly AddressType Mailing = new AddressType("MAIL", "Mailing");
        public static readonly AddressType Billing = new AddressType("BILL", "Billing");
        public static readonly AddressType Shipping = new AddressType("SHIP", "Shipping");

        public AddressType(){}

        private AddressType(string value, string displayName): base(value, displayName)
        {
            Value = value;
            DisplayName = displayName;
        }

        #region Implementation of IAddressType

        public new string Value { get; set; }

        public new string DisplayName { get; set; }

        #endregion
    }
}
