﻿using System;
using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public interface IUser
    {

        string Username { get; set; }
        string ApplicationName { get; set; }
        string Email { get; set; }
        string Comment { get; set; }
        string Password { get; set; }
        string PasswordQuestion { get; set; }
        string PasswordAnswer { get; set; }
        bool IsApproved { get; set; }
        DateTime LastActivityDate { get; set; }
        DateTime LastLoginDate { get; set; }
        DateTime LastPasswordChangedDate { get; set; }
        DateTime CreationDate { get; set; }
        bool IsOnLine { get; set; }
        bool IsLockedOut { get; set; }
        DateTime LastLockedOutDate { get; set; }
        int FailedPasswordAttemptCount { get; set; }
        int FailedPasswordAnswerAttemptCount { get; set; }
        bool MustChangePasswordOnLogin { get; set; }
        DateTime FailedPasswordAttemptWindowStart { get; set; }
        DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }
        IEnumerable<Role> Roles { get; }
        long AccountNumber { get; set; }
        IEnumerable<CreditCard> CreditCards { get; }
        string DsEnvironment { get; set; }
        bool? IsDeleted { get; set; }
        string GlobalId { get; set; }
    }
}
