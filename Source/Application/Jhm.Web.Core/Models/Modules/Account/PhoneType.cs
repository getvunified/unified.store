﻿using Foundation;

namespace Jhm.Web.Core {
    public class PhoneType : Enumeration<string>, IPhoneType
    {
        //Value	Description
        //HOME	Home Phone Number
        //CELL	Cellular Phone Number
        //WORK	Work Phone Number
        //BUS	Business Phone Number
        //UNL	UnListed Phone Number
        //800	800 Phone Number
        //FAX	Fax Phone Number
        //FAX2	Fax Phone Number 2

        public static readonly PhoneType Home = new PhoneType("HOME", "Home Phone Number");
        public static readonly PhoneType Cell = new PhoneType("CELL", "Cellular Phone Number");
        public static readonly PhoneType Work = new PhoneType("WORK", "Work Phone Number");
        public static readonly PhoneType Business = new PhoneType("BUS", "Business Phone Number");
        public static readonly PhoneType Fax = new PhoneType("FAX", "Fax Phone Number");
        
        public PhoneType(){}

        PhoneType(string value, string displayName): base(value, displayName)
        {
            Value = value;
            DisplayName = displayName;
        }


        #region Implementation of IPhoneType

        public new string Value { get; set; }
        public new string DisplayName { get; set; }

        #endregion
    }
}
