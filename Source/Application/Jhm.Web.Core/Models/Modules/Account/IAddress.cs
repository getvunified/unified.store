﻿namespace Jhm.em3.Core
{
    public interface IAddress :  IAddressReadonly
    {
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string StateCode { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
    }

    public interface IAddressReadonly
    {
        string Address1 { get; }
        string Address2 { get; }
        string City { get; }
        string StateCode { get; }
        string PostalCode { get; }
        string Country { get; }
    }

}