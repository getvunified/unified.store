﻿using Jhm.Web.Core.Models;

namespace Jhm.Web.Core
{
    public interface ICreditCard
    {
        string EndingWith { get; set; }
        string Token { get; set; }
        CreditCardType CardType { get; set; }
        int ExpirationMonth { get; set; }
        int ExpirationYear { get; set; }
        User User { get; set; }
    }
}
