﻿#region -- Using Directives --

using System;
using System.Web.Script.Serialization;
using Jhm.Common;

#endregion

namespace Jhm.Web.Core.Models
{
    public class DigitalDownload : DefaultEntity, IDigitalDownload
    {        
        #region -- Constructors --

        public DigitalDownload()
        {
        }

        public DigitalDownload(User user, StockItem stockItem)
        {
            User = user;
            ProductCode = stockItem.ProductCode;
            MediaType = stockItem.MediaType.CodeSuffix;
            CreatedDate = DateTime.Now;
            ExpirationDate = DateTime.Now.AddDays(5).SetTime(23, 59, 59);
            DownloadAttemptsRemaining = 5;
            AverageDownloadTimeInSeconds = 0;
        }

        public DigitalDownload(Guid id, User user, StockItem stockItem)
            : base(id)
        {
            User = user;
            ProductCode = stockItem.ProductCode;
            MediaType = stockItem.MediaType.CodeSuffix;
            CreatedDate = DateTime.Now;
            ExpirationDate = DateTime.Now.AddDays(5).SetTime(23, 59, 59);
            DownloadAttemptsRemaining = 5;
            AverageDownloadTimeInSeconds = 0;
        }


        #endregion

        #region -- Properties --

        public virtual DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Utility property for JSON parsing functionalitty
        /// </summary>
        public virtual string ExpirationDateFormatted { get { return ExpirationDate.ToShortDateString(); } }

        public virtual DateTime CreatedDate { get; set; }

        public virtual int DownloadAttemptsRemaining { get; set; }

        [ScriptIgnore]
        public virtual User User { get; set; }

        public virtual string ProductCode { get; set; }

        public virtual string MediaType { get; set; }

        public virtual int AverageDownloadTimeInSeconds { get; set; }
        #endregion

        public virtual bool IsActive()
        {
            return ExpirationDate > DateTime.Today && DownloadAttemptsRemaining > 0;
        }
        
    }
}