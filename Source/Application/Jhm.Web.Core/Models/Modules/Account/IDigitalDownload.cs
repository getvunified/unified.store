﻿using System;
using Jhm.Web.Core.Models;

namespace Jhm.Web.Core
{
    public interface IDigitalDownload 
    {
        Guid Id { get; }
        DateTime ExpirationDate { get; }
        DateTime CreatedDate { get; }
        int DownloadAttemptsRemaining { get; }
        string ProductCode { get; }
        string MediaType { get; }
        User User { get; }
        int AverageDownloadTimeInSeconds { get; }
    }
}