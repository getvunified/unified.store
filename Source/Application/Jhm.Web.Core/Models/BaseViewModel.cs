﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.Web.Core.Models.Modules;

namespace Jhm.Web.Core.Models
{
    public class BaseViewModel : IFormatProvider, ICustomFormatter
    {
        public Client Client { get; private set; }

        public void RegisterClient(Client client)
        {
            Client = client;
        }

        public string FormatWithCompanyCulture(string format, object value)
        {
            return FormatHelper.FormatWithClientCulture(Client,format,value);
        }

        public string FormatWithCompanyCulture(string format, params object[] args)
        {
            return Client == null ? String.Format(format, args) : String.Format(this, format, args);
        }

        #region Implementation of IFormatProvider

        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        #endregion

        #region Implementation of ICustomFormatter

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            return String.IsNullOrEmpty(format) ? arg.ToString() : FormatHelper.FormatWithClientCulture(Client,format, arg);
        }

        #endregion
    }
}
