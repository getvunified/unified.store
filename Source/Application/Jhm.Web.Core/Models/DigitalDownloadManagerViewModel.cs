﻿using System.Collections.Generic;

namespace Jhm.Web.Core.Models
{
    public class DigitalDownloadManagerViewModel : IDigitalDownloadSearchCriteria
    {
        private IEnumerable<DigitalDownload> _results;
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string[] DownloadStatuses { get; set; }
        public string SelectedStatus { get; set; }

        public IEnumerable<DigitalDownload> Results
        {
            get { return (_results = _results?? new List<DigitalDownload>()); }
            set { _results = value; }
        }
    }

    public interface IDigitalDownloadSearchCriteria
    {
        string Username { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string SelectedStatus { get; set; }
    }
}
