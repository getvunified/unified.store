using System;
using System.Collections.Generic;
using Jhm.Common;
using Jhm.Common.Lookups;

namespace Jhm.Web.Core.Models
{
    public class State : Enumeration<int>, IState
    {
        public static readonly State Washington = new State(1,  "Washington","WA", Country.UnitedStates);
        public static readonly State Oregon = new State(2,  "Oregon","OR", Country.UnitedStates);
        public static readonly State Idaho = new State(3,  "Idaho","ID", Country.UnitedStates);
        public static readonly State Alaska = new State(4,  "Alaska","AK", Country.UnitedStates);
        public static readonly State California = new State(5,  "California", "CA", Country.UnitedStates);
        public static readonly State Nevada = new State(6,  "Nevada", "NV", Country.UnitedStates);
        public static readonly State Utah = new State(7,  "Utah", "UT", Country.UnitedStates);
        public static readonly State Arizona = new State(8,  "Arizona", "AZ", Country.UnitedStates);
        public static readonly State Colorado = new State(9,  "Colorado", "CO", Country.UnitedStates);
        public static readonly State NewMexico = new State(10,  "New Mexico", "NM", Country.UnitedStates);
        public static readonly State Wyoming = new State(27,  "Wyoming", "WY", Country.UnitedStates);
        public static readonly State Montana = new State(28,  "Montana", "MT", Country.UnitedStates);
        public static readonly State Nebraska = new State(12,  "Nebraska", "NE", Country.UnitedStates);
        public static readonly State Texas = new State(13,  "Texas", "TX", Country.UnitedStates);
        public static readonly State Oklahoma = new State(14,  "Oklahoma", "OK", Country.UnitedStates);
        public static readonly State Louisiana = new State(15,  "Louisiana", "LA", Country.UnitedStates);
        public static readonly State Arkansas = new State(16,  "Arkansas", "AR", Country.UnitedStates);
        public static readonly State Mississippi = new State(17,  "Mississippi", "MS", Country.UnitedStates);
        public static readonly State Alabama = new State(18,  "Alabama", "AL", Country.UnitedStates);
        public static readonly State Tennessee = new State(19,  "Tennessee", "TN", Country.UnitedStates);
        public static readonly State Missouri = new State(20,  "Missouri", "MO", Country.UnitedStates);
        public static readonly State Florida = new State(21,  "Florida", "FL", Country.UnitedStates);
        public static readonly State Georgia = new State(22,  "Georgia", "GA", Country.UnitedStates);
        public static readonly State SouthCarolina = new State(23,  "South Carolina", "SC", Country.UnitedStates);
        public static readonly State NorthCarolina = new State(24,  "North Carolina", "NC", Country.UnitedStates);
        public static readonly State Kansas = new State(11,  "Kansas", "KS", Country.UnitedStates);
        public static readonly State NorthDakota = new State(25,  "North Dakota", "ND", Country.UnitedStates);
        public static readonly State SouthDakota = new State(26,  "South Dakota", "SD", Country.UnitedStates);
        public static readonly State Wisconsin = new State(29,  "Wisconsin", "WI", Country.UnitedStates);
        public static readonly State Iowa = new State(30,  "Iowa", "IA", Country.UnitedStates);
        public static readonly State Illinois = new State(31,  "Illinois", "IL", Country.UnitedStates);
        public static readonly State Minnesota = new State(32,  "Minnesota", "MN", Country.UnitedStates);
        public static readonly State Michigan = new State(33,  "Michigan", "MI", Country.UnitedStates);
        public static readonly State Ohio = new State(34,  "Ohio", "OH", Country.UnitedStates);
        public static readonly State Kentucky = new State(35,  "Kentucky", "KY", Country.UnitedStates);
        public static readonly State Indiana = new State(36,  "Indiana", "IN", Country.UnitedStates);
        public static readonly State NewYork = new State(37,  "New York", "NY", Country.UnitedStates);
        public static readonly State Pennsylvania = new State(38,  "Pennsylvania", "PA", Country.UnitedStates);
        public static readonly State NewJersey = new State(39,  "New Jersey", "NJ", Country.UnitedStates);
        public static readonly State Maryland = new State(40,  "Maryland", "MD", Country.UnitedStates);
        public static readonly State Maine = new State(41,  "Maine", "ME", Country.UnitedStates);
        public static readonly State Vermont = new State(42,  "Vermont", "VT", Country.UnitedStates);
        public static readonly State NewHampshire = new State(43,  "New Hampshire", "NH", Country.UnitedStates);
        public static readonly State Massachusetts = new State(44,  "Massachusetts", "MA", Country.UnitedStates);
        public static readonly State Connecticut = new State(45,  "Connecticut", "CT", Country.UnitedStates);
        public static readonly State RhodeIsland = new State(46,  "Rhode Island", "RI", Country.UnitedStates);
        public static readonly State WestVirginia = new State(47,  "West Virginia", "WV", Country.UnitedStates);
        public static readonly State Virginia = new State(48,  "Virginia", "VA", Country.UnitedStates);
        public static readonly State WashingtonDC = new State(49, "District of Columbia D.C.", "DC", Country.UnitedStates);
        public static readonly State Delaware = new State(50,  "Delaware", "DE", Country.UnitedStates);
        public static readonly State Hawaii = new State(51, "Hawaii", "HI", Country.UnitedStates);
        public static readonly State MilitaryFlorida = new State(52, "Military Americas, Florida (AA)", "AA", Country.UnitedStates);
        public static readonly State MilitaryPacific = new State(53, "Military California, Pacific (AP)", "AP", Country.UnitedStates);
        public static readonly State MilitaryInternational = new State(54, "Military International, New York (AE)", "AE", Country.UnitedStates);
        

        public static readonly State Alberta = new State(101, "Alberta", "AB", Country.Canada);
        public static readonly State BritishColumbia = new State(102, "British Columbia", "BC", Country.Canada);
        public static readonly State Manitoba = new State(103, "Manitoba", "MB", Country.Canada);
        public static readonly State NewBrunswick = new State(104, "New Brunswick", "NB", Country.Canada);
        public static readonly State NewfoundlandLabrador = new State(105, "Newfoundland and Labrador", "NL", Country.Canada);
        public static readonly State NovaScotia = new State(106, "Nova Scotia", "NS", Country.Canada);
        public static readonly State Ontario = new State(107, "Ontario", "ON", Country.Canada);
        public static readonly State PrinceEdwardIsland = new State(108, "Prince Edward Island", "PE", Country.Canada);
        public static readonly State Quebec = new State(109, "Quebec", "PQ", Country.Canada);
        public static readonly State Saskatchewan = new State(110, "Saskatchewan", "SK", Country.Canada);
        public static readonly State NorthwestTerritories = new State(111, "Northwest Territories", "NWT", Country.Canada);
        public static readonly State Nunavut = new State(112, "Nunavut", "NT", Country.Canada);
        public static readonly State YukonTerritory = new State(113, "Yukon Territory", "YT", Country.Canada);

        public static readonly State Aberdeen = new State(114, "Aberdeen", "Aberdeen", Country.UnitedKingdom);
        public static readonly State StAlbans = new State(115, "St Albans", "St Albans", Country.UnitedKingdom);
        public static readonly State Birmingham = new State(116, "Birmingham", "Birmingham", Country.UnitedKingdom);
        public static readonly State Bath = new State(117, "Bath", "Bath", Country.UnitedKingdom);
        public static readonly State Blackburn = new State(118, "Blackburn", "Blackburn", Country.UnitedKingdom);
        public static readonly State Bradford = new State(119, "Bradford", "Bradford", Country.UnitedKingdom);
        public static readonly State Bournemouth = new State(120, "Bournemouth", "Bournemouth", Country.UnitedKingdom);
        public static readonly State Bolton = new State(121, "Bolton", "Bolton", Country.UnitedKingdom);
        public static readonly State Brighton = new State(122, "Brighton", "Brighton", Country.UnitedKingdom);
        public static readonly State Bromley = new State(123, "Bromley", "Bromley", Country.UnitedKingdom);
        public static readonly State Bristol = new State(124, "Bristol", "Bristol", Country.UnitedKingdom);
        public static readonly State Belfast = new State(125, "Belfast", "Belfast", Country.UnitedKingdom);
        public static readonly State Carlisle = new State(126, "Carlisle", "Carlisle", Country.UnitedKingdom);
        public static readonly State Cambridge = new State(127, "Cambridge", "Cambridge", Country.UnitedKingdom);
        public static readonly State Cardiff = new State(128, "Cardiff", "Cardiff", Country.UnitedKingdom);
        public static readonly State Chester = new State(129, "Chester", "Chester", Country.UnitedKingdom);
        public static readonly State Chelmsford = new State(130, "Chelmsford", "Chelmsford", Country.UnitedKingdom);
        public static readonly State Colchester = new State(131, "Colchester", "Colchester", Country.UnitedKingdom);
        public static readonly State Croydon = new State(132, "Croydon", "Croydon", Country.UnitedKingdom);
        public static readonly State Canterbury = new State(133, "Canterbury", "Canterbury", Country.UnitedKingdom);
        public static readonly State Coventry = new State(134, "Coventry", "Coventry", Country.UnitedKingdom);
        public static readonly State Crewe = new State(135, "Crewe", "Crewe", Country.UnitedKingdom);
        public static readonly State Dartford = new State(136, "Dartford", "Dartford", Country.UnitedKingdom);
        public static readonly State Dundee = new State(137, "Dundee", "Dundee", Country.UnitedKingdom);
        public static readonly State Derby = new State(138, "Derby", "Derby", Country.UnitedKingdom);
        public static readonly State Dumfries = new State(139, "Dumfries", "Dumfries", Country.UnitedKingdom);
        public static readonly State Durham = new State(140, "Durham", "Durham", Country.UnitedKingdom);
        public static readonly State Darlington = new State(141, "Darlington", "Darlington", Country.UnitedKingdom);
        public static readonly State Doncaster = new State(142, "Doncaster", "Doncaster", Country.UnitedKingdom);
        public static readonly State Dorchester = new State(143, "Dorchester", "Dorchester", Country.UnitedKingdom);
        public static readonly State Dudley = new State(144, "Dudley", "Dudley", Country.UnitedKingdom);
        public static readonly State EastLondon = new State(145, "East London", "East London", Country.UnitedKingdom);
        public static readonly State EastCentral = new State(146, "East Central", "East Central", Country.UnitedKingdom);
        public static readonly State Edinburgh = new State(147, "Edinburgh", "Edinburgh", Country.UnitedKingdom);
        public static readonly State Enfield = new State(148, "Enfield", "Enfield", Country.UnitedKingdom);
        public static readonly State Exeter = new State(149, "Exeter", "Exeter", Country.UnitedKingdom);
        public static readonly State Falkirk = new State(150, "Falkirk", "Falkirk", Country.UnitedKingdom);
        public static readonly State Blackpool = new State(151, "Blackpool", "Blackpool", Country.UnitedKingdom);
        public static readonly State Glasgow = new State(152, "Glasgow", "Glasgow", Country.UnitedKingdom);
        public static readonly State Gloucester = new State(153, "Gloucester", "Gloucester", Country.UnitedKingdom);
        public static readonly State Guildford = new State(154, "Guildford", "Guildford", Country.UnitedKingdom);
        public static readonly State Guernsey = new State(155, "Guernsey", "Guernsey", Country.UnitedKingdom);
        public static readonly State Harrow = new State(156, "Harrow", "Harrow", Country.UnitedKingdom);
        public static readonly State Huddersfield = new State(157, "Huddersfield", "Huddersfield", Country.UnitedKingdom);
        public static readonly State Harrogate = new State(158, "Harrogate", "Harrogate", Country.UnitedKingdom);
        public static readonly State HemelHempstead = new State(159, "Hemel Hempstead", "Hemel Hempstead", Country.UnitedKingdom);
        public static readonly State Hereford = new State(160, "Hereford", "Hereford", Country.UnitedKingdom);
        public static readonly State Hebrides = new State(161, "Hebrides", "Hebrides", Country.UnitedKingdom);
        public static readonly State Hull = new State(162, "Hull", "Hull", Country.UnitedKingdom);
        public static readonly State Halifax = new State(163, "Halifax", "Halifax", Country.UnitedKingdom);
        public static readonly State Ilford = new State(164, "Ilford", "Ilford", Country.UnitedKingdom);
        public static readonly State IsleofMan = new State(165, "Isle of Man", "Isle of Man", Country.UnitedKingdom);
        public static readonly State Ipswich = new State(166, "Ipswich", "Ipswich", Country.UnitedKingdom);
        public static readonly State Inverness = new State(167, "Inverness", "Inverness", Country.UnitedKingdom);
        public static readonly State Jersey = new State(168, "Jersey", "Jersey", Country.UnitedKingdom);
        public static readonly State Kilmarnock = new State(169, "Kilmarnock", "Kilmarnock", Country.UnitedKingdom);
        public static readonly State KingstonUponThames = new State(170, "Kingston Upon Thames", "Kingston Upon Thames", Country.UnitedKingdom);
        public static readonly State Kirkwall = new State(171, "Kirkwall", "Kirkwall", Country.UnitedKingdom);
        public static readonly State Kirkcaldy = new State(172, "Kirkcaldy", "Kirkcaldy", Country.UnitedKingdom);
        public static readonly State Liverpool = new State(173, "Liverpool", "Liverpool", Country.UnitedKingdom);
        public static readonly State Lancaster = new State(174, "Lancaster", "Lancaster", Country.UnitedKingdom);
        public static readonly State LlandrindodWells = new State(175, "Llandrindod Wells", "Llandrindod Wells", Country.UnitedKingdom);
        public static readonly State Leicester = new State(176, "Leicester", "Leicester", Country.UnitedKingdom);
        public static readonly State Llandudno = new State(177, "Llandudno", "Llandudno", Country.UnitedKingdom);
        public static readonly State Lincoln = new State(178, "Lincoln", "Lincoln", Country.UnitedKingdom);
        public static readonly State Leeds = new State(179, "Leeds", "Leeds", Country.UnitedKingdom);
        public static readonly State Luton = new State(180, "Luton", "Luton", Country.UnitedKingdom);
        public static readonly State Manchester = new State(181, "Manchester", "Manchester", Country.UnitedKingdom);
        public static readonly State Medway = new State(182, "Medway", "Medway", Country.UnitedKingdom);
        public static readonly State MiltonKeynes = new State(183, "Milton Keynes", "Milton Keynes", Country.UnitedKingdom);
        public static readonly State Motherwell = new State(184, "Motherwell", "Motherwell", Country.UnitedKingdom);
        public static readonly State NorthLondon = new State(185, "North London", "North London", Country.UnitedKingdom);
        public static readonly State NewcastleUponTyne = new State(186, "Newcastle Upon Tyne", "Newcastle Upon Tyne", Country.UnitedKingdom);
        public static readonly State Nottingham = new State(187, "Nottingham", "Nottingham", Country.UnitedKingdom);
        public static readonly State Northhampton = new State(188, "Northhampton", "Northhampton", Country.UnitedKingdom);
        public static readonly State Newport = new State(189, "Newport", "Newport", Country.UnitedKingdom);
        public static readonly State Norwich = new State(190, "Norwich", "Norwich", Country.UnitedKingdom);
        public static readonly State NWestLondon = new State(191, "N.West London", "N.West London", Country.UnitedKingdom);
        public static readonly State Oldham = new State(192, "Oldham", "Oldham", Country.UnitedKingdom);
        public static readonly State Oxford = new State(193, "Oxford", "Oxford", Country.UnitedKingdom);
        public static readonly State Paisley = new State(194, "Paisley", "Paisley", Country.UnitedKingdom);
        public static readonly State Peterborough = new State(195, "Peterborough", "Peterborough", Country.UnitedKingdom);
        public static readonly State Perth = new State(196, "Perth", "Perth", Country.UnitedKingdom);
        public static readonly State Plymouth = new State(197, "Plymouth", "Plymouth", Country.UnitedKingdom);
        public static readonly State Portsmouth = new State(198, "Portsmouth", "Portsmouth", Country.UnitedKingdom);
        public static readonly State Preston = new State(199, "Preston", "Preston", Country.UnitedKingdom);
        public static readonly State Reading = new State(200, "Reading", "Reading", Country.UnitedKingdom);
        public static readonly State Redhill = new State(201, "Redhill", "Redhill", Country.UnitedKingdom);
        public static readonly State Romford = new State(202, "Romford", "Romford", Country.UnitedKingdom);
        public static readonly State Sheffield = new State(203, "Sheffield", "Sheffield", Country.UnitedKingdom);
        public static readonly State Swansea = new State(204, "Swansea", "Swansea", Country.UnitedKingdom);
        public static readonly State SELondon = new State(205, "SE London", "SE London", Country.UnitedKingdom);
        public static readonly State Stevenage = new State(206, "Stevenage", "Stevenage", Country.UnitedKingdom);
        public static readonly State Stockport = new State(207, "Stockport", "Stockport", Country.UnitedKingdom);
        public static readonly State Slough = new State(208, "Slough", "Slough", Country.UnitedKingdom);
        public static readonly State Sutton = new State(209, "Sutton", "Sutton", Country.UnitedKingdom);
        public static readonly State Swindon = new State(210, "Swindon", "Swindon", Country.UnitedKingdom);
        public static readonly State Southampton = new State(211, "Southampton", "Southampton", Country.UnitedKingdom);
        public static readonly State Salisbury = new State(212, "Salisbury", "Salisbury", Country.UnitedKingdom);
        public static readonly State Sunderland = new State(213, "Sunderland", "Sunderland", Country.UnitedKingdom);
        public static readonly State SouthendonSea = new State(214, "Southend on Sea", "Southend on Sea", Country.UnitedKingdom);
        public static readonly State StokeonTrent = new State(215, "Stoke on Trent", "Stoke on Trent", Country.UnitedKingdom);
        public static readonly State SWLondon = new State(216, "S.W London", "S.W London", Country.UnitedKingdom);
        public static readonly State Shrewsbury = new State(217, "Shrewsbury", "Shrewsbury", Country.UnitedKingdom);
        public static readonly State Taunton = new State(218, "Taunton", "Taunton", Country.UnitedKingdom);
        public static readonly State Galshiels = new State(219, "Galshiels", "Galshiels", Country.UnitedKingdom);
        public static readonly State Telford = new State(220, "Telford", "Telford", Country.UnitedKingdom);
        public static readonly State Tunbridge = new State(221, "Tunbridge", "Tunbridge", Country.UnitedKingdom);
        public static readonly State Torquay = new State(222, "Torquay", "Torquay", Country.UnitedKingdom);
        public static readonly State Truro = new State(223, "Truro", "Truro", Country.UnitedKingdom);
        public static readonly State Cleveland = new State(224, "Cleveland", "Cleveland", Country.UnitedKingdom);
        public static readonly State Twickenham = new State(225, "Twickenham", "Twickenham", Country.UnitedKingdom);
        public static readonly State Southall = new State(226, "Southall", "Southall", Country.UnitedKingdom);
        public static readonly State WestLondon = new State(227, "West London", "West London", Country.UnitedKingdom);
        public static readonly State Warrington = new State(228, "Warrington", "Warrington", Country.UnitedKingdom);
        public static readonly State LondonWC = new State(229, "London WC", "London WC", Country.UnitedKingdom);
        public static readonly State Watford = new State(230, "Watford", "Watford", Country.UnitedKingdom);
        public static readonly State Wakefield = new State(231, "Wakefield", "Wakefield", Country.UnitedKingdom);
        public static readonly State Wigan = new State(232, "Wigan", "Wigan", Country.UnitedKingdom);
        public static readonly State Worcester = new State(233, "Worcester", "Worcester", Country.UnitedKingdom);
        public static readonly State Walsall = new State(234, "Walsall", "Walsall", Country.UnitedKingdom);
        public static readonly State Wolverhampton = new State(235, "Wolverhampton", "Wolverhampton", Country.UnitedKingdom);
        public static readonly State York = new State(236, "York", "York", Country.UnitedKingdom);
        public static readonly State Lerwick = new State(237, "Lerwick", "Lerwick", Country.UnitedKingdom);
        //US Territories
        public static readonly State AmericanSamoa = new State(238, "AmericanSamoa", "AS", Country.AmericanSamoa);
        public static readonly State Guam = new State(239, "Guam", "GU", Country.Guam);
        public static readonly State PuertoRico = new State(240, "PuertoRico", "PR", Country.PuertoRico);
        public static readonly State VirginIsland = new State(241, "VirginIsland", "VI", Country.VirginIslandsUS);

        public static readonly List<State> UnitedKingdomAreas =
            Collection.Sort(new List<State>
                                {
                                    Aberdeen,
                                    StAlbans,
                                    Birmingham,
                                    Bath,
                                    Blackburn,
                                    Bradford,
                                    Bournemouth,
                                    Bolton,
                                    Brighton,
                                    Bromley,
                                    Bristol,
                                    Belfast,
                                    Carlisle,
                                    Cambridge,
                                    Cardiff,
                                    Chester,
                                    Chelmsford,
                                    Colchester,
                                    Croydon,
                                    Canterbury,
                                    Coventry,
                                    Crewe,
                                    Dartford,
                                    Dundee,
                                    Derby,
                                    Dumfries,
                                    Durham,
                                    Darlington,
                                    Doncaster,
                                    Dorchester,
                                    Dudley,
                                    EastLondon,
                                    EastCentral,
                                    Edinburgh,
                                    Enfield,
                                    Exeter,
                                    Falkirk,
                                    Blackpool,
                                    Glasgow,
                                    Gloucester,
                                    Guildford,
                                    Guernsey,
                                    Harrow,
                                    Huddersfield,
                                    Harrogate,
                                    HemelHempstead,
                                    Hereford,
                                    Hebrides,
                                    Hull,
                                    Halifax,
                                    Ilford,
                                    IsleofMan,
                                    Ipswich,
                                    Inverness,
                                    Jersey,
                                    Kilmarnock,
                                    KingstonUponThames,
                                    Kirkwall,
                                    Kirkcaldy,
                                    Liverpool,
                                    Lancaster,
                                    LlandrindodWells,
                                    Leicester,
                                    Llandudno,
                                    Lincoln,
                                    Leeds,
                                    Luton,
                                    Manchester,
                                    Medway,
                                    MiltonKeynes,
                                    Motherwell,
                                    NorthLondon,
                                    NewcastleUponTyne,
                                    Nottingham,
                                    Northhampton,
                                    Newport,
                                    Norwich,
                                    NWestLondon,
                                    Oldham,
                                    Oxford,
                                    Paisley,
                                    Peterborough,
                                    Perth,
                                    Plymouth,
                                    Portsmouth,
                                    Preston,
                                    Reading,
                                    Redhill,
                                    Romford,
                                    Sheffield,
                                    Swansea,
                                    SELondon,
                                    Stevenage,
                                    Stockport,
                                    Slough,
                                    Sutton,
                                    Swindon,
                                    Southampton,
                                    Salisbury,
                                    Sunderland,
                                    SouthendonSea,
                                    StokeonTrent,
                                    SWLondon,
                                    Shrewsbury,
                                    Taunton,
                                    Galshiels,
                                    Telford,
                                    Tunbridge,
                                    Torquay,
                                    Truro,
                                    Cleveland,
                                    Twickenham,
                                    Southall,
                                    WestLondon,
                                    Warrington,
                                    LondonWC,
                                    Watford,
                                    Wakefield,
                                    Wigan,
                                    Worcester,
                                    Walsall,
                                    Wolverhampton,
                                    York,
                                    Lerwick

    },(x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.name, y.name));

        public static readonly List<State> CanadianTerritories = 
            Collection.Sort(new List<State>
                                                {
                                                    Alberta,
                                                    BritishColumbia,
                                                    Manitoba,
                                                    NewBrunswick,
                                                    NewfoundlandLabrador,
                                                    NorthwestTerritories,
                                                    NovaScotia,
                                                    Nunavut,
                                                    Ontario,
                                                    PrinceEdwardIsland,
                                                    Quebec,
                                                    Saskatchewan,
                                                    YukonTerritory 
                }, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.name, y.name));

        public static readonly List<State> UnitedStates = Collection.Sort(new List<State>{
            Washington,
            Oregon,
            Idaho,
            Alaska,
            California,
            Nevada,
            Utah,
            Arizona,
            Colorado,
            NewMexico,
            Kansas,
            Nebraska,
            Texas,
            Oklahoma,
            Louisiana,
            Arkansas,
            Mississippi,
            Alabama,
            Tennessee,
            Missouri,
            Florida,
            Georgia,
            SouthCarolina,
            NorthCarolina,
            NorthDakota,
            SouthDakota,
            Wyoming,
            Montana,
            Wisconsin,
            Iowa,
            Illinois,
            Minnesota,
            Michigan,
            Ohio,
            Kentucky,
            Indiana,
            NewYork,
            Pennsylvania,
            NewJersey,
            Maryland,
            Maine,
            Vermont,
            NewHampshire,
            Massachusetts,
            Connecticut,
            RhodeIsland,
            WestVirginia,
            Virginia,
            WashingtonDC,
            Delaware,
            Hawaii}, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.name, y.name));

        public static readonly List<State> UsTerritories = Collection.Sort(new List<State>{
            AmericanSamoa,
            Guam,
            PuertoRico,
            VirginIsland
        
        }, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.name, y.name));
        
        private readonly int id;
        private string name;
        private string code;
        private Country country;

        public State(){}

        private State(int id, string name, string code, Country country)
        {
            this.id = id;
            this.name = name;
            this.code = code;
            this.country = country;
        }

        public virtual int Id
        {
            get { return id; }
        }

        public virtual string Name
        {
            get { return name;  }
            private set { name = value; }
        }

        public virtual string Code
        {
            get { return code; }
            private set { code = value; }
        }

        public virtual Country Country
        {
            get { return country; }
            private set { country = value; }
        }

        
    }
}