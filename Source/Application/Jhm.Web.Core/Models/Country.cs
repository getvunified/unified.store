using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Jhm.Common;
using Jhm.Common.Framework.Extensions;
using Jhm.Common.Lookups;

namespace Jhm.Web.Core.Models
{
    public class Country : Enumeration<int>, ICountry
    {
        public static readonly int USCompanyCode = 1;
        public static readonly int CanadaCompanyCode = 2;
        public static readonly int UKCompanyCode = 3;

        public static readonly Country Afghanistan = new Country(1, "AF", "Afghanistan", "AFG", 4, null, null, "+93", UKCompanyCode);
        public static readonly Country Albania = new Country(2, "AL", "Albania", "ALB", 8, null, null, "+355", UKCompanyCode);
        public static readonly Country Algeria = new Country(3, "DZ", "Algeria", "DZA", 12, null, null, "+213", UKCompanyCode);
        public static readonly Country AmericanSamoa = new Country(4, "AS", "American Samoa", "ASM", 16, null, null, "+1", USCompanyCode);
        public static readonly Country Andorra = new Country(5, "AD", "Andorra", "AND", 20, null, null, "+376", UKCompanyCode);
        public static readonly Country Angola = new Country(6, "AO", "Angola", "AGO", 24, null, null, "+244", UKCompanyCode);
        public static readonly Country Anguilla = new Country(7, "AI", "Anguilla", "AIA", 660, null, null, "+1", UKCompanyCode);
        public static readonly Country Antarctica = new Country(8, "AQ", "Antarctica", null, null, null, null, "", CanadaCompanyCode);
        public static readonly Country AntiguaandBarbuda = new Country(9, "AG", "Antigua and Barbuda", "ATG", 28, null, null, "+1", USCompanyCode);
        public static readonly Country Argentina = new Country(10, "AR", "Argentina", "ARG", 32, null, null, "+54", USCompanyCode);
        public static readonly Country Armenia = new Country(11, "AM", "Armenia", "ARM", 51, null, null, "+374", UKCompanyCode);
        public static readonly Country Aruba = new Country(12, "AW", "Aruba", "ABW", 533, null, null, "+297", USCompanyCode);
        public static readonly Country Australia = new Country(13, "AU", "Australia", "AUS", 36, null, null, "+61", UKCompanyCode);
        public static readonly Country Austria = new Country(14, "AT", "Austria", "AUT", 40, null, null, "+43", UKCompanyCode);
        public static readonly Country Azerbaijan = new Country(15, "AZ", "Azerbaijan", "AZE", 31, null, null, "+994", UKCompanyCode);
        public static readonly Country Bahamas = new Country(16, "BS", "Bahamas", "BHS", 44, null, null, "+1", USCompanyCode);
        public static readonly Country Bahrain = new Country(17, "BH", "Bahrain", "BHR", 48, null, null, "+973", UKCompanyCode);
        public static readonly Country Bangladesh = new Country(18, "BD", "Bangladesh", "BGD", 50, null, null, "+880", UKCompanyCode);
        public static readonly Country Barbados = new Country(19, "BB", "Barbados", "BRB", 52, null, null, "+1", USCompanyCode);
        public static readonly Country Belarus = new Country(20, "BY", "Belarus", "BLR", 112, null, null, "+375", UKCompanyCode);
        public static readonly Country Belgium = new Country(21, "BE", "Belgium", "BEL", 56, null, null, "+32", UKCompanyCode);
        public static readonly Country Belize = new Country(22, "BZ", "Belize", "BLZ", 84, null, null, "+501", USCompanyCode);
        public static readonly Country Benin = new Country(23, "BJ", "Benin", "BEN", 204, null, null, "+229", UKCompanyCode);
        public static readonly Country Bermuda = new Country(24, "BM", "Bermuda", "BMU", 60, null, null, "+1", USCompanyCode);
        public static readonly Country Bhutan = new Country(25, "BT", "Bhutan", "BTN", 64, null, null, "+975", UKCompanyCode);
        public static readonly Country Bolivia = new Country(26, "BO", "Bolivia", "BOL", 68, null, null, "+591", USCompanyCode);
        public static readonly Country BosniaandHerzegovina = new Country(27, "BA", "Bosnia and Herzegovina", "BIH", 70, null, null, "+387", UKCompanyCode);
        public static readonly Country Botswana = new Country(28, "BW", "Botswana", "BWA", 72, null, null, "+267", UKCompanyCode);
        public static readonly Country BouvetIsland = new Country(29, "BV", "Bouvet Island", null, null, null, null, "", UKCompanyCode);
        public static readonly Country Brazil = new Country(30, "BR", "Brazil", "BRA", 76, null, null, "+55", USCompanyCode);
        public static readonly Country BritishIndianOceanTerritory = new Country(31, "IO", "British Indian Ocean Territory", null, null, null, null, "246", UKCompanyCode);
        public static readonly Country BruneiDarussalam = new Country(32, "BN", "Brunei Darussalam", "BRN", 96, null, null, "+673", UKCompanyCode);
        public static readonly Country Bulgaria = new Country(33, "BG", "Bulgaria", "BGR", 100, null, null, "+359", UKCompanyCode);
        public static readonly Country BurkinaFaso = new Country(34, "BF", "Burkina Faso", "BFA", 854, null, null, "+226", UKCompanyCode);
        public static readonly Country Burundi = new Country(35, "BI", "Burundi", "BDI", 108, null, null, "+257", UKCompanyCode);
        public static readonly Country Cambodia = new Country(36, "KH", "Cambodia", "KHM", 116, null, null, "+855", UKCompanyCode);
        public static readonly Country Cameroon = new Country(37, "CM", "Cameroon", "CMR", 120, null, null, "+237", UKCompanyCode);
        public static readonly Country Canada = new Country(38, "CA", "Canada", "CAN", 124, @"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$", @"^(?:(?:[\+]?(?<CountryCode>[\d]{1,3}(?:[ ]+|[\-.])))?[(]?(?<AreaCode>[\d]{3})[\-/)]?(?:[ ]+)?)(?<Number>[a-zA-Z2-9][a-zA-Z0-9 \-.]{6,})(?:(?:[ ]+|[xX]|(i:ext[\.]?)){1,2}(?<Ext>[\d]{1,5}))?$", "+1", CanadaCompanyCode);
        public static readonly Country CapeVerde = new Country(39, "CV", "Cape Verde", "CPV", 132, null, null, "+238", UKCompanyCode);
        public static readonly Country CaymanIslands = new Country(40, "KY", "Cayman Islands", "CYM", 136, null, null, "+1", USCompanyCode);
        public static readonly Country CentralAfricanRepublic = new Country(41, "CF", "Central African Republic", "CAF", 140, null, null, "+236", UKCompanyCode);
        public static readonly Country Chad = new Country(42, "TD", "Chad", "TCD", 148, null, null, "+235", UKCompanyCode);
        public static readonly Country Chile = new Country(43, "CL", "Chile", "CHL", 152, null, null, "+56", USCompanyCode);
        public static readonly Country China = new Country(44, "CN", "China", "CHN", 156, null, null, "+86", UKCompanyCode);
        public static readonly Country ChristmasIsland = new Country(45, "CX", "Christmas Island", null, null, null, null, "+61", UKCompanyCode);
        public static readonly Country CocosKeelingIslands = new Country(46, "CC", "Cocos (Keeling) Islands", null, null, null, null, "+61", UKCompanyCode);
        public static readonly Country Colombia = new Country(47, "CO", "Colombia", "COL", 170, null, null, "+57", USCompanyCode);
        public static readonly Country Comoros = new Country(48, "KM", "Comoros", "COM", 174, null, null, "+269", UKCompanyCode);
        public static readonly Country Congo = new Country(49, "CG", "Congo, Republic of the", "COG", 178, null, null, "+242", UKCompanyCode);
        public static readonly Country CongotheDemocraticRepublicofthe = new Country(50, "CD", "Congo, the Democratic Republic of the", "COD", 180, null, null, "+243", UKCompanyCode);
        public static readonly Country CookIslands = new Country(51, "CK", "Cook Islands", "COK", 184, null, null, "+682", UKCompanyCode);
        public static readonly Country CostaRica = new Country(52, "CR", "Costa Rica", "CRI", 188, null, null, "+506", USCompanyCode);
        public static readonly Country CoteDIvoire = new Country(53, "CI", "Cote D'Ivoire (Ivory Coast)", "CIV", 384, null, null, "+225", UKCompanyCode);
        public static readonly Country Croatia = new Country(54, "HR", "Croatia", "HRV", 191, null, null, "+385", UKCompanyCode);
        public static readonly Country Cuba = new Country(55, "CU", "Cuba", "CUB", 192, null, null, "+53", USCompanyCode);
        public static readonly Country Curacao = new Country(56, "CW", "Curacao, Country of", "CUW", 531, null, null, "+599", USCompanyCode);
        public static readonly Country Cyprus = new Country(57, "CY", "Cyprus", "CYP", 196, null, null, "+357", UKCompanyCode);
        public static readonly Country CzechRepublic = new Country(58, "CZ", "Czech Republic", "CZE", 203, null, null, "+420", UKCompanyCode);
        public static readonly Country Denmark = new Country(59, "DK", "Denmark", "DNK", 208, null, null, "+45", UKCompanyCode);
        public static readonly Country Djibouti = new Country(60, "DJ", "Djibouti", "DJI", 262, null, null, "+253", UKCompanyCode);
        public static readonly Country Dominica = new Country(61, "DM", "Dominica", "DMA", 212, null, null, "+1", USCompanyCode);
        public static readonly Country DominicanRepublic = new Country(62, "DO", "Dominican Republic", "DOM", 214, null, null, "+1", USCompanyCode);
        public static readonly Country Ecuador = new Country(63, "EC", "Ecuador", "ECU", 218, null, null, "+593", USCompanyCode);
        public static readonly Country Egypt = new Country(64, "EG", "Egypt", "EGY", 818, null, null, "+20", UKCompanyCode);
        public static readonly Country ElSalvador = new Country(65, "SV", "El Salvador", "SLV", 222, null, null, "+503", USCompanyCode);
        public static readonly Country EquatorialGuinea = new Country(66, "GQ", "Equatorial Guinea", "GNQ", 226, null, null, "+240", USCompanyCode);
        public static readonly Country Eritrea = new Country(67, "ER", "Eritrea", "ERI", 232, null, null, "+291", UKCompanyCode);
        public static readonly Country Estonia = new Country(68, "EE", "Estonia", "EST", 233, null, null, "+372", UKCompanyCode);
        public static readonly Country Ethiopia = new Country(69, "ET", "Ethiopia", "ETH", 231, null, null, "+251", UKCompanyCode);
        public static readonly Country FalklandIslandsMalvinas = new Country(70, "FK", "Falkland Islands (Malvinas)", "FLK", 238, null, null, "+500", USCompanyCode);
        public static readonly Country FaroeIslands = new Country(71, "FO", "Faroe Islands", "FRO", 234, null, null, "+298", UKCompanyCode);
        public static readonly Country Fiji = new Country(72, "FJ", "Fiji", "FJI", 242, null, null, "+679", UKCompanyCode);
        public static readonly Country Finland = new Country(73, "FI", "Finland", "FIN", 246, null, null, "+358", UKCompanyCode);
        public static readonly Country France = new Country(74, "FR", "France", "FRA", 250, null, null, "+33", UKCompanyCode);
        public static readonly Country FrenchGuiana = new Country(75, "GF", "French Guiana", "GUF", 254, null, null, "+594", UKCompanyCode);
        public static readonly Country FrenchPolynesia = new Country(76, "PF", "French Polynesia", "PYF", 258, null, null, "+689", UKCompanyCode);
        public static readonly Country FrenchSouthernTerritories = new Country(77, "TF", "French Southern Territories", "ATF", 260, null, null, "", UKCompanyCode);
        public static readonly Country Gabon = new Country(78, "GA", "Gabon", "GAB", 266, null, null, "+241", UKCompanyCode);
        public static readonly Country Gambia = new Country(79, "GM", "Gambia", "GMB", 270, null, null, "+220", UKCompanyCode);
        public static readonly Country Georgia = new Country(80, "GE", "Georgia", "GEO", 268, null, null, "+995", UKCompanyCode);
        public static readonly Country Germany = new Country(81, "DE", "Germany", "DEU", 276, null, null, "+49", UKCompanyCode);
        public static readonly Country Ghana = new Country(82, "GH", "Ghana", "GHA", 288, null, null, "+233", UKCompanyCode);
        public static readonly Country Gibraltar = new Country(83, "GI", "Gibraltar", "GIB", 292, null, null, "+350", UKCompanyCode);
        public static readonly Country Greece = new Country(84, "GR", "Greece", "GRC", 300, null, null, "+30", UKCompanyCode);
        public static readonly Country Greenland = new Country(85, "GL", "Greenland", "GRL", 304, null, null, "+299", UKCompanyCode);
        public static readonly Country Grenada = new Country(86, "GD", "Grenada", "GRD", 308, null, null, "+1", USCompanyCode);
        public static readonly Country Guadeloupe = new Country(87, "GP", "Guadeloupe", "GLP", 312, null, null, "+590", UKCompanyCode);
        public static readonly Country Guam = new Country(88, "GU", "Guam", "GUM", 316, null, null, "+1", USCompanyCode);
        public static readonly Country Guatemala = new Country(89, "GT", "Guatemala", "GTM", 320, null, null, "+502", USCompanyCode);
        public static readonly Country Guernsey = new Country(90, "GG", "Guernsey, Bailiwick of", "GGY", 831, null, null, "+44", UKCompanyCode);
        public static readonly Country Guinea = new Country(91, "GN", "Guinea", "GIN", 324, null, null, "+224", UKCompanyCode);
        public static readonly Country GuineaBissau = new Country(92, "GW", "Guinea-Bissau", "GNB", 624, null, null, "+245", UKCompanyCode);
        public static readonly Country Guyana = new Country(93, "GY", "Guyana", "GUY", 328, null, null, "+592", UKCompanyCode);
        public static readonly Country Haiti = new Country(94, "HT", "Haiti", "HTI", 332, null, null, "+509", USCompanyCode);
        public static readonly Country HeardIslandandMcdonaldIslands = new Country(95, "HM", "Heard Island and Mcdonald Islands", "HMD", 334, null, null, "", UKCompanyCode);
        public static readonly Country HolySeeVaticanCityState = new Country(96, "VA", "Holy See (Vatican City State)", "VAT", 336, null, null, "+39", UKCompanyCode);
        public static readonly Country Honduras = new Country(97, "HN", "Honduras", "HND", 340, null, null, "+504", USCompanyCode);
        public static readonly Country HongKong = new Country(98, "HK", "Hong Kong", "HKG", 344, null, null, "+852", UKCompanyCode);
        public static readonly Country Hungary = new Country(99, "HU", "Hungary", "HUN", 348, null, null, "+36", UKCompanyCode);
        public static readonly Country Iceland = new Country(100, "IS", "Iceland", "ISL", 352, null, null, "+354", UKCompanyCode);
        public static readonly Country India = new Country(101, "IN", "India", "IND", 356, null, null, "+91", UKCompanyCode);
        public static readonly Country Indonesia = new Country(102, "ID", "Indonesia", "IDN", 360, null, null, "+62", UKCompanyCode);
        public static readonly Country IranIslamicRepublicof = new Country(103, "IR", "Iran, Islamic Republic of", "IRN", 364, null, null, "+98", UKCompanyCode);
        public static readonly Country Iraq = new Country(104, "IQ", "Iraq", "IRQ", 368, null, null, "+964", UKCompanyCode);
        public static readonly Country Ireland = new Country(105, "IE", "Ireland", "IRL", 372, null, null, "+353", UKCompanyCode);
        public static readonly Country IsleOfMan = new Country(106, "IM", "Isle of Man", "IMN", 833, null, null, "+44", UKCompanyCode);
        public static readonly Country Israel = new Country(107, "IL", "Israel", "ISR", 376, null, null, "+972", UKCompanyCode);
        public static readonly Country Italy = new Country(108, "IT", "Italy", "ITA", 380, null, null, "+39", UKCompanyCode);
        public static readonly Country Jamaica = new Country(109, "JM", "Jamaica", "JAM", 388, null, null, "+1", USCompanyCode);
        public static readonly Country Japan = new Country(110, "JP", "Japan", "JPN", 392, null, null, "+81", UKCompanyCode);
        public static readonly Country Jersey = new Country(111, "JE", "Jersey", "JEY", 832, null, null, "+44", UKCompanyCode);
        public static readonly Country Jordan = new Country(112, "JO", "Jordan", "JOR", 400, null, null, "+962", UKCompanyCode);
        public static readonly Country Kazakhstan = new Country(113, "KZ", "Kazakhstan", "KAZ", 398, null, null, "+7", UKCompanyCode);
        public static readonly Country Kenya = new Country(114, "KE", "Kenya", "KEN", 404, null, null, "+254", UKCompanyCode);
        public static readonly Country Kiribati = new Country(115, "KI", "Kiribati", "KIR", 296, null, null, "+686", UKCompanyCode);
        public static readonly Country KoreaDemocraticPeoplesRepublicof = new Country(116, "KP", "Korea, Democratic People's Republic of", "PRK", 408, null, null, "+850", UKCompanyCode);
        public static readonly Country KoreaRepublicof = new Country(117, "KR", "Korea, Republic of", "KOR", 410, null, null, "+82", UKCompanyCode);
        public static readonly Country Kuwait = new Country(118, "KW", "Kuwait", "KWT", 414, null, null, "+965", UKCompanyCode);
        public static readonly Country Kyrgyzstan = new Country(119, "KG", "Kyrgyzstan", "KGZ", 417, null, null, "+996", UKCompanyCode);
        public static readonly Country LaoPeoplesDemocraticRepublic = new Country(120, "LA", "Lao People's Democratic Republic", "LAO", 418, null, null, "+856", UKCompanyCode);
        public static readonly Country Latvia = new Country(121, "LV", "Latvia", "LVA", 428, null, null, "+371", UKCompanyCode);
        public static readonly Country Lebanon = new Country(122, "LB", "Lebanon", "LBN", 422, null, null, "+961", UKCompanyCode);
        public static readonly Country Lesotho = new Country(123, "LS", "Lesotho", "LSO", 426, null, null, "+266", UKCompanyCode);
        public static readonly Country Liberia = new Country(124, "LR", "Liberia", "LBR", 430, null, null, "+231", UKCompanyCode);
        public static readonly Country LibyanArabJamahiriya = new Country(125, "LY", "Great Socialist People's Libyan Arab Jamahiriya", "LBY", 434, null, null, "+218", UKCompanyCode);
        public static readonly Country Liechtenstein = new Country(126, "LI", "Liechtenstein", "LIE", 438, null, null, "+423", UKCompanyCode);
        public static readonly Country Lithuania = new Country(127, "LT", "Lithuania", "LTU", 440, null, null, "+370", UKCompanyCode);
        public static readonly Country Luxembourg = new Country(128, "LU", "Luxembourg", "LUX", 442, null, null, "+352", UKCompanyCode);
        public static readonly Country Macau = new Country(129, "MO", "Macau", "MAC", 446, null, null, "+853", UKCompanyCode);
        public static readonly Country MacedoniaTheFormerYugoslavRepublicof = new Country(130, "MK", "Macedonia, the Former Yugoslav Republic of", "MKD", 807, null, null, "+389", UKCompanyCode);
        public static readonly Country Madagascar = new Country(131, "MG", "Madagascar", "MDG", 450, null, null, "+261", UKCompanyCode);
        public static readonly Country Malawi = new Country(132, "MW", "Malawi", "MWI", 454, null, null, "+265", UKCompanyCode);
        public static readonly Country Malaysia = new Country(133, "MY", "Malaysia", "MYS", 458, null, null, "+60", UKCompanyCode);
        public static readonly Country Maldives = new Country(134, "MV", "Maldives", "MDV", 462, null, null, "+960", UKCompanyCode);
        public static readonly Country Mali = new Country(135, "ML", "Mali", "MLI", 466, null, null, "+223", UKCompanyCode);
        public static readonly Country Malta = new Country(136, "MT", "Malta", "MLT", 470, null, null, "+356", UKCompanyCode);
        public static readonly Country MarshallIslands = new Country(137, "MH", "Marshall Islands, Republic of the", "MHL", 584, null, null, "+692", UKCompanyCode);
        public static readonly Country Martinique = new Country(138, "MQ", "Martinique", "MTQ", 474, null, null, "+596", USCompanyCode);
        public static readonly Country Mauritania = new Country(139, "MR", "Mauritania", "MRT", 478, null, null, "+222", UKCompanyCode);
        public static readonly Country Mauritius = new Country(140, "MU", "Mauritius", "MUS", 480, null, null, "+230", UKCompanyCode);
        public static readonly Country Mayotte = new Country(141, "YT", "Mayotte", "", 175, null, null, "+262", UKCompanyCode);
        public static readonly Country Mexico = new Country(142, "MX", "Mexico", "MEX", 484, null, null, "+52", USCompanyCode);
        public static readonly Country MicronesiaFederatedStatesof = new Country(143, "FM", "Micronesia, Federated States of", "FSM", 583, null, null, "+691", UKCompanyCode);
        public static readonly Country MoldovaRepublicof = new Country(144, "MD", "Moldova, Republic of", "MDA", 498, null, null, "+373", UKCompanyCode);
        public static readonly Country Monaco = new Country(145, "MC", "Monaco", "MCO", 492, null, null, "+377", UKCompanyCode);
        public static readonly Country Mongolia = new Country(146, "MN", "Mongolia", "MNG", 496, null, null, "+976", UKCompanyCode);
        public static readonly Country Montenegro = new Country(147, "ME", "Montenegro", "MNE", 499, null, null, "+382", UKCompanyCode);
        public static readonly Country Montserrat = new Country(148, "MS", "Montserrat", "MSR", 500, null, null, "+1", USCompanyCode);
        public static readonly Country Morocco = new Country(149, "MA", "Morocco", "MAR", 504, null, null, "+212", UKCompanyCode);
        public static readonly Country Mozambique = new Country(150, "MZ", "Mozambique", "MOZ", 508, null, null, "+258", UKCompanyCode);
        public static readonly Country Myanmar = new Country(151, "MM", "Burma (Myanmar)", "MMR", 104, null, null, "+95", UKCompanyCode);
        public static readonly Country Namibia = new Country(152, "NA", "Namibia", "NAM", 516, null, null, "+264", UKCompanyCode);
        public static readonly Country Nauru = new Country(153, "NR", "Nauru", "NRU", 520, null, null, "+674", UKCompanyCode);
        public static readonly Country Nepal = new Country(154, "NP", "Nepal", "NPL", 524, null, null, "+977", UKCompanyCode);
        public static readonly Country Netherlands = new Country(155, "NL", "Netherlands", "NLD", 528, null, null, "+31", UKCompanyCode);
        public static readonly Country NewCaledonia = new Country(156, "NC", "New Caledonia", "NCL", 540, null, null, "+687", UKCompanyCode);
        public static readonly Country NewZealand = new Country(157, "NZ", "New Zealand", "NZL", 554, null, null, "+64", UKCompanyCode);
        public static readonly Country Nicaragua = new Country(158, "NI", "Nicaragua", "NIC", 558, null, null, "+505", USCompanyCode);
        public static readonly Country Niger = new Country(159, "NE", "Niger", "NER", 562, null, null, "+227", UKCompanyCode);
        public static readonly Country Nigeria = new Country(160, "NG", "Nigeria", "NGA", 566, null, null, "+234", UKCompanyCode);
        public static readonly Country Niue = new Country(161, "NU", "Niue", "NIU", 570, null, null, "+683", UKCompanyCode);
        public static readonly Country NorfolkIsland = new Country(162, "NF", "Norfolk Island", "NFK", 574, null, null, "+672", UKCompanyCode);
        public static readonly Country NorthernMarianaIslands = new Country(163, "MP", "Northern Mariana Islands", "MNP", 580, null, null, "+1", UKCompanyCode);
        public static readonly Country Norway = new Country(164, "NO", "Norway", "NOR", 578, null, null, "+47", UKCompanyCode);
        public static readonly Country Oman = new Country(165, "OM", "Oman", "OMN", 512, null, null, "+968", UKCompanyCode);
        public static readonly Country Pakistan = new Country(166, "PK", "Pakistan", "PAK", 586, null, null, "+92", UKCompanyCode);
        public static readonly Country Palau = new Country(167, "PW", "Palau", "PLW", 585, null, null, "+680", UKCompanyCode);
        public static readonly Country PalestinianTerritoryOccupied = new Country(168, "PS", "Palestinian Territory, Occupied", "PSE", 275, null, null, "+970", UKCompanyCode);
        public static readonly Country Panama = new Country(169, "PA", "Panama", "PAN", 591, null, null, "+507", USCompanyCode);
        public static readonly Country PapuaNewGuinea = new Country(170, "PG", "Papua New Guinea", "PNG", 598, null, null, "+675", UKCompanyCode);
        public static readonly Country Paraguay = new Country(171, "PY", "Paraguay", "PRY", 600, null, null, "+595", USCompanyCode);
        public static readonly Country Peru = new Country(172, "PE", "Peru", "PER", 604, null, null, "+51", USCompanyCode);
        public static readonly Country Philippines = new Country(173, "PH", "Philippines", "PHL", 608, null, null, "+63", USCompanyCode);
        public static readonly Country Pitcairn = new Country(174, "PN", "Pitcairn Islands", "PCN", 612, null, null, "+872", USCompanyCode);
        public static readonly Country Poland = new Country(175, "PL", "Poland", "POL", 616, null, null, "+48", UKCompanyCode);
        public static readonly Country Portugal = new Country(176, "PT", "Portugal", "PRT", 620, null, null, "+351", USCompanyCode);
        public static readonly Country PuertoRico = new Country(177, "PR", "Puerto Rico", "PRI", 630, null, null, "+1", USCompanyCode);
        public static readonly Country Qatar = new Country(178, "QA", "Qatar", "QAT", 634, null, null, "+974", UKCompanyCode);
        public static readonly Country Reunion = new Country(179, "RE", "Reunion", "REU", 638, null, null, "+262", UKCompanyCode);
        public static readonly Country Romania = new Country(180, "RO", "Romania", "ROM", 642, null, null, "+40", UKCompanyCode);
        public static readonly Country RussianFederation = new Country(181, "RU", "Russian Federation", "RUS", 643, null, null, "+7", UKCompanyCode);
        public static readonly Country Rwanda = new Country(182, "RW", "Rwanda", "RWA", 646, null, null, "+250", UKCompanyCode);
        public static readonly Country SaintBarthelemy = new Country(183, "BL", "Saint Barthélemy", "BLM", 652, null, null, "590", USCompanyCode);
        public static readonly Country SaintHelena = new Country(184, "SH", "Saint Helena", "SHN", 654, null, null, "+290", USCompanyCode);
        public static readonly Country SaintKittsandNevis = new Country(185, "KN", "Saint Kitts and Nevis", "KNA", 659, null, null, "+1", USCompanyCode);
        public static readonly Country SaintLucia = new Country(186, "LC", "Saint Lucia", "LCA", 662, null, null, "+1", USCompanyCode);
        public static readonly Country SaintMartin = new Country(187, "MF", "Saint Martin", "MAF", 663, null, null, "590", USCompanyCode);
        public static readonly Country SaintPierreandMiquelon = new Country(188, "PM", "Saint Pierre and Miquelon", "SPM", 666, null, null, "+508", USCompanyCode);
        public static readonly Country SaintVincentandtheGrenadines = new Country(189, "VC", "Saint Vincent and the Grenadines", "VCT", 670, null, null, "+1", USCompanyCode);
        public static readonly Country Samoa = new Country(190, "WS", "Samoa", "WSM", 882, null, null, "+685", USCompanyCode);
        public static readonly Country SanMarino = new Country(191, "SM", "San Marino", "SMR", 674, null, null, "+378", UKCompanyCode);
        public static readonly Country SaoTomeandPrincipe = new Country(192, "ST", "Sao Tome and Principe", "STP", 678, null, null, "+239", UKCompanyCode);
        public static readonly Country SaudiArabia = new Country(193, "SA", "Saudi Arabia", "SAU", 682, null, null, "+966", UKCompanyCode);
        public static readonly Country Senegal = new Country(194, "SN", "Senegal", "SEN", 686, null, null, "+221", UKCompanyCode);
        public static readonly Country Serbia = new Country(195, "RS", "Serbia, Republic of", "SRB", 688, null, null, "+381", UKCompanyCode);
        public static readonly Country Seychelles = new Country(196, "SC", "Seychelles", "SYC", 690, null, null, "+248", UKCompanyCode);
        public static readonly Country SierraLeone = new Country(197, "SL", "Sierra Leone", "SLE", 694, null, null, "+232", UKCompanyCode);
        public static readonly Country Singapore = new Country(198, "SG", "Singapore", "SGP", 702, null, null, "+65", UKCompanyCode);
        public static readonly Country SintMaarten = new Country(199, "SX", "Sint Maarten", "SXM", 534, null, null, "+599", USCompanyCode);
        public static readonly Country Slovakia = new Country(200, "SK", "Slovakia", "SVK", 703, null, null, "+421", UKCompanyCode);
        public static readonly Country Slovenia = new Country(201, "SI", "Slovenia", "SVN", 705, null, null, "+386", UKCompanyCode);
        public static readonly Country SolomonIslands = new Country(202, "SB", "Solomon Islands", "SLB", 90, null, null, "+677", UKCompanyCode);
        public static readonly Country Somalia = new Country(203, "SO", "Somalia", "SOM", 706, null, null, "+252", UKCompanyCode);
        public static readonly Country SouthAfrica = new Country(204, "ZA", "South Africa", "ZAF", 710, null, null, "+27", UKCompanyCode);
        public static readonly Country SouthGeorgiaandtheSouthSandwichIslands = new Country(205, "GS", "South Georgia and the South Sandwich Islands", "SGS", 239, null, null, "", USCompanyCode);
        public static readonly Country Spain = new Country(206, "ES", "Spain", "ESP", 724, null, null, "+34", UKCompanyCode);
        public static readonly Country SriLanka = new Country(207, "LK", "Sri Lanka", "LKA", 144, null, null, "+94", UKCompanyCode);
        public static readonly Country Sudan = new Country(208, "SD", "Sudan", "SDN", 736, null, null, "+249", UKCompanyCode);
        public static readonly Country Suriname = new Country(209, "SR", "Suriname", "SUR", 740, null, null, "+597", UKCompanyCode);
        public static readonly Country SvalbardandJanMayen = new Country(210, "SJ", "Svalbard and Jan Mayen", "SJM", 744, null, null, "+47", UKCompanyCode);
        public static readonly Country Swaziland = new Country(211, "SZ", "Swaziland", "SWZ", 748, null, null, "+268", UKCompanyCode);
        public static readonly Country Sweden = new Country(212, "SE", "Sweden", "SWE", 752, null, null, "+46", UKCompanyCode);
        public static readonly Country Switzerland = new Country(213, "CH", "Switzerland", "CHE", 756, null, null, "+41", UKCompanyCode);
        public static readonly Country SyrianArabRepublic = new Country(214, "SY", "Syrian Arab Republic", "SYR", 760, null, null, "+963", UKCompanyCode);
        public static readonly Country TaiwanProvinceofChina = new Country(215, "TW", "Taiwan, Province of China", "TWN", 158, null, null, "+886", UKCompanyCode);
        public static readonly Country Tajikistan = new Country(216, "TJ", "Tajikistan", "TJK", 762, null, null, "+992", UKCompanyCode);
        public static readonly Country TanzaniaUnitedRepublicof = new Country(217, "TZ", "Tanzania, United Republic of", "TZA", 834, null, null, "+255", UKCompanyCode);
        public static readonly Country Thailand = new Country(218, "TH", "Thailand", "THA", 764, null, null, "+66", UKCompanyCode);
        public static readonly Country TimorLeste = new Country(219, "TL", "Timor-Leste, The Democratic Republic of", "TLS", 626, null, null, "+670", UKCompanyCode);
        public static readonly Country Togo = new Country(220, "TG", "Togo", "TGO", 768, null, null, "+228", UKCompanyCode);
        public static readonly Country Tokelau = new Country(221, "TK", "Tokelau", "TKL", 772, null, null, "+690", UKCompanyCode);
        public static readonly Country Tonga = new Country(222, "TO", "Tonga", "TON", 776, null, null, "+676", UKCompanyCode);
        public static readonly Country TrinidadandTobago = new Country(223, "TT", "Trinidad and Tobago", "TTO", 780, null, null, "+1", USCompanyCode);
        public static readonly Country Tunisia = new Country(224, "TN", "Tunisia", "TUN", 788, null, null, "+216", UKCompanyCode);
        public static readonly Country Turkey = new Country(225, "TR", "Turkey", "TUR", 792, null, null, "+90", UKCompanyCode);
        public static readonly Country Turkmenistan = new Country(226, "TM", "Turkmenistan", "TKM", 795, null, null, "+993", UKCompanyCode);
        public static readonly Country TurksandCaicosIslands = new Country(227, "TC", "Turks and Caicos Islands", "TCA", 796, null, null, "+1", USCompanyCode);
        public static readonly Country Tuvalu = new Country(228, "TV", "Tuvalu", "TUV", 798, null, null, "+688", UKCompanyCode);
        public static readonly Country Uganda = new Country(229, "UG", "Uganda", "UGA", 800, null, null, "+256", UKCompanyCode);
        public static readonly Country Ukraine = new Country(230, "UA", "Ukraine", "UKR", 804, null, null, "+380", UKCompanyCode);
        public static readonly Country UnitedArabEmirates = new Country(231, "AE", "United Arab Emirates", "ARE", 784, null, null, "+971", UKCompanyCode);
        public static readonly Country UnitedKingdom = new Country(232, "GB", "United Kingdom", "GBR", 826, null, null, "+44", UKCompanyCode);
        public static readonly Country UnitedStates = new Country(233, "US", "United States", "USA", 840, @"^((\d{5}-\d{4})|(\d{5}))$", @"^(?:(?:[\+]?(?<CountryCode>[\d]{1,3}))?(?:\s*[\-.]?\s*)?[(]?(?<AreaCode>[\d]{3})[\-/)]?(?:[ ]+)?)(?<Number>[a-zA-Z2-9][a-zA-Z0-9 \-.]{6,})(?:(?:[ ]+|[xX]|(i:ext[\.]?)){1,2}(?<Ext>[\d]{1,5}))?$", "+1", USCompanyCode);
        public static readonly Country UnitedStatesMinorOutlyingIslands = new Country(234, "UM", "United States Minor Outlying Islands", " UMI", 581, null, null, "+1", USCompanyCode);
        public static readonly Country Uruguay = new Country(235, "UY", "Uruguay", "URY", 858, null, null, "+598", UKCompanyCode);
        public static readonly Country Uzbekistan = new Country(236, "UZ", "Uzbekistan", "UZB", 860, null, null, "+998", UKCompanyCode);
        public static readonly Country Vanuatu = new Country(237, "VU", "Vanuatu", "VUT", 548, null, null, "+678", UKCompanyCode);
        public static readonly Country Venezuela = new Country(238, "VE", "Venezuela", "VEN", 862, null, null, "+58", USCompanyCode);
        public static readonly Country VietNam = new Country(239, "VN", "Viet Nam", "VNM", 704, null, null, "+84", UKCompanyCode);
        public static readonly Country VirginIslandsBritish = new Country(240, "VG", "Virgin Islands, British", "VGB", 92, null, null, "+1", USCompanyCode);
        public static readonly Country VirginIslandsUS = new Country(241, "VI", "Virgin Islands, U.S.", "VIR", 850, null, null, "+1", USCompanyCode);
        public static readonly Country WallisandFutuna = new Country(242, "WF", "Wallis and Futuna Islands, Territory of the", "WLF", 876, null, null, "+681", UKCompanyCode);
        public static readonly Country WesternSahara = new Country(243, "EH", "Western Sahara", "ESH", 732, null, null, "+212", UKCompanyCode);
        public static readonly Country Yemen = new Country(244, "YE", "Yemen", "YEM", 887, null, null, "+967", UKCompanyCode);
        public static readonly Country Zambia = new Country(245, "ZM", "Zambia", "ZMB", 894, null, null, "+260", UKCompanyCode);
        public static readonly Country Zimbabwe = new Country(246, "ZW", "Zimbabwe", "ZWE", 716, null, null, "+263", UKCompanyCode);


        public static readonly List<Country> CanadaOfficeCountries = new List<Country>{ Canada };
        //TODO: This list should be populated when UK office integration is implemented
        public static readonly List<Country> UKOfficeCountries = new List<Country>();
        // For now US Office will handle all countries except Canada
        //public static readonly List<Country> USOfficeCountries = GetAll<Country>().ToLIST().FindAll(c => c != Canada);
        
        private readonly int id;
        private string iso;
        private string name;
        private string iso3;
        private int? numberCode;
        private Regex postalCodeRegex;
        private Regex phoneRegex;
        
        public Country(){}
        
        public Country(int id, string iso, string name, string iso3, int? numberCode, string postalCodePattern, string phonePattern, string phoneCode, int companyCode):base(id, name)
        {
            this.id = id;
            this.iso = iso;
            this.name = name;
            this.iso3 = iso3;
            this.numberCode = numberCode;
            
            if(!string.IsNullOrEmpty(postalCodePattern))
            {
                postalCodeRegex = new Regex(postalCodePattern);
            }

            if (!string.IsNullOrEmpty(phonePattern))
            {
                phoneRegex = new Regex(phonePattern);
            }

            PhoneCode = phoneCode;
            CompanyCode = companyCode;
        }

       

        public virtual int Id
        {
            get { return id; }
        }

        public virtual string Iso
        {
            get { return iso; }
            private set { iso = value; }
        }

        public virtual string Name
        {
            get { return name; }
            private set { name = value; }
        }

        public virtual string Iso3
        {
            get { return iso3; }
            private set { iso3 = value; }
        }

        public virtual int? NumberCode
        {
            get { return numberCode; }
            private set { numberCode = value; }
        }

        public Company Company
        {
            get { return TryGetFromValue<Company>(CompanyCode); }
        }

        public virtual string PhoneCode { get; set; }

        public virtual string PostalCodePattern
        {
            get { return postalCodeRegex != null ? postalCodeRegex.ToString() : null; }
            private set
            {
                if (!string.IsNullOrEmpty(value))
                    postalCodeRegex = new Regex(value);
            }
        }
        public virtual string PhonePattern
        {
            get { return phoneRegex != null ? phoneRegex.ToString() : null; }
            private set
            {
                if (!string.IsNullOrEmpty(value))
                    phoneRegex = new Regex(value);
            }
        }

        public static readonly List<Country> All  = GetAll<Country>().ToLIST();

        public static bool IsPostalCodeValid(string countryIso, string postalCode)
        {
            var found = All.Find(country => country.Iso.Equals(countryIso));
            if (found != null && found.postalCodeRegex != null)
            {
                return found.postalCodeRegex.IsMatch(postalCode);
            }
            
            return true;
        }

        public static bool IsPhoneValid(string countryIso, string phone)
        {
            var found = All.Find(country => country.Iso.Equals(countryIso));
            if (found != null && found.phoneRegex != null)
            {
#if DEBUG
                foreach (string s in found.phoneRegex.Split(phone))
                {
                    Console.WriteLine(s);
                }
#endif 
                return found.phoneRegex.IsMatch(phone);
            }
            
            return true;
        }

        public override void SetDisplayName(string displayName)
        {
            Name = displayName;
        }

        public override bool Equals(object obj)
        {
            if ( obj is Country )
            {
                var country = (Country) obj;
                return country == this;
            }
            if ( obj is String )
            {
                var str = (String) obj;
                return Iso3 == str || Iso == str || Name == str;
            }
            return base.Equals(obj);
        }

        public static Country FindByISOCode(string isoCode)
        {
            return All.Find(x => x.iso == isoCode || x.iso3 == isoCode);
        }

        public virtual int CompanyCode { get; set; }
    }
}