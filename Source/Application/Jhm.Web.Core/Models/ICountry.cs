namespace Jhm.Web.Core.Models
{
    public interface ICountry
    {
        int Id { get; }
        string Iso { get; }
        string Name { get; }
        string Iso3 { get; }
        int? NumberCode { get; }
        string PostalCodePattern { get; }
        string PhonePattern { get; }
    }
}