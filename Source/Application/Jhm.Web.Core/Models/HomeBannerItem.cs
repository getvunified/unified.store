﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Web.Core.Models
{
    public interface IHomeBannerItem
    {
        string ImageUrl { get; set; }
        string ImageLinkUrl { get; set; }
        string Title { get; set; }
        string SubTitle { get; set; }
        int Position { get; set; }
        bool IsActive { get; set; }
        bool ShowExpired { get; set; }
        DateTime? StartDate { get; set; }
        DateTime? EndDate { get; set; }
        string CustomAttributes { get; set; }
        string ExternalHtml { get; set; }
        IList<HomeBannerCompany> Companies { get; set; }
    }

    public class HomeBannerItem : DefaultEntity, IHomeBannerItem
    {
        public virtual string ImageUrl { get; set; }
        public virtual string ImageLinkUrl { get; set; }
        public virtual string Title { get; set; }
        public virtual string SubTitle { get; set; }
        public virtual int Position { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool ShowExpired { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string CustomAttributes { get; set; }
        public virtual string ExternalHtml { get; set; }
        public virtual bool OpenTargetInNewWindow { get; set; }

        private IList<HomeBannerCompany> companies;
        public virtual IList<HomeBannerCompany> Companies
        {
            get { return companies; }
            set { companies = new List<HomeBannerCompany>(value);}
        }

        public HomeBannerItem()
        {
            companies = new List<HomeBannerCompany>();
        }

        public virtual bool HasExpired()
        {
            return (StartDate != null && StartDate > DateTime.Now) || (EndDate != null && EndDate < DateTime.Now);
        }


        public virtual bool IsCurrentlyVisible()
        {
            var now = DateTime.Now;
            return IsActive && Companies.Any() && (StartDate == null || StartDate <= now) && (EndDate == null || EndDate >= now);
        }

        
    }
}
