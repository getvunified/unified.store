namespace Jhm.Web.Core.Models
{
    public interface IState
    {
        int Id { get; }
        string Name { get; }
        string Code { get; }
    }
}