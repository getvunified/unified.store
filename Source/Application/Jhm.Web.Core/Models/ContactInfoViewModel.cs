﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public class ContactInfoViewModel : CartViewModel
    {
        [DisplayName("First Name")]
        [Required(ErrorMessage="First Name is required")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [DisplayName("Address 1")]
        [Required(ErrorMessage = "Address 1 is required")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }
        
        [Required(ErrorMessage = "City is required")]
        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("State/Province")]
        [Required(ErrorMessage = "State/Province is required")]
        public string State { get; set; }

        [Required(ErrorMessage = "Zip code is required")]
        [DisplayName("Zip Code")]
        public string Zip { get; set; }

        [DisplayName("Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        [DisplayName("Phone Number")]
        public string Phone { get; set; }

        [DisplayName("Fax Number")]
        public string Fax { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string Email { get; set; }

        [DisplayName("Website")]
        public string Website { get; set; }

        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [DisplayName("Profession Title")]
        public string Title { get; set; }

        [DisplayName("Comments")]
        public string Comments { get; set; }

        //[DisplayName("Do you authorize John Hagee Ministries to email you promotional information?")]
        //public bool AuthorizeToSendPromotionalEmail { get; set; }

        //[DisplayName("Do you authorize John Hagee Ministries to share your information with John Hagee Ministries select promotional partners?")]
        //public bool AuthorizeToShareInformation { get; set; }
    }
}
