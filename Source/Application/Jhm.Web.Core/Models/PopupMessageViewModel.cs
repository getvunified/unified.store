﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;

namespace Jhm.Web.Core.Models
{
    public class PopupMessageViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }

        private List<HtmlAnchor> _buttons;
        public List<HtmlAnchor> Buttons
        {
            get { return _buttons ?? (_buttons = new List<HtmlAnchor>()); }
            set { _buttons = value; }
        }
    }
}
