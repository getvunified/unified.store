﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Jhm.Common.Utilities;

namespace Jhm.Web.Core.Models
{
    public partial class Company
    {
        

        private static Company InitializeUKCompany()
        {
            var company = new Company(UKCompanyCode, UKCompanyBinaryCode, "JHM UK", Country.All, Country.UnitedKingdom, State.UnitedKingdomAreas, "UKPROD", "120765951509", "DiDhCsUeuhcqcxaT", "WEB", true, "GBP", String.Empty, "British Pounds Sterling", new CultureInfo("en-GB"));

            var additionalData = new Dictionary<DataKeys, string>()
                                    {
                                        {DataKeys.PhoneNumber,"44-1793-862146"},
                                        {DataKeys.FullAddress,"P.O. Box 2959, Swindon, UK SN6 7WS"},
                                        {DataKeys.AddressLine1,"P.O. Box 2959"},
                                        {DataKeys.AddressLine2,"Swindon, UK SN6 7WS"},
                                        {DataKeys.LegalName,String.Empty},
                                        {DataKeys.TaxId,String.Empty},
                                        {DataKeys.TaxIdLabel,String.Empty},
                                        {DataKeys.TaxMessage,String.Empty}
                                    };


            company.AdditionalData = new ReadOnlyDictionary<DataKeys, string>(additionalData);
            company.InternationalShippingMethod = ShippingMethod.UKInternational;

            return company;
        }
    }
}
