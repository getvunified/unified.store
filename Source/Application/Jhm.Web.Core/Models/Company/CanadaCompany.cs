﻿using System.Collections.Generic;
using System.Globalization;
using Jhm.Common.Utilities;

namespace Jhm.Web.Core.Models
{
    public partial class Company
    {
        

        private static Company InitializeCanadaCompany()
        {
            var company = new Company(CanadaCompanyCode, CanadaCompanyBinaryCode, "JHM Canada", Country.All, Country.Canada, State.CanadianTerritories, "CANPROD", "120760706568",
                                      "k5HgzHECSaGmC7Py", "WEB", true, "CND", "C", "Canadian Dollars", new CultureInfo("en-CA"));

            var additionalData = new Dictionary<DataKeys, string>()
                                    {
                                        {DataKeys.PhoneNumber,"1-877-454-6226"},
                                        {DataKeys.FullAddress,"P.O. Box 9900, Toronto, Ontario M3C 2T9"},
                                        {DataKeys.AddressLine1,"P.O. Box 9900"},
                                        {DataKeys.AddressLine2,"Toronto, Ontario M3C 2T9"},
                                        {DataKeys.LegalName,"Global Evangelism Television Canada, Inc."},
                                        {DataKeys.TaxId,"89995 5439 RR0001"},
                                        {DataKeys.TaxIdLabel,"Registration Number:"},
                                        {DataKeys.TaxMessage,"CANADA REVENUE AGENCY WWW.cra.gc.calendar/charities"}
                                    };


            company.AdditionalData = new ReadOnlyDictionary<DataKeys, string>(additionalData);
            company.InternationalShippingMethod = ShippingMethod.InternationalCanada;
            return company;
        }
    }
}
