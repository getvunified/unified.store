﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Jhm.Common.Utilities;

namespace Jhm.Web.Core.Models
{
    public partial class Company
    {
        

        private static Company InitializeUSCompany()
        {
            var company = new Company(USCompanyCode, USCompanyBinaryCode, "JHM US", Country.All, Country.UnitedStates, State.UnitedStates, "JHMPROD", "120792111910",
                                      "UT6cotlEZSe38Ogv", "WEB", true, "USD", String.Empty, "US Dollars", new CultureInfo("en-US"));

            var additionalData = new Dictionary<DataKeys, string>()
                                    {
                                        {DataKeys.PhoneNumber,"1-800-854-9899"},
                                        {DataKeys.FullAddress,"P.O. Box 1400, San Antonio, Texas 78295-1400"},
                                        {DataKeys.AddressLine1,"P.O. Box 1400"},
                                        {DataKeys.AddressLine2,"San Antonio, Texas 78295-1400"},
                                        {DataKeys.LegalName,"Global Evangelism, Inc."},
                                        {DataKeys.TaxId,"74-1764843"},
                                        {DataKeys.TaxIdLabel,"Tax Id#"},
                                        {DataKeys.TaxMessage,"The Internal Revenue Code permits you to deduct the amount you gift to John Hagee Ministries that exceeds the value of materials received. We have provided goods and services consisting solely of intangible religious benefits, except as will be indicated in a premium amount. A good faith estimate of the value of any goods or services received has been included in the premium amount as required by the IRS."},

                                    };
            company.AdditionalData = new ReadOnlyDictionary<DataKeys, string>(additionalData);
            company.InternationalShippingMethod = ShippingMethod.USPSInternational;
            return company;
        }
    }
}
