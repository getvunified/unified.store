﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.Web.Core.Models.Modules.ECommerce;

namespace Jhm.Web.Core.Models
{
    public class XmlSyndicationRecordViewModel : CartViewModel
    {
        public virtual string Title { get; set; }
        public virtual string Content { get; set; }
        public virtual string Id { get; set; }
        public virtual string PublicationDate { get; set; }
    }
}
