﻿using System;
using Jhm.Common;

namespace Jhm.Web.Core
{
    [Serializable]
    public class DefaultEntity : EntityBase<Guid>
    {
        public DefaultEntity() { }

        //DO NOT USE Added only to support test harnesses
        public DefaultEntity(Guid id) : base(id) { }
       
    }
}
