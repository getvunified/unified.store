﻿using System;
using System.Text;
using System.Web.Mvc;
using Jhm.Web.Core.Models;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Core
{
    public static class CountryListHelper
    {
        public static string[] CountriesWithoutZipCodes = new string[]{
                                                                   "AFG",
                                                                   "AGO",
                                                                   "AIA",
                                                                   "ATA",
                                                                   "ABW",
                                                                   "BHS",
                                                                   "BLZ",
                                                                   "BEN",
                                                                   "BTN",
                                                                   "BWA",
                                                                   "BDI",
                                                                   "CMR",
                                                                   "COL",
                                                                   "COM",
                                                                   "COG",
                                                                   "COK",
                                                                   "CIV",
                                                                   "CUB",
                                                                   "CYP",
                                                                   "DJI",
                                                                   "DMA",
                                                                   "ECU",
                                                                   "GNQ",
                                                                   "ERI",
                                                                   "ETH",
                                                                   "FJI",
                                                                   "GMB",
                                                                   "GIB",
                                                                   "GRD",
                                                                   "GIN",
                                                                   "GUY",
                                                                   "HKG",
                                                                   "IRL",
                                                                   "JOR",
                                                                   "KEN",
                                                                   "KIR",
                                                                   "MWI",
                                                                   "MLI",
                                                                   "MRT",
                                                                   "MUS",
                                                                   "MSR",
                                                                   "NAM",
                                                                   "NRU",
                                                                   "ANT",
                                                                   "NIU",
                                                                   "OMN",
                                                                   "PAN",
                                                                   "RWA",
                                                                   "KNA",
                                                                   "LCA",
                                                                   "VCT",
                                                                   "STP",
                                                                   "SAU",
                                                                   "SYC",
                                                                   "SLE",
                                                                   "SLB",
                                                                   "SOM",
                                                                   "ZAF",
                                                                   "SUR",
                                                                   "TZA",
                                                                   "TKL",
                                                                   "TON",
                                                                   "TTO",
                                                                   "TUV",
                                                                   "UGA",
                                                                   "ARE",
                                                                   "VUT",
                                                                   "YEM",
                                                                   "ZMB",
                                                                   "ZWE"
                                                               };

        public static string GenerateJSArrayFromCountriesList(this HtmlHelper html, string arrayId, bool addScriptTags = true, string[] withProperties = null)
        {
            StringBuilder sb = new StringBuilder(addScriptTags ? "<script type='text/javascript' language='javascript'>" : String.Empty);
            sb.AppendLine();
            if (withProperties == null)
            {
                withProperties = new string[] { "Name" };
            }
            sb.AppendLine();
            sb.AppendFormat("var {0} = {{", arrayId);
            sb.AppendLine();

            var isFirst = true;
            foreach (Country c in Country.GetAll<Country>())
            {
                if (!isFirst)
                {
                    sb.AppendLine(",");
                }

                sb.AppendFormat("\"{0}\" : {{", c.Name);
                
                
                foreach (string propertyName in withProperties)
                {
                    var property = c.GetType().GetProperty(propertyName);
                    if (property == null)
                    {
                        continue;
                    }
                    sb.AppendFormat("\"{0}\":\"{1}\"", property.Name, property.GetValue(c, null));
                    isFirst = false;
                }

                sb.AppendLine("}");
            }
            
            sb.AppendLine("};");

            sb.Append(addScriptTags ? "</script>" : String.Empty);

            return sb.ToString();
        }
    }
}
