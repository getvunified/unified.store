﻿using System;

namespace Jhm.Web.Core
{
    public class PagingInfo
    {

        public PagingInfo()
        {
            CurrentPage = 1;
            TotalResults = 0;
            PageSize = 10;
        }
        public int CurrentPage { get; set; }
        public int TotalResults { get; set; }
        public int PageSize { get; set; }
        public string CurrentActionName { get; set; }
        public string CurrentControllerName { get; set; }




        public int PageCount
        {
            get { return Math.Max(1, (int) Math.Ceiling((double) TotalResults/PageSize)); }
        }

        public int FirstResultIndex
        {
            get { return TotalResults == 0 ? 0 : 1 + PageSize*(CurrentPage - 1); }
        }
        public int LastResultIndex
        {
            get { return Math.Min(TotalResults, PageSize*CurrentPage); }
        }
    }
}
