using System;
using Jhm.Common.Framework.Extensions;

namespace Jhm.Web.Core
{
    public static class RatingHelper
    {
        public static string FirstOfFiveStarWidth(decimal averageRating)
        {
            return GetStarWidth(averageRating, 1, 5);
        }
        public static string SecondOfFiveStarWidth(decimal averageRating)
        {
            return GetStarWidth(averageRating, 2, 5);
        }
        public static string ThirdOfFiveStarWidth(decimal averageRating)
        {
            return GetStarWidth(averageRating, 3, 5);
        }
        public static string FourthOfFiveStarWidth(decimal averageRating)
        {
            return GetStarWidth(averageRating, 4, 5);
        }
        public static string FifthOfFiveStarWidth(decimal averageRating)
        {
            return GetStarWidth(averageRating, 5, 5);
        }


        private static string GetStarWidth(decimal averageRating, int starCalculatingFor, int maxNumberOfStars)
        {
            if (starCalculatingFor.IsNot().GreaterThan(0))
                throw new ArgumentException("StarCalculatingFor must be Greater than ZERO.");

            if (maxNumberOfStars.IsNot().GreaterThan(starCalculatingFor, allowEqualTo: true))
                throw new ArgumentException("MaxNumberOfStars must be Greater or EqualTo the StarCalculatingFor.");

            if (averageRating == 0) return 0.ToString("0");

            int minValue = starCalculatingFor == 1 ? 0 : ((starCalculatingFor - 1) * (100 / maxNumberOfStars));
            int maxValue = starCalculatingFor * (100 / maxNumberOfStars);


            if (averageRating.Is().GreaterThan(maxValue)) return 100.ToString("0");
            if (averageRating.Is().Between(minValue, maxValue, allowEqualTo: true)) return ((CalculateStarRating(maxNumberOfStars) - (starCalculatingFor - 1)) * 100).ToString("0");
            return 0.ToString("0");
        }

        
        public static string GetRatingString(decimal ratingAverage, int numberOfStars = 5)
        {
            return CalculateStarRating(ratingAverage, numberOfStars).ToString("0.00");
        }
        public static decimal CalculateStarRating(decimal ratingAverage, int numberOfStars = 5)
        {
            if (ratingAverage.Is().GreaterThan(0))
                return ratingAverage / (100 / numberOfStars);
            return 0;
        }
    }
}