﻿using System;
using Jhm.Common;
using Jhm.Web.Core.Models.Modules;
using getv.donorstudio.core.Global;

namespace Jhm.Web.Core
{
    public static class FormatHelper
    {
        
        public static string FormatWithClientCulture(this decimal value, Client client, string format)
        {
            return FormatWithClientCulture(client, format, value);
        }

        public static string FormatWithClientCulture(Client client, string format, object value)
        {
            if ( client == null)
            {
                if (String.IsNullOrEmpty(format)) return value.ToString();
                if (value is decimal) return ((decimal)value).ToString(format);
                if (value is DateTime) return ((DateTime)value).ToString(format);
                if (value is int) return ((int)value).ToString(format);
                return String.Format(format, value);
            }
            return FormatWithCompanyCulture(client.Company, format, value);
        }

        public static string FormatWithCompanyCulture(Company company, string format, object value)
        {
            if (value == null)
            {
                return null;
            }
            if (company == null)
            {
                if (String.IsNullOrEmpty(format)) return value.ToString();
                if (value is decimal) return ((decimal)value).ToString(format);
                if (value is DateTime) return ((DateTime)value).ToString(format);
                if (value is int) return ((int)value).ToString(format);
                return String.Format(format, value);
            }

            return company.FormatWithCulture(format, value);
        }

        public static string FormatWithClientCulture(Client client, string format, params object[] args)
        {
            return client == null ? String.Format(format, args) : String.Format(new InternalFormatHelper(client.Company), format, args);
        }

        public static string FormatWithCompanyCulture(Company company, string format, params object[] args)
        {
            return company == null ? String.Format(format, args) : String.Format(new InternalFormatHelper(company), format, args);
        }

        public static string GetCultureName(Client client)
        {
            if (client == null || client.Company == null)
            {
                return System.Globalization.CultureInfo.CurrentCulture.Name;
            }
            return client.Company.Culture.Name;
        }

        public static string GetCurrencySymbol(Client client)
        {
            return client.Company.GetCurrencySymbol();
        }

        public static string ReplaceCurrencyFormat(string str, Client client)
        {
            if (String.IsNullOrEmpty(str) || str.IndexOf('[') < 0 || client == null || client.Company == null)
            {
                return str;
            }
            var start = 0;
            while ((start = str.IndexOf('[')) >= 0)
            {
                var end = str.IndexOf(']', start);
                if (end < start)
                {
                    return str;
                }
                var formatPair = str.Substring(start + 1, end - (start + 1)).Split(new[] { ':' });
                if (formatPair.Length != 2)
                {
                    return str;
                }
                var format = formatPair[0];
                object value = null;
                switch (format)
                {
                    case "C":
                        value = TypeHelper.TryParseDecimal(formatPair[1]);
                        break;
                    default:
                        value = formatPair[1];
                        break;
                }
                var formattedString = client.Company.FormatWithCulture(format, value);
                str = str.Replace(str.Substring(start, (end - start) + 1), formattedString);
            }

            return str;
        }

        private class InternalFormatHelper : IFormatProvider, ICustomFormatter
        {
            public Company Company { get; private set; }

            public InternalFormatHelper(Company company)
            {
                Company = company;
            }

            
            #region Implementation of IFormatProvider

            public object GetFormat(Type formatType)
            {
                return formatType == typeof(ICustomFormatter) ? this : null;
            }

            #endregion

            #region Implementation of ICustomFormatter

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                return String.IsNullOrEmpty(format) ? arg.ToString() : FormatHelper.FormatWithCompanyCulture(Company, format, arg);
            }

            #endregion
        }





        
    }
}
