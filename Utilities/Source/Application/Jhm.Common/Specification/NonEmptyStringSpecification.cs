namespace Jhm.Common
{
    public class NonEmptyStringSpecification : CompositeSpecification<string>
    {
        public override bool IsSatisfiedBy(string item)
        {
            return (!string.IsNullOrEmpty(item)) && item.Trim().Length > 0;
        }
    }
}