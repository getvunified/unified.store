﻿using System;

namespace Jhm.Common
{
    public class GenericSpecification<T> : CompositeSpecification<T>
    {
        private readonly Func<T, bool> func;

        public GenericSpecification(Func<T, bool> func)
        {
            this.func = func;
        }

        public override bool IsSatisfiedBy(T item)
        {
            return func(item);
        }
    }
}