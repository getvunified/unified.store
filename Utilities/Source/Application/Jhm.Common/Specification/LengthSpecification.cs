namespace Jhm.Common
{
    public class LengthSpecification : CompositeSpecification<string>
    {
        private readonly int minLength;
        private readonly int maxLength;

        public LengthSpecification(int maxLength) : this(0, maxLength)
        {
            
        }

        public LengthSpecification(int minLength, int maxLength)
        {
            this.minLength = minLength;
            this.maxLength = maxLength;
        }

        public override bool IsSatisfiedBy(string item)
        {
            return !string.IsNullOrEmpty(item) && item.Length >= minLength && item.Length <= maxLength;
        }
    }
}