using System.Text.RegularExpressions;

namespace Jhm.Common
{
    public class EmailSpecification : CompositeSpecification<string>
    {
        //@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$"
        private static readonly Regex regex = 
            new Regex(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$");

        public override bool IsSatisfiedBy(string item)
        {
            return !string.IsNullOrEmpty(item) && regex.IsMatch(item);
        }
    }
}