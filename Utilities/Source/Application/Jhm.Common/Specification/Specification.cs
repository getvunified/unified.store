namespace Jhm.Common
{
    public static class Specifications
    {
        public static readonly NonEmptyStringSpecification NonEmptyStringSpecification = new NonEmptyStringSpecification();
        public static readonly EmailSpecification EmailSpecification = new EmailSpecification();
        public static readonly RegularExpressionSpecification PostOfficeBoxSpecification =
                new RegularExpressionSpecification(
                    @"\b[P|p]*(OST|ost)*\.*\s*[O|o|0]*(ffice|FFICE)*\.*\s*[B|b][O|o|0][X|x]\b");
    }

    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T item);
    }

    public interface ICompositeSpecification<T> : ISpecification<T>
    {
        ICompositeSpecification<T> And(ISpecification<T> other);
        ICompositeSpecification<T> Or(ISpecification<T> other);
        ICompositeSpecification<T> Not(ISpecification<T> other);
        //... more operators
    }

    public abstract class CompositeSpecification<T> : ICompositeSpecification<T>
    {
        public ICompositeSpecification<T> And(ISpecification<T> other)
        {
            return new AndSpecification<T>(this, other);
        }

        public ICompositeSpecification<T> Or(ISpecification<T> other)
        {
            return new OrSpecification<T>(this, other);
        }

        public ICompositeSpecification<T> Not(ISpecification<T> other)
        {
            return new NotSpecification<T>(this);
        }

        abstract public bool IsSatisfiedBy(T item);
    }

    public class AndSpecification<T> : CompositeSpecification<T>
    {
        private readonly ISpecification<T> rhs;
        private readonly ISpecification<T> lhs;

        public AndSpecification(ISpecification<T> rhs, ISpecification<T> lhs)
        {
            this.rhs = rhs;
            this.lhs = lhs;
        }

        public override bool IsSatisfiedBy(T item)
        {
            return rhs.IsSatisfiedBy(item) &&
            lhs.IsSatisfiedBy(item);
        }
    }

    public class OrSpecification<T> : CompositeSpecification<T>
    {
        private readonly ISpecification<T> rhs;
        private readonly ISpecification<T> lhs;

        public OrSpecification(ISpecification<T> rhs, ISpecification<T> lhs)
        {
            this.rhs = rhs;
            this.lhs = lhs;
        }

        public override bool IsSatisfiedBy(T item)
        {
            return rhs.IsSatisfiedBy(item) ||
                lhs.IsSatisfiedBy(item);
        }
    }

    public class NotSpecification<T> : CompositeSpecification<T>
    {
        private readonly ISpecification<T> spec;

        public NotSpecification(ISpecification<T> spec)
        {
            this.spec = spec;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return !spec.IsSatisfiedBy(candidate);
        }
    }

}