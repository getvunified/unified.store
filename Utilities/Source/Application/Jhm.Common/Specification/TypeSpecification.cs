﻿
namespace Jhm.Common
{
    public class TypeSpecification<ItemToEvaluate, ExpectedType> : CompositeSpecification<ItemToEvaluate>
    {
        public override bool IsSatisfiedBy(ItemToEvaluate item)
        {
            //NOTE Type evaluations will not work with proxies
            //http://www.hibernate.org/hib_docs/nhibernate/html/performance.html#performance-proxies
            //http://blogs.chayachronicles.com/sonofnun/archive/2007/03/30/230.aspx
            return item is ExpectedType;
        }
    }
}