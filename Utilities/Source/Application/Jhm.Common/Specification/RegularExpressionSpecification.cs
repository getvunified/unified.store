using System.Text.RegularExpressions;

namespace Jhm.Common
{
    public class RegularExpressionSpecification : CompositeSpecification<string>
    {
        private Regex regex;

        public RegularExpressionSpecification(string pattern)
        {
            regex = new Regex(pattern);
        }

        public override bool IsSatisfiedBy(string item)
        {
            return item != null && regex.IsMatch(item);
        }
    }
}