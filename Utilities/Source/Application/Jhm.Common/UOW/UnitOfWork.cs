using System;
using Jhm.Common.LocalData;

namespace Jhm.Common.UOW
{
    public interface IUnitOfWorkContainer
    {
        IUnitOfWork CurrentUOW { get; }
        IUnitOfWork Start();
    }

    public class UnitOfWork : IUnitOfWorkContainer
    {
        private const string CurrentUnitOfWorkKey = "UnitOfWork.Current";
        private readonly IUnitOfWorkFactory factory;

        public UnitOfWork(IUnitOfWorkFactory factory)
        {
            this.factory = factory;
        }

        public IUnitOfWork CurrentUOW
        {
            get { return UnitOfWork.Current; }
        }

        public static IUnitOfWork Current
        {
            get
            {
                var uow = (IUnitOfWork)Local.Data[CurrentUnitOfWorkKey];

                if (uow == null)
                {
                    throw new UnitOfWorkException();
                }

                return uow;
            }
        }


        /// <summary>
        /// Creates a unit of work on the current thread otherwise
        /// increments the usage of the current unit of work
        /// </summary>
        /// <returns>unit of work</returns>
        public IUnitOfWork Start()
        {
            var uow = (IUnitOfWork)Local.Data[CurrentUnitOfWorkKey];
            if (uow != null)
            {
                uow.IncrementUsage();
                return uow;
            }

            uow = factory.Create();

            //change the current unit of work
            Local.Data[CurrentUnitOfWorkKey] = uow;

            return uow;
        }


        /// <summary>
        /// Creates a unit of work on the current thread otherwise
        /// increments the usage of the current unit of work
        /// </summary>
        /// <returns>unit of work</returns>
        public static IUnitOfWork Create(IUnitOfWorkFactory factory)
        {
            return new UnitOfWork(factory).Start();
        }

        public static void DisposeCurrent()
        {
            Local.Data[CurrentUnitOfWorkKey] = null;
        }

        public static bool IsStarted
        {
            get { return Local.Data[CurrentUnitOfWorkKey] != null; }
        }
    }

    

    public class UnitOfWorkException : InvalidOperationException
    {
        public UnitOfWorkException()
            : base("There is no unit of work available")
        {

        }
    }
}