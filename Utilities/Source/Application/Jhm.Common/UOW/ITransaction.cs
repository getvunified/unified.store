using System;

namespace Jhm.Common.UOW
{
    public interface ITransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}