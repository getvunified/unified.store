namespace Jhm.Common.UOW
{
    public partial class With
    {
        public delegate void Proc();

        public static void Transaction(params Proc[] procs)
        {
            Transaction(UnitOfWork.Current, procs);
        }

        public static void Transaction(IUnitOfWork uow, params Proc[] procs)
        {
            if (uow == null)
            {
                throw new UnitOfWorkException();
            }

            //Use the existing transaction if one is already started
            if (uow.IsInTransaction)
            {
                foreach (Proc proc in procs)
                {
                    proc();
                }
            }
            else
            {
                using (ITransaction tx = uow.BeginTransaction())
                {
                    try
                    {
                        foreach (Proc proc in procs)
                        {
                            proc();
                        }
                        tx.Commit();
                    }
                    catch
                    {
                        tx.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}