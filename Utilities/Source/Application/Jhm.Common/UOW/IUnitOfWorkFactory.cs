namespace Jhm.Common.UOW
{
    /// <summary>
    /// Responsible for the session setup and teardown. 
    /// Internally manages 
    /// </summary>
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }
}