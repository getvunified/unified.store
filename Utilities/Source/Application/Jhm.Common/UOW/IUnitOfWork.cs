using System;

namespace Jhm.Common.UOW
{
    /// <summary>
    /// Responsible for the session setup and teardown. 
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        void IncrementUsage();

        bool IsInTransaction { get; }
        ITransaction Transaction { get; }
        ITransaction BeginTransaction();
    }
}