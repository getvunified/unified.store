using System;
using System.Xml;
using System.Xml.XPath;

namespace Jhm.Common.Xml.Xsl
{
    public class XslExtension
    {
        public readonly string XslExtensionNamespace = "urn:extension:xslt";

        public string DateNow(string format)
        {
            return DateTime.Now.ToString(format);
        }

        public string FormatDate(object node, string format)
        {
            var dateString = GetValue(node);
            if (!string.IsNullOrEmpty(dateString))
            {
                DateTime dtm;
                return DateTime.TryParse(dateString, out dtm) ? dtm.ToString(format) : string.Empty;
            }
            return string.Empty;
        }

        public string FormatCurrency(object node)
        {
            return FormatNumber(node, "#,##0.00; (#,##0.00)");
        }

        public string FormatNumber(object node, string format)
        {
            var decimalString = GetValue(node);
            if (string.IsNullOrEmpty(decimalString))
            {
                return string.Empty;
            }

            decimal d;
            return decimal.TryParse(GetValue(node), out d) ? d.ToString(format) : string.Empty;
        }

        public string FormatText(object node, string format)
        {
            return string.Format(format, GetValue(node));
        }

        public string GetValue(object node)
        {
            if (node is string) return (string) node;
            if (node is XmlNode) return ((XmlNode) node).Value;
            if (node is XPathNodeIterator) return GetNodeValue((XPathNodeIterator) node);
            return string.Empty;
        }

        private static string GetNodeValue(XPathNodeIterator iterator)
        {
            if (iterator.Count > 0)
            {
                if (iterator.CurrentPosition < 1) iterator.MoveNext();
                return iterator.Current.Value;
            }

            return string.Empty;
        }
    }
}