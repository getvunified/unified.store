using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Jhm.Common.Xml
{
    public static class XmlSerializerHelper
    {
        public static MemoryStream Serialize(object obj)
        {
            var serializer = new XmlSerializer(obj.GetType());

            //Removes the following default namespaces
            // xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
            // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            var xsn = new XmlSerializerNamespaces();
            xsn.Add("", null);

            var ms = new MemoryStream();
            var writer = new XmlTextWriter(ms, Encoding.UTF8) { Formatting = Formatting.Indented };
            serializer.Serialize(writer, obj, xsn);
            ms.Position = 0;

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("Serialized Object: {0}", obj.GetType().FullName));
            DebugUtility.WriteMemoryStreamToConsole(ms);
#endif

            return ms;
        }

        private static MemoryStream Serialize<T>(IList<T> obj)
        {
            var serializer = new XmlSerializer(typeof(List<T>));

            var xsn = new XmlSerializerNamespaces();
            xsn.Add("", null);

            var ms = new MemoryStream();
            var writer = new XmlTextWriter(ms, Encoding.UTF8) { Formatting = Formatting.Indented };
            serializer.Serialize(writer, obj, xsn);
            ms.Position = 0;

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("Serialized Object: {0}", obj.GetType().FullName));
            DebugUtility.WriteMemoryStreamToConsole(ms);
#endif

            return ms;
        }

        public static MemoryStream SerializeToXML(object obj)
        {
            return Serialize(obj);
        }

        public static MemoryStream SerializeToXML<T>(IList<T> obj)
        {
            return Serialize(obj);
        }

        private static T DeserializeFromXML<T>(MemoryStream ms, Type type)
        {
            ms.Position = 0;
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            StreamReader streamReader = new StreamReader(ms);

            var returned = (T)deserializer.Deserialize(streamReader);
            streamReader.Close();

            return returned;

        }

        public static IList<T> DeserializeFromXML<T>(MemoryStream ms)
        {
            ms.Position = 0;
            List<T> returnList;
            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(List<T>));
                StreamReader streamReader;
                streamReader = new StreamReader(ms);
                returnList = (List<T>)deserializer.Deserialize(streamReader);
                streamReader.Close();
            }
            catch (Exception)
            {
                returnList = new List<T>();
                returnList.Add(DeserializeFromXML<T>(ms, typeof(T)));
            }
            
            return returnList;
        }

    }

#if DEBUG

    public static class DebugUtility
    {
        public static void WriteMemoryStreamToConsole(MemoryStream ms)
        {
            System.Diagnostics.Debug.WriteLine(new StreamReader(ms).ReadToEnd());
            ms.Position = 0;
        }
    }

#endif



}

