using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Jhm.Common.Xml
{
    public class XmlResourceResolver : XmlUrlResolver
    {
        private const string UriSchemeRes = "res";
        private readonly Assembly a;

        public XmlResourceResolver(Assembly assembly)
        {
            a = assembly;

#if DEBUG
            foreach (string resourceName in a.GetManifestResourceNames())
            {
                Console.WriteLine("Resource Name: {0}", resourceName);
            }
#endif
        }

        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {
            Stream stream;

            if (absoluteUri.Scheme == UriSchemeRes)
            {
                stream = a.GetManifestResourceStream(absoluteUri.AbsolutePath);
            }
            else
            {
                stream = (Stream)base.GetEntity(absoluteUri, role, typeof(Stream));
            }

            if (ofObjectToReturn == typeof(XmlReader))
            {
                return new XmlTextReader(stream);
            }
            
            return stream;
        }

        public override Uri ResolveUri(Uri baseUri, string relativeUri)
        {
            return relativeUri.StartsWith("res:") ? 
                new Uri(relativeUri) : 
                base.ResolveUri(baseUri, relativeUri);
        }
    }
}