using System;
using System.IO;
using System.Reflection;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace Jhm.Common.Xml.Xsl
{
    public class XslTransformHelper
    {
        public static XslExtension DefaultExtensionObject = new XslExtension();

        public static MemoryStream Transform(MemoryStream xsltStream, object obj)
        {
            return Transform(xsltStream, obj, new XsltArgumentList());
        }

        public static MemoryStream Transform(MemoryStream xsltStream, object obj, XsltArgumentList arguments)
        {
            arguments.AddExtensionObject(DefaultExtensionObject.XslExtensionNamespace, DefaultExtensionObject);

            var transform = GetXslTransform(xsltStream);

            var output = new MemoryStream();

            var objectStream = XmlSerializerHelper.Serialize(obj);

            transform.Transform(GetNavigator(objectStream), arguments, output);


#if DEBUG
            Console.Write(xsltStream);
            DebugUtility.WriteMemoryStreamToConsole(output);
#endif

            return output;
        }



        public static MemoryStream Transform(string stylesheetName, object obj)
        {
            return Transform(Assembly.GetCallingAssembly(), stylesheetName, obj, new XsltArgumentList());
        }

        public static MemoryStream Transform(string stylesheetName, object obj, XsltArgumentList arguments)
        {
            return Transform(Assembly.GetCallingAssembly(), stylesheetName, obj, arguments);
        }

        public static MemoryStream Transform(Assembly resourceAssembly, string stylesheetName, object obj)
        {
            return Transform(resourceAssembly, stylesheetName, obj, new XsltArgumentList());
        }

        public static MemoryStream Transform(Assembly resourceAssembly,
            string stylesheetName, object obj, XsltArgumentList arguments)
        {
            arguments.AddExtensionObject(DefaultExtensionObject.XslExtensionNamespace, DefaultExtensionObject);

            var transform = GetXslTransform(
                stylesheetName, resourceAssembly);

            var output = new MemoryStream();

            var stream = XmlSerializerHelper.Serialize(obj);

            transform.Transform(GetNavigator(stream), arguments, output);

#if DEBUG
            System.Diagnostics.Debug.WriteLine(string.Format("XSLT Transformation: {0}", stylesheetName));
            DebugUtility.WriteMemoryStreamToConsole(output);
#endif

            return output;
        }

        public static XslCompiledTransform GetXslTransform(string stylesheetName, Assembly assembly)
        {
            var transform = new XslCompiledTransform();
            var resolver = new XmlResourceResolver(assembly);
            transform.Load(stylesheetName, XsltSettings.TrustedXslt, resolver);
            return transform;
        }

        private static XslCompiledTransform GetXslTransform(Stream stream)
        {
            var transform = new XslCompiledTransform();
            transform.Load(GetNavigator(stream));
            return transform;
        }

        public static IXPathNavigable GetNavigator(Stream stream)
        {
            return new XPathDocument(stream).CreateNavigator();
        }
    }

    public class XsltArgumentListFluent
    {
        private readonly XsltArgumentList inner;

        public XsltArgumentListFluent() : this(new XsltArgumentList()) { }

        public XsltArgumentListFluent(XsltArgumentList arguments)
        {
            inner = arguments;
        }

        public XsltArgumentListFluent AddParameter(string name, string namespaceUri, object parameter)
        {
            inner.AddParam(name, namespaceUri, parameter);
            return this;
        }

        public XsltArgumentListFluent AddExtensionObject(string namespaceUri, object extension)
        {
            inner.AddExtensionObject(namespaceUri, extension);
            return this;
        }

        public static implicit operator XsltArgumentList(XsltArgumentListFluent fluent)
        {
            return fluent.inner;
        }
    }
}