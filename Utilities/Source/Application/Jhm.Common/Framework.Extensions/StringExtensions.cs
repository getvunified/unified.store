
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace Jhm.Common.Framework.Extensions
{
    public static class StringExtensions
    {
        //http://james.newtonking.com/archive/2008/03/27/formatwith-string-format-extension-method.aspx
        public static string FormatWith(this string format, params object[] args)
        {
            if (format == null) throw new ArgumentNullException("format");

            return string.Format(format, args);
        }

        public static string FormatWith(this string format, IFormatProvider provider, params object[] args)
        {
            if (format == null) throw new ArgumentNullException("format");

            return string.Format(provider, format, args);
        }

        //http://james.newtonking.com/archive/2008/03/29/formatwith-2-0-string-formatting-with-named-variables.aspx
        public static string FormatWith(this string format, object source)
        {
            return FormatWith(format, null, source);
        }

        public static string FormatWith(this string format, IFormatProvider provider, object source)
        {
            if (format == null) throw new ArgumentNullException("format");

            Regex r = new Regex(@"(?<start>\{)+(?<property>[\w\.\[\]]+)(?<format>:[^}]+)?(?<end>\})+",
                          RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

            List<object> values = new List<object>();

            string rewrittenFormat = r.Replace(format, delegate(Match m)
            {
                Group startGroup = m.Groups["start"];
                Group propertyGroup = m.Groups["property"];
                Group formatGroup = m.Groups["format"];
                Group endGroup = m.Groups["end"];
                values.Add((propertyGroup.Value == "0") ? source : DataBinder.Eval(source, propertyGroup.Value));
                return new string('{', startGroup.Captures.Count) + (values.Count - 1) + formatGroup.Value + new string('}', endGroup.Captures.Count);
            });
            return string.Format(provider, rewrittenFormat, values.ToArray());
        }

        public static string Decrypt(this string s)
        {
            return StringEncryptor.DecryptString(s);
        }

        public static string Encrypt(this string s)
        {
            return StringEncryptor.EncryptString(s);
        }

        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static string ToStringJoin(this IEnumerable<object > objs, string separator)
        {
            return string.Join(separator, objs);
        }

        public static string TruncateAt(this string s, int truncateAtLength, string returnStringSuffix = null)
        {
            if (returnStringSuffix.Is().NullOrEmpty())
                returnStringSuffix = string.Empty;

            if (s.IsNot().NullOrEmpty() && s.Length.Is().GreaterThan(truncateAtLength))
                return s.Substring(0, truncateAtLength) + returnStringSuffix;

            return s;
        }

        public static string TruncateToClosestPuntuationMarkAt(this string s, int truncateAtLength, string returnStringSuffix = null)
        {
            if(returnStringSuffix.Is().NullOrEmpty())
                returnStringSuffix = string.Empty;

            if (s.IsNot().NullOrEmpty() && s.Length.Is().GreaterThan(truncateAtLength))
                return TruncateToClosestPuntuationMark(s, truncateAtLength) + returnStringSuffix;

            return s;
        }

        private static string TruncateToClosestPuntuationMark(string s, int truncateAtLength)
        {
            var closestIndex = ClosestIndexToPositionOfAnyOf(s,truncateAtLength,".,?!".ToCharArray());
            return s.Substring(0, closestIndex);
        }

        private static int ClosestIndexToPositionOfAnyOf(string str, int position, IEnumerable<char> charsToSearch)
        {
            var diference = Int32.MaxValue;
            var closestPosition = position;
            foreach (var c in charsToSearch)
            {
                var closestBefore = str.Substring(0, position).LastIndexOf(c);
                var closestAfter = str.IndexOf(c, position);
                if ( closestBefore < 0 && closestAfter < 0)
                {
                    continue;
                }
                var closestBeforeDif = position - closestBefore;
                var closestAfterDif = closestAfter - position;
                var diferenceAux = closestBeforeDif < closestAfterDif ? -closestBeforeDif : closestAfterDif;
                if (Math.Abs(diferenceAux) >= diference)
                {
                    continue;
                }
                closestPosition = diferenceAux < 0 ? closestBefore : closestAfter;
                diference = Math.Abs(diferenceAux);
            }
            return closestPosition;
        }


        public static string ToTitleCase(this string s)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
        }
    }
   
}