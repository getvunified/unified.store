using System;
using System.Collections.Generic;

namespace Jhm.Common.Framework.Extensions
{
    public interface ISpecificationExtension<T>
    {
        T Object { get; }
        bool Evaluate(bool testedValue);
    }

        public class Is<T> : ISpecificationExtension<T>
        {
            private readonly T obj;

            public Is(T obj)
            {
                this.obj = obj;
            }

            public bool Evaluate(bool testedValue)
            {
                return testedValue;
            }

            public T Object { get { return obj; } }

            
        }

    public class IsNot<T> : ISpecificationExtension<T>
    {
        private readonly T obj;

        public IsNot(T obj)
        {
            this.obj = obj;
        }

        public bool Evaluate(bool testedValue)
        {
            return !testedValue;
        }

        public T Object { get { return obj; } }
    }

    
    public static class IsExtensions
    {
        public static Is<T> Is<T>(this T obj)
        {
            return new Is<T>(obj);
        }

        public static IsNot<T> IsNot<T>(this T obj)
        {
            return new IsNot<T>(obj);
        }

        #region Generic ISpecificationExtensions
        public static bool Null<T>(this ISpecificationExtension<T> obj)
        {
            return obj.Evaluate(obj.Object.IsNull());
        }

        public static bool Between<T>(this ISpecificationExtension<T> obj, T low, T high, bool allowEqualTo = false)
        where T : IComparable<T>
        {
            return allowEqualTo ? obj.Evaluate(obj.Object.CompareTo(low) >= 0 && obj.Object.CompareTo(high) <= 0) :
                                  obj.Evaluate(obj.Object.CompareTo(low) > 0 && obj.Object.CompareTo(high) < 0);
        }


        public static bool LessThan<T>(this ISpecificationExtension<T> obj, T value, bool allowEqualTo = false)
            where T : IComparable<T>
        {
            return allowEqualTo ? obj.Evaluate(obj.Object.CompareTo(value) <= 0) : 
                                  obj.Evaluate(obj.Object.CompareTo(value) < 0);
        }

        public static bool GreaterThan<T>(this ISpecificationExtension<T> obj, T value, bool allowEqualTo = false)
            where T : IComparable<T>
        {
            return allowEqualTo ? obj.Evaluate(obj.Object.CompareTo(value) >= 0) : 
                                  obj.Evaluate(obj.Object.CompareTo(value) > 0);
        }

        #endregion


        #region String ISpecificationExtensions

        public static bool NullOrEmpty(this ISpecificationExtension<string> obj)
        {
            return obj.Evaluate(obj.Object.IsNullOrEmpty());
        }

        public static bool LengthGreaterThan(this ISpecificationExtension<string> obj, int value)
        {
            return obj.Evaluate(obj.Object.Length > value);
        }

        public static bool LengthGreaterThanOrEqualTo(this ISpecificationExtension<string> obj, int value)
        {
            return obj.Evaluate(obj.Object.Length >= value);
        }

        public static bool LengthLessThan(this ISpecificationExtension<string> obj, int value)
        {
            return obj.Evaluate(obj.Object.Length < value);
        }

        public static bool LengthLessThanOrEqualTo(this ISpecificationExtension<string> obj, int value)
        {
            return obj.Evaluate(obj.Object.Length <= value);
        }

        public static bool StartingWith(this ISpecificationExtension<string> obj, string value)
        {
            return obj.Evaluate(obj.Object.StartsWith(value));
        }

        public static bool EndingWith(this ISpecificationExtension<string> obj, string value)
        {
            return obj.Evaluate(obj.Object.EndsWith(value));
        }
        #endregion


        public static bool Empty<T>(this ISpecificationExtension<T> obj)
            where T : IEnumerable<object>
        {
            return obj.Evaluate(obj.Object.ToLIST().Count == 0);
        }
       
    }
}