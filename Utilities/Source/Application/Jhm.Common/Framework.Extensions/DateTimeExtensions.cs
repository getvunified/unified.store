using System;

namespace Jhm.Common.Framework.Extensions
{
    public static class DateTimeExtensions
    {
        public static TimeSpan MilliSeconds(this int i)
        {
            return new TimeSpan(0, 0, 0, 0, i);
        }

        public static TimeSpan Seconds(this int i)
        {
            return new TimeSpan(0, 0, 0, i, 0);
        }

        public static TimeSpan Minutes(this int i)
        {
            return new TimeSpan(0, 0, i, 0, 0);
        }

        public static TimeSpan Hours(this int i)
        {
            return new TimeSpan(0, i, 0, 0, 0);
        }

        public static TimeSpan Days(this int i)
        {
            return new TimeSpan(i, 0, 0, 0, 0);
        }

        public static DateTime Ago(this TimeSpan ts)
        {
            return DateTime.Now.Subtract(ts);
        }

        public static DateTime MonthsAgo(this int i)
        {
            return DateTime.Now.AddMonths(-i);
        }

        public static DateTime YearsAgo(this int i)
        {
            return DateTime.Now.AddYears(-i);
        }

        public static DateTime InTheFuture(this TimeSpan ts)
        {
            return DateTime.Now.Add(ts);
        }

        public static DateTime MonthsInTheFuture(this int i)
        {
            return DateTime.Now.AddMonths(i);
        }

        public static DateTime YearsInTheFuture(this int i)
        {
            return DateTime.Now.AddYears(i);
        }
        public static DateTime TruncateToSeconds(this DateTime source)
        {
            return new DateTime(source.Ticks - (source.Ticks % TimeSpan.TicksPerSecond), source.Kind);
        }
    }
}