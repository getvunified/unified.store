using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using Foundation;


namespace Jhm.Common.Framework.Extensions
{
    public static class MVCExtensions
    {
        public static T ModelAs<T>(this ViewDataDictionary viewData) where T : class
        {
            try
            {
                return viewData.Model as T;
            }
            catch
            {
                return null;
            }
        }

        public static T FromPayload<T>(this ViewDataDictionary viewDataDictionary, IPayload payload)
        {
            return viewDataDictionary[payload.Key].TryConvertTo<T>();
        }

        public static void AddObjectWithPayload(this ViewDataDictionary viewDataDictionary, IPayload payload, object obj)
        {
            viewDataDictionary[payload.Key] = obj;
        }

        public static IEnumerable<SelectListItem> ToMVCSelectList<T>(this IEnumerable<ISelectListItem<T>> lookups)
        {
            return new SelectList(lookups, "value", "displayName");
        }

        public static IEnumerable<SelectListItem> ToMVCSelectList<T>(this IEnumerable<ISelectListItem<T>> lookups, string valueFieldName, string textFieldName)
        {
            return new SelectList(lookups, valueFieldName, textFieldName);
        }

        public static IEnumerable<SelectListItem> ToMVCSelectList<T>(this IEnumerable<ISelectListItem<T>> lookups, string valueFieldName, string textFieldName, object selectedValue)
        {
            return new SelectList(lookups, valueFieldName, textFieldName, selectedValue);
        }

        public static IEnumerable<SelectListItem> AddNoSelection(this IEnumerable<SelectListItem> selectListItems)
        {
            var rSelectListItemList = new List<SelectListItem>();
            rSelectListItemList.AddUnique(new SelectListItem() { Selected = false, Text = Lookup.NoSelection.Text, Value = Lookup.NoSelection.Value });

            foreach (var l in selectListItems)
            {
                rSelectListItemList.AddUnique(new SelectListItem() { Selected = l.Selected, Text = l.Text, Value = l.Value });
            }
            return rSelectListItemList;
        }

        public static IEnumerable<SelectListItem> SetSelected(this IEnumerable<SelectListItem> selectedListItems, params string[] selectedValues)
        {
            foreach (var selectedListItem in
                selectedValues.SelectMany(value => selectedListItems.Where(x => x.Value == value)))
            {
                selectedListItem.Selected = true;
            }
            return selectedListItems;
        }

        

        public static string RenderPartialDisplayByTypeName(this HtmlHelper htmlHelper, object model, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null)
        {
            return htmlHelper.RenderPartialByTypeName(model, model.GetType(), "Display", partialViewLocation, modelSuffix, htmlFormat);
        }

        public static string RenderPartialDisplayByTypeName(this HtmlHelper htmlHelper, object model, Type controlNameType, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null)
        {
            return htmlHelper.RenderPartialByTypeName(model, controlNameType, "Display", partialViewLocation, modelSuffix, htmlFormat);
        }

        public static string RenderPartialEditByTypeName(this HtmlHelper htmlHelper, object model, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null, bool useRazorEngine = false)
        {
            return htmlHelper.RenderPartialByTypeName(model, model.GetType(), "Edit", partialViewLocation, modelSuffix, htmlFormat, useRazorEngine);
        }

        public static string RenderPartialEditByTypeName(this HtmlHelper htmlHelper, object model, Type controlNameType, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null)
        {
            return htmlHelper.RenderPartialByTypeName(model, controlNameType, "Edit", partialViewLocation, modelSuffix, htmlFormat);
        }


        public static string RenderPartialByTypeName(this HtmlHelper htmlHelper, object model, string controlNameWithoutAscx, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null)
        {
            return RenderPartialByTypeName(htmlHelper, model, model.GetType(), controlNameWithoutAscx, partialViewLocation, modelSuffix, htmlFormat);
        }

        public static string RenderPartialByTypeName(this HtmlHelper htmlHelper, object model, Type controlNameType, string controlNameWithoutAscx, string partialViewLocation = null, string modelSuffix = null, string htmlFormat = null, bool useRazorEngine = false)
        {
            partialViewLocation = (partialViewLocation.IsNullOrEmpty()) ? string.Empty : partialViewLocation;
            modelSuffix = (modelSuffix.IsNullOrEmpty()) ? string.Empty : modelSuffix;
            var typeName = controlNameType.Name;

            var fileExtension = useRazorEngine ? "cshtml" : "ascx";
            var partialViewName = "{0}{1}{2}/{3}.{4}".FormatWith(partialViewLocation, typeName, modelSuffix, controlNameWithoutAscx, fileExtension);

            return RenderPartial(htmlHelper, model, partialViewName, htmlFormat, useRazorEngine);
        }

        public static string RenderPartial(this HtmlHelper htmlHelper, object model, string partialViewName, string htmlFormat = null, bool useRazorEngine = false)
        {
            if (model.Is().Null())
                throw new ArgumentNullException("model");

            htmlFormat = (htmlFormat.IsNullOrEmpty()) ? "{0}" : htmlFormat;

            var viewDataDictionary = new ViewDataDictionary(model);

            foreach (var keyValuePair in htmlHelper.ViewData.ModelState)
            {
                viewDataDictionary.ModelState.Add(keyValuePair.Key, keyValuePair.Value);
            }

            foreach (var keyValuePair in htmlHelper.ViewData)
            {
                viewDataDictionary.Add(keyValuePair.Key, keyValuePair.Value);
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            var x = new ViewPage { ViewData = viewDataDictionary, Url = urlHelper, ViewContext = htmlHelper.ViewContext };

            if (useRazorEngine)
            {
                return RenderPartialViewToString(htmlHelper, viewDataDictionary, partialViewName, model);
            }
            else
            {
                Control control = x.LoadControl(partialViewName);
                x.Controls.Add(control);

                var sb = new StringBuilder();

                using (var sw = new StringWriter(sb))
                {
                    using (var tw = new HtmlTextWriter(sw))
                    {
                        x.RenderControl(tw);
                        return htmlFormat.FormatWith(sb);
                    }
                } 
            }                       
        }

        private static string RenderPartialViewToString(HtmlHelper htmlHelper, ViewDataDictionary viewData, string viewName, object model)
        {
            var controllerContext = htmlHelper.ViewContext.Controller.ControllerContext;
            var tempData = htmlHelper.ViewContext.TempData;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}