
namespace Jhm.Common.Framework.Extensions
{
    public static class MapperExtensions
    {
        public interface IMapperExtension<T>
        {
            T Object { get; }
            
        }

        public class Mapper<T> : IMapperExtension<T> where T : class
        {
            private readonly T obj;

            public Mapper(T obj)
            {
                this.obj = obj;
            }

            public T Object { get { return obj; } }
        }

        public static Mapper<T> Map<T>(this T obj) where T : class 
        {
            return new Mapper<T>(obj);
        }
    }
}