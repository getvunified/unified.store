
using System.Collections.Generic;
using System.Linq;

namespace Jhm.Common.Framework.Extensions
{
    public static class MiscExtensions
    {
        /// <summary>
        /// Inserts object into list ONLY IF it doesn't already exist in the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="o"></param>
        public static void AddUnique<T>(this IList<T> list, T o)
        {
            if (list.Is().Null())
                list = list.ToLIST();

            if (!list.Contains(o))
                list.Add(o);
        }

        public static List<T> ToLIST<T>(this IEnumerable<T> obj)
        {
            return new List<T>(obj ?? new List<T>());
        }


        public static bool IsNull(this object o)
        {
            return o == null;
        }

        public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }

    }
}