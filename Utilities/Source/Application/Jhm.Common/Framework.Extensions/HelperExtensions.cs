using System;
using System.Web.Script.Serialization;

namespace Jhm.Common.Framework.Extensions
{
    public static class HelperExtensions
    {
        public static T ConvertTo<T>(this object value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static T TryConvertTo<T>(this object value)
        {
            if ( value == null )
            {
                return default(T);
            }
            try
            {
                if ((typeof(T)) == typeof(Guid))
                    return (T) Convert.ChangeType(value.ToString().TryConvertToGuid(), typeof(T));

                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        private static Guid TryConvertToGuid(this string value)
        {
            try
            {
                return new Guid(value);
            }
            catch (Exception)
            {
                return new Guid();
            }
        }


        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }

        public static T FromJSON<T>(this string obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(obj);
        }

        public static T ParseEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T TryParseEnum<T>(this string value, T defaultValue)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception)
            {

                return defaultValue;
            }

        }
    }
}