namespace Jhm.Common.Repositories
{
    /// <summary>
    /// Repositories are services for providing data access to objects. 
    /// Repositories provide CRUD (create/read/update/delete) for domain entities.
    /// </summary>
    public interface IRepository<T> : IService where T : class, IEntity
    {
        T Get(object id);
        void Save(T entity);
        void Delete(T entity);
      
        //void DeleteAll(IQuery<T> query);

        //T FindOne(IQuery<T> query);
        //IList<T> FindAll(IQuery<T> query);

    }
}