using System;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class Log4NetLog : ILog
    {
        private readonly log4net.ILog log;

        public Log4NetLog(log4net.ILog log)
        {
            this.log = log;
        }

        public void Informational(string message)
        {
            log.Info(message);
        }

        public void Informational(string messageFormat, params object[] args)
        {
            log.InfoFormat(messageFormat, args);
        }

        public void Erroneous(string message)
        {
            log.Error(message);
        }

        public void Erroneous(string message, Exception exception)
        {
            log.Error(message, exception);
        }

        public void Erroneous(Exception exception)
        {
            LogException(exception);
        }

        public void Erroneous(string messageFormat, params object[] args)
        {
            log.ErrorFormat(messageFormat, args);
        }

        private void LogException(Exception ex)
        {
            log.ErrorFormat("{0} {1}", ex.Message, ex.StackTrace);
            Exception inner = ex.InnerException;
            if (inner != null)
            {
                LogException(inner);
            }
        }
    }
}