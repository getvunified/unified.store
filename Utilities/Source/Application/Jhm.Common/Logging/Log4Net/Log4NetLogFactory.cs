using System;
using log4net;
using log4net.Config;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class Log4NetLogFactory : ILogFactory
    {
        public Log4NetLogFactory()
        {
            XmlConfigurator.Configure();
        }

        public ILog CreateFor(Type typeThatRequiresLoggingServices)
        {
            return new Log4NetLog(LogManager.GetLogger(typeThatRequiresLoggingServices));
        }
    }
}