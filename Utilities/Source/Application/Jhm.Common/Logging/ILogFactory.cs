using System;
using Jhm.Common;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public interface ILogFactory
    {
        ILog CreateFor(Type type);
    }
}