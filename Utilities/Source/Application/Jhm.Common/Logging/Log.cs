
using System;
using Jhm.Common;
using Jhm.DependencyInjection;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public class Log
    {
        public static ILog For(object itemThatRequiresLoggingServices)
        {
            return For(itemThatRequiresLoggingServices.GetType());
        }

        public static ILog For(Type type)
        {
            return DependencyResolver.GetInstance<ILogFactory>().CreateFor(type);
        }

        public static ILog For<T>()
        {
            return For(typeof(T));
        }
    }
}