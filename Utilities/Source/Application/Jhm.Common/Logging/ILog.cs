using System;

namespace Jhm.Web.UI.IBootStrapperTasks
{
    public interface ILog
    {
        void Informational(string message);
        void Informational(string messageFormat, params object[] args);
        
        void Erroneous(string message);
        void Erroneous(string message, Exception exception);
        void Erroneous(Exception exception);
        void Erroneous(string messageFormat, params object[] args);
    }
}