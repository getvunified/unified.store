using System;

namespace Jhm.Common.BootStrapper
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class BootstrapperPriorityAttribute : Attribute
    {
        private int priority = 0;

        public BootstrapperPriorityAttribute() { }

        public BootstrapperPriorityAttribute(int priority)
        {
            this.priority = priority;
        }

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }
    }

}