using System;

namespace Jhm.Common.BootStrapper
{
    public abstract class Bootstrapper
    {
        internal Action Run;
        protected Bootstrapper()
        {
            Init();
            Run = SetupRunAction();
        }

        public abstract void Init();

        public abstract Action SetupRunAction();

        public static void Bootstrap<T>() where T : Bootstrapper, new()
        {
            var b = new T();
            if (b.Run != null)
                b.Run();
        }
    }
}
