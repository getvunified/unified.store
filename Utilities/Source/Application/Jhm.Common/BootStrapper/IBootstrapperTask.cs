namespace Jhm.Common.BootStrapper
{
    public interface IBootstrapperTask
    {
        void Execute();
    }
}