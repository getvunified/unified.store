using System;

namespace Jhm.Common
{
    public class Message : IScreenMessage
    {
        public static readonly IScreenMessage Empty = new Message(String.Empty, String.Empty);
        private readonly string text;
        private readonly string title;

        public Message(string text, string title)
        {
            this.text = text;
            this.title = title;
        }

        public Message(Exception ex)
            : this(ex.Message)
        {
        }

        public Message(string text)
            : this(text, String.Empty)
        {

        }

        public string Title
        {
            get { return title; }
        }

        public string Text
        {
            get { return text; }
        }
    }
}