using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Jhm.Common
{
    public class ScreenMessageSet : IScreenMessageSet
    {
        public static readonly ScreenMessageSet Empty = new ScreenMessageSet();

        private List<IScreenMessage> messages;

        public ScreenMessageSet()
        {
            messages = new List<IScreenMessage>();
        }

        public ScreenMessageSet(params IScreenMessage[] screenMessages)
        {
            this.messages = new List<IScreenMessage>(screenMessages);
        }

        public ScreenMessageSet(BrokenRulesException ex)
            : this(ex.BrokenRuleSet)
        {

        }

        public ScreenMessageSet(Exception ex)
        {
            this.messages = new List<IScreenMessage>(new IScreenMessage[] { new Message(ex) });
        }

        public ScreenMessageSet(IBrokenRuleSet brokenRuleSet)
        {
            messages = new List<IScreenMessage>();
            foreach (string message in brokenRuleSet.Messages)
            {
                messages.Add(new Message(message));
            }
        }

        public ScreenMessageSet(params string[] messages)
        {
            this.messages = new List<IScreenMessage>();
            foreach (string message in messages)
            {
                this.messages.Add(new Message(message));
            }
        }

        public IEnumerable<IScreenMessage> Messages
        {
            get
            {
                return messages;
            }
        }

        public int Count
        {
            get { return messages.Count; }
        }

        public bool IsEmpty
        {
            get { return Count.Equals(0); }
        }

        public string ToString(string format)
        {
            StringBuilder sb = new StringBuilder();
            foreach (IScreenMessage message in messages)
            {
                sb.Append(string.Format(format, message.Text));
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return ToString("{0}\n");
        }

        public IScreenMessage this[int index]
        {
            get { return messages[index]; }
        }

        IEnumerator<IScreenMessage> IEnumerable<IScreenMessage>.GetEnumerator()
        {
            return messages.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return messages.GetEnumerator();
        }
    }
}