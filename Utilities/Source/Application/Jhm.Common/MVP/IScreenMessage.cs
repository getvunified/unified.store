using System.Collections.Generic;

namespace Jhm.Common
{
    public interface IScreenMessage
    {
        string Title { get; }
        string Text { get; }
    }

    public interface IScreenMessageSet : IEnumerable<IScreenMessage>
    {
        IEnumerable<IScreenMessage> Messages { get; }
        int Count { get; }
        bool IsEmpty { get; }
        string ToString();
        string ToString(string format);
        IScreenMessage this[int index] { get; }
    }
}