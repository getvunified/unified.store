using System;
using Jhm.Common.UOW;

namespace Jhm.Common
{
    public delegate void ViewEvent();
    public delegate void ViewEvent<P>(P param);

    public abstract class PresenterBase<V, S> : IPresenter
        where V : IView
        where S : IService
    {
        protected readonly S service;
        protected readonly V windowView;
        private readonly UnitOfWork unitOfWork;

        protected PresenterBase(V windowView, S service, UnitOfWork unitOfWork)
        {
            this.windowView = windowView;
            this.service = service;
            this.unitOfWork = unitOfWork;
        }

        public IView WindowView
        {
            get { return windowView; }
        }

        public void TryOnEvent<T>(ViewEvent<T> method, T item)
        {
            TryOnEvent(() => method(item));
        }


        public void TryOnEvent(ViewEvent method)
        {
            try
            {
                using (unitOfWork.Start())
                {
                    method();
                }
            }
            catch (BrokenRulesException ex)
            {
                windowView.ErrorMessages = new ScreenMessageSet(ex);
            }
            catch (Exception ex)
            {
                windowView.ErrorMessages = new ScreenMessageSet(ex);
                try
                {
                    //Log.For(this).Erroneous("{0} {1}", ex.Message, ex.StackTrace);
                }
                catch
                {
                }
            }
        }


    }
}
