
namespace Jhm.Common
{
        //Callbacks and event handlers to the Presenter should have no arguments
        //Presenter should go back to the View and/or the Model to get state
        //Presenter is responsible for the behavior and telling the view what piece of information to put into each UI control
        //The exception cases: send over just enough information in the handler for the Presenter to know which data member was the subject of the event
        public interface IView
        {
            IScreenMessageSet ErrorMessages { set; }
        }

        public interface IWebView : IView
        {
            void RedirectWithErrorMessageAndDelay(IScreenMessageSet errorMessages, string page, int secBeforeRedirect);
            void Redirect(string page);
            void RedirectWithDelay(int secBeforeRedirect, string page);
            void RedirectParent(string page);
            void ChangePageControlStatus(bool status);
            void RegisterAndThrowPopUpWindow(int width, int height, string url);
        }


}