using System;
using System.Collections.Generic;
using System.Text;

namespace Jhm.Common
{
    public static class Collection
    {
        public static void ForEach<T>(IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
        }

        /// <summary>
        ///  Returns null if no match is found
        /// </summary>
        public static T Get<T>(IEnumerable<T> list, Predicate<T> match)
            where T : class
        {
            foreach (var item in list)
            {
                if (match(item))
                {
                    return item;
                }
            }
            return null;
        }

        public static T Get<T>(IEnumerable<T> list, ISpecification<T> spec)
            where T : class
        {
            foreach (var item in list)
            {
                if (spec.IsSatisfiedBy(item))
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        ///  Throws and exception if not found
        /// </summary>
        public static T Find<T>(IEnumerable<T> list, Predicate<T> match)
        {
            foreach (var item in list)
            {
                if (match(item))
                {
                    return item;
                }
            }
            throw new NullReferenceException(string.Format("{0} not found", typeof(T).Name));
        }

        public static T Find<T>(IEnumerable<T> list, ISpecification<T> spec)
        {
            foreach (var item in list)
            {
                if (spec.IsSatisfiedBy(item))
                {
                    return item;
                }
            }
            throw new NullReferenceException(string.Format("{0} not found", typeof(T).Name));
        }

        public static IEnumerable<T> FindAll<T>(IEnumerable<T> list, ISpecification<T> spec)
        {
            foreach (var item in list)
            {
                if (spec.IsSatisfiedBy(item))
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<T> FindAll<T>(IEnumerable<T> list, Predicate<T> match)
        {
            foreach (var item in list)
            {
                if (match(item))
                 {
                     yield return item;
                 }
            }
        }

        public static bool Exists<T>(IEnumerable<T> list, Predicate<T> match)
        {
            foreach (var item in list)
            {
                if (match(item))
                {
                    return true;
                }
            }

            return false;
        }

        public static T[] ToArray<T>(IEnumerable<T> collection)
        {
            using(var iterator = collection.GetEnumerator())
            {
                return ToArray(iterator);
            }
        }

        public static T[] ToArray<T>(IEnumerator<T> iterator)
        {
            var list = new List<T>();
            while (iterator.MoveNext())
            {
                list.Add(iterator.Current);
            }
            return list.ToArray();
        }

        public static U[] ToArray<T, U>(IEnumerable<T> collection, Converter<T, U> func)
        {
            using (var iterator = collection.GetEnumerator())
            {
                return ToArray(iterator, func);
            }
        }

        public static U[] ToArray<T, U>(IEnumerator<T> iterator, Converter<T, U> func)
        {
            var list = new List<U>();
            while (iterator.MoveNext())
            {
                list.Add(func(iterator.Current));
            }
            return list.ToArray();
        }

        public static IEnumerable<U> ConvertAll<T, U>(IEnumerable<T> list, Converter<T, U> func)
        {
            foreach (var t in list)
            {
                yield return func(t);
            }
        }

        public static List<T> Sort<T>(IEnumerable<T> listToSort, Comparison<T> comparison)
        {
            return Sort(new List<T>(listToSort), comparison);
        }

        public static List<T> Sort<T>(List<T> listToSort, Comparison<T> comparison)
        {
            listToSort.Sort(comparison);
            return listToSort;
        }

        public static string ToString<T>(IEnumerable<T> items, string separator, Func<T, string> func)
        {
            var sb = new StringBuilder();
            foreach(var item in items)
            {
                sb.Append(func(item) + separator);
            }
            return sb.ToString();
        }
    }
}