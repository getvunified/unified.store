
namespace Jhm.Common
{
    public interface IEntity
    {
        object Id { get; }
        bool IsTransient();
    }

    public interface IEntity<T>
    {
        T Id { get; }
    }
}