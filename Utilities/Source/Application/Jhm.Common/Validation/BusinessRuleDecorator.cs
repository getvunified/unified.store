namespace Jhm.Common
{
    public class BusinessRuleDecorator<T> : IBusinessRule<T>
    {
        private readonly IBusinessRule<T> inner;

        public BusinessRuleDecorator(IBusinessRule<T> inner)
        {
            this.inner = inner;
        }

        public virtual string Name
        {
            get { return inner.Name; }
        }

        public virtual string Description
        {
            get { return inner.Name; }
        }

        public virtual bool IsSatisfiedBy(T item)
        {
            return inner.IsSatisfiedBy(item);
        }

        public virtual ICompositeSpecification<T> And(ISpecification<T> other)
        {
            return inner.And(other);
        }

        public ICompositeSpecification<T> Or(ISpecification<T> other)
        {
            return inner.Or(other);
        }

        public ICompositeSpecification<T> Not(ISpecification<T> other)
        {
            return inner.Not(other);
        }
    }
}