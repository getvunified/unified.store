using System.Collections.Generic;

namespace Jhm.Common
{
    public class BusinessRuleSet<T> : IBusinessRuleSet<T> 
    {
        private readonly IList<IBusinessRule<T>> rules;

        public BusinessRuleSet(params IBusinessRule<T>[] rules) : this(new List<IBusinessRule<T>>(rules))
        {

        }

        public BusinessRuleSet(IList<IBusinessRule<T>> rules)
        {
            this.rules = rules;
        }

        public IBrokenRuleSet BrokenBy(T item)
        {
            var brokenRules = new List<IRule>();
            foreach (var rule in rules)
            {
                if (!rule.IsSatisfiedBy(item))
                {
                    brokenRules.Add(rule);
                }
            }

            return new BrokenRuleSet(brokenRules);
        }
    }
}