using System;

namespace Jhm.Common
{
    public class BusinessRule<T> : CompositeSpecification<T>, IBusinessRule<T>
    {        
        private readonly Predicate<T> matchPredicate;
        private readonly string name;
        private readonly string description;

        public BusinessRule(string name, string description, Predicate<T> matchPredicate)
        {
            this.name = name;
            this.description = description;
            this.matchPredicate = matchPredicate;
        }        

        public override bool IsSatisfiedBy(T item)
        {
            return matchPredicate(item);
        }

        public string Name
        {
            get { return name; }
        }

        public string Description
        {
            get { return description; }
        }

        public override bool Equals(object obj)
        {
            IBusinessRule<T> other = obj as IBusinessRule<T>;
            return (other == null ? true : this.Name.Equals(other.Name));                        
        }

        public override int GetHashCode()
        {
            return matchPredicate.GetHashCode() + 29 * name.GetHashCode();
        }
    }
}