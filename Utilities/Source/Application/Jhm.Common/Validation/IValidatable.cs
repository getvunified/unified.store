namespace Jhm.Common
{
    public interface IValidatable
    {
        IBrokenRuleSet Validate();
    }
}