namespace Jhm.Common
{
    public interface IRule
    {
        string Name { get;}
        string Description { get; }
    }

    public interface IBusinessRule<T> : IRule, ICompositeSpecification<T>
    {
    }
}