﻿namespace Jhm.Common
{
    public class DefaultValidator<T> : IValidator<T>
        where T : IValidatable
    {
        public IBrokenRuleSet Validate(T entity)
        {
            return entity.Validate();
        }

        public static IBrokenRuleSet Validate(T entity, BusinessRuleSet<T> ruleSet)
        {
            return new BrokenRuleSet(ruleSet.BrokenBy(entity));
        }
    }
}