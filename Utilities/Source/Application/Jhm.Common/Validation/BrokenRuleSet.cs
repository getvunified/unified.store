using System.Collections.Generic;
using System.Text;

namespace Jhm.Common
{
    public class BrokenRuleSet : IBrokenRuleSet
    {
        public static readonly BrokenRuleSet Empty = new BrokenRuleSet();

        private IList<IRule> rules;

        public BrokenRuleSet() 
        {
            rules = new List<IRule>(); 
        }

        //aggregate the rules
        public BrokenRuleSet(params IBrokenRuleSet[] brokenRuleSets)
        {
            rules = new List<IRule>();
            foreach (IBrokenRuleSet brokenRuleSet in brokenRuleSets)
            {
                ((List<IRule>)rules).AddRange(brokenRuleSet.Rules);
            }
        }

        public BrokenRuleSet(params IRule[] rules)
        {
            this.rules = new List<IRule>(rules);
        }

        public BrokenRuleSet(IList<IRule> rules)
        {
            this.rules = rules;
        }

        public IEnumerable<IRule> Rules
        {
            get { return rules; }
        }

        public IEnumerable<string> Messages
        {
            get
            {
                foreach (IRule rule in rules)
                {
                    yield return rule.Description;
                }
            }
        }

        public bool Contains(IRule rule)
        {
            return rules.Contains(rule);
        }

        public int Count
        {
            get { return rules.Count; }
        }

        public bool IsEmpty
        {
            get { return rules.Count.Equals(0); }
        }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (string str in Messages)
            {
                builder.AppendLine(str);
            }
            return builder.ToString();
        }
    }
}