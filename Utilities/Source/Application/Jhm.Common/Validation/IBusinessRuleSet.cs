using System.Collections.Generic;

namespace Jhm.Common
{
    public interface IBusinessRuleSet<T>
    {
        IBrokenRuleSet BrokenBy(T item);    
    }

    public interface IBrokenRuleSet
    {
        IEnumerable<IRule> Rules { get; }
        IEnumerable<string> Messages { get; }
        bool Contains(IRule rule);
        int Count { get; }
        bool IsEmpty { get; }
        string ToString();
    }
}