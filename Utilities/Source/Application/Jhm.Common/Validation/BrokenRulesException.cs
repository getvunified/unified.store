using System;

namespace Jhm.Common
{
    public class BrokenRulesException : Exception
    {
        private IBrokenRuleSet brokenRuleSet;

        public BrokenRulesException(IBrokenRuleSet brokenRuleSet)
        {
            this.brokenRuleSet = brokenRuleSet;
        }

        public override string Message
        {
            get
            {
                return brokenRuleSet.ToString();
            }
        }

        public IBrokenRuleSet BrokenRuleSet
        {
            get
            {
                return brokenRuleSet;
            }
        }

        public static void ThrowIfBroken(IBrokenRuleSet brokenRules)
        {
            if (!brokenRules.IsEmpty)
            {
                throw new BrokenRulesException(brokenRules);
            }
        }
    }
}