
using System.Collections.Generic;
using System.Text;

namespace Jhm.Common
{
    public class BrokenRuleSetNameDecorator : IBrokenRuleSet
    {
        private readonly IBrokenRuleSet inner;
        private readonly string name;

        public BrokenRuleSetNameDecorator(IBrokenRuleSet inner, string name)
        {
            this.inner = inner;
            this.name = name;
        }

        public IEnumerable<IRule> Rules
        {
            get
            {
                foreach (IRule rule in inner.Rules)
                {
                    yield return new RuleNameDecorator(rule, name);
                }
            }
        }

        public IEnumerable<string> Messages
        {
            get
            {
                foreach (string str in inner.Messages)
                {
                    yield return string.Format("{0} - {1}", name, str);
                }
            }
        }

        public bool Contains(IRule rule)
        {
            return inner.Contains(rule);
        }

        public int Count
        {
            get { return inner.Count; }
        }

        public bool IsEmpty
        {
            get { return inner.IsEmpty; }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (string str in Messages)
            {
                builder.AppendLine(str);
            }
            return builder.ToString();
        }    
    }

    public class RuleNameDecorator : IRule
    {
        private readonly IRule inner;
        private readonly string name;

        public RuleNameDecorator(IRule inner, string name)
        {
            this.inner = inner;
            this.name = name;
        }

        public string Name
        {
            get { return string.Format("{0} - {1}", name, inner.Name); }
        }

        public string Description
        {
            get { return string.Format("{0} - {1}", name, inner.Description); }
        }
    }
}