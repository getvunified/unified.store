namespace Jhm.Common
{
    public interface IValidator<T>
    {
        IBrokenRuleSet Validate(T entity);
    }
}