﻿using System;

namespace Jhm.Common
{
    [Serializable]
    public abstract class EntityBase<T> : IEntity<T>, IEntity, IEquatable<EntityBase<T>>
    {
        private readonly T id;

        protected EntityBase() { }

        //DO NOT USE Added only to support test harnesses
        protected EntityBase(T id)
        {
            this.id = id;
        }

        /// <summary>
        /// ID may be of type string, int, custom type, etc.
        /// </summary>
        public virtual T Id
        {
            get { return id; }
        }

        object IEntity.Id
        {
            get { return id; }
        }

        /// <summary>
        /// Transient objects are not associated with an item already in storage.  
        /// For instance, a entity is transient if its ID is 0.
        /// </summary>
        public virtual bool IsTransient()
        {
            return id == null || id.Equals(default(T));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as EntityBase<T>);
        }

        public virtual bool Equals(EntityBase<T> other)
        {
            if (other == null) return false;
            if (IsTransient()) return base.Equals(other);
            return other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            return IsTransient() ? base.GetHashCode() : Id.GetHashCode();
        }

        public static bool operator ==(EntityBase<T> x, EntityBase<T> y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(EntityBase<T> x, EntityBase<T> y)
        {
            return !(x == y);
        }

    }
}
