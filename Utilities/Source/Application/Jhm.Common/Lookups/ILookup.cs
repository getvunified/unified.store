using System;

namespace Jhm.Common
{
    public interface ILookup : IEquatable<ILookup>
    {
        string Value { get; }
        string Text { get; }
    }
}