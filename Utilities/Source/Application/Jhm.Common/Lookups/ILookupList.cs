//http://www.jpboodhoo.com/blog/DealingWithDropDownListsWithTheMVPPattern.aspx
//http://msdn.microsoft.com/msdnmag/issues/06/08/DesignPatterns/default.aspx
using System.Collections.Generic;

namespace Jhm.Common
{
    public interface IBindableList<T> 
    {
        void Insert(int index, T item);
        void Add(T item);
        void AddRange(IEnumerable<T> items);
        void Clear();
        void Remove(int index);
        void Remove(T item);
    }

    //Exposed as a property on a view and delegated to a UI Control
    public interface ILookupList : IBindableList<ILookup>
    {
        string SelectedValue { get; set; }
        IEnumerable<string> SelectedValues { get; set; }
    }
}