using System;
using System.Collections.Generic;

namespace Jhm.Common
{
    public class Lookup : ILookup
    {
        public static readonly ILookup NoSelection = new Lookup(string.Empty, "No selection");
        private readonly string text;
        private readonly string value;

        public Lookup(){}

        public Lookup(string value)
        {
            this.value = value;
            text = value;
        }

        public Lookup(Guid value, string text)
        {
            this.value = value.ToString();
            this.text = text;
        }

        public Lookup(string value, string text)
        {
            this.value = value;
            this.text = text;
        }

        public virtual string Value
        {
            get { return value; }
        }

        public virtual string Text
        {
            get { return text; }
        }

        public static T Get<T>(IList<T> lookups, string selectedValue) where T : class, ILookup
        {
            return string.IsNullOrEmpty(selectedValue) ? null :
                Collection.Get(lookups, lookup => lookup.Equals(selectedValue));
        }

        public static bool ExistsIn<T>(string value, params IList<T>[] lookups) where T : class, ILookup
        {
            foreach (var list in lookups)
            {
                if (Collection.Exists(list, lookup => lookup.Equals(value))) return true;
            }
            return false;
        }

        public virtual bool Equals(ILookup other)
        {
            return AreEqual(this, other);
        }

        public override string ToString()
        {
            return Text;
        }

        private static bool AreEqual(ILookup obj1, object obj2)
        {
            if (obj2 == null)
                return AreEqual(obj1, (string)obj2);

            if (obj2 is ILookup)
                return AreEqual(obj1, (ILookup)obj2);

            return AreEqual(obj1, obj2.ToString());
        }

        private static bool AreEqual(ILookup obj1, ILookup obj2)
        {
            return ReferenceEquals(obj1, null ) ? 
                ReferenceEquals(obj2, null) :
                obj1.Value == obj2.Value && obj1.Text == obj2.Text;
        }

        private static bool AreEqual(ILookup obj1, string obj2)
        {
            return ReferenceEquals(obj1, null) ?
                ReferenceEquals(obj2, null) :
                obj1.Value == obj2 || obj1.Text == obj2;
        }

        public override bool Equals(object obj)
        {
            return AreEqual(this, obj);
        }

        public override int GetHashCode()
        {
            return string.IsNullOrEmpty(Value) ? 
                base.GetHashCode() : Value.GetHashCode();
        }

        public static bool operator ==(Lookup obj1, object obj2) { return AreEqual(obj1, obj2); }
        public static bool operator !=(Lookup obj1, object obj2) { return !AreEqual(obj1, obj2); }

        public static implicit operator string(Lookup lookup)
        {
            return lookup != null ? lookup.Value : null;
        }
    }
}