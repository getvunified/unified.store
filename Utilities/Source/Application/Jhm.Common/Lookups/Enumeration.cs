using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Jhm.Common.Lookups
{
    public interface ISelectListItem<T>
    {
        T Value { get; }
        string DisplayName { get; }
    }

    //http://www.lostechies.com/blogs/jimmy_bogard/archive/2008/08/12/enumeration-classes.aspx
    [Serializable]
    public abstract class Enumeration<ID> : ISelectListItem<ID>, IComparable where ID : IComparable
    {

        private ID value;
        private string displayName;
        private short binary;
        
        protected Enumeration()
        {
        }

        protected Enumeration(ID value, string displayName)
        {
            this.value = value;
            this.displayName = displayName;
        }

        protected Enumeration(ID value, short binary, string displayName)
        {
            this.value = value;
            this.displayName = displayName;
            this.binary = binary;
        }

        public virtual ID Value
        {
            get { return value; }
            private set { this.value = value; }
        }

        public virtual short Binary
        {
            get { return binary; }
            private set { binary = value; }
        }

        public virtual string DisplayName
        {
            get { return displayName; }
            private set { displayName = value; }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IEnumerable<T> GetAllSortedByDisplayName<T>() where T : Enumeration<ID>, new()
        {
            var rlist = GetAll<T>().ToList();
            rlist.Sort((x, y) => x.DisplayName.CompareTo(y.DisplayName));

            return rlist;
        }

        public static IEnumerable<T> GetAllSortedByDisplayNameWithPreferenceAtTop<T>(IEnumerable<string> preferenceAtTop, string separatorString = null) where T : Enumeration<ID>, new()
        {
            var rlist = GetAll<T>().ToList();
            rlist.Sort((x, y) => x.DisplayName.CompareTo(y.DisplayName));

            if (preferenceAtTop == null || preferenceAtTop.Count() <= 0) return rlist;

            preferenceAtTop = preferenceAtTop.Reverse();
            if (separatorString != null)
            {
                var separator = new T();
                separator.SetDisplayName(separatorString);
                rlist.Insert(0, separator);
            }

            foreach (var name in preferenceAtTop)
            {
                var item = rlist.Find(x => x.DisplayName == name);

                if (item == null) continue;

                rlist.Remove(item);
                rlist.Insert(0, item);
            }

            return rlist;
        }

        public static IEnumerable<T> GetAllSortedByValue<T>() where T : Enumeration<ID>, new()
        {
            var rlist = GetAll<T>().ToList();
            rlist.Sort((x, y) => x.Value.CompareTo(y.Value));

            return rlist;
        }

        public static IEnumerable<T> GetAll<T>() where T : Enumeration<ID>, new()
        {
            var type = typeof(T);
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            foreach (var info in fields)
            {
                var instance = new T();
                var locatedValue = info.GetValue(instance) as T;

                if (locatedValue != null)
                {
                    yield return locatedValue;
                }
            }
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration<ID>;

            if (otherValue == null)
            {
                return false;
            }

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = value.Equals(otherValue.Value);

            return typeMatches && valueMatches;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        //public static object AbsoluteDifference(Enumeration<ID> firstValue, Enumeration<ID> secondValue)
        //{
        //    var absoluteDifference = Math.Abs(firstValue.Value - secondValue.Value);
        //    return absoluteDifference;
        //}

        public static T FromValue<T>(ID value) where T : Enumeration<ID>, new()
        {
            var matchingItem = parse<T, ID>(value, "value", item => item.Value.CompareTo(value) == 0);
            return matchingItem;
        }

        public static T TryGetFromValue<T>(ID value) where T : Enumeration<ID>, new()
        {
            var matchingItem = Tryparse<T, ID>(value, "value", item => item.Value.CompareTo(value) == 0);
            return matchingItem;
        }

        public static T FromDisplayName<T>(string displayName) where T : Enumeration<ID>, new()
        {
            var matchingItem = parse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        public static T TryGetFromDisplayName<T>(string displayName) where T : Enumeration<ID>, new()
        {
            var matchingItem = Tryparse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        private static T Tryparse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration<ID>, new()
        {
            try
            {
                return parse<T, K>(value, description, predicate);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static T parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration<ID>, new()
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
            {
                var message = string.Format("'{0}' is not a valid {1} in {2}", value, description, typeof(T));
                throw new ApplicationException(message);
            }

            return matchingItem;
        }

        public virtual int CompareTo(object other)
        {
            return Value.CompareTo(((Enumeration<ID>)other).Value);
        }

        public virtual void SetDisplayName(string name)
        {
            DisplayName = name;
        }

        
    }

}