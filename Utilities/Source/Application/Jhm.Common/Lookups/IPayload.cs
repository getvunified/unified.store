namespace Jhm.Common.Lookups
{
    public interface IPayload
    {
        string Key { get; }
    }
}