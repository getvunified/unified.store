using System;
using System.Collections.Generic;

namespace Jhm.Common
{
    /// <summary>
    /// Binds a list of ILookups to a LookupList
    /// </summary>
    public class LookupBinder
    {
        private readonly ILookupList[] lookupLists;

        private LookupBinder(params ILookupList[] to)
        {
            lookupLists = to;
        }

        public static LookupBinder Using(params ILookupList[] to)
        {
            return new LookupBinder(to);
        }

        public LookupBinder Bind<T>(Converter<T, ILookup> func, params IEnumerable<T>[] items)
        {
            if (items != null)
            {
                return Visit(lookupList =>
                             {
                                lookupList.Clear();
                                foreach (var enumerableList in items)
                                {
                                    foreach (var item in enumerableList)
                                    {
                                        lookupList.Add(func(item));
                                    }
                                }
                            });
            }
            return this;
        }

        public LookupBinder Bind<TLookup>(params IEnumerable<TLookup>[] items) where TLookup : ILookup
        {
            if (items != null)
            {
                return Visit(lookupList =>
                                 {
                                     lookupList.Clear();
                                     foreach (var enumerableList in items)
                                     {
                                         foreach (var item in enumerableList)
                                         {
                                             lookupList.Add(item);
                                         }
                                     }
                                 });
            }
            return this;
        }

        public LookupBinder Bind<TEnum>()
        {
            return Bind(ConvertToLookups<TEnum>());
        }

        public LookupBinder Bind(IEnumerable<string> stringList)
        {
            return Bind(ConvertToLookups(stringList));
        }

        private static IEnumerable<ILookup> ConvertToLookups<TEnum>()
        {
            foreach (var item in TypeHelper.GetEnumValues<TEnum>())
            {
                yield return new Lookup(item.ToString(), FriendlyName.Convert(item.ToString()));
            }
        }

        private static IEnumerable<ILookup> ConvertToLookups(IEnumerable<string> stringList)
        {
            if (stringList != null)
            {
                foreach (var str in stringList)
                {
                    yield return new Lookup(str, str);
                }
            }
        }

        public LookupBinder SetSelected(params string[] values)
        {
            return Visit(lookupList => lookupList.SelectedValues = values);
        }

        public LookupBinder SetSelected<T>(IEnumerable<T> lookups) where T : ILookup
        {
            return Visit(lookupList => lookupList.SelectedValues = Collection.ConvertAll<T, string>(lookups,
                                                                   delegate(T lookup)
                                                                   {
                                                                       return lookup.Value;
                                                                   }));
        }

        public LookupBinder AddLookup(ILookup lookup)
        {
            return Visit(lookupList => lookupList.Add(lookup));
        }

        public LookupBinder InsertLookup(int index, ILookup lookup)
        {
            return Visit(lookupList => lookupList.Insert(index, lookup));
        }

        public LookupBinder Remove(Lookup lookup)
        {
            return Visit(lookupList => lookupList.Remove(lookup));
        }

        public LookupBinder Visit(Action<ILookupList> handler)
        {
            foreach (var lookupList in lookupLists)
            {
                handler(lookupList);
            }

            return this;
        }
    }
}
