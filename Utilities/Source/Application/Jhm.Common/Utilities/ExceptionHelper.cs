﻿using System;

namespace Jhm.Common
{
    public class ExceptionHelper
    {
       public static string GetMessages(Exception ex)
        {
            var message = ex.Message;
            if (ex.InnerException != null)
            {
                message = string.Format("{0}\n{1}", message.Trim(), GetMessages(ex.InnerException));
            }
            return message;
        }
    }
}