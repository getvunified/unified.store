using System.Collections.Generic;
using System.Windows.Forms;

namespace Jhm.Common
{
    public class DataGridViewHelper
    {
        static List<DataGridViewColumnHeader> columnHeaders;

        public DataGridViewHelper()
        {
            columnHeaders = new List<DataGridViewColumnHeader>();
        }

        public DataGridViewHelper AddColumnHeader(DataGridViewColumnHeader columnHeader)
        {
            if (!columnHeaders.Contains(columnHeader))
            {
                columnHeaders.Add(columnHeader);
            }

            return this;
        }

        public void Bind<T>(IEnumerable<T> list, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;

            dataGridView.AutoGenerateColumns = columnHeaders.Count == 0;

            if (columnHeaders.Count > 0)
                dataGridView.ColumnCount = columnHeaders.Count;

            foreach (var dataGridViewColumnHeader in Collection.Sort(columnHeaders, (x, y) => x.ColumnId.CompareTo(y.ColumnId)))
            {
                var id = dataGridViewColumnHeader.ColumnId;

                dataGridView.Columns[id].Name = dataGridViewColumnHeader.Name;
                dataGridView.Columns[id].DataPropertyName = dataGridViewColumnHeader.DataPropertyName;

                if (dataGridViewColumnHeader.Visible)
                {
                    dataGridView.Columns[id].MinimumWidth = dataGridViewColumnHeader.MinWidth > 2 ? dataGridViewColumnHeader.MinWidth : 2;

                    if (dataGridViewColumnHeader.AutoSizeColumnMode != null)
                        dataGridView.Columns[id].AutoSizeMode = dataGridViewColumnHeader.AutoSizeColumnMode.Value;
                }
                else
                {
                    dataGridView.Columns[id].Visible = dataGridViewColumnHeader.Visible;
                    //dataGridView.Columns[id].re need to validate readonly before/after change of text
                }
            }



            dataGridView.DataSource = new List<T>(list);
        }
    }

    public class DataGridViewColumnHeader
    {
        public DataGridViewColumnHeader(int columnId, string name, string dataPropertyName, bool visible, int minWidth, DataGridViewAutoSizeColumnMode? autoSizeColumnMode)
        {
            ColumnId = columnId;
            Name = name;
            DataPropertyName = dataPropertyName;
            Visible = visible;
            MinWidth = minWidth;
            AutoSizeColumnMode = autoSizeColumnMode ?? DataGridViewAutoSizeColumnMode.None;
        }

        /// <summary>
        /// ColumnId = 0-based index;
        /// </summary>
        public int ColumnId { get; set; }
        public string Name { get; set; }
        public string DataPropertyName { get; set; }
        public bool Visible { get; set; }
        public int MinWidth { get; set; }
        public DataGridViewAutoSizeColumnMode? AutoSizeColumnMode { get; set; }
    }
}