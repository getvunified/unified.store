using System;
using System.Drawing;
using System.IO;
using System.Xml;

namespace Jhm.Common
{
    public class AssemblyResourceHelper
    {
        /// <summary>
        /// Extracts an embedded file out of a given assembly.
        /// </summary>
        /// <param name="assemblyName">The namespace of you assembly.</param>
        /// <param name="fileName">The name of the file to extract.</param>
        /// <returns>A stream containing the file data.</returns>
        public static Stream GetEmbeddedFile(string assemblyName, string fileName)
        {
            try
            {
                System.Reflection.Assembly a = System.Reflection.Assembly.Load(assemblyName);
                Stream str = a.GetManifestResourceStream(assemblyName + "." + fileName);

                if (str == null)
                    throw new Exception("Could not locate embedded resource '" + fileName + "' in assembly '" + assemblyName + "'");
                return str;
            }
            catch (Exception e)
            {
                throw new Exception(assemblyName + ": " + e.Message);
            }
        }


        /// <summary>
        /// Extracts an embedded file out of a given assembly.
        /// </summary>
        /// <param name="assemblyName">The namespace of you assembly.</param>
        /// <param name="fileName">The name of the file to extract.</param>
        /// <returns>A stream containing the file data. OR NULL if file IS NOT FOUND</returns>
        public static Stream TryGetEmbeddedFile(string assemblyName, string fileName)
        {
            try
            {
                System.Reflection.Assembly a = System.Reflection.Assembly.Load(assemblyName);
                Stream str = a.GetManifestResourceStream(assemblyName + "." + fileName);

                return str;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Stream GetEmbeddedFile(System.Reflection.Assembly assembly, string fileName)
        {
            string assemblyName = assembly.GetName().Name;
            return GetEmbeddedFile(assemblyName, fileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly">Assembly where the embedded file exists</param>
        /// <param name="fileName">IE: Folder1.Folder2.File.txt</param>
        /// <returns>A stream containing the file data OR NULL if file IS NOT FOUND</returns>
        public static Stream TryGetEmbeddedFile(System.Reflection.Assembly assembly, string fileName)
        {
            string assemblyName = assembly.GetName().Name;
            return TryGetEmbeddedFile(assemblyName, fileName);
        }

        public static Stream GetEmbeddedFile(Type type, string fileName)
        {
            string assemblyName = type.Assembly.GetName().Name;
            return GetEmbeddedFile(assemblyName, fileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName">IE: Folder1.Folder2.File.txt</param>
        /// <returns>A stream containing the file data OR NULL if file IS NOT FOUND</returns>
        public static Stream TryGetEmbeddedFile(Type type, string fileName)
        {
            string assemblyName = type.Assembly.GetName().Name;
            return TryGetEmbeddedFile(assemblyName, fileName);
        }

        public static Bitmap GetEmbeddedImage(Type type, string fileName)
        {
            Stream str = GetEmbeddedFile(type, fileName);
            return new Bitmap(str);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName">IE: Folder1.Folder2.File.jpg</param>
        /// <returns>A stream containing the file data OR NULL if file IS NOT FOUND</returns>
        public static Bitmap TryGetEmbeddedImage(Type type, string fileName)
        {
            Stream str = TryGetEmbeddedFile(type, fileName);

            if (str != null)
                return new Bitmap(str);
            else
                return null;
        }

        public static XmlDocument GetEmbeddedXml(Type type, string fileName)
        {
            Stream str = GetEmbeddedFile(type, fileName);

            XmlTextReader tr = new XmlTextReader(str);
            XmlDocument xml = new XmlDocument();
            xml.Load(tr);
            return xml;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="fileName">IE: Folder1.Folder2.File.xml</param>
        /// <returns>A stream containing the file data OR NULL if file IS NOT FOUND</returns>
        public static XmlDocument TryGetEmbeddedXml(Type type, string fileName)
        {
            Stream str = TryGetEmbeddedFile(type, fileName);


            if (str != null)
            {
                XmlTextReader tr = new XmlTextReader(str);
                XmlDocument xml = new XmlDocument();
                xml.Load(tr);
                return xml;
            }
            else
                return null;
        }
    }
}