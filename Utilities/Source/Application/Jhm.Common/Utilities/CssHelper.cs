﻿using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Jhm.Common.Utilities
{
    public enum BrowserType
    {
        IE = 1,
        Other = 99
    }




    public class CssHelper
    {
        public  static void SetCSSOnBrowserType(HttpRequest request, Page page, string ieCSS, string otherCSS)
        {
            HtmlLink css = new HtmlLink();
            switch (TypeHelper.TryParseEnum<BrowserType>(request.Browser.Browser, BrowserType.Other))
            {
                case BrowserType.IE:
                    {
                        css.Href = ieCSS;
                        break;
                    }
                case BrowserType.Other:
                    {
                        goto default;
                    }
                default:
                    {
                        css.Href = otherCSS;
                        break;
                    }
            }
            css.Attributes["rel"] = "stylesheet";
            css.Attributes["type"] = "text/css";
            css.Attributes["media"] = "all";
            page.Header.Controls.Add(css);
        }

    }
}
