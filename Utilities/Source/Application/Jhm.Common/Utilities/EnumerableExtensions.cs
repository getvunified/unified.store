﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Jhm.Common.Utilities
{
    public static class EnumerableExtensions
    {
        public static bool ContainsAllOf<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> values, IEqualityComparer<TSource> comparer)
        {
            var containsAll = values.All(value => source.Contains(value, comparer));
            return containsAll || !(from value in values
                                    from s in source
                                    where !s.ToString().Contains(value.ToString(), StringComparison.InvariantCultureIgnoreCase)
                                    select value).Any();
        }

        public static bool ContainsAnyOf<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> values, IEqualityComparer<TSource> comparer)
        {
            var containsAny = values.Any(value => source.Contains(value, comparer));
            return containsAny || (from value in values
                                    from s in source
                                    where s.ToString().Contains(value.ToString(), StringComparison.InvariantCultureIgnoreCase)
                                    select value).Any();
        }
    }
}
