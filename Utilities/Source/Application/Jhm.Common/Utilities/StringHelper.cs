using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Jhm.Common
{
    public enum RegExType
    {
        RemoveNonSqlFriendlyChars,
        RemoveAllLetters,
        LeaveOnlyNumbers,
        LeaveOnlyLetters,
        LeaveOnlySpecialChars
    }

    public static class StringHelper
    {
        public static string GetEnumName(Enum e)
        {
            return Enum.GetName(e.GetType(), e);
        }

        public static string Left(string text, int length)
        {
            return (text == null ? String.Empty :
                (length < 0 ? text :
                    text.Substring(0, (text.Length < length ? text.Length : length))));
        }

        public static string Right(this string text, int length)
        {
            return length > 0 ? text.Substring(text.Length - length, length) : text;
        }

        public static string ToTitleCase(this string data)
        {
            try
            {
                return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(data.ToLower());
            }
            catch
            {
                return data;
            }
        }

        private static readonly Regex _separateCapitalizedWordsRegex = new Regex(@"[A-Z]+|[0-9]+");

        public static string SeparateCapitalizedWords(this string name)
        {
            return name != null ? _separateCapitalizedWordsRegex.Replace(name, m => (m.Index == 0 ? String.Empty : " ") + m).Replace("  ", " ") : String.Empty;
        }

        
        public static bool Contains(this string original, string value, StringComparison comparisionType)
        {
            return original.IndexOf(value, comparisionType) >= 0;
        }

        public static string Remove(RegExType type, string s)
        {
            switch (type)
            {
                case RegExType.RemoveNonSqlFriendlyChars:
                    s = RemoveNonSqlFriendlyChars(s);
                    break;

                case RegExType.RemoveAllLetters:
                    s = RemoveAllLetters(s);
                    break;

                case RegExType.LeaveOnlyNumbers:
                    s = LeaveOnlyNumbers(s);
                    break;

                case RegExType.LeaveOnlyLetters:
                    s = LeaveOnlyLetters(s);
                    break;

                case RegExType.LeaveOnlySpecialChars:
                    s = LeaveOnlySpecialChars(s);
                    break;
            }

            return s;
        }
        //NOTE: Should probably just 'Escape' the to allow user to enter them but then replace the string sql appropriate text
        private static string RemoveNonSqlFriendlyChars(string s)
        {
            const string pattern = @"[""]|[']";
            return Regex.Replace(s, pattern, string.Empty);
        }

        private static string RemoveAllLetters(string s)
        {
            const string pattern = @"[:alpha:]+";
            return Regex.Replace(s, pattern, string.Empty);
        }

        private static string LeaveOnlyNumbers(string s)
        {
            const string pattern = @"[^0123456789\.]";
            return Regex.Replace(s, pattern, string.Empty);
        }

        private static string LeaveOnlyLetters(string s)
        {
            const string pattern = @"[\W]";
            return Regex.Replace(s, pattern, string.Empty);
        }

        private static string LeaveOnlySpecialChars(string s)
        {
            const string pattern = "[:alnum:]+";
            return Regex.Replace(s, pattern, string.Empty);
        }

        public static string ToSafeForUrl(this string s)
        {
            const string pattern = @"[:/]";
            return Regex.Replace(s, pattern, string.Empty);
        }


        public static string GetStubbedString(int totalLength, char stubWith, string suffix)
        {
            var s = string.Empty;

            for (int i = 0; i < totalLength - suffix.Length; i++)
            {
                s = s + stubWith;
            }

            s = s + suffix;

            return s;
        }
    }

    public class StringIgnoreCaseComparer<TSource> : IEqualityComparer<TSource>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(TSource x, TSource y)
        {

            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.ToString().Equals(y.ToString(), StringComparison.InvariantCultureIgnoreCase);
        }


        public int GetHashCode(TSource obj)
        {
            return obj.ToString().GetHashCode();
        }
    }
}