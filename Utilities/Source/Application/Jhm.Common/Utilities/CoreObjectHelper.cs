using System.Collections.Generic;
using Jhm.Common.Xml;

namespace Jhm.Common
{
    public static class CoreObjectHelper
    {
        public static T Clone<T>(T obj)
        {
            var ms = XmlSerializerHelper.SerializeToXML(obj);
            return XmlSerializerHelper.DeserializeFromXML<T>(ms)[0];
        }

        public static IList<T> Clone<T>(IList<T> obj)
        {
            var ms = XmlSerializerHelper.SerializeToXML<T>(obj);
            return XmlSerializerHelper.DeserializeFromXML<T>(ms);
        }
    }
}