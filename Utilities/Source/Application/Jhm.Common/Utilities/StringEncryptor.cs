using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;

namespace Jhm.Common
{
    public static class StringEncryptor
    {
         private static byte[] _key = System.Text.ASCIIEncoding.ASCII.GetBytes("JHMAHAGCBDUWLMIADKOPAMHJ");
         private static byte[] _iv = System.Text.ASCIIEncoding.ASCII.GetBytes("JHMWMMHJ");
         private static TripleDESCryptoServiceProvider _provider = new TripleDESCryptoServiceProvider();


        public static string EncryptString(string plainText)
        {
            return Transform(plainText, _provider.CreateEncryptor(_key, _iv));
        }

        public static string DecryptString(string encryptedText)
        {
            return Transform(encryptedText, _provider.CreateDecryptor(_key, _iv));
        }


        private static string Transform(string text, ICryptoTransform transform)
        {
            if (text == null)
            {
                return null;
            }
            using (MemoryStream stream = new MemoryStream())
            {
            	using (CryptoStream cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                {
                    byte[] input = Encoding.Default.GetBytes(text);
                    cryptoStream.Write(input, 0, input.Length);
                    cryptoStream.FlushFinalBlock();

                    return Encoding.Default.GetString(stream.ToArray());
                }
            }
        }

        public static string HashString(string str)
        {
            string encodedStr;
            MachineKeySection machineKey = (MachineKeySection) ConfigurationManager.GetSection("system.web/machineKey");
            var hash = new HMACSHA1 { Key = HexToByte(machineKey.ValidationKey) };
            encodedStr = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(str)));
            return encodedStr;
        }

        //   Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration.    
        private static byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }
    }
}