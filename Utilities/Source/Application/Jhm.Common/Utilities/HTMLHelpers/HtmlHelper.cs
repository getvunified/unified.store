﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;


namespace Jhm.Common.Utilities.HTMLHelpers
{
    public static class MyHtmlHelper
    {
        /// <summary>
        /// Creates an anchor tag based on the passed in controller type and method
        /// </summary>
        /// <typeparam name="TController">The Controller Type</typeparam>
        /// <param name="action">The Method to route to</param>
        /// <param name="linkText">The linked text to appear on the page</param>
        /// <returns>System.String</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an Extension Method which allows the user to provide a strongly-typed argument via Expression")]
        public static MvcHtmlString AuthenticateWithShadowBoxActionLink<TController>(this System.Web.Mvc.HtmlHelper helper, Expression<Action<TController>> action, string linkText, object htmlAttributes = null, string area = null) where TController : Controller
        {
            RouteValueDictionary routingValues = Microsoft.Web.Mvc.Internal.ExpressionHelper.GetRouteValuesFromExpression(action);
            if (area == null)
            {
                area = String.Empty;
            }
            routingValues.Add("Area", area);

            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            var url = urlHelper.Action(routingValues["Action"].ToString(), routingValues["Controller"].ToString(), routingValues);

            var anchor = new TagBuilder("a");
            anchor.SetInnerText(linkText);
            anchor.Attributes.Add("href", helper.ViewContext.RequestContext.HttpContext.Request.IsAuthenticated ? url : "javascript:showLogonShadowBox('" + url + "');");
            if (htmlAttributes != null) anchor.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return new MvcHtmlString(anchor.ToString());
        }

        public static string ExposeProperty<T>(Expression<Func<T>> property)
        {
            var expression = GetMemberInfo(property);
            //string path = string.Concat(expression.Member.DeclaringType.FullName,
            //    ".", expression.Member.Name);
            // Do ExposeProperty work here...
            return expression.Member.Name;
        }

        private static MemberExpression GetMemberInfo(Expression method)
        {
            LambdaExpression lambda = method as LambdaExpression;
            if (lambda == null)
                throw new ArgumentNullException("method");

            MemberExpression memberExpr = null;

            if (lambda.Body.NodeType == ExpressionType.Convert)
            {
                memberExpr =
                    ((UnaryExpression)lambda.Body).Operand as MemberExpression;
            }
            else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpr = lambda.Body as MemberExpression;
            }

            if (memberExpr == null)
                throw new ArgumentException("method");

            return memberExpr;
        }
    }
}
