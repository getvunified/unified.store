﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Jhm.Common.Utilities.HTMLHelpers
{
    public static class RepeaterExtensions
    {
        public static void Repeater<T>(this HtmlHelper html, 
            IEnumerable<T> items, 
            string className, 
            string classNameAlt, 
            Action<T, string> render)
        {
            if (items == null)
                return;

            var i = 0;
            foreach (var item in items)
            {
                render(item, (i++ % 2 == 0) ? className : classNameAlt);
            };
        }

        public static void Repeater<T>(this HtmlHelper html, 
            string viewDataKey,
            string cssClass, 
            string altCssClass, 
            Action<T, string> render)
        {
            var items = GetViewDataAsEnumerable<T>(html, viewDataKey);

            var i = 0;
            foreach (var item in items)
            {
                render(item, (i++ % 2 == 0) ? cssClass : altCssClass);
            };
        }

        static IEnumerable<T> GetViewDataAsEnumerable<T>(HtmlHelper html, string viewDataKey)
        {
            var items = html.ViewContext.ViewData as IEnumerable<T>;
            var viewData = html.ViewContext.ViewData as IDictionary<string, object>;
            if (viewData != null)
            {
                items = viewData[viewDataKey] as IEnumerable<T>;
            }
            else
            {
                items = new ViewDataDictionary(viewData)[viewDataKey]
                  as IEnumerable<T>;
            }
            return items;
        }
    }
}
