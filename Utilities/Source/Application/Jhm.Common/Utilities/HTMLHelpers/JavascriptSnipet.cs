using System;
using System.Collections.Generic;
using Foundation;

namespace Jhm.Common
{

    public interface IJavascriptSnipet
    {
        int Id { get; }
        string Key { get; }
        string Script { get; }
        string MethodSignature { get; }
        string CallFunction();
        string CallFunction(string var);
        string CallFunction_HtmlLink(string var);
        string CallFunction(IList<string> vars);
        string CallFunction_HtmlLink(IList<string> vars);
    }

    public class JavascriptSnipet : Lookup, IJavascriptSnipet
    {


        private readonly int id;
        private readonly string text;
        private string key;
        private string script;
        private string methodSignature;

        public JavascriptSnipet(int id, string key, string script, string methodSignature)
        {
            this.id = id;
            this.key = key;
            this.script = script;
            this.methodSignature = methodSignature;
        }

        public JavascriptSnipet() { }

        private JavascriptSnipet(int id, string text)
        {
            this.id = id;
            this.text = text;
        }

        public virtual int Id
        {
            get { return id; }
        }

        public string Key
        {
            get { return key; }
        }

        public string Script
        {
            get { return script; }
        }

        public string MethodSignature
        {
            get { return methodSignature; }
        }

        public string CallFunction()
        {
            return methodSignature;
        }

        public string CallFunction_HtmlLink(string var)
        {
            return string.Format("javascript:{0}", (string.Format(methodSignature, var)));
        }

        public string CallFunction_HtmlLink(IList<string> vars)
        {
            var values = new object[vars.Count];

            for (int i = 0; i < vars.Count; i++)
            {
                values[i] = vars[i];
            }

            return string.Format("javascript:{0}", (string.Format(methodSignature, values)));
        }

        public string CallFunction(string var)
        {
            return string.Format(methodSignature, var);
        }

        public string CallFunction(IList<string> vars)
        {
            var values = new object[vars.Count];

            for (int i = 0; i < vars.Count; i++)
            {
                values[i] = vars[i];
            }

            return string.Format(methodSignature, values);
        }

        public override string Value
        {
            get { return script; }
        }

        public override string Text
        {
            get { return script; }
        }

        //JAVASCRIPT SNIPETS
        public static readonly JavascriptSnipet ConfirmPopup = new JavascriptSnipet(0,
            string.Empty,
            @"function confirmPopup(message) {
            var agree = confirm(message);
            if (agree)
                return true;
            else
                return false;
            }
            ",
            @"return confirmPopup('{0}')");

        public static readonly JavascriptSnipet DoBlur = new JavascriptSnipet(1,
            "onBlur",
            @"function DoBlur(fld) {
            fld.className = 'normalfld';
            }",
            @"DoBlur({0});");

        public static readonly JavascriptSnipet DoFocus = new JavascriptSnipet(2,
            "onFocus",
            @"function DoFocus(fld) {
            fld.className = 'focusfld';
            }",
            @"DoFocus({0});");

        

        public static readonly JavascriptSnipet PopUpWindow = new JavascriptSnipet(3,
            string.Empty,
            @"function popUp(URL, width, height) {
            day = new Date();
            id = day.getTime();
            eval(""page"" + id + "" = window.open(URL, '"" + id + ""', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="" + width + "",height="" + height + ""');"");
            }", "popUp('{0}', {1}, {2})");


        public static readonly JavascriptSnipet SelectAll = new JavascriptSnipet(4,
           string.Empty,
           @" function selectAll(on, id) {
            var allElts = document.forms[0].elements;
            var i;
            for (i = 0; i < allElts.length; i++) {
                var elt = allElts[i];
                var result = elt.name.match(id);
                if (elt.type == ""checkbox"" && result != null && elt.disabled == false) {
                    elt.checked = on;
                }
            }
            }", "selectAll({0},{1})");

        public static readonly JavascriptSnipet CallButtonClick = new JavascriptSnipet(5,
          string.Empty,
          @" function callButtonClick(id) {
            document.getElementById(id).click();
            }", "selectAll");

        public static readonly JavascriptSnipet ConfirmPopupAndShowProgress = new JavascriptSnipet(6,
          string.Empty,
          @"function confirmPopupAndShowProgress(message) {
            var agree = confirm(message);
            if (agree) {
                showProgressBox();
                return true;
            }
            else {
                return false;
            }
        }", "return confirmPopupAndShowProgress('{0}')");

        public static readonly JavascriptSnipet ShadowBox = new JavascriptSnipet(7,
         string.Empty,
         @"
            function showBox(innerHtml) {
                var width = document.documentElement.clientWidth + document.documentElement.scrollLeft;

                var layer = document.createElement('div');
                layer.style.zIndex = 2;
                layer.id = 'layer';
                layer.style.position = 'absolute';
                layer.style.top = '0px';
                layer.style.left = '0px';
                layer.style.height = document.documentElement.clientHeight + 'px';
                layer.style.width = width + 'px';
                layer.style.backgroundColor = 'black';
                layer.style.opacity = '.6';
                layer.style.filter += (""progid:DXImageTransform.Microsoft.Alpha(opacity=60)"");
                document.body.appendChild(layer);

                var div = document.createElement('div');
                div.style.zIndex = 3;
                div.id = 'box';
                div.style.position = (navigator.userAgent.indexOf('MSIE 6') > -1) ? 'absolute' : 'fixed';
                div.style.top = '200px';
                div.style.left = (width / 2) - (400 / 2) + 'px';
                div.style.width = '400px';
                div.style.backgroundColor = 'white';
                div.style.border = '2px solid silver';
                div.style.padding = '20px';
                div.style.textAlign = 'center';
                document.body.appendChild(div);

                var p = document.createElement('p');
                p.innerHTML = innerHtml;
                div.appendChild(p);
            }"
         , "showBox");


        public static readonly JavascriptSnipet ProgressBox = new JavascriptSnipet(100,
            string.Empty,
            @"
            function showProgressBox() {
                var pic = document.getElementById(""progressImage"");
                showBox(""<img id='dynamicProgressImage' src='""+ pic.src +""' /><br /><br /><strong>Processing Please Wait...</strong><br />"");
                return true;
            }"
            , @"return showProgressBox()");


        public static readonly JavascriptSnipet ExtractStyles = new JavascriptSnipet(8,
            string.Empty, @"
    
        var output = """";
        var selector = """";
        var tclasses = new Array();

        $(document).ready(function() {

            $(selector + "" [id],"" + selector + ""[class]"").each(function(i) {
                var tmp = """", childOutput = """";
                var tid = $(this).attr(""id"");
                var tclass = $(this).attr(""class"");
                var tstyle = $(this).attr(""style"");

                if (jQuery.inArray(tclass, tclasses) == -1 && typeof (tstyle) != ""undefined"" && tstyle.toLowerCase().split("";"").length > 0 && tstyle.toLowerCase().split("";"")[0] != """") {
                    if (tclass) tclasses.push(tclass);

                    $(this).children(""ul"").each(function() {
                        if (tid) {
                            childOutput = ""#"" + tid;
                        } else {
                            childOutput = ""."" + tclass;
                        }
                        childOutput = childOutput + "" ul\n{\n}\n\n"" + childOutput + "" li\n{\n}\n\n"";
                    });

                    if (typeof (tstyle) != ""undefined"") {
                        properties = new Array();
                        properties = tstyle.toLowerCase().split("";"");

                        //testing
                        //$(""#test"").html($(""textarea"").html() + ""\n\n"" + properties)

                        for (var i = 0, len = properties.length; i < len; ++i) {
                            if (properties[i] != """") properties[i] = jQuery.trim(properties[i]);
                        }

                        tmp = properties.sort().join("";\n"") + "";"";

                        if (tmp.substring(0, 1) == "";"") {
                            tstyle = tmp.substring(1, tmp.length);
                        } else {
                            if (tmp != """") tstyle = ""\n"" + tmp;
                        }
                    } else {
                        tstyle = """";
                    }

                    if (tid) {
                        output = output + ""#"" + tid + ""\n{"" + tstyle + ""\n}\n\n"" + childOutput;
                    } else {
                        output = output + ""."" + tclass + ""\n{"" + tstyle + ""\n}\n\n"" + childOutput;
                    }
                }
            });

            $(""body"").append(""<textarea id='output'>"" + output + ""<\/textarea>"");
            $(""#output"").css({ height: 400, width: 800,    position: 'absolute', zIndex: 5000, left: '0px', top: '0px'});

        });", string.Empty);




        //SORTING
        public static readonly List<JavascriptSnipet> All = Collection.Sort(new List<JavascriptSnipet>
        {
        ConfirmPopup,
        DoBlur,
        DoFocus,
        PopUpWindow,
        SelectAll,
        CallButtonClick,
        ConfirmPopupAndShowProgress,
        ShadowBox,
        ProgressBox}, (x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.id, y.id));

    }
}