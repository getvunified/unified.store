﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Jhm.Common.Utilities.HTMLHelpers
{
    public static class DMHtmlHelper
    {
        public static HtmlString DifferenceMediaTitleHeader(this HtmlHelper helper, string titleText)
        {
            if ( string.IsNullOrEmpty(titleText) )
            {
                return new HtmlString(string.Empty);
            }
            return new HtmlString("<div id='storeTitle'>" + titleText + "</div><style type='text/css'>#bodyContent{ background-color:white; padding-bottom:10px; }</style>");
        }
    }
}
