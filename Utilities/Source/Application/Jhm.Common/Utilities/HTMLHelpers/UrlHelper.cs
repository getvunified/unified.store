﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Jhm.Common
{
    public static class UrlHelperExtension
    {

        public static string Absolute(this UrlHelper url, string relativeOrAbsolute)
        {
            var uri = new Uri(relativeOrAbsolute, UriKind.RelativeOrAbsolute);
            if (uri.IsAbsoluteUri)
            {
                return relativeOrAbsolute;
            }
            // At this point, we know the url is relative.
            return VirtualPathUtility.ToAbsolute(relativeOrAbsolute);
        }

        public static string AbsoluteContent(this UrlHelper url, string relativeOrAbsolute)
        {
            relativeOrAbsolute = url.Content(relativeOrAbsolute);
            var uri = new Uri(relativeOrAbsolute, UriKind.RelativeOrAbsolute);
            if (uri.IsAbsoluteUri)
            {
                //
                // Make sure that returned Urls match the current Scheme of the website.
                //
                if (url.RequestContext.HttpContext.Request.Url != null && uri.Scheme != url.RequestContext.HttpContext.Request.Url.Scheme)
                {
                    relativeOrAbsolute = relativeOrAbsolute.ToLower().Replace(uri.Scheme.ToLower(), url.RequestContext.HttpContext.Request.Url.Scheme.ToLower());
                }
                return relativeOrAbsolute;
            }
            if (url.RequestContext.HttpContext.Request.Url != null)
            {
                string hostUrl = url.RequestContext.HttpContext.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);
                // At this point, we know the url is relative.
                return hostUrl + VirtualPathUtility.ToAbsolute(relativeOrAbsolute);
            }
            return relativeOrAbsolute;
        }
    }
}
