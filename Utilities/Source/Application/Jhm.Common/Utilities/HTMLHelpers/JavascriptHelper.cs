using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Jhm.Common
{
    public static class JavascriptHelper
    {
        public static void AddJavascript(Page page, IList<JavascriptSnipet> snipets)
        {
            var script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");

            foreach (JavascriptSnipet snipet in snipets)
            {
                script.InnerHtml += "\n\n" + snipet.Script + "\n\n";
            }

            page.Header.Controls.Add(script);

        }

        public static void AddJavascript(Page page, JavascriptSnipet snipet)
        {
            var script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");

            script.InnerHtml += "\n\n" + snipet.Script + "\n\n";
            page.Header.Controls.Add(script);
        }

        public static void AddJavascriptLink(Page page, string link)
        {
            HtmlGenericControl js = new HtmlGenericControl("script");
            js.Attributes["type"] = "text/javascript";
            js.Attributes["src"] = link;
            page.Header.Controls.Add(js);
        }

        public static MvcHtmlString FieldIdFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            string inputFieldId = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName);
            return MvcHtmlString.Create(inputFieldId);
        }
    }
}