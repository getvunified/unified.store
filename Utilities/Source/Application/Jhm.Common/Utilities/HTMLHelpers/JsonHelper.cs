﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Jhm.Common.Utilities.HTMLHelpers
{
    public static class JsonHelper {}


    public class RenderJsonResult : ActionResult
    {
        /// <summary>
        /// The result object to render using JSON.
        /// </summary>
        public object Result { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/json";
            var serializer = new JavaScriptSerializer();
            var sb = new StringBuilder();
            serializer.Serialize(this.Result, sb);
            context.HttpContext.Response.Output.Write(sb);
        }
    }
}
