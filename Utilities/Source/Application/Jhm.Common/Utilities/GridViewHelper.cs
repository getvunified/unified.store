﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Jhm.Common
{
    public class GridViewHelper
    {
        public static void GetCheckBoxHiddenFieldValues(IList<string> list, GridView gridView, string checkBoxId, string hiddenFieldId)
        {
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                GridViewRow row = gridView.Rows[i];
                bool isChecked = ((CheckBox)row.FindControl(checkBoxId)).Checked;

                if (isChecked)
                {
                    list.Add(((HiddenField)row.FindControl(hiddenFieldId)).Value);
                }
            }
        }

        public static void GetHiddenFieldValues(IList<string> list, GridView gridView, string hiddenFieldId)
        {
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                GridViewRow row = gridView.Rows[i];


                list.Add(((HiddenField)row.FindControl(hiddenFieldId)).Value);
            }
        }

        public static void AddButtonEvent(GridView gridView, string buttonId, ButtonType buttonType, EventHandler onClick, string onClientClick)
        {
            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                GridViewRow row = gridView.Rows[i];

                switch (buttonType)
                {
                    case ButtonType.Button:
                        {
                            Button button = (Button)row.FindControl(buttonId);

                            if (onClick != null)
                                button.Click += onClick;

                            if (onClientClick != null)
                                button.OnClientClick += onClientClick;

                            break;
                        }

                    case ButtonType.Link:
                        {
                            LinkButton button = (LinkButton)row.FindControl(buttonId);

                            if (onClick != null)
                                button.Click += onClick;

                            if (onClientClick != null)
                                button.OnClientClick += onClientClick;

                            break;
                        }
                }
            }
        }


    }
}
