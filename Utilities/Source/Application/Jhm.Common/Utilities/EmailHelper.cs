﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Jhm.Common.Utilities
{
    public static class EmailHelper
    {
        public static readonly string EmailFromAccount = "noreply@jhm.org";
        public static readonly string ContactUsEmailAccount = "support@jhm.org";
        public static readonly string PraiseEmailAccount = "praise@jhm.org";
        public static readonly string PrayerEmailAccount = "prayer@jhm.org";



        public static string MapObjectToString(Object obj, Type declaringType)
        {
            var sb = new StringBuilder();
            sb.AppendLine("<table>");

            var properties = declaringType.GetProperties().Where(x => x.DeclaringType == declaringType);
            foreach (var prop in properties.Where(prop => prop.CanRead))
            {
                sb.AppendLine("<tr><td>");
                try
                {
                    var attribute =(DisplayNameAttribute) prop.GetCustomAttributes(typeof (DisplayNameAttribute), true).Single();
                    sb.AppendFormat("<b>{0}:</b>", attribute.DisplayName);
                }
                catch (Exception ex)
                {
                    sb.AppendFormat("<b>{0}:</b>", prop.Name);
                }
                sb.AppendFormat(" {0}", FormatValue(prop.GetValue(obj, null)));
                sb.AppendLine("</td></tr>");
            }
            sb.AppendLine("</table>");
            return sb.ToString();
        }

        private static object FormatValue(object obj)
        {
            if ( obj is bool )
            {
                return ((bool) obj) ? "Yes" : "No";
            }
            return obj;
        }
    }
}
