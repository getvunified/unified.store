using System;

namespace Jhm.Common
{
    public static class DateHelper
    {
        public static string GetJulianDate(DateTime dtm)
        {
            return (dtm.Year - 1900) + dtm.DayOfYear.ToString("000");
        }

        public static DateTime? ParseDate(string dateTime)
        {
            DateTime dtm;
            DateTime.TryParse(dateTime, out dtm);
            return dtm == DateTime.MinValue ? (DateTime?)null : dtm;
        }

        public static DateTime ParseDate(int julianDate)
        {
            DateTime dtm;
            int year = TypeHelper.TryParseInt(StringHelper.Left(julianDate.ToString(),3)) + 1900;
            double dayofyear = TypeHelper.TryParseInt(StringHelper.Right(julianDate.ToString(), 3));
            dtm = new DateTime(year, 1,1);
            dtm = dtm.AddDays(dayofyear-1);
            return (DateTime) (dtm == DateTime.MinValue ? (DateTime?)null : dtm);
        }

        public static DateTime GetLastMonthDate()
        {
            return (DateTime.Now.AddMonths(-1));
        }

        public static DateTime GetStartOfWeekDate(DateTime dateTime)
        {
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;

            DayOfWeek fdow = ci.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek dayOfWeek = dateTime.DayOfWeek;
            
            return dateTime.AddDays(-(dayOfWeek - fdow)).Date;
        }

        public static DateTime GetEndOfWeekDate(DateTime dateTime)
        {
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
            DayOfWeek fdow = ci.DateTimeFormat.FirstDayOfWeek;
            DayOfWeek dayOfWeek = dateTime.DayOfWeek;
            DateTime fdowDate = dateTime.AddDays(-(dayOfWeek - fdow)).Date;
            
            return fdowDate.AddDays(6);
        }

        public static DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            DateTime dt = new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
            return dt;
        }

        public static bool WithinRange(DateTime? dateTime, int range)
        {
            var within = false;
            
            if(dateTime != null)
            {
                if (dateTime.Value.Date <= DateTime.Now.Date && dateTime.Value.Date >= DateTime.Now.AddDays(range).Date)
                {
                    within = true;
                }
            }

            return within;
        }

        /// <summary>
        /// Gets a DateTime representing midnight on the current date
        /// </summary>
        /// <param name="current">The current date</param>
        public static DateTime Midnight(this DateTime current)
        {
            DateTime midnight = new DateTime(current.Year, current.Month, current.Day);
            return midnight;
        }

        /// <summary>
        /// Gets a DateTime representing noon on the current date
        /// </summary>
        /// <param name="current">The current date</param>
        public static DateTime Noon(this DateTime current)
        {
            DateTime noon = new DateTime(current.Year, current.Month, current.Day, 12, 0, 0);
            return noon;
        }
        /// <summary>
        /// Sets the time of the current date with minute precision
        /// </summary>
        /// <param name="current">The current date</param>
        /// <param name="hour">The hour</param>
        /// <param name="minute">The minute</param>
        public static DateTime SetTime(this DateTime current, int hour, int minute)
        {
            return SetTime(current, hour, minute, 0, 0);
        }

        /// <summary>
        /// Sets the time of the current date with second precision
        /// </summary>
        /// <param name="current">The current date</param>
        /// <param name="hour">The hour</param>
        /// <param name="minute">The minute</param>
        /// <param name="second">The second</param>
        /// <returns></returns>
        public static DateTime SetTime(this DateTime current, int hour, int minute, int second)
        {
            return SetTime(current, hour, minute, second, 0);
        }

        /// <summary>
        /// Sets the time of the current date with millisecond precision
        /// </summary>
        /// <param name="current">The current date</param>
        /// <param name="hour">The hour</param>
        /// <param name="minute">The minute</param>
        /// <param name="second">The second</param>
        /// <param name="millisecond">The millisecond</param>
        /// <returns></returns>
        public static DateTime SetTime(this DateTime current, int hour, int minute, int second, int millisecond)
        {
            DateTime atTime = new DateTime(current.Year, current.Month, current.Day, hour, minute, second, millisecond);
            return atTime;
        }
    }
}