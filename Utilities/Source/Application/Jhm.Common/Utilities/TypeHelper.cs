using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Jhm.Common
{
    public static class TypeHelper
    {
        public static Guid TryParseGuid(string value)
        {
            try
            {
                return new Guid(value.Trim());
            }
            catch
            {
                return Guid.Empty;
            }
        }

       

        private static readonly Regex TrueExpression = 
            new Regex(@"^(true|1|yes|-1)$", RegexOptions.IgnoreCase);

        public static bool TryParseBoolean(bool? value)
        {
            try
            {
                if (value == null)
                {
                    return false;
                }

                return value.Value;
            }
            catch
            {
                return false;
            }
        }

        public static bool TryParseBoolean(string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return false;
                }

                return TrueExpression.IsMatch(value);
            }
            catch
            {
                return false;
            }
        }

        public static T TryParseEnum<T>(string value, T defaultValue)
        {
            try
            {
                return ParseEnum<T>(value);
            }
            catch 
            {
                return defaultValue;
            }
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T[] GetEnumValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }

        public static string[] GetEnumNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static int TryParseInt(string value)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }

        public static long TryParseLong(string value)
        {
            long result;
            long.TryParse(value, out result);
            return result;
        }

        public static float TryParseFloat(string value)
        {
            float result;
            float.TryParse(value, out result);
            return result;
        }

        public static double TryParseDouble(string value)
        {
            double result;
            double.TryParse(value, out result);
            return result;
        }

        public static decimal TryParseDecimal(string value)
        {
            decimal result;
            decimal.TryParse(value, out result);
            return result;
        }

        public static string GetDisplayName(Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            try
            {
                DisplayAttribute attrs = (DisplayAttribute)(field.GetCustomAttributes(typeof(DisplayAttribute), false)).First();
                return attrs.GetName();
            }
            catch (Exception)
            {
                return value.ToString();
            }
        }
    }
}