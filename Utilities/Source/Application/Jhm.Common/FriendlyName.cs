using System;
using System.Text.RegularExpressions;

namespace Jhm.Common
{
    /*
     * Builds a string or string array of names from an enum 
     * by separating the names at each capital letter
     * Example: 'MyExampleType' becomes 'My Example Type'
     * Useful for populating dropdowns with friendly names from an enum
     * 
     * */

    public static class FriendlyName
    {
        private static readonly Regex re = new Regex(@"[A-Z]+|[0-9]+");

        public static string Convert(string name)
        {
            if (name != null)
            {
                return re.Replace(name, delegate(Match m)
                                            {
                                                return (m.Index == 0 ? String.Empty : " ") + m;
                                            }).Replace("  ", " ");
            }

            return String.Empty;
        }
    }
}
