﻿using System.Net.Mail;

namespace Jhm.Common
{
    public interface IEmailer
    {
        void SendEmail(MailMessage message);
    }
}