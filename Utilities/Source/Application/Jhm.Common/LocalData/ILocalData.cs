namespace Jhm.Common.LocalData
{
    public interface ILocalData
    {
        object this[object key] { get; set; }
        void Clear();
    }
}