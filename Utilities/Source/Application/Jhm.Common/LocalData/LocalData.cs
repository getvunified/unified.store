using System;
using System.Collections;
using System.Web;

namespace Jhm.Common.LocalData
{
    //Thread.GetData(Thread.GetNamedDataSlot(CurrentSessionKey)) as ISession;
    //CallContext.GetData(CurrentSessionKey);
    //CallContext.SetData(CurrentTransactionKey, value)

    public class Local
    {
        static ILocalData current = new LocalData();
        static object LocalDataHashtableKey = new object();

        private class LocalData : ILocalData
        {
            [ThreadStatic]
            static Hashtable threadHashtable;

            private static Hashtable LocalHashtable
            {
                get
                {
                    if (!RunningInWebContext)
                    {
                        return threadHashtable ?? (threadHashtable = new Hashtable());
                    }

                    Hashtable webHashtable = HttpContext.Current.Items[LocalDataHashtableKey] as Hashtable;
                    if (webHashtable == null)
                    {
                        HttpContext.Current.Items[LocalDataHashtableKey] = webHashtable = new Hashtable();
                    }
                    return webHashtable;
                }
            }

            public object this[object key]
            {
                get { return LocalHashtable[key]; }
                set { LocalHashtable[key] = value; }
            }

            public void Clear()
            {
                LocalHashtable.Clear();
            }
        }

        public static ILocalData Data
        {
            get { return current; }
        }

        public static bool RunningInWebContext
        {
            get { return HttpContext.Current != null; }
        }
    }
}