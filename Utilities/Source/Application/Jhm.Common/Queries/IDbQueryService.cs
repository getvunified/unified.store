using System;
using System.Collections.Generic;

namespace Jhm.Common.Queries
{
    public interface IQueryService : IService
    {
        IList<T> Execute<T>(IQuery<T> query, Converter<object[], T> converter);
        IList<T> Execute<T>(IQuery<T> query);
        T FindOne<T>(IQuery<T> query);
    }

    public interface IDbQueryService : IService
    {
        IList<T> Execute<T>(IDbQuery<T> query, Converter<object[], T> converter);
    }
}