namespace Jhm.Common.Queries
{
    public class DbQuery<T> : Query<T>, IDbQuery<T>
    {
        public DbQuery(string queryString)
            : base(queryString)
        {

        }

        //fluent interface style
        public new IDbQuery<T> SetParameter(string key, object value)
        {
            base.SetParameter(key, value);
            return this;
        }
    }
}