using System.Collections.Generic;

namespace Jhm.Common.Queries
{
    public class Query<T> : IQuery<T>
    {
        private readonly string queryString;

        //When an instance of a Query is declared statically
        //then multiple threads could change the parameters values
        private readonly IDictionary<string, object> parameters = new Dictionary<string, object>();

        public Query(string queryString)
        {
            this.queryString = queryString;
        }

        public string QueryString
        {
            get { return queryString; }
        }

        public IDictionary<string, object> Parameters
        {
            get { return parameters; }
        }

        //fluent interface style
        public virtual IQuery<T> SetParameter(string key, object value)
        {
            if (!parameters.ContainsKey(key))
            {
                parameters.Add(key, value);
            }
            else
            {
                parameters[key] = value;
            }

            return this;
        }
    }
}