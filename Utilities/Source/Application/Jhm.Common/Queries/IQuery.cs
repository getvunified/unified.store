using System.Collections.Generic;

namespace Jhm.Common.Queries
{
    public interface IQuery<T>
    {
        string QueryString { get; }
        IDictionary<string, object> Parameters { get; }
        IQuery<T> SetParameter(string key, object value); //fluent style
    }
}