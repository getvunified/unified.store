using System;
using System.Collections;
using NHibernate.Transform;

namespace Jhm.FluentNHibernateProvider
{
    public class FluentTupleToObjectResultTransformer<T> : IResultTransformer
    {
        private readonly Converter<object[], T> converter;

        public FluentTupleToObjectResultTransformer(Converter<object[], T> converter)
        {
            this.converter = converter;
        }

        public object TransformTuple(object[] tuple, string[] aliases)
        {
            return converter(tuple);
        }

        public IList TransformList(IList collection)
        {
            return collection;
        }
    }
}