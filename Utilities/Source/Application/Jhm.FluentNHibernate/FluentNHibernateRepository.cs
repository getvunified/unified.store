using System.Collections.Generic;
using Jhm.Common;
using Jhm.Common.Queries;
using Jhm.Common.Repositories;

namespace Jhm.FluentNHibernateProvider
{
    public class FluentNHibernateRepository<T> : IRepository<T> where T : class, IEntity
    {
        public T Get(object id) 
        {
            return FluentNHibernateHelper.CurrentSession.Get<T>(id);
        }

        public T FindOne(IQuery<T> query)
        {
            return FluentNHibernateHelper.CreateHqlQuery(query).UniqueResult<T>();
        }

        public IList<T> FindAll(IQuery<T> query)
        {
            return FluentNHibernateHelper.CreateHqlQuery(query).List<T>();
        }

        public void Save(T entity) 
        {
            FluentNHibernateHelper.CurrentSession.SaveOrUpdate(entity);
        }

        public void Delete(T entity) 
        {
            FluentNHibernateHelper.CurrentSession.Delete(entity);
        }

        public void DeleteAll(IQuery<T> query)
        {
            var entities = FluentNHibernateHelper.CreateHqlQuery(query).List<T>();
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

    }
}
