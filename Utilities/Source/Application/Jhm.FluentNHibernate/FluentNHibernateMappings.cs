using System;
using System.Reflection;
using FluentNHibernate.Cfg;

namespace Jhm.FluentNHibernateProvider
{
    public static class FluentNHibernateMappings
    {
        public static Action<MappingConfiguration> FluentMappingsFromAssembly(Type type)
        {
            return cfg => cfg.FluentMappings.AddFromAssembly(Assembly.GetAssembly(type));
        }
    }
}