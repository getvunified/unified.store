using System;
using System.Collections.Generic;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;

namespace Jhm.FluentNHibernateProvider
{
    public sealed class FluentNHibernateHelper
    {
        private static readonly object lockObj = new object();
        private static ISessionFactory sessionFactory = null;

        public static ISessionFactory SessionFactory(FluentNHibernateConfiguration FNHConfiguration)
        {
            if (sessionFactory == null)
            {
                lock (lockObj)
                {
                    if (sessionFactory == null)
                    {
                        //ISessionFactory is immutable and is an expensive-to-create, 
                        //thread safe object intended to be shared by all application threads.
                        Configuration fluentConfiguration = Fluently.Configure().Database(FNHConfiguration.PersistenceConfigurer()).Mappings(FNHConfiguration.MappingConfiguration()).BuildConfiguration();
                        sessionFactory = fluentConfiguration.BuildSessionFactory();
                    }
                }
            }

            return sessionFactory;
        }

        public static ISession CurrentSession
        {
            get
            {
                ISession session = FluentNHibernateUnitOfWorkFactory.CurrentSession;
                if (session == null)
                {
                    throw new InvalidOperationException("Cannot perform this operation on an unopened session.");
                }

                return session;
            }
        }

        private static void SetNamedParameters(IQuery query, IDictionary<string, object> parameters)
        {
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> parameter in parameters)
                {
                    query.SetParameter(parameter.Key, parameter.Value);
                }
            }
        }

        public static IQuery CreateHqlQuery(string queryString, IDictionary<string, object> parameters)
        {
            IQuery hqlQuery = CurrentSession.CreateQuery(queryString);
            SetNamedParameters(hqlQuery, parameters);
            return hqlQuery;
        }

        public static IQuery CreateHqlQuery<T>(Jhm.Common.Queries.IQuery<T> query)
        {
            return CreateHqlQuery(query.QueryString, query.Parameters);
        }

        
    }
}
