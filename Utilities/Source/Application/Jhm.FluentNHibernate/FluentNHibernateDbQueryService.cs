using System;
using System.Collections.Generic;
using System.Data;
using Jhm.Common.Queries;
namespace Jhm.FluentNHibernateProvider
{
    public class FluentNHibernateQueryService : IDbQueryService, IQueryService
    {
        private static void CreateDbDataParameters(IDbCommand command, IDictionary<string, object> parameters)
        {
            foreach (KeyValuePair<string, object> pair in parameters)
            {
                IDbDataParameter p = command.CreateParameter();
                p.ParameterName = pair.Key;
                p.Value = pair.Value;
                command.Parameters.Add(p);
            }
        }

        public IList<T> Execute<T>(IDbQuery<T> query, Converter<object[], T> converter)
        {
            IDbConnection connection = FluentNHibernateHelper.CurrentSession.Connection;

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = query.QueryString;
                command.CommandType = CommandType.Text;

                CreateDbDataParameters(command, query.Parameters);
                IDataReader reader = command.ExecuteReader();
                IList<T> results = new List<T>();

                while (reader.Read())
                {
                    results.Add(converter(DataReaderToTupleConverter(reader)));
                }

                reader.Close();

                return results;
            }
        }

        public IList<T> Execute<T>(IQuery<T> query, Converter<object[], T> converter)
        {
            return FluentNHibernateHelper.CreateHqlQuery(query)
                .SetResultTransformer(new FluentTupleToObjectResultTransformer<T>(converter))
                .List<T>();
        }

        public IList<T> Execute<T>(IQuery<T> query)
        {
            return FluentNHibernateHelper.CreateHqlQuery(query).List<T>();
        }

        public T FindOne<T>(IQuery<T> query)
        {
            return FluentNHibernateHelper.CreateHqlQuery(query).UniqueResult<T>();
        }

        private static object[] DataReaderToTupleConverter(IDataRecord reader)
        {
           var values = new object[reader.FieldCount];
           reader.GetValues(values);

           //Convert to .NET null
           for (var i = 0; i < values.Length; i++)
           {
               if (values[i] == DBNull.Value) values[i] = null;
           }

           return values;
        }
    }
}