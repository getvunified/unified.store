using System;
using Jhm.Common.UOW;
using NHibernate;
using ITransaction=Jhm.Common.UOW.ITransaction;

namespace Jhm.FluentNHibernateProvider
{
    /// <summary>
    /// This is not thread safe
    /// </summary>
    public class FluentNHibernateUnitOfWork : IUnitOfWork
    {
        private readonly ISession session;
        private int usageCount = 1;

        public FluentNHibernateUnitOfWork(ISession session)
        {
            this.session = session;
        }

        public void IncrementUsage()
        {
            usageCount++;
        }

        public bool IsInTransaction
        {
            get { return session.Transaction.IsActive; }
        }

        public ITransaction Transaction
        {
            get { return new FluentNHibernateTransaction(session.Transaction); }
        }

        public ITransaction BeginTransaction()
        {
            return new FluentNHibernateTransaction(session.BeginTransaction());
        }

        public void Dispose()
        {
            usageCount--;
            if (usageCount != 0) return;
            FluentNHibernateUnitOfWorkFactory.DisposeUnitOfWork();
            session.Dispose();
        }

        public ISession Session
        {
            get { return session; }
        }
    }
}