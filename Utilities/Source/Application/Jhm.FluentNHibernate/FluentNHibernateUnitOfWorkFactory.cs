using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Jhm.Common.LocalData;
using Jhm.Common.UOW;
using NHibernate;

namespace Jhm.FluentNHibernateProvider
{
    public class FluentNHibernateUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private const string CurrentSessionKey = "NHibernate.Session";
        private readonly Action<MappingConfiguration> mappingConfiguration;
        private readonly IPersistenceConfigurer persistenceConfigurer;

        public FluentNHibernateUnitOfWorkFactory(IPersistenceConfigurer persistenceConfigurer, Action<MappingConfiguration> mappingConfiguration)
        {
            this.mappingConfiguration = mappingConfiguration;
            this.persistenceConfigurer = persistenceConfigurer;
        }

        public IUnitOfWork Create()
        {

            ISession session = FluentNHibernateHelper.SessionFactory(FluentNHibernateConfiguration.Create(persistenceConfigurer, mappingConfiguration)).OpenSession();
            session.FlushMode = FlushMode.Commit;
            CurrentSession = session;
            return new FluentNHibernateUnitOfWork(session);
        }

        //Context specific session handling
        public static ISession CurrentSession
        {
            get { return Local.Data[CurrentSessionKey] as ISession; }
            set { Local.Data[CurrentSessionKey] = value; }
        }

        public static void DisposeUnitOfWork()
        {
            CurrentSession = null;
            UnitOfWork.DisposeCurrent();
        }
    }
}