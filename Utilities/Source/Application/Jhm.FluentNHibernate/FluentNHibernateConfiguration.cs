using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace Jhm.FluentNHibernateProvider
{
    public class FluentNHibernateConfiguration
    {
        private readonly IPersistenceConfigurer persistenceConfigurer;
        private readonly Action<MappingConfiguration> mappingConfiguration;
        
        private FluentNHibernateConfiguration(IPersistenceConfigurer persistenceConfigurer, Action<MappingConfiguration> mappingConfiguration)
        {
            this.persistenceConfigurer = persistenceConfigurer;
            this.mappingConfiguration = mappingConfiguration;
            
        }

        public IPersistenceConfigurer PersistenceConfigurer() { return persistenceConfigurer; }
        public Action<MappingConfiguration> MappingConfiguration() { return mappingConfiguration; }
        

        public static FluentNHibernateConfiguration Create(IPersistenceConfigurer persistenceConfigurer, Action<MappingConfiguration> mappingConfiguration)
        {
            return new FluentNHibernateConfiguration(persistenceConfigurer, mappingConfiguration);
        }
    }
}