using Jhm.Common.UOW;

namespace Jhm.FluentNHibernateProvider
{
    public class FluentNHibernateTransaction : ITransaction
    {
        private readonly NHibernate.ITransaction transaction;

        public FluentNHibernateTransaction(NHibernate.ITransaction transaction)
        {
            this.transaction = transaction;
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            transaction.Dispose();
        }
    }
}