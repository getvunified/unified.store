using System;

namespace Jhm.Data
{
    public class DataSessionException : Exception
    {
        public DataSessionException(string message, params object[] args) : base(String.Format(message, args))
        {
        }

        public DataSessionException(Exception innerException, string message, params object[] args)
            : base(String.Format(message, args), innerException)
        {
            
        }
    }
}
