using System;
using System.Data;

namespace Jhm.Data
{
    public interface IDataSession : IDisposable
    {
        IDbCommand CreateCommand();
        IDbCommand CreateCommand(string commandText);

        IDbConnection Connection { get; }

        //manage an IDbTransaction
        bool IsInTransaction { get; }
        IDbTransaction Transaction { get; }
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

        IDataReader ExecuteReader(string commandText, params object[] parameterValues);

        int ExecuteNonQuery(IDbCommand command);
        int ExecuteNonQuery(IDbCommand command, params object[] parameterValues);
        int ExecuteNonQuery(string commandText, params object[] parameterValues);
    }
}
