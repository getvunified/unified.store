using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace Jhm.Data
{
    public class DataSession : IDataSession
    {
        private DbProviderFactory dbProviderFactory;
        private IDbConnection dbConnection;
        private IDbTransaction dbTransaction;

        public DataSession(string settingName)
        {
            //Retrieve the connection string from the config file
            ConnectionStringSettings connectionStringSetting = ConfigurationManager.ConnectionStrings[settingName];

            if (connectionStringSetting != null)
            {
                dbProviderFactory = DbProviderFactories.GetFactory(connectionStringSetting.ProviderName);
                dbConnection = dbProviderFactory.CreateConnection();
                dbConnection.ConnectionString = connectionStringSetting.ConnectionString;
            }
            else
                throw new DataSessionException(
                    "Unable to access the '{0}' connection string from the default configuration.", 
                    settingName);
        }
        
        private void CloseConnection()
        {
            try
            {
                if(dbTransaction != null)
                {
                    dbTransaction = null;
                }

                if (dbConnection != null && 
                    dbConnection.State != ConnectionState.Closed)
                {
                    dbConnection.Close();
                }
            }
            catch
            {
            }
        }

        public IDbCommand CreateCommand()
        {
            IDbCommand dbCommand = dbProviderFactory.CreateCommand();
            dbCommand.Connection = dbConnection;
            return dbCommand;
        }
        

        public IDbCommand CreateCommand(string commandText)
        {
            return CreateCommand(commandText, CommandType.Text);
        }

        public IDbConnection Connection
        {
            get { return dbConnection; }
        }

        public IDbCommand CreateCommand(string commandText, CommandType commandType)
        {
            IDbCommand dbCommand = dbConnection.CreateCommand();
            dbCommand.Connection = dbConnection;
            dbCommand.CommandType = commandType;
            dbCommand.CommandText = commandText;

            Regex regEx = new Regex(@"\@\w+");

            //Find named parameters @parameter1
            MatchCollection matches = regEx.Matches(commandText);

            foreach (Match match in matches)
            {
                if (!dbCommand.Parameters.Contains(match.ToString()))
                {
                    IDbDataParameter dbDataParameter = dbCommand.CreateParameter();
                    dbDataParameter.ParameterName = match.ToString();
                    dbCommand.Parameters.Add(dbDataParameter);    
                }
            }

            return dbCommand;
        }

        public bool IsInTransaction
        {
            get { return dbTransaction != null; }
        }

        public IDbTransaction Transaction
        {
            get { return dbTransaction; }
        }

        public void BeginTransaction()
        {
            try
            {
                dbConnection.Open();
                dbTransaction = dbConnection.BeginTransaction();
            }
            catch (Exception)
            {
                CloseConnection();
                throw;
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                dbTransaction.Rollback();
            }
            catch(Exception ex)
            {
                throw new DataSessionException(ex, "Unable to rollback transaction.") ;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void CommitTransaction()
        {
            try
            {
                dbTransaction.Commit();
            }
            catch (Exception ex)
            {
                if (dbTransaction != null)
                {
                    dbTransaction.Rollback();
                }

                throw new DataSessionException(ex, "Unable to commit transaction.");
            }
            finally
            {
                CloseConnection();
            }
        }

        public int ExecuteNonQuery(string commandText, params object[] parameterValues)
        {
            IDbCommand dbCommand = CreateCommand(commandText);
            return ExecuteNonQuery(dbCommand, parameterValues);
        }

        public int ExecuteStoredProcedure(string commandText, params object[] parameterValues)
        {
            IDbCommand dbCommand = CreateCommand(commandText, CommandType.StoredProcedure);
            return ExecuteNonQuery(dbCommand, parameterValues);
        }

        public int ExecuteNonQuery(IDbCommand dbCommand, params object[] parameterValues)
        {
            AddParameterValues(dbCommand, parameterValues);
            return ExecuteNonQuery(dbCommand);
        }

        public int ExecuteNonQuery(IDbCommand dbCommand)
        {
            if (dbConnection.State == ConnectionState.Closed)
                dbConnection.Open();

            if (IsInTransaction)
                dbCommand.Transaction = dbTransaction;

            return dbCommand.ExecuteNonQuery();
        }

        public object ExecuteScalar(string commandText, params object[] parameterValues)
        {
            IDbCommand dbCommand = CreateCommand(commandText);
            AddParameterValues(dbCommand, parameterValues);

            if (dbConnection.State == ConnectionState.Closed)
                dbConnection.Open();

            if (IsInTransaction)
                dbCommand.Transaction = dbTransaction;

            object myScalar = dbCommand.ExecuteScalar();

            return myScalar;
        }

        private static void AddParameterValues(IDbCommand dbCommand, params object[] parameterValues)
        {
            if(parameterValues.Length > 0 && parameterValues.Length == dbCommand.Parameters.Count)
            {
                for (int i = 0; i < parameterValues.Length; i++)
                {
                    ((IDbDataParameter)dbCommand.Parameters[i]).Value = 
                        (parameterValues[i] != null ? parameterValues[i] : DBNull.Value) ;
                }
            }
        }

        public IDataReader ExecuteReader(string commandText, params object[] parameterValues)
        {
            IDbCommand dbCommand = CreateCommand(commandText);
            AddParameterValues(dbCommand, parameterValues);

            if (dbConnection.State == ConnectionState.Closed)
                dbConnection.Open();

            IDataReader dataReader = dbCommand.ExecuteReader();

            return dataReader;
        }


        public DataTable ExecuteQuery(string tableName, string commandText, params object[] parameterValues)
        {
            IDbCommand dbCommand = CreateCommand(commandText);
            AddParameterValues(dbCommand, parameterValues);

            if (dbConnection.State == ConnectionState.Closed)
                dbConnection.Open();
            
            DataTable dataTable = new DataTable(tableName);
//            IDataAdapter da = dbProviderFactory.CreateDataAdapter();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = (SqlCommand) dbCommand;
            dataAdapter.Fill(dataTable);

            return dataTable;
        }



        bool isDisposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed) // only dispose once!
            {
                if (disposing)
                {
                    //safe to access objects within class
                    CloseConnection();
                }
            }
            this.isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            // tell the GC not to finalize
            GC.SuppressFinalize(this);
        }

        ~DataSession()
        {
            Dispose(false);
        }


        //DO NOT REMOVE
#if DEBUG
        [Obsolete("For testing purposes only. Not part of release build.")]
        public IDbTransaction __dbTransaction
        {
            set { dbTransaction = value; }
        }

        [Obsolete("For testing purposes only. Not part of release build.")]
        public IDbConnection __dbConnection
        {
            set { dbConnection = value; }
        }
#endif
    }
}
