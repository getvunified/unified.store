using Jhm.Common;
using Jhm.Common.Repositories;
using Jhm.Common.UOW;
using Machine.Specifications;

namespace Jhm.mSpec
{
    public abstract class RepositoryIntegrationSpecBase<T> : IntegrationSpecBase<T>
        where T : class, IEntity
    {
        public RepositoryIntegrationSpecBase()
        {
            entity = CreateEntity();
            compareEntities = CompareEntities;
            modifyEntity = ModifyEntity;
            deleteEntityDependencies = DeleteEntityDependencies;
            repository = GetRepository();
        }



        private delegate void CompareEntitiesDel(T actual, T expected);
        private delegate void ModifyEntityDel(T entity);
        private delegate void DeleteEntityDependenciesDel();
        protected abstract void ModifyEntity(T entity);
        protected abstract void CompareEntities(T actual, T expected);
        protected abstract void DeleteEntityDependencies();
        protected abstract T CreateEntity();
        protected abstract IRepository<T> GetRepository();

        
        private static T entity;
        private static CompareEntitiesDel compareEntities;
        private static ModifyEntityDel modifyEntity;
        private static DeleteEntityDependenciesDel deleteEntityDependencies;
        protected static IRepository<T> repository;


        

        public class when_repository_is_configured
        {

            public static It should_be_able_to_SAVE_a_new_entity = () => WithUOWAndTransaction(() =>
                                                                                                    {
                                                                                                        entity1 = entity;
                                                                                                        repository.Save(entity1);
                                                                                                    });

            public static It should_be_able_to_Get_Compare_Modify_And_Save_Entity1 = () => WithUOW(() =>
                                                                                                        {
                                                                                                            entity2 = repository.Get(entity1.Id);
                                                                                                            compareEntities(entity2, entity1);  //Assert saved values
                                                                                                            modifyEntity(entity2);
                                                                                                            With.Transaction(() => repository.Save(entity2));
                                                                                                        });

            public static It should_be_able_to_Get_Compare_And_Delete_Entity2 = () => WithUOW(() =>
                                                                                                    {
                                                                                                        entity3 = repository.Get(entity2.Id);
                                                                                                        compareEntities(entity3, entity2);  //Assert saved values
                                                                                                        With.Transaction(() =>
                                                                                                                        {
                                                                                                                            repository.Delete(entity3);
                                                                                                                            deleteEntityDependencies();
                                                                                                                        });
                                                                                                    });

            public static It should_have_NO_Entities_in_datastore = () => WithUOW(() =>
                                                                                        {
                                                                                            T entitydb = repository.Get(entity2.Id);
                                                                                            entitydb.ShouldBeNull();
                                                                                        });
        }

        private static T entity1;
        private static T entity2;
        private static T entity3;

    }
}