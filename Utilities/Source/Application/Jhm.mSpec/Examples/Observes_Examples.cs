using System.Collections.Generic;
using Machine.Specifications;
using Machine.Specifications.DevelopWithPassion.Rhino;

namespace Jhm.mSpec.Examples.Observers_Examples
{
    /*
     * When OPENING the car door
     *  - should turn on the interior light
     *  - should turn on the Door Ajar Beep
     *  
     * * When CLOSING the car door
     *  - should turn on the interior light
     *  - should turn on the Door Ajar Beep
     */

    public class concern : Observes<Car>
    {
        Establish c = () =>
        {
            driverIndicator = the_dependency<IDriverIndicator>();
        };
        protected static IDriverIndicator driverIndicator;
    }


    [Subject(typeof(Car))]
    public class when_OPENING_the_car_door : concern
    {
        Because of = () => sut.OpenDoor();

        It should_turn_ON_the_interior_dome_light =
            () =>
            {
                driverIndicator.received(x => x.TurnOn(Indicator.InteriorDomeLight));
                sut.ActiveIndicators().ShouldContain(Indicator.InteriorDomeLight);
            };

        It should_turn_ON_the_Door_Ajar_Beep =
            () =>
            {
                driverIndicator.received(x => x.TurnOn(Indicator.DoorAjarBeep));
                sut.ActiveIndicators().ShouldContain(Indicator.DoorAjarBeep);
            };


    }


    [Subject(typeof(Car))]
    public class when_CLOSING_the_car_door : concern
    {
        Because of = () => sut.CloseDoor();

        It should_turn_OFF_the_interior_dome_light =
            () =>
                {
                    driverIndicator.received(x => x.TurnOff(Indicator.InteriorDomeLight));
                    sut.ActiveIndicators().ShouldNotContain(Indicator.InteriorDomeLight);
                };

        private It should_turn_OFF_the_Door_Ajar_Beep =
            () =>
                {
                    driverIndicator.received(x => x.TurnOff(Indicator.DoorAjarBeep));
                    sut.ActiveIndicators().ShouldNotContain(Indicator.DoorAjarBeep);
                };

    }



    /// <summary>
    /// These class has been added ONLY for the purpose of demonstrating the test framework.
    /// </summary>
    public class Car
    {
        private readonly IDriverIndicator driverIndicator;
        private List<Indicator> activeIndicators = new List<Indicator>();

        public Car(IDriverIndicator driverIndicator)
        {
            this.driverIndicator = driverIndicator;
        }

        public IList<Indicator> ActiveIndicators()
        {
            return new List<Indicator>(activeIndicators);
        }

        public void OpenDoor()
        {
            TurnOnIndicator(Indicator.InteriorDomeLight);
            TurnOnIndicator(Indicator.DoorAjarBeep);
        }

        public void CloseDoor()
        {
            TurnOffIndicator(Indicator.InteriorDomeLight);
            TurnOffIndicator(Indicator.DoorAjarBeep);
        }

        private void TurnOnIndicator(Indicator indicator)
        {
            driverIndicator.TurnOn(indicator);
            activeIndicators.Add(indicator);
        }

        private void TurnOffIndicator(Indicator indicator)
        {
            driverIndicator.TurnOff(indicator);
            activeIndicators.Remove(indicator);
        }
    }

    /// <summary>
    /// These interface has been added ONLY for the purpose of demonstrating the test framework.
    /// </summary>
    public interface IDriverIndicator
    {
        void TurnOn(Indicator indicator);
        void TurnOff(Indicator indicator);
    }


    /// <summary>
    /// These enum has been added ONLY for the purpose of demonstrating the test framework.
    /// </summary>
    public enum Indicator
    {
        InteriorDomeLight,
        DoorAjarBeep
    }
}