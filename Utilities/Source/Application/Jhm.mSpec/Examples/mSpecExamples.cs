using Machine.Specifications;

namespace Jhm.mSpec.Examples.Operator
{

    public class concern
    {
        private Establish c = () =>
                                  {
                                      
                                      calculator = new Operator();
                                      value = 0m;
                                  };
        
        protected static Operator calculator;
        protected static decimal value;
    }


    [Subject(typeof(Operator), "Add")]
    public class when_you_add_42_plus_42 : concern
    {
        Because of = () =>
            value = calculator.Add(valueToAdd1, valueToAdd2);

        It should_equal_84 = () =>
                                          {
                                              var expectedSum = valueToAdd1 + valueToAdd2;
                                              value.ShouldEqual(expectedSum);
                                          };


        private static decimal valueToAdd1 = 42.0m;
        private static decimal valueToAdd2 = 42.0m;
    }

    [Subject(typeof(Operator))]
    public class when_adding_multiple_operands : concern
    {
        Because of = () =>
            value = calculator.Add(valueToAdd1, valueToAdd2, valueToAdd3);

        It should_add_all_operands = () =>
                                        {
                                            var expectedSum = valueToAdd1 + valueToAdd2 + valueToAdd3;
                                            value.ShouldEqual(expectedSum);
                                        };

        private static decimal valueToAdd1 = 42.0m;
        private static decimal valueToAdd2 = 42.0m;
        private static decimal valueToAdd3 = 42.0m;
    }

    public class Operator
    {
        public decimal Add(decimal firstOperand, decimal secondOperand)
        {
            return firstOperand + secondOperand;
        }

        public decimal Add(params decimal[] operands)
        {
            decimal value = 0m;

            foreach (var operand in operands)
            {
                value += operand;
            }

            return value;
        }
    }
}