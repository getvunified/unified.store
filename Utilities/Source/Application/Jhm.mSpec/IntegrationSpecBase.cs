﻿using System;
using Jhm.Common;
using Jhm.Common.UOW;

namespace Jhm.mSpec
{
    public abstract class IntegrationSpecBase<T>
        where T : class, IEntity
    {
        protected IntegrationSpecBase()
        {
            uow = GetUnitOfWork();
        }

        protected abstract UnitOfWork GetUnitOfWork();

        protected static UnitOfWork uow;


        public static void WithUOWAndTransaction(Action action)
        {
            using (uow.Start())
            {
                With.Transaction(() => action());
            }
        }

        public static void WithUOW(Action action)
        {
            using (uow.Start())
            {
                action();
            }
        }
    }
}